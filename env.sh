#!/bin/bash

# include bash base
. _base.sh

# Get the slug
slug="$1"

if [[ "$slug" == "" ]]
then
    read -p "${orange}Enter env. slug: ${reset}" slug
    slug=${slug:-"supr"}
fi
echo "${green}The environment slug \"${slug}\" will be used.${reset}"

# Get the domain
domain="$2"

if [[ "$domain" == "" ]]
then
    read -p "${orange}Enter your domain: ${reset}" domain
    domain=${domain:-supr.network}
fi
echo "${green}The domain ${blue}${underline}${domain}${nounderline}${green} will be used.${reset}"

random_string ()
{
    cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-32} | head -n 1
}

# Create a session prefix from slug
spre=${slug//[^[:alpha:]]/}
spre=${spre^^}

{
    # Add host specific settings...
    echo HOST_UID=`id -u`;
    echo HOST_GID=`id -g`;
    # Environment slug
    echo ENV_SLUG="${slug}";
    # Add MariaDB specific settings...
    echo MYSQL_HOST=supr-mariadb
    echo MYSQL_DATABASE="${slug}";
    echo MYSQL_USER="${slug}";
    echo MYSQL_PASSWORD=$(random_string 16)
    # Add PHP specific settings...
    echo OPCACHE_VALIDATE_TIMESTAMPS=1;
    echo PHP_SESSION_HOST=supr-memcached:11211;
    echo PHP_SESSION_NAME="${spre}_SESSION_ID";
    # Add WordPress specific settings...
    echo DOMAIN_CURRENT_SITE="${domain}";
    echo WORDPRESS_AUTH_KEY=$(random_string);
    echo WORDPRESS_SECURE_AUTH_KEY=$(random_string);
    echo WORDPRESS_LOGGED_IN_KEY=$(random_string);
    echo WORDPRESS_NONCE_KEY=$(random_string);
    echo WORDPRESS_AUTH_SALT=$(random_string);
    echo WORDPRESS_SECURE_AUTH_SALT=$(random_string);
    echo WORDPRESS_LOGGED_IN_SALT=$(random_string);
    echo WORDPRESS_NONCE_SALT=$(random_string);
} > .env;
