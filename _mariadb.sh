#!/bin/bash

# include bash base
. _base.sh

if [ ! -f "${_dirRoot}/supr-app/wordpress/wp-config.php" ]
then
    echo  "${bold}${magenta}Please create wp-config.php file from wp-config.sample.php at supr-app/wordpress/ directory and add your database connection data!${reset}"
    exit 1
fi

MyDockerHostName="supr-mariadb"

MySQL="mysql"
MyHOST=$(cat supr-app/wordpress/wp-config.php | grep ^[^#\;\/] | grep "define(.*DB_HOST" | sed "s/^.*define(.*DB_HOST.*, '\(.*\)');.*$/\1/")
MyUSER=$(cat supr-app/wordpress/wp-config.php | grep ^[^#\;\/] | grep "define(.*DB_USER" | sed "s/^.*define(.*DB_USER.*, '\(.*\)');.*$/\1/")
MyPASSWORD=$(cat supr-app/wordpress/wp-config.php | grep ^[^#\;\/] | grep "define(.*DB_PASSWORD" | sed "s/^.*define(.*DB_PASSWORD.*, '\(.*\)');.*$/\1/")
MyDATABASE=$(cat supr-app/wordpress/wp-config.php | grep ^[^#\;\/] | grep "define(.*DB_NAME" | sed "s/^.*define(.*DB_NAME.*, '\(.*\)');.*$/\1/")

echo "${green}Current MySQL configuration will be used:${reset}"
echo "${blue} » host: ${MyHOST}; user: ${MyUSER}; password: ${MyPASSWORD}; database: ${MyDATABASE}${reset}"
