#!/bin/bash

# include bash base
. _base.sh

# include mysql configuration
. _mariadb.sh

# get action
action="$1"

_file="$2"

if [ "${_file}" == "" ]
then
    _file="${MyDATABASE}.sql"
fi

if [ ! -d ${_dirBackup} ]
then
    mkdir -p ${_dirBackup}
    chmod 777 ${_dirBackup}
    echo "${orange}The backup directory created at \"${_dirBackup}\"${reset}"
fi

if [ "$action" == "backup" ]
then
    read -p "${orange}Backup MariaDB (y/n)? ${reset}"
    if [ "$REPLY" == "y" ]
    then
        if [ -f ${_dirBackup}/${_file} ]
        then
            _now=$(date +"%Y%m%d_%H%M%S")
            _fileBackup="${MyDATABASE}~${_now}.sql"

            read -p "${orange}Old MariaDB dump file \"${_file}\" at \"${_dirBackup}\" directory found! Create a copy to \"${_fileBackup}\" (y/n)? ${reset}"
            if [ "$REPLY" == "y" ]
            then
                cp ${_dirBackup}/${_file} ${_dirBackup}/${_fileBackup}
                echo "${underline}${green}Old MariaDB dump file \"${_file}\" stored to \"${_fileBackup}\" at \"${_dirBackup}\" directory${reset}"
            fi
        fi

        read -p "${orange}Create new MariaDB dump to file \"${_file}\" at \"${_dirBackup}\" directory (y/n)? ${reset}"
        if [ "$REPLY" == "y" ]
        then
            if [ "$(docker ps -a | grep ${MyDockerHostName})" ]
            then
                echo "$ docker exec ${MyDockerHostName} /usr/bin/mysqldump --extended-insert=FALSE -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} > ${_dirBackup}/${_file}"
                docker exec ${MyDockerHostName} /usr/bin/mysqldump --extended-insert=FALSE -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} > ${_dirBackup}/${_file}
            else
                echo "$ mysqldump --extended-insert=FALSE -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} > ${_dirBackup}/${_file}"
                mysqldump --extended-insert=FALSE -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} > ${_dirBackup}/${_file}
            fi
            echo "${underline}${green}The MariaDB dump file \"${_file}\" at \"${_dirBackup}\" directory was created${reset}"
        fi
    fi
elif [ "$action" == "restore" ]
then
    if [ -f ${_dirBackup}/${_file} ]
    then
        read -p "${orange}Restore the MariaDB dump file \"${_file}\" at \"${_dirBackup}\" directory (y/n)? ${reset}"
        if [ "$REPLY" == "y" ]
        then
            if [ "$(docker ps -a | grep ${MyDockerHostName})" ]
            then
                echo "$ cat ${_dirRoot}/${MyDockerHostName}/drop-tables.sql | docker exec -i ${MyDockerHostName} /usr/bin/mysql -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE}"
                cat ${_dirRoot}/${MyDockerHostName}/drop-tables.sql | docker exec -i ${MyDockerHostName} /usr/bin/mysql -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE}
                echo "$ cat ${_dirBackup}/${_file} | docker exec -i ${MyDockerHostName} /usr/bin/mysql -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE}"
                cat ${_dirBackup}/${_file} | docker exec -i ${MyDockerHostName} /usr/bin/mysql -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE}
            else
                echo "$ mysql -A -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} < ${_dirRoot}/${MyDockerHostName}/drop-tables.sql"
                mysql -A -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} < ${_dirRoot}/${MyDockerHostName}/drop-tables.sql
                echo "$ mysql -A -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} < ${_dirBackup}/${_file}"
                mysql -A -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} < ${_dirBackup}/${_file}
            fi
            echo "${underline}${green}The MariaDB dump file \"${_file}\" at \"${_dirBackup}\" directory was restored${reset}"
        fi
    else
        echo -e "${underline}${red}The MariaDB dump file \"${_file}\" at \"${_dirBackup}\" directory not found!${reset}"
    fi
else
    echo -e "${bold}Options: backup | restore${reset}"
fi
