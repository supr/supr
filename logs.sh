#!/bin/bash

# include bash base
. _base.sh

read -p "${orange}All log files will be deleted (y/n)? ${reset}"
if [[ "$REPLY" != "y" ]]
then
    echo "${underline}${magenta}Aborting deleting all log files!${reset}"
    exit 1
fi

#find ./.log/ -type f -name "*.log" -exec rm -i {} \;
find ./.log/ -type f -name "*.log" -exec rm -f {} \;
echo "${underline}${green}All log files deleted!${reset}"
