#!/bin/sh

CONTAINER_IP=$(hostname -i)

# Sync fixtures
rsync -r --ignore-existing /app/fixtures/acme-certs/ /opt/acme-certs

gunicorn -w 2 -b "${CONTAINER_IP}:5000" --error-logfile /var/log/gunicorn/error.log --access-logfile /var/log/gunicorn/access.log api:app
