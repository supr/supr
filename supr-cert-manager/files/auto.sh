#!/bin/bash

FUNCTION_TO_RUN=$1
DOMAIN=$2
ACME_SH_OPTIONS=$3

echo "Start: certificate creation"
# TODO: run test against staging server of letsencrypt
# certbot certonly --webroot -w /var/www/certs -d ${DOMAIN}
#        if [ $? -ne 0 ]; then
#          exit 0
#        fi

function create_cert {
    rm -rf /var/log/acmesh/${DOMAIN}_create_cert.log
    /var/custom/scripts/acme.sh --issue --domain ${DOMAIN} --webroot /opt/acme-challenge --cert-home /opt/acme-certs ${ACME_SH_OPTIONS} &>> /var/log/acmesh/${DOMAIN}_create_cert.log && \
    cp -v /app/files/www.sample.com.conf /etc/nginx-domains-conf-to-be-tested/${DOMAIN}.conf &>> /var/log/acmesh/${DOMAIN}_create_cert.log && \
    sed -i "s/#DOMAIN_HOLDER#/${DOMAIN}/g" /etc/nginx-domains-conf-to-be-tested/${DOMAIN}.conf &>> /var/log/acmesh/${DOMAIN}_create_cert.log && \
    echo "# sed complete. Printing resulting file:" &>> /var/log/acmesh/${DOMAIN}_create_cert.log && \
    cat /etc/nginx-domains-conf-to-be-tested/${DOMAIN}.conf &>> /var/log/acmesh/${DOMAIN}_create_cert.log
}

function domain_current_site_cert {
    rm -rf /var/log/acmesh/${DOMAIN}_domain_current_site_cert.log
    /var/custom/scripts/acme.sh --issue --domain ${DOMAIN} --domain "*.${DOMAIN}" --dns dns_aws --cert-home /opt/acme-certs ${ACME_SH_OPTIONS} &>> /var/log/acmesh/${DOMAIN}_domain_current_site_cert.log && \
    cp -v /app/files/www.sample.com.conf /etc/nginx-domains-conf-to-be-tested/${DOMAIN}.conf &>> /var/log/acmesh/${DOMAIN}_domain_current_site_cert.log && \
    sed -i "s/server_name #DOMAIN_HOLDER#/server_name .${DOMAIN}/g" /etc/nginx-domains-conf-to-be-tested/${DOMAIN}.conf &>> /var/log/acmesh/${DOMAIN}_domain_current_site_cert.log && \
    sed -i "s/#DOMAIN_HOLDER#/${DOMAIN}/g" /etc/nginx-domains-conf-to-be-tested/${DOMAIN}.conf &>> /var/log/acmesh/${DOMAIN}_domain_current_site_cert.log && \
    echo "# sed complete. Printing resulting file:" &>> /var/log/acmesh/${DOMAIN}_domain_current_site_cert.log && \
    cat /etc/nginx-domains-conf-to-be-tested/${DOMAIN}.conf &>> /var/log/acmesh/${DOMAIN}_domain_current_site_cert.log
}

function renew_cert {
    rm -rf /var/log/acmesh/${DOMAIN}_renew_cert.log
    /var/custom/scripts/acme.sh --renew --domain ${DOMAIN} --webroot /opt/acme-challenge --cert-home /opt/acme-certs ${ACME_SH_OPTIONS} &>> /var/log/acmesh/${DOMAIN}_renew_cert.log
}

function revoke_cert {
    rm -rf /var/log/acmesh/${DOMAIN}_revoke_cert.log
    /var/custom/scripts/acme.sh --revoke --domain ${DOMAIN} --webroot /opt/acme-challenge --cert-home /opt/acme-certs &>> /var/log/acmesh/${DOMAIN}_revoke_cert.log
}

function remove_cert {
    rm -rf /var/log/acmesh/${DOMAIN}_remove_cert.log
    /var/custom/scripts/acme.sh --remove --domain ${DOMAIN} --webroot /opt/acme-challenge --cert-home /opt/acme-certs &>> /var/log/acmesh/${DOMAIN}_remove_cert.log && \
    echo "# deleting cert and conf file if any" &>> /var/log/acmesh/${DOMAIN}_remove_cert.log && \
    rm -rfv /opt/acme-certs/${DOMAIN} /etc/nginx-domains-conf/${DOMAIN}.conf /etc/nginx-domains-conf-to-be-tested/${DOMAIN}.conf &>> /var/log/acmesh/${DOMAIN}_remove_cert.log
}

function test_config_nginx {
    curl -v -H 'Content-Type: application/json' -X POST --unix-socket /var/run/docker.sock http://localhost/containers/s2-nginx/exec -d '{"AttachStdin": true,"AttachStdout": true,"AttachStderr": true,"Cmd": ["nginx", "-T"],"Tty": true}'
}

function reload_nginx {
    # Reload nginx
    curl -v -H 'Content-Type: application/json' -X POST --unix-socket /var/run/docker.sock http://localhost/containers/s2-nginx/exec -d '{"AttachStdin": true,"AttachStdout": true,"AttachStderr": true,"Cmd": ["nginx", "-s", "reload"],"DetachKeys": "ctrl-p,ctrl-q","Privileged": true,"Tty": true}'
}

${FUNCTION_TO_RUN}