import os, time, sys, logging, docker, subprocess, shutil
from flask import Flask, request, make_response, jsonify


app = Flask(__name__)


@app.route('/certificate/create_cert', methods=["GET"])
def create_cert():
    domain = request.args.get('domain')
    acme_sh_options = request.args.get('acme_sh_options')

    if (acme_sh_options is None) or (acme_sh_options == ""):
        bash_command = 'bash /app/files/auto.sh create_cert ' + str(domain)
    else:
        bash_command = 'bash /app/files/auto.sh create_cert ' + str(domain) + ' ' + str(acme_sh_options)

    try:
        if not domain:
            raise Exception('"domain" is required for this endpoint')

        result = os.system(bash_command)

        with open('/var/log/acmesh/' + str(domain) + '_create_cert.log') as f:
            read_log = f.read()
        f.close()

        if result == 0:
            return make_response(jsonify({"status": "success", "message": read_log}), 200)
        return make_response(jsonify({"status": "failed", "message": read_log}), 400)

    except Exception as e:
        return make_response(jsonify({"status": "failed", "message": str(e)}), 400)


@app.route('/certificate/domain_current_site_cert', methods=["GET"])
def domain_current_site_cert():
    domain = request.args.get('domain')
    acme_sh_options = request.args.get('acme_sh_options')

    if (acme_sh_options is None) or (acme_sh_options == ""):
        bash_command = 'bash /app/files/auto.sh domain_current_site_cert ' + str(domain)
    else:
        bash_command = 'bash /app/files/auto.sh domain_current_site_cert ' + str(domain) + ' ' + str(acme_sh_options)

    try:
        if not domain:
            raise Exception('"domain" is required for this endpoint')

        result = os.system(bash_command)

        with open('/var/log/acmesh/' + str(domain) + '_domain_current_site_cert.log') as f:
            read_log = f.read()
        f.close()

        if result == 0:
            return make_response(jsonify({"status": "success", "message": read_log}), 200)
        return make_response(jsonify({"status": "failed", "message": read_log}), 400)

    except Exception as e:
        return make_response(jsonify({"status": "failed", "message": str(e)}), 400)


@app.route('/certificate/renew_cert', methods=["GET"])
def renew_cert():
    domain = request.args.get('domain')
    acme_sh_options = request.args.get('acme_sh_options')

    if (acme_sh_options is None) or (acme_sh_options == ""):
        bash_command = 'bash /app/files/auto.sh renew_cert ' + str(domain)
    else:
        bash_command = 'bash /app/files/auto.sh renew_cert ' + str(domain) + ' ' + str(acme_sh_options)

    try:
        if not domain:
            raise Exception('"domain" is required for this endpoint')

        result = os.system(bash_command)

        with open('/var/log/acmesh/' + str(domain) + '_renew_cert.log') as f:
            read_log = f.read()
        f.close()

        if result == 0:
            return make_response(jsonify({"status": "success", "message": read_log}), 200)
        return make_response(jsonify({"status": "failed", "message": read_log}), 400)

    except Exception as e:
        return make_response(jsonify({"status": "failed", "message": str(e)}), 400)


@app.route('/certificate/revoke_cert', methods=["GET"])
def revoke_cert():
    domain = request.args.get('domain')

    try:
        if not domain:
            raise Exception('"domain" is required for this endpoint')

        result = os.system('bash /app/files/auto.sh revoke_cert ' + str(domain))

        with open('/var/log/acmesh/' + str(domain) + '_revoke_cert.log') as f:
            read_log = f.read()
        f.close()

        if result == 0:
            return make_response(jsonify({"status": "success", "message": read_log}), 200)
        return make_response(jsonify({"status": "failed", "message": read_log}), 400)

    except Exception as e:
        return make_response(jsonify({"status": "failed", "message": str(e)}), 400)


@app.route('/certificate/remove_cert', methods=["GET"])
def remove_cert():
    domain = request.args.get('domain')

    try:
        if not domain:
            raise Exception('"domain" is required for this endpoint')

        result = os.system('bash /app/files/auto.sh remove_cert ' + str(domain))

        with open('/var/log/acmesh/' + str(domain) + '_remove_cert.log') as f:
            read_log = f.read()
        f.close()

        if result == 0:
            return make_response(jsonify({"status": "success", "message": read_log}), 200)
        return make_response(jsonify({"status": "failed", "message": read_log}), 400)

    except Exception as e:
        return make_response(jsonify({"status": "failed", "message": str(e)}), 400)


@app.route('/certificate/cert_age', methods=["GET"])
def cert_age():
    domain = request.args.get('domain')

    try:
        if not domain:
            raise Exception('"domain" is required for this endpoint')

        cert_file_path = '/opt/acme-certs/' + str(domain) + '/' + str(domain) + '.cer'

        exists = os.path.isfile(cert_file_path)

        if exists:
            result = int((time.time() - os.path.getatime(cert_file_path)) / 60 / 60 / 24)
            return make_response(jsonify({"status": "success", "message": {"days": str(result)}}), 200)
        else:
            return make_response(jsonify({"status": "failed", "message": "cert does not exists"}), 400)

    except Exception as e:
        return make_response(jsonify({"status": "failed", "message": str(e)}), 400)


@app.route('/nginx/cert_conf_exists', methods=["GET"])
def cert_conf_exists():
    domain = request.args.get('domain')

    try:
        if not domain:
            raise Exception('"domain" is required for this endpoint')

        conf_exists = os.path.isfile('/etc/nginx-domains-conf/' + str(domain) + '.conf')
        cert_exists = os.path.isfile('/opt/acme-certs/' + str(domain) + '/' + str(domain) + '.cer')
        key_exists = os.path.isfile('/opt/acme-certs/' + str(domain) + '/' + str(domain) + '.key')

        if conf_exists and cert_exists and key_exists:
            return make_response(jsonify({"status": "success", "message": "conf and cert exist"}), 200)
        else:
            return make_response(jsonify({"status": "failed", "message": "conf and/or cert do not exist"}), 400)

    except Exception as e:
        return make_response(jsonify({"status": "failed", "message": str(e)}), 400)


@app.route('/nginx/test_conf_move_to_live', methods=["GET"])
def test_conf_move_to_live():
    try:
        return_code = subprocess.call(['nginx', '-T'])
        result = subprocess.check_output(['nginx', '-T'])

        if return_code == 0:
            src_dir = '/etc/nginx-domains-conf-to-be-tested/'
            dst_dir = '/etc/nginx-domains-conf/'

            conf_files = os.listdir(src_dir)

            for f in conf_files:
                shutil.move(src_dir + f, dst_dir + f)

            return make_response(jsonify({"status": "success",
                                          "message": {"nginx_reload_result": str(result),
                                                      "conf_files_moved": conf_files}}), 200)
        else:
            return make_response(jsonify({"status": "failed", "message": str(result)}), 400)
    except Exception as e:
        return make_response(jsonify({"status": "failed", "message": str(e)}), 400)


@app.route('/nginx/test_config', methods=["GET"])
def test_config_nginx():
    try:
        client = docker.DockerClient(base_url='unix://var/run/docker.sock')
        container = client.containers.get('s2-nginx')
        result = container.exec_run(cmd="nginx -T", tty=True)
        return make_response(jsonify({"status": "success", "message": str(result)}), 200)
    except Exception as e:
        return make_response(jsonify({"status": "failed", "message": str(e)}), 400)


@app.route('/nginx/reload', methods=["GET"])
def reload_nginx():
    try:
        client = docker.DockerClient(base_url='unix://var/run/docker.sock')
        container = client.containers.get('s2-nginx')
        result = container.exec_run(cmd="nginx -s reload", tty=True)
        return make_response(jsonify({"status": "success", "message": str(result)}), 200)
    except Exception as e:
        return make_response(jsonify({"status": "failed", "message": str(e)}), 400)


if __name__ == '__main__':
    app.run()
    app.logger.addHandler(logging.StreamHandler(stream=sys.stdout))
    app.logger.setLevel(logging.INFO)
