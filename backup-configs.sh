#!/bin/bash

# include bash base
. _base.sh

# get action
action="$1"

if [ ! -d ${_dirBackup} ]
then
    mkdir -p ${_dirBackup}
    chmod 777 ${_dirBackup}
    echo "${orange}The backup directory created at \"${_dirBackup}\"${reset}"
fi

if [ "$action" == "backup" ]
then
    read -p "${orange}Backup wp-config.php (y/n)? ${reset}"
    if [ "$REPLY" == "y" ]
    then
        if [ -f "${_dirBackup}/wp-config.php" ]
        then
            _now=$(date +"%Y%m%d_%H%M%S")
            _configFileHistory="${_dirBackup}/wp-config~${_now}.php"

            read -p "${orange}Old backup of the config file found! Make a history of it to \"${_configFileHistory}\" (y) or it will be deleted (n)? ${reset}"
            if [ "$REPLY" == "y" ]
            then
                mv ${_dirBackup}/wp-config.php ${_configFileHistory}
                echo "${underline}${green}The history of the \"${_dirBackup}/wp-config.php\" file created at \"${_configFileHistory}\"${reset}"
            fi
        fi

        cp wordpress/wp-config.php ${_dirBackup}/wp-config.php
        echo "${underline}${green}The backup of the \"wordpress/wp-config.php\" to \"${_dirBackup}/wp-config.php\" created${reset}"
    fi
elif [ "$action" == "restore" ]
then
    read -p "${orange}Restore wp-config.php (y/n)? ${reset}"
    if [ "$REPLY" == "y" ]
    then
        cp ${_dirBackup}/wp-config.php wordpress/wp-config.php
        echo "${underline}${green}The \"${_dirBackup}/wp-config.php\" was restored to \"wordpress/wp-config.php\"${reset}"
    fi
else
    echo -e "${bold}Options: backup | restore${reset}"
fi
