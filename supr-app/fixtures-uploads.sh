#!/bin/bash

# include bash base
. _base.sh

# get action
action="$1"

# File for check if fixtures was already loaded
testFile="sites/8/GeoLite2-Country.mmdb"

_dirUploadsFixtures="${_dirFixtures}/uploads"
_dirUploads="${_dirRoot}/wordpress/wp-content/uploads"

# For automated actions we need this script to shut up
if [ "$2" == "y" ] || [ "$2" == "silent" ]
then
    silent="$2"
else
    silent=""
fi

# Check if we have already fixtures on the storage
if ([ "$silent" == "y" ] || [ "$silent" == "silent" ]) && [ -f "$_dirUploads/$testFile" ]
then
    echo "${underline}${green}The fixtures already exist!${reset}"
    exit 0
fi

# @todo Load the files into a container!!!
#docker cp fixtures/uploads/sites/ "$(docker-compose ps -q s2-app)":/var/www/supr/wordpress/wp-content/uploads/sites

if [ ! -d "${_dirUploadsFixtures}" ]
then
    mkdir -p ${_dirUploadsFixtures}
    chmod 777 ${_dirUploadsFixtures}
    echo "${underline}${green}The fixtures dump target directory at \"${_dirUploadsFixtures}\" created!${reset}"
fi

if [ "$action" == "dump" ]
then
    if [ "$silent" == "" ]
    then
        read -p "${orange}Dump the uploads from \"${_dirUploads}\" to fixtures \"${_dirUploadsFixtures}\" directory (y/n)? ${reset}"
    fi

    if [ "$REPLY" == "y" ] || [ "$silent" == "y" ] || [ "$silent" == "silent" ]
    then
        rsync -auv --exclude=".*" ${_dirUploads}/ ${_dirUploadsFixtures}
        find ${_dirUploadsFixtures} -name '*.log' -type f -delete
        echo "${underline}${green}The uploads dump from \"${_dirUploads}\" to fixtures \"${_dirUploadsFixtures}\" directory finished${reset}"
    fi
elif [ "${action}" == "load" ]
then
    if [ "$silent" == "" ]
    then
        read -p "${orange}Restore the fixtures from \"${_dirUploadsFixtures}\" to \"${_dirUploads}\" directory (y/n)? ${reset}"
    fi

    if [ "$REPLY" == "y" ] || [ "$silent" == "y" ] || [ "$silent" == "silent" ]
    then
        # Before syncing the fixtures to the storage we change the directory mode...
        find ${_dirUploadsFixtures}/ -type d -exec chmod 0777 {} \;
        # For the files included we do the same...
        find ${_dirUploadsFixtures}/ -type f -exec chmod 0666 {} \;
        rsync -aruv --chmod=+rw --exclude='.*' --exclude-from 'exclude-list.txt' ${_dirUploadsFixtures}/ ${_dirUploads}
        echo "${underline}${green}The fixtures from \"${_dirUploadsFixtures}\" successful loaded into the uploads at \"${_dirUploads}\" directory!${reset}"
    fi
else
    echo -e "${bold}Options: dump | load${reset}"
fi
