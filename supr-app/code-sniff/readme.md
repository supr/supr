# Code style check

This script is started with each circle-ci build.

The script checks code style of all our plugins (PHP, CSS and JS).

# Run local

```bash
cd ./s2-app/code-sniff
./codestyle-check.sh
```
