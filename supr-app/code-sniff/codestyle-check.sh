#!/bin/bash

set -e
set -u
set -o pipefail

PATHS_TO_CHECK=( '../wordpress/wp-content/*plugins/supr-*' '../wordpress/wp-content/themes/*-supr' )

# Installs git
function maybe_install_git() {
    # If git exists then return
    if which git >/dev/null; then
        return
    else
        # Install git
        sudo apt-get install git-all
    fi

    echo 'Git installed.'

    return
}

# Installs PHPCS and PHPBF
function maybe_install_dependencies() {
    maybe_install_git

    # Install phpcs with git clone
    [[ ! -f "$HOME/PHP_CodeSniffer/phpcs.phar" ]] && wget https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar -P $HOME/PHP_CodeSniffer
    [[ ! -f "$HOME/PHP_CodeSniffer/phpcbf.phar" ]] && wget https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar -P $HOME/PHP_CodeSniffer
    [[ ! -d "$HOME/PHPCompatibility" ]] && git clone --branch 9.3.5 git@github.com:PHPCompatibility/PHPCompatibility.git $HOME/PHPCompatibility

    echo 'All packages installed.'

    return
}

function php_code_sniff() {
    maybe_install_dependencies

    # Set new config file
    php $HOME/PHP_CodeSniffer/phpcs.phar --config-set installed_paths $HOME/PHPCompatibility/PHPCompatibility

    # Do sniffing
    for i in "${PATHS_TO_CHECK[@]}"
    do
        #Auto fix errors
        #php $HOME/PHP_CodeSniffer/phpcbf.phar --standard=./config.xml --extensions=php,css,js -p -s --colors $i

        # Check code for errors
        php $HOME/PHP_CodeSniffer/phpcs.phar --standard=./config.xml --extensions=php,css,js -p -s --colors $i
    done

    return
}

php_code_sniff
exit 0
