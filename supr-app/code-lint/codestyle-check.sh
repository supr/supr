#!/bin/bash

set -e
set -u
set -o pipefail

PATHS_TO_CHECK=('../wordpress/wp-content/*plugins/supr-*' '../wordpress/wp-content/themes/*-supr' '../../e2e/cypress')

# Installs lint
function install_lint() {
    # Install npm
    sudo apt install npm
    sudo ln -s /usr/bin/nodejs /usr/bin/node
    sudo npm install -g n
    sudo n stable
    # Install node packet manager
    #npm install nvm
    #PATH="$PATH"
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
    export NVM_DIR="/opt/circleci/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
    # Change node version
    nvm install 10.21.0
    nvm use 10.21.0
    # Install stylelint
    sudo npm install stylelint -g
    sudo npm install stylelint
    # Install config recommended
    sudo npm install stylelint-config-recommended --save-dev
    # Install config standart
    sudo npm install stylelint-config-standard --save-dev
    # Install config recommended for scss
    sudo npm install stylelint-scss stylelint-config-recommended-scss --save-dev

    echo 'Stylelint installed.'
}

function lint() {
    install_lint

    # Do linting
    for i in "${PATHS_TO_CHECK[@]}"; do
        # Autofix errors
        #stylelint --config=./config.json "$i/**/*.*css" "$i/**/*.js" --fix

        # Check code for errors
        echo "Check: $i"
        stylelint --config=./config.json "$i/**/*.*css" "$i/**/*.js"
    done

    return
}

lint
exit 0
