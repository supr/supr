<?php
/**
 * Plugin Name: SUPR - WooCommerce Mods
 * Plugin URI: https://supr.com
 * Description: SUPR WooCommerce extension to add additional options and functions.
 * Text Domain: supr-woocommerce
 * Domain Path: /languages
 * Version: 2.0.0
 * Author: SUPR
 * Author URI: https://supr.com
 */

define('SUPR_WOOCOMMERCE_PATH', plugin_dir_path(__FILE__));
define('SUPR_WOOCOMMERCE_URL', plugins_url('/', __FILE__));
define('SUPR_WOOCOMMERCE_FILE', plugin_basename(__FILE__));
define('SUPR_WOOCOMMERCE_DIR_NAME', basename(__DIR__));

define('SUPR_WOOCOMMERCE_VERSION', '2.0.0');

// Load main Class
require SUPR_WOOCOMMERCE_PATH . 'includes/Plugin.php';
