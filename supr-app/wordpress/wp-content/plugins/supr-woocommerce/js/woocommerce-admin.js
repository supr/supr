jQuery(document).ready(function ($) {
    $('#variable_product_options_inner').find('.button-primary').attr('href', 'https://supr.help/produkte/variationen-bzw-varianten-hinzufuegen/');
    $('body.post-type-shop_coupon .woocommerce-BlankState a').not('.button-primary').attr('href', 'https://supr.help/apps/gutscheine/');
    $('body.post-type-shop_order .woocommerce-BlankState a').attr('href', 'https://supr.help/inhalte/bestellungen/');
});