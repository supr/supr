jQuery(document).ready(function ($) {

    /**
     * CART
     */

    // Update cart on quantity change
    $('.woocommerce').on('change blur keyup', 'input.qty', callFunctionWithDelay(function () {
        // Update only if we nave a number in qty field
        var numberReg = /^\d+$/;
        var qty = $(this).val();
        if (numberReg.test(qty) && parseInt(qty) > 0) {
            $("button[name='update_cart']").trigger('click');
        }
    }, 1000));

    /**
     * CHECKOUT
     */

    // Additional text for creation account
    var checkout_createaccount_additional_text_showed = false;

    $('input#createaccount').change(function () {
        if (checkout_createaccount_additional_text_showed) {
            return;
        }
        $(this).closest('div').append($('<p>').html(supr_woocommerce.checkout_createaccount_additional_text));
        checkout_createaccount_additional_text_showed = true;
    });
});

/**
 * Call a function with delay.
 * It can be used if you need to know when the user stopped his input.
 *
 * @param fn
 * @param ms
 * @returns {function(...[*]=)}
 */
function callFunctionWithDelay(fn, ms)
{
    let timer = 0;
    return function (...args) {
        clearTimeout(timer);
        timer = setTimeout(fn.bind(this, ...args), ms || 0);
    }
}
