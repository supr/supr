<?php

namespace SuprWoocommerce;

/**
 * Class Menu
 *
 * @package SuprWoocommerce
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add menu page
        \add_action('network_admin_menu', [__CLASS__, 'createMenu']);

        // Add settings link on plugin page
        \add_filter('network_admin_plugin_action_links_' . SUPR_WOOCOMMERCE_FILE, [__CLASS__, 'addSettingsPage']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createMenu(): void
    {
        \add_menu_page(
            __('SUPR Woocommerce', 'supr-woocommerce'),
            __('SUPR Woocommerce', 'supr-woocommerce'),
            'manage_network',
            'supr_woocommerce_settings',
            [Page::class, 'networkSettingsPage'],
            'dashicons-cart'
        );
    }

    /**
     * @param $links
     * @return mixed
     */
    public static function addSettingsPage($links)
    {
        $settings_link = '<a href="admin.php?page=supr_woocommerce_settings">' . __('Settings', 'supr-woocommerce') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
