<?php

namespace SuprWoocommerce;

/**
 * Class Plugin
 *
 * @package SuprWoocommerce
 */
class Plugin
{
    /**
     * @var Plugin
     */
    private static $instance;

    /**
     * @return Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Admin init
     */
    public function adminInit(): void
    {
        // Add js into footer
        \add_action('admin_enqueue_scripts', [ResourceManager::class, 'addAdminResources'], 999);
    }

    /**
     * Init
     */
    public function init(): void
    {
        Menu::addMenu();

        // Add delete action to customer accounts
        CustomerAccount::addMenuItemDeleteAccount();

        // Add store owner fields
        \add_filter('woocommerce_general_settings', [GeneralSettings::class, 'addStoreOwnerFields']);

        // Replace WC Register emails
        Email::replaceWooCommerceRegisterUserEmail();

        if (!\is_admin()) {
            \add_action('wp_enqueue_scripts', [ResourceManager::class, 'addWooCommerceResources'], 999);
        }

        // Maybe someone use it :(
        ShortCode::registerSuprWoocommerceGermanizedShortCode();

        // Add payment method
        \add_filter('woocommerce_payment_gateways', [GatewayManager::class, 'addPaymentOnPickUp']);

        // Disable country sort
        \add_filter('woocommerce_sort_countries', [Country::class, 'disableCountrySorting']);

        // If we are in germanized settings (it should be happen before admin_init)
        if (isset($_GET['tab']) && $_GET['tab'] === 'germanized') {
            // Delete some features
            // @todo delete it if we have Pro version
            \add_filter('woocommerce_gzd_admin_settings_tabs', [Germanized::class, 'deleteSettingsTabs'], 11, 1);
        }

        // Add order amount restrictions
        OrderAmountRestriction::init();
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // Load translations
        add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        add_action('admin_init', [$this, 'adminInit']);
        add_action('init', [$this, 'init']);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_WOOCOMMERCE_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::instance();
