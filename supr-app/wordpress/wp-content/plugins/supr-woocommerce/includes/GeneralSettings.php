<?php

namespace SuprWoocommerce;

/**
 * Class GeneralSettings
 *
 * @package SuprWoocommerce
 */
class GeneralSettings
{
    /**
     * Add additional settings fields
     *
     * @param array $settings
     * @return array
     */
    public static function addStoreOwnerFields($settings): array
    {
        $storeOwner = [
            [
                'title' => __('Store Owner', 'supr-woocommerce'),
                'type' => 'title',
                'desc' => __('Enter the contact details and basic information for the store', 'supr-woocommerce'),
                'id' => 'store_owner',
            ],
            [
                'title' => __('Company', 'supr-woocommerce'),
                'desc' => __('Enter name of your company or store', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_company',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'title' => __('Store category', 'supr-woocommerce'),
                'desc' => __('Select a category that fits to your store', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_store_category',
                'desc_tip' => true,
                'default' => '5964',
                'type' => 'select',
                // Copy/Paste from COP-API
                'options' => [
                    '5964' => 'Andere Waren',
                    '5912' => 'Apotheken',
                    '5511' => 'Automobilindustrie',
                    '5192' => 'Bücher &amp; Zeitschriften',
                    '5999' => 'Büro- &amp; Gastroartikel',
                    '4816' => 'Datenhosting',
                    '7273' => 'Dating / Flirt &amp; Community',
                    '5967' => 'Erotik E-Commerce',
                    '6211' => 'Finanzdienstleistung',
                    '600' => 'Fluglinien',
                    '7999' => 'Freizeit &amp; Familie',
                    '5977' => 'Gesundheitspflege',
                    '7995' => 'Glücksspiel',
                    '5251' => 'Haushalts- &amp; Gartenartikel',
                    '7922' => 'Kartenvertrieb &amp; Couponing',
                    '602' => 'Marktplätze',
                    '5311' => 'Möbel &amp; Haushaltswaren',
                    '5621' => 'Mode Frauen',
                    '5641' => 'Mode Kinder/Baby',
                    '5611' => 'Mode Männer',
                    '5691' => 'Mode Männer/Frauen',
                    '5735' => 'Musik Downloads',
                    '5499' => 'Nahrungsmittel',
                    '9399' => 'Öffentliche Hand',
                    '603' => 'Online-Spiele',
                    '7221' => 'Photografie',
                    '4722' => 'Reisevermittlung',
                    '5094' => 'Schmuck',
                    //'5977' => 'Schönheit &amp; Kosmetik',
                    '7372' => 'Software Downloads',
                    '7299' => 'Sonstige Dienstleistungen',
                    '5941' => 'Sport &amp; Sportbekleidung',
                    '4121' => 'Taxi &amp; Limousinenservice',
                    '4814' => 'Telekommunikation',
                    '4789' => 'Transport- &amp; Kurierdienstleistungen',
                    '5732' => 'Unterhaltungselektronik',
                    '6300' => 'Versicherung'
                ]
            ],
            [
                'title' => __('Salutation', 'supr-woocommerce'),
                'desc' => __('Select your salutation', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_salutation',
                'desc_tip' => true,
                'default' => 'mr',
                'type' => 'select',
                'options' => [
                    'm' => __('Mr', 'supr-woocommerce'),
                    'f' => __('Mrs', 'supr-woocommerce')
                ]
            ],
            [
                'title' => __('First name', 'supr-woocommerce'),
                'desc' => __('The first name of the store owner', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_first_name',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'title' => __('Last name', 'supr-woocommerce'),
                'desc' => __('The last name of the store owner', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_last_name',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'title' => __('Value added tax', 'supr-woocommerce'),
                'desc' => __('Enter a value added tax', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_vat',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'title' => __('Commercial register number', 'supr-woocommerce'),
                'desc' => __('Enter your commercial register number', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_commercial_register_number',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'type' => 'sectionend',
                'id' => 'store_owner',
            ]
        ];

        $storeContact = [
            [
                'title' => __('Store contact', 'supr-woocommerce'),
                'type' => 'title',
                'desc' => __('Enter the contact details for your store', 'supr-woocommerce'),
                'id' => 'store_contact',
            ],
            [
                'title' => __('E-Mail', 'supr-woocommerce'),
                'desc' => __('This is the e-mail to get in contact with you', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_email',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'title' => __('Phone', 'supr-woocommerce'),
                'desc' => __('Enter a phone number', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_phone',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'title' => __('Phone costs', 'supr-woocommerce'),
                'desc' => __('Enter a price information', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_phone_costs',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'title' => __('Fax', 'supr-woocommerce'),
                'desc' => __('Enter a fax number', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_fax',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'title' => __('Fax costs', 'supr-woocommerce'),
                'desc' => __('Enter a price information', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_fax_costs',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'type' => 'sectionend',
                'id' => 'store_contact',
            ],
            [
                'title' => __('Marketing', 'supr-woocommerce'),
                'type' => 'title',
                'desc' => __('Enter the contact details for your store', 'supr-woocommerce'),
                'id' => 'store_contact',
            ],
            [
                'title' => __('Facebook page', 'supr-woocommerce'),
                'desc' => __('Enter the full link to your facebook page', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_marketing_facebook',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'title' => __('Instagram name', 'supr-woocommerce'),
                'desc' => __('Enter your instagram name ', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_marketing_instagram',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'title' => __('Twitter name', 'supr-woocommerce'),
                'desc' => __('Enter your twitter name ', 'supr-woocommerce'),
                'id' => 'woocommerce_store_owner_marketing_twitter',
                'default' => '',
                'type' => 'text',
                'desc_tip' => true,
            ],
            [
                'type' => 'sectionend',
                'id' => 'store_contact',
            ],
        ];

        array_splice($settings, 0, 0, $storeOwner);
        array_splice($settings, 16, 0, $storeContact);

        return $settings;
    }
}
