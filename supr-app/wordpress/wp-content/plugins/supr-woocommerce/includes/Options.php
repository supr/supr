<?php

namespace SuprWoocommerce;

/**
 * Class Options
 *
 * @package SuprWoocommerce
 */
class Options
{
    /**
     * @var Options
     */
    private static $instance;

    /**
     * @var array
     */
    private $options;

    /**
     * @return Options
     */
    public static function instance(): Options
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
    }

    /**
     * Get option by name
     *
     * @param $optionName
     * @return mixed|null
     */
    public function getOption($optionName)
    {
        if ($this->options !== null) {
            return $this->options[$optionName] ?? null;
        }

        $this->options = \get_blog_option(1, 'supr_woocommerce_settings_reg', []);

        return $this->getOption($optionName);
    }
}
