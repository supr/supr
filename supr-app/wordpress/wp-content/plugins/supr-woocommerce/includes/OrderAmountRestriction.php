<?php

namespace SuprWoocommerce;

class OrderAmountRestriction
{
    /**
     * Bootstraps the class and hooks required actions & filters.
     */
    public static function init(): void
    {
        \add_filter('woocommerce_get_sections_shipping', [self::class, 'addSection'], 30);
        \add_action('woocommerce_get_settings_shipping', [self::class, 'renderSection'], 10, 2);

        // If order amount control enabled
        if (filter_var(\get_option('supr_woocommerce_amount_restriction_enabled'), FILTER_VALIDATE_BOOLEAN)) {
            \add_action('woocommerce_checkout_process', [self::class, 'checkOrderAmount']);
            \add_action('woocommerce_before_cart', [self::class, 'checkOrderAmount']);
        }
    }

    /**
     * Add a new section to the WooCommerce Shipment settings tabs array.
     *
     * @param array $sections Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
     * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Subscription tab.
     */
    public static function addSection(array $sections): array
    {
        $sections['order_amount_restriction'] = __('Min & max order value', 'supr-woocommerce');

        return $sections;
    }

    /**
     * Uses the WooCommerce admin fields API to output settings via the @see \woocommerce_admin_fields() function.
     *
     * @uses \woocommerce_admin_fields()
     * @uses self::get_settings()
     */
    public static function renderSection(array $settings, string $currentSection): array
    {
        if ($currentSection === 'order_amount_restriction') {
            return self::getSettings();
        }

        return $settings;
    }

    /**
     * Get all the settings for this tab.
     */
    public static function getSettings(): array
    {
        return [
            'title' => [
                'name' => __('Minimum and maximum order value settings', 'supr-woocommerce'),
                'type' => 'title',
                'desc' => __('On this page you can set the minimum and maximum order values, which apply to all orders of your shop. If you leave the field empty, the restriction will not be applied.', 'supr-woocommerce'),
                'id' => 'supr_woocommerce_amount_restriction_title'
            ],
            'enabled' => [
                'name' => __('Enabled', 'supr-woocommerce'),
                'type' => 'checkbox',
                'desc' => __('Activate min & max order value check', 'supr-woocommerce'),
                'id' => 'supr_woocommerce_amount_restriction_enabled'
            ],
            'min_amount' => [
                'name' => __('Min amount', 'supr-woocommerce'),
                'type' => 'number',
                'desc' => __('If the order value is lower than indicated, the buyer will not be able to complete the purchase. A notification will be shown that the minimum order value has not been reached.', 'supr-woocommerce'),
                'id' => 'supr_woocommerce_amount_restriction_min_amount'
            ],
            'max_amount' => [
                'name' => __('Max amount', 'supr-woocommerce'),
                'type' => 'number',
                'desc' => __('If the order value is higher than indicated, the buyer will not be able to complete the purchase. A notification will be shown that the maximum order value has been exceeded.', 'supr-woocommerce'),
                'id' => 'supr_woocommerce_amount_restriction_max_amount'
            ],
            'include_shipping_costs' => [
                'name' => __('Include shipping', 'supr-woocommerce'),
                'type' => 'checkbox',
                'desc' => __('Include shipping costs when checking the order value', 'supr-woocommerce'),
                'id' => 'supr_woocommerce_amount_restriction_include_shipping_costs'
            ],
            'section_end' => [
                'type' => 'sectionend',
                'id' => 'wc_settings_tab_order_amount_restriction_section_end'
            ]
        ];
    }

    /**
     * Add notice if order amount is wrong
     */
    public static function checkOrderAmount(): void
    {
        $minOrderAmount = (float)\get_option('supr_woocommerce_amount_restriction_min_amount', 0);
        $maxOrderAmount = (float)\get_option('supr_woocommerce_amount_restriction_max_amount', 0);

        // If empty or 0
        if ($minOrderAmount + $maxOrderAmount <= 0) {
            return;
        }

        // Get total costs
        $orderAmount = self::getOrderTotalCosts();

        // Create error
        $errorMessage = null;

        if ($minOrderAmount > 0 && $orderAmount < $minOrderAmount) {
            $errorMessage = sprintf('Your current order total is %s — you must have an order with a minimum of %s to place your order.', \wc_price($orderAmount), \wc_price($minOrderAmount));
        }

        if ($maxOrderAmount > 0 && $orderAmount > $maxOrderAmount) {
            $errorMessage = sprintf('Your current order total is %s — you must have an order with a maximum of %s to place your order.', \wc_price($orderAmount), \wc_price($maxOrderAmount));
        }

        if ($errorMessage === null) {
            return;
        }

        // Show error
        if (\is_cart()) {
            \wc_print_notice($errorMessage, 'error');
        } else {
            \wc_add_notice($errorMessage, 'error');
        }
    }

    /**
     * Get total costs of order (with or without shipping costs)
     *
     * @return float
     */
    private static function getOrderTotalCosts(): float
    {
        $includeShippingCosts = filter_var(\get_option('supr_woocommerce_amount_restriction_include_shipping_costs'), FILTER_VALIDATE_BOOLEAN);

        if ($includeShippingCosts) {
            $orderAmount = (float)\WC()->cart->get_total(null);
        } else {
            // Important: in WC shipping_total doesn't include shipping tax.
            $orderAmount = (float)\WC()->cart->get_total(null) - (float)\WC()->cart->get_shipping_total() - (float)\WC()->cart->get_shipping_tax();
        }

        return $orderAmount;
    }
}
