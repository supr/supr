<?php

namespace SuprWoocommerce;

/**
 * Class ShortCode
 *
 * @package SuprWoocommerce
 */
class ShortCode
{
    /**
     * Add shortcode.
     * @deprecated see: /wp-content/mu-plugins/supr-third-plugins-hacking/includes/Elementor/Widgets/ProductSingleLegalInfo.php
     */
    public static function registerSuprWoocommerceGermanizedShortCode(): void
    {
        // We need it because there is not any Element in Elementor
        \add_shortcode('supr_woocommerce_germanized_product_legal_info', [self::class, 'getProductLegalInfo']);
    }

    /*
     * Get legal info for current product
     */
    public static function getProductLegalInfo(): string
    {
        // If needed functions don't exist
        if (!function_exists('\woocommerce_gzd_template_single_legal_info') || !function_exists('\is_product')) {
            return '';
        }

        // If it is not single product view
        if (!\is_product()) {
            return '';
        }

        // Render legal info and return
        ob_start();

        \woocommerce_gzd_template_single_legal_info();

        return ob_get_clean();
    }
}
