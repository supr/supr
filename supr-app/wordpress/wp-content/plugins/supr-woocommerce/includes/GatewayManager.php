<?php

namespace SuprWoocommerce;

use SuprWoocommerce\PaymentGateway\OnPickup;

/**
 * Class GatewayManager
 *
 * @package SuprWoocommerce
 */
class GatewayManager
{
    /**
     * Add payment to the list
     *
     * @param $gateways
     * @return array
     */
    public static function addPaymentOnPickUp($gateways): array
    {
        $gateways[] = '\SuprWoocommerce\PaymentGateway\OnPickup';

        return $gateways;
    }
}
