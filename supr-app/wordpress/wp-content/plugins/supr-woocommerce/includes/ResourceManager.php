<?php

namespace SuprWoocommerce;

/**
 * Class ResourceManager
 *
 * @package SuprWoocommerce
 */
class ResourceManager
{
    /**
     * Register files for admin
     */
    public static function addAdminResources(): void
    {
        // JavaScript
        \wp_enqueue_script('supr_woocommerce_admin_script', SUPR_WOOCOMMERCE_URL . 'js/woocommerce-admin.js', ['jquery'], SUPR_WOOCOMMERCE_VERSION, true);
    }

    /**
     * Add css and js for checkout
     */
    public static function addWooCommerceResources(): void
    {

        \wp_enqueue_script('supr_woocommerce_frontend_script', SUPR_WOOCOMMERCE_URL . 'js/woocommerce-theme.js', ['jquery'], SUPR_WOOCOMMERCE_VERSION, true);

        $translation_array = [
            'checkout_createaccount_additional_text' => sprintf(
                __(
                    'For the creation of a user account, I accept <a href="%s" target="_blank">the privacy statements of SUPR Network</a> as operator of the e-commerce platform SUPR.',
                    'supr-woocommerce'
                ),
                'https://supr.network/rechtliche-seiten/datenschutz/'
            )
        ];

        \wp_localize_script('supr_woocommerce_frontend_script', 'supr_woocommerce', $translation_array);
    }
}
