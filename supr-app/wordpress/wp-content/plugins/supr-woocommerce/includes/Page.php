<?php

namespace SuprWoocommerce;

/**
 * Class Page
 *
 * @package SuprCronSettings
 */
class Page
{
    /**
     * Show setting page
     */
    public static function networkSettingsPage(): void
    {
        include SUPR_WOOCOMMERCE_PATH . 'templates/network-settings-page.php';
    }

    /**
     * Show delete account page
     */
    public static function deleteAccountPage(): void
    {
        include SUPR_WOOCOMMERCE_PATH . 'templates/delete-account-page.php';
    }
}
