<?php

namespace SuprWoocommerce\PaymentGateway;

use WC_Order;
use WC_Payment_Gateway;

/**
 * Class OnPickup
 *
 * @package SuprWoocommerce\PaymentGateway
 */
class OnPickup extends WC_Payment_Gateway
{
    public $instructions;

    /**
     * Constructor for the gateway.
     */
    public function __construct()
    {
        $this->id = 'onpickup';
        $this->icon = \apply_filters('woocommerce_onpickup_icon', '');
        $this->has_fields = false;
        $this->method_title = __('Payment on pickup', 'supr-woocommerce');
        $this->method_description = __('Take payments yourself on pickup of product.', 'supr-woocommerce');

        // Define user set variables.
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');
        $this->instructions = $this->get_option('instructions');

        // Load the settings.
        $this->init_form_fields();
        $this->init_settings();

        // Actions.
        \add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
        \add_action('woocommerce_thankyou_onpickup', [$this, 'thankyou_page']);

        // Customer Emails.
        \add_action('woocommerce_email_before_order_table', [$this, 'email_instructions'], 10, 3);
    }

    /**
     * Initialise Gateway Settings Form Fields.
     */
    public function init_form_fields(): void
    {
        $this->form_fields = [
            'enabled' => [
                'title' => __('Enable/Disable', 'supr-woocommerce'),
                'type' => 'checkbox',
                'label' => __('Enable check payments', 'supr-woocommerce'),
                'default' => 'no',
            ],
            'title' => [
                'title' => __('Title', 'supr-woocommerce'),
                'type' => 'text',
                'description' => __('This controls the title which the user sees during checkout.', 'supr-woocommerce'),
                'default' => __('Payment at pickup', 'supr-woocommerce'),
                'desc_tip' => true,
            ],
            'description' => [
                'title' => __('Description', 'supr-woocommerce'),
                'type' => 'textarea',
                'description' => __('Payment method description that the customer will see on your checkout.', 'supr-woocommerce'),
                'default' => __('Please come in our shop and pay for your order.', 'supr-woocommerce'),
                'desc_tip' => true,
            ],
            'instructions' => [
                'title' => __('Instructions', 'supr-woocommerce'),
                'type' => 'textarea',
                'description' => __('Instructions that will be added to the thank you page and emails.', 'supr-woocommerce'),
                'default' => '',
                'desc_tip' => true,
            ]
        ];
    }

    /**
     * Output for the order received page.
     */
    public function thankyou_page(): void
    {
        if ($this->instructions) {
            echo \wp_kses_post(wpautop(wptexturize($this->instructions)));
        }
    }

    /**
     * Add content to the WC emails.
     *
     * @access public
     * @param WC_Order $order         Order object.
     * @param bool     $sent_to_admin Sent to admin.
     * @param bool     $plain_text    Email format: plain text or HTML.
     */
    public function email_instructions($order, $sent_to_admin, $plain_text = false): void
    {
        if ($this->instructions && !$sent_to_admin && 'onpickup' === $order->get_payment_method() && $order->has_status('on-hold')) {
            echo \wp_kses_post(wpautop(wptexturize($this->instructions)) . PHP_EOL);
        }
    }

    /**
     * Process the payment and return the result.
     *
     * @param int $order_id Order ID.
     * @return array
     */
    public function process_payment($order_id): array
    {
        $order = wc_get_order($order_id);

        if ($order->get_total() > 0) {
            // Mark as on-hold (we're awaiting the pickup).
            $order->update_status(apply_filters('woocommerce_onpickup_process_payment_order_status', 'on-hold', $order), _x('Awaiting check payment', 'Check payment method', 'supr-woocommerce'));
        } else {
            $order->payment_complete();
        }

        // Remove cart.
        WC()->cart->empty_cart();

        // Return thankyou redirect.
        return [
            'result' => 'success',
            'redirect' => $this->get_return_url($order),
        ];
    }
}
