<?php

namespace SuprWoocommerce;

class Country
{
    /**
     * Disable sorting countries list
     *
     * @param $bool
     * @return bool
     */
    public static function disableCountrySorting($bool): bool
    {
        return false;
    }
}
