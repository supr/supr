<?php

namespace SuprWoocommerce;

class CustomerAccount
{
    /**
     * Add filters and actions for custom menu item in WooCommerce Account.
     */
    public static function addMenuItemDeleteAccount(): void
    {
        \add_filter('woocommerce_account_menu_items', [self::class, 'filterMenuItemDeleteAccount'], 40);
        \add_rewrite_endpoint('customer-delete-account', EP_ROOT | EP_PAGES);
        \add_action('woocommerce_account_customer-delete-account_endpoint', [Page::class, 'deleteAccountPage']);
    }

    /**
     * Add item to menu for customer
     *
     * @param array $menuLinks
     * @return array
     */
    public static function filterMenuItemDeleteAccount($menuLinks): array
    {
        $menuLinks['customer-delete-account'] = __('Delete account', 'supr-woocommerce');

        return $menuLinks;
    }
}
