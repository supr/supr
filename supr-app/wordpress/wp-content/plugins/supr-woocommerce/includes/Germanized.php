<?php

namespace SuprWoocommerce;

/**
 * Class Germanized
 *
 * @package SuprWoocommerce
 */
class Germanized
{
    /**
     * Delete pro tabs and not simple features
     *
     * @param array $tabs
     * @return array
     */
    public static function deleteSettingsTabs(array $tabs): array
    {
        $tabsToDisable = ['contract', 'invoices', 'multistep_checkout', 'terms_generator', 'revocation_generator', 'dhl', 'trusted_shops', 'shipments'];

        foreach ($tabs as $slug => $name) {
            if (in_array($slug, $tabsToDisable, true)) {
                unset($tabs[$slug]);
            }
        }
        return $tabs;
    }
}
