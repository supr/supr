<?php

namespace SuprWoocommerce;

class TextDomain
{
    public static $domainName = 'supr-woocommerce';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_WOOCOMMERCE_DIR_NAME . '/languages/');
    }
}
