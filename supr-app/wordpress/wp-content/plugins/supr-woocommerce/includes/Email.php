<?php

namespace SuprWoocommerce;

/**
 * Class Email
 *
 * @package SuprWoocommerce
 */
class Email
{
    /**
     * Add actions to replace emails
     */
    public static function replaceWooCommerceRegisterUserEmail(): void
    {
        \add_action('woocommerce_email', [self::class, 'disableWooCommerceCreatedCustomerNotification'], 11, 1);
        \add_action('woocommerce_email', [self::class, 'enableCampaignMonitorCreatedCustomerNotification'], 12, 1);
    }

    /**
     * Disable all emails
     *
     * @param $mailer
     */
    public static function disableWooCommerceCreatedCustomerNotification($mailer): void
    {
        if (\class_exists('\WC_GZD_Customer_Helper') && \class_exists('\SuprCampaignMonitor\Manager')) {
            // Remove own WC mail
            \remove_action('woocommerce_created_customer_notification', [$mailer, 'customer_new_account'], 10);

            // Remove Germanized mail
            $wcGzdCustomerHelper = \WC_GZD_Customer_Helper::instance();
            \remove_action('woocommerce_created_customer_notification', [$wcGzdCustomerHelper, 'customer_new_account_activation'], 9);
        }
    }

    /**
     * Enable our email
     *
     * @param $mailer
     */
    public static function enableCampaignMonitorCreatedCustomerNotification($mailer)
    {
        \add_action('woocommerce_created_customer_notification', [self::class, 'sendCampaignMonitorCreatedCustomerNotification'], 10, 3);
    }

    /**
     * Send email with CM
     *
     * @param       $customerId
     * @param array $newCustomerData
     * @param bool  $passwordGenerated
     */
    public static function sendCampaignMonitorCreatedCustomerNotification($customerId, array $newCustomerData = [], $passwordGenerated = false): void
    {
        if (!$customerId || !\class_exists('\WC_GZD_Customer_Helper') || !\class_exists('\SuprCampaignMonitor\Manager')) {
            return;
        }

        if (empty(Options::instance()->getOption('cm_id'))) {
            error_log('[Supr Woocommerce] We can not send email about registration of user. There is not Campaign Monitor transaction ID.');

            return;
        }

        $user = new \WP_User($customerId);
        $wcGzdCustomerHelper = \WC_GZD_Customer_Helper::instance();

        $userPass = !empty($newCustomerData['user_pass']) ? $newCustomerData['user_pass'] : '';
        $userActivation = $wcGzdCustomerHelper->get_customer_activation_meta($customerId);
        $userActivationUrl = $wcGzdCustomerHelper->get_customer_activation_url($userActivation);

        $data = [
            'user_pass' => $userPass,
            'user_activation_url' => $userActivationUrl,
            'user_email' => $user->user_email,
            'shop_name' => \get_bloginfo('name'),
            'shop_url' => \get_bloginfo('url')
        ];

        // Disable tracking
        \SuprCampaignMonitor\Manager::instance()->disableUserTracking();

        $success = \SuprCampaignMonitor\Manager::sendTransactionMail(
            Options::instance()->getOption('cm_id'),
            $data,
            $user->user_email
        );

        if (!$success) {
            error_log('[Supr Woocommerce] We can not send email about registration of user ' . $user->user_email . '.');
        }
    }
}
