<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

if (isset($_GET['delete_account']) && $_GET['delete_account'] === 'yes') {
    global $wpdb;
    // Add needed functions
    require_once(ABSPATH . 'wp-admin/includes/user.php');
    // Get current user id
    $userId = get_current_user_id();
    // Delete the user's from blog
    \wp_delete_user($userId);
    // Delete metadata of user
    $meta = $wpdb->get_col($wpdb->prepare("SELECT umeta_id FROM $wpdb->usermeta WHERE user_id = %d", $userId));
    foreach ($meta as $mid) {
        delete_metadata_by_mid('user', $mid);
    }
    // Delete user from DB
    $wpdb->delete($wpdb->users, array('ID' => $userId));
    // Redirect to main page
    \wp_redirect('/');
}

echo sprintf(__('<p>In order to delete your account click this link: %s.</p>', 'supr-woocommerce'), '<a href="?delete_account=yes">' . __('Delete my account and all my personal data', 'supr-woocommerce') . '</a>');
