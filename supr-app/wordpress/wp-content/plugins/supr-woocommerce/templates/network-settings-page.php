<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$tabs = [
    'main' => __('Main', 'supr-cron-settings')
];

$currentTab = (isset($_GET['tab'], $tabs[$_GET['tab']])) ? $_GET['tab'] : 'main';

?>

<div class="wrap supr-cron-settings">
    <h2><?= get_admin_page_title(); ?></h2>
    <h2 class="nav-tab-wrapper">
        <?php foreach ($tabs as $tab => $tabName) : ?>
            <a class="nav-tab<?= $currentTab === $tab ? ' nav-tab-active' : ''; ?>" href="?page=supr_woocommerce_settings&tab=<?= $tab; ?>"><?= $tabName; ?></a>
        <?php endforeach; ?>
    </h2>

    <?php
    // Include tabs template
    include SUPR_WOOCOMMERCE_PATH . "templates/settings-tabs/{$currentTab}.php";
    ?>
</div>