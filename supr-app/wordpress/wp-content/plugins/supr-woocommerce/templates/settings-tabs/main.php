<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/*
 * Info: dosc means DeleteOldShopsCron
 */

// Check dependencies
if (!\is_plugin_active_for_network('supr-campaign-monitor/supr-campaign-monitor.php')) : ?>
    <div class="notice notice-warning is-dismissible">
        <p><?= __('In order to use Campaign Monitor templates, you require <b>SUPR - Campaign Monitor</b> plugin.', 'supr-woocommerce'); ?></p>
    </div>
<?php endif;

// Handle request with options
if (isset($_POST['supr_woocommerce_settings_reg']) && is_array($_POST['supr_woocommerce_settings_reg'])) {
    $options = $_POST['supr_woocommerce_settings_reg'];

    // Trim all values
    $options = array_map('trim', $options);
    // Remove empty values
    $options = array_filter($options);
    // Sanitize values with WP special function
    $options = array_map('sanitize_text_field', $options);

    \update_option('supr_woocommerce_settings_reg', $options);
    ?>
    <div class="notice notice-success is-dismissible">
        <p><?= __('Options were saved.', 'supr-woocommerce'); ?></p>
    </div>
    <?php
}

$options = \get_option('supr_woocommerce_settings_reg', []);
?>

<form action="" method="POST">
    <table class="form-table">
        <tr>
            <th colspan="2"><?= __('Main', 'supr-woocommerce'); ?></th>
        </tr>
        <tr>
            <th colspan="2"><?= __('Campaign Monitor transaction IDS', 'supr-woocommerce'); ?></th>
        </tr>
        <tr>
            <td><label for="supr_woocommerce_settings_reg_cm_id"><?= __('Your login data mail', 'supr-woocommerce'); ?></label></td>
            <td><input type="text" id="supr_woocommerce_settings_reg_cm_id" name="supr_woocommerce_settings_reg[cm_id]" value="<?= $options['cm_id'] ?? '' ?>"/></td>
            <td><?= __('This is the mail that the customer (buyer) receives after successful registration.', 'supr-woocommerce'); ?></td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>
