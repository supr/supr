<?php
/**
 * Plugin Name: SUPR - DB Migration
 * Plugin URI: https://supr.com
 * Description: It does migrations of DB
 * Text Domain: supr-db-migration
 * Domain Path: /languages
 * Version: 1.0.0
 * Author: SUPR Development
 * License: GPL
 */

define('SUPR_DB_MIGRATION_PATH', plugin_dir_path(__FILE__));
define('SUPR_DB_MIGRATION_URL', plugins_url('/', __FILE__));
define('SUPR_DB_MIGRATION_FILE', plugin_basename(__FILE__));
define('SUPR_DB_MIGRATION_DIR_NAME', basename(__DIR__));

define('SUPR_DB_MIGRATION_VERSION', '1.0.0');

// Load main Class
require SUPR_DB_MIGRATION_PATH . 'includes/Plugin.php';
