<?php

namespace SuprDBMigration;

/**
 * Class Migration30
 *
 * Fix width of default WC gallery in the product
 *
 * @package SuprDBMigration
 */
class Migration30 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        \update_option('woo_variation_gallery_width', '100', true);
        // Clear cache
        \do_action('supr-clear-cache');

        error_log('[SUPR DB Migration] Migration 30 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
