<?php

namespace SuprDBMigration;

/**
 * Class Migration43
 *
 * Free shipping prio
 *
 * @package SuprDBMigration
 */
class Migration43 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        // Update options for germanized (erzwinge kostenlosen Versand wenn die Methode zur Verfügung steht)
        \update_option('woocommerce_gzd_display_checkout_free_shipping_select', 'yes');

        error_log('[SUPR DB Migration] Migration 43 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
