<?php

namespace SuprDBMigration;

/**
 * Class Migration1
 *
 * @package SuprDBMigration
 */
class Migration1 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        // Upgrade WP to new DB Version
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        wp_upgrade();
        upgrade_all();

        error_log('[SUPR DB Migration] Global migration 1 was executed.');

        return true;
    }
}
