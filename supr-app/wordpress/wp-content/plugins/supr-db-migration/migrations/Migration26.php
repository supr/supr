<?php

namespace SuprDBMigration;

/**
 * Class Migration26
 *
 * Delete wp ultimo cron from sub blogs
 *
 * @package SuprDBMigration
 */
class Migration26 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        $option = \get_option('cron', []);

        foreach ($option as $time => $crons) {
            if (isset($crons['wub_cron_20'])) {
                unset($option[$time]);
            }
        }

        \update_option('cron', $option);

        error_log('[SUPR DB Migration] Migration 26 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
