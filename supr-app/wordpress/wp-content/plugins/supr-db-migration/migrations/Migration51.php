<?php

namespace SuprDBMigration;

/**
 * Class Migration51
 *
 * Elementor: Font Awesome 4 upgrade to version 5.
 *
 * @package SuprDBMigration
 */
class Migration51 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        // Delete option, that says to use old version of the font
        \delete_option('elementor_icon_manager_needs_update');

        error_log('[SUPR DB Migration] Migration 51 for blog #' . \get_current_blog_id() . ' was executed.');

        return true;
    }
}
