<?php

namespace SuprDBMigration;

/**
 * Class Migration34
 *
 * Fix last ssl certificate update for subdomains
 *
 * @package SuprDBMigration
 */
class Migration34 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        // Check, that we know main domain
        if (!defined('DOMAIN_CURRENT_SITE') || empty(DOMAIN_CURRENT_SITE)) {
            error_log('[SUPR DB Migration] Migration 34 cannot be executed, because constant DOMAIN_CURRENT_SITE is not defined.');

            return false;
        }

        $domainCurrentSite = DOMAIN_CURRENT_SITE;

        $queryResult = $wpdb->query('UPDATE `' . $wpdb->base_prefix . "supr_own_domain` SET `ssl_generated` = null WHERE `domain` LIKE '%.{$domainCurrentSite}';");

        if ($queryResult === false) {
            error_log('[SUPR DB Migration] Migration 34 cannot be executed. Error: ' . $wpdb->last_error);

            return false;
        }

        error_log('[SUPR DB Migration] Migration 34 was executed (' . $queryResult . ' rows were updated).');

        return true;
    }
}
