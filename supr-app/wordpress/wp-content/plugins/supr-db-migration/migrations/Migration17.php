<?php

namespace SuprDBMigration;

/**
 * Class Migration17
 *
 * @package SuprDBMigration
 */
class Migration17 extends Migration
{
    /**
     * Show categories in Menu-Items for all users
     *
     * @return bool
     */
    public function execute(): bool
    {
        global $wpdb;

        $usersMeta = $wpdb->get_results("SELECT * FROM `usermeta` WHERE `meta_key` = 'metaboxhidden_nav-menus'");

        foreach ($usersMeta as $userMeta) {
            $value = \unserialize($userMeta->meta_value, ['allowed_classes' => false]);

            if (\is_array($value) && ($key = array_search('add-product_cat', $value, true)) !== false) {
                unset($value[$key]);

                $value = \serialize($value);
                $wpdb->query("UPDATE `usermeta` SET `meta_value` = '{$value}' WHERE `umeta_id` = {$userMeta->umeta_id};");
            }
        }

        error_log('[SUPR DB Migration] Migration 17 was executed.');

        return true;
    }
}
