<?php

namespace SuprDBMigration;

/**
 * Class Migration36
 *
 * Delete WooCommerce logs from storage
 *
 * @package SuprDBMigration
 */
class Migration36 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        $blogId = \get_current_blog_id();
        $logsDir = ABSPATH . 'wp-content/uploads/ ' . ($blogId > 1 ? "sites/{$blogId}/" : '') . ' wc-logs/';

        if (\function_exists('shell_exec') && (int)\shell_exec('echo 1') === 1) {
            \shell_exec("find {$logsDir} -name \"*.log\" -type f -delete");
        }

        error_log('[SUPR DB Migration] Migration 36 was executed.');

        return true;
    }
}
