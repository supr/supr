<?php

namespace SuprDBMigration;

/**
 * Class Migration35
 *
 * Inside the SUPR Checkout a variable for environment is required. This will be set initially by domain.
 *
 * @package SuprDBMigration
 */
class Migration35 extends Migration
{
    private const SUPR_COMMERCE_SUITE_INDICATIVE_DEVELOPMENT = 'aa31114d-ab22-4fff-abd3-537c150ca610';
    private const SUPR_COMMERCE_SUITE_INDICATIVE_PRODUCTION = '8b8eeaf1-e441-42a7-8e04-ac89b4493732';

    /**
     * @return bool
     */
    public function execute(): bool
    {
        if (!\class_exists('\SuprCommerceSuite\CheckoutOption')) {
            return true;
        }

        $option = \SuprCommerceSuite\CheckoutOption::getOption();
        $isProduction = \in_array(\get_network()->domain, ['mysupr.de', 'mysupr.at', 'myvikingshop.de']);
        $option['env'] = true === $isProduction ? 'production' : 'development';
        $option['indicative_tracking_key'] = true === $isProduction ? self::SUPR_COMMERCE_SUITE_INDICATIVE_PRODUCTION : self::SUPR_COMMERCE_SUITE_INDICATIVE_DEVELOPMENT;
        \update_option(\SuprCommerceSuite\CheckoutOption::SUPR_COMMERCE_SUITE_OPTION, $option);
        error_log('[SUPR DB Migration] Migration 35 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
