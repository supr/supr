<?php

namespace SuprDBMigration;

/**
 * Class Migration50
 *
 * @package SuprDBMigration
 */
class Migration50 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Cannot execute migration 50 for blog #' . get_current_blog_id() . ': ' . $wpdb->last_error);

        return false;
    }
}
