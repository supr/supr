<?php

namespace SuprDBMigration;

/**
 * Class Migration19
 *
 * Disable minify of js files
 *
 * @package SuprDBMigration
 */
class Migration20 extends Migration
{
    /**
     * Disable minify of js files. But not processing!
     *
     * @return bool
     */
    public function execute(): bool
    {
        // set not process google fonts
        \update_option('fastvelocity_min_disable_js_minification', 1);

        error_log('[SUPR DB Migration] Migration 20 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
