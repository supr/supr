<?php

namespace SuprDBMigration;

/**
 * Class Migration39
 *
 * Set default location for WooCommerce
 *
 * @package SuprDBMigration
 */
class Migration39 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        // Set default location to geolocation
        if (empty(\get_option('woocommerce_default_customer_address'))) {
            \update_option('woocommerce_default_customer_address', 'geolocation');
        }

        error_log('[SUPR DB Migration] Migration 39 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
