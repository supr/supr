<?php

namespace SuprDBMigration;

/**
 * Class Migration2
 *
 * @package SuprDBMigration
 */
class Migration2 extends Migration
{
    /**
     * Update text for registration
     *
     * @return bool
     */
    public function execute(): bool
    {
        update_blog_option(1, 'supr_signup_domain_headline', 'Gib deinen Shopnamen ein');
        update_blog_option(1, 'supr_signup_domain_description', 'Beginn die kostenlose Registrierung und wähle einen Namen für deinen neuen Onlineshop.');
        update_blog_option(1, 'supr_signup_account_headline', 'Gib deine Kontaktdaten ein');
        update_blog_option(1, 'supr_signup_account_description', 'Mit diesen Informationen richten wir deinen Onlineshop ein und legen dir automatisch ein Impressum an.');
        update_blog_option(1, 'supr_signup_merchant-info_headline', 'Benutzerkonto anlegen');
        update_blog_option(1, 'supr_signup_merchant-info_description', 'Um dein Benutzerkonto anzulegen gib bitte deine E-Mail Adresse ein und lege ein sicheres Passwort fest.');
        update_blog_option(1, 'supr_signup_plan_headline', 'Wähle deinen Tarif');
        update_blog_option(1, 'supr_signup_plan_description', 'Du kannst jetzt kostenlos starten oder direkt alle Funktionen und Designs mit dem beliebten SMART Tarif nutzen. Natürlich kannst du jederzeit einen Tarif auswählen.');

        error_log('[SUPR DB Migration] Global migration 2 was executed.');

        return true;
    }
}
