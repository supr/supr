<?php

namespace SuprDBMigration;

/**
 * Class Migration18
 *
 * This is a copy of Migration 10.
 *
 * @package SuprDBMigration
 */
class Migration18 extends Migration
{
    /**
     * Replace http with https after disable of Simple SSL Plugin
     *
     * @return bool
     */
    public function execute(): bool
    {
        global $wpdb;

        // Replace http with https
        $wpdb->query('UPDATE ' . $wpdb->get_blog_prefix() . 'options SET option_value = REPLACE(option_value,\'http:\',\'https:\') WHERE option_name LIKE \'home\' OR option_name LIKE \'siteurl\'');

        error_log('[SUPR DB Migration] Migration 18 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
