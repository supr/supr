<?php

namespace SuprDBMigration;

/**
 * Class Migration6
 *
 * @package SuprDBMigration
 */
class Migration6 extends Migration
{
    /**
     * Delete duplicate of tax rate
     *
     * @return bool
     */
    public function execute(): bool
    {
        global $wpdb;

        $wpdb->delete($wpdb->get_blog_prefix() . 'woocommerce_tax_rates', ['tax_rate_id' => 119]);

        error_log('[SUPR DB Migration] Migration 6 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
