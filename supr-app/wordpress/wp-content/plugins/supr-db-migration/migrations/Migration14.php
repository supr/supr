<?php

namespace SuprDBMigration;

/**
 * Class Migration14
 *
 * @package SuprDBMigration
 */
class Migration14 extends Migration
{
    /**
     * Set space limit for wp-ultimo for all plans
     *
     * @return bool
     */
    public function execute(): bool
    {
        global $wpdb;

        $quotas = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . "`postmeta` WHERE `meta_key` = 'wpu_quotas'");

        foreach ($quotas as $quota) {
            $value = \unserialize($quota->meta_value, ['allowed_classes' => false]);

            $value['upload'] = 2000;

            $value = \serialize($value);

            $wpdb->query('UPDATE ' . $wpdb->get_blog_prefix() . "`postmeta` SET `meta_value` = '{$value}' WHERE `meta_id` = {$quota->meta_id};");
        }

        error_log('[SUPR DB Migration] Migration 14 was executed.');

        return true;
    }
}
