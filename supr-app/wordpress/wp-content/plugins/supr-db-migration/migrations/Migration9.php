<?php

namespace SuprDBMigration;

/**
 * Class Migration9
 *
 * @package SuprDBMigration
 */
class Migration9 extends Migration
{
    /**
     * Rename tax rates
     *
     * @return bool
     */
    public function execute(): bool
    {
        global $wpdb;

        // Delete english version
        $wpdb->delete($wpdb->get_blog_prefix() . 'woocommerce_tax_rates', ['tax_rate_class' => 'reduced-rate']);

        // Rename normal tax rates
        $wpdb->query('UPDATE `' . $wpdb->get_blog_prefix() . 'woocommerce_tax_rates` SET tax_rate_name = CONCAT(tax_rate_name, \' \', CAST(tax_rate  AS DECIMAL(10,0)), \'%\')');

        error_log('[SUPR DB Migration] Migration 9 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
