<?php

namespace SuprDBMigration;

/**
 * Class Migration5
 *
 * @package SuprDBMigration
 */
class Migration5 extends Migration
{
    /**
     * Change old credentials to new
     *
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Migration 5 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
