<?php

namespace SuprDBMigration;

/**
 * Class Migration4
 *
 * @package SuprDBMigration
 */
class Migration4 extends Migration
{
    /**
     * Disable GOOGLE API MAPS
     *
     * @return bool
     */
    public function execute(): bool
    {
        $options = get_option('pa_maps_save_settings');
        if (\is_array($options) && isset($options['premium-map-disable-api'])) {
            $options['premium-map-disable-api'] = 0;
            update_option('pa_maps_save_settings', $options);
        }

        error_log('[SUPR DB Migration] Migration 4 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
