<?php

namespace SuprDBMigration;

/**
 * Class Migration31
 *
 * Delete wp ultimo logs from storage (additional to migration #28)
 *
 * @package SuprDBMigration
 */
class Migration31 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        $logsDir = ABSPATH . 'wp-content/uploads/wu-logs/';

        $filesToClear = [
            'billwerk-webhooks.log',
        ];

        if (\function_exists('shell_exec') && (int)\shell_exec('echo 1') === 1) {
            foreach ($filesToClear as $file) {
                // Sync dirs with many sub-processes
                $filePath = $logsDir . $file;
                \shell_exec("echo '' > {$filePath}");
            }
        }

        error_log('[SUPR DB Migration] Migration 31 was executed.');

        return true;
    }
}
