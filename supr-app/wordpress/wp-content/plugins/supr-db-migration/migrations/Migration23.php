<?php

namespace SuprDBMigration;

/**
 * Class Migration23
 *
 * @package SuprDBMigration
 */
class Migration23 extends Migration
{
    /**
     * Clear cache
     *
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Migration 23 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
