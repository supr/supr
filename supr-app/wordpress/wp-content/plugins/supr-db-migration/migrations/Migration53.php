<?php

namespace SuprDBMigration;

/**
 * Class Migration52
 *
 * SUPR BRANDING BAGE: Clean up DB after plugin was deleted
 *
 * @package SuprDBMigration
 */
class Migration53 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        global $wpdb;

        // Remove old options
        $optionsTableName = $wpdb->base_prefix . 'options';
        $wpdb->query("DELETE FROM `{$optionsTableName}` WHERE `option_name` LIKE 'supr_branding_badge%'");

        error_log('[SUPR DB Migration] Migration 53 was executed.');

        return true;
    }
}
