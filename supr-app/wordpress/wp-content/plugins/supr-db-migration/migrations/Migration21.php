<?php

namespace SuprDBMigration;

/**
 * Class Migration19
 *
 * Clear cache for all shops (after enable of W3 TC)
 *
 * @package SuprDBMigration
 */
class Migration21 extends Migration
{
    /**
     * Clear cache
     *
     * @return bool
     */
    public function execute(): bool
    {
        \do_action('supr-clear-cache');

        error_log('[SUPR DB Migration] Migration 21 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
