<?php

namespace SuprDBMigration;

/**
 * Class Migration54
 *
 * @package SuprDBMigration
 */
class Migration54 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Cannot execute migration 54 for blog #' . get_current_blog_id() . ': ' . $wpdb->last_error);

        return false;
    }
}
