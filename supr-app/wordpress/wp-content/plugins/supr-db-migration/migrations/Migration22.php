<?php

namespace SuprDBMigration;

/**
 * Class Migration22
 *
 * Delete old options (now used anymore)
 *
 * @package SuprDBMigration
 */
class Migration22 extends Migration
{
    /**
     * Clear cache
     *
     * @return bool
     */
    public function execute(): bool
    {
        \delete_blog_option(1, 'supr_dashboard_countdown_code');
        \delete_blog_option(1, 'supr_dashboard_countdown_discount');
        \delete_blog_option(1, 'supr_dashboard_countdown_days');
        \delete_blog_option(1, 'supr_dashboard_countdown_content');
        \delete_blog_option(1, 'supr_dashboard_countdown_title');

        error_log('[SUPR DB Migration] Migration 22 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
