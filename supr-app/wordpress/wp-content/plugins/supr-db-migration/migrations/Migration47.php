<?php

namespace SuprDBMigration;

/**
 * Class Migration47
 *
 * Switch to new theme (hello elementor)
 *
 * @package SuprDBMigration
 */
class Migration47 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        // We change only Storefront-Supr Theme to Hello-Elementor Theme (but not PRO-Themes)
        if (\wp_get_theme()->get_stylesheet() !== 'storefront-supr') {
            return true;
        }

        //Get current logo
        $siteLogoId = \get_theme_mod('custom_logo', '');
        // Switch to new theme
        \switch_theme('hello-elementor-supr');
        // Set logo back
        \set_theme_mod('custom_logo', $siteLogoId);
        // Clear cache
        \do_action('supr-clear-cache');

        error_log('[SUPR DB Migration] Migration 47 for blog #' . \get_current_blog_id() . ' was executed.');

        return true;
    }
}
