<?php

namespace SuprDBMigration;

/**
 * Class Migration29
 *
 * Set up ignore list for FastVelocityMinify
 *
 * @package SuprDBMigration
 */
class Migration29 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        $ignoreList = \get_option('fastvelocity_min_ignorelist', '');

        $obligateLinks = [
            '/Avada/assets/js/main.min.js',
            '/woocommerce-product-search/js/product-search.js',
            '/includes/builder/scripts/frontend-builder-scripts.js',
            '/assets/js/jquery.themepunch.tools.min.js',
            '/js/TweenMax.min.js',
            '/jupiter/assets/js/min/full-scripts',
            '/wp-content/themes/Divi/core/admin/js/react-dom.production.min.js',
            '/LayerSlider/static/layerslider/js/greensock.js',
            '/themes/kalium/assets/js/main.min.js',
            '/elementor/assets/js/common.min.js',
            '/elementor/assets/js/frontend.min.js',
            '/elementor-pro/assets/js/frontend.min.js',
            '/Divi/core/admin/js/react-dom.production.min.js',
            '/kalium/assets/js/main.min.js',
            '/js/jquery/jquery-migrate.min.js',
            '/js/jquery/jquery-migrate.js',
            '/js/jquery/jquery.min.js',
            '/js/jquery/jquery.js'
        ];
        // Get as array
        $ignoreList = explode(PHP_EOL, $ignoreList);
        // Merge
        $ignoreList = array_merge($obligateLinks, $ignoreList);
        // Remove empty values
        $ignoreList = array_filter($ignoreList);

        // Save back
        \update_option('fastvelocity_min_ignorelist', implode(PHP_EOL, $ignoreList));

        error_log('[SUPR DB Migration] Migration 29 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
