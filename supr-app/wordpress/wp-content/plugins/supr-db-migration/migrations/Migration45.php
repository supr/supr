<?php

namespace SuprDBMigration;

/**
 * Class Migration45
 *
 * Delete folders related to old plugins
 *
 * @package SuprDBMigration
 */
class Migration45 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        $blogId = get_current_blog_id();

        $cssDirAdminTheme = ABSPATH . 'wp-content/uploads/' . ($blogId > 1 ? "sites/{$blogId}/" : '') . 'wp-less-cache';
        $cssDirHummingbird = ABSPATH . 'wp-content/uploads/' . ($blogId > 1 ? "sites/{$blogId}/" : '') . 'hummingbird-assets';
        $ownDirWpDefender = ABSPATH . 'wp-content/uploads/' . ($blogId > 1 ? "sites/{$blogId}/" : '') . 'wp-defender';

        $this->rrmdir($cssDirAdminTheme);
        $this->rrmdir($cssDirHummingbird);
        $this->rrmdir($ownDirWpDefender);

        error_log('[SUPR DB Migration] Migration 45 for blog #' . \get_current_blog_id() . ' was executed.');

        return true;
    }

    /**
     * Recursve remove dir with all files
     *
     * @param $dir
     * @return bool
     */
    private function rrmdir($dir): bool
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object !== '.' && $object !== '..') {
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . '/' . $object)) {
                        $this->rrmdir($dir . DIRECTORY_SEPARATOR . $object);
                    } else {
                        unlink($dir . DIRECTORY_SEPARATOR . $object);
                    }
                }
            }
            return rmdir($dir);
        }

        return false;
    }
}
