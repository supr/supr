<?php

namespace SuprDBMigration;

/**
 * Class Migration32
 *
 * Back to old theme :(
 *
 * @package SuprDBMigration
 */
class Migration32 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        // Switch to new theme
        \switch_theme('storefront-supr');
        // Clear cache
        \do_action('supr-clear-cache');
        // Set gallery to 100%
        if (\get_option('woo_variation_gallery_width', false)) {
            \update_option('woo_variation_gallery_width', '100', true);
        } else {
            \add_option('woo_variation_gallery_width', '100', true);
        }


        error_log('[SUPR DB Migration] Migration 32 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
