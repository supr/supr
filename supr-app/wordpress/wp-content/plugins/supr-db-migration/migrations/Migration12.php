<?php

namespace SuprDBMigration;

/**
 * Class Migration12
 *
 * @package SuprDBMigration
 */
class Migration12 extends Migration
{
    /** Add option to Premium Addons for Elementor Plugin
     *
     * @return bool
     */
    public function execute(): bool
    {
        $optionValue = \get_option('pa_maps_save_settings', false);

        if (!$optionValue || !\is_array($optionValue) || isset($optionValue['premium-map-cluster'])) {
            return true;
        }

        // Set new value
        $optionValue['premium-map-cluster'] = 0;

        update_option('pa_maps_save_settings', $optionValue);

        error_log('[SUPR DB Migration] Migration 12 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
