<?php

namespace SuprDBMigration;

/**
 * Class Migration42
 *
 * WooCommerce Germanized update
 *
 * @package SuprDBMigration
 */
class Migration42 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        // Update options for germanized (disable wizards and ads)
        \update_option('_wc_gzd_setup_wizard_redirect', false);
        \update_option('_wc_gzd_activation_redirect', false);

        // Hide notices
        \update_option('woocommerce_gzd_hide_tour', 1);
        \update_option('_wc_gzd_hide_theme_notice', 1);
        \update_option('_wc_gzd_hide_pro_notice', 1);
        \update_option('_wc_gzd_hide_review_notice', 1);
        \update_option('_wc_gzd_hide_theme_supported_notice', 'yes');
        \update_option('_wc_gzd_hide_update_notice', 'yes');

        // We are waiting for WooCommerce 4.0.1
        if (version_compare(\get_option('woocommerce_version', '4.0.1'), '4.0.1', '<')) {
            return false;
        }

        // We can execute it only in admin panel
        if (!\is_admin()) {
            return false;
        }

        // If Germanized is not installed
        if (!class_exists('\WC_GZD_Install')) {
            return true;
        }

        $_GET['do_update_woocommerce_gzd'] = true;

        // Bugfix: later happens an redirect, but we need to have a last migration version
        \update_option('supr_db_migration_db_version', 42);

        // Only this method has update logic :(
        \WC_GZD_Install::redirect();

        error_log('[SUPR DB Migration] Migration 42 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
