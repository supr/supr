<?php

namespace SuprDBMigration;

/**
 * Class Migration27
 *
 * Delete wp ultimo crons and clear wp crons
 *
 * @package SuprDBMigration
 */
class Migration27 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        // Disable scrapper
        if (\class_exists('WU_Settings') && get_current_blog_id() === 1) {
            \WU_Settings::save_setting('enable_screenshot_scraper', false);
        }

        // Disable all crons. They will be created again from plugins.
        \update_option('cron', []);

        error_log('[SUPR DB Migration] Migration 27 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
