<?php

namespace SuprDBMigration;

/**
 * Class Migration45
 *
 * Add license key for geolocation and dismiss notices
 *
 * @package SuprDBMigration
 */
class Migration46 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Migration 46 for blog #' . \get_current_blog_id() . ' was executed.');

        return true;
    }
}
