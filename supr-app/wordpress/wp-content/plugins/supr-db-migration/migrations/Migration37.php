<?php

namespace SuprDBMigration;

/**
 * Class Migration37
 *
 * Disable adding basket to PayPal
 *
 * @package SuprDBMigration
 */
class Migration37 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Migration 37 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
