<?php

namespace SuprDBMigration;

/**
 * Class Migration10
 *
 * @package SuprDBMigration
 */
class Migration10 extends Migration
{
    /**
     * Replace http with https for old shops
     *
     * @return bool
     */
    public function execute(): bool
    {
        global $wpdb;

        // Replace http with https
        $wpdb->query('UPDATE ' . $wpdb->get_blog_prefix() . 'options SET option_value = REPLACE(option_value,\'http:\',\'https:\') WHERE option_name LIKE \'home\' OR option_name LIKE \'siteurl\'');

        error_log('[SUPR DB Migration] Migration 10 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
