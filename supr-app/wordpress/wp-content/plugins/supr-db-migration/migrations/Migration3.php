<?php

namespace SuprDBMigration;

/**
 * Class Migration3
 *
 * @package SuprDBMigration
 */
class Migration3 extends Migration
{
    /**
     * Change COP credentials (test -> live)
     *
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Migration 0 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
