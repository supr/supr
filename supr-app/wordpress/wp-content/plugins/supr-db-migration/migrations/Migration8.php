<?php

namespace SuprDBMigration;

/**
 * Class Migration8
 *
 * @package SuprDBMigration
 */
class Migration8 extends Migration
{
    /**
     * Delete trash files from template-shop
     *
     * @return bool
     */
    public function execute(): bool
    {
        $templateShopId = 8;
        $wpUploadInfo = wp_upload_dir();
        $uploadsDir = str_replace(' ', "\\ ", trailingslashit($wpUploadInfo['basedir']));

        $files = $this->getFileList($uploadsDir . 'sites/' . $templateShopId . DIRECTORY_SEPARATOR);

        $whiteList = [
            '/2018/09/supr_demo_image_0004.jpg',
            '/2018/09/supr_demo_image_0002-1.jpg',
            '/2018/11/supr_demo_image_0001.jpg',
            '/2019/06/supr_demo_product_image_0015.jpg',
            '/2018/09/supr_demo_product_image_0013-1.jpg',
            '/2018/09/supr_demo_product_image_0006-1.jpg',
            '/2018/09/supr_demo_product_image_0005-1.jpg',
            '/2018/09/supr_demo_product_image_0004-1.jpg',
            '/2018/09/supr_demo_product_image_0003-1.jpg',
            '/2018/09/supr_demo_product_image_0009-1.jpg',
            '/2018/09/supr_demo_product_image_0010-1.jpg',
            '/2018/09/supr_demo_product_image_0012-1.jpg',
            '/2018/09/supr_demo_product_image_0011-1.jpg',
            '/2018/09/supr_demo_product_image_0008-1.jpg',
            '/2018/09/supr_demo_product_image_0007-1.jpg',
            '/2019/01/supr_demo_product_image_0002.jpg',
            '/2019/01/supr_demo_product_image_0001.jpg',
            '/2019/01/supr_demo_product_image_0014.jpg',
            '/2018/09/demo-logo-black.png',
            '/2018/09/cropped-demo-logo-black.png',
            '.htaccess',
            '.mmdb',
            '.html',
            '.php',
            '.css',
            '/fonts/'
        ];

        foreach ($files as $file) {
            // Delete if don't in white list
            $delete = true;
            foreach ($whiteList as $wlFileName) {
                if (strpos($file, $wlFileName) !== false) {
                    $delete = false;
                }
            }

            if ($delete) {
                if (unlink($file)) {
                    error_log('[SUPR DB Migration] Migration 8. File was deleted: ' . $file);
                } else {
                    error_log('[SUPR DB Migration] Migration 8. File was not deleted: ' . $file . '. Error: ' . print_r(error_get_last(), true));
                }
            } else {
                error_log('[SUPR DB Migration] Migration 8. File was skipped: ' . $file);
            }
        }

        error_log('[SUPR DB Migration] Migration 8 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }

    /**
     * Get recursive file-list
     *
     * @param $dir
     * @return array
     */
    private function getFileList($dir): array
    {
        $result = [];
        $cdir = \scandir($dir, null);
        foreach ($cdir as $key => $value) {
            if (!\in_array($value, ['.', '..'])) {
                if (\is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                    $result = \array_merge($result, $this->getFileList($dir . $value . DIRECTORY_SEPARATOR));
                } else {
                    $result[] = $dir . $value;
                }
            }
        }

        return $result;
    }
}
