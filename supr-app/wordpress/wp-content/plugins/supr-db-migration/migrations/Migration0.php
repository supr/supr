<?php

namespace SuprDBMigration;

/**
 * Class Migration0
 *
 * @package SuprDBMigration
 */
class Migration0 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Migration 0 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
