<?php

namespace SuprDBMigration;

/**
 * Class Migration25
 *
 * Switch to new theme (hello elementor)
 *
 * @package SuprDBMigration
 */
class Migration25 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        //Get current logo
        $siteLogoId = \get_theme_mod('custom_logo', '');
        // Switch to new theme
        \switch_theme('hello-elementor-supr');
        // Set logo back
        \set_theme_mod('custom_logo', $siteLogoId);
        // Clear cache
        \do_action('supr-clear-cache');

        error_log('[SUPR DB Migration] Migration 25 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
