<?php

namespace SuprDBMigration;

/**
 * Class Migration15
 *
 * @package SuprDBMigration
 */
class Migration15 extends Migration
{
    /** Set space limit for wp-ultimo for each shop
     *
     * @return bool
     */
    public function execute(): bool
    {
        update_option('blog_upload_space', 2000);

        error_log('[SUPR DB Migration] Migration 15 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
