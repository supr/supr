<?php

namespace SuprDBMigration;

/**
 * Class Migration40
 *
 * Add license to some plugins
 *
 * @package SuprDBMigration
 */
class Migration40 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Migration 40 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
