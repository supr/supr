<?php

namespace SuprDBMigration;

/**
 * Class Migration33
 *
 * Galery fix
 *
 * @package SuprDBMigration
 */
class Migration33 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        // Set gallery to 100%
        if (\get_option('woo_variation_gallery_width', false)) {
            \update_option('woo_variation_gallery_width', '100', true);
        } else {
            \add_option('woo_variation_gallery_width', '100', true);
        }

        // Clear cache
        \do_action('supr-clear-cache');

        error_log('[SUPR DB Migration] Migration 33 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
