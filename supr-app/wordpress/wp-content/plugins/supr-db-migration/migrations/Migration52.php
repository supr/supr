<?php

namespace SuprDBMigration;

/**
 * Class Migration52
 *
 * SUPR DASHBOARD: Migrate a lot of options in one array
 *
 * @package SuprDBMigration
 */
class Migration52 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        global $wpdb;

        // Get existed options
        $newOptions = [
            'wizard_finished' => \get_option('supr_dashboard_wizard_finished', null),
            'wizard_store_owner' => \get_option('supr_dashboard_wizard_store_owner', null),
            'wizard_design' => \get_option('supr_dashboard_wizard_design', null),
            'wizard_product' => \get_option('supr_dashboard_wizard_product', null),
            'wizard_shipping' => \get_option('supr_dashboard_wizard_shipping', null),
            'wizard_legal' => \get_option('supr_dashboard_wizard_legal', null),
            'wizard_plan' => \get_option('supr_dashboard_wizard_plan', null)
        ];

        // Remove NULLs
        $newOptions = array_filter($newOptions);

        // Cast to bool
        // There are; 1 / "1" / true / "yes"
        foreach ($newOptions as $num => $val) {
            $newOptions[$num] = filter_var($val, FILTER_VALIDATE_BOOLEAN);
        }

        // Remove old options
        $optionsTableName = $wpdb->prefix . 'options';
        $wpdb->query("DELETE FROM `{$optionsTableName}` WHERE `option_name` LIKE 'supr_dashboard_wizard%'");

        // Save as new option
        \update_option('supr_dashboard_wizard_options', $newOptions);

        error_log('[SUPR DB Migration] Migration 52 for blog #' . \get_current_blog_id() . ' was executed.');

        return true;
    }
}
