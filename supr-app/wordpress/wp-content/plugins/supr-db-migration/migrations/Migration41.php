<?php

namespace SuprDBMigration;

/**
 * Class Migration41
 *
 * Fix blogs without owners
 *
 * @package SuprDBMigration
 */
class Migration41 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        global $wpdb;

        // This migration is a fix for WP-Ultimo
        if (!\class_exists('\WU_Site_Owner')) {
            return true;
        }

        $blogsTable = $wpdb->base_prefix . 'blogs';
        $wuSiteOwnerTable = $wpdb->base_prefix . 'wu_site_owner';
        $userMetaTable = $wpdb->base_prefix . 'usermeta';

        // Find blogs without owners (according WP-Ultimo)
        $blogsWithoutOwner = $wpdb->get_results("SELECT `$blogsTable`.`blog_id` FROM `{$blogsTable}` LEFT JOIN `{$wuSiteOwnerTable}` ON `{$wuSiteOwnerTable}`.`site_id` = `{$blogsTable}`.`blog_id` WHERE `{$wuSiteOwnerTable}`.`user_id` IS NULL", ARRAY_A);

        foreach ($blogsWithoutOwner as $blogWithoutOwner) {
            $blogId = $blogWithoutOwner['blog_id'];

            // Get blog owner (according WordPress)
            $querystring = "SELECT `user_id` FROM `{$userMetaTable}` WHERE (`meta_key` LIKE 'primary_blog' AND `meta_value` LIKE '{$blogId}') ORDER BY `user_id` LIMIT 1";
            $userId = (int)$wpdb->get_var($querystring);

            // If blog has an owner in Word-Press table, we make his as an owner in WP-Ultimo
            if ($userId) {
                $wpdb->insert($wuSiteOwnerTable, ['site_id' => $blogId, 'user_id' => $userId]);
                error_log('[SUPR DB Migration] Migration 41: user #' . $userId . ' was set as an owner for blog #' . $blogId . '.');

                // Check, that the user has a tariff
                if (!\wu_get_subscription($userId)) {
                    error_log('[SUPR DB Migration] Migration 41: user #' . $userId . ' doesn\'t have any subscription. Please fix it manualy.');
                }
            } else {
                error_log('[SUPR DB Migration] Migration 41: blog #' . $blogId . ' doesn\'t have any administrator.');
            }
        }

        error_log('[SUPR DB Migration] Migration 41 was executed.');

        return true;
    }
}
