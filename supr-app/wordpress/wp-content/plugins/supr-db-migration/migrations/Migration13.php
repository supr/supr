<?php

namespace SuprDBMigration;

/**
 * Class Migration13
 *
 * @package SuprDBMigration
 */
class Migration13 extends Migration
{
    /**
     *
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Migration 13 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
