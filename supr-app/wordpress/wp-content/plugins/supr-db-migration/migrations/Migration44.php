<?php

namespace SuprDBMigration;

/**
 * Class Migration44
 *
 * License for WooVariationSwatches
 *
 * @package SuprDBMigration
 */
class Migration44 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Migration 44 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
