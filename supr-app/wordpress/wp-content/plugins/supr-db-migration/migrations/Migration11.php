<?php

namespace SuprDBMigration;

/**
 * Class Migration11
 *
 * @package SuprDBMigration
 */
class Migration11 extends Migration
{
    /**
     * Turn off Defender Pro file scanning #S2-962
     *
     * @return bool
     */
    public function execute(): bool
    {
        global $wpdb;

        $wpdb->query('UPDATE ' . $wpdb->get_blog_prefix() . "`sitemeta` SET `meta_value` = 'a:14:{s:9:\"scan_core\";s:1:\"0\";s:9:\"scan_vuln\";s:1:\"0\";s:12:\"scan_content\";s:1:\"0\";s:8:\"receipts\";a:2:{i:0;i:2;i:1;i:625;}s:12:\"notification\";s:1:\"1\";s:11:\"always_send\";s:1:\"1\";s:12:\"max_filesize\";s:1:\"1\";s:13:\"email_subject\";s:57:\"Scan of {SITE_URL} complete. {ISSUES_COUNT} issues found.\";s:15:\"email_has_issue\";s:268:\"Hi {USER_NAME},\r\n\r\nWP Defender here, reporting back from the front.\r\n\r\nI\'ve finished scanning {SITE_URL} for vulnerabilities and I found {ISSUES_COUNT} issues that you should take a closer look at!\r\n{ISSUES_LIST}\r\n\r\nStay Safe,\r\nWP Defender\r\nOfficial WPMU DEV Superhero\";s:12:\"email_all_ok\";s:350:\"Hi {USER_NAME},\r\n\r\nWP Defender here, reporting back from the front.\r\n\r\nI\'ve finished scanning {SITE_URL} for vulnerabilities and I found nothing. Well done for running such a tight ship!\r\n\r\nKeep up the good work! With regular security scans and a well-hardened installation you\'ll be just fine.\r\n\r\nStay safe,\r\nWP Defender\r\nOfficial WPMU DEV Superhero\";s:9:\"frequency\";s:1:\"1\";s:3:\"day\";s:6:\"Monday\";s:4:\"time\";s:4:\"8:00\";s:14:\"lastReportSent\";s:19:\"2018-08-08 14:13:26\";}' WHERE `sitemeta`.`meta_id` = 127;");

        error_log('[SUPR DB Migration] Migration 11 was executed.');

        return true;
    }
}
