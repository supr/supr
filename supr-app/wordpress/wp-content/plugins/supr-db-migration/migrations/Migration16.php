<?php

namespace SuprDBMigration;

/**
 * Class Migration16
 *
 * @package SuprDBMigration
 */
class Migration16 extends Migration
{
    /** Add jQuery in Black-List of the cache plugin
     *
     * @return bool
     */
    public function execute(): bool
    {
        $optionValue = \get_option('fastvelocity_min_ignorelist', null);

        if ($optionValue === null) {
            return true;
        }

        $items = \explode("\r\n", $optionValue);

        $newItems = [
            '/js/jquery/jquery-migrate.min.js',
            '/js/jquery/jquery-migrate.js',
            '/js/jquery/jquery.min.js',
            '/js/jquery/jquery.js'
        ];

        foreach ($newItems as $link) {
            if (!\in_array($link, $items, true)) {
                $items[] = $link;
            }
        }

        $optionValue = \implode("\r\n", $items);

        \update_option('fastvelocity_min_ignorelist', $optionValue);

        error_log('[SUPR DB Migration] Migration 14 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
