<?php

namespace SuprDBMigration;

/**
 * Class Migration7
 *
 * @package SuprDBMigration
 */
class Migration7 extends Migration
{
    /**
     * Delete IPs from comments
     *
     * @return bool
     */
    public function execute(): bool
    {
        global $wpdb;

        $wpdb->query('UPDATE ' . $wpdb->get_blog_prefix() . 'comments SET comment_author_IP = NULL');

        error_log('[SUPR DB Migration] Migration 7 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
