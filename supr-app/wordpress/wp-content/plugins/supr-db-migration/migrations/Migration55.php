<?php

namespace SuprDBMigration;

/**
 * Class Migration54
 *
 * PART COPY OF Migration44. License for WooVariationSwatches
 *
 * @package SuprDBMigration
 */
class Migration55 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        error_log('[SUPR DB Migration] Migration 54 for blog #' . \get_current_blog_id() . ' was executed.');

        return true;
    }
}
