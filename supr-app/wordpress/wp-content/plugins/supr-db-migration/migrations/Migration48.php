<?php

namespace SuprDBMigration;

/**
 * Class Migration48
 *
 * Define default reduced images formats (because of costs)
 *
 * @package SuprDBMigration
 */
class Migration48 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        $option = [
            0 => 'on',
            'woocommerce_thumbnail' => 'on',
            'woocommerce_single' => 'on'
        ];

        \update_option('tinypng_sizes', $option);

        error_log('[SUPR DB Migration] Migration 48 for blog #' . \get_current_blog_id() . ' was executed.');

        return true;
    }
}
