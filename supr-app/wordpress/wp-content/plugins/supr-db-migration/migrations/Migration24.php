<?php

namespace SuprDBMigration;

/**
 * Class Migration24
 *
 * Activate network wide new theme (hello elementor)
 *
 * @package SuprDBMigration
 */
class Migration24 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        \WP_Theme::network_enable_theme('hello-elementor-supr');

        error_log('[SUPR DB Migration] Global migration 24 was executed.');

        return true;
    }
}
