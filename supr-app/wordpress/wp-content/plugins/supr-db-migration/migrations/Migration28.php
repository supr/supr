<?php

namespace SuprDBMigration;

/**
 * Class Migration28
 *
 * Delete wp ultimo logs from storage
 *
 * @package SuprDBMigration
 */
class Migration28 extends Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        $logsDir = ABSPATH . 'wp-content/uploads/wu-logs/';

        $filesToClear = [
            'billwerk-api.log',
            'billwerk-js-errors.log',
            'database-changes.log',
            'mailer-errors.log',
            'screenshot-scraper.log',
            'signup.log',
            'wub-cron.log',
        ];

        if (\function_exists('shell_exec') && (int)\shell_exec('echo 1') === 1) {
            foreach ($filesToClear as $file) {
                // Sync dirs with many sub-processes
                $filePath = $logsDir . $file;
                \shell_exec("echo '' > {$filePath}");
            }
        }

        error_log('[SUPR DB Migration] Migration 28 was executed.');

        return true;
    }
}
