<?php

namespace SuprDBMigration;

/**
 * Class Migration19
 *
 * Don't process Google Fonts with FVM plugin
 *
 * @package SuprDBMigration
 */
class Migration19 extends Migration
{
    /**
     * Replace http with https after disable of Simple SSL Plugin
     *
     * @return bool
     */
    public function execute(): bool
    {
        // set not process google fonts
        \update_option('fastvelocity_gfonts_method', 2);
        \update_option('fastvelocity_min_skip_google_fonts', 1);

        error_log('[SUPR DB Migration] Migration 19 for blog #' . get_current_blog_id() . ' was executed.');

        return true;
    }
}
