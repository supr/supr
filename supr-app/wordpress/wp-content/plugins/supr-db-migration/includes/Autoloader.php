<?php

namespace SuprDBMigration;

/**
 * Class Autoloader
 *
 * @package SuprDBMigration
 */
class Autoloader
{
    /**
     * Run autoloader.
     *
     * Register a function as `__autoload()` implementation.
     */
    public static function run(): void
    {
        spl_autoload_register([__CLASS__, 'autoload']);
    }

    /**
     * Autoload.
     *
     * For a given class, check if it exist and load it.
     *
     * @param string $class Class name.
     */
    private static function autoload($class): void
    {
        $realClassName = str_replace(__NAMESPACE__ . '\\', '', $class);

        // Classes
        $filePathClasses = SUPR_DB_MIGRATION_PATH . 'includes/' . $realClassName . '.php';

        if (file_exists($filePathClasses) && is_readable($filePathClasses)) {
            require_once $filePathClasses;
            return;
        }

        // Migrations
        $filePathMigrations = SUPR_DB_MIGRATION_PATH . 'migrations/' . $realClassName . '.php';

        if (file_exists($filePathMigrations) && is_readable($filePathMigrations)) {
            require_once $filePathMigrations;
            return;
        }
    }
}
