<?php

namespace SuprDBMigration;

/**
 * Class Manager
 *
 * @package SuprDBMigration
 */
class Manager
{
    /**
     * @var Manager
     */
    private static $instance;

    /**
     * @var int
     */
    private $dbVersion = 55;

    /**
     * Types of migrations: global, currentSite
     *
     * @var array
     */
    private $dbMigrationTypes = [
        0 => 'currentSite',
        1 => 'global',
        2 => 'global',
        3 => 'currentSite',
        4 => 'currentSite',
        5 => 'currentSite',
        6 => 'currentSite',
        7 => 'currentSite',
        8 => 'global',
        9 => 'currentSite',
        10 => 'currentSite',
        11 => 'global',
        12 => 'currentSite',
        13 => 'currentSite',
        14 => 'global',
        15 => 'currentSite',
        16 => 'currentSite',
        17 => 'global',
        18 => 'currentSite',
        19 => 'currentSite',
        20 => 'currentSite',
        21 => 'currentSite',
        22 => 'global',
        23 => 'currentSite',
        24 => 'global',
        25 => 'currentSite',
        26 => 'currentSite',
        27 => 'currentSite',
        28 => 'global',
        29 => 'currentSite',
        30 => 'currentSite',
        31 => 'global',
        32 => 'currentSite',
        33 => 'currentSite',
        34 => 'global',
        35 => 'currentSite',
        36 => 'currentSite',
        37 => 'currentSite',
        38 => 'currentSite',
        39 => 'currentSite',
        40 => 'currentSite',
        41 => 'global',
        42 => 'currentSite',
        43 => 'currentSite',
        44 => 'currentSite',
        45 => 'currentSite',
        46 => 'currentSite',
        47 => 'currentSite',
        48 => 'currentSite',
        49 => 'currentSite',
        50 => 'currentSite',
        51 => 'currentSite',
        52 => 'currentSite',
        53 => 'global',
        54 => 'currentSite',
        55 => 'currentSite',
    ];

    /**
     * Manager constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return Manager
     */
    public static function getInstance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Does all migrations
     *
     * @return bool
     */
    public function migrate(): bool
    {
        $lastExecutionTime = (int)\get_option('supr_db_migration_last_exec', 0);

        // If other migration works (we execute migration not more than 1 time per hour)
        if ($lastExecutionTime > \time() - 3600) {
            return false;
        }

        // Check version
        $currentSiteVersion = (int)\get_option('supr_db_migration_db_version', 0);
        $globalVersion = (int)\get_blog_option(1, 'supr_db_migration_db_version', 0);

        // Go from small version to big
        for ($i = min($currentSiteVersion, $globalVersion); $i <= $this->dbVersion; $i++) {
            $migrationType = $this->dbMigrationTypes[$i];

            // If current site was also updated
            if ($migrationType === 'currentSite' && $i <= $currentSiteVersion) {
                continue;
            }

            // If main migration was also executed
            if ($migrationType === 'global' && $i <= $globalVersion) {
                continue;
            }

            // Disable W3TC caching
            define('DONOTCACHEDB', true);

            // Set lock
            \update_option('supr_db_migration_last_exec', time());

            $migrationClassName = '\SuprDBMigration\Migration' . $i;
            /** @var Migration $migration */
            $migration = new $migrationClassName();

            try {
                if ($migration->execute()) {
                    switch ($migrationType) {
                        case 'currentSite':
                            \update_option('supr_db_migration_db_version', $i);
                            break;
                        case 'global':
                            \update_blog_option(1, 'supr_db_migration_db_version', $i);
                            break;
                    }
                } else {
                    // The Migration was not successful. We will try to execute it again in one hour.
                    // Without this migration we cannot go forward.
                    return false;
                }
            } catch (\Exception $e) {
                error_log('[SUPR DB Migration] I cannot execute migration #' . $i . '. Error: ' . $e->getMessage());

                return false;
            }
        }

        return true;
    }
}
