<?php

namespace SuprDBMigration;

/**
 * Class Migration
 *
 * @package SuprDBMigration
 */
class Migration
{
    /**
     * @return bool
     */
    public function execute(): bool
    {
        return false;
    }

    /**
     * @return bool
     */
    public function rollBack(): bool
    {
        return false;
    }
}
