<?php

namespace SuprDBMigration;

/**
 * Class Plugin
 *
 * @package SuprDBMigration
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function getInstance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Init
     */
    public function init(): void
    {
        // Don't do any migrations if CLI or AJAX
        if (defined('DOING_AJAX') || defined('WP_CLI')) {
            return;
        }

        // Some migrations can be executed only in admin panel.
        // We don't have such logic in manager. So we start plugin only in admin panel.
        if (!\is_admin()) {
            return;
        }

        // Do necessary migrations
        Manager::getInstance()->migrate();
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        \add_action('init', [$this, 'init'], 0);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_DB_MIGRATION_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::getInstance();
