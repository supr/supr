<?php

namespace SuprWordpress;

/**
 * Class ShortCodeManager
 *
 * @package SuprWordpress
 */
class ShortCodeManager
{
    /**
     * Register short codes
     */
    public static function registerShortCodes(): void
    {
        \add_shortcode('default-imprint', [self::class, 'getDefaultImprint']);
    }

    /**
     * Return default imprint
     */
    public static function getDefaultImprint(): string
    {
        $store_owner_company = \get_option('woocommerce_store_owner_company', '');
        $store_owner_first_name = \get_option('woocommerce_store_owner_first_name', '');
        $store_owner_last_name = \get_option('woocommerce_store_owner_last_name', '');
        $store_owner_address = \get_option('woocommerce_store_address', '');
        $store_owner_city = \get_option('woocommerce_store_city', '');
        $store_owner_postcode = \get_option('woocommerce_store_postcode', '');
        $store_owner_email = \get_option('woocommerce_store_owner_email', '');
        $store_owner_phone = \get_option('woocommerce_store_owner_phone', '');
        $store_owner_phone_costs = \get_option('woocommerce_store_owner_phone_costs', '');
        $store_owner_fax = \get_option('woocommerce_store_owner_fax', '');
        $store_owner_fax_costs = \get_option('woocommerce_store_owner_fax_costs', '');

        ob_start();
        ?>
        <h4><?= __('Imprint', 'supr-wordpress') ?></h4>
        <p><?= __('This store is operated by', 'supr-wordpress') ?>:</p>
        <hr>
        <p>
            <strong><?= $store_owner_company ?></strong><br>
            <?= $store_owner_first_name . ' ' . $store_owner_last_name ?><br>
            <?= $store_owner_address ?><br>
            <?= $store_owner_city . ' ' . $store_owner_postcode ?>
        </p>
        <p>
            <strong><?php _e('Represented by', 'supr-wordpress') ?>:</strong><br>
            <?= $store_owner_first_name . ' ' . $store_owner_last_name ?><br>
        </p>
        <p>
            <strong><?php _e('Contact', 'supr-wordpress') ?>:</strong><br>
            <?php _e('E-Mail', 'supr-wordpress') ?>: <?= $store_owner_email ?><br>
            <?php _e('Phone', 'supr-wordpress') ?>: <?= $store_owner_phone ?><br>
            <small><?= $store_owner_phone_costs ?></small>
            <br>
            <?php _e('Fax', 'supr-wordpress'); ?>: <?= $store_owner_fax ?><br>
            <small><?= $store_owner_fax_costs ?></small>
        </p>
        <?php
        return ob_get_clean();
    }
}
