<?php

namespace SuprWordpress;

class Uploads
{
    /**
     * New allowed mime types
     *
     * @param array $mimes
     * @return array
     */
    public static function addMimeTypes(array $mimes): array
    {
        if (!isset($mimes['webp'])) {
            $mimes['webp'] = 'image/webp';
        }

        if (!isset($mimes['jp2'])) {
            $mimes['jp2'] = 'image/jp2';
        }

        if (!isset($mimes['jpx'])) {
            $mimes['jpx'] = 'application/octet-stream';
        }

        return $mimes;
    }
}
