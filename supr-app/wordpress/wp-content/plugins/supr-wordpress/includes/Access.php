<?php

namespace SuprWordpress;

/**
 * Class Access
 *
 * @package SuprWordpress
 */
class Access
{
    /**
     * Pages: /wp-admin/edit.php?post_type=page
     *
     * @var array
     */
    private $protectedPages;

    /**
     * Templates: /wp-admin/edit.php?post_type=elementor_library
     *
     *
     * @var array
     */
    private $protectedTemplates;

    /**
     * Posts: /wp-admin/post.php?post=4&action=edit
     *
     *
     * @var array
     */
    private $blockedTemplates;

    /**
     * @var bool Don't check access rules to pages for network admins
     */
    private $allowNetworkAdmin;

    /**
     * @var Access
     */
    public static $instance;

    /**
     * @return Access
     */
    public static function getInstance(): Access
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
        $this->loadOptions();
    }

    private function loadOptions(): void
    {
        $options = \get_site_option('supr_wordpress_options', []);

        $this->protectedPages = $options['protected_page_ids'] ?? [];
        $this->protectedTemplates = $options['protected_template_ids'] ?? [];
        $this->blockedTemplates = $options['blocked_template_ids'] ?? [];
        $this->allowNetworkAdmin = $options['allow_network_admin'] ?? false;
    }

    /**
     * Prevent post from deleting by non superadmin
     */
    public function listenPostDeletion(): void
    {
        // Don't allow to delete
        \add_action('wp_trash_post', [$this, 'checkPostDeletionRestriction'], 10, 1);
        \add_action('before_delete_post', [$this, 'checkPostDeletionRestriction'], 10, 1);
        // Don't allow to change status
        \add_action('publish_to_pending', [$this, 'setPublishStatus'], 10, 1);
        \add_action('publish_to_draft', [$this, 'setPublishStatus'], 10, 1);
    }

    /**
     * Disable wordpress admin pages
     */
    public function blockPages(): void
    {
        if (!isset($_GET['post']) || ($this->allowNetworkAdmin && \is_super_admin())) {
            return;
        }

        if (in_array((int)$_GET['post'], $this->blockedTemplates, true)) {
            \wp_die(__('You cannot edit this entry!', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }
    }

    /**
     * We should not update plugins on WEB-Interface
     */
    public function disablePluginsUpdate(): void
    {
        // Via /wp-admin[/network]/plugins.php
        if (isset($_GET['action'], $_GET['plugin']) && $_GET['action'] === 'upgrade-plugin') {
            \wp_die(__('Plugin update via admin-panel is disabled. Sorry.', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }

        // Via /wp-admin/admin-ajax.php
        if (isset($_POST['action'], $_POST['plugin']) && $_POST['action'] === 'update-plugin') {
            \wp_die(__('Plugin update via admin-ajax is disabled. Sorry.', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }

        // Via /wp-admin/network/update-core.php?action=do-plugin-upgrade
        if (isset($_GET['action'], $_POST['checked']) && $_GET['action'] === 'do-plugin-upgrade') {
            \wp_die(__('Plugin update via update-core is disabled. Sorry.', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }
    }

    /**
     * We should not delete plugins on WEB-Interface
     */
    public function disablePluginsDeletion(): void
    {
        // Via /wp-admin[/network]/plugins.php
        if (isset($_GET['action'], $_GET['checked']) && $_GET['action'] === 'delete-selected' && strpos($_SERVER['REQUEST_URI'], 'plugins.php') !== false) {
            \wp_die(__('Plugin deletion via admin-panel is disabled. Sorry.', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }

        // Via /wp-admin/admin-ajax.php
        if (isset($_POST['action'], $_POST['plugin']) && $_POST['action'] === 'delete-plugin' && strpos($_SERVER['REQUEST_URI'], 'admin-ajax.php') !== false) {
            \wp_die(__('Plugin deletion via admin-ajax is disabled. Sorry.', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }
    }

    /**
     * We should not update theme on WEB-Interface
     */
    public function disableThemeUpdate(): void
    {
        // Via /wp-admin[/network]/themes.php
        if (isset($_GET['action'], $_GET['theme']) && $_GET['action'] === 'upgrade-theme') {
            \wp_die(__('Theme update via admin-panel is disabled. Sorry.', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }

        // Via /wp-admin/admin-ajax.php
        if (isset($_POST['action'], $_POST['slug']) && $_POST['action'] === 'update-theme') {
            \wp_die(__('Theme update via admin-ajax is disabled. Sorry.', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }
    }

    /**
     * We should not delete theme on WEB-Interface
     */
    public function disableThemeDeletion(): void
    {
        // Via /wp-admin[/network]/themes.php
        if (isset($_GET['action'], $_GET['checked']) && $_GET['action'] === 'delete-selected' && strpos($_SERVER['REQUEST_URI'], 'themes.php') !== false) {
            \wp_die(__('Theme deletion via admin-panel is disabled. Sorry.', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }

        // Via /wp-admin/admin-ajax.php
        if (isset($_POST['action'], $_POST['slug']) && $_POST['action'] === 'delete-theme' && strpos($_SERVER['REQUEST_URI'], 'admin-ajax.php') !== false) {
            \wp_die(__('Theme deletion via admin-ajax is disabled. Sorry.', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }
    }

    /**
     * We should not update WP on WEB-Interface
     */
    public function disableWPCoreUpdate(): void
    {
        // Via /wp-admin[/network]/update-core.php?action=do-core-upgrade
        if (isset($_GET['action']) && $_GET['action'] === 'do-core-upgrade' && strpos($_SERVER['REQUEST_URI'], 'update-core.php') !== false) {
            \wp_die(__('WP update via web is disabled. Sorry.', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }
    }

    /**
     * Show error if not allowed
     *
     * @param $postId
     */
    public function checkPostDeletionRestriction($postId): void
    {
        if ($this->allowNetworkAdmin && \is_super_admin()) {
            return;
        }

        if (\in_array($postId, $this->protectedPages, true) || \in_array($postId, $this->protectedTemplates, true) || \in_array($postId, $this->blockedTemplates, true)) {
            \wp_die(__('You cannot delete this entry!', 'supr-wordpress'), __('Access denied', 'supr-wordpress'), ['response' => 403]);
        }
    }

    /**
     * Set status to publish for protected posts
     *
     * @param \WP_Post $post
     */
    public function setPublishStatus($post): void
    {
        if ($this->allowNetworkAdmin && \is_super_admin()) {
            return;
        }

        if (\in_array($post->ID, $this->protectedPages, true) || \in_array($post->ID, $this->protectedTemplates, true) || \in_array($post->ID, $this->blockedTemplates, true)) {
            \wp_update_post(
                [
                    'ID' => $post->ID,
                    'post_status' => 'publish'
                ]
            );
        }
    }
}
