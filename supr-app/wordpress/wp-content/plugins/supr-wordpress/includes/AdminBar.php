<?php

namespace SuprWordpress;

/**
 * Class AdminBar
 *
 * @package SuprWordpress
 */
class AdminBar
{
    /**
     * Show setting page
     */
    public static function renderAdminBar(): void
    {
        include SUPR_WORDPRESS_PATH . 'templates/network-admin-bar.php';
    }

    /**
     * Add and remove some nodes
     */
    public static function changeAdminBarNodes(): void
    {
        /** @var \WP_Admin_Bar $wp_admin_bar */
        global $wp_admin_bar;

        $wp_admin_bar->remove_menu('wp-logo');
        $wp_admin_bar->remove_menu('about');
        $wp_admin_bar->remove_menu('updates');
        $wp_admin_bar->remove_menu('comments');
        $wp_admin_bar->remove_menu('customize');
        $wp_admin_bar->remove_menu('themes');
        $wp_admin_bar->remove_menu('my-account');
        $wp_admin_bar->remove_menu('new_draft');
        $wp_admin_bar->remove_menu('new-draft');
        $wp_admin_bar->remove_menu('new-content');
        $wp_admin_bar->remove_menu('site-name');
        $wp_admin_bar->remove_menu('search');
        $wp_admin_bar->remove_menu('elementor-maintenance-on');
        $wp_admin_bar->remove_menu('wu-my-account');
        $wp_admin_bar->remove_menu('w3tc');

        // Get blog public status to show in admin bar | '1' = maintenance on
        $elementorMaintenanceModeMode = \get_option('elementor_maintenance_mode_mode');

        if ($elementorMaintenanceModeMode) {
            $wp_admin_bar->add_menu(
                [
                    'id' => 'store-status',
                    'parent' => 'top-secondary',
                    'title' => '<span data-tippy-content="' . \__('Store not available for customers (change)', 'supr-wordpress') . '" class="tippy pulse-danger"></span>',
                    'href' => \admin_url("admin.php?page=supr_wordpress_options"),
                    'meta' => false
                ]
            );
        } else {
            $wp_admin_bar->add_menu(
                [
                    'id' => 'store-status',
                    'parent' => 'top-secondary',
                    'title' => '<span data-tippy-content="' . \__('Store available for customers (change)', 'supr-wordpress') . '" class="tippy pulse-success"></span>',
                    'href' => \admin_url("admin.php?page=supr_wordpress_options"),
                    'meta' => false
                ]
            );
        }

        if (\is_admin()) {
            // If it is admin panel

            $wp_admin_bar->add_menu(
                [
                    'id' => 'visit-store',
                    'parent' => 'top-secondary',
                    'title' => \__('Visit store', 'supr-wordpress'),
                    'href' => \home_url(),
                    'meta' => [
                        'target' => '_blank'
                    ]
                ]
            );

            $wp_admin_bar->add_menu(
                [
                    'id' => 'supr-clear-cache',
                    'parent' => 'top-secondary',
                    'title' => '<span class="tippy" data-tippy-content="' . \__('Recreate all cache files (fonts, js, css, html)', 'supr-wordpress') . '">' . __('Clear cache', 'supr-wordpress') . '</span>',
                ]
            );

            $wp_admin_bar->add_menu(
                [
                    'id' => 'supr-account',
                    'parent' => 'top-secondary',
                    'title' => \__('Account', 'supr-wordpress'),
                ]
            );
            $wp_admin_bar->add_menu(
                [
                    'id' => 'wu-plan',
                    'parent' => 'supr-account',
                    'title' => \__('Plan', 'supr-wordpress'),
                    'href' => \admin_url("admin.php?page=wu-my-account")
                ]
            );
            $wp_admin_bar->add_menu(
                [
                    'id' => 'profile',
                    'parent' => 'supr-account',
                    'title' => \__('Profile', 'supr-wordpress'),
                    'href' => \get_edit_profile_url(),
                ]
            );
            $wp_admin_bar->add_menu(
                [
                    'id' => 'log-out',
                    'parent' => 'supr-account',
                    'title' => \__('Log out', 'supr-wordpress'),
                    'href' => \wp_logout_url(),
                ]
            );
        } else {
            // If it is front of store

            $wp_admin_bar->add_menu(
                [
                    'id' => 'store-admin',
                    'parent' => 'top-secondary',
                    'title' => \__('Shop administration', 'supr-wordpress'),
                    'href' => \home_url("/wp-admin")
                ]
            );
        }
    }
}
