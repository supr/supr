<?php

namespace SuprWordpress;

/**
 * Class Menu
 *
 * @package SuprCopOnboarding
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add network menu page
        \add_action('network_admin_menu', [__CLASS__, 'createNetworkMenu']);

        // Add network settings link on plugin page
        \add_filter('network_admin_plugin_action_links_' . SUPR_WORDPRESS_FILE, [__CLASS__, 'addNetworkSettingsPage']);

        // Add menu page
        \add_action('admin_menu', [__CLASS__, 'createAdminMenu']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createAdminMenu(): void
    {
        \add_menu_page(
            __('Configuration', 'supr-wordpress'),
            __('Configuration', 'supr-wordpress'),
            'manage_options',
            'supr_wordpress_options',
            [Page::class, 'settingsPage'],
            'dashicons-media-text',
            205
        );
    }

    /**
     * Add menu to admin panel
     */
    public static function createNetworkMenu(): void
    {
        \add_menu_page(
            __('SUPR Wordpress', 'supr-wordpress'),
            __('SUPR Wordpress', 'supr-wordpress'),
            'manage_network',
            'supr_wordpress_network_settings',
            [Page::class, 'networkSettingsPage'],
            'dashicons-wordpress'
        );
    }

    /**
     * @param $links
     * @return mixed
     */
    public static function addNetworkSettingsPage($links)
    {
        $settings_link = '<a href="admin.php?page=supr_wordpress_network_settings">' . __('Settings', 'supr-wordpress') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
