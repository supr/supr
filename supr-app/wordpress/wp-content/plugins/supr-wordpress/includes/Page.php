<?php

namespace SuprWordpress;

/**
 * Class Page
 *
 * @package SuprWordpress
 */
class Page
{
    /**
     * Show setting page
     */
    public static function settingsPage(): void
    {
        include SUPR_WORDPRESS_PATH . 'templates/settings-page.php';
    }

    /**
     * Show network setting page
     */
    public static function networkSettingsPage(): void
    {
        include SUPR_WORDPRESS_PATH . 'templates/network-settings-page.php';
    }
}
