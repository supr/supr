<?php

namespace SuprWordpress;

/**
 * Class MaintenanceMode
 *
 * @package SuprWordpress
 */
class MaintenanceMode
{
    public static $maintenanceModeOptionName = 'supr-maintenance-mode';

    /**
     * Load domain of main blog
     */
    public function __construct()
    {
        global $wpdb;

        // We don't have maintenance mode for CLI commands
        if (defined('WP_CLI') && WP_CLI === true) {
            return;
        }

        $sql = "SELECT * FROM {$wpdb->base_prefix}options WHERE `option_name` = '{$this::$maintenanceModeOptionName}'";
        $row = $wpdb->get_row($sql, ARRAY_A);

        if ($row && isset($row['option_value']) && (int)$row['option_value'] === 1) {
            header('HTTP/1.1 503 Service Temporarily Unavailable');
            header('Status: 503 Service Temporarily Unavailable');
            header('Retry-After: 300');
            die;
        }
    }
}
