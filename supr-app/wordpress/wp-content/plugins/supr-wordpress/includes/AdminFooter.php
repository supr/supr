<?php

namespace SuprWordpress;

/**
 * Class AdminFooter
 *
 * @package SuprWordpress
 */
class AdminFooter
{
    /**
     * Render Footer
     */
    public static function renderAdminFooter(): void
    {
        include SUPR_WORDPRESS_PATH . 'templates/admin-footer.php';
    }
}
