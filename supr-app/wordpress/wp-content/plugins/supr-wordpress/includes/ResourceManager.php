<?php

namespace SuprWordpress;

/**
 * Class ResourceManager
 *
 * @package SuprWordpress
 */
class ResourceManager
{
    /**
     * Register files for plugin
     */
    public static function addAdminResources(): void
    {
        // SUPR Admin Style & Script
        \wp_enqueue_style('supr_admin_style', SUPR_WORDPRESS_URL . 'scss/admin.css', [], SUPR_WORDPRESS_VERSION);
        \wp_enqueue_script('supr_admin_script', SUPR_WORDPRESS_URL . 'js/admin.js', ['jquery'], SUPR_WORDPRESS_VERSION, true);
        // Add js variables
        $values = [
            'swal_clear_cache_success_message' => __('All cache files were successfully deleted.', 'supr-wordpress')
        ];
        \wp_localize_script('supr_admin_script', 'supr_wordpress_translation', $values);

        // Sweet Alert 2 (in use)
        \wp_enqueue_style('supr_admin_style_swal', SUPR_WORDPRESS_URL . 'assets/sweetalert2/sweetalert2.min.css', [], SUPR_WORDPRESS_VERSION, 'all');
        \wp_enqueue_script('supr_admin_script_swal', SUPR_WORDPRESS_URL . 'assets/sweetalert2/sweetalert2.min.js', ['jquery'], SUPR_WORDPRESS_VERSION, true);

        // Tippy tooltip (in use)
        \wp_enqueue_script('supr_tippy', SUPR_WORDPRESS_URL . 'assets/tippy/tippy.all.min.js', ['jquery'], SUPR_WORDPRESS_VERSION, true);

        // Supr settings page
        if (isset($_GET['page']) && $_GET['page'] === 'supr_wordpress_options') {
            // LC Switch (in use)
            \wp_enqueue_style('supr_admin_style_lcswitch', SUPR_WORDPRESS_URL . 'assets/lc-switch/lc_switch.css', [], SUPR_WORDPRESS_VERSION);
            \wp_enqueue_script('supr_admin_script_lcswitch', SUPR_WORDPRESS_URL . 'assets/lc-switch/lc_switch.min.js', ['jquery'], SUPR_WORDPRESS_VERSION, true);

            // JS for settings page
            \wp_enqueue_script('supr_admin_script_supr_wordpress_options', SUPR_WORDPRESS_URL . 'js/supr-wordpress-options.js', ['jquery'], SUPR_WORDPRESS_VERSION, true);

            // Add js variables
            $values = [
                'lc_switch_on' => __('On', 'supr-wordpress'),
                'lc_switch_off' => __('Off', 'supr-wordpress'),
                'swal_instant_save_success' => __('Changes saved!', 'supr-wordpress'),
                'swal_instant_save_wait' => __('Saving changes…', 'supr-wordpress'),
                'swal_clear_cache_success_message' => __('All cache files were successfully deleted.', 'supr-wordpress')
            ];
            \wp_localize_script('supr_admin_script_supr_wordpress_options', 'supr_wordpress_translation', $values);
        }

        // Hide woocommerce settings
        if (isset($_GET['page'], $_GET['tab']) && $_GET['page'] === 'wc-settings' && $_GET['tab'] === 'advanced') {
            // JS for settings page
            \wp_enqueue_script('supr_admin_script_wc_settings_advanced', SUPR_WORDPRESS_URL . 'js/wc-settings-advanced.js', ['jquery'], SUPR_WORDPRESS_VERSION, true);
        }

        // Add css for User Registration plugin because of our wrap in admin-panel
        if (isset($_GET['page']) && $_GET['page'] === 'add-new-registration') {
            \wp_enqueue_style('supr_admin_style_user_registration', SUPR_WORDPRESS_URL . 'scss/admin/user-registration.css', [], SUPR_WORDPRESS_VERSION);
        }
    }

    public static function addFrontendAdminBarStyles(): void
    {
        if (\is_user_logged_in()) {
            \wp_enqueue_style('supr_frontend_style', SUPR_WORDPRESS_URL . 'scss/frontend.css', [], SUPR_WORDPRESS_VERSION);
        }
    }
}
