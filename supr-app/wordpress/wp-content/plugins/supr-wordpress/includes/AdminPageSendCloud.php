<?php

namespace SuprWordpress;

/**
 * Class AdminPageSendCloud
 *
 * @package SuprWordpress
 */
class AdminPageSendCloud
{

    /**
     * Register short codes
     */
    public static function initAdminPage(): void
    {
        if (isset($_GET['page'], $_GET['tab']) && $_GET['page'] === 'wc-settings' && $_GET['tab'] === 'sendcloud') {
            \add_action('woocommerce_sections_sendcloud', [self::class, 'renderTemplate'], 999);
        }
    }

    /**
     * Register short codes
     */
    public static function renderTemplate(): void
    {
        include SUPR_WORDPRESS_PATH . 'templates/admin-page-sendcloud.php';
    }
}
