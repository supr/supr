<?php

namespace SuprWordpress;

class AjaxRestEndpoint
{
    /**
     * Add endpoints to get colors and fonts settings
     */
    public static function addEndPoints()
    {
        \add_action('wp_ajax_supr_settings_general_handle_ajax_request', [self::class, 'handleAjaxResponse']);
        \add_action('wp_ajax_supr_clear_cache', [self::class, 'clearCache']);
    }

    /**
     * @return void
     */
    public static function handleAjaxResponse(): void
    {
        // @todo validate
        $returned = ['success' => true, 'errors' => []];

        if ($_POST !== null) {
            $field = \sanitize_text_field($_POST['field']);
            $value = \sanitize_text_field($_POST['value']);
            \update_option($field, $value);
            $returned['success'] = true;
        } else {
            $returned['success'] = false;
        }

        echo json_encode($returned);
        \wp_die();
    }

    /**
     * Clear cache endpoint
     *
     * @return void
     */
    public static function clearCache(): void
    {
        // @todo validate
        $returned = ['success' => true, 'errors' => []];

        \do_action('supr-clear-cache');

        echo json_encode($returned);
        \wp_die();
    }
}
