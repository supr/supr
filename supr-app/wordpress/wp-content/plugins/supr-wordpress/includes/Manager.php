<?php

namespace SuprWordpress;

/**
 * Class Manager
 *
 * @package SuprWordpress
 */
class Manager
{
    /**
     * Limitation of category for any posts
     *
     * @var int
     */
    private static $categoriesCountLimit = 10;

    /**
     * Limitation of tags for any posts
     *
     * @var int
     */
    private static $tagsCountLimit = 10;

    /**
     * @var Manager
     */
    public static $instance;

    /**
     * Manager constructor.
     */
    private function __construct()
    {
    }

    /**
     * @return Manager
     */
    public static function getInstance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Remove editor from pages (frontpage) to restrict wrong usage
     */
    public static function removePostTypeSupport(): void
    {
        if (\is_super_admin()) {
            return;
        }

        $restrictedFeatures = [2];
        $features = ['page-attributes', 'editor', 'elementor', 'thumbnail', 'excerpt', 'post-formats', 'author'];

        if (isset($_GET['post']) && \in_array($_GET['post'], $restrictedFeatures, true)) {
            global $_wp_post_type_features;

            foreach ($features as $feature) {
                if (isset($_wp_post_type_features['page'][$feature])) {
                    unset($_wp_post_type_features['page'][$feature]);
                }
            }
        }
    }

    /**
     * Remove type='text/css' and type='text/javascript' (W3C standart)
     */
    public static function addRemoveTypeAttrOfTagsFilter(): void
    {
        \add_filter('style_loader_tag', [self::class, 'removeTypeAttrOfTags'], 10, 1);
        \add_filter('script_loader_tag', [self::class, 'removeTypeAttrOfTags'], 10, 1);
        ob_start([self::class, 'removeTypeAttrOfTags']);
    }

    /**
     * Remove type='text/css' and type='text/javascript'
     *
     * @param $string
     * @return mixed
     */
    public static function removeTypeAttrOfTags($string)
    {
        return preg_replace("/type=['\"]text\/(javascript|css)['\"] ?/", '', $string);
    }

    /**
     * Listen creation of new blog and set it to HTTPS
     */
    public function changeNewBlogToHttps(): void
    {
        \add_action('wp_initialize_site', [$this, 'updateBlogToHttps'], 11, 2);
    }

    /**
     * Update home and blog url to https
     *
     * @param \WP_Site $newSite
     * @param array    $args
     */
    public function updateBlogToHttps($newSite, array $args = []): void
    {
        \switch_to_blog($newSite->id);
        $oldHomeUrl = trailingslashit(esc_url(\get_option('home')));
        $oldSiteUrl = trailingslashit(esc_url(\get_option('siteurl')));
        $newHomeUrl = preg_replace('/^http:/', 'https:', $oldHomeUrl);
        $newSiteUrl = preg_replace('/^http:/', 'https:', $oldSiteUrl);
        \update_option('home', $newHomeUrl);
        \update_option('siteurl', $newSiteUrl);
        restore_current_blog();
    }

    /**
     * Back update of terms if limit reached
     *
     * @param int      $id
     * @param \WP_Post $post
     * @param bool     $update
     */
    public static function limitProductTermsCount(int $id, $post, bool $update): void
    {
        // It happens in elementor ( I don't know why :( )
        // From elementor comes instance of stdClass
        if (!$post instanceof \WP_Post) {
            return;
        }

        // Check if the post has a lot of products categories
        if ($post->post_type !== 'product') {
            return;
        }

        $postCategoryIds = \wp_get_post_terms($id, 'product_cat', ['fields' => 'ids']);
        if (\count($postCategoryIds) > self::$categoriesCountLimit) {
            \wp_set_post_terms($id, array_slice($postCategoryIds, 0, self::$categoriesCountLimit), 'product_cat');
        }

        $postTagIds = \wp_get_post_terms($id, 'product_tag', ['fields' => 'ids']);
        if (\count($postTagIds) > self::$tagsCountLimit) {
            \wp_set_post_terms($id, array_slice($postTagIds, 0, self::$tagsCountLimit), 'product_tag');
        }
    }
}
