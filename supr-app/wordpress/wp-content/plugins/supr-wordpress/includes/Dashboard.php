<?php

namespace SuprWordpress;

/**
 * Class Dashboard
 *
 * @package SuprCopOnboarding
 */
class Dashboard
{
    /**
     * Remove widgets from other plugins or from WP, that we don't need
     */
    public static function removeDashboardMetaBoxes(): void
    {
        \remove_meta_box('wpclever_dashboard_widget', 'dashboard', 'normal');
        \remove_meta_box('dashboard_primary', 'dashboard', 'normal');
        \remove_meta_box('tinypng_dashboard_widget', 'dashboard', 'normal');
        \remove_meta_box('dashboard_site_health', 'dashboard', 'normal');
        \remove_action('welcome_panel', 'wp_welcome_panel');
    }
}
