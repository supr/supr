<?php

namespace SuprWordpress;

/**
 * Class AdminStyle
 *
 * @package SuprWordpress
 */
class AdminStyle
{
    /**
     * Add style to admin area
     */
    public static function adminStyleInit(): void
    {
        \add_filter('admin_body_class', [self::class, 'addAdminBodyClass']);
    }

    /**
     * Add based on domain name classes to admin body
     *
     * @param $classes
     * @return string
     */
    public static function addAdminBodyClass($classes): string
    {
        $domain = str_replace('.', '-', DOMAIN_CURRENT_SITE ?? $_SERVER['HTTP_HOST']);

        if (\current_user_can('manage_network')) {
            return "{$classes} {$domain} {$domain}_network-admin";
        }

        return "$classes $domain";
    }
}
