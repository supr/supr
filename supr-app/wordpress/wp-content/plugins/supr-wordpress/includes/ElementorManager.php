<?php

namespace SuprWordpress;

/**
 * Class ElementorManager
 *
 * @package SuprWordpress
 */
class ElementorManager
{
    /**
     * Remove post columns
     */
    public static function removePostColumns(): void
    {
        \add_filter('manage_elementor_library_posts_columns', [self::class, 'deleteColumns']);
    }

    /**
     * @param array $columns
     * @return array
     */
    public static function deleteColumns($columns): array
    {
        unset($columns['date'], $columns['shortcode'], $columns['author'], $columns['instances'], $columns['elementor_library_type']);

        return $columns;
    }

    /**
     * Render maintenance bar
     */
    public static function renderMaintenanceMode(): void
    {
        include SUPR_WORDPRESS_PATH . 'templates/maintenance-bar.php';
    }

    /**
     * Check and add maintenance bar
     */
    public static function addMaintenanceModeBar(): void
    {
        if (\get_option('elementor_maintenance_mode_mode', false)) {
            \add_action('wp_footer', [self::class, 'renderMaintenanceMode']);
        }
    }

    /**
     * Add custom CSS to elemento editor
     */
    public static function suprElementorStyle(): void
    {
        \wp_enqueue_style('style-name', SUPR_WORDPRESS_URL . '/scss/elementor.css');
    }
}
