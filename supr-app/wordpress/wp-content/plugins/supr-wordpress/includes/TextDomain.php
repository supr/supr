<?php

namespace SuprWordpress;

class TextDomain
{
    public static $domainName = 'supr-wordpress';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_WORDPRESS_DIR_NAME . '/languages/');
    }
}
