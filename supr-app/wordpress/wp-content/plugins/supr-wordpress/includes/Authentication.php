<?php

namespace SuprWordpress;

/**
 * Class Authentication
 *
 * @package SuprWordpress
 */
class Authentication
{
    /**
     * Check membership and show notice if needed
     */
    public static function checkMembershipOfShop(): void
    {
        if (!\is_user_logged_in() || \is_super_admin()) {
            return;
        }

        if (!\is_user_member_of_blog()) {
            \wc_print_notice(
                __(
                    'This shop belongs to a shop network with many other online shops. We have noticed that there is already an active customer account there, so that the address fields are filled from it. Of course, this data will not be transferred until the purchase is actually made.',
                    'supr-wordpress'
                ),
                'notice'
            );
        }
    }
}
