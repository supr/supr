<?php

namespace SuprWordpress;

/**
 * Class CustomizerManager
 *
 * @package SuprWordpress
 */
class CustomizerManager
{
    /**
     * Register short codes
     */
    public static function addStyleSnippet(): void
    {
        \add_action('customize_controls_print_styles', [self::class, 'suprCustomizerStyleSnippet'], 999);
        \add_action('customize_preview_init', [self::class, 'suprCustomizerPreviewStyleSnippet']);
    }

    /**
     * Print default imprint
     */
    public static function suprCustomizerStyleSnippet(): void
    {
        ?>
        <style>
            #accordion-section-kt_woomail_template {
                display: none !important;
                visibility: hidden !important;
            }
        </style>
        <?php
    }

    public static function suprCustomizerPreviewStyleSnippet(): void
    {
        ?>
        <style>
            #theme-maintenance-warning {
                display: none !important;
                visibility: hidden !important;
            }
        </style>
        <?php
    }
}