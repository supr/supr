<?php

namespace SuprWordpress;

/**
 * Class Plugin
 *
 * @package SuprWordpress
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function getInstance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Admin init
     */
    public function adminInit(): void
    {
        Access::getInstance()->blockPages();

        Access::getInstance()->disablePluginsUpdate();
        Access::getInstance()->disablePluginsDeletion();
        Access::getInstance()->disableThemeUpdate();
        Access::getInstance()->disableThemeDeletion();
        Access::getInstance()->disableWPCoreUpdate();

        \add_action('admin_enqueue_scripts', [ResourceManager::class, 'addAdminResources'], 990);
        AjaxRestEndpoint::addEndPoints();
        NetworkAdminBar::addAdminBar();
        ElementorManager::removePostColumns();
        AdminPageSendCloud::initAdminPage();
        AdminStyle::adminStyleInit();
        \add_action('wp_dashboard_setup', [Dashboard::class, 'removeDashboardMetaBoxes'], 999);
    }

    /**
     * Init
     */
    public function init(): void
    {
        \add_action('wp_head', [ResourceManager::class, 'addFrontendAdminBarStyles']);
        \add_action('wp_before_admin_bar_render', [AdminBar::class, 'changeAdminBarNodes'], 1);
        ElementorManager::addMaintenanceModeBar();
        Manager::addRemoveTypeAttrOfTagsFilter();
        Manager::removePostTypeSupport();
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // Load translations
        \add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        // Create Menu
        Menu::addMenu();
        // Listen all post deletion
        Access::getInstance()->listenPostDeletion();
        // Set blogs to https
        Manager::getInstance()->changeNewBlogToHttps();
        // Listen shortcodes
        ShortCodeManager::registerShortCodes();
        // Modify wordpress customizer
        CustomizerManager::addStyleSnippet();

        \add_action('admin_init', [$this, 'adminInit']);

        \add_action('init', [$this, 'init']);

        \add_action('elementor/editor/before_enqueue_scripts', [ElementorManager::class, 'suprElementorStyle']);

        \add_action('woocommerce_before_checkout_form', [Authentication::class, 'checkMembershipOfShop']);

        // Don't allow to have more than 10 categories or tags per one product
        \add_action('save_post', [Manager::class, 'limitProductTermsCount'], 10, 3);

        // Add a SUPR Footer to Admin
        \add_action('admin_footer', [AdminFooter::class, 'renderAdminFooter']);

        // Add webp as mime type
        \add_filter('mime_types', [Uploads::class, 'addMimeTypes']);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_WORDPRESS_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}
