<?php

namespace SuprWordpress;

/**
 * Class NetworkAdminBar
 *
 * @package SuprWordpress
 */
class NetworkAdminBar
{
    /**
     * Show setting page
     */
    public static function renderAdminBar(): void
    {
        include SUPR_WORDPRESS_PATH . 'templates/network-admin-bar.php';
    }

    /**
     * Add admin bar to footer
     */
    public static function addAdminBar(): void
    {
        \add_action('admin_footer', [self::class, 'renderAdminBar']);
    }
}
