<?php
/**
 * Plugin Name: SUPR - Wordpress Mods
 * Plugin URI: https://supr.com
 * Description: SUPR WordPress modifications.
 * Text Domain: supr-wordpress
 * Domain Path: /languages
 * Version: 2.1.0
 * Author: SUPR
 * Author URI: https://supr.com
 */

define('SUPR_WORDPRESS_PATH', plugin_dir_path(__FILE__));
define('SUPR_WORDPRESS_URL', plugins_url('/', __FILE__));
define('SUPR_WORDPRESS_FILE', plugin_basename(__FILE__));
define('SUPR_WORDPRESS_DIR_NAME', basename(__DIR__));

define('SUPR_WORDPRESS_VERSION', '2.1.0');

// Load main Class
require(SUPR_WORDPRESS_PATH . 'includes/Plugin.php');

\SuprWordpress\Plugin::getInstance();
