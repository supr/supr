<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();
?>
<footer id="supr-admin-footer">
    <div class="row">
        <div class="wu-col-sm-12 wu-col-md-6">
            &copy; Copyrights <?php echo date("Y"); ?> <strong>SUPR</strong> <span class="shop-id"><?= sprintf(__('Shop ID: %d', 'supr-wordpress'), get_current_blog_id()) ?></span>
        </div>
        <div class="wu-col-sm-12 wu-col-md-6 text-right">
            <ul>
                <li><a href="https://supr.network" target="_blank"><?php _e('Website', 'supr-wordpress'); ?></a></li>
                <li><a href="https://help.supr.network" target="_blank"><?php _e('Help', 'supr-wordpress'); ?></a></li>
                <li><a href="https://supr.network/changelog" target="_blank"><?php _e('Changelog', 'supr-wordpress'); ?></a></li>
                <li><a href="https://facebook.com/supr-network" target="_blank">Facebook</a></li>
                <li><a href="https://www.youtube.com/c/SUPR-network" target="_blank">YouTube</a></li>
            </ul>
        </div>
    </div>
</footer>