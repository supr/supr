<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

function supr_settings_general_handle_post_request()
{
    // The $_POST contains all the data sent via ajax
    if (isset($_POST['supr_settings_general'])) {
        $fields = [
            'blogname',
            'blogdescription',
            'blog_public',
            'elementor_maintenance_mode_mode',
            'page_on_front'
        ];

        foreach ($fields as $field) {
            switch ($field) {
                case 'blog_public':
                    update_option($field, isset($_POST[$field]) ? 1 : 0);
                    break;
                case 'elementor_maintenance_mode_mode':
                    update_option($field, !isset($_POST[$field]) ? 1 : 0);
                    break;
                default:
                    update_option($field, sanitize_text_field($_POST[$field]));
                    break;
            }
        }

        // Because we show in main menu Shop status. But there we changed it
        \wp_redirect(admin_url('admin.php?page=supr_wordpress_options&status=success'));
    }

    // Show success message after redirect
    if (isset($_GET['status']) && $_GET['status'] === 'success') : ?>
        <div class="notice notice-success inline">
            <p>
                <?php _e('Your settings have been saved', 'supr-wordpress'); ?>
            </p>
        </div>
    <?php endif;
}

?>

<div class="wrap supr-general-settings">

    <h1 class="wp-heading-inline">
        <?php _e('General settings', 'supr-wordpress'); ?>
    </h1>

    <form method="POST" id="supr-general-settings-form">
        <div class="postbox">
            <div class="inside">
                <?php
                supr_settings_general_handle_post_request();

                $blogname = get_option('blogname');
                $blogdescription = get_option('blogdescription');
                $blog_public = get_option('blog_public');
                $elementor_maintenance_mode_mode = get_option('elementor_maintenance_mode_mode');
                $page_on_front = get_option('page_on_front');
                $admin_email = get_option('admin_email');

                ?>

                <div class="container-fluid">
                    <!-- Blog name and slogan  -->

                    <div class="form-section">
                        <div class="row">
                            <div class="wu-col-sm-12">
                                <h3><?php _e('Storename and slogan', 'supr-wordpress'); ?></h3>
                                <p>
                                    <?php _e('Here you can define the name and a slogan for your store. These are used in different places like in the design or in mails.', 'supr-wordpress'); ?>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="wu-col-sm-12">
                                <table class="form-table">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <label for="blogname">
                                                <?php _e('Storename', 'supr-wordpress'); ?>
                                            </label>
                                        </td>
                                        <td>
                                            <input name="blogname" id="blogname" value="<?= $blogname; ?>" class="regular-text" type="text">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label for="blogdescription">
                                                <?php _e('Slogan', 'supr-wordpress'); ?>
                                            </label>
                                        </td>
                                        <td>
                                            <input name="blogdescription" id="blogdescription" aria-describedby="tagline-description" value="<?= $blogdescription; ?>" class="regular-text" type="text">
                                            <p class="description" id="tagline-description">
                                                <?php _e('Enter the slogan for your store', 'supr-wordpress'); ?>
                                            </p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- Store E-Mail -->

                    <div class="form-section">
                        <div class="row">
                            <div class="wu-col-sm-12">
                                <h3><?php _e('Admin E-Mail', 'supr-wordpress'); ?></h3>
                                <p>
                                    <?= sprintf(
                                        __(
                                            'This address will be used for administrative purposes. This address can be changed only by the store administrator on his <a href="%s">profile page</a>.',
                                            'supr-wordpress'
                                        ),
                                        '/wp-admin/profile.php'
                                    ); ?>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="wu-col-sm-12">
                                <table class="form-table">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <label for="new_admin_email">
                                                <?php _e('Admin E-Mail', 'supr-wordpress'); ?>
                                            </label>
                                        </td>
                                        <td>
                                            <input id="new_admin_email" value="<?= $admin_email; ?>" class="regular-text" type="text" readonly>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- Store public -->

                    <div class="form-section">
                        <div class="row">
                            <div class="wu-col-sm-12">
                                <h3><?php _e('Store opened to public', 'supr-wordpress'); ?></h3>
                                <p>
                                    <?php _e(
                                        'Here you can set whether your store is reachable for your customers. If you deactivate this option, only you as logged in admin can see and edit your store.',
                                        'supr-wordpress'
                                    ); ?>
                                </p>
                            </div>
                        </div>

                        <table class="form-table">
                            <tbody>
                            <tr>
                                <td>
                                    <input type="checkbox" name="elementor_maintenance_mode_mode" value="1" class="elementor_maintenance_mode_mode lc-switch"
                                        <?= $elementor_maintenance_mode_mode ? '' : 'checked'; ?>/>
                                    <label class="label-lc-switch">
                                        <?php _e('My store is opened to public', 'supr-wordpress'); ?>
                                    </label>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <!-- Search engine -->

                    <div class="form-section">
                        <div class="row">
                            <div class="wu-col-sm-12">
                                <h3><?php _e('Allow search engines', 'supr-wordpress'); ?></h3>
                                <p>
                                    <?php _e('Allow search engines like Google or Bing to index your site to find your shop and your products.', 'supr-wordpress'); ?>
                                </p>
                            </div>
                        </div>

                        <table class="form-table">
                            <tbody>
                            <tr>
                                <td>
                                    <input type="checkbox" name="blog_public" value="1" id="blog_public" class="blog_public lc-switch"
                                        <?= $blog_public ? 'checked' : ''; ?>/>
                                    <label class="label-lc-switch">
                                        <?php _e('Allow search engines to find your store', 'supr-wordpress'); ?>
                                    </label>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <!-- Frontpage -->

                    <div class="form-section">
                        <div class="row">
                            <div class="wu-col-sm-12">
                                <h3><?php _e('Frontpage', 'supr-wordpress'); ?></h3>
                                <p>
                                    <?php _e('Select which page should be used as the front page. By default, the page named "Home" is selected.', 'supr-wordpress'); ?>
                                </p>
                            </div>
                        </div>

                        <table class="form-table">
                            <tbody>
                            <tr>
                                <td>
                                    <label for="page_on_front"><?php _e('Select a page', 'supr-wordpress'); ?></label>
                                </td>
                                <td>
                                    <?php
                                    \wp_dropdown_pages(
                                        [
                                            'name' => 'page_on_front',
                                            'child_of' => 0,
                                            'sort_order' => 'ASC',
                                            'sort_column' => 'post_title',
                                            'hierarchical' => 1,
                                            'post_type' => 'page',
                                            'selected' => $page_on_front
                                        ]
                                    );
                                    ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="postbox postbox-bar">
            <div class="inside">
                <input type="submit" name="supr_settings_general" class="button button-primary" value="<?php _e('Save settings', 'supr-wordpress'); ?>"/>
            </div>
        </div>
    </form>
</div>