<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();
?>
<div class="wrap woocommerce">
    <section class="postbox">
        <div class="inside">
            <div class="container-fluid">
                <div class="row">
                    <div class="wu-col-md-7">
                        <h2 class="text-uppercase"><?= __('SENDCLOUD', 'supr-wordpress') ?></h2>
                        <p>
                            <?= __('All-in-One shipping solution', 'supr-wordpress') ?>
                        </p>
                        <p>
                            <strong>
                                <?= __('With SendCloud you can now connect your preferred shipping services and send your orders worldwide.', 'supr-wordpress') ?>
                            </strong>
                        </p>
                        <p>
                            <?= __(
                                'More than 15,000 e-commerce companies use SendCloud to optimize their shipping processes. With a simple connection to your SUPR shop, your orders are automatically transferred and you can easily prepare for shipping.',
                                'supr-wordpress'
                            ) ?>
                        </p>
                        <h2><?= __('How to connect with SendCloud?', 'supr-wordpress') ?></h2>
                        <ul>
                            <li><?= sprintf(__('Create or log in to your SendCloud account <a href="%s" target="_blank">here</a>.', 'supr-wordpress'), 'https://panel.sendcloud.sc') ?></li>
                            <li><?= sprintf(__('Add your sender address in your SendCloud account (Settings -> Addresses -> Sender address -> Add new) <a href="%s" target="_blank">here</a>.', 'supr-wordpress'), 'https://panel.sendcloud.sc/#/settings/addresses/sender') ?></li>
                            <li><?= __('After successfully completing the steps above, click on the "Connect to SendCloud" button below.') ?></li>
                        </ul>
                    </div>
                    <div class="wu-col-md-5">
                        <img src="<?php echo SUPR_WORDPRESS_URL; ?>img/sendcloud.svg" alt="Sendcloud">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="postbox postbox-bar">
        <div class="inside">
            <div class="row">
                <div class="wu-col-md-6 text-left">
                    <input type="submit" name="connect" class="button button-primary" value="<?php _e('Connect to SendCloud', 'supr-wordpress'); ?>">
                </div>
                <div class="wu-col-md-6 text-right">
                    <a class="button button-secondary" href="https://panel.sendcloud.sc" target="_blank" rel="noopener noreferrer"><?php _e('Go to SendCloud', 'supr-wordpress'); ?></a>
                </div>
            </div>

        </div>
    </section>
</div>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'bottom',
            showConfirmButton: false,
            timer: 200000,
            customClass: 'animated bounceInUp',
            showClass: {
                popup: '',
                backdrop: '',
                icon: '',
            },
            hideClass: {
                popup: '',
                backdrop: '',
                icon: '',
            }
        });

        Toast.fire({
            icon: 'info',
            html: '<div style="text-align: left; font-size: 70%; margin-left: .625em;"><?= __(
                "Connect your shop to SendCloud and all your new orders will be transferred automatically. This page will not change after successfully connecting to SendCloud - you can see in your SendCloud account if you are connected to your shop.",
                "supr-wordpress"
            ) ?></div>'
        });

    });
</script>
