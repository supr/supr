<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();
?>
<div class="wrap">
    <h2><?= get_admin_page_title(); ?></h2>
    <?php
    // Handle request
    if (isset($_POST['supr_wordpress_options'])) {
        $options = [];

        // Get and process protected pages
        $options['protected_page_ids'] = \sanitize_text_field($_POST['supr_wordpress_options']['protected_page_ids']);
        $options['protected_page_ids'] = explode(',', $options['protected_page_ids']);
        $options['protected_page_ids'] = array_map('trim', $options['protected_page_ids']);
        $options['protected_page_ids'] = array_map('intval', $options['protected_page_ids']);
        $options['protected_page_ids'] = array_filter($options['protected_page_ids']);

        // Get and process protected templates
        $options['protected_template_ids'] = \sanitize_text_field($_POST['supr_wordpress_options']['protected_template_ids']);
        $options['protected_template_ids'] = explode(',', $options['protected_template_ids']);
        $options['protected_template_ids'] = array_map('trim', $options['protected_template_ids']);
        $options['protected_template_ids'] = array_map('intval', $options['protected_template_ids']);
        $options['protected_template_ids'] = array_filter($options['protected_template_ids']);

        // Get and process blocked templates
        $options['blocked_template_ids'] = \sanitize_text_field($_POST['supr_wordpress_options']['blocked_template_ids']);
        $options['blocked_template_ids'] = explode(',', $options['blocked_template_ids']);
        $options['blocked_template_ids'] = array_map('trim', $options['blocked_template_ids']);
        $options['blocked_template_ids'] = array_map('intval', $options['blocked_template_ids']);
        $options['blocked_template_ids'] = array_filter($options['blocked_template_ids']);

        // Checkbox
        $options['allow_network_admin'] = filter_var($_POST['supr_wordpress_options']['allow_network_admin'], FILTER_VALIDATE_BOOLEAN);

        update_site_option('supr_wordpress_options', $options);
    }

    $options = \get_site_option('supr_wordpress_options', []);
    ?>

    <form action="" method="POST">
        <table class="form-table">
            <tr>
                <td><label for="supr_wordpress_options_protected_page_ids"><?= __('IDs of protected pages', 'supr-wordpress'); ?></label></td>
                <td><input type="text" id="supr_wordpress_options_protected_page_ids" placeholder="1,2,3" name="supr_wordpress_options[protected_page_ids]" value="<?= $options['protected_page_ids'] ? implode(',', $options['protected_page_ids']) : '' ?>"/></td>
                <td>
                    <p><?= sprintf(__('These pages cannot be deleted or set as junk. You can find the pages in each blog there:<br><code>%s</code>', 'supr-wordpress'), '/wp-admin/edit.php?post_type=page') ?></p>
                </td>
            </tr>
            <tr>
                <td><label for="supr_wordpress_options_protected_template_ids"><?= __('IDs of protected templates', 'supr-wordpress'); ?></label></td>
                <td><input type="text" id="supr_wordpress_options_protected_template_ids" placeholder="1,2,3" name="supr_wordpress_options[protected_template_ids]" value="<?= $options['protected_template_ids'] ? implode(',', $options['protected_template_ids']) : '' ?>"/></td>
                <td>
                    <p><?= sprintf(__('These templates cannot be deleted or set as junk. You can find the templates in each blog there:<br><code>%s</code>', 'supr-wordpress'), '/wp-admin/edit.php?post_type=elementor_library') ?></p>
                </td>
            </tr>
            <tr>
                <td><label for="supr_wordpress_options_blocked_template_ids"><?= __('IDs of blocked templates', 'supr-wordpress'); ?></label></td>
                <td><input type="text" id="supr_wordpress_options_blocked_template_ids" placeholder="1,2,3" name="supr_wordpress_options[blocked_template_ids]" value="<?= $options['blocked_template_ids'] ? implode(',', $options['blocked_template_ids']) : '' ?>"/></td>
                <td>
                    <p><?= sprintf(__('These templates cannot be changed. You can find the templates in each blog there:<br><code>%s</code>', 'supr-wordpress'), '/wp-admin/edit.php?post_type=elementor_library') ?></p>
                </td>
            </tr>
            <tr>
                <td><label for="supr_wordpress_options_allow_network_admin"><?= __('Allow grand access for network administrators', 'supr-wordpress'); ?></label></td>
                <td><input type="checkbox" value="1" id="supr_wordpress_options_allow_network_admin" name="supr_wordpress_options[allow_network_admin]"<?= $options['allow_network_admin'] ? ' checked' : '' ?>/></td>
                <td>
                    <p><?= __('Allow network administrator to manage all pages without restrictions.', 'supr-wordpress') ?></p>
                </td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
</div>