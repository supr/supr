<?php

// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$supr_wordpress_current_user = \wp_get_current_user();

if (is_super_admin($supr_wordpress_current_user->ID)) :
    $supr_wordpress_current_blog_id = get_current_blog_id();
    $supr_wordpress_current_admin_email = get_option('admin_email');
    $supr_wordpress_current_blog_user = get_user_by('email', $supr_wordpress_current_admin_email);

    $wu_site = wu_get_site(get_current_blog_id());
    $wu_current_plan = $wu_site->get_plan() ? $wu_site->get_plan()->title : false;

    $supr_wordpress_current_screen = get_current_screen();
    ?>
    <ul id="networkadmin-bar" class="networkadmin-bar networkadmin-bar-hidden">
        <li>
            <span class="dashicons dashicons-menu" id="toggle-networkadmin-bar"></span>
        </li>
        <li>
            Hey <?= $supr_wordpress_current_user->user_firstname; ?> ;)
        </li>
        <li>
            Shop ID: <?= $supr_wordpress_current_blog_id; ?>
        </li>
        <li>
            Shopname: <?= get_option('blogname'); ?>
        </li>
        <li>
            Merchant: <?= get_option('woocommerce_store_owner_first_name'); ?> <?= get_option('woocommerce_store_owner_last_name'); ?>
        </li>
        <li>
            Subscripton: <?= $wu_current_plan ?? 'none'; ?>
        </li>
        <?php if ($wu_current_plan && $supr_wordpress_current_blog_user) : ?>
            <li>
                <a href="https://<?= DOMAIN_CURRENT_SITE; ?>/wp-admin/network/admin.php?page=wu-edit-subscription&user_id=<?= $supr_wordpress_current_blog_user->ID ?>" target="_blank">
                    Edit subscription
                </a>
            </li>
        <?php endif; ?>
        <li>
            <a href="https://<?= DOMAIN_CURRENT_SITE; ?>/wp-admin/network/site-info.php?id=<?= $supr_wordpress_current_blog_id; ?>" target="_blank">
                Edit Network
            </a>
        </li>
        <li>
            <a href="https://supr-wcd.freshdesk.com/a/search/customers?term=<?= $supr_wordpress_current_admin_email ?>" target="_blank">
                Freshdesk
            </a>
        </li>
        <li data-tippy="I'm a tooltip!">
            Screen: <?= $supr_wordpress_current_screen->id; ?>
        </li>
    </ul>

<?php endif;

