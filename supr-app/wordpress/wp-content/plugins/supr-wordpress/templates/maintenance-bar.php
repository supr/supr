<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();
?>
<a id="theme-maintenance-warning" class="theme-maintenance-warning" href="/wp-admin/admin.php?page=supr_wordpress_options">
    <strong><?php _e('Maintenance mode active!', 'supr-wordpress'); ?></strong>
    <?php _e('Customers can\'t visit your store or buy products. Click here to change status!', 'supr-wordpress'); ?>
</a>