jQuery(document).ready(function ($) {

    // Toggle Network Admin bar
    $('#toggle-networkadmin-bar').on('click', function () {
        $('#networkadmin-bar').toggleClass('networkadmin-bar-hidden');
    });

    // Cache clear click
    $('#wp-admin-bar-supr-clear-cache').on('click', function () {
        Swal.showLoading();
        $.post(ajaxurl, {'action': 'supr_clear_cache'}).done(function (data) {
                Swal.close();
                data = $.parseJSON(data);
                // Show succes message
            if (data.success) {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    text: supr_wordpress_translation.swal_clear_cache_success_message,
                    showConfirmButton: false,
                    timer: 2000
                    });
            }
        }).error(function () {
            Swal.close();
        });
    });

    // Remove elements with java script and not just css
    var remove_elements = [
        'body.shop_page_wpo_wcpdf_options_page .wcpdf-extensions-ad',
        'body.shop_page_wpo_wcpdf_options_page .nav-tab-debug',
        'div#wp-ultimo-actions'
    ];

    $.each(remove_elements, function (i, val) {
        $(val).remove();
    });

    // Tooltip with tippy
    tippy('.tippy', {
        delay: 100,
        arrow: true,
        arrowType: 'sharp',
        distance: 20,
        size: 'small',
        theme: 'supr-tippy'
    });
});