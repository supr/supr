jQuery(document).ready(function ($) {

    //LS Switch
    $('.lc-switch').lc_switch(
        supr_wordpress_translation.lc_switch_on,
        supr_wordpress_translation.lc_switch_off
    );

    // Listen switch
    jQuery('body').delegate('.lc-switch', 'lcs-statuschange', function () {

        var field = jQuery(this).attr('name');
        var status = jQuery(this).is(':checked') ? 1 : null;

        // If checked: it means maintenance mode deactivated -> 0
        if (field === 'elementor_maintenance_mode_mode') {
            status = (status === null) ? 1 : null;
        }

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            showClass: {
                popup: '',
                backdrop: '',
                icon: '',
            },
            hideClass: {
                popup: '',
                backdrop: '',
                icon: '',
            }
        });

        console.log(supr_wordpress_translation.swal_instant_save_wait);

        Toast.fire({
            icon: 'info',
            title: supr_wordpress_translation.swal_instant_save_wait
        });

        jQuery.ajax({
            type: 'POST',
            url: ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
            data: {
                'action': 'supr_settings_general_handle_ajax_request',
                'field': field,
                'value': status
            },
            success: function (data) {
                Toast.fire({
                    icon: 'success',
                    title: supr_wordpress_translation.swal_instant_save_success
                });
            },
            error: function (errorThrown) {
                Toast.fire({
                    icon: 'error',
                    title: errorThrown
                });
            }
        });
    });
});