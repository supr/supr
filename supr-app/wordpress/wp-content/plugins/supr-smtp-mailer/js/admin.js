jQuery(document).ready(function () {
    "use strict";

    var verify_button = jQuery('<a />').addClass('button-primary').css('margin-left', '5px').html(supr_smtp_mailer_values.validate_button_text);
    verify_button.insertAfter("#woocommerce_store_owner_email").click(function () {
        Swal.fire({
            title: supr_smtp_mailer_values.swal_title,
            text: supr_smtp_mailer_values.swal_text,
            icon: 'question',
            showCancelButton: true,
            cancelButtonText: supr_smtp_mailer_values.swal_cancel,
            confirmButtonText: supr_smtp_mailer_values.swal_confirm
        }).then(function (result) {
            if (result.value) {
                var data = {
                    emailAddress: jQuery('#woocommerce_store_owner_email').val()
                };

                jQuery.ajax({
                    url: '/wp-json/supr-smtp-mailer-rest/v1/validate-email',
                    data: data,
                    type: 'POST',
                    beforeSend: function (xhr) {
                        // Add cookies to check auth
                        xhr.setRequestHeader('X-WP-Nonce', supr_smtp_mailer_values.nonce);

                        // Show loading
                        Swal.fire({
                            title: supr_smtp_mailer_values.swal_loading_title,
                            html: supr_smtp_mailer_values.swal_loading_text,
                            onOpen: function () {
                                Swal.showLoading();
                            }
                        });
                    },
                    success: function (data) {
                        Swal.fire(
                            supr_smtp_mailer_values.swal_success_title,
                            data.data !== undefined && data.data.message !== undefined ? data.data.message : supr_smtp_mailer_values.swal_success_description,
                            'success'
                        ).then(function () {
                            // Save the entered data
                            $('#mainform').find('[type="submit"]').click();
                        });
                    },
                    error: function (xhr, status, error) {
                        // Get data
                        var data = {};
                        if (xhr.responseJSON !== undefined) {
                            data = xhr.responseJSON.data;
                        }

                        Swal.fire(
                            supr_smtp_mailer_values.swal_error_title,
                            data.message !== undefined ? data.message : supr_smtp_mailer_values.swal_error_description,
                            'error'
                        );
                    }
                });
            }
        })
    });
});