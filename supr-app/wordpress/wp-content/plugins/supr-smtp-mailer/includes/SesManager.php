<?php

namespace SuprSmtpMailer;

use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

/**
 * Class SesManager
 *
 * @package SuprSmtpMailer
 */
class SesManager
{
    /**
     * @var null|SesManager
     */
    private static $instance;

    /**
     * @var \Aws\Ses\SesClient
     */
    private $sesClient;

    private $verificationTemplateName = 'SuprVerificationEmailAddressTemplate';

    /**
     * @return \SuprSmtpMailer\SesManager
     */
    public static function instance(): SesManager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        $params = ['version' => '2010-12-01', 'region' => 'eu-west-1'];

        $this->sesClient = new SesClient($params);
    }

    /**
     * Get all verified email addresses
     *
     * @return null|array
     */
    public function getVerifiedEmailAddresses(): ?array
    {
        $returned = null;

        try {
            $result = $this->sesClient->listVerifiedEmailAddresses([]);
            $returned = $result->get('VerifiedEmailAddresses');
        } catch (\Exception $e) {
            error_log('[SUPR SMTP MAILER] I cannot get verified email addresses: ' . $e->getMessage());
        }

        return $returned;
    }

    /**
     * Check if email address is verified.
     *
     * @param $emailAddress
     * @return bool
     */
    public function isEmailAddressVerified($emailAddress): bool
    {
        $verifiedEmails = $this->getVerifiedEmailAddresses() ?? [];

        return \in_array($emailAddress, $verifiedEmails, true);
    }

    /**
     * @param      $emailAddress
     * @param bool $withTemplate
     * @return bool
     */
    public function verifyEmailAddress($emailAddress, $withTemplate = true): bool
    {
        if (!\filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
            error_log('[SUPR SMTP MAILER] Email addresse is not valid: ' . $emailAddress);

            return false;
        }

        $emailAddress = \strtolower($emailAddress);

        try {
            if ($withTemplate && !empty($this->getCustomVerificationEmailTemplate())) {
                // With custom template
                $this->sesClient->sendCustomVerificationEmail(['EmailAddress' => $emailAddress, 'TemplateName' => $this->verificationTemplateName]);
            } else {
                // Default
                $this->sesClient->verifyEmailAddress(['EmailAddress' => $emailAddress]);
            }

            return true;
        } catch (AwsException $e) {
            // In this case we will return empty array
            error_log('[SUPR SMTP MAILER] I cannot verify email address ' . $emailAddress . ': ' . $e->getAwsErrorCode());
        } catch (\Exception $e) {
            error_log('[SUPR SMTP MAILER] I cannot verify email address ' . $emailAddress . ': ' . $e->getMessage());
        }

        return false;
    }

    /**
     * @param $emailAddress
     * @return bool
     */
    public function deleteVerifiedEmailAddress($emailAddress): bool
    {
        if (!\filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
            error_log('[SUPR SMTP MAILER] Email addresse is not valid: ' . $emailAddress);

            return false;
        }

        try {
            $this->sesClient->deleteVerifiedEmailAddress(['EmailAddress' => $emailAddress]);

            return true;
        } catch (\Exception $e) {
            error_log('[SUPR SMTP MAILER] I cannot delete verified email address ' . $emailAddress . ': ' . $e->getMessage());
        }

        return false;
    }

    /**
     * Get custom verification email template
     *
     * @return array
     */
    public function getCustomVerificationEmailTemplate(): array
    {
        try {
            $result = $this->sesClient->getCustomVerificationEmailTemplate(['TemplateName' => $this->verificationTemplateName]);

            return $result->toArray();
        } catch (AwsException $e) {
            // In this case we will return empty array
            if ($e->getAwsErrorCode() !== 'CustomVerificationEmailTemplateDoesNotExist') {
                error_log('[SUPR SMTP MAILER] I cannot get custom verification email template by name ' . $this->verificationTemplateName . ': ' . $e->getAwsErrorCode());
            }
        } catch (\Exception $e) {
            error_log('[SUPR SMTP MAILER] I cannot get custom verification email template by name ' . $this->verificationTemplateName . ': ' . $e->getMessage());
        }

        return [];
    }

    /**
     * Create custom verification email template
     *
     * @param $data
     * @return bool
     */
    private function createCustomVerificationEmailTemplate($data): bool
    {
        $data['TemplateName'] = $this->verificationTemplateName;

        try {
            $this->sesClient->createCustomVerificationEmailTemplate($data);

            return true;
        } catch (AwsException $e) {
            error_log("[SUPR SMTP MAILER] I cannot create custom verification email template. Data:\n" . print_r($data, true) . "\nError: " . $e->getAwsErrorCode());
        } catch (\Exception $e) {
            error_log("[SUPR SMTP MAILER] I cannot create custom verification email template. Data:\n" . print_r($data, true) . "\nError: " . $e->getMessage());
        }

        return false;
    }

    /**
     * Update custom verification email template
     *
     * @param $data
     * @return bool
     */
    public function updateCustomVerificationEmailTemplate($data): bool
    {
        $data['TemplateName'] = $this->verificationTemplateName;

        try {
            $this->sesClient->updateCustomVerificationEmailTemplate($data);

            return true;
        } catch (AwsException $e) {
            if ($e->getAwsErrorCode() === 'CustomVerificationEmailTemplateDoesNotExist') {
                return $this->createCustomVerificationEmailTemplate($data);
            }

            error_log("[SUPR SMTP MAILER] I cannot update custom verification email template. Data:\n" . print_r($data, true) . "\nError: " . $e->getStatusCode());
        } catch (\Exception $e) {
            error_log("[SUPR SMTP MAILER] I cannot update custom verification email template. Data:\n" . print_r($data, true) . "\nError: " . $e->getMessage());
        }

        return false;
    }

    /**
     * Delete custom verification email template
     *
     * @return bool
     */
    public function deleteCustomVerificationEmailTemplate(): bool
    {
        try {
            $this->sesClient->deleteCustomVerificationEmailTemplate(['TemplateName' => $this->verificationTemplateName]);

            return true;
        } catch (AwsException $e) {
            error_log('[SUPR SMTP MAILER] I cannot delete custom verification email template ' . $this->verificationTemplateName . "\nError: " . $e->getStatusCode());
        } catch (\Exception $e) {
            error_log('[SUPR SMTP MAILER] I cannot delete custom verification email template ' . $this->verificationTemplateName . "\nError: " . $e->getMessage());
        }

        return false;
    }
}
