<?php

namespace SuprSmtpMailer;

class TextDomain
{
    public static $domainName = 'supr-smtp-mailer';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_SMTP_MAILER_DIR_NAME . '/languages/');
    }
}
