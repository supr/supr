<?php

namespace SuprSmtpMailer;

/**
 * Class Plugin
 *
 * @package SuprSmtpMailer
 */
class Plugin
{
    /**
     * @var null
     */
    public static $instance;

    /**
     * @return \SuprSmtpMailer\Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();

            //Fires when SuprSmtpMailer was fully loaded and instantiated.
            do_action('supr-smtp-mailer/loaded');
        }

        return self::$instance;
    }

    /**
     * Init
     */
    public function adminInit(): void
    {
        // Add js and css files
        ResourceManager::addResources();

        // Add variables to JS script
        ResourceManager::addJsVariables();

        // Action after the plugin was loaded
        do_action('supr-smtp-mailer/init');
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // Register translations
        TextDomain::registerTranslations();

        // Add menu
        Menu::addMenu();

        // Load translations
        add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        // Do all configurations for SMTP mailer
        Manager::instance()->configureSmtpMails();

        // Register end points
        Api::registerRoutes();

        add_action('admin_init', [$this, 'adminInit'], 0);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_SMTP_MAILER_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::instance();
