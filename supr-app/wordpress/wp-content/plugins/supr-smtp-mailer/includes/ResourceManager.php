<?php

namespace SuprSmtpMailer;

/**
 * Class ResourceManager
 *
 * @package SuprSmtpMailer
 */
class ResourceManager
{
    /**
     * Register files for plugin
     */
    public static function addResources(): void
    {
        // JS
        \wp_register_script('supr_smtp_mailer_js', SUPR_SMTP_MAILER_URL . 'js/admin.js', ['jquery'], SUPR_SMTP_MAILER_VERSION, true);
        // Add css and js into footer
        \add_action('admin_enqueue_scripts', [__CLASS__, 'enqueueUpsaleFiles'], 0);
    }

    /**
     * Add files
     */
    public static function enqueueUpsaleFiles(): void
    {
        \wp_enqueue_script('supr_smtp_mailer_js');
    }

    /**
     * Add js variables to js script
     */
    public static function addJsVariables(): void
    {
        $array = [
            'validate_button_text' => __('Verify email', 'supr-smtp-mailer'),
            'swal_title' => __('Do you want to verify your email address?', 'supr-smtp-mailer'),
            'swal_text' => __('We will send  you a confirmation mail. Click on the link and your customers will receive all mails from your email address.', 'supr-smtp-mailer'),
            'swal_cancel' => __('Cancel', 'supr-smtp-mailer'),
            'swal_confirm' => __('Verify', 'supr-smtp-mailer'),
            'swal_success_title' => __('Verification mail was sent', 'supr-smtp-mailer'),
            'swal_success_description' => __('Please check you mail box.', 'supr-smtp-mailer'),
            'swal_error_title' => __('Something went wrong', 'supr-smtp-mailer'),
            'swal_error_description' => __('Please try again or contact our support.', 'supr-smtp-mailer'),
            'swal_loading_title' => __('Waiting...', 'supr-smtp-mailer'),
            'swal_loading_text' => __('We are sending the mail...', 'supr-smtp-mailer'),
            'nonce' => \wp_create_nonce('wp_rest'),
        ];

        \wp_localize_script('supr_smtp_mailer_js', 'supr_smtp_mailer_values', $array);
    }
}
