<?php

namespace SuprSmtpMailer;

class Api
{
    public static function registerRoutes(): void
    {
        add_action(
            'rest_api_init',
            function () {
                register_rest_route(
                    'supr-smtp-mailer-rest/v1',
                    '/validate-email',
                    [
                        'methods' => 'POST',
                        'callback' => [self::class, 'verifyEmail'],
                        'permission_callback' => function () {
                            return \current_user_can('manage_options');
                        }
                    ]
                );
            }
        );
    }

    /**
     * Verify email
     */
    public static function verifyEmail(): void
    {
        if (!isset($_POST['emailAddress']) || !\filter_var($_POST['emailAddress'], FILTER_VALIDATE_EMAIL)) {
            \wp_send_json_error(['message' => __('Please provide a valid email address.', 'supr-smtp-mailer')], 400);
        }

        if (\in_array(\strtolower($_POST['emailAddress']), SesManager::instance()->getVerifiedEmailAddresses(), true)) {
            \wp_send_json_success(['message' => __('This email is already verified.', 'supr-smtp-mailer')], 200);
        }

        if (SesManager::instance()->verifyEmailAddress($_POST['emailAddress'])) {
            \wp_send_json_success();
        }

        \wp_send_json_error(['message' => __('The email address cannot be verified.', 'supr-smtp-mailer')], 400);
    }
}
