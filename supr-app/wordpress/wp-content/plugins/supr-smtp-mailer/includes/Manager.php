<?php

namespace SuprSmtpMailer;

/**
 * Class Manager
 *
 * @package SuprSmtpMailer
 */
class Manager
{
    /**
     * @var null|Manager
     */
    private static $instance;

    /**
     * @return \SuprSmtpMailer\Manager
     */
    public static function instance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Configure php mailer
     */
    public function configureSmtpMails(): void
    {
        // Rewrite default sender
        add_action('phpmailer_init', [$this, 'configureSmtpMail'], 999);

        // Deactivate regexp validation: trying to resolve a bug #S2-673. See also https://stackoverflow.com/questions/52336131/why-wont-i-send-an-email-thru-wordpress-when-i-can-send-it-thru-a-php-script
        add_filter('wp_mail_from', [$this, 'deactivateRegexpValidation'], 1);

        // Replace from address if not verified
        add_filter('wp_mail_from', [$this, 'checkVerification'], 999);

        // Write log if error
        add_action('wp_mail_failed', [$this, 'writeErrorLog'], 10, 1);
    }

    /**
     * @param \PHPMailer $phpmailer
     * @return \PHPMailer
     */
    public function configureSmtpMail($phpmailer): \PHPMailer
    {
        $options = get_blog_option(1, 'supr_smtp_mailer_options');

        if ($options !== null) {
            $phpmailer->isSMTP();
            $phpmailer->SMTPAuth = true;
            $phpmailer->Host = $options['host'];
            $phpmailer->Port = $options['port'];
            $phpmailer->Username = $options['user'];
            $phpmailer->Password = $options['pass'];
            $phpmailer->SMTPSecure = $options['secure'];
            $phpmailer->AuthType = 'PLAIN';
            // Remove X-Mailer PHPMailer headers
            $phpmailer->XMailer = ' ';

            // We set it, because our application should work, if the mail server not reachable...
            $phpmailer->Timeout = 10;

            // @todo delete it, it is DEBUG
            //$phpmailer->SMTPDebug = 3;
        }

        return $phpmailer;
    }

    /**
     * @param $from
     * @return mixed
     */
    public function deactivateRegexpValidation($from)
    {
        \PHPMailer::$validator = 'noregex';

        return $from;
    }

    /**
     * @param $from
     * @return mixed
     */
    public function checkVerification($from)
    {
        $sesVerificationOptions = get_blog_option(1, 'supr_smtp_mailer_ses_options', []);
        $sesAllowOwnEmailAddress = (isset($sesVerificationOptions['active_validation']) && !empty($sesVerificationOptions['active_validation']) && (int)$sesVerificationOptions['active_validation'] === 1);

        // Check verified email of customer and add to from
        if ($sesAllowOwnEmailAddress === false || !SesManager::instance()->isEmailAddressVerified($from)) {
            //\error_log("[SUPR SMTP-MAILER] DEBUG: email address {$from} is not verified.");
            // Replace email of customer
            $sesSmtpOptions = get_blog_option(1, 'supr_smtp_mailer_options', []);
            if (isset($sesSmtpOptions['from']) && !empty($sesSmtpOptions['from'])) {
                $from = $sesSmtpOptions['from'];
            }
        }

        return $from;
    }

    /**
     * @param \WP_Error $wpError
     */
    public function writeErrorLog($wpError): void
    {
        // We don't need in Logs the body of message: too big
        $errorData = $wpError->get_error_data();
        if (\is_array($errorData) && isset($errorData['message'])) {
            unset($errorData['message']);
        }

        error_log('[SUPR SMTP-MAILER] Error: #' . $wpError->get_error_code() . ', ' . $wpError->get_error_message() . "\nData:\n" . print_r($errorData, true));
    }
}
