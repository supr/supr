<?php

namespace SuprSmtpMailer;

/**
 * Class Menu
 *
 * @package SuprSmtpMailer
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add network menu page
        add_action('network_admin_menu', [__CLASS__, 'createNetworkMenu']);

        // Add settings link on plugin page
        add_filter('network_admin_plugin_action_links_' . SUPR_SMTP_MAILER_FILE, [__CLASS__, 'addNetworkSettingsPage']);
    }

    /**
     * Add menu to network admin panel
     */
    public static function createNetworkMenu(): void
    {
        add_menu_page(
            __('SMTP Mailer', 'supr-smtp-mailer'),
            __('SMTP Mailer', 'supr-smtp-mailer'),
            'manage_network',
            'supr_smtp_mailer_settings',
            [Page::class, 'networkSettingsPage'],
            'dashicons-email',
            205
        );
    }

    /**
     * @param $links
     * @return mixed
     */
    public static function addNetworkSettingsPage($links)
    {
        $settings_link = '<a href="admin.php?page=supr_smtp_mailer_settings">' . __('Settings', 'supr-smtp-mailer') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
