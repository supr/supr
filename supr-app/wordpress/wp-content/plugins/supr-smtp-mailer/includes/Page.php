<?php

namespace SuprSmtpMailer;

/**
 * Class Page
 *
 * @package SuprSmtpMailer
 */
class Page
{
    /**
     * Show setting page
     */
    public static function networkSettingsPage(): void
    {
        include SUPR_SMTP_MAILER_PATH . 'templates/network-settings-page.php';
    }
}
