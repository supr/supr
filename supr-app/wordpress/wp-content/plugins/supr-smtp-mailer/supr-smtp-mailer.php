<?php
/**
 * Plugin Name: SUPR - SMTP Mailer
 * Plugin URI: https://supr.com
 * Description: Integration of SMTP for emails
 * Text Domain: supr-smtp-mailer
 * Domain Path: /languages
 * Version: 1.0.0
 * Author: SUPR Development
 * License: GPL
 */

define('SUPR_SMTP_MAILER_PATH', plugin_dir_path(__FILE__));
define('SUPR_SMTP_MAILER_URL', plugins_url('/', __FILE__));
define('SUPR_SMTP_MAILER_FILE', plugin_basename(__FILE__));
define('SUPR_SMTP_MAILER_DIR_NAME', basename(__DIR__));

define('SUPR_SMTP_MAILER_VERSION', '1.0.0');

// Load main Class
require SUPR_SMTP_MAILER_PATH . 'includes/Plugin.php';
