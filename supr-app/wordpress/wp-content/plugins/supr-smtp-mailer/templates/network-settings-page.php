<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$tabs = [
    'smtp-settings' => __('SMTP Settings', 'supr-smtp-mailer'),
    'test-send-mail' => __('Test mail', 'supr-smtp-mailer'),
    'verification-emails' => __('Verification of emails', 'supr-smtp-mailer')
];

$currentTab = (isset($_GET['tab']) && isset($tabs[$_GET['tab']])) ? $_GET['tab'] : 'smtp-settings';
?>

<div class="wrap supr-smtp-mailer">
    <h2><?= get_admin_page_title(); ?></h2>
    <h2 class="nav-tab-wrapper">
        <?php foreach ($tabs as $tab => $tabName) : ?>
            <a class="nav-tab<?= $currentTab === $tab ? ' nav-tab-active' : ''; ?>" href="?page=<?= $_GET['page']?>&tab=<?= $tab; ?>"><?= $tabName; ?></a>
        <?php endforeach; ?>
    </h2>

    <?php
    // Include tabs template
    include SUPR_SMTP_MAILER_PATH . "templates/settings-tabs/{$currentTab}.php";
    ?>
</div>