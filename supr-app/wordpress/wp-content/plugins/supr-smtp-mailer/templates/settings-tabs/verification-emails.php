<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$messages = [];

// Handle request with options
if (isset($_POST['supr_smtp_mailer_ses_options'])) {
    $newOptions = $_POST['supr_smtp_mailer_ses_options'];

    // Trim all values
    $newOptions = array_map('trim', $newOptions);
    // Remove empty values
    $newOptions = array_filter($newOptions);
    // Sanitize values with WP special function
    $newOptions = array_map('sanitize_text_field', $newOptions);

    update_option('supr_smtp_mailer_ses_options', $newOptions);

    $messages[] = ['type' => 'success', 'text' => 'Options were saved.'];
}

// Validate new email
if (isset($_POST['new_email_address']) && !empty($_POST['new_email_address'])) {
    if (!filter_var($_POST['new_email_address'], FILTER_VALIDATE_EMAIL)) {
        $messages[] = ['type' => 'error', 'text' => 'Email is not valid'];
    } elseif (\SuprSmtpMailer\SesManager::instance()->verifyEmailAddress($_POST['new_email_address'])) {
        $messages[] = ['type' => 'success', 'text' => 'Email for verification was sent.'];
    } else {
        $messages[] = ['type' => 'error', 'text' => 'Email for verification was not sent. See log for details.'];
    }
}

// Update custom email verification template
if (isset($_POST['template_content'])) {
    if (empty($_POST['template_content'])) {
        // Delete current template
        if (\SuprSmtpMailer\SesManager::instance()->deleteCustomVerificationEmailTemplate()) {
            $messages[] = ['type' => 'success', 'text' => 'Template was deleted.'];
        } else {
            $messages[] = ['type' => 'error', 'text' => 'Template was not deleted. See log for details.'];
        }
    } else {
        // Update current template
        $data = [
            'FailureRedirectionURL' => sanitize_text_field($_POST['failure_redirection_url']),
            'FromEmailAddress' => sanitize_text_field($_POST['from_email_address']),
            'SuccessRedirectionURL' => sanitize_text_field($_POST['success_redirection_url']),
            'TemplateContent' => \stripslashes(sanitize_textarea_field($_POST['template_content'])),
            'TemplateSubject' => sanitize_text_field($_POST['template_subject'])
        ];



        if (!filter_var($data['FromEmailAddress'], FILTER_VALIDATE_EMAIL)) {
            $messages[] = ['type' => 'error', 'text' => 'Email is not valid'];
        } elseif (\SuprSmtpMailer\SesManager::instance()->updateCustomVerificationEmailTemplate($data)) {
            $messages[] = ['type' => 'success', 'text' => 'Template was saved.'];
        } else {
            $messages[] = ['type' => 'error', 'text' => 'Template was not saved. See log for details.'];
        }
    }
}

// Delete email address
if (isset($_GET['delete_email_address']) && !empty($_GET['delete_email_address'])) {
    if (!filter_var($_GET['delete_email_address'], FILTER_VALIDATE_EMAIL)) {
        $messages[] = ['type' => 'error', 'text' => 'Email is not valid'];
    } elseif (\SuprSmtpMailer\SesManager::instance()->deleteVerifiedEmailAddress($_GET['delete_email_address'])) {
        $messages[] = ['type' => 'success', 'text' => 'Email was deleted.'];
    } else {
        $messages[] = ['type' => 'error', 'text' => 'Email was not deleted. See log for details.'];
    }
}

// Errors / Successes output
foreach ($messages as $message) {
    ?>
    <div class="notice notice-<?= $message['type']; ?> is-dismissible">
        <p><?= __($message['text'], 'supr-smtp-mailer'); ?></p>
    </div>
    <?php
}

$options = get_option('supr_smtp_mailer_ses_options', []);
?>

<form action="" method="POST">
    <!-- If you delete it, than you cannot deactivate checkbox :) -->
    <input type="hidden" name="supr_smtp_mailer_ses_options[test]" value="1" />
    <table class="form-table">
        <tr>
            <th colspan="3"><?= __('Own email addresses', 'supr-smtp-mailer'); ?></th>
        </tr>
        <tr>
            <td><label for="supr_smtp_mailer_active_validation"><?= __('Activate own email addresses', 'supr-smtp-mailer'); ?></label></td>
            <td>
                <input type="checkbox" id="supr_smtp_mailer_active_validation" name="supr_smtp_mailer_ses_options[active_validation]" value="1" <?= (isset($options['active_validation']) && (int)$options['active_validation'] === 1 ? 'checked' : ''); ?>/>
            </td>
            <td><?= __('* We will send mails from the addresse of seller if it was validated.', 'supr-smtp-mailer'); ?></td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>

<form action="" method="POST">
    <table class="form-table">
        <tr>
            <th colspan="2"><?= __('Validation of an email address', 'supr-smtp-mailer'); ?></th>
        </tr>
        <tr>
            <td><label for="new_email_address"><?= __('Verify new email address', 'supr-smtp-mailer'); ?></label></td>
            <td>
                <input type="email" id="new_email_address" name="new_email_address" value=""/>
            </td>
            <td><?= __('* The verification email will be send on this addresse.', 'supr-smtp-mailer'); ?></td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>

<form action="" method="POST">
    <table class="form-table">
        <tr>
            <th colspan="2"><?= __('Custom verification template', 'supr-smtp-mailer'); ?></th>
        </tr>
        <?php
        $customVerificationEmailTemplate = \SuprSmtpMailer\SesManager::instance()->getCustomVerificationEmailTemplate();
        ?>
        <tr>
            <td><label for="failure_redirection_url"><?= __('FailureRedirectionURL', 'supr-smtp-mailer'); ?></label></td>
            <td>
                <input type="text" id="failure_redirection_url" name="failure_redirection_url" value="<?= $customVerificationEmailTemplate['FailureRedirectionURL'] ?? ''; ?>"/>
            </td>
        </tr>
        <tr>
            <td><label for="from_email_address"><?= __('FromEmailAddress', 'supr-smtp-mailer'); ?></label></td>
            <td>
                <input type="email" id="from_email_address" name="from_email_address" value="<?= $customVerificationEmailTemplate['FromEmailAddress'] ?? ''; ?>"/>
            </td>
        </tr>
        <tr>
            <td><label for="success_redirection_url"><?= __('SuccessRedirectionURL', 'supr-smtp-mailer'); ?></label></td>
            <td>
                <input type="text" id="success_redirection_url" name="success_redirection_url" value="<?= $customVerificationEmailTemplate['SuccessRedirectionURL'] ?? ''; ?>"/>
            </td>
        </tr>
        <tr>
            <td><label for="template_subject"><?= __('TemplateSubject', 'supr-smtp-mailer'); ?></label></td>
            <td>
                <input type="text" id="template_subject" name="template_subject" value="<?= $customVerificationEmailTemplate['TemplateSubject'] ?? ''; ?>"/>
            </td>
        </tr>
        <tr>
            <td><label for="template_content"><?= __('TemplateContent', 'supr-smtp-mailer'); ?></label></td>
            <td>
                <textarea id="template_content" name="template_content"><?= \stripslashes($customVerificationEmailTemplate['TemplateContent'] ?? ''); ?></textarea>
            </td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>

<!-- List of validated email addresses -->
<table class="form-table">
    <tr>
        <th colspan="3"><?= __('Verified email addresses', 'supr-smtp-mailer'); ?></th>
    </tr>
    <?php
    $emailAddresses = \SuprSmtpMailer\SesManager::instance()->getVerifiedEmailAddresses();
    if ($emailAddresses === null) {
        echo '<tr><td colspan="3">' . __('Cannot get email adresses list. See log for details.', 'supr-smtp-mailer') . '</td></tr>';
    } else {
        foreach ($emailAddresses as $emailAddress) {
            echo "<tr><td>{$emailAddress}</td><td><a href='?page=supr_smtp_mailer_settings&tab=verification-emails&delete_email_address={$emailAddress}'>Delete</a></td></tr>";
        }
    }
    ?>
</table>