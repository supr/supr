<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

// Handle request with test email
if (isset($_POST['supr_smtp_mailer_test'])) {
    $email = sanitize_email($_POST['supr_smtp_mailer_test']['email']);
    $subject = sanitize_text_field($_POST['supr_smtp_mailer_test']['subject']);
    $msg = sanitize_textarea_field($_POST['supr_smtp_mailer_test']['msg']);

    add_action('wp_mail_failed', 'supr_smtp_mailer_show_test_mail_error', 10, 1);

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo '<div class="notice notice-error is-dismissible"><p>' . __('From email address is not valid.', 'supr-smtp-mailer') . '</p></div>';
    } elseif (wp_mail($email, $subject, $msg)) {
        echo '<div class="notice notice-success is-dismissible"><p>' . __('Email was sent.', 'supr-smtp-mailer') . '</p></div>';
    } else {
        echo '<div class="notice notice-warning is-dismissible"><p>' . __('Email was not sent.', 'supr-smtp-mailer') . '</p></div>';
    }
}
?>
<form action="" method="POST">
    <table class="form-table">
        <tr>
            <th colspan="2"><?= __('Test mail', 'supr-smtp-mailer'); ?></th>
        </tr>
        <tr>
            <td><label for="supr_smtp_mailer_test_address"><?= __('Email', 'supr-smtp-mailer'); ?></label></td>
            <td><input type="text" id="supr_smtp_mailer_test_address" name="supr_smtp_mailer_test[email]"/></td>
        </tr>
        <tr>
            <td><label for="supr_smtp_mailer_test_subject"><?= __('Subject', 'supr-smtp-mailer'); ?></label></td>
            <td><input type="text" id="supr_smtp_mailer_test_subject" name="supr_smtp_mailer_test[subject]" value="<?= __('Test Subject!', 'supr-smtp-mailer'); ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_smtp_mailer_test_msg"><?= __('Message', 'supr-smtp-mailer'); ?></label></td>
            <td><textarea id="supr_smtp_mailer_test_msg" name="supr_smtp_mailer_test[msg]"><?= __('Hi, I am a test email!', 'supr-smtp-mailer'); ?></textarea></td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>

<?php

/**
 * @param WP_Error $wpError
 */
function supr_smtp_mailer_show_test_mail_error($wpError)
{
    echo '<div class="notice notice-error is-dismissible"><p>' . $wpError->get_error_message() . '</p></div>';
}