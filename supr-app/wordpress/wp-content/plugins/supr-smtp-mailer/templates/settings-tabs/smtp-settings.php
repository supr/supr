<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$options = get_option('supr_smtp_mailer_options', []);

$secureSelect = [
    'tls' => __('Ssl', 'supr-smtp-mailer'),
    'ssl' => __('Tls', 'supr-smtp-mailer')
];

// Handle request with options
if (isset($_POST['supr_smtp_mailer_options'])) {
    $options = $_POST['supr_smtp_mailer_options'];

    // Trim all values
    $options = array_map('trim', $options);
    // Remove empty values
    $options = array_filter($options);
    // Sanitize values with WP special function
    $options = array_map('sanitize_text_field', $options);

    /////////////////////
    // Validate values //
    /////////////////////

    // Validate email
    if (isset($options['from']) && !filter_var($options['from'], FILTER_VALIDATE_EMAIL)) {
        $error = __('From email address is not valid.', 'supr-smtp-mailer');
    }

    // Validate hostname (if reachable)
    if (!filter_var(gethostbyname($options['host']), FILTER_VALIDATE_IP)) {
        $error = __('Hostname is not valid.', 'supr-smtp-mailer');
    }

    // Validate port number
    if ((int)$options['port'] <= 0 || !filter_var($options['port'], FILTER_VALIDATE_INT)) {
        $error = __('Port number is not valid.', 'supr-smtp-mailer');
    }

    // Validate connection type
    if (!isset($secureSelect[$options['secure']])) {
        $error = __('Encryption system is not valid.', 'supr-smtp-mailer');
    }

    // Save or show error
    if ($error === null) {
        update_option('supr_smtp_mailer_options', $options);
        echo '<div class="notice notice-success is-dismissible"><p>' . __('Options were saved.', 'supr-smtp-mailer') . '</p></div>';
    } else {
        echo '<div class="notice notice-error is-dismissible"><p>' . $error . '</p></div>';
    }
}

?>
<form action="" method="POST">
    <table class="form-table">
        <tr>
            <th colspan="2"><?= __('Main options', 'supr-smtp-mailer'); ?></th>
        </tr>
        <tr>
            <td><label for="supr_smtp_mailer_user"><?= __('Benutzername für die SMTP-Authentifizierung', 'supr-smtp-mailer'); ?></label></td>
            <td><input type="text" id="supr_smtp_mailer_user" name="supr_smtp_mailer_options[user]" value="<?= $options['user'] ?? ''; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_smtp_mailer_pass"><?= __('Passwort für die SMTP-Authentifizierung', 'supr-smtp-mailer'); ?></label></td>
            <td><input type="password" id="supr_smtp_mailer_pass" name="supr_smtp_mailer_options[pass]" value="<?= $options['pass'] ?? ''; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_smtp_mailer_host"><?= __('Der Hostname des Mailservers', 'supr-smtp-mailer'); ?></label></td>
            <td><input type="text" id="supr_smtp_mailer_host" name="supr_smtp_mailer_options[host]" value="<?= $options['host'] ?? ''; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_smtp_mailer_from"><?= __('SMTP Von E-Mail-Adresse', 'supr-smtp-mailer'); ?></label></td>
            <td><input type="text" id="supr_smtp_mailer_from" name="supr_smtp_mailer_options[from]" value="<?= $options['from'] ?? ''; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_smtp_mailer_name"><?= __('SMTP Von Name', 'supr-smtp-mailer'); ?></label></td>
            <td><input type="text" id="supr_smtp_mailer_name" name="supr_smtp_mailer_options[name]" value="<?= $options['name'] ?? ''; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_smtp_mailer_port"><?= __('SMTP-Portnummer - wahrscheinlich 25, 465 oder 587', 'supr-smtp-mailer'); ?></label></td>
            <td><input type="number" id="supr_smtp_mailer_port" name="supr_smtp_mailer_options[port]" value="<?= $options['port'] ?? ''; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_smtp_mailer_secure"><?= __('Zu verwendendes Verschlüsselungssystem - ssl oder tls', 'supr-smtp-mailer'); ?></label></td>
            <td>
                <select id="supr_smtp_mailer_secure" name="supr_smtp_mailer_options[secure]">
                    <?php foreach ($secureSelect as $type => $name) : ?>
                        <option value="<?= $type ?>"<?= ($options['secure'] ?? null) === $type ? ' selected' : '' ?>><?= $name ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>
