# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-10 16:29+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: includes/Resource.php:40
msgid "Cancel"
msgstr "Abbrechen"

#: includes/Resource.php:38
msgid "Do you want to verify your email address?"
msgstr "Möchtest du deine Emailadresse verifizieren?"

#: includes/Resource.php:43
msgid "Please check you mail box."
msgstr "Bitte schaue in dein E-Mail Postfach."

#: includes/Api.php:35
msgid "Please provide a valid email address."
msgstr "Bitte gib eine gültige Emailadresse an."

#: includes/Resource.php:45
msgid "Please try again or contact our support."
msgstr "Bitte versuche es erneut oder kontaktiere unseren Support."

#: includes/Menu.php:30 includes/Menu.php:31
msgid "SMTP Mailer"
msgstr ""

#: includes/Menu.php:46
msgid "Settings"
msgstr "Einstellungen"

#: includes/Resource.php:44
msgid "Something went wrong"
msgstr "Ein Fehler ist aufgetreten"

#: includes/Api.php:46
msgid "The email address cannot be verified."
msgstr "Die Emailadresse konnte nicht verifiziert werden."

#: includes/Api.php:39
msgid "This email is already verified."
msgstr "Diese Emailadresse ist bereits verifiziert"

#: includes/Resource.php:42
msgid "Verification mail was sent"
msgstr "Eine E-Mail zur Verifikation wurde versendet"

#: includes/Resource.php:41
msgid "Verify"
msgstr "Verifizieren"

#: includes/Resource.php:37
msgid "Verify email"
msgstr "E-Mail verifizieren"

#: includes/Resource.php:46
msgid "Waiting..."
msgstr "Bitte warten..."

#: includes/Resource.php:47
msgid "We are sending the mail..."
msgstr "Wir verschicken die E-Mail..."

#: includes/Resource.php:39
msgid ""
"We will send  you a confirmation mail. Click on the link and your customers "
"will receive all mails from your email address."
msgstr "Wir schicken dir eine Verifikationsmail an deine Emailadresse. Wenn du dort auf den Link klickst, werden alle Emails von deiner Emailadresse versendet."