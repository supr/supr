��          �                    )   $     N  %   i  (   �     �     �  %   �     �          7     >  
   K     V  {   q  B  �  	   0  -   :  %   h  (   �  :   �     �        1     *   M  ,   x     �     �     �     �  �   �   Cancel Do you want to verify your email address? Please check you mail box. Please provide a valid email address. Please try again or contact our support. Settings Something went wrong The email address cannot be verified. This email is already verified. Verification mail was sent Verify Verify email Waiting... We are sending the mail... We will send  you a confirmation mail. Click on the link and your customers will receive all mails from your email address. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-09-10 16:29+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=CHARSET
Content-Transfer-Encoding: 8bit
 Abbrechen Möchtest du deine Emailadresse verifizieren? Bitte schaue in dein E-Mail Postfach. Bitte gib eine gültige Emailadresse an. Bitte versuche es erneut oder kontaktiere unseren Support. Einstellungen Ein Fehler ist aufgetreten Die Emailadresse konnte nicht verifiziert werden. Diese Emailadresse ist bereits verifiziert Eine E-Mail zur Verifikation wurde versendet Verifizieren E-Mail verifizieren Bitte warten... Wir verschicken die E-Mail... Wir schicken dir eine Verifikationsmail an deine Emailadresse. Wenn du dort auf den Link klickst, werden alle Emails von deiner Emailadresse versendet. 