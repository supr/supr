jQuery(document).ready(function ($) {

    var tourDashboard = function () {
        $('html').itour({
            showAbsentElement: true,
            introCover: '/wp-content/plugins/supr-wordpress/img/tour-image.gif',
            introShow: true,
            lang: {
                introTitle: 'SHOP TOUR',
                introContent: 'Starte jetzt die Tour durch deinen Onlineshop und lerne alle Funktionen des Shops kennen. Beginne die Tour mit dem Klick auf den Button START.',
                tourMapTitle: 'Tour Map',
                nextTextDefault: 'Weiter',
                prevTextDefault: 'Zurück',
                endText: 'Tour beenden',
                introDialogBtnStart: 'Start',
                introDialogBtnCancel: 'Abbrechen',
                cancelText: 'Tour abbrechen',
                hideText: 'verstecken',
                contDialogTitle: 'Mit der Führung fortfahren?',
                contDialogContent: 'Du kannst mit deiner laufenden Führung fortfahren oder eine neue beginnen.',
                contDialogBtnBegin: 'Neu beginnen ',
                contDialogBtnContinue: 'Fortsetzen'
            },
            steps: [{
                title: 'ÜBERSICHT',
                content: 'Herzlich Willkommen in deinem eigenen Onlineshop!\n' +
                    'Im Menü findest du alle Einstellmöglichkeiten zur Erstellung deines Shops. Zunächst beginnst du in \n' +
                    'der Übersicht. Die Übersicht enthält die wichtigen Informationen und Hilfestellungen zur Erstellung \n' +
                    'deines Onlineshops.',
                name: '#wpbody',
                contentPosition: 'center',
                loc: 'index.php'
            }, {
                title: 'SUPR SLIDER',
                content: 'Die Dashboard Slider des Shops beinhalten die wichtigsten Informationen rund um deinen SUPR \n' +
                    'Onlineshop und machen auf Gewinnspiele, Apps und vieles mehr aufmerksam.',
                name: '#supr-dashboard-slider',
                contentPosition: 'auto',
                loc: 'index.php'
            }, {
                title: 'ERSTE SCHRITTE',
                content: 'Im ersten Schritte Abschnitt kannst du per Klick in die wichtigen Bereiche des Shops geleitet \n' +
                    'werden. Hierzu zählen das Hinterlegen der Shopbetreiberdaten, das Erstellen der Produkte, das \n' +
                    'Einstellen der Bezahlarten und das Überprüfen der rechtlichen Seiten. Durch die Veränderung der \n' +
                    'Farbe in grün bei Erledigung der Aufgabe hast du immer einen genauen Überblick, was noch zu tun \n' +
                    'ist.',
                name: '#supr_dashboard_first_steps',
                contentPosition: 'auto',
                loc: 'index.php'

            }, {
                title: 'SUPR DASHBOARD',
                content: 'Das Dashboard deines Onlineshops beinhaltet unterschiedliche Themengebiete, welche dir beim \n' +
                    'Aufbau deines Shops helfen. Dort findest du Informationen zu Bestellungen, Umsätzen, Tutorials und \n' +
                    'vieles mehr. Besonders der SUPR Blog bietet dir Hilfestellungen, Tipps und Wissenswertes rund um \n' +
                    'die Welt des E-Commerce. Damit das SUPR Dashboard für dich übersichtlich bleibt, kannst du alles per Drag & Drop verschieben.',
                name: '.wuapc-dashboard',
                contentPosition: 'tcc',
                loc: 'index.php'
            }, {
                title: 'NAVIGATION',
                content: 'Das Menü deines Onlineshops ist in unterschiedliche Oberpunkte aufgeteilt. Dein Anfangspunkt ist \n' +
                    'immer die Übersicht, nachfolgend findest du Einstellmöglichkeiten zu MEIN SHOP, VERWALTUNG, \n' +
                    'APPS, SHOPDESIGN und ERWEITERT.',
                name: '#adminmenuwrap',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'PRODUKTE',
                content: 'Unter dem Punkt PRODUKTE kannst du Einstellungen zum Erstellen deiner Produkte vornehmen.\n' +
                    'Du kannst ALLE PRODUKTE aufrufen und eine Übersicht erhalten. Zusätzlich hast du die Möglichkeit \n' +
                    'KATEGORIEN anzulegen, EIGENSCHAFTEN zu hinterlegen, SCHLAGWÖRTER hinzufügen und \n' +
                    'einen PRODUKT IMPORT oder auch einen PRODUKT EXPORT durchzuführen. \n' +
                    'Weitere Hilfestellungen aus diesem Bereich findest du im Hilfebereich unter dem Punkt PRODUKTE.',
                name: '.ame-menu-fa-shopping-cart',
                loc: 'index.php'
            }, {
                title: 'BESTELLUNGEN',
                content: 'Unter dem Punkt BESTELLUNGEN kannst du die Bestellungen deiner Kunden verwalten. Dort findest \n' +
                    'du alle Bestellinformationen zu verkauften Produkten. Zur besseren Übersicht kannst du den Status \n' +
                    'der jeweiligen Bestellungen verändern, um bestmöglich zu arbeiten. Weitere Informationen findest du \n' +
                    'im Hilfebereich unter dem Punkt BESTELLUNGEN.',
                name: '.ame-menu-fa-inbox',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'SEITEN',
                content: 'Hier kannst du nicht nur die rechtlichen Seiten verwalten, sondern auch individuelle Seiten anfertigen.',
                name: '.ame-menu-fa-file',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'KONFIGURATION',
                content: 'Unter dem Punkt KONFIGURATION kannst du Einstellungen zu den Menüpunkten Shop, Allgemein, \n' +
                    'Produkte, Mehrwertsteuer, Bezahlarten, Rechnungen, Versand, Kundenkonten, Emails, Rechtliches, \n' +
                    'Preishinweise, Lieferzeiten und Einheiten vornehmen.',
                name: '.ame-menu-fa-sliders',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'BERICHTE',
                content: 'Unter dem Punkt BERICHTE findest du zu den unterschiedlichsten Themen Auswertungen. Die verschiedenen Statistiken bspw. zu den meistverkauften Produkten helfen dir dabei, die Kunden deines Shops besser kennenzulernen und das Kaufverhalten zu analysieren. ',
                name: '.ame-menu-fa-pie-chart',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'KUNDEN',
                content: 'Verwalte hier die angelegten Benutzer deines Shops und vergebe Nutzerrollen.',
                name: '.ame-menu-fa-users',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'BEWERTUNGEN',
                content: 'Unter dem Punkt BEWERTUNGEN kannst du die Bewertungen und Kommentare deiner Kunden verwalten. Zusätzlich zum Bearbeiten und Freigeben besteht die Möglichkeit die Bewertungen auf den Produktseiten deines Shops anzuzeigen.',
                name: '.ame-menu-fa-comments-o',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'BLOG',
                content: 'Unter dem Unterpunkt BLOG hast du die Möglichkeit einen Blog zu erstellen mit Beiträgen, \n' +
                    'unterschiedlichen Kategorien und auch das Hinterlegen von Schlagwörtern ist möglich.',
                name: '.ame-menu-fa-newspaper-o',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'GUTSCHEINE',
                content: 'Unter dem Punkt GUTSCHEINE besteht die Möglichkeit attraktive Gutscheincodes anzulegen. Diese sind die perfekte Möglichkeit deinen Umsatz zu erhöhen, denn Rabatte locken Kunden an.',
                name: '.ame-menu-fa-ticket',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'APPS HINZUFÜGEN',
                content: 'Zusätzlich zu den bereits verwendeten Apps können weitere Apps hinzugefügt werden. Nutze die große Auswahl der Apps deines gebuchten Tarifs, um deinen Kunden die besten Funktionen anzubieten.',
                name: '.ame-menu-fa-plus',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'DESIGN',
                content: 'Unter dem Punkt Design kannst du Änderungen am Design deines Shops vornehmen. Verändere \n' +
                    'nicht nur die Farben, sondern auch Schriftgrößen, dein Menü und vieles mehr.',
                name: '.ame-menu-fa-paint-brush',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'VORLAGEN',
                content: 'Unter dem Punkt VORLAGEN findest du eine große Auswahl an vorgefertigten Designvorlagen, die von unseren \n' +
                    'professionellen Designern erstellt wurden. Finde dort dein passendes Design!',
                name: '.ame-menu-fa-th-large',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'MEDIEN',
                content: 'Unter dem Punkt Mediathek findest du alle Bilder, welche in deinem Onlineshop hochgeladen worden \n' +
                    'sind. ',
                name: '.ame-menu-fa-picture-o',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'DOMAIN',
                content: 'Unter dem Punkt DOMAIN kannst du deine eigene Domain hinterlegen. Mit dieser können dich deine \n' +
                    'Kunden besser im Internet finden.',
                name: '#toplevel_page_supr_own_domain_settings',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'EXPERTEN',
                content: 'Unter dem Punkt "Experten" kannst du erweiterte Einstellungen vornehmen. Wir empfehlen diesen Bereich nur zu verwenden, wenn du dich gut mit Onlineshops auskennst.',
                name: '#toplevel_page_wuapc-page-187',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'KONTO',
                content: 'Unter "Konto" findest du Informationen über deinen Kontostatus, deinen aktuellen Tarif und deinen Rechnungsverlauf',
                name: '#toplevel_page_wu-my-account',
                contentPosition: 'rtb',
                loc: 'index.php'
            }, {
                title: 'MEIN KONTO',
                content: 'Im oberen Bereich deines Dashboardes findest du unter dem Punkt MEIN KONTO die Option der Buchung der unterschiedlichen Tarife und genauere Informationen über den aktuellen Status deines Tarifs. Zusätzlich hierzu hast du die Möglichkeit dein eigenes Profil zu bearbeiten. Ein weiterer Punkt ist die Option des Ausloggens aus deinem Shop, wodurch eine erneute Anmeldung erforderlich ist.',
                name: '#wp-admin-bar-supr-account',
                contentPosition: 'bcc',
                loc: 'index.php'
            }, {
                title: 'SHOP ANSEHEN',
                content: 'Nutze diese Funktion um bspw. Änderungen am Design aus Kundensicht zu sehen.',
                name: '#wp-admin-bar-visit-store',
                contentPosition: 'bcc',
                loc: 'index.php'
            }, {
                title: 'SHOPSTATUS',
                content: 'Der farbliche Kreis zeigt dir an, in welchem Status sich dein Onlineshop befindet. Wenn die Ampel die grüne Farbe anzeigt, dann können Kunden in deinem Shop einkaufen, da dieser live geschaltet ist. Sobald die Farbe rot anzeigt, ist dein Shop im Wartungsmodus.',
                name: '#wp-admin-bar-store-status',
                contentPosition: 'bcc',
                loc: 'index.php'
            }]
        });
    };

    $('li#wp-admin-bar-supr-admin-tour a').on('click', function (evt) {
        console.log($(this).attr('title'));
        tourDashboard();
        if (evt && evt.preventDefault) {
            evt.preventDefault();
        } else if (window.event && window.event.returnValue) {
            window.event.returnValue = false;
        }
    });

    if (location.hash === '#tour') {
        location.hash = '';
        tourDashboard();
    }
});
