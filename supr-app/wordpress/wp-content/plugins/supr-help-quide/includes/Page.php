<?php

namespace SuprHelpQuide;

/**
 * Class Page
 *
 * @package SuprHelpQuide
 */
class Page
{
    /**
     * Show network setting page
     */
    public static function networkSettingsPage(): void
    {
        include SUPR_HELP_GUIDE_PATH . 'templates/network-settings-page.php';
    }
}
