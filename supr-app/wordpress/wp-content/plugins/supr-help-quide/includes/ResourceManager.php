<?php

namespace SuprHelpQuide;

/**
 * Class ResourceManager
 *
 * @package SuprHelpQuide
 */
class ResourceManager
{
    /**
     * Register files for plugin
     */
    public static function addAdminResources(): void
    {
        \wp_enqueue_style('supr_help_guide_style', SUPR_HELP_GUIDE_URL . 'css/style.css', [], SUPR_HELP_GUIDE_VERSION);
        // Tippy tooltip (in use)
        \wp_enqueue_script('supr_tippy', SUPR_HELP_GUIDE_URL . 'assets/tippy/tippy.all.min.js', ['jquery'], SUPR_HELP_GUIDE_VERSION, true);

        // Admin tour
        if (AdminTourManager::showAdminTour()) {
            \wp_enqueue_style('supr_help_guide_style_itour', SUPR_HELP_GUIDE_URL . 'assets/itour/itour.css', [], SUPR_HELP_GUIDE_VERSION);
            \wp_enqueue_script('supr_help_guide_script_itour', SUPR_HELP_GUIDE_URL . 'assets/itour/jquery.itour.js', ['jquery'], SUPR_HELP_GUIDE_VERSION, true);
            \wp_enqueue_script('supr_help_guide_script_admin_tour', SUPR_HELP_GUIDE_URL . 'js/admin-tour.js', ['jquery'], SUPR_HELP_GUIDE_VERSION, true);
        }
    }
}
