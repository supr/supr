<?php

namespace SuprHelpQuide;

/**
 * Class AdminTourManager
 *
 * @package SuprHelpQuide
 */
class AdminTourManager
{
    public static function showAdminTour(): bool
    {
        // Show only on dashboard
        if (!\in_array($_SERVER['REQUEST_URI'], ['/wp-admin/', '/wp-admin/index.php'], true)) {
            return false;
        }

        // Get minimum information about current blog
        $blogDetails = \get_blog_details(null, false);

        // Get settings: when to hide (default: 30 days)
        $hideAfter = (new \DateTime())->sub(new \DateInterval(Options::getInstance()->getOption('hide_admin_tour_after')))->getTimestamp();

        $showAdminTour = strtotime($blogDetails->registered) > $hideAfter;

        return \apply_filters('supr_help_quide_show_admin_tour', $showAdminTour);
    }
}
