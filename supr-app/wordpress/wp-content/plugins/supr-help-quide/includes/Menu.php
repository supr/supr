<?php

namespace SuprHelpQuide;

/**
 * Class Menu
 *
 * @package SuprCopOnboarding
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add network menu page
        \add_action('network_admin_menu', [__CLASS__, 'createNetworkMenu']);

        // Add network settings link on plugin page
        \add_filter('network_admin_plugin_action_links_' . SUPR_HELP_GUIDE_FILE, [__CLASS__, 'addNetworkSettingsPage']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createNetworkMenu(): void
    {
        \add_menu_page(
            __('SUPR Help quide', 'supr-help-quide'),
            __('SUPR Help quide', 'supr-help-quide'),
            'manage_network',
            'supr_help_quide_network_settings',
            [Page::class, 'networkSettingsPage'],
            'dashicons-sos'
        );
    }

    /**
     * @param $links
     * @return mixed
     */
    public static function addNetworkSettingsPage($links)
    {
        $settings_link = '<a href="admin.php?page=supr_help_quide_network_settings">' . __('Settings', 'supr-help-quide') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
