<?php

namespace SuprHelpQuide;

/**
 * Class AdminBar
 *
 * @package SuprHelpQuide
 */
class AdminBar
{
    /**
     * Add some nodes
     */
    public static function changeAdminBarNodes(): void
    {
        /** @var \WP_Admin_Bar $wp_admin_bar */
        global $wp_admin_bar;

        if (AdminTourManager::showAdminTour()) {
            $wp_admin_bar->add_menu(
                [
                    'id' => 'supr-admin-tour',
                    'parent' => 'top-secondary',
                    'title' => \__('Start help tour', 'supr-help-quide'),
                    'href' => \admin_url(),
                    'meta' => [
                        'title' => \__('We will help you know better your store', 'supr-help-quide'),
                        'class' => 'supr-admin-tour'
                    ]
                ]
            );
        }
    }
}
