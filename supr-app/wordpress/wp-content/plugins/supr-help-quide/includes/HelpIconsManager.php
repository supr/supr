<?php

namespace SuprHelpQuide;

/**
 * Class HelpIconsManager
 *
 * @package SuprHelpQuide
 */
class HelpIconsManager
{
    /**
     * Check if string is a valid regex
     *
     * @param $string
     * @return bool
     */
    public static function isRegularExpression($string): bool
    {
        return @preg_match($string, '') !== false;
    }

    /**
     * Find help icon for current page
     *
     * @return array|null
     * @throws \Exception
     */
    public static function getCurrentHelpIcon(): ?array
    {
        $return = null;

        $currentRequestUrl = $_SERVER['REQUEST_URI'];

        $helpIconsRules = \SuprHelpQuide\Options::getInstance()->getOption('help_icons_rules');

        foreach ($helpIconsRules as $iconRule) {
            $regexp = stripcslashes($iconRule['regexp']);
            if (filter_var($currentRequestUrl, FILTER_VALIDATE_REGEXP, ["options"=>["regexp"=>$regexp]])) {
                $return = $iconRule;
                break;
            }
        }

        return $return;
    }

    /**
     * Render help icon
     *
     * @param array $helpIcon
     */
    public static function renderHelpIcon(array $helpIcon): void
    {
        include SUPR_HELP_GUIDE_PATH . 'templates/help-icon.php';
    }

    /**
     * Add help icon to current page if exist
     */
    public static function addHelpIcon(): void
    {
        $helpIcon = HelpIconsManager::getCurrentHelpIcon();

        if ($helpIcon !== null) {
            self::renderHelpIcon($helpIcon);
        }
    }
}
