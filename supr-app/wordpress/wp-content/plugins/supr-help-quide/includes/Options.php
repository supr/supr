<?php

namespace SuprHelpQuide;

/**
 * Class Options
 *
 * @package SuprDashboard
 */
class Options
{
    /**
     * @var Options
     */
    public static $instance;

    private $optionsName = 'supr_help_guide_settings';

    /**
     * Options with default values
     *
     * @var array
     */
    private $optionsNames = [
        'hide_admin_tour_after' => 'P1M',
        'help_icons_rules' => []
    ];

    private $options;

    /**
     * @return Options
     */
    public static function getInstance(): Options
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->loadOptions();
    }

    /**
     * Load all of options in this class
     */
    private function loadOptions(): void
    {
        // Get from DB
        $options = \get_site_option($this->optionsName, []);
        // Merge with default
        $this->options = \array_merge($this->optionsNames, $options);
    }

    /**
     * @param string $optionsName
     * @return mixed
     * @throws \Exception
     */
    public function getOption($optionsName)
    {
        if (!isset($this->optionsNames[$optionsName])) {
            throw new \Exception("[SUPR DASHBOARD] The option '{$optionsName}' is not registered.");
        }

        return $this->options[$optionsName];
    }

    /**
     * @param string $optionsName
     * @param        $optionsValue
     * @throws \Exception
     */
    public function setOption($optionsName, $optionsValue): void
    {
        if (!isset($this->optionsNames[$optionsName])) {
            throw new \Exception("[SUPR HELP GUIDE] The option '{$optionsName}' is not registered.");
        }

        $this->options[$optionsName] = $optionsValue;

        \update_site_option($this->optionsName, $this->options);
    }
}
