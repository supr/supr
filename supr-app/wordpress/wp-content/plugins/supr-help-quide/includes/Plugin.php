<?php

namespace SuprHelpQuide;

/**
 * Class Plugin
 *
 * @package SuprHelpQuide
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function getInstance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Admin init
     */
    public function adminInit(): void
    {
        \add_action('admin_enqueue_scripts', [ResourceManager::class, 'addAdminResources']);
        \add_action('wp_before_admin_bar_render', [AdminBar::class, 'changeAdminBarNodes'], 1);
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // Load translations
        \add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        // Create Menu
        Menu::addMenu();

        \add_action('admin_init', [$this, 'adminInit']);

        // Add a help icon
        \add_action('admin_footer', [HelpIconsManager::class, 'addHelpIcon']);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_HELP_GUIDE_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}
