<?php

namespace SuprHelpQuide;

class TextDomain
{
    public static $domainName = 'supr-help-quide';

    public static function registerTranslations(): void
    {
        load_plugin_textdomain(self::$domainName, false, SUPR_HELP_GUIDE_DIR_NAME . '/languages/');
    }
}
