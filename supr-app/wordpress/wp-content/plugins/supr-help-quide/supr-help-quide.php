<?php
/**
 * Plugin Name: SUPR - Help Guide
 * Plugin URI: https://supr.com
 * Description: SUPR notices, help icons and admin tour. All, that can help our user to learn the shop.
 * Text Domain: supr-help-quide
 * Domain Path: /languages
 * Version: 1.0.0
 * Author: SUPR
 * Author URI: https://supr.com
 */

define('SUPR_HELP_GUIDE_PATH', plugin_dir_path(__FILE__));
define('SUPR_HELP_GUIDE_URL', plugins_url('/', __FILE__));
define('SUPR_HELP_GUIDE_FILE', plugin_basename(__FILE__));
define('SUPR_HELP_GUIDE_DIR_NAME', basename(__DIR__));

define('SUPR_HELP_GUIDE_VERSION', '1.0.0');

// Load main Class
require(SUPR_HELP_GUIDE_PATH . 'includes/Plugin.php');

\SuprHelpQuide\Plugin::getInstance();
