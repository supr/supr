<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

// Handle request
if (isset($_POST['supr_help_guide_settings']) && is_array($_POST['supr_help_guide_settings'])) {
    $newOptions = $_POST['supr_help_guide_settings'];

    // Trim all values
    $newOptions = array_map('trim', $newOptions);
    // Remove empty values
    $newOptions = array_filter($newOptions);
    // Sanitize values with WP special function
    $newOptions = array_map('sanitize_text_field', $newOptions);

    foreach ($newOptions as $name => $value) {
        \SuprHelpQuide\Options::getInstance()->setOption($name, $value);
    }
}

$hideAdminTourAfterSelect = [
    'P1D' => \__('1 day', 'supr-help-quide'),
    'P10D' => \__('10 days', 'supr-help-quide'),
    'P1M' => \__('1 month', 'supr-help-quide'),
    'P3M' => \__('3 months', 'supr-help-quide'),
];
?>
<form action="" method="POST">
    <table class="form-table">
        <tr>
            <td><label for="supr_help_guide_settings_hide_admin_tour_after"><?= __('Hide admin tour after', 'supr-help-quide'); ?></label></td>
            <td>
                <select id="supr_help_guide_settings_hide_admin_tour_after" name="supr_help_guide_settings[hide_admin_tour_after]">
                    <?php foreach ($hideAdminTourAfterSelect as $value => $name) : ?>
                        <option value="<?= $value ?>"<?= \SuprHelpQuide\Options::getInstance()->getOption('hide_admin_tour_after') === $value ? ' selected' : '' ?>><?= $name ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>
