<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

// Handle request

$error = null;

if (isset($_POST['help_icons_rules']) && is_array($_POST['help_icons_rules'])) {
    $helpIconsRules = $_POST['help_icons_rules'];

    foreach ($helpIconsRules as $num => $helpIconsRule) {
        // If it was deleted
        if (empty($helpIconsRule['regexp'])) {
            unset($helpIconsRules[$num]);
            continue;
        }
        // Validate regexp
        if (!\SuprHelpQuide\HelpIconsManager::isRegularExpression($helpIconsRules[$num]['regexp'])) {
            $error = sprintf(\__('%d rule has not valid regular expression.', 'supr-help-quide'), $num + 1);
            break;
        }
        //Validate url
        if (!filter_var($helpIconsRules[$num]['link'], FILTER_VALIDATE_URL)) {
            $error = sprintf(\__('%d rule has not valid link.', 'supr-help-quide'), $num + 1);
            break;
        }
        // Sanitize title
        $helpIconsRules[$num]['title'] = \sanitize_text_field($helpIconsRules[$num]['title']);
    }

    // Save or show error
    if ($error === null) {
        \SuprHelpQuide\Options::getInstance()->setOption('help_icons_rules', $helpIconsRules);
        echo '<div class="notice notice-success is-dismissible"><p>' . __('Options were saved.', 'supr-help-quide') . '</p></div>';
    } else {
        echo '<div class="notice notice-error is-dismissible"><p>' . $error . '</p></div>';
    }
} else {
    $helpIconsRules = \SuprHelpQuide\Options::getInstance()->getOption('help_icons_rules');
}

// Add new empty rule
if ($error === null) {
    $helpIconsRules[] = ['regexp' => '', 'link' => '', 'title' => ''];
}

?>
<p><?= \__('On this page, you can add rules in the form of regular expressions and links to pages with help information for pages matching these regular expressions.', 'supr-help-quide') ?></p>
<p><?= \__('Please be careful and make sure that one page does not fall under two rules at once.', 'supr-help-quide') ?></p>
<p><?= sprintf(\__('Before adding youe regular expression please test all of them <a href="%s" target="_blank">here</a>.', 'supr-help-quide'), 'https://regex101.com/') ?></p>
<p>* <?= \__('Examples:', 'supr-help-quide') ?></p>
<ul>
    <li><?= \__('This regular expression <code>~/wp-admin/edit.php\?post_type=product~</code> matches this page <code>/wp-admin/edit.php?post_type=product</code>.', 'supr-help-quide') ?></li>
    <li><?= \__('But it does not match this page <code>/wp-admin/edit.php?post_type=product&orderby=price&order=asc</code>.', 'supr-help-quide') ?></li>
    <li><?= \__('To match the second page, the regular expression should look like this: <code>~/wp-admin/edit.php\?post_type=product\.*~</code>.', 'supr-help-quide') ?></li>
</ul>
<form action="" method="POST">
    <table class="form-table">
        <tr>
            <th colspan="3"><?= __('Help icons settings', 'supr-help-quide') ?></th>
        </tr>
        <tr>
            <td><?= __('Rule', 'supr-help-quide') ?></td>
            <td><?= __('Link', 'supr-help-quide') ?></td>
            <td><?= __('Title', 'supr-help-quide') ?></td>
        </tr>
        <?php foreach ($helpIconsRules as $num => $iconRule) : ?>
            <tr>
                <td><input type="text" name="help_icons_rules[<?= $num ?>][regexp]" value='<?= str_replace("'", "&#39;", stripcslashes($iconRule['regexp'])) ?>'></td>
                <td><input type="url" name="help_icons_rules[<?= $num ?>][link]" value="<?= $iconRule['link'] ?>"></td>
                <td><input type="text" name="help_icons_rules[<?= $num ?>][title]" value="<?= $iconRule['title'] ?>"></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php submit_button(); ?>
</form>
