<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/** @var array $helpIcon */
?>
<a href="<?= $helpIcon['link'] ?>"
   target="_blank"
   class="tippy"
   id="supr-help-quide-help-icon"
   data-tippy-content="<?= $helpIcon['title'] ?>">
    <span class="supr-help-quide-help-text"><?= __('Show help', 'supr-help-quide'); ?></span>
</a>
