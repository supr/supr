<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$tabs = [
    'help-icons' => __('Help icons', 'supr-help-quide'),
    'admin-tour' => __('Admin tour', 'supr-help-quide')
];

$currentTab = isset($_GET['tab'], $tabs[$_GET['tab']]) ? $_GET['tab'] : 'help-icons';
?>
<div class="wrap supr-help-quide">
    <h2><?= \get_admin_page_title(); ?></h2>
    <h2 class="nav-tab-wrapper">
        <?php foreach ($tabs as $tab => $tabName) : ?>
            <a class="nav-tab<?= $currentTab === $tab ? ' nav-tab-active' : ''; ?>" href="?page=<?= $_GET['page'] ?>&tab=<?= $tab; ?>"><?= $tabName; ?></a>
        <?php endforeach; ?>
    </h2>

    <?php

    // Include tabs template
    include SUPR_HELP_GUIDE_PATH . "templates/settings-tabs/{$currentTab}.php";
    ?>
</div>
