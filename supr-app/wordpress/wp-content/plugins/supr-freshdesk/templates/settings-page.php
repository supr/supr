<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();
?>
<div class="wrap">
    <h2><?= get_admin_page_title(); ?></h2>
    <?php
    // Handle request
    if (isset($_POST['supr_freshdesk_options'])) {
        $newOptions = $_POST['supr_freshdesk_options'];

        // Trim all values
        $newOptions = array_map('trim', $newOptions);
        // Remove empty values
        $newOptions = array_filter($newOptions);
        // Sanitize values with WP special function
        $newOptions = array_map('sanitize_text_field', $newOptions);

        // Default for checkbox
        if (!isset($newOptions['chat_active'])) {
            $newOptions['chat_active'] = 0;
        }
        update_option('supr_freshdesk_options', $newOptions);
    }

    $options = get_option('supr_freshdesk_options', []);
    ?>

    <form action="" method="POST">
        <table class="form-table">
            <tr>
                <th colspan="2"><?= __('Main options', 'supr-freshdesk'); ?></th>
            </tr>
            <tr>
                <td><label for="supr_freshdesk_option_api_key"><?= __('API Key', 'supr-freshdesk'); ?></label></td>
                <td><input type="text" id="supr_freshdesk_option_api_key" name="supr_freshdesk_options[api_key]" value="<?= $options['api_key'] ?? ''; ?>"/></td>
            </tr>
            <tr>
                <td><label for="supr_freshdesk_option_password"><?= __('Password', 'supr-freshdesk'); ?></label></td>
                <td><input type="text" id="supr_freshdesk_option_password" name="supr_freshdesk_options[password]" value="<?= $options['password'] ?? ''; ?>"/></td>
            </tr>
            <tr>
                <td><label for="supr_freshdesk_option_password"><?= __('Domain name', 'supr-freshdesk'); ?></label></td>
                <td><input type="text" id="supr_freshdesk_option_password" name="supr_freshdesk_options[domain]" value="<?= $options['domain'] ?? ''; ?>"/></td>
            </tr>
            <tr>
                <td><label for="supr_freshdesk_option_not_sync_domain"><?= __('Black list template for shop domain', 'supr-campaign-monitor'); ?></label></td>
                <td>
                    <input type="text" id="supr_freshdesk_option_not_sync_domain" placeholder="/^test[0-9a-z]{1,}/" name="supr_freshdesk_options[not_sync_domain]" value="<?= $options['not_sync_domain'] ?? ''; ?>"/>
                </td>
            </tr>
            <tr>
                <td><label for="supr_freshdesk_option_legal"><?= __('Text for accept terms and conditions', 'supr-freshdesk'); ?></label></td>
                <td><textarea id="supr_freshdesk_option_legal" name="supr_freshdesk_options[legal]"><?= stripslashes($options['legal'] ?? ''); ?></textarea></td>
            </tr>
            <tr>
                <th colspan="2"><?= __('Chat options', 'supr-freshdesk'); ?></th>
            </tr>
            <tr>
                <td><label for="supr_freshdesk_option_chat_active"><?= __('Activate chat', 'supr-freshdesk'); ?></label></td>
                <td><input type="checkbox" id="supr_freshdesk_option_chat_active" name="supr_freshdesk_options[chat_active]" value="1" <?= (int)$options['chat_active'] > 0 ? ' checked' : ''; ?>>
                </td>
            </tr>
            <tr>
                <td><label for="supr_freshdesk_option_chat_token"><?= __('Chat token', 'supr-freshdesk'); ?></label></td>
                <td><input type="text" id="supr_freshdesk_option_chat_token" name="supr_freshdesk_options[chat_token]" value="<?= $options['chat_token'] ?? ''; ?>"></td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
    <p><?= __('You can use a shortcode for tickets form:', 'supr-freshdesk'); ?></p>
    <pre>[supr-freshdesk-contact-form]</pre>
    <p><?= __('Or with parameters:', 'supr-freshdesk'); ?></p>
    <pre>[supr-freshdesk-contact-form priority=1 group_id=43000436542 responder_id=43010668056 status=2 subject='I have a problem' ticket_type='Neukunde']</pre>
</div>
