<?php
/**
 * Plugin Name: SUPR - Freshdesk
 * Plugin URI: https://supr.com
 * Description: Integration of Freshdesk in WP Ultimo
 * Text Domain: supr-freshdesk
 * Domain Path: /languages
 * Version: 1.1.0
 * Author: SUPR Development
 * License: GPL
 */

define('SUPR_FRESHDESK_PATH', plugin_dir_path(__FILE__));
define('SUPR_FRESHDESK_URL', plugins_url('/', __FILE__));
define('SUPR_FRESHDESK_FILE', plugin_basename(__FILE__));
define('SUPR_FRESHDESK_DIR_NAME', basename(__DIR__));


define('SUPR_FRESHDESK_VERSION', '1.1.0');

// Vendor files
if (file_exists(ABSPATH . '../vendor/autoload.php')) {
    require_once ABSPATH . '../vendor/autoload.php';
}

// Load main Class
require SUPR_FRESHDESK_PATH . 'includes/Plugin.php';
