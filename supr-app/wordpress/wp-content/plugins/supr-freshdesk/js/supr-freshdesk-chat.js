(function () {
    "use strict";

    window.fcWidget.init({
        token: values.token,
        host: 'https://wchat.freshchat.com'
    });

    window.fcWidget.setExternalId(values.external_id);
    window.fcWidget.user.setFirstName(values.first_name);
    window.fcWidget.user.setEmail(values.email);
    window.fcWidget.user.setProperties({
        shop_name: values.shop_name,
        shop_url: values.shop_url
    });
}());