<?php

namespace SuprFreshDesk;

/**
 * Class Menu
 *
 * @package SuprFreshDesk
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        $menuPrefix = \is_multisite() ? 'network_admin' : 'admin';
        $actionsLinksPrefix = \is_multisite() ? 'network_admin_' : '';

        // Add menu page
        \add_action($menuPrefix . '_menu', [__CLASS__, 'createMenu']);

        // Add settings link on plugin page
        \add_filter($actionsLinksPrefix . 'plugin_action_links_' . SUPR_FRESHDESK_FILE, [__CLASS__, 'addSettingsPage']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createMenu(): void
    {
        \add_menu_page(
            __('Freshdesk', 'supr-freshdesk'),
            __('Freshdesk', 'supr-freshdesk'),
            \is_multisite() ? 'manage_network' : 'manage_options',
            'supr_freshdesk_settings',
            [Page::class, 'settingsPage'],
            'dashicons-sos',
            206
        );
    }

    /**
     * @param $links
     * @return mixed
     */
    public static function addSettingsPage($links)
    {
        $settings_link = '<a href="admin.php?page=supr_freshdesk_settings">' . __('Settings', 'supr-freshdesk') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
