<?php

namespace SuprFreshDesk;

class Form
{
    public static function registerShorCode(): void
    {
        \add_shortcode('supr-freshdesk-contact-form', [self::class, 'getContactForm']);
    }

    /**
     * Print contact form
     *
     * @param $args
     * @return string
     * @throws \Exception
     */
    public static function getContactForm($args): string
    {
        ob_start();

        $pluginOptions = \is_multisite() ? \get_blog_option(1, 'supr_freshdesk_options') : \get_option('supr_freshdesk_options');

        $ticketsType = Manager::getField('ticket_type');

        $args = \shortcode_atts(
            [
                'priority' => 1,
                'status' => 2,
                'group_id' => 43000436542,
                'responder_id' => 43010668056,
                'subject' => '',
                'ticket_type' => 'Frage'
            ],
            $args
        );

        // If we have POST data and we haven't already sent it
        if (isset($_POST['supr_freshdesk_token']) && !Manager::instance()->formSent) {
            $message = '';
            if (isset($_COOKIE['supr_freshdesk_token']) && $_POST['supr_freshdesk_token'] === $_COOKIE['supr_freshdesk_token']) {
                $message .= '<p class="supr-freshdesk error">' . __('You have already sent us one request.', 'supr-freshdesk') . '</p>';
            } else {
                if (!empty($_POST['robot'])) {
                    $message .= '<p class="supr-freshdesk error">' . __('You are a robot.', 'supr-freshdesk') . '</p>';
                } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                    $message .= '<p class="supr-freshdesk error">' . __('Email is not valid.', 'supr-freshdesk') . '</p>';
                } elseif (empty($_POST['subject'])) {
                    $message .= '<p class="supr-freshdesk error">' . __('Subject is empty.', 'supr-freshdesk') . '</p>';
                } elseif (empty($_POST['description'])) {
                    $message .= '<p class="supr-freshdesk error">' . __('Message is empty.', 'supr-freshdesk') . '</p>';
                } elseif (!isset($_POST['legal'])) {
                    $message .= '<p class="supr-freshdesk error">' . __('You must accept terms and conditions.', 'supr-freshdesk') . '</p>';
                } else {
                    // Save token and submit data
                    setcookie('supr_freshdesk_token', $_POST['supr_freshdesk_token'], time() + 3600);

                    if (isset($_FILES['attachments'])) {
                        $attachments = [];

                        foreach ($_FILES['attachments'] as $param => $values) {
                            foreach ($values as $num => $value) {
                                if (!isset($attachments[$num])) {
                                    $attachments[$num] = [];
                                }

                                $attachments[$num][$param] = $value;
                            }
                        }
                    }

                    $ticket = Manager::createTicket(
                        [
                            'subject' => $_POST['subject'],
                            'description' => $_POST['description'],
                            'name' => trim($_POST['salutation'] . ' ' . $_POST['firstName'] . ' ' . $_POST['lastName']),
                            'email' => $_POST['email'],
                            'status' => $_POST['status'],
                            'priority' => $_POST['priority'],
                            'ticket_type' => $_POST['ticket_type'],
                            'group_id' => $_POST['group_id'],
                            'responder_id' => $_POST['responder_id'],
                            'attachments' => $attachments ?? null
                        ]
                    );

                    if (\is_array($ticket)) {
                        $message .= '<p class="supr-freshdesk success">' . str_replace(
                            '[number]',
                            $ticket['id'],
                            __(
                                'The form has been submitted successfully. Your request has been registered under the number #[number].',
                                'supr-freshdesk'
                            )
                        ) . '</p>';
                        Manager::instance()->formSent = true; // global
                    } else {
                        $message .= '<p class="supr-freshdesk error">' . __('An error has occurred. Please try later.', 'supr-freshdesk') . '</p>';
                    }
                }
            }

            Manager::instance()->formMessage = $message;
        }

        // Print errors
        if (!empty(Manager::instance()->formMessage)) {
            echo Manager::instance()->formMessage;
        }

        if (!Manager::instance()->formSent) : ?>
            <form method="post" id="supr-freshdesk" class="supr-freshdesk" enctype="multipart/form-data">
                <input type="hidden" name="priority" value="<?= $args['priority']; ?>">
                <input type="text" style="display: none;" name="robot">
                <input type="hidden" name="status" value="<?= $args['status']; ?>">
                <input type="hidden" name="group_id" value="<?= $args['group_id']; ?>">
                <input type="hidden" name="responder_id" value="<?= $args['responder_id']; ?>">
                <input type="hidden" name="supr_freshdesk_token" value="<?= random_int(10000, 99999); ?>>">
                <div class="supr-freshdesk-input-row">
                    <label for="supr-freshdesk-ticket-type"><?= __('Kind of problem', 'supr-freshdesk'); ?></label>
                    <select id="supr-freshdesk-ticket-type" class="supr-freshdesk-select" name="ticket_type">
                        <?php
                        foreach ($ticketsType['choices'] as $value) {
                            echo '<option value=\'' . $value . '\'' . (($value === ($_POST['ticket_type'] ?? $args['ticket_type'])) ? ' selected' : '') . '>' . __(
                                $value,
                                'supr-freshdesk'
                            ) . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="supr-freshdesk-input-row">
                    <label for="supr-freshdesk-subject"><?= __('Subject', 'supr-freshdesk'); ?></label>
                    <input type="text" id="supr-freshdesk-subject" class="supr-freshdesk-input" name="subject" value="<?= $_POST['subject'] ?? $args['subject']; ?>">
                </div>
                <div class="supr-freshdesk-input-row">
                    <label for="supr-freshdesk-salutation"><?= __('Salutation', 'supr-freshdesk'); ?></label>
                    <select id="supr-freshdesk-salutation" class="supr-freshdesk-select" name="salutation">
                        <option value="<?= __('Mr.', 'supr-freshdesk'); ?>"><?= __('Mr.', 'supr-freshdesk'); ?></option>
                        <option value="<?= __('Ms.', 'supr-freshdesk'); ?>"><?= __('Ms.', 'supr-freshdesk'); ?></option>
                        <option value="<?= __('Dr.', 'supr-freshdesk'); ?>"><?= __('Dr.', 'supr-freshdesk'); ?></option>
                    </select>
                </div>
                <div class="supr-freshdesk-input-row">
                    <label for="supr-freshdesk-first-name"><?= __('First name', 'supr-freshdesk'); ?></label>
                    <input type="text" id="supr-freshdesk-first-name" class="supr-freshdesk-input" name="firstName" value="<?= $_POST['firstName'] ?? ''; ?>">
                </div>
                <div class="supr-freshdesk-input-row">
                    <label for="supr-freshdesk-last-name"><?= __('Last name', 'supr-freshdesk'); ?></label>
                    <input type="text" id="supr-freshdesk-last-name" class="supr-freshdesk-input" name="lastName" value="<?= $_POST['lastName'] ?? ''; ?>">
                </div>
                <div class="supr-freshdesk-input-row">
                    <label for="supr-freshdesk-email"><?= __('E-Mail-Adresse', 'supr-freshdesk'); ?></label>
                    <input type="email" id="supr-freshdesk-email" class="supr-freshdesk-input" name="email" value="<?= $_POST['email'] ?? ''; ?>">
                </div>
                <div class="supr-freshdesk-input-row">
                    <label for="supr-freshdesk-message"><?= __('Nachricht', 'supr-freshdesk'); ?></label>
                    <textarea id="supr-freshdesk-message" class="supr-freshdesk-input supr-freshdesk-textarea" name="description"><?= $_POST['description'] ?? ''; ?></textarea>
                </div>
                <!--
                <div class="supr-freshdesk-input-row">
                    <label for="supr-freshdesk-attachment"><?= __('Attachment', 'supr-freshdesk'); ?></label>
                    <input type="file" multiple id="supr-freshdesk-attachment" class="supr-freshdesk-input" name="attachments[]" value="<?= $_POST['attachments'] ?? ''; ?>">
                </div>
                -->
                <div class="supr-freshdesk-input-row">
                    <input type="checkbox" id="supr-freshdesk-legal" class="supr-freshdesk-checkbox" name="legal">
                    <label for="supr-freshdesk-legal"><?= stripslashes($pluginOptions['legal'] ?? __('Accept terms and conditions', 'supr-freshdesk')); ?></label>
                </div>
                <div class="supr-freshdesk-input-row">
                    <button type="submit" id="supr-freshdesk-send" class="button supr-freshdesk-button">
                        <?= __('Absenden', 'supr-freshdesk'); ?>
                    </button>
                </div>
            </form>
            <?php
        endif;

        return ob_get_clean();
    }
}