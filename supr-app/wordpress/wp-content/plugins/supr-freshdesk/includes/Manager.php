<?php

namespace SuprFreshDesk;

/**
 * Class Manager
 *
 * @package SuprFreshDesk
 */
class Manager
{
    /**
     * @var Manager
     */
    public static $instance;

    private $apiKey;

    private $password;

    private $domain;

    // We can have 2 Forms on one page. And this variable check id, if we have already sent data.
    public $formSent = false;
    public $formMessage = '';

    public $shopsWithoutPlan = [1, 8];

    /**
     * Manager constructor.
     */
    public function __construct()
    {
        $options = \is_multisite() ? \get_blog_option(1, 'supr_freshdesk_options') : \get_option('supr_freshdesk_options');

        $this->apiKey = $options['api_key'] ?? null;
        $this->password = $options['password'] ?? null;
        $this->domain = $options['domain'] ?? null;
    }

    /**
     * @return string
     */
    private function getMainUrl(): string
    {
        return "https://{$this->domain}.freshdesk.com/api/v2/";
    }

    /**
     * @return bool
     */
    public function hasApiKey(): bool
    {
        return $this->apiKey !== null;
    }

    /**
     * @return Manager
     */
    public static function instance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $shopId
     * @return null|\WP_User
     */
    public static function getUserByShopId($shopId): ?\WP_User
    {
        global $wpdb;

        $userId = null;
        $user = null;

        if (\class_exists('\WU_Site_Owner')) {
            $tableName = \WU_Site_Owner::get_table_name();
            $result = $wpdb->get_row("SELECT user_id FROM {$tableName} WHERE site_id = {$shopId}");

            if ($result) {
                $userId = (int)$result->user_id;
            }
        }

        if (!$userId) {
            $tableName = $wpdb->base_prefix . 'usermeta';
            $querystring = "SELECT user_id FROM {$tableName} WHERE (meta_key LIKE 'primary_blog' AND meta_value LIKE '{$shopId}') LIMIT 1";
            $userId = (int)$wpdb->get_var($querystring);
        }

        if ($userId) {
            $user = \get_user_by('id', $userId);

            // Fix the return type, because we can't handle mixed types!
            if (\is_bool($user)) {
                return null;
            }
        }

        return $user;
    }

    /**
     * @param $userId
     * @return false|\WP_Site
     */
    public static function getShopByUserId($userId)
    {
        global $wpdb;
        $shopId = null;

        if (\class_exists('\WU_Site_Owner')) {
            $tableName = \WU_Site_Owner::get_table_name();
            $result = $wpdb->get_row("SELECT site_id FROM {$tableName} WHERE user_id = {$userId}");
            $shopId = $result ? $result->site_id : null;
        } else {
            $shops = \get_blogs_of_user($userId);
            if (\is_array($shops) && !empty($shops)) {
                $shopId = $shops[0]->site_id;
            }
        }

        if ($shopId === null) {
            return false;
        }

        return \get_blog_details(['blog_id' => $shopId], true);
    }

    /**
     * @param null|int|\WP_Site $shop
     * @param null|string       $wuPlan
     * @param null|string       $status
     * @return null|array
     */
    public static function getShopData($shop = null, $wuPlan = null, $status = null): ?array
    {
        if ($shop instanceof \WP_Site) {
            // All is ok
        } else {
            $shopId = \is_int($shop) ? $shop : \get_current_blog_id();
            $shop = \get_blog_details(['blog_id' => $shopId], true);
        }

        // Get user of shop
        $user = self::getUserByShopId($shop->id);

        // There is not any user
        if ($user === null || (int)$user->ID === 0) {
            return null;
        }

        $params = [
            'email' => \get_blog_option($shop->id, 'woocommerce_store_owner_email', $user->user_email),
            'name' => \get_blog_option($shop->id, 'woocommerce_store_owner_first_name', $user->first_name) . ' ' . \get_blog_option($shop->id, 'woocommerce_store_owner_last_name', $user->last_name),
            'address' => \get_blog_option($shop->id, 'woocommerce_store_postcode', '-') . ' ' . \get_blog_option($shop->id, 'woocommerce_store_city', '-') . ' ' . \get_blog_option(
                $shop->id,
                'woocommerce_store_address',
                '-'
            ),
            'custom_fields' => [],
            //'language' => strtolower(substr(get_locale(), 0, 2)),
        ];

        // It is required field #S2-778
        if (empty(trim($params['name']))) {
            $params['name'] = $shop->blogname;
        }

        if ($wuPlan === null) {
            if (\function_exists('\wu_get_account_plan')) {
                $wuPlan = \wu_get_account_plan($shop->id);
            }

            if (!$wuPlan) {
                // Write log
                if (!\in_array($shop->id, self::instance()->shopsWithoutPlan, true)) {
                    error_log('[Freshdesk] Plan for shop #' . \get_current_blog_id() . ' doesn\'t exist.');
                }

                $wuPlan = 'none';
            }
        }

        // Get custom fields
        $params['custom_fields']['shop'] = $shop->blogname;
        $params['custom_fields']['package'] = $wuPlan;
        $params['custom_fields']['signup_date'] = $user->user_registered ? date('Y-m-d', strtotime($user->user_registered)) : date('Y-m-d');
        $params['custom_fields']['last_update'] = date('Y-m-d');
        // There is in docs, but the error comes back: Unexpected/invalid field in request
        //$params['custom_fields']['company_name'] = \get_blog_option($shop->id, 'woocommerce_store_owner_company', '');

        switch_to_blog($shop->id);
        $params['custom_fields']['orders'] = (int)wc_orders_count('completed');
        $params['custom_fields']['products'] = (int)wp_count_posts('product')->publish;
        restore_current_blog();

        $params['custom_fields']['shop_id'] = $shop->id;
        $params['custom_fields']['shop_url'] = $shop->siteurl;
        $params['custom_fields']['main_blog'] = \get_network()->domain;

        $shopStatus = [];

        // If status was already detected
        if ($status !== null) {
            $shopStatus[] = $status;
        } else {
            // Get statuses of shop
            if (get_blog_option($shop->id, 'elementor_maintenance_mode_mode', null)) {
                $shopStatus[] = 'Maintenance mode';
            }

            if ((int)$shop->public === 0) {
                $shopStatus[] = 'Closed for search engine';
            }

            if ($shop->archived) {
                $shopStatus[] = 'Archived';
            }

            if ($shop->mature) {
                $shopStatus[] = 'Mature';
            }

            if ($shop->spam) {
                $shopStatus[] = 'Spam';
            }

            if ($shop->deleted) {
                $shopStatus[] = 'Deleted';
            }

            if (\count($shopStatus) === 0) {
                $shopStatus[] = 'Public';
            }
        }

        $params['custom_fields']['shop_status'] = implode(', ', $shopStatus);

        return $params;
    }

    /**
     * Sanitize data before send to API
     *
     * @param array $params
     * @return array
     */
    public static function sanitizeData(array $params): array
    {
        // Error: /,",www. not allowed in name
        if (isset($params['name'])) {
            $params['name'] = str_replace(['/', '"', 'www', '.'], '', $params['name']);
        }

        if (isset($params['email'])) {
            $params['email'] = \sanitize_email($params['email']);
        }

        return $params;
    }

    /**
     * @param string $fieldName
     * @return mixed|null
     */
    public static function getField($fieldName)
    {
        $result = self::instance()->sendRequest('GET', [], 'ticket_fields');

        if ($result === false) {
            return null;
        }

        foreach ($result as $field) {
            if ($field['name'] === $fieldName) {
                return $field;
            }
        }

        return null;
    }

    public static function onDeleteBlog($shop): bool
    {
        return self::updateBlog($shop, null, 'Deleted');
    }

    /**
     * Send data to FD about current shop
     *
     * @param null|\WP_Site|int $shop
     * @param null|string       $wuPlan
     * @param null|string       $status
     * @return bool
     */
    public static function updateBlog($shop = null, $wuPlan = null, $status = null): bool
    {
        // Set cookie for 1 hour, after that we will try again
        self::setCookie('PT1H');

        if (!self::instance()->hasApiKey()) {
            error_log('[Freshdesk] Api key doesn\'t exist.');

            return false;
        }

        if ($shop instanceof \WP_Site) {
            // All is ok
        } elseif (\is_int($shop)) {
            $shop = \get_blog_details(['blog_id' => $shop], true);
        } else {
            $shop = \get_blog_details(['blog_id' => \get_current_blog_id()], true);
        }

        // We have all test-shops in black list
        if (self::inBlackList($shop->domain) || \in_array($shop->id, self::instance()->shopsWithoutPlan, true)) {
            return false;
        }

        // For main blog we don't need Fresh Sales
        if ($shop->id === 1 && \is_multisite()) {
            return true;
        }

        $params = self::getShopData($shop, $wuPlan, $status);
        $params = self::sanitizeData($params);

        // If we don't have a data jet
        if ($params === null || (empty($params['email']) && empty($params['name']))) {
            return false;
        }

        // Check email
        if (!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
            error_log('[Freshdesk] Email ' . $params['email'] . ' for the shop ' . \get_bloginfo('url') . ' is not valid.');

            return false;
        }

        // Check name
        if (empty(trim($params['name']))) {
            error_log('[Freshdesk] The name of user ' . $params['email'] . ' for the shop ' . \get_bloginfo('url') . ' is empty.');

            return false;
        }

        // Try to get a contact id from blog-options
        $freshdeskUserId = \get_blog_option($shop->id, Plugin::$clientIdOptionName);

        // Try immediately to update
        if ($freshdeskUserId) {
            $result = self::instance()->sendRequest('PUT', $params, 'contacts/' . $freshdeskUserId);
            if ($result) {
                self::setCookie();

                return true;
            }
        }

        // Try to get a contact by email if not in blog options or if update was wrong
        $existedContacts = self::instance()->sendRequest('GET', ['email' => $params['email']], 'contacts');
        if (\is_array($existedContacts) && !empty($existedContacts)) {
            $freshdeskUserId = $existedContacts[0]['id'];
            // Update option for next time
            \update_option(Plugin::$clientIdOptionName, $freshdeskUserId);
            self::setCookie();

            return true;
        }

        // Create a new contact
        $newContact = self::instance()->sendRequest('POST', $params, 'contacts');
        // Set id of user from freshdesk
        if (isset($newContact['id'])) {
            \update_option(Plugin::$clientIdOptionName, $newContact['id']);
            self::setCookie();

            return true;
        }

        error_log('[Freshdesk] Can\'t update info of the shop ' . \get_bloginfo('url') . ' (' . $params['email'] . '):' . print_r($newContact, true));

        return false;
    }

    /**
     * Set cookie (shop was updated)
     *
     * @param string $dateInterval
     * @throws \Exception
     */
    public static function setCookie($dateInterval = 'P1D'): void
    {
        setcookie(Plugin::$cookieName, '1', (new \DateTime())->add(new \DateInterval($dateInterval))->getTimestamp());  /* expired after 1 day */
    }

    /**
     * @param array            $object
     * @param \WU_Subscription $subscription
     *
     * @return array
     */
    public static function updateBlogByPlan($object, $subscription): ?array
    {
        $shop = self::getShopByUserId($subscription->user_id);

        // If we cannot get a shop
        if ($shop === false) {
            return $object;
        }

        $wuPlan = $subscription->get_plan()->title;

        self::updateBlog($shop, $wuPlan);

        return $object;
    }

    /**
     * @param $params
     * @return array|bool|mixed|object
     */
    public static function createTicket($params)
    {
        $paramsToSend = [
            'subject' => htmlspecialchars($params['subject']),
            'description' => htmlspecialchars($params['description']),
            'email' => $params['email'],
            'status' => (int)$params['status'],
            'priority' => (int)$params['priority'],
            'type' => htmlspecialchars($params['ticket_type']),
            'group_id' => (int)$params['group_id'],
            'responder_id' => (int)$params['responder_id'],
            'name' => (string)$params['name']
        ];

        // @todo check it.
        // Example: https://github.com/freshdesk/fresh-samples/blob/master/PHP/create_ticket_with_attachment.php
        // Docs: https://developers.freshdesk.com/api/#tickets
        // Freshdesk: https://supr-wcd.freshdesk.com/a/tickets
        // Try witch poor curl: https://developers.freshdesk.com/api/#create_ticket_with_attachment

        // First variant
        /*if (isset($params['attachments'])) {
            $paramsToSend['attachments'] = $params['attachments'];
        }*/

        // Curl variant
        /*if ($params['attachments'] && \is_array($params['attachments'])) {
            $paramsToSend['attachments'] = [];

            // Convert to curl
            foreach ($params['attachments'] as $attachment) {
                $paramsToSend['attachments'][] = "https://www.w3schools.com/w3images/fjords.jpg";
                //$paramsToSend['attachments'][] = $attachment['tmp_name'];
                //$paramsToSend['attachments'][] = new \CURLFile($attachment['tmp_name'], $attachment['type']);
            }

            //var_dump(curl_file_create("data/x.png", "image/png", "x.png")); exit;
            //var_dump($params['name']); exit;
            //var_dump($params['attachments']); exit;
        }*/

        //$paramsToSend['attachments'] = curl_file_create("data/x.png", "image/png", "x.png");

        return self::instance()->sendRequest('POST', $paramsToSend, 'tickets');
    }

    /**
     * @param string $type
     * @param array  $data
     * @param        $url
     * @return array|bool|mixed|object
     */
    private function sendRequest($type, $data, $url)
    {
        $url = $this->getMainUrl() . $url;

        // Add GET params
        if ($type === 'GET' && \count($data) > 0) {
            $url .= '?' . http_build_query($data);
        }

        $ch = curl_init($url);
        $header[] = 'Content-type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);

        switch ($type) {
            case 'GET':
                break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($ch, CURLOPT_HEADER, true);
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                break;
        }

        curl_setopt($ch, CURLOPT_USERPWD, "{$this->apiKey}:{$this->password}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        try {
            $serverOutput = curl_exec($ch);
        } catch (\Exception $e) {
            error_log("[Freshdesk] Cannot connect to API: {$e->getMessage()}");

            return false;
        }

        $info = curl_getinfo($ch);
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = '';
        curl_close($ch);

        $response = json_decode($serverOutput, true);

        // Hack: we get from server always different responses :( One time with headers, another time without headers.
        if (json_last_error() !== JSON_ERROR_NONE) {
            $headers = substr($serverOutput, 0, $headerSize);
            $response = json_decode(substr($serverOutput, $headerSize), true);
        }

        if ($info['http_code'] >= 200 && $info['http_code'] < 300) {
            return $response;
        }

        if ($info['http_code'] === 404) {
            error_log("[Freshdesk] Url is false\nHTTP Status Code: {$info['http_code']}\nUrl: {$url}");

            return false;
        }

        error_log(
            '[Freshdesk] Error in the shop ' . \get_bloginfo('url') . "\nUrl: {$url}\nData: " . print_r($data, true) . "\nHTTP Status Code: " . $info['http_code'] . "\nHeaders:" . print_r(
                $headers,
                true
            ) . "\nResponse" . print_r($response, true)
        );

        //var_dump('[Freshdesk] Error, HTTP Status Code: ' . $info['http_code'] . "\nHeaders:" . print_r($headers, true) . "\nResponse" . print_r($response, true));

        return false;
    }

    /**
     * @param string $shopDomain
     * @return bool
     */
    public static function inBlackList($shopDomain): bool
    {
        $options = \is_multisite() ? \get_blog_option(1, 'supr_freshdesk_options') : \get_option('supr_freshdesk_options');

        if (!empty($options['not_sync_domain'])) {
            // If regexp
            if (@preg_match($options['not_sync_domain'], null) !== false) {
                preg_match($options['not_sync_domain'], $shopDomain, $matches);

                return \count($matches) > 0;
            }

            // If string
            return strpos($shopDomain, $options['not_sync_domain']) !== false;
        }

        return false;
    }
}
