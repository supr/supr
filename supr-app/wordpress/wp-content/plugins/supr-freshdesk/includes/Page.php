<?php

namespace SuprFreshDesk;

/**
 * Class Page
 *
 * @package SuprFreshDesk
 */
class Page
{
    /**
     * Show setting page
     */
    public static function settingsPage(): void
    {
        include SUPR_FRESHDESK_PATH . 'templates/settings-page.php';
    }
}
