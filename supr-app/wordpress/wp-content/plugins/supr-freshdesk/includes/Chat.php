<?php

namespace SuprFreshDesk;

/**
 * Class Chat
 *
 * @package SuprFreshDesk
 */
class Chat
{
    /**
     * @var null
     */
    public static $instance;

    private $token;

    private $active;

    /**
     * Chat constructor.
     */
    public function __construct()
    {
        $options = is_multisite() ? get_blog_option(1, 'supr_freshdesk_options') : get_option('supr_freshdesk_options');

        $this->token = $options['chat_token'] ?? null;
        $this->active = (int)($options['chat_active'] ?? 0);
    }

    /**
     * @return \SuprFreshDesk\Chat
     */
    public static function instance(): Chat
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active > 0 && !empty($this->token);
    }

    /**
     * @return string
     */
    public function getToken(): ?string
    {
        return $this->token;
    }
}
