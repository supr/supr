<?php

namespace SuprFreshDesk;

/**
 * Class ResourceManager
 *
 * @package SuprFreshDesk
 */
class ResourceManager
{
    /**
     * Register files for plugin
     */
    public static function addChatResources(): void
    {
        // JS and CSS
        wp_register_script('supr_freshdesk_chat_src_js', 'https://wchat.freshchat.com/js/widget.js', [], false, true);
        wp_register_script('supr_freshdesk_chat_js', SUPR_FRESHDESK_URL . 'js/supr-freshdesk-chat.js', ['jquery'], '1.0.0', true);
        // Use the registered jquery and style above
        add_action('admin_enqueue_scripts', [__CLASS__, 'enqueueFiles']);
    }

    /**
     * Add files
     */
    public static function enqueueFiles(): void
    {
        wp_enqueue_script('supr_freshdesk_chat_src_js');
        wp_enqueue_script('supr_freshdesk_chat_js');
    }

    /**
     * Add js variables to js script
     */
    public static function addChatJsVariables(): void
    {
        // Get user of shop
        $user = Manager::getUserByShopId(get_current_blog_id());
        $shop = get_blog_details(['blog_id' => get_current_blog_id()], true);

        $translation_array = [
            'token' => Chat::instance()->getToken(),
            'external_id' => $shop->id,
            'first_name' => get_option('woocommerce_store_owner_first_name', $user ? $user->first_name : ''),
            'email' => get_option('woocommerce_store_owner_email', $user ? $user->user_email : ''),
            'shop_name' => $shop->blogname,
            'shop_url' => $shop->siteurl
        ];

        wp_localize_script('supr_freshdesk_chat_js', 'values', $translation_array);
    }
}
