=== Protected Shops ===
Contributors: conpark
Author: conpark GmbH
Author URI: http://www.conpark.de
Plugin URI: http://www.protectedshops.de/
Tags: impressum, agb, widerrufsbelehrung
Requires at least: 3.0.1
Tested up to: 3.9.1
Stable tag: trunk
Version 2.0

Wordpress Plugin für Zugriff auf die API von Protected Shops

== Description ==

Die Protected Shops GmbH bietet einen Service zur fragebogengestützten und individuellen Erstellung von AGB, Widerrufsbelehrung, Impressum, Datenschutzerklärung und ggf. von anderen Dokumenten an und aktualisiert diese Dokumente bei Änderung der rechtlichen Lage. Die Änderungen, die nach Urteilen oder Gesetzesänderungen regelmäßig notwendig werden, sorgen dafür, dass man immer auf dem neuesten Stand ist und man keine Angriffsfläche für Abmahnung bietet.

Das Plugin synchronisiert die Rechtstexte automatisch mit den Servern der Protected Shops GmbH. Der Administrator kann dabei auswählen, welche Dokumente genutzt werden sollen und in welchen Intervallen diese mit Wordpress synchronisiert werden.

Zu weiteren Informationen zum Service der Protected Shops GmbH besuchen Sie bitte die Seite www.protectedshops.de

== Installation ==

1. Laden Sie den Inhalt der Zip-Datei in das Verzeichnis '/wp-content/plugins/' hoch
2. Aktivieren Sie das Plugin über den Menüpunkt 'Plugins' im WordPress Adminbereich
3. Hinterlegen Sie die Shop-ID und speichern Sie die Seite.
4. Im Menü Protected Shops > Shortcodes finden Sie die passenden Shortcodes zum Einbinden in den Seiten