<?php
/**
 * Plugin Name: Protected Shops
 * Version: 2.0
 * Description: Wordpress Plugin für Zugriff auf die API von Protected Shops
 * Plugin URI: https://www.protectedshops.de/
 * Author: conpark GmbH
 * Author URI: http://www.conpark.de/
**/

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

require_once('include/inc_shortcodes.php');
require_once('include/inc_helperfunc.php');

require_once('include/inc_global.php');

register_activation_hook( __FILE__, array( 'wpps', 'installPlugin' ) );

$wpps = new wpps();

require_once('include/inc_readsettings.php');

$wpps->settings = wpps_readsettings();

require_once('include/inc_dashboard.php');

require_once('include/inc_cronjob.php');




?>