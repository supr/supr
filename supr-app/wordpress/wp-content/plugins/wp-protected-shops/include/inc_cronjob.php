<?php
// inc_cronjob.php
// Überprüfen auf fällige Aufgaben

function wpps_doTasks() {
	fullUpdate();
}

function fullUpdate() {
	global $wpps;
	// Auto Update nach angegebener Zeit
	if ((current_time('timestamp', 0) > ($wpps->settings['lastupdate'] + (3600 * $wpps->settings['interval']))) && $wpps->settings['updatetype'] == 'auto') {
		$wpps->setSilent();
		$wpps->updateAll(1);
	}
}


function usageUpdate_onSave($post_id) {
	global $wpps;

	$typestocheck = array('page');
	
	$post = get_post($post_id);
	if(in_array($post->post_type, $typestocheck)){
		$wpps->checkUsage($post_id);
	}
	

}

function usageUpdate_all() {
		$count_posts = wp_count_posts('page');

		$shortcodes = get_posts(array('posts_per_page'   => 99, 'post_type' => 'wpps-shortcodes'));
		foreach ( $shortcodes as $code ) : setup_postdata( $code ); 
				update_post_meta($code->ID, 'wpps-status', '0');
		endforeach;

		$posts = get_posts(array('posts_per_page'   => $count_posts->publish, 'post_type' => 'page'));
		foreach ( $posts as $post ) : setup_postdata( $post ); 
				usageUpdate_onSave($post->ID);
		endforeach;


}


add_action( 'init', 'wpps_doTasks' );
add_action( 'save_post', 'usageUpdate_onSave' );
add_action( 'wp_trash_post', 'usageUpdate_all' );
add_action( 'wp_untrash_post', 'usageUpdate_all' );

?>