<?php
// inc_global.php
// Bereitstellen der globalen Datenstruktur von wpps

class wpps {
	public function __construct() {
		$this->settings = false;
		$this->silentmode = false;
		$this->filesdir = plugin_dir_path( __FILE__ ) . 'files';
		$this->plugins = array();
		$this->forceupdate = false;
		$this->pleaseupdate = false;
	}

	public function setSilent() {
		$this->silentmode = true;
	}

	public function addPlugin($name) {
		$this->plugins[0] = $name;
	}

	public function updateAll($maxchanges = 99) {
		if($this->updateShortcodes($maxchanges)) {
			update_option('wpps_lastupdate', current_time('timestamp', 0));
		}
	}

	private function updateShortcodes($maxchanges){
		global $wpps;

		require_once('inc_fetchrequest.php');

		$data = getDocumentInfo();

		$updatecount = 0;
		$newcount = 0;
		$nochangecount = 0 ;
		$failcount = 0;

		$changecount = 0;

        foreach ($data as $document) {

            // Anzahl der maximalen Durchläufe begrenzen um Performance bei Autoupdate nicht zu beeinflussen
            if($changecount < $maxchanges) {

                $thisdoc = get_page_by_title($document['id'], OBJECT , 'wpps-shortcodes');

                if($thisdoc != NULL){
                    // Existiert, nur Update

                    if ((get_post_meta($thisdoc->ID, 'wpps-version', true) == $document['version'] . '/' . $wpps->settings['format']) && !$this->forceupdate) {
                        // Gleiche Version, nichts zu tun

                        $nochangecount++;
                    }else{
                        // Neue Version, Content holen

                        $doctext = $this->getDocText($document['id'], $wpps->settings['format']);
                        $dochash = $this->getDocHash($document['id'], $wpps->settings['format']);



                        if(md5($doctext) == $dochash){
                            // Checksumme passt

                            update_post_meta($thisdoc->ID, 'wpps-version', $document['version'] . '/' . $wpps->settings['format']);
                            update_post_meta($thisdoc->ID, 'wpps-content', $doctext);

                            do_action('wpps-update-document', $document);

                            $updatecount++;
                        }else{
                            // Checksum Fehler, Abbruch


                            //debug
                            //$position = strspn($doctext ^ $checksumtext, "\0");

                            $wpps->raiseError('Integrität der Daten von Dokument '.$document['id'].' konnte nicht Überprüft werden. Versuchen Sie es später noch ein mal.<br/>Checksumme des heruntergeladenen Dokuments: ' . md5($doctext) . '<br/>Vorgegeben: ' . $dochash);

                            $failcount++;
                        }

                        $changecount++;


                    }

                }else{
                    // Existiert nicht, neu erstellen

                    $newdoc = wp_insert_post(array('post_title' => $document['id'],'post_status' => 'publish', 'post_type' => 'wpps-shortcodes'));

                    $doctext = $this->getDocText($document['id'], $wpps->settings['format']);
                    $dochash = $this->getDocHash($document['id'], $wpps->settings['format']);



                    if(md5($doctext) == $dochash){
                        // Checksumme passt

                        add_post_meta($newdoc, 'wpps-version', $document['version'] . '/' . $wpps->settings['format'], true);
                        add_post_meta($newdoc, 'wpps-content', $doctext, true);
                        add_post_meta($newdoc, 'wpps-aktiv', '1', true);
                        add_post_meta($newdoc, 'wpps-status', '0', true);
                        add_post_meta($newdoc, 'wpps-lastpostid', '0', true);

                        do_action('wpps-new-document', $document);

                        $newcount++;

                    }else{
                        // Checksum Fehler, Abbruch
                        $wpps->raiseError('Integrität der Daten von Dokument '.$document['id'].' konnte nicht Überprüft werden. Versuchen Sie es später noch ein mal.');

                        $failcount++;
                    }



                    $changecount++;

                }

            }

        }

		if(($updatecount + $nochangecount + $newcount + $failcount) > 0) {
			$this->raiseSuccess('Aktualisierung abgeschlossen. <br/><br/>Aktualisiert: ' . $updatecount . '<br/>Neu: '. $newcount . '<br/>Keine &Auml;nderung: ' . $nochangecount . '<br/>Fehler: ' . $failcount);
		}

		if(($updatecount + $nochangecount + $newcount + $failcount) == sizeof($data)) {
			// Alle Dokumente durchlaufen, Update komplett

			return true;
		}

		return false;
	}

	public function getDocText($did, $format){
		global $wpps;

		$data = getDocument($did, $format);

		if($data != false) {
			if($format == 'Text') {
				// Windows Linebreaks bei Text
				return normalizeText((string)$data->Document);
			}else{
				return (string)$data->Document;
			}

		}else{
			return false;
		}

	}


	public function getDocHash($did, $format){
		global $wpps;

		$data = getDocument($did, $format);

		if($data != false) {
			return (string)$data->MD5;
		}else{
			return false;
		}

	}

	public function getShortcodeText($doc, $plaintext = false, $striptags = false) {

		$thisdoc = get_page_by_title($doc, OBJECT , 'wpps-shortcodes');
		$output = '';

		if(get_post_meta($thisdoc->ID, 'wpps-aktiv', true) == '1') {
			update_post_meta($thisdoc->ID, 'wpps-status', '1');
			update_post_meta($thisdoc->ID, 'wpps-lastpostid', get_the_ID());

			if(!$plaintext) {
				$output = get_post_meta($thisdoc->ID, 'wpps-content', true);

				if($striptags) {
					$output = preg_replace('#<br\s*/?>#i', "\n", $output);
					$output = str_replace('&nbsp;', "\n", $output);
					$output = strip_tags($output);

				}

				$output = $output . '<style>/* ProtectedShops */ div.ps_document { max-width: 60em; } div.ps_document td { vertical-align: top; } div.ps_document td.text { text-align: justify; } /* END ProtectedShops */</style>';

			}else{
				$output = get_post_meta($thisdoc->ID, 'wpps-plaintext', true);
			}


		}

		return $output;
	}

	public function installPlugin() {
		update_option('wpps_updatetype', 'auto');
		update_option('wpps_interval', '24');
		update_option('wpps_apiurl', 'https://www.protectedshops.de/api/');
		update_option('wpps_format', 'Html');
	}

	public function checkUsage($saved_post_id) {
		$saved_post = get_post($saved_post_id);

		preg_match_all('/\[wpps doc="(.*?)".*\]/i', $saved_post->post_content, $atts);

		$is_used = false;

		// Finde verlinkten Shortcode und überprüfe ob noch verwendet
		$args = array(
		'post_type' => 'wpps-shortcodes',
		'meta_query' => array(
			array(
				'key' => 'wpps-lastpostid',
				'value' => $saved_post_id
			)
		)
		);

		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post );
			foreach($atts[1] as $sinstance) {
				// Falls mehrere Shortcodes
				if($sinstance == $post->post_title) {
					$is_used = true;
				}
			}

			if(!$is_used) {
				update_post_meta($post->ID, 'wpps-status', '0');
			}else{
				update_post_meta($post->ID, 'wpps-status', '1');
			}

		endforeach;

		// Alle Shortcodes im Text finden und aktiv schalten

		foreach($atts[1] as $sinstance) {
			$usedshortcode = get_page_by_title($sinstance, OBJECT, 'wpps-shortcodes');
			update_post_meta($usedshortcode->ID, 'wpps-status', '1');
			update_post_meta($usedshortcode->ID, 'wpps-lastpostid', $saved_post_id);
		}

	}

	public function raiseSuccess($msg){
		if(!$this->silentmode && is_admin()) {
			?>
			<div class="updated"><p><strong><?php print $msg; ?></strong></p></div>
			<?php
		}

	}

	public function raiseWarning($msg){
		if(!$this->silentmode && is_admin()) {
			?>
			<div class="update-nag"><p><strong><?php print $msg; ?></strong></p></div>
			<?php
		}
	}

	public function raiseError($msg){
		if(!$this->silentmode && is_admin()) {
			?>
			<div class="error"><p><strong><?php print $msg; ?></strong></p></div>
			<?php
		}
	}
}



function wpps_init() {
	global $wpps;

	register_post_type( 'wpps-shortcodes',
		array(
			'labels' => array(
				'name' => __( 'Shortcodes' ),
				'singular_name' => __( 'Shortcode' )
			),
			'public' => true,
			'has_archive' => false,
			'rewrite' => array('slug' => 'wpps-shortcodes'),
			'supports' => array('title', 'custom-fields'),
		)
	);
}

add_action( 'init', 'wpps_init' );


?>