<?php
	if (!current_user_can('manage_options'))
    {
      wp_die( __('You do not have sufficient permissions to access this page.') );
    }

    if(isset($_POST['deactivate_shortcode'])){	
    	update_post_meta($_POST['deactivate_shortcode'], 'wpps-aktiv', '0');
    }elseif(isset($_POST['activate_shortcode'])){	
    	update_post_meta($_POST['activate_shortcode'], 'wpps-aktiv', '1');
    }

    if(isset($_GET['createpage'])) {
    	// Neue Seite mit Shortcode erstellen
    	$postargs = array(	'post_title' => $_GET['createpage'],
    						'post_content' => '[wpps doc="'.$_GET['createpage'].'"]',
    						'post_type' => 'page'
    						);

    	$newid = wp_insert_post( $postargs);
    	?>
    		<meta http-equiv="refresh" content="0; URL=post.php?post=<?php print $newid; ?>&action=edit">
    	<?php
    }
?>
<style>
button.link {
    background:none!important;
    border:none; 
    padding:0!important;
    cursor: pointer; 
    cursor: hand;
    color: #0074A2;
}

.second {
	color: #999999;
}

</style>
<div class="wrap">
	<h2>Protected Shops Shortcodes</h2>
	<p>In dieser Tabelle sehen Sie alle Ihnen zur Verfügung stehenden Dokumente. Kopieren Sie den Code in der Spalte "Shortcode" und fügen Sie ihn auf der von Ihnen gewünschten Seite ein um dort den Dokumententext einzufügen.</p>
	<p>Durch Klick auf das Icon in der Spalte "Aktiv" haben Sie die Möglichkeit nicht benötigte Dokumente zu deaktivieren um für diese keine Warnmeldungen zu erhalten.</p>

	<table class="wp-list-table widefat fixed plugins">
		<thead>
			<tr>
				<th scope="col" id="cb" class="manage-column column-cb check-column" style="width:1px;"></th>
				<th scope='col' id='title' class='manage-column column-title' style="width:15%;"><span>Dokument</span></th>
				<th scope='col' id='title' class='manage-column column-date' style="width:82%;"><span>Shortcode</span></th>
			</tr>
		</thead>

		<tbody id="the-list">
			<?php
				$query = new WP_Query('post_type=wpps-shortcodes');
	      		while ($query->have_posts()) : $query->the_post(); 

	      			


	      			if (get_post_meta(get_the_ID(), 'wpps-aktiv', true) == '1') {
	      				$aktivicon = '<form name="form1" method="post" action="">
										<input type="hidden" name="deactivate_shortcode" value="'. get_the_ID() . '" />
										<button type="submit" name="btn-deactivate" class="link" title="Diesen Shortcode deaktivieren" />
											Deaktivieren
										</button>
									</form> 

									';
	      				$rowclass = 'active';
	      				$rowtitle = '<strong>' . get_the_title() . '</strong>';


	      			}else{

	      				$aktivicon = '<form name="form1" method="post" action="">
										<input type="hidden" name="activate_shortcode" value="'. get_the_ID() . '" />
										<button type="submit" name="btn-activate" class="link" title="Diesen Shortcode aktivieren" />
											Aktivieren
										</button>
									</form>';
						$statusicon = '';
	      				$rowclass = '';
	      				$createpage = '';
	      				$rowtitle = get_the_title();
	      			}

	      			if ((get_post_meta(get_the_ID(), 'wpps-status', true) == '0') && (get_post_meta(get_the_ID(), 'wpps-aktiv', true) == '1')) {
	      				$statusrow = '<tr class="plugin-update-tr">
									    <td class="plugin-update colspanchange" colspan="3">
									        <div class="update-message">
									        	Dieses Dokument ist aktiviert aber nicht eingebunden und somit nicht im Frontend sichtbar! 
									        	<a href="admin.php?page=wpps_menu_shortcodes&createpage=' . get_the_title(get_the_ID()) . '">
												    Seite erstellen
												</a>
									        </div>
									    </td>
									</tr>';
						$rowclass = $rowclass . ' update';
	      			}else{
	      				$statusrow = '';
	      			}

	      			?>	
					<tr class="level-0 <?php print $rowclass; ?>">
						<th class="check-column" scope="row" style="width:1px;"></th>
						<td class="post-title page-title column-title">
							<?php print $rowtitle; ?>
							<div class="row-actions visible">
							    <?php print $aktivicon; ?>
							</div>

						</td>
						<td>
								<div>[wpps doc="<?php the_title(); ?>"]</div>
								<div class="second">Version: <?php print get_post_meta(get_the_ID(), 'wpps-version', true); ?></div>
						</td>
					</tr>
					<?php print $statusrow; ?>	
			<?php endwhile;  wp_reset_query(); ?>
		</tbody>

	</table>

</div>