<?php

	if (!current_user_can('manage_options'))
    {
      wp_die( __('You do not have sufficient permissions to access this page.') );
    }


    if(isset($_POST['wpps_updatetype']) && (isset($_POST['wpps_interval'])) && (isset($_POST['wpps_apiurl'])) && (isset($_POST['wpps_shopid'])) && (isset($_POST['wpps_format']))) {

        update_option('wpps_updatetype', $_POST['wpps_updatetype']);
        update_option('wpps_interval', $_POST['wpps_interval']);
        update_option('wpps_apiurl', $_POST['wpps_apiurl']);
        update_option('wpps_shopid', $_POST['wpps_shopid']);
        update_option('wpps_format', $_POST['wpps_format']);

		?>
		<div class="updated"><p><strong>Einstellungen &uuml;bernommen</strong></p></div>
		<?php

    }elseif(isset($_POST['updatenow'])){
    	
    	$wpps->updateAll();
    	
    }elseif(isset($_POST['forceupdatenow'])){
    	$wpps->forceupdate = true;
    	$wpps->updateAll();
    	
    }

	$val_updatetype = get_option('wpps_updatetype');
    $val_interval = get_option('wpps_interval');
    $val_lastupdate = get_option('wpps_lastupdate');
    $val_apiurl = get_option('wpps_apiurl');
    $val_shopid = get_option('wpps_shopid');
    $val_format = get_option('wpps_format');

    ?>
	
	<div class="wrap">
	<h2>Protected Shops Einstellungen</h2>
  


		<form name="form1" method="post" action="">

		<table class="form-table">
		<tr>
			<th scope="row">
				<label for="wpps_updatetype">Updatemethode</label>
			</th>
			<td>
				<select name='wpps_updatetype' class='postform'>
					<option class="level-0" value="auto" <?php ($val_updatetype == 'auto' ? print 'selected="selected"' : '') ?>>Automatisch</option>
					<option class="level-0" value="man" <?php ($val_updatetype == 'man' ? print 'selected="selected"' : '') ?>>Manuell</option>
				</select>
			</td>
		</tr>
		<tr>
			<th scope="row">
				<label for="wpps_interval">Update Intervall</label>
			</th>
			<td>
				<select name='wpps_interval' class='postform' >
					<option class="level-0" value="6" <?php ($val_interval == '6' ? print 'selected="selected"' : '') ?>>6 Stunden</option>
					<option class="level-0" value="12" <?php ($val_interval == '12' ? print 'selected="selected"' : '') ?>>12 Stunden</option>
					<option class="level-0" value="24" <?php ($val_interval == '24' ? print 'selected="selected"' : '') ?>>24 Stunden</option>
				</select>
			</td>
		</tr>
		<tr>
			<th scope="row">
				<label for="wpps_format">Textformat</label>
			</th>
			<td>
				<select name='wpps_format' class='postform' >
					<option class="level-0" value="Html" <?php ($val_format == 'Html' ? print 'selected="selected"' : '') ?>>HTML</option>
					<option class="level-0" value="HtmlLite" <?php ($val_format == 'HtmlLite' ? print 'selected="selected"' : '') ?>>HTML Lite</option>
					<option class="level-0" value="Text" <?php ($val_format == 'Text' ? print 'selected="selected"' : '') ?>>Text</option>
				</select>
			</td>
		</tr>
		</table>
		<hr/>
		<h2>API Einstellungen</h2>
		<table class="form-table">
		<tr>
			<th scope="row">
				<label for="wpps_apiurl">API-URL</label>
			</th>
			<td>
				<input type="text" name="wpps_apiurl" value="<?php print $val_apiurl; ?>" size="45" />
			</td>
		</tr>
		<tr>
			<th scope="row">
				<label for="wpps_shopid">Shop-Id</label>
			</th>
			<td>
				<input type="text" name="wpps_shopid" value="<?php print $val_shopid; ?>" size="45" />
			</td>
		</tr>

		</table>
		<?php

			if($val_shopid == '') {
				?>
					<div class="update-nag">
						<a href="http://www.protectedshops.de" target="_blank"><img src="<?php print plugin_dir_url('wp-protected-shops/wp-protected-shops.php').'img/protectedshops.png'; ?>" style="float:left;height:70px;margin-right:10px;margin-bottom:10px;"></a>
						<p>Die <strong>Protected Shops GmbH</strong> bietet einen Service zur fragebogengestützten und individuellen Erstellung von AGB, Widerrufsbelehrung, Impressum, Datenschutzerklärung und ggf. von anderen Dokumenten an und aktualisiert diese Dokumente bei Änderung der rechtlichen Lage. Die Änderungen, die nach Urteilen oder Gesetzesänderungen regelmäßig notwendig werden, sorgen dafür, dass man immer auf dem neuesten Stand ist und man keine Angriffsfläche für Abmahnung bietet.</p>
 
						<p>Das Plugin synchronisiert die Rechtstexte automatisch mit den Servern der Protected Shops GmbH. Der Administrator kann dabei auswählen, welche Dokumente genutzt werden sollen und in welchen Intervallen diese mit Wordpress synchronisiert werden.</p>
 
						<p>Zu weiteren Informationen zum Service der Protected Shops GmbH besuchen Sie bitte die Seite <a href="http://www.protectedshops.de" target="_blank">www.protectedshops.de</a></p></div>
				<?php
			}
		?>
		<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="Speichern" />
		</p>

		</form>
		<hr/>
		<h2>Update Status</h2>
		<form name="form2" method="post" action="">
			<p><strong>Letztes Update</strong> <?php print date('d.m.Y H:i', $val_lastupdate); ?></p>
			<?php if($val_updatetype == 'auto') { ?>
				<p><strong>N&auml;chstes geplantes Update:</strong> <?php print date('d.m.Y H:i', $val_lastupdate + (3600 * $val_interval)); ?></p>
			<?php } ?>
			<input type="hidden" name="updatenow" value="1" />
			<input type="submit" name="btn-updatenow" class="button-primary" value="Jetzt aktualisieren" />
		</form>
		<hr/>
		<h2>Komplettabgleich</h2>
		<form name="form2" method="post" action="">
			<p>Der Komplettabgleich ignoriert den Versionsabgleich von Dokumenten und forciert somit eine komplette Aktualisierung aller Dokumente.</p>
			<p>Führen Sie einen Komplettabgleich durch wenn Sie soeben ein neues Modul installiert haben oder die normale Aktualisierung Probleme bereitet. Beachten Sie, dass ein Komplettabgleich deutlich länger als eine normale Aktualisierung dauert.</p>
			<input type="hidden" name="forceupdatenow" value="1" />
			<input type="submit" name="btn-updatenow" class="button-secondary" value="Komplettabgleich" />
		</form>

		<?php
			if(sizeof($wpps->plugins) > 0) {
				?>
				<hr/>
				<h2>Aktive Module</h2>
				<p>
				<?php

				foreach($wpps->plugins as $plugin) {
					print '<div class="dashicons dashicons-yes" style="color:green;font-size:2em;"></div> ' . $plugin;
				}

				print '</p>';
			}
		?>

	</div>