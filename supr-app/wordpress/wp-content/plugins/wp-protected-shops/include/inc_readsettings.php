<?php
// inc_readsettings.php
// Auslesen der wpps Einstellungen zur globalen Verwendung

function wpps_readsettings() {
	$val['updatetype'] = get_option('wpps_updatetype');
    $val['interval'] = get_option('wpps_interval');
    $val['lastupdate'] = get_option('wpps_lastupdate');
    $val['apiurl'] = get_option('wpps_apiurl');
    $val['shopid'] = get_option('wpps_shopid');
    $val['format'] = get_option('wpps_format');

    return $val;
}