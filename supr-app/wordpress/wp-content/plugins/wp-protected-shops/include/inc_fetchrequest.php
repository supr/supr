<?php
// inc_fetchrequest.php
// Holen und aufbereiten der API Daten

function httpGet($param) {
	global $wpps;

	$param['ShopId'] = $wpps->settings['shopid'];

	$url_params = http_build_query($param);
	$full_url = $wpps->settings['apiurl'] . '?' . $url_params;

	$response = false;
	$info = false;


	if(function_exists('curl_init')) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $wpps->settings['apiurl']);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		curl_setopt($ch,CURLOPT_USERAGENT,$_SERVER['HTTP_USER_AGENT']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		if( ! $response = curl_exec($ch))
	    {
	        $response = false;

	    }

	}else{
		$wpps->raiseError('Dieses Plugin benötigt das Modul cURL um die Texte von der Protected Shops Schnittstelle abzurufen.');
	}

	return array('response' => $response, 'info' => curl_getinfo($ch, CURLINFO_HTTP_CODE));

	curl_close($ch);

}

/**
 * @return array
 */
function getDocumentInfo() {
	global $wpps;

	$http = httpGet(array('Request' => 'GetDocumentInfo'));
	$data = [];

	if($http['response'] != false && $http['info'] == "200") {
		try {
			$xml = new SimpleXMLElement($http['response']);

			if($xml->error[0]) {
				// Fehlermeldung der API Ausgeben
				$wpps->raiseError('Die Schnittstelle meldet: ' . (string)$xml->error[0]->msg);
			}else{
				// Umbau $var[key] = val -> $var[n][key] = key & $var[n][val] = val
				$i = 0;
				foreach ($xml->DocumentDate[0] as $document => $version) {
                    $data[$i] = [];
					$data[$i]['id'] = $document;
					$data[$i]['version'] = (string)$version;

					$i++;
				}
			}
		} catch (Exception $e) {
			$wpps->raiseError('Daten können momentan nicht abgerufen werden. Überprüfen sie ob der Dienst verfügbar ist und ob die eingegebene API-URL korrekt ist. HTTP-Status Code: ' . $http['info']);
		}

	}else{
		$wpps->raiseError('Daten können momentan nicht abgerufen werden. Überprüfen sie ob der Dienst verfügbar ist und ob die eingegebene API-URL korrekt ist. HTTP-Status Code: ' . $http['info']);
	}

	return $data;
}

function getDocument($did, $format) {
	global $wpps;

	$http = httpGet(array('Request' => 'GetDocument', 'Document' => $did, 'Format' => $format));


	if($http['response'] != false) {

		$xml = new SimpleXMLElement($http['response'], LIBXML_NOCDATA);

		return $xml;

	}else{
		$wpps->raiseError('Daten können momentan nicht abgerufen werden.');
		return false;

	}


}




?>
