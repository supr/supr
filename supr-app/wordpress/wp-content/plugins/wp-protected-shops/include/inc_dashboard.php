<?php
// inc_dashboard.php
// Aufbau des Wordpress-Dashboard Menü zur Backend Administration

add_action( 'admin_menu', 'wpps_init_menu' );

function wpps_init_menu() {
	add_menu_page( 'Protected Shops Setup', 'Protected Shops', 'manage_options', 'wpps_menu', 'wpps_menu_setup');
	add_submenu_page( 'wpps_menu', 'Protected Shops Shortcodes', 'Shortcodes', 'manage_options', 'wpps_menu_shortcodes', 'wpps_menu_shortcodes');
	do_action('wpps-admin-menu');
}

function wpps_menu_shortcodes() {
	global $wpps;
	require_once('pages/pg_shortcodes.php');
}

function wpps_menu_setup() {
	global $wpps;
	require_once('pages/pg_settings.php');
}

function wpps_checklist() {
	global $wpps;

	// Check ob alle Shortcodes verwendet, nur im Dashboard
	if (is_admin() ) {
	    $s_args = array(
		'post_type' => 'wpps-shortcodes',
		'meta_query' => array(
			array(
				'key' => 'wpps-aktiv',
				'value' => '1'
			),
			array(
				'key' => 'wpps-status',
				'value' => '0'
			)
		)
		);
		$s_posts = get_posts( $s_args );

		$i = 0;
		$unusedwarning = array();

		foreach ($s_posts as $post) {
			$unusedwarning[$i] = $post->post_title;
			$i++;
		}


		if(sizeof($unusedwarning) == 1) {
			$wpps->raiseWarning('Protected Shops: Dokument "' . $unusedwarning[0] . '" wird derzeit nicht verwendet. Besuchen Sie die <a href="admin.php?page=wpps_menu_shortcodes">Einstellungen</a> um den Shortcode zum Einbinden zu erfahren oder ihn zu deaktivieren.');
		}elseif(sizeof($unusedwarning) > 1) {
			$wpps->raiseWarning('Protected Shops: Mehrere Dokumente werden derzeit nicht verwendet. Besuchen Sie die <a href="admin.php?page=wpps_menu_shortcodes">Einstellungen</a> um den Shortcode zum Einbinden zu erfahren oder ihn zu deaktivieren.');			
		}


		

	}

	if($wpps->pleaseupdate OR (get_option('wpps-pleaseupdate') == 1)) {
		$wpps->raiseSuccess('Protected Shops: Sie haben ein neues Modul aktiviert. Bitte führen Sie einen Komplettabgleich Ihrer Shortcodes durch um alle Funktionen des neuen Moduls nutzen zu können.');
		update_option('wpps-pleaseupdate', '0');
	}

	
}

add_action('admin_notices', 'wpps_checklist');

?>