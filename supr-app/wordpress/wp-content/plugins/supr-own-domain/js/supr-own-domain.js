(function ($) {
    /* Add new existed domain */
    $('#button-supr-own-domain-add').on('click', function () {
        Swal.fire({
            title: supr_own_domain_values.modal_title,
            html: supr_own_domain_values.modal_text,
            input: 'text',
            inputValue: '',
            showCancelButton: true,
            confirmButtonText: supr_own_domain_values.modal_add,
            cancelButtonText: supr_own_domain_values.modal_cancel
        }).then(function (result) {
            if (result.value) {
                // Create and submit form
                var form = document.createElement('form');
                form.setAttribute('method', 'post');
                form.setAttribute('action', 'admin.php?page=supr_own_domain_settings');

                var domainField = document.createElement('input');
                domainField.setAttribute('type', 'hidden');
                domainField.setAttribute('name', 'domain');
                domainField.setAttribute('value', result.value);
                form.appendChild(domainField);

                document.body.appendChild(form);

                showLoading(true);

                form.submit();
            }
        });
    });

    /* Check if domain is free */
    $('#form-supr-own-domain-check').submit(function (e) {
        e.preventDefault();
        var domainName = $('#input-supr-own-domain-check').val();

        showLoading(true);
        $.get('/wp-json/supr-own-domain-rest/v1/check-domain', {domain: domainName}).done(function (data) {
            var resultContainer = $('#check-domains-result');
            resultContainer.addClass('show-domains');
            resultContainer.html('<h2>' + supr_own_domain_values.ud_domain_list_title + '</h2>');
            $.each(data.data.domains, function (num, val) {
                var domainRow = $('<div />').html(val.domain + '<span class="supr-own-domain-status-booked">' + supr_own_domain_values.ud_domain_list_booked + '</span><span class="supr-own-domain-status-free">' + supr_own_domain_values.ud_domain_list_free + '</span>');
                domainRow.data('domain', val.domain);
                domainRow.data('status', val.status ? '1' : '0');

                // Add class to show upsale
                if (val.status && parseInt(supr_own_domain_values.upsale) === 1) {
                    domainRow.addClass('supr-own-domain-show-upsale');
                }
                domainRow.addClass('checked-domain');
                domainRow.addClass(val.status ? 'free' : 'booked');
                resultContainer.append(domainRow);
            });

            listenDomainRegClick();
        }).fail(function () {
            showLoading(false);
            alert('Error!');
        }).always(function (value, args) {
            showLoading(false);
        });

        return false;
    });

    /* Register domain */
    function listenDomainRegClick()
    {
        "use strict";
        $('.checked-domain').click(function () {
            var domain = $(this).data('domain');
            var domainStatus = $(this).data('status');
            var acceptLegal = undefined;

            // Damain is not free
            if (parseInt(domainStatus) === 0) {
                showMessage('error', supr_own_domain_values.ud_modal_error_already_registered_title, supr_own_domain_values.ud_modal_error_already_registered_text);
                return;
            }

            // Upsale should be showed
            if (parseInt(supr_own_domain_values.upsale) === 1 && typeof supr_upsale_show_modal === 'function') {
                supr_upsale_show_modal('own_domain');

                return;
            }

            // Register new domain
            Swal.fire({
                title: supr_own_domain_values.ud_modal_title,
                icon: 'info',
                html: supr_own_domain_values.ud_modal_text.replace('[domain]', domain) + '<br><br><input type="checkbox" id="supr-own-domain-ud-accept-legal" value="1"> <label for="supr-own-domain-ud-accept-legal">' + supr_own_domain_values.ud_modal_legal_text + '</label>',
                customClass: 'animated zoomIn faster',
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: true,
                confirmButtonText: supr_own_domain_values.ud_modal_ok,
                cancelButtonText: supr_own_domain_values.ud_modal_cancel,
                showClass: {
                    popup: '',
                    backdrop: '',
                    icon: '',
                },
                hideClass: {
                    popup: '',
                    backdrop: '',
                    icon: '',
                },
                onClose: () => {
                    acceptLegal = document.getElementById("supr-own-domain-ud-accept-legal").checked
                }
            }).then(function (object) {
                if (object.value) {
                    showLoading(true);
                    jQuery.ajax({
                        url: '/wp-json/supr-own-domain-rest/v1/register-domain',
                        data: {domain: domain, acceptLegal: acceptLegal},
                        type: 'POST',
                        beforeSend: function (xhr) {
                            // Add cookies to check auth
                            xhr.setRequestHeader('X-WP-Nonce', supr_own_domain_values.nonce);
                        },
                        success: function (data) {
                            showMessage(data.success ? 'success' : 'error', supr_own_domain_values.ud_modal_success_title, data.data.message);
                        },
                        error: function (xhr, status, error) {
                            showLoading(false);
                            alert('Error!');
                        },
                        always: function (value, args) {
                            showLoading(false);
                        }
                    });
                }
            })
        });
    }

    /**
     * Type: error / success/ info
     *
     * @param $type
     * @param $title
     * @param $message
     */
    function showMessage($type, $title, $message)
    {
        Swal.fire(
            $title,
            $message,
            $type
        ).then(function (result) {
            "use strict";
            if ($type === 'success') {
                location.reload();
            }
        });
    }

    function showLoading($show)
    {
        if ($show) {
            Swal.fire({
                width: '400px',
                imageUrl: '/wp-content/plugins/supr-own-domain/img/supr-loader.gif',
                html: supr_own_domain_values.modal_loading_text,
                showConfirmButton: false,
            });
        } else {
            Swal.close();
        }
    }
})(jQuery);