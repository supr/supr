<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

// Handle request (save options)
if (isset($_POST['options_type'], $_POST['supr_own_domain_options'])) {
    switch ($_POST['options_type']) {
        case 'ud':
            // Do bool from checkbox
            $_POST['supr_own_domain_options']['ud_enabled'] = filter_var($_POST['supr_own_domain_options']['ud_enabled'] ?? false, FILTER_VALIDATE_BOOLEAN);
            // Set legal text from WP Text editor to all settings
            $_POST['supr_own_domain_options']['ud_legal_text'] = $_POST['supr_own_domain_options_ud_legal_text'];
            break;
        case 'general':
            // Validate IPS and clear
            $_POST['supr_own_domain_options']['a_ip_addresses'] = \SuprOwnDomain\Manager::instance()->clearIpAddresses($_POST['supr_own_domain_options']['a_ip_addresses']);
            break;
    }

    // Get old options
    $options = get_option('supr_own_domain_options', []);

    // Renew old options
    foreach ($_POST['supr_own_domain_options'] as $name => $value) {
        $options[$name] = $value;
    }

    // Save back
    update_option('supr_own_domain_options', $options);

    // Reload options
    \SuprOwnDomain\Manager::instance()->loadOptions(true);
}

// Handle request (change domain)
if (isset($_POST['activateDomain'], $_POST['blogId'])) {
    switch_to_blog($_POST['blogId']);
    \SuprOwnDomain\DomainManager::instance()->processPostData();
    restore_current_blog();
}

$tabs = [
    'aws' => __('AWS', 'supr-own-domain'),
    'united-domains' => __('United domains', 'supr-own-domain')
];

$currentTab = (isset($_GET['tab'], $tabs[$_GET['tab']])) ? $_GET['tab'] : 'aws';
?>

<div class="wrap supr-own-domain">
    <h2><?= get_admin_page_title(); ?></h2>
    <h2 class="nav-tab-wrapper">
        <?php foreach ($tabs as $tab => $tabName) : ?>
            <a class="nav-tab<?= $currentTab === $tab ? ' nav-tab-active' : ''; ?>" href="?page=<?= $_GET['page'] ?>&tab=<?= $tab; ?>"><?= $tabName; ?></a>
        <?php endforeach; ?>
    </h2>

    <?php

    // Include tabs template
    include SUPR_OWN_DOMAIN_PATH . "templates/settings-tabs/{$currentTab}.php";
    ?>
</div>