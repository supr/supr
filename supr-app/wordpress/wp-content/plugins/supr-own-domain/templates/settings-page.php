<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

use SuprOwnDomain\DomainManager;
use SuprOwnDomain\Manager;

?>
<div class="wrap supr-own-domain">
    <h1 class="wp-heading-inline"><?= get_admin_page_title(); ?></h1>

    <?php DomainManager::instance()->processPostData(); ?>
    <?php DomainManager::instance()->processGetData(); ?>

    <?php if (\SuprOwnDomain\Manager::instance()->getOption('ud_enabled')) : ?>
        <!-- Register new domain -->
        <div class="postbox">
            <div class="inside">
                <div class="container-fluid">
                    <div class="row">
                        <div class="wu-col-sm-12 wu-col-md-7">
                            <h2 class="text-uppercase"><?= __('Register a free domain', 'supr-own-domain') ?></h2>
                            <p><?= __('Eine eigene Domain für deinen Shop ist sehr wichtig, damit deine Kunden dich besser finden und vertrauen zu deiner Marke aufbauen.', 'supr-own-domain') ?></p>
                            <p>
                                <?= __('<strong>With our SMART or PRO plan, you can register a domain for free!</strong> Check now if your desired Domain is available with one of the following endings.', 'supr-own-domain') ?>:
                                <?= Manager::instance()->getOption('ud_check_zones') ?: '' ?>
                            </p>
                        </div>
                        <div class="wu-col-sm-12 wu-col-md-5">
                            <img class="supr-own-domain-image" src="<?= SUPR_OWN_DOMAIN_URL . 'img/own-domain-preview.png'; ?>">
                        </div>
                    </div>
                    <div id="form-supr-own-domain-check-container">
                        <div class="row">
                            <div class="wu-col-sm-12 wu-col-md-8">
                                <form id="form-supr-own-domain-check" action="">
                                    <div id="label-supr-own-domain-check"><?= __('Enter the domain you want to register', 'supr-own-domain') ?></div>
                                    <input type="text" id="input-supr-own-domain-check">
                                    <input type="submit" id="submit-supr-own-domain-check" class="button button-primary" value="<?= __('Check domain', 'supr-own-domain'); ?>">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="check-domains-result"></div>
            </div>
        </div>
    <?php endif; ?>
    <!-- Add own domain -->
    <div class="postbox">
        <div class="inside">
            <div class="container-fluid">
                <div class="row">
                    <div class="wu-col-md-12">
                        <h3><?= __('Add an existing domain', 'supr-own-domain') ?></h3>

                        <?php
                        $aIpAddresses = Manager::instance()->getOption('a_ip_addresses') ?: [];
                        echo sprintf(__('If you already own a domain from an other provider you can connect this domain with your shop by adding one or all of the following adresses as an A-record at your domain provider (A-records: %s). Then click on the button below and insert the domain name.', 'supr-own-domain'), implode(', ', $aIpAddresses));
                        ?>
                        <p><?= __('<a href="https://supr.help/konfiguration/eigene-domain-verwenden/" target="_blank">Find here a guide how to connect your domain.</a>', 'supr-own-domain'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="postbox postbox-bar">
        <div class="inside">
            <button id="button-supr-own-domain-add" class="button button-primary"><?= __('Connect existing domain', 'supr-own-domain'); ?></button>
        </div>
    </div>
    <!-- Existed domains -->
    <form action="admin.php?page=supr_own_domain_settings" method="post">
        <div class="postbox">
            <div class="inside">
                <h2><?= __('Your domains', 'supr-own-domain'); ?></h2>
                <table id="supr-own-domain-existing-domains" class="wp-list-table widefat fixed striped supr-own-domain-existing-domains">
                    <thead>
                    <tr>
                        <th scope="col" id="supr-own-domain-primary" class="manage-column supr-own-domain-column-primary"><?= __('Primary', 'supr-own-domain'); ?></th>
                        <th scope="col" id="supr-own-domain-domain" class="manage-column supr-own-domain-column-domain"><?= __('Domain', 'supr-own-domain'); ?></th>
                        <th scope="col" id="supr-own-domain-domain" class="manage-column supr-own-domain-column-options"><?= __('Options', 'supr-own-domain'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach (DomainManager::instance()->getDomainsByBlogId() as $domain) : ?>
                        <tr>
                            <td class="supr-own-domain-column-primary"><input id="primary_<?= $domain->getId(); ?>" type="radio" name="activateDomain" value="<?= $domain->getId(); ?>" <?= $domain->getPrimary() ? 'checked' : ''; ?>></td>
                            <td class="supr-own-domain-column-domain">
                                <label for="primary_<?= $domain->getId(); ?>">
                                    <?= $domain->getDomain(); ?><?= Manager::instance()->hasSsl($domain->getDomain()) ? '<span class="ssl">' . __('SSL') . '</span>' : '<span class="no-ssl">' . __('no SSL') . '</span>' ?>
                                </label>
                            </td>
                            <td class="supr-own-domain-column-options">
                                <a href="admin.php?page=supr_own_domain_settings&deleteDomain=<?= $domain->getId(); ?>" class="button button-secondary"><span class="dashicons dashicons-trash"></span></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="postbox postbox-bar">
            <div class="inside">
                <input type="submit" name="" class="button button-primary" value="<?= __('Save settings', 'supr-own-domain'); ?>">
            </div>
        </div>
    </form>
</div>