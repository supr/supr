<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

use SuprOwnDomain\DomainManager;

?>
<form action="" method="POST">
    <input type="hidden" name="options_type" value="general">
    <table class="form-table">
        <?php
        $aOpAddresses = \SuprOwnDomain\Manager::instance()->getOption('a_ip_addresses') ?: [];
        $aOpAddresses[] = '';

        foreach ($aOpAddresses as $aOpAddresse) : ?>
            <tr>
                <td><label for="supr_own_domain_options_a_ip_addresse"><?= __('IP-Address (A-Record)', 'supr-own-domain'); ?></label></td>
                <td><input type="text" id="supr_own_domain_options_a_ip_addresse" name="supr_own_domain_options[a_ip_addresses][]" value="<?= $aOpAddresse; ?>"/></td>
            </tr>
        <?php endforeach; ?>

        <tr>
            <?php $optionValue = \SuprOwnDomain\Manager::instance()->getOption('admin_email') ?? ''; ?>
            <td>
                <label for="supr_own_domain_options_admin_email">
                    <?= __('Admin email', 'supr-own-domain'); ?><br>
                    <small><?= __('We will send an email if a new domain has been registered.', 'supr-own-domain'); ?></small>
                </label>
            </td>
            <td>
                <input type="text" id="supr_own_domain_options_admin_email" name="supr_own_domain_options[admin_email]" value="<?= $optionValue; ?>"/>
            </td>
        </tr>
        <tr>
            <?php $optionValue = \SuprOwnDomain\Manager::instance()->getOption('cm_transaction_id') ?? ''; ?>
            <td>
                <label for="supr_own_domain_options_cm_transaction_id">
                    <?= __('Campaign Monitor transaction id', 'supr-own-domain'); ?><br>
                </label>
            </td>
            <td>
                <input type="text" id="supr_own_domain_options_cm_transaction_id" name="supr_own_domain_options[cm_transaction_id]" value="<?= $optionValue; ?>"/>
            </td>
        </tr>
        <tr>
            <td colspan="2"><?php submit_button(); ?></td>
        </tr>
    </table>
</form>
<?php
// Create assoc array
$blogToDomain = [];
$ownDomainsCount = 0;

foreach (\SuprOwnDomain\DomainManager::instance()->getDomains() as $domain) {
    if (!isset($blogToDomain[$domain->getBlogId()])) {
        $blogToDomain[$domain->getBlogId()] = [];
    }
    $blogToDomain[$domain->getBlogId()][] = $domain;

    // Count own domains
    if (!DomainManager::instance()->isSubdomainOfMainSite($domain->getDomain())) {
        $ownDomainsCount++;
    }
}
?>

<h2><?= sprintf(__('%d own domains', 'supr-own-domain'), $ownDomainsCount); ?></h2>

<table class="wp-list-table widefat fixed striped supr-own-domain-existing-domains">
    <tr>
        <th><?= __('Shop', 'supr-own-domain'); ?> / <?= __('Primary', 'supr-own-domain'); ?></th>
        <th class="domain-name"><?= __('Domain', 'supr-own-domain'); ?></th>
        <th><?= __('Ssl', 'supr-own-domain'); ?></th>
        <th><?= __('Ssl generated', 'supr-own-domain'); ?></th>
        <th><?= __('Domain created', 'supr-own-domain'); ?></th>
        <th title="<?= __('Registered by United Domains', 'supr-own-domain'); ?>"><?= __('UD', 'supr-own-domain'); ?></th>
        <th></th>
    </tr>

    <?php
    /** @var \SuprOwnDomain\Domain[] $domains */
    foreach ($blogToDomain as $blogId => $domains) : ?>
        <form action="" method="POST">
            <input type="hidden" name="blogId" value="<?= $blogId; ?>">
            <tr>
                <td colspan="6" class="shop-name">
                    <a title="<?= __('Go to domain settings of the shop', 'supr-own-domain'); ?>" href="<?= get_admin_url($blogId, 'admin.php?page=supr_own_domain_settings') ?>" target="_blank"><?= get_blog_details($blogId)->blogname; ?></a>
                </td>
                <td style="text-align: right"><input type="submit" name="submit" class="button button-primary" value="<?= __('Save', 'supr-own-domain'); ?>"></td>
            </tr>
            <?php foreach ($domains as $domain) : ?>
                <tr>
                    <td colspan="2">
                        <input id="primary_<?= $domain->getId(); ?>" type="radio" name="activateDomain" value="<?= $domain->getId(); ?>" <?= $domain->getPrimary() ? 'checked' : ''; ?>>
                        <label for="primary_<?= $domain->getId(); ?>">
                            <?= $domain->getDomain(); ?>
                        </label>
                    </td>
                    <td>
                        <?php
                        $sslGenerated = $domain->getSslGenerated() !== null ? new \DateTime($domain->getSslGenerated()) : null;

                        if (\SuprOwnDomain\Manager::instance()->hasSsl($domain->getDomain())) {
                            if ($sslGenerated !== null && (new \DateTime())->sub(new \DateInterval('P60D')) > $sslGenerated) {
                                echo '<span class="old-ssl">' . sprintf(__('%d days old'), (new \DateTime())->diff($sslGenerated)->format('%a')) . '</span>';
                            } else {
                                echo '<span class="ssl">' . __('SSL') . '</span>';
                            }
                        } else {
                            echo '<span class="no-ssl">' . __('no SSL') . '</span>';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($sslGenerated !== null) {
                            echo "<span title='{$sslGenerated->format('d.m.Y H:i:s')}'>{$sslGenerated->format('d.m.Y')}</span>";
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        $created = new \DateTime($domain->getCreated());
                        echo "<span title='{$created->format('d.m.Y H:i:s')}'>{$created->format('d.m.Y')}</span>";
                        ?>
                    </td>
                    <td><?= $domain->getUnitedDomains() ? __('Yes', 'supr-own-domain') : '' ?></td>
                    <td></td>
                </tr>
            <?php endforeach; ?>
        </form>
    <?php endforeach; ?>
</table>
