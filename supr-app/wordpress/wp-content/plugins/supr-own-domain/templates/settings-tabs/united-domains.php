<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();
?>
<form action="" method="POST">
    <input type="hidden" name="options_type" value="ud">
    <table class="form-table">
        <?php
        $user = \SuprOwnDomain\Manager::instance()->getOption('ud_user') ?: '';
        $password = \SuprOwnDomain\Manager::instance()->getOption('ud_password') ?: '';
        $socket = \SuprOwnDomain\Manager::instance()->getOption('ud_socket') ?: '';
        $additionalParams = \SuprOwnDomain\Manager::instance()->getOption('ud_additional_params') ?: '';
        $checkZones = \SuprOwnDomain\Manager::instance()->getOption('ud_check_zones') ?: '';
        $dns = \SuprOwnDomain\Manager::instance()->getOption('ud_dns') ?: '';
        $allowedDomainsCount = \SuprOwnDomain\Manager::instance()->getOption('ud_allowed_domains_count') ?: '';
        $legalText = \SuprOwnDomain\Manager::instance()->getOption('ud_legal_text') ?: '';
        $enabled = \SuprOwnDomain\Manager::instance()->getOption('ud_enabled') ?? true;
        ?>
        <tr>
            <th colspan="2"><?= __('United domains (API) settings', 'supr-own-domain') ?></th>
        </tr>
        <tr>
            <td><label for="supr_own_domain_options_ud_user"><?= __('User', 'supr-own-domain'); ?></label></td>
            <td><input type="text" id="supr_own_domain_options_ud_user" name="supr_own_domain_options[ud_user]" value="<?= $user; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_own_domain_options_ud_password"><?= __('Password', 'supr-own-domain'); ?></label></td>
            <td><input type="text" id="supr_own_domain_options_ud_password" name="supr_own_domain_options[ud_password]" value="<?= $password; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_own_domain_options_ud_socket"><?= __('Socket', 'supr-own-domain'); ?></label></td>
            <td><input type="text" id="supr_own_domain_options_ud_socket" name="supr_own_domain_options[ud_socket]" value="<?= $socket; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_own_domain_options_ud_additional_params"><?= __('Additional parameters', 'supr-own-domain'); ?></label></td>
            <td><input type="text" id="supr_own_domain_options_ud_additional_params" name="supr_own_domain_options[ud_additional_params]" value="<?= $additionalParams; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_own_domain_options_ud_check_zones"><?= __('Checked zones', 'supr-own-domain'); ?></label></td>
            <td><input type="text" id="supr_own_domain_options_ud_check_zones" name="supr_own_domain_options[ud_check_zones]" value="<?= $checkZones; ?>" placeholder="de,com,info"/></td>
        </tr>
        <tr>
            <td><label for="supr_own_domain_options_ud_dns"><?= __('DNS', 'supr-own-domain'); ?></label></td>
            <td><input type="text" id="supr_own_domain_options_ud_dns" name="supr_own_domain_options[ud_dns]" value="<?= $dns; ?>" placeholder="ns1a.dodns.net, ns2a.dodns.net"/></td>
        </tr>
        <tr>
            <th colspan="2"><?= __('Other settings', 'supr-own-domain') ?></th>
        </tr>
        <tr>
            <td><label for="supr_own_domain_options_ud_enabled"><?= __('Show booking dialog', 'supr-own-domain'); ?></label></td>
            <td><input type="checkbox" id="supr_own_domain_options_ud_enabled" name="supr_own_domain_options[ud_enabled]" value="true" <?= $enabled ? ' checked' : '' ?>/></td>
        </tr>
        <tr>
            <td>
                <label for="supr_own_domain_options_ud_legal_text">
                    <?= __('Legal terms', 'supr-own-domain'); ?>
                </label>
            </td>
            <td>
                <?php
                $settings = array(
                    'wpautop' => false,
                    'media_buttons' => false,
                    'textarea_rows' => 2
                );
                \wp_editor(stripslashes($legalText), 'supr_own_domain_options_ud_legal_text', $settings);
                ?>
            </td>
        </tr>
        <tr>
            <td><label for="supr_own_domain_options_ud_allowed_domains_count"><?= __('Allowed domains count', 'supr-own-domain'); ?></label></td>
            <td><input type="text" id="supr_own_domain_options_ud_allowed_domains_count" name="supr_own_domain_options[ud_allowed_domains_count]" value="<?= $allowedDomainsCount; ?>"/></td>
        </tr>
    </table>
    <?php submit_button(); ?>
    <p>* <?= __('You can see all registered domains in you account on <a href="https://domainreselling.de/" target="_blank">domainreselling.de</a>. Use please login and pass above.', 'supr-own-domain') ?></p>
    <p>** <?= __('Please don\'t forget to clear domains of deleted blogs.', 'supr-own-domain') ?></p>
</form>