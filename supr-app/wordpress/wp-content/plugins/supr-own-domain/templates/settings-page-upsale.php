<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();
?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?= get_admin_page_title(); ?></h1>
    <div class="postbox">
        <div class="inside">

            <div class="container-fluid">
                <div class="row">
                    <div class="wu-col-md-6">
                        <h2><?= __('Your own domain', 'supr-own-domain'); ?></h2>
                        <p>
                            <?= __(
                                'A own domain improves your business by making it easier to find your shop. If you have already registered a domain, you can connect it to your SUPR shop here.',
                                'supr-own-domain'
                            ); ?>
                        </p>
                        <p>
                            <strong>
                                <?= __('Upgrade now to SMART plan to connect your own domain!', 'supr-own-domain'); ?>
                            </strong>
                        </p>
                        <br>
                        <p>
                            <a href="#show-upsale" id="button-supr-own-domain-add" class="button button-primary"><?= __('Upgrade to SMART plan', 'supr-own-domain'); ?></a>
                        </p>
                    </div>
                    <div class="wu-col-md-6">
                        <img class="supr-own-domain-image" src="<?= SUPR_OWN_DOMAIN_URL . 'img/own-domain-preview.png'; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>