# SUPR Own domains

This plugin allows to have one or more own domains for one shop.

## Installation

1. Activate the plugin.

2. Configure the plugin there: `/wp-admin/network/admin.php?page=supr_own_domain_network_settings`

3. Add this code to `wp-content/sunrise.php`, in oder to have a redirect from old domain to a new domain:

```php
/**
 * SUPR OWN DOMAIN
 * Include redirect from old domains to new domains
 */
$supr_own_domain_sunrise = WP_CONTENT_DIR . '/plugins/supr-own-domain/includes/Sunrise.php';
if ( file_exists($supr_own_domain_sunrise) ) {
    require $supr_own_domain_sunrise;
}
```

## Database Store

All domains are placed in the table `supr_own_domain`:

 - id - int(11)
 - blog_id - bigint(20)
 - domain - varchar(255)
 - primary - smallint(1)
 - created - timestamp
 - changed - timestamp
 - ssl_generated - timestamp 

## SSL certificates

This plugin works with API of the cert-manager.

Cert-manager is a service, that generates Let's Encrypt ssl certificates for own domains. This service has own API. The logic and the settings are there: `includes/CertificateManager.php`.

## Regenerate of SSL

We have the cron there: `/cron/supr-own-domain-cron`. The cron is started in all Word-Press containers. The logic:

1. Lock lockfile: `/wordpress/wp-content/temp/supr-own-domain-cron.lock`
2. Delete invalid domains and switch their shops to subdomains of main blog.
3. Regenerate SSL if they are older as 60 days.
4. Unlock lockfile
5. Restart Nginx if one of domains was changed in the last 5 minutes.

Because we have checking of restart every 5 minutes, we can have this time invalid SSL certifacate on some containers. It happens after the user added own domain.

## What will be checked before new domain will be added

1. Is it not an ip address
2. Do we have valid url
3. Is it a subdomain of us
3. Do we have our a record for this domain
4. Do somebody else have this domain
5. Can we get headers

## Other

Super admin can activate domain without a valid ssl certificate.

Super admin can add / delete a subdomain as a new domain to a shop.