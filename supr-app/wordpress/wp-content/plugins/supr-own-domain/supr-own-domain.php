<?php
/**
 * Plugin Name: SUPR - Own domain
 * Plugin URI: https://supr.com
 * Description: Allow merchant to add own domain
 * Text Domain: supr-own-domain
 * Domain Path: /languages
 * Version: 3.2.1
 * Author: SUPR Development
 * License: GPL
 */

// We use it always in Sunrise.php
if (!defined('SUPR_OWN_DOMAIN_PATH')) {
    define('SUPR_OWN_DOMAIN_PATH', plugin_dir_path(__FILE__));
}

define('SUPR_OWN_DOMAIN_URL', plugins_url('/', __FILE__));
define('SUPR_OWN_DOMAIN_FILE', plugin_basename(__FILE__));
define('SUPR_OWN_DOMAIN_DIR_NAME', basename(__DIR__));

define('SUPR_OWN_DOMAIN_VERSION', '3.2.1');

// Load main Class
require SUPR_OWN_DOMAIN_PATH . 'includes/Plugin.php';

// After activation of plugin
register_activation_hook(__FILE__, [\SuprOwnDomain\Plugin::class, 'activationHook']);
