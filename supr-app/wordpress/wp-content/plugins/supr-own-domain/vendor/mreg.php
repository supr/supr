<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * mREG Socket Class
 * @author              Stefan Meinecke <meinecke@united-domains.de>
 * @author              Tobias Sattler <sattler@united-domains.de>
 * @author              Tim-Oliver Ettel <ettel@ud-reselling.com>
 * @package             mreg
 * @copyright           Copyright (c) 2016 united-domains Reselling GmbH, Starnberg.
 */

/**
 * @staticvar MAX_REQUEST_PER_SESSION Maximal count of Requests per Connection-Session, reconnect if > than this
 */
define('MAX_REQUEST_PER_SESSION', 100);
/**
 * @staticvar MAX_RECONNECT_TRY If disconnect while processing a request & reconnect count > this, errormsg
 */
define('MAX_RECONNECT_TRY', 5);
/**
 * @staticvar GLOBAL_STREAM_TIMEOUT Size of Block thats readed in one step.
 */
define('STREAM_BLOCK_SIZE', 8192);


/**
 * define mregsocket-class
 * @package       mreg
 */
class mreg extends mregTools
{

    /**
     * Class Socket-Handle
     * @var handle
     */
    private $hSocket;
    /**
     * Class Request-Count for MAX_REQUEST_PER_SESSION
     * @var integer
     */
    private $iReqCount;
    /**
     * Class Reconnect-Count for MAX_RECONNECT_TRY
     * @var integer
     */
    private $iReconnectCount;
    /**
     * $hSocket is connected?
     * @var bool
     */
    private $bConnected;
    /**
     * Configuration ( Socket-URL, use SSL .. )
     * @var array
     */
    private $hSocketHost;

    /**
     * initialize all class variables.
     * @return void
     */
    public function __construct($sApiType)
    {
        $this->iReqCount        = 0;
        $this->iReconnectCount  = 0;
        $this->hSocket          = null;
        $this->hSocketHost      = '';
        $this->bConnected       = false;
        $this->aConfig          = array();
        parent::setApiType($sApiType);
    }

    /**
    * execute a call on socket.
    *
    * @param  array $command        command to execute
    * @param  array $aConfig        config for mreg_call
    * @return mixed                 returns result of command (string) or errormsg if failed.
    */
    public function mreg_call_raw($aCommand, $aConfig = null)
    {
        if ($aConfig != null) {
            $this->aConfig = $aConfig;                  // set Config global.
        }
        $sCommand   = $this->mreg_create_command($aCommand);    // generate command-string
        // replace incorrect chars
        $aReplaceBy = array ("\r\n", "\n\r", "\r");
        $sCommand   = str_replace($aReplaceBy, '', $sCommand);
        $sCommand   = preg_replace("/[\t ]*(\n|$)[\t ]*/", "\n", $sCommand);
        $sCommand   = preg_replace("/(^|\n)\[[^\n]+(\n|$)/", "\\1", $sCommand);
        $sCommand   = preg_replace("/(^|\n)EOF(\n|$)/", "\\1", $sCommand);

        // parse url for protocol-type
        if (strpos($this->aConfig['socket'], ':') === false) {
            die('Invalid URL:'.$this->aConfig['socket']);
        }

        list($sProto, $sURL) = explode(':', $this->aConfig['socket']);
        switch (strtolower($sProto)) {
            case 'https':
                $this->aConfig['ssl'] = 1;
                return $this->mreg_call_raw_http($sCommand);
            case 'http':
                $this->aConfig['ssl'] = 0;
                return $this->mreg_call_raw_http($sCommand);
            default:
                die('Protocol unknown.');
        }
    }

    /**
     * open connection to remote socket
     * @param  string   $sHost          remote hostname
     * @return bool                     true if connection established
     */
    private function Connect($sHost)
    {
        $this->iReqCount = 0;
        $this->iReconnectCount++;
        if ($this->aConfig['ssl']) {
            $this->hSocket = stream_socket_client('ssl://'. $sHost.':443', $iErrno, $sErr, GLOBAL_STREAM_TIMEOUT);
        } else {
            $this->hSocket = stream_socket_client('tcp://'. $sHost . ':80', $iErrno, $sErr, GLOBAL_STREAM_TIMEOUT);
        }
        if (!$this->hSocket) {
            echo 'Couldn\'t connect ' . $sHost . '- '.$iErrno.':'.$sErr."\n";
            return $this->bConnected = false;
        }
        stream_set_blocking($this->hSocket, 0);
        stream_set_timeout($this->hSocket, GLOBAL_STREAM_TIMEOUT);
        $this->hSocketHost = $sHost;
        return $this->bConnected = true;
    }

    /**
     * send request HTTP header
     * @param  array    $aHeader        includes path and host
     * @param  string   $sData          data to post
     * @return void
     */
    private function mreg_send_header($aHeader, &$sData)
    {
        // send Data to socket
        if (isset($this->aConfig['GET'])) {
            fputs($this->hSocket, "GET ".$aHeader['path'] . '?' . $sData[0] . " HTTP/1.1\n");
        } else {
            fputs($this->hSocket, "POST ".$aHeader['path'] . '?' . $sData[0] . " HTTP/1.1\n");
        }
        fputs($this->hSocket, "User-Agent: " . HTTTP_USER_AGENT . "\n");
        fputs($this->hSocket, "Host: ".$aHeader['host']."\n");
        fputs($this->hSocket, "Accept: */*"."\n");
        // if last request for this session, send "close", else "keep-alive"
        if (($this->iReqCount+1) < MAX_REQUEST_PER_SESSION) {
            fputs($this->hSocket, "Connection: Keep-Alive\n");
        } else {
            fputs($this->hSocket, "Connection: Close\n");
        }
        if (!isset($this->aConfig['GET'])) {
            fputs($this->hSocket, "Content-Length: ". strlen($sData[1]) ."\n");
            fputs($this->hSocket, "Content-Type: application/x-www-form-urlencoded\n\n");
            // send data
            fputs($this->hSocket, $sData[1]);
        } else {
            fputs($this->hSocket, "\n");
        }

        // Requestcount + 1
        $this->iReqCount++;
    }

    /**
     * HTTP-socket routine - send command to host and recieve result
     * @param  string   $command        command as strign
     * @return mixed                    result of query, errormsg or false if failed.
     */
    public function mreg_call_raw_http($sCommand)
    {
        $sSocket = $this->genSocket($this->aConfig, $sCommand);
        if (!$sSocket) {                         // return if no socket defined.
            return false;
        }

        // check urlformat and export host, path and parameters
        if (preg_match('/^(http|https):\/\/([^\/]+)([^\?]+)\?(.+)/', $sSocket, $aMatch)) {
            $sHost = $aMatch[2];
            $sPath = $aMatch[3];
            $sData = array(((substr($aMatch[4], 0, 1) == '&') ? substr($aMatch[4], 1) : $aMatch[4]), $sCommand);

            // reset ReconnectCount
            $this->iReconnectCount = 0;

            // Disconnect if MAX_REQUEST_PER_SESSION exceeded
            if (($this->iReqCount >= MAX_REQUEST_PER_SESSION) &&
                (is_object($this->hSocket)) &&
                (!feof($this->hSocket))) {
                fclose($this->hSocket);
                $this->bConnected = false;
            }

            // if disconnect, socket invalid or new host, reconnect.
            if (!$this->bConnected || feof($this->hSocket) || $this->hSocketHost != $sHost) {
                if (!$this->Connect($sHost)) {
                    return("code = 999\ndescription = no socket connection!");
                }
            }

            // send header
            $this->mreg_send_header(array('path' => $sPath, 'host' => $sHost), $sData);

            // define vars
            $bHeadOver      = false;
            $sResHeader     = '';
            $sResHeadLn     = '';
            $sResBody       = '';
            $iBodyLength    = 0;
            $iOldLen        = 0;
            $bDoDisconnect  = false;
            $bDidGetData    = false;
            $bDisconnectWhileGet = false;

            // as long as connected
            while (!$bDoDisconnect) {
                // only wait if no data was recieved
                $bDidGetData = false;
                // and reconnect if disconnected.
                if (feof($this->hSocket)) {
                    $bDisconnectWhileGet = true;
                    if (!$this->Connect($sHost)) {
                        return("code = 999\ndescription = no socket connection!");
                    }
                    $this->mreg_send_header(array('path' => $sPath, 'host' => $sHost), $sData);
                    $iBodyLength    = 0;
                    $bHeadOver      = false;
                    if ($this->iReconnectCount >= MAX_RECONNECT_TRY) {
                        return("code = 998\ndescription = no socket connection! too many retries .");
                    }
                } else {
                    // split recieving by HTTP-Header and Body-Part.
                    if ($bHeadOver) {
                        // read body
                        $sResBody .= fread($this->hSocket, STREAM_BLOCK_SIZE);
                        // and set true, if something recieved
                        if ($iOldLen != strlen($sResBody)) {
                            $gotbDidGetData = true;
                            $iOldLen    = strlen($sResBody);
                        }
                        // "disconnect" => return, if request results recieved.
                        if (strlen($sResBody) >= $iBodyLength) {
                            $bDoDisconnect  = true;
                        }
                    } else {
                        // get header
                        $sResHeadLn         = fgets($this->hSocket, STREAM_BLOCK_SIZE);
                        // and set true, if something recieved
                        if (strlen($sResHeadLn) > 0) {
                            $bDidGetData    = true;
                        }
                        // header is over, when there's an "\r\n" || "\n"
                        if ($sResHeadLn == "\n" || $sResHeadLn == "\r\n") {
                            $bHeadOver      = true;
                        } else {
                            // parse Content-Length for body-size
                            if (strpos($sResHeadLn, 'Content-Length') !== false) {
                                $iBodyLength = intval(substr($sResHeadLn, 16));
                            }
                            $sResHeader     .= $sResHeadLn;
                        }
                    }
                }
                // sleep while nothing recieving
                if (!$bDidGetData) {
                    usleep(2000);
                }
            }
            // return Data-Body
            return $sResBody;
        }
        // url was invalid.
        return '';
    }
}
