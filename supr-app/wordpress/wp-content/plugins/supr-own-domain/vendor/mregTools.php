<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * mREG Class
 * @author              Stefan Meinecke <meinecke@united-domains.de>
 * @author              Tobias Sattler <sattler@united-domains.de>
 * @author              Tim-Oliver Ettel <ettel@ud-reselling.com>
 * @package             mreg
 * @copyright           Copyright (c) 2016 united-domains Reselling GmbH, Starnberg.
 */

/**
 * @staticvar GLOBAL_STREAM_TIMEOUT Timeout (in Sec.) for Socket-Stream
 */
define('GLOBAL_STREAM_TIMEOUT', 30);
/**
 * @staticvar IGNORE_SSL_WARNING Ingore SSL-Certificate Warnings
 */
define('IGNORE_SSL_WARNING', 'OFF');
/**
 * @staticvar HTTTP_USER_AGENT UserAgent to send to httpd
 */
define('HTTTP_USER_AGENT', 'libmreg/PHP');

/**
 * define mreg-class
 * @package       mreg
 */

abstract class mregTools
{
    /**
     * configuration ( URL, use SSL .. )
     * @var array
     */
    public $aConfig;

    /**
     * set api type
     * @var string
     */
    private $sApiType;

    abstract public function mreg_call_raw($aCommand, $aConfig = null);
    abstract public function __construct($sApiType);

    /**
     * @param   string      $sApiType       set API type hash or object
     * @return void
     */
    public function setApiType($sApiType)
    {
        $this->sApiType = $sApiType;
    }

    /**
     * execute a call on socket.
     *
     * @param  array        $aCommand       command to execute
     * @param  array        $aConfig        socket url to use
     * @return mixed                        returns result of command (array) or errormsg if failed.
     */
    public function mreg_call($aCommand, $aConfig)
    {
        return $this->mreg_parse_response(                  // parse result
            $this->mreg_call_raw($aCommand, $aConfig)   // run command
        );
    }

    /**
     * @param array     $aConfig
     * @param string    $sCommand
     * @return string
     */
    public function genSocket(&$aConfig, &$sCommand)
    {
        $sSocket = $aConfig['socket'];  // get global 'Socket' URL
        $bFirst = false;

        if (strpos($sSocket, '?') === false) {
            $sSocket .= '?';
            $bFirst = true;
        }

        foreach (array('s_login' => 'user', 's_pw' => 'pass', 's_user' => 'subuser') as $sKey => $sConfigKey) {
            if (isset($aConfig[$sConfigKey])) {
                $sSocket .= ($bFirst ? '' : '&') .$sKey.'='.rawurlencode($aConfig[$sConfigKey]);
                $bFirst = false;
            }
        }

        if (isset($aConfig['GET'])) {
            $sSocket .= ( $bFirst ? '' : '&' ) . $sCommand;
        }

        return $sSocket;
    }

    /**
    * add every "command-item" to one long url
    *
    * @param  array     $aCmdArr    command array
    * @return string                returns the parsed and added command string
    */
    public function mreg_create_command($aCmdArr)
    {
        if (!is_array($aCmdArr)) {
            return $aCmdArr;
        }
        $sCmd = '';
        foreach ($aCmdArr as $sKey => $sValue) {
            if (is_array($sValue)) {
                $sCmd .= $this->mreg_create_command($sValue);
            } else {
                $sValue = str_replace(array("\r","\n"), '', $sValue);
                $sCmd .= rawurlencode($sKey).'='.rawurlencode($sValue)."&";
            }
        }

        if (substr($sCmd, -1, 1) == '&') {
            $sCmd = substr($sCmd, 0, -1);
        }

        return $sCmd;
    }

    /**
    * parses the string of mreg_call_raw_http
    *
    * @param  string    $sResponse  result string of mreg_call_raw_http
    * @return mixed                 array or object with all propertys of the response
    */
    public function mreg_parse_response($sResponse)
    {
        // if no response, return empty array.
        if (empty($sResponse)) {
            if ($this->sApiType == 'hash') {
                return array();
            } else {
                return new stdClass();
            }
        }

        if ($this->sApiType == 'hash') {
            // add all entries below the 'PROPERTY' hash as hash
            $mHash = array('PROPERTY' => array());

            // split the response
            foreach (explode("\n", $sResponse) as $sItem) {
                // get all entries - but skip [RESPONSE], EOF etc.
                if (preg_match('/^([^\=]*[^\t\= ])[\t ]*=[\t ]*(.*)$/', $sItem, $aMatch)) {
                    $sAttr = strtoupper($aMatch[1]);
                    $sValue = preg_replace('/[\t ]*$/', '', $aMatch[2]);
                    $sub = &$mHash['PROPERTY'];

                    // get everything between []
                    if (preg_match_all('/\[([^]]+)\]/', $sAttr, $aMatch)) {
                        // as long as [] are there, create multi hash
                        foreach ($aMatch[1] as $elem) {
                            $elem = str_replace(array(' ', "\t"), '', strtoupper($elem));
                            // German: ich habe keine Ahnung, wer das programmiert hat, aber es riecht nach Scheiße :(
                            if (!isset($sub[$elem]) || !is_array($sub[$elem])) {
                                $sub[$elem] = array();
                            }
                            $sub = &$sub[$elem];
                        }

                        // last element reached - set value
                        $sub = $sValue;
                    } else {
                        // has no multi hashs - set value
                        $mHash[$sAttr] = $sValue;
                    }
                }
            }
        } else {
            // add all entries below the stdClass property as class, string or hash
            $mHash = new stdClass();
            $mHash->property = new stdClass();

            // split the response
            foreach (explode("\n", $sResponse) as $sItem) {
                // get all entries - but skip [RESPONSE], EOF etc.
                if (preg_match('/^([^\=]*[^\t\= ])[\t ]*=[\t ]*(.*)$/', $sItem, $aMatch)) {
                    $sAttr = strtolower($aMatch[1]);
                    $sValue = preg_replace('/[\t ]*$/', '', $aMatch[2]);
                    $sub = &$mHash->property;

                    // get everything between []
                    if (preg_match_all('/\[([^]]+)\]/', $sAttr, $aMatch)) {
                        // as long as [] are there, move on
                        foreach ($aMatch[1] as $key => $elem) {
                            $elem = str_replace(array(' ', "\t"), '', strtolower($elem));

                            // check if element is numeric then add string
                            // if the next element has the same name move things to array
                            // e.g.
                            // $myobj->property->domainname = 'mydomain.com';
                            // $myobj->property->nameserver[0] = 'ns1.mydomain.com';
                            // $myobj->property->nameserver[1] = 'ns2.mydomain.com';
                            if (is_numeric($elem)) {
                                if (is_string($sub)) {
                                    $sub = array( $sub );
                                    $sub = &$sub[$elem];
                                } elseif (is_array($sub)) {
                                    $sub = &$sub[$elem];
                                }
                            } else {
                                // thats for multi level
                                // $myobj->property->domain->name->test->foo->bar ....
                                if (!isset($sub->$elem)) {
                                    $sub->$elem = new stdClass();
                                }
                                $sub = &$sub->$elem;
                            }
                        }

                        // last element reached - set value
                        $sub = $sValue;
                    } else {
                        // has no multi hashs - set value
                        $mHash->$sAttr = $sValue;
                    }
                }
            }
        }

        // final result parsed
        return $mHash;
    }

    /**
    * parses the command-string to an array - DEPRECATED
    *
    * @param  string    $sCommand   command string to parse
    * @return array                 array with all commandpropertys
    */
    public function mreg_parse_command($sCommand)
    {
        if (is_array($sCommand)) {
            return $sCommand;
        }

        // replace incorrect chars
        $aReplaceBy = array ("\r\n", "\n\r", "\r");
        $sCommand   = str_replace($aReplaceBy, "\n", $sCommand);
        $sCommand   = preg_replace("/[\t ]*(\n|$)[\t ]*/", "\n", $sCommand);
        $sCommand   = preg_replace("/(^|\n)\[[^\n]+(\n|$)/", "\\1", $sCommand);
        $sCommand   = preg_replace("/(^|\n)EOF(\n|$)/", "\\1", $sCommand);

        // split command by breakline
        $aCmdLines = explode("\n", $sCommand);

        // split command by '=' into key and value
        $m = '';
        $aCmdArray = array();
        foreach ($aCmdLines as $sCmdLine) {
            preg_match("/^(.+?)=(.+)$/is", $sCmdLine, $m);
            $aCmdArray[trim(strtolower($m[1]))] = trim($m[2]);
        }

        // return parsed commandstring
        return $aCmdArray;
    }
} // end class mregTools
