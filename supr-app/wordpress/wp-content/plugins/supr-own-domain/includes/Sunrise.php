<?php

namespace SuprOwnDomain;

/**
 * Class Sunrise
 *
 * @package SuprOwnDomain
 */
class Sunrise
{
    /**
     * Domain of main site
     *
     * @var string
     */
    private $mainDomain;

    private $shopNotFoundPage = '/shop-nicht-gefunden/';

    /**
     * Sunrise constructor.
     */
    public function __construct()
    {
        $this->registerAutoloader();

        $this->loadMainDomain();

        $this->redirectToPrimaryDomain();

        if (\defined('COOKIE_DOMAIN')) {
            error_log('[Supr Own Domain] Constant COOKIE_DOMAIN is already defined.');
        } else {
            // Set cookies domain
            $currentDomain = $_SERVER['SERVER_NAME'];
            $currentDomainParts = explode('.', $currentDomain);
            $currentSecondLevelDomain = $currentDomainParts[\count($currentDomainParts) - 2] . '.' . $currentDomainParts[\count($currentDomainParts) - 1];

            if ($this->mainDomain === null || $currentSecondLevelDomain === $this->mainDomain) {
                // For such domain test.mysupr.de, myslug.mysupr.de: .mysupr.de
                \define('COOKIE_DOMAIN', '.' . $currentSecondLevelDomain);
            } else {
                // For own domains: example.com
                \define('COOKIE_DOMAIN', $currentDomain);
            }
        }

        // Add redirect hook
        add_filter('pre_get_site_by_path', [self::class, 'findBlogIdByDomain'], 10, 5);
        // We have a custom page for not registered shops
        \add_action('ms_site_not_found', [$this, 'redirectToShopNotFoundPage'], 10, 3);
    }

    /**
     * Load domain of main blog
     */
    private function loadMainDomain(): void
    {
        global $wpdb;

        if (defined('DOMAIN_CURRENT_SITE')) {
            $this->mainDomain = DOMAIN_CURRENT_SITE;

            return;
        }

        // Get from DB
        $sql = "SELECT * FROM {$wpdb->base_prefix}blogs WHERE `blog_id` = 1";
        $blogRow = $wpdb->get_row($sql, ARRAY_A);

        if ($blogRow && isset($blogRow['domain'])) {
            $this->mainDomain = $blogRow['domain'];
        }
    }

    /**
     * Return blog by domain name if WordPress has not found it.
     *
     * @param bool|null|\WP_Site $pre
     * @param string             $domain
     * @param string             $path
     * @param array              $segments
     * @param array              $paths
     * @return bool|null|\WP_Site
     */
    public static function findBlogIdByDomain($pre, $domain, $path, $segments, $paths)
    {
        /** @var \wpdb */
        global $wpdb;

        if ($pre === null) {
            $domainManager = DomainManager::instance($wpdb);

            // Remove www
            $domain = preg_replace('/^www\./', '', $domain);

            try {
                $ownDomain = $domainManager->getDomainByName($domain);
            } catch (\Exception $e) {
                $ownDomain = null;
                error_log('[Supr Own Domain] Error while getting domain by ' . $domain . '. Error: ' . $e->getMessage());
            }

            if ($ownDomain !== null) {
                // Load the blog
                $pre = \WP_Site::get_instance($ownDomain->getBlogId());
            }
        }

        return $pre;
    }

    /**
     * Register auto loader.
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_OWN_DOMAIN_PATH . '/includes/Autoloader.php';

        Autoloader::run();
    }

    /**
     * Redirect to primary own domain
     */
    public function redirectToPrimaryDomain(): void
    {
        global $wpdb;
        $domainManager = DomainManager::instance($wpdb);

        // Get domain
        $ownDomain = $domainManager->getDomainByName($_SERVER['SERVER_NAME']);

        // If it is not primary, try to redirect
        if ($ownDomain !== null && $ownDomain->getPrimary() === false) {
            $primaryOwnDomain = $domainManager->getPrimaryDomain($ownDomain->getBlogId());
            if ($primaryOwnDomain !== null) {
                header('Location: ' . ($_SERVER['REQUEST_SCHEME'] ?? 'https') . '://' . $primaryOwnDomain->getDomain() . $_SERVER['REQUEST_URI']);
                exit;
            }
        }
    }

    /**
     * Redirect to custom page if this page exists
     *
     * @param        $currentSite
     * @param string $domain
     * @param string $path
     */
    public function redirectToShopNotFoundPage($currentSite, string $domain, string $path): void
    {
        if ($this->shopNotFoundPage !== null) {
            header('Location: ' . $_SERVER['REQUEST_SCHEME'] . '://' . $this->mainDomain . $this->shopNotFoundPage, true, 307);
            die();
        }
    }
}

if (!\defined('SUPR_OWN_DOMAIN_PATH')) {
    \define('SUPR_OWN_DOMAIN_PATH', __DIR__ . '/../');
}

new Sunrise();
