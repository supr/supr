<?php

namespace SuprOwnDomain;

class Api
{
    /**
     * Register all REST routes for plugin functionality
     */
    public static function registerRoutes(): void
    {
        \add_action(
            'rest_api_init',
            function () {
                \register_rest_route(
                    'supr-own-domain-rest/v1',
                    '/check-domain',
                    [
                        'methods' => 'GET',
                        'callback' => [self::class, 'checkDomain'],
                        'permission_callback' => function () {
                            return true; // For all
                        }
                    ]
                );
            }
        );

        \add_action(
            'rest_api_init',
            function () {
                \register_rest_route(
                    'supr-own-domain-rest/v1',
                    '/register-domain',
                    [
                        'methods' => 'POST',
                        'callback' => [self::class, 'registerDomain'],
                        'permission_callback' => function () {
                            return \current_user_can('administrator') || \current_user_can('editor');
                        }
                    ]
                );
            }
        );
    }

    /**
     * REST: return array of domains with statuses: free or busy
     */
    public static function checkDomain(): void
    {
        $domain = isset($_GET['domain']) ? \sanitize_text_field($_GET['domain']) : null;

        if ($domain === null) {
            \wp_send_json_error(['message' => __('Domain is required.', 'supr-own-domain')]);
        }

        $domains = UnitedDomainManager::instance()->getFreeDomains($domain);

        if (!is_array($domains)) {
            \wp_send_json_error(['message' => __('An error is occurred in the domain checking process.', 'supr-own-domain')]);
        }

        $returned = [
            'domains' => $domains
        ];

        \wp_send_json_success($returned);
    }

    /**
     * REST: Register new domain for current blog
     *
     * @param $data
     * @throws \Exception
     */
    public static function registerDomain($data): void
    {
        // Get data
        $domain = isset($_POST['domain']) ? \sanitize_text_field($_POST['domain']) : null;
        $acceptLegal = isset($_POST['acceptLegal']) ? \sanitize_text_field($_POST['acceptLegal']) : null;

        if ($domain === null) {
            \wp_send_json_error(['message' => __('Domain is required.', 'supr-own-domain')]);
        }

        if (!filter_var($acceptLegal, FILTER_VALIDATE_BOOLEAN)) {
            \wp_send_json_error(['message' => __('You should accept terms and conditions.', 'supr-own-domain')]);
        }

        if (!Manager::instance()->registerDomainAllowed()) {
            \wp_send_json_error(['message' => __('Please book <b>Smart</b> or <b>Pro</b> plan.', 'supr-own-domain')]);
        }

        // Check limits of domains
        $allowedUnitedDomainsCount = Manager::instance()->getOption('ud_allowed_domains_count') ?: 1;
        $currentUnitedDomainsCount = DomainManager::instance()->getUnitedDomainsCount(\get_current_blog_id());

        if ($currentUnitedDomainsCount >= $allowedUnitedDomainsCount) {
            \wp_send_json_error(['message' => sprintf(__('You have already booked %d from %d possible domains. ', 'supr-own-domain'), $currentUnitedDomainsCount, $allowedUnitedDomainsCount)]);
        }

        // Create contact
        $user = \wp_get_current_user();

        $params = [
            'firstname' => \get_option('woocommerce_store_owner_first_name', $user->first_name),
            'lastname' => \get_option('woocommerce_store_owner_last_name', $user->last_name),
            'street' => \get_option('woocommerce_store_address', '-'),
            'zip' => \get_option('woocommerce_store_postcode', '-'),
            'city' => \get_option('woocommerce_store_city', '-'),
            'country' => \get_option('woocommerce_default_country', '-'),
            'phone' => \get_option('woocommerce_store_owner_phone', '-'),
            'email' => \get_option('woocommerce_store_owner_email', $user->user_email)
        ];

        // Sanitize data according to UD rules
        $params['phone'] = preg_replace('/[^\d+.x-]/', '', $params['phone']);
        $params['email'] = \sanitize_email($params['email']);

        $contact = UnitedDomainManager::instance()->createContact($params);

        if ($contact === null) {
            \wp_send_json_error(['message' => __("We cannot provide your contact data for this domain. Please fill it at first on <a target='_blank' class='rot bold' href='/wp-admin/admin.php?page=wc-settings'>this page</a>. The following data must be filled in correctly: firstname, lastname, street, zip, city, country, phone and email.", 'supr-own-domain')]);
        }

        // Register domain
        if (!UnitedDomainManager::instance()->registerDomain($contact, $domain)) {
            \wp_send_json_error(['message' => __('An error is occurred in the domain registering process.', 'supr-own-domain')]);
        }

        // Add to the domains
        $domainObject = new Domain();
        $domainObject->setBlogId(\get_current_blog_id())
            ->setDomain($domain)
            ->setUnitedDomains(true)
            ->setPrimary(false);

        DomainManager::instance()->save($domainObject);

        \do_action('supr_own_domain_ud_booked', \get_current_blog_id(), $domain);

        \wp_send_json_success(['message' => sprintf(__('Domain %s was registered and configured successfully. After 2 hours you can set it below on this page as a primary domain of your shop.', 'supr-own-domain'), $domain)]);
    }
}
