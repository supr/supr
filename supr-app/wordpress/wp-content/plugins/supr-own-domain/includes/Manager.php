<?php

namespace SuprOwnDomain;

/**
 * Class Manager
 *
 * @package SuprOwnDomain
 */
class Manager
{
    /**
     * @var Manager
     */
    private static $instance;

    /**
     * @var null|array
     */
    private $options;

    public static $defaultScheme = 'https';

    // @todo Now we don't check ssl, but we should check it! But how, if we still don't have certificate?
    public static $validateSsl = false;

    /**
     * @return Manager
     */
    public static function instance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $optionName
     * @return bool|string|array
     */
    public function getOption($optionName)
    {
        if ($this->options !== null) {
            return $this->options[$optionName] ?? false;
        }

        $this->loadOptions(true);

        return $this->getOption($optionName);
    }

    /**
     * Load options from DB in the class
     *
     * @param bool $force
     */
    public function loadOptions($force = false): void
    {
        if ($this->options !== null && $force === false) {
            return;
        }

        $this->options = \get_blog_option(1, 'supr_own_domain_options', []);
    }

    /**
     * Get current plan for shop
     *
     * @param int|null $shopId
     * @return \WU_Plan|bool
     */
    public function getPlan($shopId = null)
    {
        if ($shopId === null) {
            $shopId = get_current_blog_id();
        }

        $site = \wu_get_site($shopId);

        $plan = $site->get_plan();

        if ($plan === false && $shopId > 1) {
            error_log('[Supr Own Domain] Plan for shop #' . $shopId . ' doesn\'t exist.');
        }

        return $plan;
    }

    /**
     * Get name of current plan for shop
     *
     * @param int|null $shopId
     * @return string
     */
    public function getPlanName($shopId = null): string
    {
        $plan = $this->getPlan($shopId);

        return $plan ? strtolower($plan->title) : 'starter';
    }

    /**
     * Is user allowed to register domain
     *
     * @return bool
     */
    public function registerDomainAllowed(): bool
    {
        return \in_array(Manager::instance()->getPlanName(), ['smart', 'pro']);
    }

    /**
     * @param array $ips
     * @return array
     */
    public function clearIpAddresses($ips): array
    {
        foreach ($ips as $num => $ip) {
            if (!\filter_var($ip, FILTER_VALIDATE_IP)) {
                unset($ips[$num]);

                continue;
            }

            $ips[$num] = \trim($ip);
        }

        return \array_filter(\array_unique($ips));
    }

    /**
     * Add filter to ms function domain_exists
     * We save old domains (test.musupr.de) in a table after the merchant has own domain (test.com). And the merchant can come back to this domain later.
     * That why we don't allow to reserve this domains.
     */
    public static function addFilterDomainExists(): void
    {
        \add_filter('domain_exists', [self::class, 'checkDomainExists'], 10, 4);
    }

    /**
     * @param null|int $result
     * @param string   $domain
     * @param string   $path
     * @param int      $network_id
     * @return null|int
     */
    public static function checkDomainExists($result, $domain, $path, $network_id): ?int
    {
        $domainManager = DomainManager::instance();
        $existsDomain = $domainManager->getDomainByName($domain);

        if ($existsDomain !== null && $existsDomain->getBlogId() !== \get_current_blog_id()) {
            $result = $existsDomain->getBlogId();
        }

        return $result ?? null;
    }

    /**
     * @param string $oldUrl
     * @param string $newUrl
     * @param string $shopName
     * @param string $shopUrl
     * @return bool
     */
    public function sendNewDomainAddedMail($oldUrl, $newUrl, $shopName, $shopUrl): bool
    {
        if (\class_exists('\SuprCampaignMonitor\Manager') && $this->getOption('admin_email') && $this->getOption('cm_transaction_id')) {
            $data = [
                'shop_url' => $shopUrl,
                'shopname' => $shopName,
                'old_url' => $oldUrl,
                'new_url' => $newUrl
            ];

            \SuprCampaignMonitor\Manager::sendTransactionMail($this->getOption('cm_transaction_id'), $data, $this->getOption('admin_email'));

            return true;
        }

        // @todo Fallback
        return false;
    }

    /**
     * Activate new domain for blog
     *
     * @param int $blogId
     * @param string   $newDomain
     */
    public function activateNewDomain($blogId, $newDomain): void
    {
        \switch_to_blog($blogId);

        // Rewrite rules can't be flushed during switch to blog.
        //\delete_option('rewrite_rules');

        $blog_data = [
            'scheme' => self::$defaultScheme,
            'domain' => $newDomain,
            'path' => '/'
        ];

        \update_blog_details($blogId, $blog_data);

        \update_option('home', $blog_data['scheme'] . '://' . $blog_data['domain'] . $blog_data['path']);
        \update_option('siteurl', $blog_data['scheme'] . '://' . $blog_data['domain'] . $blog_data['path']);

        \restore_current_blog();

        // If we are in network admin, we don't need a redirect
        if (!\is_network_admin()) {
            \wp_redirect(\admin_url('admin.php?page=supr_own_domain_settings&update=updated'));
        }
    }

    /**
     * Check if we have valid ssl connection
     *
     * @param string $domain
     * @return bool
     */
    public function hasSsl($domain): bool
    {
        // All of sub domains have an ssl certificate
        return DomainManager::instance()->isSubdomainOfMainSite($domain) || CertificateManager::instance()->sslExist($domain);
    }

    /**
     * Delete all own Domains from table
     *
     * @param \WP_Site $blog
     * @throws \Exception
     */
    public function deleteAllDomains($blog): void
    {
        $domains = DomainManager::instance()->getDomainsByBlogId($blog->blog_id);

        foreach ($domains as $domain) {
            DomainManager::instance()->delete($domain->getId());
        }
    }
}
