<?php

namespace SuprOwnDomain;

/**
 * Class Plugin
 *
 * @package SuprOwnDomain
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function getInstance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Init
     */
    public function adminInit(): void
    {
        // We need it only on domain pages
        if (isset($_GET['page']) && ($_GET['page'] === 'supr_own_domain_settings' || $_GET['page'] === 'supr_own_domain_network_settings')) {
            ResourceManager::addResources();
            ResourceManager::addJsVariables();
        }
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        DomainManager::instance()->changeTableStructur();

        \add_action('wp_uninitialize_site', [Manager::instance(), 'deleteAllDomains']);

        // Register end points
        Api::registerRoutes();

        // Add menu
        Menu::addMenu();

        Manager::addFilterDomainExists();

        // Load translations
        \add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        \add_action('admin_init', [$this, 'adminInit'], 0);
    }

    /**
     * Create new tables and write
     */
    public static function activationHook(): void
    {
        (new DomainManager())->createTable();
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_OWN_DOMAIN_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::getInstance();
