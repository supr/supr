<?php

namespace SuprOwnDomain;

/**
 * Class Autoloader
 *
 * @package SuprOwnDomain
 */
class Autoloader
{
    /**
     * Run autoloader.
     *
     * Register a function as `__autoload()` implementation.
     *
     * @since  1.6.0
     * @access public
     * @static
     */
    public static function run(): void
    {
        spl_autoload_register([__CLASS__, 'autoload']);
    }

    /**
     * Autoload.
     *
     * For a given class, check if it exist and load it.
     *
     * @since  1.6.0
     * @access private
     * @static
     *
     * @param string $class Class name.
     */
    private static function autoload($class): void
    {
        $realClassName = str_replace(__NAMESPACE__ . '\\', '', $class);

        $filePath = SUPR_OWN_DOMAIN_PATH . 'includes/' . $realClassName . '.php';

        if (file_exists($filePath) && is_readable($filePath)) {
            require_once $filePath;

            return;
        }

        // Try to load from vendor
        $filePath = SUPR_OWN_DOMAIN_PATH . 'vendor/' . $realClassName . '.php';

        if (file_exists($filePath) && is_readable($filePath)) {
            require_once $filePath;

            return;
        }
    }
}
