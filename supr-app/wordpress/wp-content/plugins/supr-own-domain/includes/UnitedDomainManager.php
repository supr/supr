<?php

namespace SuprOwnDomain;

/**
 * Class UnitedDomainManager
 *
 * @package SuprOwnDomain
 */
class UnitedDomainManager
{
    /**
     * @var UnitedDomainManager
     */
    private static $instance;

    /**
     * Rest end point
     *
     * @var string
     */
    private $socket;

    /**
     * @var array
     */
    private $checkZones;

    /**
     * @var array
     */
    private $dns;

    /**
     * @var \mreg
     */
    private $mreg;

    /**
     * UnitedDomainManager constructor.
     *
     * @param \wpdb|null $wpdb
     */
    public function __construct($wpdb = null)
    {
        $user = Manager::instance()->getOption('ud_user') ?: '';
        $password = Manager::instance()->getOption('ud_password') ?: '';
        $socket = Manager::instance()->getOption('ud_socket') ?: '';
        $additionalParams = Manager::instance()->getOption('ud_additional_params') ?: '';
        $checkZones = Manager::instance()->getOption('ud_check_zones') ?: '';
        $dns = Manager::instance()->getOption('ud_dns') ?: '';

        $this->socket = $socket . '?s_login=' . $user . '&s_pw=' . $password . (!empty($additionalParams) ? '&' . $additionalParams : '');
        $this->checkZones = explode(',', $checkZones);
        $this->dns = explode(',', $dns);
    }

    /**
     * @return \mreg
     */
    public function getMReg(): \mreg
    {
        if ($this->mreg === null) {
            $this->mreg = new \mreg('hash');
            $this->mreg->setApiType('hash');
        }

        return $this->mreg;
    }

    /**
     * @param \wpdb|null $wpdb
     * @return \SuprOwnDomain\UnitedDomainManager
     */
    public static function instance($wpdb = null): UnitedDomainManager
    {
        if (self::$instance === null) {
            self::$instance = new self($wpdb);
        }

        return self::$instance;
    }

    /**
     * Check domains by string
     *
     * @param $domain
     * @return array|null
     */
    public function getFreeDomains($domain): ?array
    {
        // Parse domain name
        $domain = preg_replace('/\..*/', '', $domain);
        $domain = preg_replace('/[^a-z0-9\-]/', '', $domain);

        $command = ['command' => 'CheckDomains'];
        $domainsForChecking = [];

        // Get all domains
        foreach ($this->checkZones as $num => $zone) {
            $command['domain' . $num] = $domain . '.' . $zone;
            $domainsForChecking[$num] = [
                'status' => false,
                'domain' => $domain . '.' . $zone
            ];
        }

        $hash = $this->getMReg()->mreg_call($command, ['socket' => $this->socket]);
        $hash['CODE'] = (int)$hash['CODE'];

        // Code 200 wird generell zurückgegeben, wenn die Ausführung des Kommandos erfolgreich war.
        if ($hash['CODE'] === 200) {
            foreach ($hash['PROPERTY']['DOMAINCHECK'] as $index => $status) {
                if (preg_match('/^210 /', $status)) {
                    $domainsForChecking[$index]['status'] = true;
                }
            }

            return $domainsForChecking;
        }

        // Return Code ungleich 210 und 211. Kommando nicht erfolgreich ausgeführt.
        error_log('[Supr Own Domain] Cannot check domains from string \'' . $domain . '\'. Response: ' . print_r($hash, true));

        return null;
    }

    /**
     * Create contact
     *
     * @param array $params
     * @return string|null
     */
    public function createContact(array $params): ?string
    {
        // Find contact: it can be already given
        $paramsContactFind = [
            'command' => 'QueryContactList',
            'email' => $params['email']
        ];

        $hash = $this->getMReg()->mreg_call($paramsContactFind, ['socket' => $this->socket]);

        if ((int)$hash['CODE'] === 200 && isset($hash['PROPERTY']['CONTACT'][0])) {
            // Update existed contact
            $params['command'] = 'ModifyContact';
            $params['contact'] = $hash['PROPERTY']['CONTACT'][0];
        } else {
            // Create new contact
            $params['command'] = 'AddContact';
        }

        $hash = $this->getMReg()->mreg_call($params, ['socket' => $this->socket]);

        if ((int)$hash['CODE'] === 200) {
            return $hash['PROPERTY']['CONTACT'][0];
        }

        // Return Code ungleich 210 und 211. Kommando nicht erfolgreich ausgeführt.
        error_log("[Supr Own Domain] Cannot create contact. Params: \n" . print_r($params, true) . ". Response: \n" . print_r($hash, true));

        return null;
    }

    /**
     * Register new domain and create DNS settings
     *
     * @param $contactId
     * @param $domain
     * @return bool
     */
    public function registerDomain($contactId, $domain): bool
    {
        // 1. Create DNS-Zone for our future domain
        $params = [
            'command' => 'CreateDNSZone',
            'dnszone' => $domain
        ];

        $index = 0;

        // Set dns
        foreach ($this->dns as $num => $val) {
            $params['rr' . $index] = "@ IN NS {$val}.";
            $index++;
        }

        // Set A-Records
        $aIpAddresses = Manager::instance()->getOption('a_ip_addresses') ?: [];

        foreach ($aIpAddresses as $num => $val) {
            // rr3 = @ IN A 1.2.3.4
            // rr4 = * IN A 1.2.3.5
            $params['rr' . $index] = ($num === 0 ? '@' : '*') . " IN A {$val}";
            $index++;
        }

        $hash = $this->getMReg()->mreg_call($params, ['socket' => $this->socket]);

        if ((int)$hash['CODE'] !== 200) {
            error_log("[Supr Own Domain] Cannot create DNS and A-Records for domain {$domain}. Params: \n" . print_r($params, true) . ". Response: \n" . print_r($hash, true));

            return false;
        }

        // 2. Register domain
        $params = [
            'command' => 'AddDomain',
            'domain' => $domain,
            'ownercontact0' => $contactId,
            'admincontact0' => $contactId,
            'techcontact0' => $contactId,
            'billingcontact0' => $contactId,
        ];

        foreach ($this->dns as $num => $val) {
            $params['nameserver' . $num] = $val;
        }

        $hash = $this->getMReg()->mreg_call($params, ['socket' => $this->socket]);

        if ((int)$hash['CODE'] !== 200) {
            error_log("[Supr Own Domain] Cannot register domain {$domain}. Params: \n" . print_r($params, true) . ". Response: \n" . print_r($hash, true));

            // Rollback of the DNS zone
            $this->deleteDNSZone($domain);

            return false;
        }

        return true;
    }

    /**
     * Delete DNS zone by name
     *
     * @param string $dnsZone
     * @return bool
     */
    public function deleteDNSZone(string $dnsZone): bool
    {
        $params = [
            'command' => 'DeleteDNSZone',
            'dnszone' => $dnsZone
        ];

        $hash = $this->getMReg()->mreg_call($params, ['socket' => $this->socket]);

        if ((int)$hash['CODE'] !== 200) {
            error_log("[Supr Own Domain] Cannot delete dns zone {$domain}. Params: \n" . print_r($params, true) . ". Response: \n" . print_r($hash, true));

            return false;
        }

        return true;
    }
}
