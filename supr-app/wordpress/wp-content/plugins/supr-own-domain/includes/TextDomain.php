<?php

namespace SuprOwnDomain;

class TextDomain
{
    public static $domainName = 'supr-own-domain';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_OWN_DOMAIN_DIR_NAME . '/languages/');
    }
}
