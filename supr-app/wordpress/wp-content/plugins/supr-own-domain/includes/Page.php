<?php

namespace SuprOwnDomain;

/**
 * Class Page
 *
 * @package SuprOwnDomain
 */
class Page
{
    /**
     * Show setting page
     */
    public static function settingsPage(): void
    {
        if (get_current_blog_id() !== 1 && !\in_array(Manager::instance()->getPlanName(), ['smart', 'smart trial', 'pro'])) {
            include SUPR_OWN_DOMAIN_PATH . 'templates/settings-page-upsale.php';
        } else {
            include SUPR_OWN_DOMAIN_PATH . 'templates/settings-page.php';
        }
    }

    /**
     * Show setting page
     */
    public static function networkSettingsPage(): void
    {
        include SUPR_OWN_DOMAIN_PATH . 'templates/network-settings-page.php';
    }
}
