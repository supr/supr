<?php

namespace SuprOwnDomain;

/**
 * Class CertificateManager
 *
 * @package SuprOwnDomain
 */
class CertificateManager
{
    /**
     * @var null|CertificateManager
     */
    private static $instance;

    /**
     * @var string
     */
    private $apiHost = 'http://s2-cert-manager:5000';

    /**
     * @var mixed
     */
    private $lastResponse;

    /**
     * @return \SuprOwnDomain\CertificateManager
     */
    public static function instance(): CertificateManager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Delete certificate for domain
     *
     * @param $domain
     * @return bool
     */
    public function deleteSSL($domain): bool
    {
        if ($this->execute('/nginx/cert_conf_exists?domain=' . $domain)->status !== 'success') {
            return true;
        }

        if ($this->execute('/certificate/revoke_cert?domain=' . $domain)->status !== 'success') {
            error_log('[Supr Own Domain] Cannot revoke certificate of domain ' . $domain . '. Response: ' . print_r($this->lastResponse, true));

            return false;
        }

        if ($this->execute('/certificate/remove_cert?domain=' . $domain)->status !== 'success') {
            error_log('[Supr Own Domain] Cannot remove certificate of domain ' . $domain . '. Response: ' . print_r($this->lastResponse, true));

            return false;
        }

        if ($this->execute('/nginx/reload')->status !== 'success') {
            error_log('[Supr Own Domain]: Cannot reload nginx arter removimg of certificate of domain ' . $domain . '. Response: ' . print_r($this->lastResponse, true));

            return false;
        }

        return true;
    }

    /**
     * Check if SSL certificate exists
     *
     * @param $domain
     * @return bool
     */
    public function sslExist($domain): bool
    {
        return $this->execute('/nginx/cert_conf_exists?domain=' . $domain)->status === 'success';
    }

    /**
     * Generate / recreate certificate for domain
     *
     * @param $domain
     * @return bool
     */
    public function generateSSL($domain): bool
    {
        // 1. Check if certificate already exist
        if ($this->execute('/nginx/cert_conf_exists?domain=' . $domain)->status === 'success') {
            // Check age of domain (60 days)
            if ((int)$this->execute('/certificate/cert_age?domain=' . $domain)->message->days > 60) {
                // Renew certificate
                if ($this->execute("/certificate/renew_cert?domain={$domain}&acme_sh_options=--force")->status === 'success') {
                    // Test configuration
                    if ($this->execute('/nginx/test_config')->status === 'success') {
                        // Reload Nginx
                        if ($this->execute('/nginx/reload')->status === 'success') {
                            // @todo delete
                            error_log('[Supr Own Domain] DEBUG: Certificate for domain ' . $domain . ' was successfully updated.');

                            return true;
                        }

                        error_log('[Supr Own Domain] Nginx configtest passed but failed to reload nginx container. Response: ' . print_r($this->lastResponse, true));

                        return false;
                    }

                    error_log('[Supr Own Domain] Nginx configtest failed for ' . $domain . '. Response: ' . print_r($this->lastResponse, true));

                    return false;
                }

                error_log('[Supr Own Domain] Cert age is over 60 days but renewal failed for ' . $domain . '. Response: ' . print_r($this->lastResponse, true));

                return false;
            }

            // @todo delete
            error_log('[Supr Own Domain] DEBUG: Certificate for domain ' . $domain . ' already exists and is valid.');

            return true;
        }

        // 2. Generate new certificate
        if ($this->execute("/certificate/create_cert?domain={$domain}&acme_sh_options=--force")->status === 'success') {
            // Test configuration
            if ($this->execute('/nginx/test_conf_move_to_live')->status === 'success') {
                // Reload Nginx
                if ($this->execute('/nginx/reload')->status === 'success') {
                    // @todo delete
                    error_log('[Supr Own Domain] DEBUG: Certificate for domain ' . $domain . ' was successfully generated.');

                    return true;
                }

                error_log('[Supr Own Domain] Nginx configtest passed but failed to reload nginx container. Response: ' . print_r($this->lastResponse, true));

                return false;
            }

            error_log('[Supr Own Domain] Nginx configtest failed for ' . $domain . '. Response: ' . print_r($this->lastResponse, true));

            return false;
        }

        error_log('[Supr Own Domain] Failed generating certs for ' . $domain . '. Response: ' . print_r($this->lastResponse, true));

        return false;
    }

    /**
     * Execute api command
     *
     * @param $command
     * @return object
     */
    private function execute($command)
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => '',     // handle compressed
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
        );

        $ch = curl_init($this->apiHost . $command);
        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);

        // Rewrite curl error in out format
        if ($content === false) {
            $this->lastResponse = new \StdClass();
            $this->lastResponse->status = 'curl-error';
            $this->lastResponse->message = \curl_error($ch);
        } else {
            $this->lastResponse = \json_decode($content);
        }

        curl_close($ch);

        return $this->lastResponse;
    }
}
