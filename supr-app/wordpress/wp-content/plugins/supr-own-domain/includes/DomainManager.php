<?php

namespace SuprOwnDomain;

/**
 * Class DomainManager
 *
 * @package SuprOwnDomain
 */
class DomainManager
{
    /**
     * DB adapter
     *
     * @var \wpdb
     */
    private $db;

    /**
     * Table name
     *
     * @var string
     */
    private $tableName;

    /**
     * @var string
     */
    private $activationTimeout = 'PT1H';

    /**
     * @var string
     */
    private $dnsSettingsTimeout = 'PT2H';

    /**
     * @var DomainManager
     */
    private static $instance;

    /**
     * DomainManager constructor.
     */
    public function __construct()
    {
        global $wpdb;

        $this->db = $wpdb;
        $this->tableName = $this->db->base_prefix . 'supr_own_domain';
    }

    /**
     * @return \SuprOwnDomain\DomainManager
     */
    public static function instance(): DomainManager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * Create table for color schemes if not exist
     */
    public function createTable(): void
    {
        $blogsTableName = $this->db->base_prefix . 'blogs';

        $sql = "CREATE TABLE IF NOT EXISTS {$this->tableName} (
        `id` INT NOT NULL AUTO_INCREMENT,
        `blog_id` BIGINT(20),
        `domain` VARCHAR(255),
        `primary` SMALLINT(1) NOT NULL DEFAULT 0,
        `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `ssl_generated` TIMESTAMP NULL DEFAULT NULL,
        `united_domains` SMALLINT(1) NOT NULL DEFAULT 0,
        `dns_check_last` TIMESTAMP,
        `dns_check_fail` SMALLINT DEFAULT 0,
        PRIMARY KEY(`id`),
        FOREIGN KEY (`blog_id`)
            REFERENCES {$blogsTableName} (`blog_id`)
            ON DELETE CASCADE,
        UNIQUE KEY(`domain`) 
        );";

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        \dbDelta($sql);

        \add_blog_option(1, "{$this->tableName}_table_version", SUPR_OWN_DOMAIN_VERSION);
    }

    /**
     * Add columns to table
     */
    public function changeTableStructur(): void
    {
        switch (\get_blog_option(1, "{$this->tableName}_table_version")) {
            case '1.0.0':
                $sql = "ALTER TABLE {$this->tableName} ADD COLUMN `ssl_generated` TIMESTAMP NULL DEFAULT NULL;";
                $this->db->query($sql);
                $sql = "ALTER TABLE {$this->tableName} CHANGE COLUMN `timestamp` `changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;";
                $this->db->query($sql);
                $sql = "ALTER TABLE {$this->tableName} ADD COLUMN `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;";
                $this->db->query($sql);
                $sql = "ALTER TABLE {$this->tableName} ADD COLUMN `united_domains` SMALLINT(1) NOT NULL DEFAULT 0;";
                $this->db->query($sql);
                $sql = "ALTER TABLE {$this->tableName} ADD COLUMN `dns_check_last` TIMESTAMP;";
                $this->db->query($sql);
                $sql = "ALTER TABLE {$this->tableName} ADD COLUMN `dns_check_fail` SMALLINT DEFAULT 0;";
                $this->db->query($sql);
                break;
            case '2.0.0':
            case '2.1.0':
                $sql = "ALTER TABLE {$this->tableName} ADD COLUMN `united_domains` SMALLINT(1) NOT NULL DEFAULT 0;";
                $this->db->query($sql);
                $sql = "ALTER TABLE {$this->tableName} ADD COLUMN `dns_check_last` TIMESTAMP;";
                $this->db->query($sql);
                $sql = "ALTER TABLE {$this->tableName} ADD COLUMN `dns_check_fail` SMALLINT DEFAULT 0;";
                $this->db->query($sql);
                break;
            case '3.0.0':
                $sql = "ALTER TABLE {$this->tableName} ADD COLUMN `dns_check_last` TIMESTAMP;";
                $this->db->query($sql);
                $sql = "ALTER TABLE {$this->tableName} ADD COLUMN `dns_check_fail` SMALLINT DEFAULT 0;";
                $this->db->query($sql);
                break;
        }

        \update_blog_option(1, "{$this->tableName}_table_version", SUPR_OWN_DOMAIN_VERSION);
    }

    /**
     * @param int|null $blogId
     * @return Domain[]
     * @throws \Exception
     */
    public function getDomainsByBlogId($blogId = null): array
    {
        $blogId = ($blogId !== null) ? (int)$blogId : get_current_blog_id();

        $returned = [];

        $sql = "SELECT * FROM {$this->tableName} WHERE `blog_id` = %d";

        $sql .= ' ORDER BY `domain`';

        $sql = $this->db->prepare($sql, [$blogId]);

        $domains = $this->db->get_results($sql, ARRAY_A);

        if ($domains) {
            foreach ($domains as $domain) {
                $returned[] = new Domain($domain);
            }
        }

        if (\count($returned) === 0) {
            $this->addCurrentDomainToDomains();

            return $this->getDomainsByBlogId($blogId);
        }

        return $returned;
    }

    /**
     * Get count of united domains for blog
     *
     * @param int $blogId
     * @return int
     * @throws \Exception
     */
    public function getUnitedDomainsCount($blogId = null): int
    {
        $blogId = ($blogId !== null) ? (int)$blogId : get_current_blog_id();
        $count = 0;
        $domains = $this->getDomainsByBlogId($blogId);

        foreach ($domains as $domain) {
            if ($domain->getUnitedDomains()) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * Get all domains
     *
     * @return Domain[]
     */
    public function getDomains(): array
    {
        $returned = [];

        $sql = "SELECT * FROM {$this->tableName} ORDER BY `domain` ASC";
        $domains = $this->db->get_results($sql, ARRAY_A);

        if ($domains) {
            foreach ($domains as $domain) {
                $returned[] = new Domain($domain);
            }
        }

        return $returned;
    }

    /**
     * Add current domain to domains table.
     *
     * @throws \Exception
     */
    private function addCurrentDomainToDomains(): void
    {
        $url = \get_bloginfo('url');
        $url = $this->getHost($url);

        $blogId = \get_current_blog_id();

        if ($this->getDomainByName($url) !== null) {
            throw new \Exception('[SUPR Own Domain]: I cannot add domain ' . $url . ' to blog #' . $blogId);
        }

        $domain = new Domain();
        $domain->setBlogId($blogId)
            ->setDomain($url)
            ->setPrimary(true);

        $this->save($domain);
    }

    /**
     * @param int|null $blogId
     * @return null|Domain
     */
    public function getPrimaryDomain($blogId = null): ?Domain
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE `blog_id` = %d AND `primary` = 1";
        $sql = $this->db->prepare($sql, [$blogId]);
        $domains = $this->db->get_results($sql, ARRAY_A);

        return isset($domains[0]) ? new Domain($domains[0]) : null;
    }

    /**
     * @param int $id
     * @return null|Domain
     */
    public function getDomainById($id): ?Domain
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE `id` = %d";
        $sql = $this->db->prepare($sql, [$id]);
        $domains = $this->db->get_results($sql, ARRAY_A);

        return isset($domains[0]) ? new Domain($domains[0]) : null;
    }

    /**
     * @param string $domain
     * @return null|Domain
     */
    public function getDomainByName($domain): ?Domain
    {
        $domain = \strtolower(\trim($domain));

        $sql = "SELECT * FROM {$this->tableName} WHERE `domain` like '%s'";
        $sql = $this->db->prepare($sql, [$domain]);
        $domains = $this->db->get_results($sql, ARRAY_A);

        return isset($domains[0]) ? new Domain($domains[0]) : null;
    }

    /**
     * Save domain in DB.
     *
     * @param Domain $domain
     */
    public function save(&$domain): void
    {
        if ($domain->getId() === null) {
            $domain->setCreated((new \DateTime())->format(DATE_ATOM));
        }

        $domain->setChanged((new \DateTime())->format(DATE_ATOM));

        $data = $domain->serialize();

        // Small bug: if we have null, it means current_time() :(
        $data = \array_filter($data);

        // Update or insert
        if ($domain->getId() > 0) {
            unset($data['id']);
            $this->db->update($this->tableName, $data, ['id' => $domain->getId()]);
        } else {
            $this->db->insert($this->tableName, $data);
            // Update id
            $domain->setId($this->db->insert_id);
        }
    }

    /**
     * @param int $id
     */
    public function delete($id): void
    {
        $this->db->delete($this->tableName, ['id' => $id]);
    }

    /**
     * Handle request for POST method
     */
    public function processPostData(): void
    {
        // If we have new Domain
        if (isset($_POST['domain']) && $this->isDomainValid($_POST['domain'])) {
            $host = $this->getHost($_POST['domain']);

            // Check if not exist
            if ($this->getDomainByName($host) !== null) {
                $this->showError(__('This domain already exists.', 'supr-own-domain'));

                return;
            }

            // Add to the domains
            $domain = new Domain();
            $domain->setBlogId(\get_current_blog_id())
                ->setDomain($host)
                ->setPrimary(false);

            $this->save($domain);

            $this->showSuccess(__('Domain was added to your shop.', 'supr-own-domain'));

            if ($this->updateSslCertificate($domain)) {
                $this->showSuccess(__('The ssl certificate was generated.', 'supr-own-domain'));
            } else {
                $this->showError(__('Generation of SSL certificate failed.', 'supr-own-domain'));
            }

            Manager::instance()->sendNewDomainAddedMail(\get_site_url(), 'https://' . $domain->getDomain(), \get_bloginfo('name'), \get_site_url());
        }

        // If we must activate new Domain
        if (isset($_POST['activateDomain'])) {
            if (\get_current_blog_id() === 1) {
                $this->showError(__('You cannot change domain of main blog. Ssory, but it is stupid!', 'supr-own-domain'));

                return;
            }

            $this->activateDomain($_POST['activateDomain']);
        }
    }

    /**
     * Handle request for GET method
     */
    public function processGetData(): void
    {
        // If we have new Domain
        if (isset($_GET['deleteDomain'])) {
            $this->deleteDomain($_GET['deleteDomain']);
        }

        // Show success message after redirect
        if (isset($_GET['update']) && $_GET['update'] === 'updated') {
            $this->showSuccess(__('Domain was saved as primary.', 'supr-own-domain'));
        }
    }

    /**
     * @param string $message
     */
    private function showError($message): void
    {
        echo "<div class='notice notice-error'><p>{$message}</p></div>";
    }

    /**
     * @param string $message
     */
    private function showSuccess($message): void
    {
        echo "<div class='notice notice-success'><p>{$message}</p></div>";
    }

    /**
     * @param int $id
     * @return bool
     */
    private function deleteDomain($id): bool
    {
        $domain = $this->getDomainById((int)$id);

        if ($domain === null) {
            $this->showError(__('We cannot find this domain.', 'supr-own-domain'));

            return false;
        }

        if ($domain->getBlogId() !== get_current_blog_id()) {
            $this->showError(__('It is not your domain.', 'supr-own-domain'));

            return false;
        }

        if ($domain->getPrimary()) {
            $this->showError(__('This domain is a primary domain and you cannot delete it.', 'supr-own-domain'));

            return false;
        }

        if (!\is_super_admin() && $this->isSubdomainOfMainSite($domain->getDomain())) {
            $this->showError(__('You cannot delete a subdomain of the main site.', 'supr-own-domain'));

            return false;
        }

        // Delete certificate
        if (!$this->isSubdomainOfMainSite($domain->getDomain()) && !CertificateManager::instance()->deleteSSL($domain->getDomain())) {
            $this->showError(__('Deleting of SSL certificate failed.', 'supr-own-domain'));
        }

        $this->delete($domain->getId());

        $this->showSuccess(__('Domain was deleted.', 'supr-own-domain'));

        return true;
    }

    /**
     * Update or create certificate for domain and for www sub domain
     *
     * @param Domain $domain
     * @return bool
     * @throws \Exception
     */
    private function updateSslCertificate(&$domain): bool
    {
        // We have global certificate for all sub domains
        if ($this->isSubdomainOfMainSite($domain->getDomain())) {
            return true;
        }

        if (CertificateManager::instance()->generateSSL($domain->getDomain())) {
            // Update SSL generation time
            $domain->setSslGenerated((new \DateTime())->format(DATE_ATOM));
            $this->save($domain);

            // Get second domain with or without www
            $secondDomainName = \strpos($domain->getDomain(), 'www') !== 0 ? 'www.' . $domain->getDomain() : \substr($domain->getDomain(), 4);

            // Try to generate ssl for www (without www) too
            if ($this->hasValidARecord($secondDomainName)) {
                if (CertificateManager::instance()->generateSSL($secondDomainName)) {
                    // Load domain from DB or create new
                    $secondDomain = $this->getDomainByName($secondDomainName) ?? new Domain();
                    $secondDomain->setBlogId(\get_current_blog_id())
                        ->setDomain($secondDomainName)
                        ->setSslGenerated((new \DateTime())->format(DATE_ATOM))
                        ->setPrimary(false);

                    $this->save($secondDomain);
                }
            } else {
                error_log('[Supr Own Domain] DEBUG: Certificate for domain ' . $secondDomainName . ' cannot be generated because A-Record is invalid.');
            }

            return true;
        }

        return false;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    private function activateDomain($id): bool
    {
        $domain = $this->getDomainById((int)$id);

        if ($domain === null) {
            $this->showError(__('We cannot find this domain.', 'supr-own-domain'));

            return false;
        }

        if ($domain->getBlogId() !== \get_current_blog_id()) {
            $this->showError(__('It is not your domain.', 'supr-own-domain'));

            return false;
        }

        if ($domain->getPrimary()) {
            $this->showError(__('This domain is always primary domain.', 'supr-own-domain'));

            return false;
        }

        // We need to wait of DNS
        if ($domain->getUnitedDomains()) {
            /** @var \DateTime $domainCreatedDateTime */
            $domainCreatedDateTime = $domain->getCreated() instanceof \DateTime ? $domain->getCreated() : new \DateTime($domain->getCreated());
            $allowActivationAfter = $domainCreatedDateTime->add(new \DateInterval($this->dnsSettingsTimeout));

            if ($allowActivationAfter > new \DateTime()) {
                $this->showError(sprintf(__('You can activate this domain only after %s. This time is necessary for the new DNS settings to take effect.', 'supr-own-domain'), $this->getMinutes($allowActivationAfter, new \DateTime()) . ' Min'));

                return false;
            }
        }

        // If we have already domain config
        $domainConfigured = $domain->getSslGenerated() !== null;

        if (!$this->isDomainValid($domain->getDomain(), true, $domainConfigured)) {
            $this->showError(__('Domain validation failed.', 'supr-own-domain'));

            return false;
        }

        // We need timeout after ssl-generation (because all new configs should be loaded on all containers)
        if ($domain->getSslGenerated() !== null) {
            /** @var \DateTime $sslGeneratedDateTime */
            $sslGeneratedDateTime = $domain->getSslGenerated() instanceof \DateTime ? $domain->getSslGenerated() : new \DateTime($domain->getSslGenerated());
            $allowActivationAfter = $sslGeneratedDateTime->add(new \DateInterval($this->activationTimeout));

            if ($allowActivationAfter > new \DateTime()) {
                // Show message
                $this->showError(sprintf(__('You can activate this domain after %s.', 'supr-own-domain'), $this->getMinutes($allowActivationAfter, new \DateTime()) . ' Min'));

                return false;
            }
        }

        // Generate certificate for other domains
        if (!$this->updateSslCertificate($domain)) {
            // Show message
            $this->showError(__('Generation of SSL certificate failed.', 'supr-own-domain'));

            return false;
        }

        // Deactivate all domains and save our domain
        if ($this->deactivateAllDomains()) {
            $domain->setPrimary(true);
            $this->save($domain);

            Manager::instance()->activateNewDomain($domain->getBlogId(), $domain->getDomain());

            $this->showSuccess(__('Domain was saved as primary.', 'supr-own-domain'));

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function deactivateAllDomains(): bool
    {
        return (bool)$this->db->update($this->tableName, ['primary' => 0], ['blog_id' => \get_current_blog_id()]);
    }

    /**
     * Check if domain is right configured
     *
     * @param string $url
     * @param bool   $allowSubDomains Allow subdomains of main blog.
     * @param bool   $configured      If domain already exists in system and has configuration.
     * @return bool
     */
    private function isDomainValid($url, $allowSubDomains = false, $configured = false): bool
    {
        // Parse URL
        $urlParts = \parse_url(\filter_var($url, FILTER_SANITIZE_URL));

        // Check host exist else path assign to host
        if (!isset($urlParts['host'])) {
            $urlParts['host'] = $urlParts['path'];
        }

        if (!empty($urlParts['host'])) {
            // Add scheme if not found
            if (!isset($urlParts['scheme'])) {
                $urlParts['scheme'] = Manager::$defaultScheme;
            }

            // Remove www
            $urlParts['host'] = preg_replace('/^www\./', '', $urlParts['host']);

            // Do we have a records
            if (!\checkdnsrr($urlParts['host'], 'A')) {
                $this->showError(__('Domain has incorrect A record.', 'supr-own-domain'));

                return false;
            }

            // Do we have a valid scheme
            if (!\in_array($urlParts['scheme'], ['http', 'https'])) {
                $this->showError(__('Scheme is not valid.', 'supr-own-domain'));

                return false;
            }

            // Is it ip address
            if (\ip2long($urlParts['host']) !== false) {
                $this->showError(__('Domain should not be IP address.', 'supr-own-domain'));

                return false;
            }

            // Is it a subdomain of us
            if (!$allowSubDomains && !\is_super_admin() && $this->isSubdomainOfMainSite($urlParts['host'])) {
                $this->showError(sprintf(__('Subdomains of <b>%s</b> are not allowed.', 'supr-own-domain'), DOMAIN_CURRENT_SITE));

                return false;
            }

            $url = $urlParts['scheme'] . '://' . $urlParts['host'] . '/';

            // Do we have valid url
            if (\filter_var($url, FILTER_VALIDATE_URL) === false) {
                $this->showError(__('Domain is not valid.', 'supr-own-domain'));

                return false;
            }

            // Validate ssl certificate with headers
            if (Manager::$validateSsl === false) {
                \stream_context_set_default(
                    [
                        'ssl' => [
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                        ]
                    ]
                );
            }

            // We cannot wait more that 5 sec
            \stream_context_set_default(
                [
                    'http' => [
                        'timeout' => 5
                    ]
                ]
            );

            // Can we get headers
            if ($configured && !@\get_headers($url)) {
                $this->showError(__('We cannot get headers.', 'supr-own-domain'));

                return false;
            }

            // Do we have our a record for this domain
            if (!$this->hasValidARecord($urlParts['host'], true)) {
                return false;
            }

            // Do somebody else have this domain (also as secondary)
            $domainEntity = $this->getDomainByName($urlParts['host']);
            if ($domainEntity !== null && $domainEntity->getBlogId() !== \get_current_blog_id()) {
                $this->showError(__('Somebody else already uses this domain.', 'supr-own-domain'));

                return false;
            }

            // Is this domain already active used
            $blogIfFromUrl = \get_blog_id_from_url($urlParts['host']);
            if ($blogIfFromUrl > 0 && $blogIfFromUrl !== \get_current_blog_id()) {
                $this->showError(__('This domain is already active used.', 'supr-own-domain'));

                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * Check if domain has a valid A record
     *
     * @param      $domain
     * @param bool $printError
     * @return bool
     */
    private function hasValidARecord($domain, $printError = false): bool
    {
        $aIpAddresses = Manager::instance()->getOption('a_ip_addresses') ?: [];

        // Set timeout and retry: 1 time, 5 sec
        \putenv('RES_OPTIONS=retrans:1 retry:1 timeout:5 attempts:1');
        $hostIp = \gethostbyname($domain);

        // Do we have our a record for this domain
        if (!\in_array($hostIp, $aIpAddresses, false)) {
            if ($printError) {
                $this->showError(
                    sprintf(
                        __('A record of this domain is not valid. It should be: <b>%s</b>. But <b>%s</b> given.', 'supr-own-domain'),
                        implode(__(' or ', 'supr-own-domain'), $aIpAddresses),
                        $hostIp
                    )
                );
            }

            return false;
        }

        return true;
    }

    /**
     * @param $url
     * @return string
     */
    private function getHost($url): string
    {
        // Parse URL
        $urlParts = parse_url(filter_var($url, FILTER_SANITIZE_URL));

        // Check host exist else path assign to host
        if (!isset($urlParts['host'])) {
            $urlParts['host'] = $urlParts['path'];
        }

        $urlParts['host'] = preg_replace('/^www\./', '', $urlParts['host']);

        return \strtolower(\trim($urlParts['host']));
    }

    /**
     * @param string $domain
     * @return bool
     */
    public function isSubdomainOfMainSite($domain): bool
    {
        return \strpos($domain, '.' . DOMAIN_CURRENT_SITE) !== false;
    }

    /**
     * @param \DateTime $firstDateTime
     * @param \DateTime $secondDateTime
     * @return int
     */
    private function getMinutes(\DateTime $firstDateTime, \DateTime $secondDateTime): int
    {
        return (int)abs($firstDateTime->getTimestamp() - $secondDateTime->getTimestamp()) / 60;
    }
}
