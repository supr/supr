<?php

namespace SuprOwnDomain;

/**
 * Class ResourceManager
 *
 * @package SuprOwnDomain
 */
class ResourceManager
{
    /**
     * Register files for plugin
     */
    public static function addResources(): void
    {
        // CSS
        \wp_register_style('supr_own_domain_style', SUPR_OWN_DOMAIN_URL . 'css/supr-own-domain.css', false, '1.0.0', 'all');

        // JS
        \wp_register_script('supr_own_domain_js', SUPR_OWN_DOMAIN_URL . 'js/supr-own-domain.js', ['jquery'], '1.0.0', true);

        // Add css and js into footer
        \add_action('admin_enqueue_scripts', [__CLASS__, 'enqueueFiles'], 0);
    }

    /**
     * Add files
     */
    public static function enqueueFiles(): void
    {
        wp_enqueue_style('supr_own_domain_style');
        wp_enqueue_script('supr_own_domain_js');
    }

    /**
     * Add js variables to js script
     */
    public static function addJsVariables(): void
    {
        $array = [
            'nonce' => wp_create_nonce('wp_rest'),
            'upsale' => Manager::instance()->registerDomainAllowed() ? 0 : 1,
            'modal_title' => __('Connect your domain', 'supr-own-domain'),
            'modal_text' => __('Please enter the name of the domain you want to connect to your SUPR Shop.', 'supr-own-domain'),
            'modal_add' => __('Connect domain', 'supr-own-domain'),
            'modal_cancel' => __('Cancel', 'supr-own-domain'),

            'ud_domain_list_title' => __('Domain search results', 'supr-own-domain'),
            'ud_domain_list_booked' => __('Domain taken', 'supr-own-domain'),
            'ud_domain_list_free' => __('Domain available', 'supr-own-domain'),

            'ud_modal_title' => __('Register domain?', 'supr-own-domain'),
            'ud_modal_text' => __('Do you really want to register the domain <strong>[domain]</strong> for your shop? You can\'t change the domain name after!', 'supr-own-domain'),
            'ud_modal_legal_text' => str_replace(['<p>', '</p>'], '', stripslashes(Manager::instance()->getOption('ud_legal_text') ?: '')),
            'ud_modal_ok' => __('Book domain', 'supr-own-domain'),
            'ud_modal_cancel' => __('Cancel', 'supr-own-domain'),

            'modal_loading_text' => __('Please wait a moment we check your request', 'supr-own-domain'),

            'ud_modal_error_already_registered_title' => __('Domain is already taken', 'supr-own-domain'),
            'ud_modal_error_already_registered_text' => __('The selected domain is already taken please select another domain from the list', 'supr-own-domain')
        ];

        wp_localize_script('supr_own_domain_js', 'supr_own_domain_values', $array);
    }
}
