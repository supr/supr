<?php

namespace SuprOwnDomain;

/**
 * Class Domain
 *
 * @package SuprOwnDomain
 */
class Domain
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $blogId;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var bool
     */
    private $primary = false;

    /**
     * @var \DateTime|string
     */
    private $changed;

    /**
     * @var \DateTime|string
     */
    private $created;

    /**
     * @var \DateTime|string
     */
    private $sslGenerated;

    /**
     * @var bool
     */
    private $unitedDomains = false;

    /**
     * @var \DateTime|string
     */
    private $dnsCheckLast;

    /**
     * @var int
     */
    private $dnsCheckFail;

    /**
     * Domain constructor.
     *
     * @param null|array $properties
     */
    public function __construct($properties = null)
    {
        if ($properties !== null) {
            $this->unserialize($properties);
        }
    }

    /**
     * @param $properties
     */
    public function unserialize($properties): void
    {
        if (isset($properties['id'])) {
            $this->setId($properties['id']);
        }
        if (isset($properties['blog_id'])) {
            $this->setBlogId($properties['blog_id']);
        }
        if (isset($properties['domain'])) {
            $this->setDomain($properties['domain']);
        }

        $this->setPrimary($properties['primary'] ?? false);

        if (isset($properties['changed'])) {
            $this->setChanged($properties['changed']);
        }

        if (isset($properties['created'])) {
            $this->setCreated($properties['created']);
        }

        if (isset($properties['ssl_generated'])) {
            $this->setSslgenerated($properties['ssl_generated']);
        }

        $this->setUnitedDomains($properties['united_domains'] ?? false);

        if (isset($properties['dns_check_last'])) {
            $this->setDnsCheckLast($properties['dns_check_last']);
        }

        if (isset($properties['dns_check_fail'])) {
            $this->setDnsCheckFail($properties['dns_check_fail']);
        }
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'id' => $this->getId(),
            'blog_id' => $this->getBlogId(),
            'domain' => $this->getDomain(),
            'primary' => $this->getPrimary(),
            'changed' => $this->getChanged(),
            'created' => $this->getCreated(),
            'ssl_generated' => $this->getSslGenerated(),
            'united_domains' => $this->getUnitedDomains(),
            'dns_check_last' => $this->getDnsCheckLast(),
            'dns_check_fail' => $this->getDnsCheckFail()
        ];
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return \SuprOwnDomain\Domain
     */
    public function setId($id): self
    {
        $this->id = (int)$id;

        return $this;
    }

    /**
     * @return int
     */
    public function getBlogId(): int
    {
        return $this->blogId;
    }

    /**
     * @param $blogId
     * @return \SuprOwnDomain\Domain
     */
    public function setBlogId($blogId): self
    {
        $this->blogId = (int)$blogId;

        return $this;
    }

    /**
     * @param string $domain
     * @return \SuprOwnDomain\Domain
     */
    public function setDomain($domain): self
    {
        $this->domain = \mb_strtolower($domain);

        return $this;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @param $primary
     * @return \SuprOwnDomain\Domain
     */
    public function setPrimary($primary): self
    {
        $this->primary = (bool)$primary;

        return $this;
    }

    /**
     * @return bool
     */
    public function getPrimary(): bool
    {
        return $this->primary;
    }

    /**
     * @param $unitedDomains
     * @return \SuprOwnDomain\Domain
     */
    public function setUnitedDomains($unitedDomains): self
    {
        $this->unitedDomains = (bool)$unitedDomains;

        return $this;
    }

    /**
     * @return bool
     */
    public function getUnitedDomains(): bool
    {
        return $this->unitedDomains;
    }

    /**
     * @return mixed
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * @param $changed
     * @return \SuprOwnDomain\Domain
     */
    public function setChanged($changed): self
    {
        $this->changed = $changed;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param $created
     * @return \SuprOwnDomain\Domain
     */
    public function setCreated($created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSslGenerated()
    {
        return $this->sslGenerated;
    }

    /**
     * @param $sslGenerated
     * @return \SuprOwnDomain\Domain
     */
    public function setSslGenerated($sslGenerated): self
    {
        $this->sslGenerated = $sslGenerated;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDnsCheckLast()
    {
        return $this->dnsCheckLast;
    }

    /**
     * @param $dnsCheckLast
     * @return \SuprOwnDomain\Domain
     */
    public function setDnsCheckLast($dnsCheckLast): self
    {
        $this->dnsCheckLast = $dnsCheckLast;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDnsCheckFail()
    {
        return $this->dnsCheckFail;
    }

    /**
     * @param $dnsCheckFail
     * @return \SuprOwnDomain\Domain
     */
    public function setDnsCheckFail($dnsCheckFail): self
    {
        $this->dnsCheckFail = $dnsCheckFail;

        return $this;
    }
}
