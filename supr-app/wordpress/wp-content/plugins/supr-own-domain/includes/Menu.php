<?php

namespace SuprOwnDomain;

/**
 * Class Menu
 *
 * @package SuprOwnDomain
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add network menu page
        add_action('network_admin_menu', [__CLASS__, 'createNetworkMenu']);

        // Add settings link on plugin page
        add_filter('network_admin_plugin_action_links_' . SUPR_OWN_DOMAIN_FILE, [__CLASS__, 'addNetworkSettingsPage']);

        // Add menu page
        add_action('admin_menu', [__CLASS__, 'createMenu']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createMenu(): void
    {
        add_menu_page(
            __('Own domain', 'supr-own-domain'),
            __('Own domain', 'supr-own-domain'),
            'manage_options',
            'supr_own_domain_settings',
            [Page::class, 'settingsPage'],
            'dashicons-admin-site'
        );
    }

    /**
     * Add menu to network admin panel
     */
    public static function createNetworkMenu(): void
    {
        add_menu_page(
            __('Own domain', 'supr-own-domain'),
            __('Own domain', 'supr-own-domain'),
            'manage_network',
            'supr_own_domain_network_settings',
            [Page::class, 'networkSettingsPage'],
            'dashicons-admin-site',
            300
        );
    }

    /**
     * @param $links
     * @return mixed
     */
    public static function addNetworkSettingsPage($links)
    {
        $settings_link = '<a href="admin.php?page=supr_own_domain_network_settings">' . __('Settings', 'supr-own-domain') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
