��    1      �  C   ,      8     9  �   ;  "        <     N     [     o     �  	   �     �     �     �     �     �     �               *     3  '   ?  (   g     �  
   �     �     �  
   �     �  %   �       
        )     0     5     C     T     ]     i  "   q  #   �     �  
   �  �   �     l     s  d   �     �  
   		     	  �   	     �	  �   �	  '   s
     �
     �
     �
     �
     �
     �
             +   5  
   a     l     �     �     �     �     �  '   �  (        ,     D     Q  	   m     w  +   �  ,   �     �  	   �  	   �     �               "     0     >     J     h     �     �  �   �     >  )   E  s   o     �     �              &       1                 *            %   "       .                           ,   +         $         )          
       -                #   0          '   !   (                 	                              /                     % * Per month. 6 months duration with a notice period of 6 weeks to the respective expiration. Otherwise, the term is extended by another 6 months. All prices excl. VAT. Sales commission on the selling price including VAT.e ** Available for shops in Germany. Activate all apps Activate app Additional Features All Design Templates All Payment Methods Big price Book binding Contact support Coupon cannot be saved. Coupon code Coupon invalid. Coupon saved. Coupon successfully applied. Description Discount Footer text GET value restriction_name is required. Integration of Upsale-Modal in WP Ultimo Make your shop Smart! New upsale No more Limitations Now only Old upsale Please book Smart Please book Smart to use this option. Premium apps Promo code Redeem SALE SUPR - Upsale SUPR Development Settings Show upsale Success The trial period ends after %s day The trial period ends after %s days Title Trial time Upgrade your shop now and benefit from many great features that make your SUPR Shop even more professional: International shipping, premium designs and much more! Upsale We check if the coupon is valid You reached your media upload limit of %d images. Upgrade your account to unlock more media uploads. https://supr.com instead of € MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Project-Id-Version: supr-upsale
Language: de
 % * pro Monat. 6 Monate Laufzeit mit einer Kündigungsfrist von 6 Wochen vor Ablauf der Laufzeit. Ansonsten wird der Tarif um weitere 6 Monate verlängert.
Alle Preise exclusive MwSt.  ** Verfügbar für Shops in Deutschland Alle Apss aktivieren App aktivieren Zusätzliche Funktionen Alle Designvorlagen Alle Bezahlmethoden Großer Preis Verbindlich buchen Support kontaktieren Rabattcode konnte nicht gespeichert werden. Rabattcode Rabattcode ungültig. Rabattcode gespeichert. Rabattcode eingelöst. Beschreibung Rabatt Text in der Fusszeile GET value restriction_name is required. Integration of Upsale-Modal in WP Ultimo Mach deinen Shop SMART! Upsale Seite Keine Einschränkungen mehr Jetzt nur Upsale Modal Buche bitte Smart um diese Option zu nutzen Buche bitte Smart um diese Option zu nutzen. Premium Apps Promocode einlösen SALE SUPR - Upsale SUPR Development Einstellungen Upsale zeigen Erfolgreich Die Probezeit endet in %s Tag Die Probezeit endet in %s Tagen Titel Testzeitraum Upgrade your shop now and benefit from many great features that make your SUPR Shop even more professional: International shipping, premium designs and much more! Upsale Wir prüfen ob der Rabattcode gültig ist Du hast das Limit von %d Medien in deinem Tarif erreicht. Bitte buch den SMART Tarif um weitere Medien hochzuladen. https://supr.com an Stelle von € 