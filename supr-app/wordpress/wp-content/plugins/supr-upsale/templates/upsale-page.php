<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

use \SuprUpsale\Manager;

/** @var \WU_Plan|null $targetPlan */
$targetPlan = null;
/** @var \WU_Plan[] $plans */
$plans = \WU_Plans::get_plans();
foreach ($plans as $plan) {
    if (\strtolower($plan->title) === 'smart') {
        $targetPlan = $plan;
        break;
    }
}

if ($targetPlan === null) {
    error_log('[SUPR-UPSALE] Upsale page doesn\'t work because the plan \'smart\' is not found.');
}

$title = Manager::instance()->getNewOption('title');
$description = Manager::instance()->getNewOption('description');
$couponCode = Manager::instance()->getNewOption('coupon_code');
$discount = Manager::instance()->getNewOption('discount');

$footerText = Manager::instance()->getNewOption('footer_text');

$billwerkVariants = $targetPlan->billwerk_variant_ids ?: [];
$freq = WU_Settings::get_setting('default_pricing_option');

$priceAttr = '';
foreach ($billwerkVariants as $variantFreq => $variantId) {
    $priceAttr .= " data-variant-id-{$freq}='{$variantId}'";
}

$bigPrice = Manager::instance()->getOption('bigPrice') * 1;
?>

<div class="wrap" id="supr-upsale-new-content-box">
    <div class="supr-upsale-page-hero">
        <img src="<?= SUPR_UPSALE_URL ?>img/supr-upsale-page-hero.jpg">
    </div>

    <div class="supr-upsale-page-content">
        <?php if ($title) : ?>
            <h2 class="supr-upsale-page-title">
                <?= $title; ?>
            </h2>
        <?php endif; ?>
        <div class="row">
            <div class="supr-upsale-message wu-col-sm-12"></div>
        </div>
        <form method="post" action="<?php echo wu_get_active_gateway()->get_url('change-plan'); ?>">
            <div class="row">
                <div class="wu-col-sm-8">
                    <div class="supr-upsale-page-description">
                        <?= do_shortcode(stripslashes($description)); ?>
                    </div>
                </div>
                <div class="wu-col-sm-4">
                    <div class="supr-upsale-modal-price-container animated bounce">
                        <span id="supr-upsale-coupon-flag" class="hide supr-upsale-modal-price-ribbon"><?= __('%', 'supr-upsale'); ?></span>
                        <span id="supr-upsale-original-price" class="hide text-line-through text-danger text-bold text-uppercase original-price"><?= $targetPlan->get_price() . __(
                            '€',
                            'supr-upsale'
                        ); ?></span>
                        <span class="text-bold text-uppercase block"><?= __('Now only', 'supr-upsale'); ?></span>
                        <div class="supr-upsale-modal-price" data-original-price='<?= $targetPlan->get_price(); ?>' data-package-id="<?= get_post_meta(
                            $targetPlan->get_id(),
                            'wpu_billwerk_plan_id',
                            true
                        ); ?>" data-freq="<?= $freq; ?>"<?= $priceAttr; ?>>
                            <span class="supr-upsale-modal-price-number"><?= $targetPlan->get_price(); ?></span><?= __('€', 'supr-upsale'); ?>
                            <sup>*</sup>
                        </div>
                        <?php if ($bigPrice > $targetPlan->get_price()) : ?>
                            <span class="text-uppercase"><?= __('instead of', 'supr-upsale'); ?> <?= (int)$bigPrice; ?><?= __('€', 'supr-upsale'); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="supr-upsale-modal-promocode">
                        <input name="coupon_id" class="input-promocode supr-upsale-coupon-input" type="text" value="" placeholder="<?= __('Promo code', 'supr-upsale'); ?>"/>
                        <button type="button" class="button button-secondary button-block supr-upsale-coupon-activate-button"><?= __('Redeem', 'supr-upsale'); ?></button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="wu_action" value="wu_change_plan">
            <input type="hidden" id="wu_plan_freq" name="plan_freq" value="<?= $freq; ?>">
            <input type="hidden" name="plan_id" value="<?= $targetPlan->get_id(); ?>">
            <?php
            // Print _nonce without id, because on some pages we already have other nonce with ids
            echo str_replace('id="_wpnonce"', '', wp_nonce_field('wu-change-plan', '_wpnonce', true, false));
            ?>
            <input type="submit" class="button button-primary button-block" value="<?= __('Book binding', 'supr-upsale'); ?>">
            <div class="row">
                <div class="wu-col-sm-12">
                    <div class="supr-upsale-page-content-footer">
                        <?= do_shortcode(stripslashes($footerText)); ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>