<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

// Handle request
if (isset($_POST['supr_upsale_options_new']) && is_array($_POST['supr_upsale_options_new'])) {
    $newOptions = $_POST['supr_upsale_options_new'];

    // Trim all values
    $newOptions = array_map('trim', $newOptions);
    // Remove empty values
    $newOptions = array_filter($newOptions);
    // Sanitize values with WP special function
    $newOptions = array_map('sanitize_text_field', $newOptions);

    // Save also post from WP Editor
    if (isset($_POST['supr_upsale_options_new_description'])) {
        $newOptions['description'] = wp_kses_post($_POST['supr_upsale_options_new_description']);
    }

    if (isset($_POST['supr_upsale_options_new_footer_text'])) {
        $newOptions['footer_text'] = wp_kses_post($_POST['supr_upsale_options_new_footer_text']);
    }

    update_option('supr_upsale_options_new', $newOptions);
}

$options = get_option('supr_upsale_options_new', []);
?>
<form action="" method="POST">
    <table class="form-table">
        <tr>
            <td><label for="supr_upsale_options_new_trial_time"><?= __('Trial time', 'supr-upsale'); ?></label></td>
            <td><input style="width: 100%;" type="text" placeholder="PT1H or P30D or P1M" id="supr_upsale_options_new_trial_time" name="supr_upsale_options_new[trial_time]" value="<?= $options['trial_time'] ?? ''; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_upsale_options_new_title"><?= __('Title', 'supr-upsale'); ?></label></td>
            <td><input style="width: 100%" type="text" id="supr_upsale_options_new_title" name="supr_upsale_options_new[title]" value="<?= $options['title'] ?? ''; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_upsale_options_new_description"><?= __('Description', 'supr-upsale'); ?></label></td>
            <td>
                <?php
                $settings = array(
                    'wpautop' => false,
                    'media_buttons' => false,
                    'textarea_rows' => 10,
                );
                wp_editor(stripslashes($options['description'] ?? ''), 'supr_upsale_options_new_description', $settings);
                ?>
            </td>
        </tr>
        <tr>
            <td><label for="supr_upsale_options_new_footer_text"><?= __('Footer text', 'supr-upsale'); ?></label></td>
            <td>
                <?php
                $settings = array(
                    'wpautop' => false,
                    'media_buttons' => false,
                    'textarea_rows' => 10
                );
                wp_editor(stripslashes($options['footer_text'] ?? ''), 'supr_upsale_options_new_footer_text', $settings);
                ?>
            </td>
        </tr>
        <tr>
            <td><label for="supr_upsale_options_new_coupon_code"><?= __('Coupon code', 'supr-upsale'); ?></label></td>
            <td><input style="width: 100%" type="text" id="supr_upsale_options_new_coupon_code" name="supr_upsale_options_new[coupon_code]" value="<?= $options['coupon_code'] ?? ''; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_upsale_options_new_discount"><?= __('Discount', 'supr-upsale'); ?></label></td>
            <td><input style="width: 200px;" type="number" id="supr_upsale_options_new_discount" name="supr_upsale_options_new[discount]" value="<?= $options['discount'] ?? ''; ?>"/>%</td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>