<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

// Handle request
if (isset($_POST['supr_upsale_options']) && is_array($_POST['supr_upsale_options'])) {
    $newOptions = $_POST['supr_upsale_options'];

    // Trim all values
    $newOptions = array_map('trim', $newOptions);
    // Remove empty values
    $newOptions = array_filter($newOptions);
    // Sanitize values with WP special function
    $newOptions = array_map('sanitize_text_field', $newOptions);

    update_option('supr_upsale_options', $newOptions);
}

$options = get_option('supr_upsale_options', []);
?>
<button id="supr_upsale_show_upsale" class="button button-primary"><?= __('Show upsale', 'supr-upsale'); ?></button>
<form action="" method="POST">
    <table class="form-table">
        <tr>
            <td><label for="supr_upsale_option_title"><?= __('Title', 'supr-upsale'); ?></label></td>
            <td><input style="width: 80%" type="text" id="supr_upsale_option_title" name="supr_upsale_options[title]" value="<?= $options['title'] ?? ''; ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_upsale_option_description"><?= __('Description', 'supr-upsale'); ?></label></td>
            <td><textarea style="width: 80%; height: 200px" id="supr_upsale_option_description" name="supr_upsale_options[description]"><?= $options['description'] ?? ''; ?></textarea></td>
        </tr>
        <tr>
            <td><label for="supr_upsale_option_big_price"><?= __('Big price', 'supr-upsale'); ?></label></td>
            <td><input type="text" id="supr_upsale_option_big_price" name="supr_upsale_options[bigPrice]" value="<?= $options['bigPrice'] ?? ''; ?>"/></td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>