<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$tabs = [
    'old-upsale' => __('Old upsale', 'supr-upsale'),
    'new-upsale' => __('New upsale', 'supr-upsale')
];

$currentTab = (isset($_GET['tab']) && isset($tabs[$_GET['tab']])) ? $_GET['tab'] : 'old-upsale';
?>
<div class="wrap supr-upsale">
    <h2><?= get_admin_page_title(); ?></h2>
    <h2 class="nav-tab-wrapper">
        <?php foreach ($tabs as $tab => $tabName) : ?>
            <a class="nav-tab<?= $currentTab === $tab ? ' nav-tab-active' : ''; ?>" href="?page=<?= $_GET['page'] ?>&tab=<?= $tab; ?>"><?= $tabName; ?></a>
        <?php endforeach; ?>
    </h2>

    <?php

    // Include tabs template
    include SUPR_UPSALE_PATH . "templates/settings-tabs/{$currentTab}.php";
    ?>
</div>