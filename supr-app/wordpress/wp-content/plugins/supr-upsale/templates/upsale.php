<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

use SuprUpsale\Manager;

/** @var \WU_Plan|null|bool $plan */

if (!$plan) {
    error_log('[SUPR-UPSALE] Plan for template upsale.php is undefined. The template cannot be rendered.');
} else {
    $billwerkVariants = $plan->billwerk_variant_ids ?: [];
    $freq = WU_Settings::get_setting('default_pricing_option');

    $priceAttr = '';
    foreach ($billwerkVariants as $variantFreq => $variantId) {
        $priceAttr .= " data-variant-id-{$freq}='{$variantId}'";
    }

    $bigPrice = Manager::instance()->getOption('bigPrice') * 1;
    ?>
    <div id="supr-upsale-modal-wrapper">
        <div class="supr-upsale-modal">
            <div class="row">
                <div class="supr-upsale-message wu-col-sm-12"></div>
            </div>
            <form method="post" action="<?php echo wu_get_active_gateway()->get_url('change-plan'); ?>">
                <input type="hidden" name="wu_action" value="wu_change_plan">
                <input type="hidden" id="wu_plan_freq" name="plan_freq" value="<?= $freq; ?>">
                <input type="hidden" name="plan_id" value="<?= $plan->get_id(); ?>">
                <?php
                // Print _nonce without id, because on some pages we already have other nonce with ids
                echo str_replace('id="_wpnonce"', '', wp_nonce_field('wu-change-plan', '_wpnonce', true, false));
                ?>
                <div class="row">
                    <div class="wu-col-sm-8">
                        <h2 class="text-uppercase text-bolder wp-heading-inline">
                    <span>
                        <?= __(Manager::instance()->getOption('title'), 'supr-upsale'); ?>
                    </span>
                        </h2>
                        <p class="text-lead">
                            <?= __(Manager::instance()->getOption('description'), 'supr-upsale'); ?>
                        </p>
                        <div class="row">
                            <div class="wu-col-sm-4">
                                <div class="text-uppercase text-bold supr-upsale-modal-arg">
                                    <i class="la la-brush"></i><br>
                                    <?= __('All Design Templates', 'supr-upsale'); ?>
                                </div>
                            </div>
                            <div class="wu-col-sm-4">
                                <div class="text-uppercase text-bold supr-upsale-modal-arg">
                                    <i class="la la-coins"></i><br>
                                    <?= __('All Payment Methods', 'supr-upsale'); ?>
                                </div>
                            </div>
                            <!--
                        <div class="wu-col-sm-4">
                            <div class="text-uppercase text-bold supr-upsale-modal-arg">
                                <i class="streamline streamline-Lock-Unlock animated tada delay-3s"></i><br>
                                <?= __('No more Limitations', 'supr-upsale'); ?>
                        </div>
                        </div>-->
                            <div class="wu-col-sm-4">
                                <div class="text-uppercase text-bold supr-upsale-modal-arg">
                                    <i class="la la-list-alt"></i><br>
                                    <?= __('Additional Features', 'supr-upsale'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wu-col-sm-4">
                        <div class="supr-upsale-modal-price-container animated bounce">
                            <?php if ($bigPrice > $plan->get_price()) : ?>
                                <span class="supr-upsale-modal-price-ribbon"><?= __('SALE', 'supr-upsale'); ?></span>
                            <?php endif; ?>
                            <span id="supr-upsale-coupon-flag" class="hide supr-upsale-modal-price-ribbon"><?= __('%', 'supr-upsale'); ?></span>
                            <span id="supr-upsale-original-price" class="hide text-line-through text-danger text-bold text-uppercase original-price"><?= $plan->get_price() . __('€', 'supr-upsale'); ?></span>
                            <span class="text-bold text-uppercase block"><?= __('Now only', 'supr-upsale'); ?></span>
                            <div class="supr-upsale-modal-price" data-original-price='<?= $plan->get_price(); ?>' data-package-id="<?= get_post_meta($plan->get_id(), 'wpu_billwerk_plan_id', true); ?>" data-freq="<?= $freq; ?>" <?= $priceAttr; ?>>
                                <span class="supr-upsale-modal-price-number"><?= $plan->get_price(); ?></span><?= __('€', 'supr-upsale'); ?>
                                <sup>*</sup>
                            </div>
                            <?php if ($bigPrice > $plan->get_price()) : ?>
                                <span class="text-uppercase"><?= __('instead of', 'supr-upsale'); ?> <?= (int)$bigPrice; ?><?= __('€', 'supr-upsale'); ?></span>
                            <?php endif; ?>
                        </div>
                        <div class="supr-upsale-modal-promocode">
                            <input name="coupon_id" class="input-promocode supr-upsale-coupon-input" type="text" value="" placeholder="<?= __('Promo code', 'supr-upsale'); ?>"/>
                            <button type="button" class="button button-secondary button-block supr-upsale-coupon-activate-button"><?= __('Redeem', 'supr-upsale'); ?></button>
                        </div>
                    </div>
                </div>
                <input type="submit" class="button button-primary button-block" value="<?= __('Book binding', 'supr-upsale'); ?>">
                <div class="wu-col-sm-12">
                    <small>
                        <?= __(
                            '* Per month. 6 months duration with a notice period of 6 weeks to the respective expiration. Otherwise, the term is extended by another 6 months. All prices excl. VAT. Sales commission on the selling price including VAT.e',
                            'supr-upsale'
                        ); ?>
                        <?= __('** Available for shops in Germany.', 'supr-upsale'); ?>
                    </small>
                </div>
            </form>
        </div>
    </div>
<?php }
