<?php

namespace SuprUpsale;

/**
 * Class ShortCodeManager
 *
 * @package SuprUpsale
 */
class ShortCodeManager
{
    /**
     * Register short codes
     */
    public static function registerShortCodes(): void
    {
        \add_shortcode('upsale-couponcode', [self::class, 'printCouponCode']);
        \add_shortcode('upsale-discount', [self::class, 'printCouponDiscount']);
    }

    /**
     * Print coupon code
     */
    public static function printCouponCode(): string
    {
        return Manager::instance()->getNewOption('coupon_code');
    }

    /**
     * Print coupon discount
     */
    public static function printCouponDiscount(): string
    {
        return Manager::instance()->getNewOption('discount');
    }
}
