<?php

namespace SuprUpsale;

/**
 * Class ResourceManager
 *
 * @package SuprUpsale
 */
class ResourceManager
{
    /**
     * Register files for plugin
     */
    public static function addUpsaleResources(): void
    {
        // CSS
        \wp_register_style('supr_upsale_style', SUPR_UPSALE_URL . 'css/supr-upsale.css', [], SUPR_UPSALE_VERSION, 'all');

        // JS
        \wp_register_script('supr_upsale_js', SUPR_UPSALE_URL . 'js/supr-upsale.js', ['jquery'], SUPR_UPSALE_VERSION, true);

        // Add css and js into footer
        \add_action('admin_enqueue_scripts', [__CLASS__, 'enqueueUpsaleFiles'], 0);

        // Add upsale template also in footer
        \add_action('admin_footer', [__CLASS__, 'printUpsaleTemplate']);
    }

    /**
     * Register files for plugin
     */
    public static function addDownsaleResources(): void
    {
        // JS
        \wp_register_script('supr_downgrade_js', SUPR_UPSALE_URL . 'js/supr-downgrade.js', ['jquery'], SUPR_UPSALE_VERSION, true);

        // Add css and js into footer
        \add_action('admin_enqueue_scripts', [__CLASS__, 'enqueueDownsaleFiles'], 0);
    }

    /**
     * Add files
     */
    public static function enqueueUpsaleFiles(): void
    {
        \wp_enqueue_style('supr_upsale_style');
        \wp_enqueue_script('supr_upsale_js');
    }

    /**
     * Add files
     */
    public static function enqueueDownsaleFiles(): void
    {
        \wp_enqueue_script('supr_downgrade_js');
    }

    /**
     * Add js variables to js script
     */
    public static function addJsUpsaleVariables(): void
    {
        $array = [
            'root' => \esc_url_raw(rest_url()),
            'nonce' => \wp_create_nonce('wp_rest'),
            'coupon_error' => __('Coupon invalid.', 'supr-upsale'),
            'coupon_success' => __('Coupon successfully applied.', 'supr-upsale'),
            'coupon_check' => __('We check if the coupon is valid', 'supr-upsale')
        ];

        \wp_localize_script('supr_upsale_js', 'supr_upsale_values', $array);
    }

    /**
     * Add js variables to js script
     */
    public static function addJsDownsaleVariables(): void
    {
        $array = [
            'current_plan' => strtolower(wu_get_account_plan(get_current_blog_id())),
            'button_contact_support' => __('Contact support', 'supr-upsale')
        ];

        \wp_localize_script('supr_downgrade_js', 'supr_downgrade_values', $array);
    }

    public static function printUpsaleTemplate(): void
    {
        $plan = null;

        if (\function_exists('\wu_get_plan_by_slug')) {
            \switch_to_blog(1);
            $plan = \wu_get_plan_by_slug('smart');
            \restore_current_blog();
        }

        require SUPR_UPSALE_PATH . 'templates/upsale.php';
    }
}
