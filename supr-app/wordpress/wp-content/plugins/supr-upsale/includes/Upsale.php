<?php

namespace SuprUpsale;

/**
 * Class Upsale
 *
 * @package SuprUpsale
 */
class Upsale
{
    /**
     * @var Upsale
     */
    private static $instance;

    /**
     * @var string
     */
    public static $trialPlan = 'smart trial';

    /**
     * @var string
     */
    private $trialPeriod;

    private $whiteListPages = [
        '/wp-admin/edit.php?post_type=shop_order',
        '/wp-admin/tools.php?page=remove_personal_data',
        '/wp-admin/admin.php?page=wc-settings',
        '/wp-admin/admin.php?page=wu-my-account',
        '/wp-admin/admin-ajax.php'
    ];

    //public static $trialPeriod = 'P1M';

    /**
     * @return Upsale
     */
    public static function instance(): Upsale
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
    }

    /**
     * Get trial period
     *
     * @return bool|string
     */
    public function getTrialPeriod()
    {
        if ($this->trialPeriod !== null) {
            return $this->trialPeriod;
        }

        // Default 1 month
        $this->trialPeriod = Manager::instance()->getNewOption('trial_time') ?? 'P1M';

        return $this->trialPeriod;
    }

    public function checkTrialPeriod(): void
    {
        // If trial period has been expired
        if ($this->trialPeriodExpired()) {
            // Enable maintenance mode
            \update_option('elementor_maintenance_mode_mode', 1);

            // Redirect to upsale page
            if ((!isset($_GET['page']) || $_GET['page'] !== 'supr_upsale_upsale_page')
                && !$this->isWhitePage()
                && \is_admin()
            ) {
                wp_redirect('/wp-admin/admin.php?page=supr_upsale_upsale_page');
                die();
            }
        }
    }

    /**
     * Check if trial period expired
     *
     * @return bool
     * @throws \Exception
     */
    public function trialPeriodExpired(): bool
    {
        $blogDetails = get_blog_details(null, false);

        $registeredDate = new \DateTime($blogDetails->registered);

        return Manager::instance()->getPlanName() === $this::$trialPlan && $registeredDate->add(new \DateInterval($this->getTrialPeriod())) < new \DateTime();
    }

    /**
     * Check if trial plan
     *
     * @return bool
     */
    public function isTrialPlan(): bool
    {
        return Manager::instance()->getPlanName() === $this::$trialPlan;
    }

    /**
     * @return \DateInterval|null
     * @throws \Exception
     */
    public function getTrialRestTime(): ?\DateInterval
    {
        if (!$this->isTrialPlan() || $this->trialPeriodExpired()) {
            return null;
        }

        $blogDetails = get_blog_details(null, false);

        $registeredDate = new \DateTime($blogDetails->registered);

        return $registeredDate->add(new \DateInterval($this->getTrialPeriod()))->diff(new \DateTime());
    }

    /**
     * @return bool
     */
    private function isWhitePage(): bool
    {
        foreach ($this->whiteListPages as $whiteListPage) {
            if (\strpos($_SERVER['REQUEST_URI'], $whiteListPage) === 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Rest endpoint
     */
    public static function saveCoupon()
    {
        $userId = wp_get_current_user()->ID;
        $metaKey = 'coupon_id';
        $coupon = strtoupper($_REQUEST['coupon']);

        if (update_user_meta($userId, $metaKey, $coupon) || add_user_meta($userId, $metaKey, $coupon)) {
            return wp_send_json_success(
                [
                    'message' => __('Coupon saved.', 'supr-upsale'),
                    'coupon' => $coupon
                ]
            );
        }

        return wp_send_json_error(
            [
                'message' => __('Coupon cannot be saved.', 'supr-upsale')
            ]
        );
    }
}
