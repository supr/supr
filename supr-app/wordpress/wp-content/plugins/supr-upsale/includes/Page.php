<?php

namespace SuprUpsale;

/**
 * Class Page
 *
 * @package SuprUpsale
 */
class Page
{
    /**
     * Show setting page
     */
    public static function networkSettingsPage(): void
    {
        include SUPR_UPSALE_PATH . 'templates/network-settings-page.php';
    }

    public static function upsalePage(): void
    {
        include SUPR_UPSALE_PATH . 'templates/upsale-page.php';
    }
}
