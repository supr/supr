<?php

namespace SuprUpsale;

/**
 * Class Plugin
 *
 * @package SuprUpsale
 */
class Plugin
{
    /**
     * @var null
     */
    public static $instance;

    /**
     * @return \SuprUpsale\Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();

            //Fires when SuprUpsale was fully loaded and instantiated.
            do_action('supr-upsale/loaded');
        }

        return self::$instance;
    }

    /**
     * Init
     */
    public function adminInit(): void
    {
        // We don't load the Plugin for users with smart and pro
        if ((isset($_GET['page']) && \in_array($_GET['page'], ['supr_upsale_settings', 'supr_upsale_upsale_page']))
            || !\in_array(Manager::instance()->getPlanName(), ['smart', 'pro'])
        ) {
            ResourceManager::addUpsaleResources();
            ResourceManager::addJsUpsaleVariables();
        }

        // Show trial time
        AdminBar::instance()->addAdminBar();

        // Add customisation for Plan page
        ResourceManager::addDownsaleResources();
        ResourceManager::addJsDownsaleVariables();

        // Action after the plugin was loaded
        do_action('supr-upsale/init');
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // Add menu
        Menu::addMenu();

        // Register API routes
        Api::registerRoutes();

        // Check Trial Period (but not for main blog)
        if (\get_current_blog_id() !== 1) {
            $upsale = Upsale::instance();
            add_action('init', [$upsale, 'checkTrialPeriod'], 0);
            add_action('admin_init', [$upsale, 'checkTrialPeriod'], 0);
        }

        // Shortcodes
        ShortCodeManager::registerShortCodes();

        // Load translations
        add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        add_action('admin_init', [$this, 'adminInit'], -10);

        add_action('wp_ajax_supr_upsale_save_coupon', [Upsale::class, 'saveCoupon']);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_UPSALE_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::instance();
