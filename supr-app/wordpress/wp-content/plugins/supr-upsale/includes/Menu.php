<?php

namespace SuprUpsale;

/**
 * Class Menu
 *
 * @package SuprUpsale
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add menu page
        add_action('network_admin_menu', [__CLASS__, 'createNetworkAdminMenu']);

        // Add upsale page
        add_action('admin_menu', [__CLASS__, 'createUpsalePage']);

        // Add settings link on plugin page
        add_filter('network_admin_plugin_action_links_' . SUPR_UPSALE_FILE, [__CLASS__, 'addSettingsPage']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createUpsalePage()
    {
        add_submenu_page(
            null,
            __('Please book Smart', 'supr-upsale'),
            __('Please book Smart', 'supr-upsale'),
            'manage_options',
            'supr_upsale_upsale_page',
            [Page::class, 'upsalePage']
        );
    }

    /**
     * Add menu to network admin panel
     */
    public static function createNetworkAdminMenu()
    {
        add_menu_page(
            __('Upsale', 'supr-upsale'),
            __('Upsale', 'supr-upsale'),
            'manage_network',
            'supr_upsale_settings',
            [Page::class, 'networkSettingsPage'],
            'dashicons-external',
            207
        );
    }

    /**
     * @param $links
     * @return mixed
     */
    public static function addSettingsPage($links)
    {
        $settings_link = '<a href="admin.php?page=supr_upsale_settings">' . __('Settings', 'supr-upsale') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
