<?php

namespace SuprUpsale;

class Api
{
    public static function registerRoutes(): void
    {
        add_action(
            'rest_api_init',
            function () {
                register_rest_route(
                    'supr_upsale_rest/v1',
                    '/check_restriction',
                    [
                        'methods' => 'GET',
                        'callback' => [self::class, 'checkRestriction'],
                        'permission_callback' => function () {
                            return current_user_can('administrator') || current_user_can('editor');
                        }
                    ]
                );
            }
        );
    }

    public static function checkRestriction($data): void
    {
        if (!isset($_GET['restriction_name'])) {
            wp_send_json_error(__('GET value restriction_name is required.', 'supr-upsale'));

            return;
        }

        $restrictionName = trim($_GET['restriction_name']);

        // We allow it for network admins
        // @todo maybe for all restrictions ?
        if ($restrictionName === 'design_store' && is_super_admin()) {
            wp_send_json_success(__('Success', 'supr-upsale'));

            return;
        }

        switch ($restrictionName) {
            case 'design_store':
            case 'own_domain':
                if (\in_array(Manager::instance()->getPlanName(), ['smart', 'smart trial', 'pro'])) {
                    wp_send_json_success(__('Success', 'supr-upsale'));

                    return;
                }
                break;
            case 'payment_methods':
                if (\in_array(Manager::instance()->getPlanName(), ['basic', 'smart', 'smart trial', 'pro'])) {
                    wp_send_json_success(__('Success', 'supr-upsale'));

                    return;
                }
                break;
            case 'media_count':
                if (!class_exists('\WU_Plan')) {
                    error_log('[Supr Upsale] Class \WU_Plan doesn\'t exist.');
                    break;
                }

                // Get user post count in that post type
                $post_count = wp_count_posts('attachment');
                $post_count = $post_count->inherit;

                // Get the allowed quota
                $plan = Manager::instance()->getPlan();
                $quota = $plan ? $plan->get_quota('attachment') : 0;

                if ($quota > 0 && $post_count >= $quota) {
                    wp_send_json_error(sprintf(__('You reached your media upload limit of %d images. Upgrade your account to unlock more media uploads.', 'supr-upsale'), $quota));

                    return;
                }

                wp_send_json_success(__('Success', 'supr-upsale'));

                return;

                break;
        }

        wp_send_json_error(__('Please book Smart to use this option.', 'supr-upsale'));
    }
}
