<?php

namespace SuprUpsale;

class TextDomain
{
    public static $domainName = 'supr-upsale';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_UPSALE_DIR_NAME . '/languages/');
    }
}
