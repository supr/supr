<?php

namespace SuprUpsale;

/**
 * Class AdminBar
 *
 * @package SuprUpsale
 */
class AdminBar
{
    /**
     * @var AdminBar
     */
    private static $instance;

    /**
     * @var \DateInterval
     */
    private $trialRestTime;

    /**
     * @return AdminBar
     */
    public static function instance(): AdminBar
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Add trial time node
     */
    public function addTrialTimeAdminBarNode(): void
    {
        /** @var \WP_Admin_Bar $wp_admin_bar */
        global $wp_admin_bar;

        if (\is_admin() && \is_user_logged_in() && $this->getTrialRestTime() !== null) {
            $wp_admin_bar->add_menu(
                array(
                    'id' => 'trial-time',
                    'parent' => 'top-secondary',
                    'title' => \sprintf(
                        __($this->getTrialRestTime()->days === 1 ? 'The trial period ends after %s day' : 'The trial period ends after %s days', 'supr-upsale'),
                        \number_format_i18n($this->getTrialRestTime()->days)
                    ),
                    'href' => '#'
                )
            );
        }
    }

    /**
     * Add admin bar
     */
    public function addAdminBar(): void
    {
        \add_action('wp_before_admin_bar_render', [$this, 'addTrialTimeAdminBarNode']);
    }

    /**
     * Get
     *
     * @return bool|\DateInterval
     */
    public function getTrialRestTime()
    {
        if ($this->trialRestTime === null) {
            $this->trialRestTime = Upsale::instance()->getTrialRestTime();
        }

        return $this->trialRestTime;
    }
}
