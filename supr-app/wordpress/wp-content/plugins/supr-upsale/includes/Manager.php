<?php

namespace SuprUpsale;

/**
 * Class Manager
 *
 * @package SuprUpsale
 */
class Manager
{
    /**
     * @var null|Manager
     */
    private static $instance;

    private $shopsWithoutPlan = [1, 8];

    /**
     * @var null|array
     */
    private $options;

    /**
     * @var null|array
     */
    private $optionsNew;

    /**
     * CampaignMonitorManager constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return \SuprUpsale\Manager
     */
    public static function instance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Options for old upsale
     *
     * @param $optionName
     * @return bool|string
     */
    public function getOption($optionName)
    {
        if ($this->options !== null) {
            return $this->options[$optionName] ?? false;
        }

        $options = get_blog_option(1, 'supr_upsale_options', []);

        $this->options = array_merge($this->getDefaultOptions(), array_filter($options));

        return $this->getOption($optionName);
    }

    /**
     * @return array
     */
    public function getDefaultOptions(): array
    {
        return [
            'title' => __('Make your shop Smart!', 'supr-upsale'),
            'description' => __(
                'Upgrade your shop now and benefit from many great features that make your SUPR Shop even more professional: International shipping, premium designs and much more!',
                'supr-upsale'
            ),
            'bigPrice' => 0
        ];
    }

    /**
     * Options for new upsale
     *
     * @param $optionName
     * @return bool|string
     */
    public function getNewOption($optionName)
    {
        if ($this->optionsNew !== null) {
            return $this->optionsNew[$optionName] ?? false;
        }

        $options = get_blog_option(1, 'supr_upsale_options_new', []);

        $this->optionsNew = array_filter($options);

        return $this->getNewOption($optionName);
    }

    /**
     * Get current plan for shop
     *
     * @param null $shopId
     * @return \WU_Plan|false
     */
    public function getPlan($shopId = null)
    {
        if ($shopId === null) {
            $shopId = \get_current_blog_id();
        }

        if (\in_array($shopId, $this->shopsWithoutPlan, true)) {
            return false;
        }

        $site = \wu_get_site($shopId);

        $plan = $site->get_plan();

        if ($plan === false) {
            error_log('[Supr Upsale] Plan for shop #' . $shopId . ' doesn\'t exist.');
        }

        return $plan;
    }

    /**
     * Get name of current plan for shop
     *
     * @param null $shopId
     * @return string
     */
    public function getPlanName($shopId = null): string
    {
        $plan = $this->getPlan($shopId);

        return $plan ? strtolower($plan->title) : 'starter';
    }
}
