<?php
/**
 * Plugin Name: SUPR - Upsale
 * Plugin URI: https://supr.com
 * Description: Integration of Upsale-Modal in WP Ultimo
 * Text Domain: supr-upsale
 * Domain Path: /languages
 * Version: 1.0
 * Author: SUPR Development
 * License: GPL
 */

define('SUPR_UPSALE_PATH', plugin_dir_path(__FILE__));
define('SUPR_UPSALE_URL', plugins_url('/', __FILE__));
define('SUPR_UPSALE_FILE', plugin_basename(__FILE__));
define('SUPR_UPSALE_DIR_NAME', basename(__DIR__));

define('SUPR_UPSALE_VERSION', '1.0.0');

// Load main Class
require SUPR_UPSALE_PATH . 'includes/Plugin.php';
