(function ($) {
    var supr_upsale_selectors = [{
        selector: '.supr-design-store-object-activate',
        restriction: 'design_store'
    }, {
        selector: '#set-post-thumbnail',
        restriction: 'media_count'
    }, {
        selector: '#insert-media-button',
        restriction: 'media_count'
    }, {
        selector: '#__wp-uploader-id-1',
        restriction: 'media_count'
    }, {
        selector: '#__wp-uploader-id-2',
        restriction: 'media_count'
    }, {
        selector: '#supr-cop-onboarding-sign-up',
        restriction: 'payment_methods'
    }, {
        selector: '#wu-custom-domain, #wu-custom-domain button.wu-confirm, #wu-custom-domain input[name=custom-domain]',
        restriction: 'own_domain'
    }
    ];

    // Listen all upsale selectors
    for (var i = 0; i < supr_upsale_selectors.length; i++) {
        // We should add i to the function, because we wil not see it in loop
        $(supr_upsale_selectors[i].selector).on('click', {number: i}, function (event) {
            // Set i for nexts functions
            var i = event.data.number;

            // Save element
            var element = $(this);

            // We check, if it is also allowed
            if ($(this).data('supr_upsale_allowed') === true) {
                // We do nothing
                return true;
            } else {
                // Not allow to go further
                event.stopImmediatePropagation();
            }

            supr_check_restriction(supr_upsale_selectors[i].restriction, function () {
                // Allow to ckick and try to trigger next action
                element.data('supr_upsale_allowed', true);
                element.click();
            });

            return false;
        });
    }

    // Show it in Admin per click
    $('#supr_upsale_show_upsale').on('click', function () {
        supr_upsale_show_modal('admin_call', 'Hello! I\'m here because you called me.');
    });

    // Show it on plugins page
    $('.supr-upsale-plugins-show-upsale').on('click', function () {
        supr_upsale_show_modal('plugins');
    });

    // Show it in Admin bar per click
    $('#wp-admin-bar-trial-time').on('click', function () {
        supr_upsale_show_modal();
    });

    // Show it if we have hash
    supr_upsale_check_hash();

    // Listen on hash change
    $(window).on('hashchange', function () {
        supr_upsale_check_hash();
    });

    // Listen coupon code in new upsale
    if ($('#supr-upsale-new-content-box').length) {
        listen_coupon_code('#supr-upsale-new-content-box');
    }

    // Enable own domain input
    $('#wu-custom-domain input[name=custom-domain]').removeAttr("disabled").attr('readonly', true);
})(jQuery);

/**
 * Check hash and show upsale
 */
function supr_upsale_check_hash()
{
    // For example: #show-upsale_coupon-LALA
    var hash = window.location.hash;
    hash = hash.replace("#", "");
    var hashes = hash.split("_");

    var start_upsale = false;
    var coupon = undefined;

    for (var i = 0; i < hashes.length; i++) {
        // Check if show
        if (hashes[i] === 'show-upsale') {
            start_upsale = true;
            continue;
        }
        // Check if coupon
        var parts = hashes[i].split('-');

        if (parts.length === 2 && parts[0] === 'coupon') {
            coupon = parts[1];
        }
    }

    if (start_upsale) {
        // Without it we have `swal is not defined`
        setTimeout(function () {
            supr_upsale_show_modal(undefined, undefined, coupon);
        }, 750);
    }
}

function supr_check_restriction(restriction_name, callback)
{
    var data = {
        restriction_name: restriction_name
    };

    jQuery.ajax({
        url: supr_upsale_values.root + 'supr_upsale_rest/v1/check_restriction',
        data: data,
        type: 'GET',
        beforeSend: function (xhr) {
            // Add cookies to check auth
            xhr.setRequestHeader('X-WP-Nonce', supr_upsale_values.nonce);
        },
        success: function (data) {
            if (data.success) {
                // If allowed, call callback
                if (callback !== undefined) {
                    callback();
                }
            } else {
                // Show upsale
                supr_upsale_show_modal(restriction_name, data.data);
            }
        },
        error: function (xhr, status, error) {
            console.log('Error: I can\'t check restriction \'' + restriction_name + '\'.');
        }
    });
}

function supr_upsale_show_modal(restriction_name, message, coupon)
{
    Swal.fire({
        html: jQuery('#supr-upsale-modal-wrapper').html(),
        width: '55rem',
        padding: '30px 50px',
        customClass: 'animated zoomIn faster',
        showCloseButton: true,
        showCancelButton: false,
        showConfirmButton: false,
        allowEscapeKey: false,
        showClass: {
            popup: '',
            backdrop: '',
            icon: '',
        },
        hideClass: {
            popup: '',
            backdrop: '',
            icon: '',
        },
        onOpen: function () {
            listen_coupon_code(Swal.getContent(), message, coupon);
        }
    });
}


function listen_coupon_code(selector, message, coupon)
{
    var content_box = jQuery(selector);
    var coupon_button = content_box.find('.supr-upsale-coupon-activate-button');
    var coupon_input = content_box.find('.supr-upsale-coupon-input');
    var price_box = content_box.find('.supr-upsale-modal-price');
    var freq = price_box.data('freq');
    var variantId = price_box.data('variant-id-' + freq);
    var message_box = content_box.find('.supr-upsale-message');

    if (message !== undefined) {
        message_box.html(jQuery('<div />').addClass('notice').addClass('inline').addClass('notice-info').append(jQuery('<p />').html(message)));
    }

    // Listen click
    coupon_button.on('click', function () {
        apply_coupon();
    });

    // Listen enter
    coupon_input.on('keypress', function (event) {
        if (event.which === 13) {
            // Not allow to send form
            event.stopImmediatePropagation();
            apply_coupon();
            return false;
        }
    });

    /**
     * Check coupon if valid and sent it to save
     */
    var apply_coupon = function () {
        jQuery.ajax({
            url: '/wp-admin/admin-ajax.php',
            data: {
                action: 'wub_check_coupon',
                coupon: coupon_input.val()
            },
            type: 'GET',
            beforeSend: function () {
                content_box.addClass('disabled');
            },
            success: function (data) {
                content_box.removeClass('disabled');
                if (data.success === true) {
                    // Check if this coupon is for this plan
                    if (data.data.coupon.Targets[variantId] !== true) {
                        message_box.html(jQuery('<div />').addClass('notice').addClass('inline').addClass('notice-error').append(jQuery('<p />').html(supr_upsale_values.coupon_error)));
                        return;
                    }

                    var discount = data.data.coupon.Effect.ReductionPercent;

                    if (discount !== undefined) {
                        // Save to user meta data and show discount
                        save_coupon_to_user(coupon_input.val(), discount);
                    } else {
                        message_box.html(jQuery('<div />').addClass('notice').addClass('inline').addClass('notice-error').append(jQuery('<p />').html(supr_upsale_values.coupon_error)));
                    }
                } else {
                    message_box.html(jQuery('<div />').addClass('notice').addClass('inline').addClass('notice-error').append(jQuery('<p />').html(supr_upsale_values.coupon_error)));
                }
            },
            error: function (xhr, status, error) {
                content_box.removeClass('disabled');
                console.log('Error: I can\'t check coupon \'' + coupon_input.val() + '\'.');
            }
        });
    };

    /**
     * Save coupon in user meta for the next payment page
     *
     * @param coupon
     * @param discount
     */
    var save_coupon_to_user = function (coupon, discount) {
        jQuery.ajax({
            url: '/wp-admin/admin-ajax.php',
            data: {action: "supr_upsale_save_coupon", coupon: coupon},
            type: 'POST',
            beforeSend: function () {
                content_box.addClass('disabled');
            },
            success: function (data) {
                content_box.removeClass('disabled');
                if (data.success) {
                    price_box.find('.supr-upsale-modal-price-number').html(Math.round(price_box.data('original-price') * (1 - discount / 100)));
                    message_box.html(jQuery('<div />').addClass('notice').addClass('inline').addClass('notice-success').append(jQuery('<p />').html(supr_upsale_values.coupon_success)));
                    jQuery('#supr-upsale-coupon-flag, #supr-upsale-original-price').fadeIn();
                    jQuery('.supr-upsale-modal-price-container').addClass('animated tada');
                } else {
                    message_box.html(jQuery('<div />').addClass('notice').addClass('inline').addClass('notice-error').append(jQuery('<p />').html(supr_upsale_values.coupon_error)));
                }
            }
        });
    };

    // If we have already a coupon
    if (coupon !== undefined) {
        coupon_input.val(coupon);
        setTimeout(function () {
            apply_coupon();
        }, 500);
    }
}
