/** @var array supr_downgrade_values **/
/** @var string supr_downgrade_values.current_plan **/

(function ($) {
    // If there is not any current plan
    if (!supr_downgrade_values.current_plan) {
        return;
    }

    var plan_table = $('div.layer.plans');

    // If there is not any price table
    if (plan_table.length === 0) {
        return;
    }

    var plan_containers = plan_table.children('div.wu-plan');

    var apply_downgrade = true;

    plan_containers.each(function () {
        var plan_container = $(this);

        var plan_title = plan_container.find('h4.wp-ui-primary').html().toLowerCase();
        var submit_button = plan_container.find('button[type=submit]');

        if (plan_title === supr_downgrade_values.current_plan) {
            apply_downgrade = false;
        }

        // Same plan
        if (plan_title === supr_downgrade_values.current_plan) {
            submit_button.on('click', function (event) {
                event.stopImmediatePropagation();
                console.log('Why do yoy click here?');

                return false;
            });
        }

        // Upsale
        if (plan_title !== supr_downgrade_values.current_plan && apply_downgrade === false && plan_title === 'smart') {
            submit_button.on('click', function (event) {
                event.stopImmediatePropagation();
                supr_upsale_show_modal();

                return false;
            });
        }

        // Downsale
        if (plan_title !== supr_downgrade_values.current_plan && apply_downgrade === true) {
            submit_button
                .removeAttr('disabled')
                .html(supr_downgrade_values.button_contact_support)
                .on('click', function (event) {
                    event.stopImmediatePropagation();

                    var win = window.open('https://supr.help/downgrade/', '_blank');
                    win.focus();

                    return false;
                });
        }
    });
})(jQuery);