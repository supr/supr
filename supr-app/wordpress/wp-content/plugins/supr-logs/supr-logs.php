<?php
/**
 * Plugin Name: SUPR - Logs
 * Plugin URI: https://supr.com
 * Description: Show logs for Networkadmin
 * Text Domain: supr-logs
 * Domain Path: /languages
 * Version: 1.1
 * Author: SUPR Development
 * License: GPL
 */

// Add network admin menu page
add_action('network_admin_menu', 'supr_logs_create_network_menu');

function supr_logs_create_network_menu()
{
    add_menu_page(
        __('Logs', 'supr-design-store'),
        __('Logs', 'supr-design-store'),
        'manage_network',
        'supr_logs_output',
        'supr_logs_output'
    );
}

/**
 * Logs output
 */
function supr_logs_output()
{
    $errorPath = ini_get('error_log');

    if (isset($_GET['delete_logs']) && (int)$_GET['delete_logs'] === 1) {
        file_put_contents($errorPath, '');

        // Clear custom logs loo
        if (class_exists('\SuprThirdPluginsHacking\DebugLogManager')) {
            \SuprThirdPluginsHacking\DebugLogManager::clearLog();
        }

        wp_redirect(self_admin_url('admin.php?page=supr_logs_output'));
        exit;
    }

    // We need to use low memory and cannot use file()
    $descriptor = fopen($errorPath, 'r');
    $countLines = 0;
    $linePerPage = 500;

    /*$errorFile = file($errorPath);
    $countLines = count($errorFile);*/
    ?>
    <div class="wrap supr-logs-output">
        <h1 class="wp-heading-inline"><?= get_admin_page_title(); ?></h1>
        <div class="notice">
            <p>Path to log file: <?= $errorPath; ?> (<a href="<?= self_admin_url('admin.php?page=supr_logs_output&delete_logs=1'); ?>">delete logs</a>)</p>
        </div>
        <div style="overflow: auto;">
            <pre>
    <?php
    if ($descriptor) {
        // Count of lines
        while (($string = fgets($descriptor)) !== false) {
            $countLines++;
        }

        // Count pages
        $countOfPages = ceil($countLines / $linePerPage);
        $currentPage = (int)($_GET['page_num'] ?? $countOfPages);
        $lineFrom = $linePerPage * ($currentPage - 1);
        $lineTill = $linePerPage * ($currentPage);

        fclose($descriptor);

        // Output
        $descriptor = fopen($errorPath, 'r');
        $currentLine = 0;
        while (($string = fgets($descriptor)) !== false) {
            if ($currentLine > $lineFrom && $currentLine < $lineTill) {
                echo $string;
            }
            $currentLine++;
        }
        fclose($descriptor);
    } else {
        echo 'Cannot open log file: ' . $errorPath;
    }
    ?>
            </pre>
        </div>
        <?php
        // Pagination
        if (isset($countOfPages, $currentPage)) {
            echo '<p style="text-align: center">';
            $i = 1;
            while ($i <= $countOfPages) {
                if ($i > 1) {
                    echo ' | ';
                }
                echo $i === $currentPage ? "<span>{$i}</span>" : "<a href='?page=supr_logs_output&page_num={$i}'>{$i}</a>";
                $i++;
            }
            echo '</p>';
        }
        ?>
    </div>
    <?php

    // Print custom logs
    if (class_exists('\SuprThirdPluginsHacking\DebugLogManager')) {
        \SuprThirdPluginsHacking\DebugLogManager::printLogs();
    }
}

// Save important WP-Ultimo logs
add_action('wu_log_add', 'supr_logs_wu_log_add', 10, 2);

/**
 * Write also some WP-Ultimo logs to our logs
 *
 * @param string $handle
 * @param string $message
 */
function supr_logs_wu_log_add($handle, $message)
{
    if ($handle === 'billwerk-js-errors') {
        error_log('[WP-Ultimo] ' . $message);
    }
}
