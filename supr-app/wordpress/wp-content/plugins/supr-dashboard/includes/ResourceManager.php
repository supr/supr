<?php

namespace SuprDashboard;

/**
 * Class Resource
 *
 * @package SuprDashboard
 */
class ResourceManager
{
    /**
     * Register files for admin
     */
    public static function addAdminResources(): void
    {
        \wp_register_style('supr_dashbooard_admin_style', SUPR_DASHBOARD_URL . 'css/admin.css', [], SUPR_DASHBOARD_VERSION);
        \wp_register_script('supr_dashbooard_admin_js', SUPR_DASHBOARD_URL . 'js/admin.js', ['jquery'], SUPR_DASHBOARD_VERSION, true);
        \add_action('admin_enqueue_scripts', [__CLASS__, 'enqueueAdminFiles'], 999);
    }

    /**
     * Register files for dashboard
     */
    public static function addDashboardResources(): void
    {
        // Soon plugin for countdown
        \wp_enqueue_script('supr_dashboard_asset_soon_script', SUPR_DASHBOARD_URL . 'assets/soon.min.js', array('jquery'), SUPR_DASHBOARD_VERSION, true);
        \wp_enqueue_style('supr_dashboard_asset_soon_style', SUPR_DASHBOARD_URL . 'assets/soon.min.css', array(), SUPR_DASHBOARD_VERSION);

        // SUPR dashboard styles
        \wp_register_style('supr_dashboard_style', SUPR_DASHBOARD_URL . 'css/dashboard.css', [], SUPR_DASHBOARD_VERSION);
        \wp_register_script('supr_dashbooard_js', SUPR_DASHBOARD_URL . 'js/dashboard.js', ['jquery'], SUPR_DASHBOARD_VERSION, true);

        \add_action('admin_enqueue_scripts', [__CLASS__, 'enqueueDashboardFiles'], 999);
    }

    /**
     * Register files for wizard
     */
    public static function addWizardResources(): void
    {
        \wp_register_style('supr_dashboard_wizard_style', SUPR_DASHBOARD_URL . 'css/wizard.css', [], SUPR_DASHBOARD_VERSION);
        \wp_register_script('supr_dashboard_wizard_js', SUPR_DASHBOARD_URL . 'js/wizard.js', ['jquery'], '1.0.0', true);

        \add_action('admin_enqueue_scripts', [__CLASS__, 'enqueueWizardFiles'], 999);
    }

    /**
     * Add files for admin
     */
    public static function enqueueAdminFiles(): void
    {
        // CSS
        \wp_enqueue_style('supr_dashbooard_admin_style');
        // JS
        \wp_enqueue_script('supr_dashbooard_admin_js');
    }

    /**
     * Add files for dashboard
     */
    public static function enqueueDashboardFiles(): void
    {
        // CSS
        \wp_enqueue_style('supr_dashboard_asset_soon_style');
        \wp_enqueue_style('supr_dashboard_style');
        // JS
        \wp_enqueue_script('supr_dashboard_asset_soon_script');
        \wp_enqueue_script('supr_dashbooard_js');
    }

    /**
     * Add files for wizard
     */
    public static function enqueueWizardFiles(): void
    {
        // CSS
        \wp_enqueue_style('supr_dashboard_wizard_style');
        // JS
        \wp_enqueue_script('supr_dashboard_wizard_js');
    }

    /**
     * Add js variables for Dashboard script
     */
    public static function addDashboardJsVariables(): void
    {
        $values = [
            'status_share_url' => esc_url(home_url("/")),
            'status_share_title' => __('Share your store', 'supr-dashboard'),
            'status_share_button' => __('Copy to clipboard', 'supr-dashboard'),
            'status_share_copied' => __('Copied to clipboard', 'supr-dashboard'),
            'status_share_cancel' => __('Cancel', 'supr-dashboard')
        ];

        \wp_localize_script('supr_dashbooard_js', 'supr_dashboard_values', $values);
    }

    /**
     * Add js variables for Wizard script
     */
    public static function addWizardJsVariables(): void
    {
        $values = [
            'skip_modal_title' => __('Skip wizard', 'supr-dashboard'),
            'skip_modal_desc' => __('Do you really want to skip the wizard that helps you with the setup? It will not be displayed again.', 'supr-dashboard'),
            'skip_modal_accept' => __('Yes, skip wizard', 'supr-dashboard'),
            'skip_modal_cancel' => __('Cancel', 'supr-dashboard'),
            'publish_title' => __('Publish shop', 'supr-dashboard'),
            'publish_desc' => __('If you publish your shop now, anyone can visit it and order your products. Please make sure that you have done all the important steps and that your legal pages exist!', 'supr-dashboard'),
            'publish_accept' => __('Yes, publish my shop', 'supr-dashboard'),
            'publish_cancel' => __('Cancel', 'supr-dashboard'),
            'published_title' => __('Your shop has been published', 'supr-dashboard'),
        ];

        \wp_localize_script('supr_dashboard_wizard_js', 'supr_dashboard_wizard_values', $values);
    }
}
