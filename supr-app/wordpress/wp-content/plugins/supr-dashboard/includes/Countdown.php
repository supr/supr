<?php

namespace SuprDashboard;

/**
 * Class Countdown
 *
 * @package SuprDashboard
 */
class Countdown
{
    /**
     * @var Countdown
     */
    private static $instance;

    /**
     * @var array
     */
    private $options = [];

    /**
     * @var \DateTime
     */
    private $signupDate;

    /**
     * @var array|null
     */
    private $activeCode;

    /**
     * @return Countdown
     */
    public static function instance(): Countdown
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->load();
    }

    /**
     * Load all of options in this class
     */
    private function load(): void
    {
        $options = \get_blog_option(1, 'supr_dashboard_countdown_options', []);

        $this->options['code'] = $options['supr_dashboard_countdown_code'] ?? [];
        $this->options['discount'] = $options['supr_dashboard_countdown_discount'] ?? [];
        $this->options['days'] = $options['supr_dashboard_countdown_days'] ?? [];
        $this->options['content'] = $options['supr_dashboard_countdown_content'] ?? '';
        $this->options['title'] = $options['supr_dashboard_countdown_title'] ?? '';

        // Doing objects from blog options
        $this->options['codes'] = [];

        foreach ($this->options['code'] as $num => $code) {
            $this->options['codes'][$this->options['days'][$num]] = [
                'code' => $this->options['code'][$num],
                'period_days' => $this->options['days'][$num],
                'discount' => $this->options['discount'][$num]
            ];
        }

        // Sorting by time
        ksort($this->options['codes']);

        // Set signup date
        $currentBlogDetails = \get_blog_details(\get_current_blog_id());
        $this->signupDate = new \DateTime($currentBlogDetails->registered);

        foreach ($this->options['codes'] as $code) {
            $expireDay = (clone $this->signupDate)->add(new \DateInterval('P' . $code['period_days'] . 'D'));
            if ($expireDay > new \DateTime()) {
                $this->activeCode = $code;
                $this->activeCode['expire_day'] = $expireDay;
                $this->activeCode['content'] = $this->options['content'];
                break;
            }
        }
    }

    public function getActiveCode(): ?array
    {
        return $this->activeCode;
    }

    public function getSettings(): array
    {
        return $this->options;
    }

    public function saveSettingsFromPostRequest(): bool
    {
        // Remove all entities, that don't have a code
        foreach ($_POST['supr_dashboard_countdown_code'] as $num => $code) {
            // We delete empty values
            if (empty($code)) {
                unset(
                    $_POST['supr_dashboard_countdown_code'][$num],
                    $_POST['supr_dashboard_countdown_discount'][$num],
                    $_POST['supr_dashboard_countdown_days'][$num]
                );
            }
        }

        $options = [
            'supr_dashboard_countdown_code' => array_map('sanitize_text_field', $_POST['supr_dashboard_countdown_code']),
            'supr_dashboard_countdown_discount' => array_map('intval', $_POST['supr_dashboard_countdown_discount']),
            'supr_dashboard_countdown_days' => array_map('intval', $_POST['supr_dashboard_countdown_days']),
            'supr_dashboard_countdown_content' => wp_kses_post($_POST['supr_dashboard_countdown_content']),
            'supr_dashboard_countdown_title' => sanitize_text_field($_POST['supr_dashboard_countdown_title'])
        ];

        return \update_option('supr_dashboard_countdown_options', $options);
    }
}
