<?php

namespace SuprDashboard;

/**
 * Class WizardOptions
 *
 * @package SuprDashboard
 */
class WizardOptions
{
    /**
     * @var WizardOptions
     */
    public static $instance;

    private $optionsName = 'supr_dashboard_wizard_options';

    /**
     * Options with default values
     *
     * @var array
     */
    private $optionsNames = [
        'wizard_finished' => false,
        'wizard_store_owner' => false,
        'wizard_design' => false,
        'wizard_product' => false,
        'wizard_shipping' => false,
        'wizard_legal' => false,
        'wizard_plan' => false
    ];

    private $options;

    /**
     * @return WizardOptions
     */
    public static function instance(): WizardOptions
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->loadOptions();
    }

    /**
     * Load all of options in this class
     */
    private function loadOptions(): void
    {
        // Get from DB
        $options = \get_option($this->optionsName, []);
        // Merge with default
        $this->options = \array_merge($this->optionsNames, $options);
    }

    /**
     * @param string $optionsName
     * @return mixed
     * @throws \Exception
     */
    public function getOption($optionsName)
    {
        if (!isset($this->optionsNames[$optionsName])) {
            throw new \Exception("[SUPR DASHBOARD] The option '{$optionsName}' is not registered.");
        }

        return $this->options[$optionsName];
    }

    /**
     * @param string $optionsName
     * @param        $optionsValue
     * @throws \Exception
     */
    public function setOption($optionsName, $optionsValue): void
    {
        if (!isset($this->optionsNames[$optionsName])) {
            throw new \Exception("[SUPR DASHBOARD] The option '{$optionsName}' is not registered.");
        }

        $this->options[$optionsName] = $optionsValue;

        \update_option($this->optionsName, $this->options);
    }
}
