<?php

namespace SuprDashboard;

/**
 * Class Page
 *
 * @package SuprDashboard
 */
class Page
{
    /**
     * Show setting page
     */
    public static function settingsPage(): void
    {
        include SUPR_DASHBOARD_PATH . 'templates/settings-page.php';
    }
}
