<?php

namespace SuprDashboard;

/**
 * Class WizardStatus
 *
 * @package SuprDashboard
 */
class WizardStatus
{
    /**
     * @var WizardStatus
     */
    public static $instance;

    /**
     * @var bool
     */
    private $ownerStepStatus;

    /**
     * @var bool
     */
    private $designStepStatus;

    /**
     * @var bool
     */
    private $shippingStepStatus;

    /**
     * @var bool
     */
    private $productsStepStatus;

    /**
     * @var bool
     */
    private $paymentStepStatus;

    /**
     * @var bool
     */
    private $legalStepStatus;

    /**
     * @var bool
     */
    private $planStepStatus;

    /**
     * @var bool
     */
    private $publicStepStatus;

    /**
     * @return \SuprDashboard\WizardStatus
     * @throws \Exception
     */
    public static function getInstance(): WizardStatus
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * WizardStatus constructor.
     * @throws \Exception
     */
    private function __construct()
    {
        // Collect status of each step
        $this->ownerStepStatus = WizardOptions::instance()->getOption('wizard_store_owner');
        $this->designStepStatus = WizardOptions::instance()->getOption('wizard_design');
        $this->shippingStepStatus = WizardOptions::instance()->getOption('wizard_shipping');
        $this->productsStepStatus = $this->checkProducts();
        $this->paymentStepStatus = \get_option('supr_cop_onboarding_options') ? true : false;
        $this->legalStepStatus = $this->checkLegal();
        $this->planStepStatus = $this->checkPlan();
        $this->publicStepStatus = !\get_option('elementor_maintenance_mode_mode');
    }


    /**
     * Get owner status
     * @return string
     */
    public function getOwnerStepStatus(): string
    {
        return $this->ownerStepStatus ? 'done' : 'waiting';
    }

    /**
     * Get design status
     * @return string
     */
    public function getDesignStepStatus(): string
    {
        return $this->designStepStatus ? 'done' : 'waiting';
    }

    /**
     * Get shipping status
     * @return string
     */
    public function getShippingStepStatus(): string
    {
        return $this->shippingStepStatus ? 'done' : 'waiting';
    }

    /**
     * Get products status
     * @return string
     */
    public function getProductsStepStatus(): string
    {
        return $this->productsStepStatus ? 'done' : 'waiting';
    }

    /**
     * Get payment status
     * @return string
     */
    public function getPaymentStepStatus(): string
    {
        return $this->paymentStepStatus ? 'done' : 'waiting';
    }

    /**
     * Get legal status
     * @return string
     */
    public function getLegalStepStatus(): string
    {
        return $this->legalStepStatus ? 'done' : 'waiting';
    }

    /**
     * Get plan status
     * @return string
     */
    public function getPlanStepStatus(): string
    {
        return $this->planStepStatus ? 'done' : 'waiting';
    }

    /**
     * Get public status
     * @return string
     */
    public function getPublicStepStatus(): string
    {
        return $this->publicStepStatus ? 'done' : 'waiting';
    }

    /**
     * Check if any step is finished
     * @return bool
     */
    public function isFinished(): bool
    {
        $finished = $this->ownerStepStatus
            && $this->designStepStatus
            && $this->shippingStepStatus
            && $this->productsStepStatus
            && $this->paymentStepStatus
            && $this->legalStepStatus
            && $this->planStepStatus
            && $this->publicStepStatus;

        // If all of steps were finished, then we can finish the wizard
        if ($finished) {
            WizardOptions::instance()->setOption('wizard_finished', true);
        }

        return $finished;
    }

    private function checkPlan(): bool
    {
        // Check if plan selection is skipped
        if (WizardOptions::instance()->getOption('wizard_plan') === true) {
            return true;
        }

        if (in_array(Manager::getPlanName(), ['basic', 'smart', 'pro'])) {
            WizardOptions::instance()->setOption('wizard_plan', true);

            return true;
        }

        return false;
    }

    /**
     * Wizard check own products
     */
    private function checkProducts(): bool
    {
        // Check if product option already exist
        if (WizardOptions::instance()->getOption('wizard_product') === true) {
            return true;
        }

        // Check if none demo product exist
        if (class_exists('\SuprDemocontent\Manager')) {
            $products_demo_ids = \SuprDemocontent\Manager::instance()->getDemoProductIds();
            $products_query = new \WP_Query(
                [
                    'post_type' => 'product',
                    'post__not_in' => $products_demo_ids
                ]
            );

            if (!$products_query->have_posts()) {
                return false;
            }
        }

        WizardOptions::instance()->setOption('wizard_product', true);

        return true;
    }

    /**
     * Wizard check legal content
     */
    public function checkLegal(): bool
    {
        // Check if legal option already exist
        if (WizardOptions::instance()->getOption('wizard_legal') === true) {
            return true;
        }

        $restrictions = [
            [
                'posts' => [
                    \get_option('woocommerce_data_security_page_id'),
                    \get_option('woocommerce_terms_page_id'),
                    \get_option('woocommerce_revocation_page_id')
                ],
                'length' => 400
            ],
            [
                'posts' => [
                    \get_option('woocommerce_imprint_page_id')
                ],
                'length' => 25
            ]
        ];

        foreach ($restrictions as $restriction) {
            foreach (\get_posts(['post__in' => $restriction['posts'], 'post_type' => 'page']) as $post) {
                if (mb_strlen($post->post_content) < $restriction['length']) {
                    return false;
                }
            }
        }

        // Set option as done
        WizardOptions::instance()->setOption('wizard_legal', true);

        return true;
    }
}
