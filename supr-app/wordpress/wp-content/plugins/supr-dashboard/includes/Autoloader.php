<?php

namespace SuprDashboard;

/**
 * Class Autoloader
 *
 * @package SuprDashboard
 */
class Autoloader
{
    /**
     * Register a function as `__autoload()` implementation.
     */
    public static function run(): void
    {
        \spl_autoload_register([__CLASS__, 'autoload']);
    }

    /**
     * For a given class, check if it exist and load it.
     *
     * @param string $class Class name.
     */
    private static function autoload($class): void
    {
        $realClassName = \str_replace(__NAMESPACE__ . '\\', '', $class);

        $filePath = SUPR_DASHBOARD_PATH . 'includes/' . $realClassName . '.php';

        if (\file_exists($filePath) && \is_readable($filePath)) {
            require_once $filePath;
        }
    }
}
