<?php

namespace SuprDashboard;

/**
 * Class AdminBar
 *
 * @package SuprDashboard
 */
class Manager
{
    /**
     * @var Manager
     */
    private static $instance;

    /**
     * @return Manager
     */
    public static function getInstance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Init wizard
     */
    public function initWizard(): void
    {
        // Remove dashboard meta boxes
        $this->removeDashboardMetaBoxes();
        // Remove supr slider
        \add_filter('supr-admanager-show-slider', [$this, 'hideSuprAdSlider']);
        // Add wizard
        \add_action('admin_notices', [$this, 'showDashboardWizard']);
    }

    /**
     * Init Dashboard
     */
    public function initDashboard(): void
    {
        // If we have active code and we can do upsale
        if (Countdown::instance()->getActiveCode() !== null && !in_array(self::getPlanName(), ['smart', 'pro'])) {
            \add_meta_box('supr_dashboard_countdown', __('Countdown', 'supr-dashboard'), [$this, 'dashboardPanelCountdown'], 'dashboard', 'normal', 'high');
        }
        \add_meta_box('supr_dashboard_status', __('Status', 'supr-dashboard'), [$this, 'dashboardPanelStatus'], 'dashboard', 'side', 'high');
        \add_meta_box('supr_dashboard_help', __('Help', 'supr-dashboard'), [$this, 'dashboardPanelHelp'], 'dashboard', 'side', 'low');
        \add_meta_box('supr_dashboard_blog', __('Blog', 'supr-dashboard'), [$this, 'dashboardPanelBlog'], 'dashboard', 'side', 'low');
    }

    /**
     * Dashboard status panel
     */
    public function dashboardPanelCountdown(): void
    {
        require SUPR_DASHBOARD_PATH . 'templates/panel-countdown.php';
    }

    /**
     * Wizard get wp-ultimo plan
     *
     * @return \WU_Plan|null
     */
    public static function getPlan(): ?\WU_Plan
    {
        if (!\function_exists('\wu_get_account_plan')) {
            return null;
        }
        $site = \wu_get_site(\get_current_blog_id());
        $plan = $site->get_plan();
        if ($plan === false) {
            error_log('[Supr Dashboard] Plan for shop #' . \get_current_blog_id() . ' doesn\'t exist.');
            return null;
        }
        return $plan;
    }

    /**
     * Wizard get plan by name
     */
    public static function getPlanName(): string
    {
        $plan = self::getPlan();

        return $plan !== null ? strtolower($plan->title) : 'starter';
    }

    /**
     * Dashboard status panel
     */
    public function dashboardPanelStatus(): void
    {
        require SUPR_DASHBOARD_PATH . 'templates/panel-status.php';
    }

    /**
     * Dashboard help panel
     */
    public function dashboardPanelHelp(): void
    {
        require SUPR_DASHBOARD_PATH . 'templates/panel-help.php';
    }

    /**
     * Dashboard blog panel
     */
    public function dashboardPanelBlog(): void
    {
        require SUPR_DASHBOARD_PATH . 'templates/panel-blog.php';
    }

    /**
     * Hide slider
     *
     * @param $show
     * @return bool
     */
    public function hideSuprAdSlider($show): bool
    {
        return false;
    }

    /**
     * Remove all meta boxes
     */
    public function removeDashboardMetaBoxes(): void
    {
        \remove_meta_box('wp-ultimo-account', 'dashboard', 'normal');
        \remove_meta_box('wp_search_engine_notice_db_widget', 'dashboard', 'normal');
        \remove_meta_box('woocommerce_dashboard_recent_reviews', 'dashboard', 'normal');
        \remove_meta_box('woocommerce_dashboard_status', 'dashboard', 'normal');
        \remove_meta_box('wp-ultimo-quotas', 'dashboard', 'side');
    }

    public function showDashboardWizard(): void
    {
        $screen = \get_current_screen();

        if ($screen !== null && $screen->id === 'dashboard') {
            require SUPR_DASHBOARD_PATH . 'templates/wizard.php';
        }
    }

    public function showWizard(): bool
    {
        // Wizard was already finished
        if (WizardOptions::instance()->getOption('wizard_finished')) {
            return false;
        }

        // If the blog was registered before 20.03.2020
        $blogDetails = \get_blog_details();
        if (new \DateTime($blogDetails->registered) < new \DateTime('2020-03-20 00:00:00')) {
            return false;
        }

        return true;
    }
}
