<?php

namespace SuprDashboard;

/**
 * Class TextDomain
 *
 * @package SuprDashboard
 */
class TextDomain
{
    public static $domainName = 'supr-dashboard';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_DASHBOARD_DIR_NAME . '/languages/');
    }
}
