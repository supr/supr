<?php

namespace SuprDashboard;

/**
 * Class Menu
 *
 * @package SuprDashboard
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add menu page
        add_action('network_admin_menu', [__CLASS__, 'createMenu']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createMenu(): void
    {
        add_menu_page(
            __('Dashboard', 'supr-dashboard'),
            __('Dashboard', 'supr-dashboard'),
            'manage_network',
            'supr_dashboard_settings',
            [Page::class, 'settingsPage'],
            'dashicons-welcome-learn-more'
        );
    }
}
