<?php

namespace SuprDashboard;

/**
 * Class AjaxRestEndpoint
 *
 * @package SuprDashboard
 */
class AjaxRestEndpoint
{
    /**
     * Define endpoints for ajax queries
     */
    public static function addEndPoints(): void
    {
        \add_action('wp_ajax_supr_dashboard_design_done', [self::class, 'wizardSetDesignDone']);
        \add_action('wp_ajax_supr_dashboard_shipping_done', [self::class, 'wizardSetShippingDone']);
        \add_action('wp_ajax_supr_dashboard_shop_owner', [self::class, 'wizardSaveShopOwnerForm']);
        \add_action('wp_ajax_supr_dashboard_shop_public', [self::class, 'wizardSetShopPublic']);
        \add_action('wp_ajax_supr_dashboard_skip', [self::class, 'wizardSkip']);
        \add_action('wp_ajax_supr_dashboard_plan_skip', [self::class, 'wizardPlanSkip']);
        \add_action('wp_ajax_supr_dashboard_finish', [self::class, 'wizardFinish']);
    }

    /**
     * Skip wizard
     *
     * @throws \Exception
     */
    public static function wizardSkip(): void
    {
        // Save option as done
        WizardOptions::instance()->setOption('wizard_finished', true);

        \wp_send_json(['message' => 'Wizard skiped']);
    }

    /**
     * Skip plan selection
     *
     * @throws \Exception
     */
    public static function wizardPlanSkip(): void
    {
        // Save option as done
        WizardOptions::instance()->setOption('wizard_plan', true);

        \wp_send_json(['message' => 'Wizard plan skiped']);
    }

    /**
     * Finish wizard after every step is done
     *
     * @throws \Exception
     */
    public static function wizardFinish(): void
    {
        // Save option as done
        WizardOptions::instance()->setOption('wizard_finished', true);

        \wp_send_json(['message' => 'Wizard finished']);
    }

    /**
     * Set design step as done
     *
     * @throws \Exception
     */
    public static function wizardSetDesignDone(): void
    {
        // Save option as done
        WizardOptions::instance()->setOption('wizard_design', true);

        \wp_send_json(['message' => 'Design set to done']);
    }

    /**
     * Set shipping step as done
     *
     * @throws \Exception
     */
    public static function wizardSetShippingDone(): void
    {
        // Save option as done
        WizardOptions::instance()->setOption('wizard_shipping', true);

        \wp_send_json(['message' => 'Shipping set to done']);
    }

    /**
     * Publish shop
     *
     * @throws \Exception
     */
    public static function wizardSetShopPublic(): void
    {
        // Maintenance mode off
        \update_option('elementor_maintenance_mode_mode', false);
        // Allow search engines
        \update_option('blog_public', true);

        \wp_send_json(['message' => 'Shop set to public']);
    }

    /**
     * Save shop owner info
     *
     * @throws \Exception
     */
    public static function wizardSaveShopOwnerForm(): void
    {
        // Get form data
        $formDataArray = $_POST['formData'];

        // Required fields
        $requiredFields = [
            'woocommerce_store_owner_store_category',
            'woocommerce_store_owner_salutation',
            'woocommerce_store_owner_first_name',
            'woocommerce_store_owner_last_name',
            'woocommerce_store_owner_phone',
            'woocommerce_store_address',
            'woocommerce_store_city',
            'woocommerce_store_postcode'
        ];

        $defaultRegex = '/^[a-z \.,&_\-0-9äöüÄÖÜß]{0,}$/i';
        $salutationRegex = '/^[mf]{1}$/';
        $defaultNumberRegex = '/^[0-9]+$/';
        $phoneNumberRegex = '/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\.\/0-9]*$/';
        // See: https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s21.html
        $vatRegex = '/^((AT)?U[0-9]{8}|(BE)?0[0-9]{9}|(BG)?[0-9]{9,10}|(CY)?[0-9]{8}L|(CZ)?[0-9]{8,10}|(DE)?[0-9]{9}|(DK)?[0-9]{8}|(EE)?[0-9]{9}|(EL|GR)?[0-9]{9}|(ES)?[0-9A-Z][0-9]{7}[0-9A-Z]|(FI)?[0-9]{8}|(FR)?[0-9A-Z]{2}[0-9]{9}|(GB)?([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3})|(HU)?[0-9]{8}|(IE)?[0-9]S[0-9]{5}L|(IT)?[0-9]{11}|(LT)?([0-9]{9}|[0-9]{12})|(LU)?[0-9]{8}|(LV)?[0-9]{11}|(MT)?[0-9]{8}|(NL)?[0-9]{9}B[0-9]{2}|(PL)?[0-9]{10}|(PT)?[0-9]{9}|(RO)?[0-9]{2,10}|(SE)?[0-9]{12}|(SI)?[0-9]{8}|(SK)?[0-9]{10})$/';
        $urlRegex = '/^(https?):\/\/[^\s\/$.?#].[^\s]*$/iS';
        $instagramNameRegex = '/^\@[a-zA-Z0-9._]+$/';

        // Fields with numbers
        $validationRules = [
            'woocommerce_store_owner_salutation' => $salutationRegex,
            'woocommerce_store_owner_store_category' => $defaultNumberRegex,
            'woocommerce_store_owner_phone' => $phoneNumberRegex,
            'woocommerce_store_postcode' => $defaultNumberRegex,
            'woocommerce_store_owner_company' => $defaultRegex,
            'woocommerce_store_owner_first_name' => $defaultRegex,
            'woocommerce_store_owner_last_name' => $defaultRegex,
            'woocommerce_store_address' => $defaultRegex,
            'woocommerce_store_city' => $defaultRegex,
            'woocommerce_store_owner_vat' => $vatRegex,
            'woocommerce_store_owner_marketing_facebook' => $urlRegex,
            'woocommerce_store_owner_marketing_instagram' => $instagramNameRegex
        ];

        // Error messages
        $errorMsg = [];

        // Check fields
        foreach ($formDataArray as $formField) {
            // If field is required but it is empty
            if (empty(trim($formField['value'])) && in_array($formField['name'], $requiredFields, true)) {
                $errorMsg[$formField['name']] = '!required field is empty / field: ' . $formField['name'] . ' / value: ' . $formField['value'];

                continue;
            }

            // If field is not it the list
            if (!isset($validationRules[$formField['name']])) {
                $errorMsg[$formField['name']] = '!field is not allowed / field: ' . $formField['name'] . ' / value: ' . $formField['value'];

                continue;
            }

            // If field is not valid
            if (!empty($formField['value']) && !preg_match($validationRules[$formField['name']], $formField['value'])) {
                $errorMsg[$formField['name']] = '!field not valid / field: ' . $formField['name'] . ' / value: ' . $formField['value'];
            }
        }

        // Save fields without errors
        foreach ($formDataArray as $formField) {
            if (!isset($errorMsg[$formField['name']])) {
                \update_option($formField['name'], $formField['value']);
            }
        }

        // Save step done or show errors
        if (empty($errorMsg)) {
            WizardOptions::instance()->setOption('wizard_store_owner', true);
            \wp_send_json(['message' => 'Shop owner set to done']);
        } else {
            \wp_send_json_error($errorMsg);
        }
    }
}
