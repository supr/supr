<?php

namespace SuprDashboard;

/**
 * Class Plugin
 *
 * @package SuprDashboard
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function getInstance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Admin init
     */
    public function adminInit(): void
    {
        global $pagenow;

        // Don't load dashboard for network and for main blog
        if (\is_network_admin() || (\is_multisite() && \is_main_site())) {
            return;
        }

        // Add js and css for network admin
        if (isset($_GET['page']) && $_GET['page'] === 'supr_dashboard_settings') {
            ResourceManager::addAdminResources();
        }

        if (Manager::getInstance()->showWizard()) {
            // Hide admin tour if wizard was not finished
            \add_filter('supr_help_quide_show_admin_tour', '__return_false');

            if ($pagenow === 'index.php') {
                ResourceManager::addWizardResources();
                ResourceManager::addWizardJsVariables();
                Manager::getInstance()->initWizard();
            }
            AjaxRestEndpoint::addEndPoints();
        } else {
            if ($pagenow === 'index.php') {
                ResourceManager::addDashboardResources();
                ResourceManager::addDashboardJsVariables();
                Manager::getInstance()->initDashboard();
            }
        }
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // Create Menu
        Menu::addMenu();

        // Load translations
        \add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        \add_action('admin_init', [$this, 'adminInit'], 9);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_DASHBOARD_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::getInstance();
