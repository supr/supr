<?php

namespace SuprDashboard;

/**
 * Class Feed
 *
 * @package SuprDashboard
 */
class Feed
{
    /**
     * Get rss posts from source
     *
     * @param string $url
     * @param int    $limit
     * @return array|string
     */
    public static function getFeed($url = 'https://de.supr.com/feed', $limit = 3)
    {
        $rss = fetch_feed($url);
        $feed = [];

        if (is_wp_error($rss)) {
            return __('<strong>RSS Error</strong>: ', 'supr-dashboard') . $rss->get_error_message();
        }

        if (!$rss->get_item_quantity()) {
            $rss->__destruct();
            unset($rss);

            return __('Sorry! Unfortunately we did not find any news.', 'supr-dashboard');
        }

        foreach ($rss->get_items(0, $limit) as $item) {
            $feed[] = [
                'link' => esc_url(strip_tags($item->get_link())),
                'title' => esc_html($item->get_title()),
                'content' => wp_html_excerpt($item->get_content(), 150) . '…',
            ];
        }

        $rss->__destruct();
        unset($rss);

        return $feed;
    }
}
