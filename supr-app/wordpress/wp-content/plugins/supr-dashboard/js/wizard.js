(function ($) {
    /*
     * Set wizard status
     */
    getWizardStatusBarStatus();

    jQuery('#supr-dashboard-wizard-skip-link').on('click', function () {
        Swal.fire({
            icon: 'warning',
            title: supr_dashboard_wizard_values.skip_modal_title + '?',
            text: supr_dashboard_wizard_values.skip_modal_desc,
            confirmButtonText: supr_dashboard_wizard_values.skip_modal_accept,
            showCancelButton: true,
            cancelButtonText: supr_dashboard_wizard_values.skip_modal_cancel
        }).then(function (result) {
            if (result.value) {
                // Set shop public
                wizardSwalLoaderOpen();
                jQuery.post('/wp-admin/admin-ajax.php', {action: 'supr_dashboard_skip'})
                    .done(function () {
                        window.location.reload();
                    })
                    .fail(function () {
                        console.log('Error by sending request to WP-AJAX: supr_dashboard_skip.');
                    });
            }
        });
    });

    /*
     * Button click to check design as done
     */
    jQuery('#dashboard-cta-button-design').on('click', function () {
        wizardSwalLoaderOpen();
        jQuery.post('/wp-admin/admin-ajax.php', {action: 'supr_dashboard_design_done'})
            .done(function () {
                window.location.replace("/wp-admin/admin.php?page=supr_design_store_designs");
            })
            .fail(function () {
                console.log('Error by sending request to WP-AJAX: supr_dashboard_design_done.');
            });
    });

    /*
     * Button click to check design as done
     */
    jQuery('#dashboard-cta-button-finished').on('click', function () {
        wizardSwalLoaderOpen();
        jQuery.post('/wp-admin/admin-ajax.php', {action: 'supr_dashboard_finish'})
            .done(function () {
                window.location.replace("/wp-admin");
            })
            .fail(function () {
                console.log('Error by sending request to WP-AJAX: supr_dashboard_design_done.');
            });
    });

    /*
     * Button click to skip plan booking
     */
    jQuery('#dashboard-cta-button-plan-skip').on('click', function () {
        wizardSwalLoaderOpen();
        jQuery.post('/wp-admin/admin-ajax.php', {action: 'supr_dashboard_plan_skip'})
            .done(function () {
                window.location.replace("/wp-admin");
            })
            .fail(function () {
                console.log('Error by sending request to WP-AJAX: supr_dashboard_skip_plan.');
            });
    });

    /*
     * Button click to check shipping as done
     */
    jQuery('#dashboard-cta-button-shipping').on('click', function () {
        wizardSwalLoaderOpen();
        jQuery.post('/wp-admin/admin-ajax.php', {action: 'supr_dashboard_shipping_done'})
            .done(function () {
                window.location.replace("/wp-admin/admin.php?page=wc-settings&tab=shipping");
            })
            .fail(function () {
                console.log('Error by sending request to WP-AJAX: supr_dashboard_shipping_done.');
            });
    });

    /*
     * Button to publish shop
     */
    jQuery('#dashboard-cta-button-public').on('click', function () {
        Swal.fire({
            icon: 'warning',
            title: supr_dashboard_wizard_values.publish_title,
            text: supr_dashboard_wizard_values.publish_desc,
            confirmButtonText: supr_dashboard_wizard_values.publish_accept,
            showCancelButton: true,
            cancelButtonText: supr_dashboard_wizard_values.publish_cancel
        }).then(function (result) {
            if (result.value) {
                // Set shop public
                wizardSwalLoaderOpen();
                jQuery.post('/wp-admin/admin-ajax.php', {action: 'supr_dashboard_shop_public'})
                    .done(function () {
                        wizardSwalLoaderClose();
                        Swal.fire({
                            icon: 'success',
                            title: supr_dashboard_wizard_values.published_title,
                            showConfirmButton: false
                        });
                        window.location.reload();
                    })
                    .fail(function () {
                        console.log('Error by sending request to WP-AJAX: supr_dashboard_design_done.');
                    });
            }
        });
    });

    /*
     * Button click to send shop owner form data
     */
    jQuery('#dashboard-cta-button-owner').on('click', function () {
        wizardSwalLoaderOpen();
        let form = jQuery('#supr-dashboard-shop-owner-form');
        form.removeClass('waiting-form');
        jQuery.post('/wp-admin/admin-ajax.php', {action: 'supr_dashboard_shop_owner', formData: form.serializeArray()})
            .done(function (data) {
                wizardSwalLoaderClose();
                if (data.success === false) {
                    jQuery('#supr-dashboard-shop-owner-form-error').show();
                    // Remove error class from all fields
                    jQuery('#supr-dashboard-shop-owner-form input, #supr-dashboard-shop-owner-form select').parent().removeClass('form-field-error');
                    // Add error class to not valid fields
                    jQuery.each(data.data, function (key, value) {
                        jQuery('#' + key).parent().addClass('form-field-error');
                    });
                } else {
                    jQuery('#supr-dashboard-shop-owner-form-error').hide();
                    jQuery('form#supr-dashboard-shop-owner-form').slideUp();
                    jQuery('#supr-dashboard-wizard-shop-owner')
                        .removeClass('supr-dashboard-wizard-waiting')
                        .addClass('supr-dashboard-wizard-done');
                    setTimeout(function () {
                        getWizardStatusBarStatus();
                    }, 500);
                }
            })
            .fail(function (data) {
                wizardSwalLoaderClose();
                console.log('Error by sending request to WP-AJAX: supr_dashboard_shipping_done.' + data);
            });
    });

    /*
     * Expand form on focus input
     */
    jQuery('#supr-dashboard-shop-owner-form').on('click', function () {
        jQuery(this).removeClass('waiting-form');
    });

})(jQuery);

/*
 * Set wizard status based von classes of postboxes
 */
function getWizardStatusBarStatus()
{
    var done = jQuery('.supr-dashboard-wizard-done:not(#supr-dashboard-wizard-finished)').length;
    var total = jQuery('.supr-dashboard-postbox:not(#supr-dashboard-wizard-finished)').length;
    jQuery('#supr-dashboard-wizard-current').text(done);
    jQuery('#supr-dashboard-wizard-total').text(total);
    jQuery('#supr-dashboard-wizard-statusbox-bar-inner').width(done / total * 100 + '%');
}

/*
 * Open sweet alert with supr loading animation
 */
function wizardSwalLoaderOpen()
{
    Swal.fire({
        showConfirmButton: false,
        imageUrl: '/wp-content/plugins/supr-dashboard/img/supr-loader.gif',
        width: '200px'
    });
}

/*
 * Close sweet alert
 */
function wizardSwalLoaderClose()
{
    Swal.close();
}