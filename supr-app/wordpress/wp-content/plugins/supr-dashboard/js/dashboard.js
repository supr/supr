(function ($) {

    // Copy share link
    jQuery('#share-store').on('click', function () {
        var shared_clicked = false;
        Swal.fire({
            icon: 'info',
            title: supr_dashboard_values.status_share_title,
            html: '<input id="store-share-input" class="swal2-input text-center" value="' + supr_dashboard_values.status_share_url + '">',
            customClass: 'animated fadeIn fast',
            allowOutsideClick: false,
            confirmButtonText: supr_dashboard_values.status_share_button,
            showCancelButton: true,
            cancelButtonText: supr_dashboard_values.status_share_cancel,
            focusConfirm: false,
            showClass: {
                popup: '',
                backdrop: '',
                icon: '',
            },
            hideClass: {
                popup: '',
                backdrop: '',
                icon: '',
            },
            preConfirm: function () {
                var copyText = document.getElementById("store-share-input");
                copyText.select();
                document.execCommand("copy");
                shared_clicked = true;
            },
            onAfterClose: function () {
                if (shared_clicked) {
                    Swal.fire({
                        icon: 'success',
                        title: supr_dashboard_values.status_share_copied,
                        showConfirmButton: false,
                        timer: 2000
                    })
                }
            }
        })
    });
})(jQuery);