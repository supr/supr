(function ($) {
    jQuery("#supr-dashboard-help-setting-clone").on("click", function () {
        console.log();
        jQuery("#supr-dashboard-help-values-clone li:first-child").clone().appendTo("#supr-dashboard-help-sortable");
    });

    jQuery(document).on("click", "span.dashicons-post-trash", function () {
        jQuery(this).parent().parent().parent().remove();
    });



    jQuery("#supr-dashboard-help-sortable").sortable();
    jQuery("#supr-dashboard-help-sortable").disableSelection();
})(jQuery);