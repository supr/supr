#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: SUPR - Dashboard\n"
"POT-Creation-Date: 2020-03-04 16:18+0100\n"
"PO-Revision-Date: 2018-09-15 11:19+0200\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: supr-dashboard.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;"
"_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: includes/Feed.php:25
msgid "<strong>RSS Error</strong>: "
msgstr ""

#: includes/Feed.php:32
msgid "Sorry! Unfortunately we did not find any news."
msgstr ""

#: includes/Manager.php:49 templates/settings-page.php:10
msgid "Countdown"
msgstr ""

#: includes/Manager.php:51
msgid "Status"
msgstr ""

#: includes/Manager.php:52
msgid "Help"
msgstr ""

#: includes/Manager.php:53
msgid "Blog"
msgstr ""

#: includes/Menu.php:27 includes/Menu.php:28
msgid "Dashboard"
msgstr ""

#: includes/ResourceManager.php:91
msgid "Share your store"
msgstr ""

#: includes/ResourceManager.php:92
msgid "Copy to clipboard"
msgstr ""

#: includes/ResourceManager.php:93
msgid "Copied to clipboard"
msgstr ""

#: includes/ResourceManager.php:94 includes/ResourceManager.php:109
#: includes/ResourceManager.php:113
msgid "Cancel"
msgstr ""

#: includes/ResourceManager.php:106
msgid "Skip wizard"
msgstr ""

#: includes/ResourceManager.php:107
msgid ""
"Do you really want to skip the wizard that helps you with the setup? It "
"will not be displayed again."
msgstr ""

#: includes/ResourceManager.php:108
msgid "Yes, skip wizard"
msgstr ""

#: includes/ResourceManager.php:110
msgid "Publish shop"
msgstr ""

#: includes/ResourceManager.php:111
msgid ""
"If you publish your shop now, anyone can visit it and order your products. "
"Please make sure that you have done all the important steps and that your "
"legal pages exist!"
msgstr ""

#: includes/ResourceManager.php:112
msgid "Yes, publish my shop"
msgstr ""

#: includes/ResourceManager.php:114
msgid "Your shop has been published"
msgstr ""

#: templates/panel-blog.php:20
msgid ""
"In our blog you will find all news about your shop and current e-commerce "
"posts."
msgstr ""

#: templates/panel-blog.php:21
msgid "Visit our blog"
msgstr ""

#: templates/panel-countdown.php:18
msgid "Day"
msgstr ""

#: templates/panel-countdown.php:18
msgid "Days"
msgstr ""

#: templates/panel-countdown.php:19
msgid "Hour"
msgstr ""

#: templates/panel-countdown.php:19
msgid "Hours"
msgstr ""

#: templates/panel-countdown.php:20
msgid "Minute"
msgstr ""

#: templates/panel-countdown.php:20
msgid "Minutes"
msgstr ""

#: templates/panel-countdown.php:21
msgid "Second"
msgstr ""

#: templates/panel-countdown.php:21
msgid "Seconds"
msgstr ""

#: templates/panel-countdown.php:25
msgid "Book smart plan now"
msgstr ""

#: templates/panel-help.php:13
msgid ""
"Need some help setting up your store? You will find the right answer to "
"all your questions in our help section."
msgstr ""

#: templates/panel-help.php:25
msgid "Search"
msgstr ""

#: templates/panel-status.php:10
msgid "Check availability of your store and share it with others"
msgstr ""

#: templates/panel-status.php:19
msgid "Your store is <strong>not available</strong> for customers"
msgstr ""

#: templates/panel-status.php:20 templates/panel-status.php:28
#: templates/panel-status.php:39 templates/panel-status.php:47
msgid "Change"
msgstr ""

#: templates/panel-status.php:27
msgid "Your store is <strong>available</strong> for customers"
msgstr ""

#: templates/panel-status.php:38
msgid "Your store is <strong>available</strong> for search engines"
msgstr ""

#: templates/panel-status.php:46
msgid "Your store is <strong>not available</strong> for search engines"
msgstr ""

#: templates/panel-status.php:55
msgid "Visit store"
msgstr ""

#: templates/settings-page.php:9
msgid "Help panel"
msgstr ""

#: templates/settings-page.php:26
msgid "Your settings have been saved"
msgstr ""

#: templates/settings-tabs/dashboard-countdown.php:28
msgid "Title"
msgstr ""

#: templates/settings-tabs/dashboard-countdown.php:42
msgid "Content"
msgstr ""

#: templates/settings-tabs/dashboard-countdown.php:54
msgid ""
"You can use short codes [dashboard-countdown-coupon] and [dashboard-"
"countdown-discount]."
msgstr ""

#: templates/settings-tabs/dashboard-countdown.php:66
msgid "Coupon code"
msgstr ""

#: templates/settings-tabs/dashboard-countdown.php:80
msgid "Discount (%)"
msgstr ""

#: templates/settings-tabs/dashboard-countdown.php:94
msgid "Countdown days"
msgstr ""

#: templates/settings-tabs/dashboard-countdown.php:115
msgid ""
"* You cann add or delete coupons. We will show one coupon with the minimum "
"remaining time. Please, also check if all of them are valid."
msgstr ""

#: templates/wizard.php:15
msgid "Hi! This is your new SUPR shop"
msgstr ""

#: templates/wizard.php:17
msgid ""
"With our setup wizard we help you to set up your shop in no time at all. "
"The checklist guides you step by step through the most important steps - "
"start now! "
msgstr ""

#: templates/wizard.php:23
msgid "Wizard progress"
msgstr ""

#: templates/wizard.php:23
msgid "of"
msgstr ""

#: templates/wizard.php:24
msgid "tasks completed"
msgstr ""

#: templates/wizard.php:40
msgid "Give details about your shop"
msgstr ""

#: templates/wizard.php:41
msgid ""
"<strong>Let us start with the setup of your shop.</strong> First of all, "
"enter your owner and contact details so that we can generate your imprint "
"and your customers can see you as the owner. You can change these data at "
"any time."
msgstr ""

#: templates/wizard.php:43
msgid ""
"<strong>Settings not saved!</strong><br>Please check the marked fields and "
"save the settings again."
msgstr ""

#: templates/wizard.php:46
msgid "Edit shop data"
msgstr ""

#: templates/wizard.php:50
msgid "Contact"
msgstr ""

#: templates/wizard.php:52
msgid "Salutation"
msgstr ""

#: templates/wizard.php:54
msgid "Mr."
msgstr ""

#: templates/wizard.php:55
msgid "Mrs."
msgstr ""

#: templates/wizard.php:59
msgid "First name"
msgstr ""

#: templates/wizard.php:64
msgid "Last name"
msgstr ""

#: templates/wizard.php:69
msgid "Phone"
msgstr ""

#: templates/wizard.php:72
msgid "Address"
msgstr ""

#: templates/wizard.php:74
msgid "Street, No."
msgstr ""

#: templates/wizard.php:78
msgid "City"
msgstr ""

#: templates/wizard.php:82
msgid "Postcode"
msgstr ""

#: templates/wizard.php:87
msgid "Your business"
msgstr ""

#: templates/wizard.php:89
msgid "Name of your business"
msgstr ""

#: templates/wizard.php:94
msgid "Shop category"
msgstr ""

#: templates/wizard.php:97
msgid "Pharmacies"
msgstr ""

#: templates/wizard.php:98
msgid "Automotive Industry"
msgstr ""

#: templates/wizard.php:99
msgid "Books & Magazines"
msgstr ""

#: templates/wizard.php:100
msgid "Office & catering supplies"
msgstr ""

#: templates/wizard.php:101
msgid "Erotic e-commerce"
msgstr ""

#: templates/wizard.php:102
msgid "Leisure & Family"
msgstr ""

#: templates/wizard.php:103
msgid "Health Care"
msgstr ""

#: templates/wizard.php:104
msgid "Gambling"
msgstr ""

#: templates/wizard.php:105
msgid "Household & Garden products"
msgstr ""

#: templates/wizard.php:106
msgid "Card sales & Couponing"
msgstr ""

#: templates/wizard.php:107
msgid "Furniture & household goods"
msgstr ""

#: templates/wizard.php:108
msgid "Fashion Women"
msgstr ""

#: templates/wizard.php:109
msgid "Fashion children/baby"
msgstr ""

#: templates/wizard.php:110
msgid "Fashion Men"
msgstr ""

#: templates/wizard.php:111
msgid "Fashion men/women"
msgstr ""

#: templates/wizard.php:112
msgid "Music downloads"
msgstr ""

#: templates/wizard.php:113
msgid "Food"
msgstr ""

#: templates/wizard.php:114
msgid "Photography"
msgstr ""

#: templates/wizard.php:115
msgid "Jewellery"
msgstr ""

#: templates/wizard.php:116
msgid "Software-Downloads"
msgstr ""

#: templates/wizard.php:117
msgid "Other services"
msgstr ""

#: templates/wizard.php:118
msgid "Sport & Sportswear"
msgstr ""

#: templates/wizard.php:119
msgid "Telecommunications"
msgstr ""

#: templates/wizard.php:120
msgid "Consumer electronics"
msgstr ""

#: templates/wizard.php:121
msgid "Other goods"
msgstr ""

#: templates/wizard.php:133
msgid "Vat."
msgstr ""

#: templates/wizard.php:136
msgid "Social media"
msgstr ""

#: templates/wizard.php:138
msgid "Facebook page"
msgstr ""

#: templates/wizard.php:143
msgid "Instagram account"
msgstr ""

#: templates/wizard.php:149
msgid "Save shop data"
msgstr ""

#: templates/wizard.php:167
msgid "Design your shop"
msgstr ""

#: templates/wizard.php:169
msgid ""
"<strong>A good shop design is important for happy customers!</strong> You "
"can choose from a variety of free designs, all optimized for mobile "
"devices like smartphones and tablets."
msgstr ""

#: templates/wizard.php:171
msgid "Select a design"
msgstr ""

#: templates/wizard.php:194
msgid "Create your first product"
msgstr ""

#: templates/wizard.php:195
msgid ""
"<strong>Create or import products!</strong> Simply add descriptions, "
"prices, pictures or product variations and fill your shop with life. In "
"our video we tell you how to do this the fastest way."
msgstr ""

#: templates/wizard.php:196
msgid "Create a product"
msgstr ""

#: templates/wizard.php:197
msgid "Create a category"
msgstr ""

#: templates/wizard.php:220
msgid "Set up payment methods"
msgstr ""

#: templates/wizard.php:221
msgid ""
"<strong>Activate the most popular payment methods now! </strong> To enable "
"your customers to pay for their order in your shop in their preferred way, "
"you can now use the SUPR payment methods."
msgstr ""

#: templates/wizard.php:222
msgid "Activate payment methods"
msgstr ""

#: templates/wizard.php:245
msgid "Check shipment"
msgstr ""

#: templates/wizard.php:246
msgid ""
"<strong> At the latest before you set your shop online the shipping "
"options must be correct.</strong> In this section you can set the "
"countries you want to ship to and the costs and methods."
msgstr ""

#: templates/wizard.php:247
msgid "Set up shipping methods & costs"
msgstr ""

#: templates/wizard.php:270
msgid "Legal pages"
msgstr ""

#: templates/wizard.php:271
msgid ""
"<strong>Important! Without these contents you should not go online with "
"your shop.</strong> The legal pages are one of the most important contents "
"of your shop. Please add this content and if necessary get professional "
"help to protect yourself from reprimands."
msgstr ""

#: templates/wizard.php:272
msgid "Terms and conditions"
msgstr ""

#: templates/wizard.php:273
msgid "Imprint"
msgstr ""

#: templates/wizard.php:274
msgid "Privacy policy"
msgstr ""

#: templates/wizard.php:296
msgid "Publish your shop"
msgstr ""

#: templates/wizard.php:300
msgid "Publish the shop now"
msgstr ""

#: templates/wizard.php:309
msgid "I want to skip the setup wizard"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "SUPR - Dashboard"
msgstr ""

#. Plugin URI of the plugin/theme
#. Author URI of the plugin/theme
msgid "https://supr.com"
msgstr ""

#. Description of the plugin/theme
msgid "Nothing"
msgstr ""

#. Author of the plugin/theme
msgid "SUPR - Piwi"
msgstr ""

#:
msgid "Revocation"
msgstr ""