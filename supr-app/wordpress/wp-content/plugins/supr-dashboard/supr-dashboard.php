<?php
/**
 * Plugin Name: SUPR - Dashboard
 * Plugin URI: https://supr.com
 * Description: Nothing
 * Text Domain: supr-dashboard
 * Domain Path: /languages
 * Version: 1.0.0
 * Author: SUPR - Piwi
 * Author URI: https://supr.com
 * License: GPL12
 */

define('SUPR_DASHBOARD_PATH', plugin_dir_path(__FILE__));
define('SUPR_DASHBOARD_URL', plugins_url('/', __FILE__));
define('SUPR_DASHBOARD_FILE', plugin_basename(__FILE__));
define('SUPR_DASHBOARD_DIR_NAME', basename(__DIR__));

define('SUPR_DASHBOARD_VERSION', '1.1.0');

// Load main Class
require(SUPR_DASHBOARD_PATH . 'includes/Plugin.php');
