<?php

// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$tabs = [
    'dashboard-help' => __('Help panel', 'supr-dashboard'),
    'dashboard-countdown' => __('Countdown', 'supr-dashboard')
];

$currentTab = (isset($_GET['tab'], $tabs[$_GET['tab']])) ? $_GET['tab'] : 'dashboard-help';
?>

<div class="wrap supr-dashboard">
    <h1 class="wp-heading-inline">
        <?= get_admin_page_title(); ?>
    </h1>

    <?php
    // Show success message after redirect
    if (isset($_GET['status']) && $_GET['status'] === 'success') : ?>
        <div class="notice notice-success inline">
            <p>
                <?php _e('Your settings have been saved', 'supr-dashboard'); ?>
            </p>
        </div>
    <?php endif; ?>

    <h2 class="nav-tab-wrapper">
        <?php foreach ($tabs as $tab => $tabName) : ?>
            <a class="nav-tab<?= $currentTab === $tab ? ' nav-tab-active' : ''; ?>" href="?page=<?= $_GET['page'] ?>&tab=<?= $tab; ?>"><?= $tabName; ?></a>
        <?php endforeach; ?>
    </h2>
    <?php
    // Include tabs template
    include SUPR_DASHBOARD_PATH . "templates/settings-tabs/{$currentTab}.php";
    ?>
</div>