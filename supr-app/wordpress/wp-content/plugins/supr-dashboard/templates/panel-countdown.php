<?php

// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$activeCode = \SuprDashboard\Countdown::instance()->getActiveCode();
?>
<div>
    <div>
        <?= \str_replace(['[dashboard-countdown-coupon]', '[dashboard-countdown-discount]'], [$activeCode['code'], $activeCode['discount']], $activeCode['content']); ?>
    </div>
    <div class="soon" id="soon-amor"
         data-format="d,h,m,s"
         data-layout="group"
         data-scale-max="l"
         data-due="<?= $activeCode['expire_day']->format(\DateTime::ATOM) ?>"
         data-labels-days="<?= __('Day', 'supr-dashboard') ?>,<?= __('Days', 'supr-dashboard') ?>"
         data-labels-hours="<?= __('Hour', 'supr-dashboard') ?>,<?= __('Hours', 'supr-dashboard') ?>"
         data-labels-minutes="<?= __('Minute', 'supr-dashboard') ?>,<?= __('Minutes', 'supr-dashboard') ?>"
         data-labels-seconds="<?= __('Second', 'supr-dashboard') ?>,<?= __('Seconds', 'supr-dashboard') ?>"
         data-face="flip color-light corners-sharp shadow-soft">
    </div>
    <a href="javascript: supr_upsale_show_modal(undefined, undefined, '<?= $activeCode['code']; ?>');" class="button button-primary button-block text-uppercase">
        <?php _e('Book smart plan now', 'supr-dashboard'); ?>
    </a>
</div>