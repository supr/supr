<?php

// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

use \SuprDashboard\Feed;

//Get feed
$feed = Feed::getFeed();

?>
<div class="row">
    <div class="wu-col-sm-3">
        <div class="blog-icon">
            <i class="streamline streamline-Newspaper-Fold"></i>
        </div>
    </div>
    <div class="wu-col-sm-9">
        <p><?= __('In our blog you will find all news about your shop and current e-commerce posts.', 'supr-dashboard'); ?></p>
        <a class="button button-secondary" target="_blank" href="https://de.supr.com/blog"><?= __('Visit our blog', 'supr-dashboard'); ?></a>
    </div>
</div>
<?php if (is_array($feed)) : ?>
    <ul>
        <?php foreach ($feed as $post) : ?>
            <li>
                <a href='<?= $post['link']; ?>'>
                    <h4><?= $post['title']; ?></h4>
                    <small><?= $post['content']; ?></small>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
<?php elseif (is_string($feed)) : ?>
    <p><?= $feed ?></p>
<?php endif; ?>

