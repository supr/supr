<?php

// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

// Handle request
if (isset($_POST['supr_dashboard_help_posts']) && is_array($_POST['supr_dashboard_help_posts'])) {
    $helpPosts = [
        'titles' => $_POST['supr_dashboard_help_posts']['titles'],
        'urls' => $_POST['supr_dashboard_help_posts']['urls']
    ];

    // Trim all values
    $helpPosts['titles'] = array_map('trim', $helpPosts['titles']);
    $helpPosts['urls'] = array_map('trim', $helpPosts['urls']);
    // Remove empty values
    $helpPosts['titles'] = array_filter($helpPosts['titles']);
    $helpPosts['urls'] = array_filter($helpPosts['urls']);
    // Sanitize values with WP special function
    $helpPosts['titles'] = array_map('sanitize_text_field', $helpPosts['titles']);
    $helpPosts['urls'] = array_map('sanitize_text_field', $helpPosts['urls']);

    if (\update_option('supr_dashboard_help_posts', $helpPosts)) {
        \wp_redirect(\network_admin_url('admin.php?page=supr_dashboard_settings&tab=dashboard-help&status=success'));
    }
}

$helpPosts = \get_option('supr_dashboard_help_posts', []);
?>
<form method="POST" id="supr-dashboard-help-form">
    <ul id="supr-dashboard-help-sortable">
        <?php for ($i = 0; $i <= count($helpPosts['titles']) - 1; $i++) : ?>
            <li class="ui-state-default">
                <div class="row">
                    <div class="wu-col-sm-1">
                        <span class="dashicons dashicons-menu"></span>
                    </div>
                    <div class="wu-col-sm-5">
                        <label>Help title</label>
                        <input name="supr_dashboard_help_posts[titles][]"
                               value="<?= $helpPosts['titles'][$i]; ?>"
                               class="regular-text"
                               type="text"
                               style="width: 100%">
                    </div>
                    <div class="wu-col-sm-5">
                        <label>Help URL</label>
                        <input name="supr_dashboard_help_posts[urls][]"
                               value="<?= $helpPosts['urls'][$i]; ?>"
                               class="regular-text"
                               type="text"
                               style="width: 100%">
                    </div>
                    <div class="wu-col-sm-1">
                        <span class="dashicons dashicons-post-trash"></span>
                    </div>
                </div>
            </li>
        <?php endfor; ?>
    </ul>
    <br>
    <span class="button button-xecondary" id="supr-dashboard-help-setting-clone">Add entry</span>
    <?php submit_button(); ?>
</form>


<ul id="supr-dashboard-help-values-clone" style="display: none;">
    <li class="ui-state-default">
        <div class="row">
            <div class="wu-col-sm-1">
                <span class="dashicons dashicons-menu"></span>
            </div>
            <div class="wu-col-sm-5">
                <label>Help title</label>
                <input name="supr_dashboard_help_posts[titles][]"
                       value=""
                       class="regular-text"
                       type="text"
                       style="width: 100%">
            </div>
            <div class="wu-col-sm-5">
                <label>Help URL</label>
                <input name="supr_dashboard_help_posts[urls][]"
                       value=""
                       class="regular-text"
                       type="text"
                       style="width: 100%">
            </div>
            <div class="wu-col-sm-1">
                <span class="dashicons dashicons-post-trash"></span>
            </div>
        </div>
    </li>
</ul>