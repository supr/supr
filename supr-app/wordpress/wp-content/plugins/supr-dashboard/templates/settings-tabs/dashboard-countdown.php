<?php

// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

// Save countdown options
if (isset($_POST['supr_dashboard_countdown_code']) && \is_array($_POST['supr_dashboard_countdown_code'])) {
    if (\SuprDashboard\Countdown::instance()->saveSettingsFromPostRequest()) {
        \wp_redirect(\network_admin_url('admin.php?page=supr_dashboard_settings&tab=dashboard-countdown&status=success'));
    }
}

// Get countdown options
$countdownOptions = \SuprDashboard\Countdown::instance()->getSettings();

// Add empty values for form
$countdownOptions['code'][] = '';
$countdownOptions['discount'][] = '';
$countdownOptions['days'][] = '';
?>
<form method="POST" id="supr-dashboard-countdown-form">
    <table class="form-table">
        <tbody>
        <tr>
            <td>
                <label for="supr_dashboard_countdown_title">
                    <?= __('Title', 'supr-dashboard'); ?>
                </label>
            </td>
            <td>
                <input name="supr_dashboard_countdown_title"
                       id="supr_dashboard_countdown_title"
                       value="<?= $countdownOptions['title']; ?>"
                       class="regular-text"
                       type="text">
            </td>
        </tr>
        <tr>
            <td>
                <label for="supr_dashboard_countdown_content">
                    <?= __('Content', 'supr-dashboard'); ?>
                </label>
            </td>
            <td>
                <?php
                $settings = array(
                    'wpautop' => false,
                    'media_buttons' => true,
                    'textarea_rows' => 4
                );
                wp_editor(stripslashes($countdownOptions['content']), 'supr_dashboard_countdown_content', $settings);
                ?>
                <p><?= __('You can use short codes [dashboard-countdown-coupon] and [dashboard-countdown-discount].', 'supr-dashboard'); ?></p>
            </td>
        </tr>
        <?php foreach ($countdownOptions['code'] as $num => $val) : ?>
            <tr>
                <th scope="row" colspan="2">
                    <hr>
                </th>
            </tr>
            <tr>
                <th>
                    <label for=supr_dashboard_countdown_code_<?= $num; ?>">
                        <?= __('Coupon code', 'supr-dashboard'); ?>
                    </label>
                </th>
                <td>
                    <input name="supr_dashboard_countdown_code[<?= $num; ?>]"
                           id="supr_dashboard_countdown_code_<?= $num; ?>"
                           value="<?= $countdownOptions['code'][$num]; ?>"
                           class="regular-text"
                           type="text">
                </td>
            </tr>
            <tr>
                <th>
                    <label for=supr_dashboard_countdown_discount_<?= $num; ?>">
                        <?= __('Discount (%)', 'supr-dashboard'); ?>
                    </label>
                </th>
                <td>
                    <input name="supr_dashboard_countdown_discount[<?= $num; ?>]"
                           id="supr_dashboard_countdown_discount_<?= $num; ?>"
                           value="<?= $countdownOptions['discount'][$num]; ?>"
                           class="regular-text"
                           type="number">
                </td>
            </tr>
            <tr>
                <th>
                    <label for=supr_dashboard_countdown_days_<?= $num; ?>">
                        <?= __('Countdown days', 'supr-dashboard'); ?>
                    </label>
                </th>
                <td>
                    <input name="supr_dashboard_countdown_days[<?= $num; ?>]"
                           id="supr_dashboard_countdown_days_<?= $num; ?>"
                           value="<?= $countdownOptions['days'][$num]; ?>"
                           class="small-text"
                           type="number">
                </td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <th scope="row" colspan="2">
                <?php submit_button(); ?>
            </th>
        </tr>
        </tbody>
    </table>
</form>

<p><?= __('* You cann add or delete coupons. We will show one coupon with the minimum remaining time. Please, also check if all of them are valid.', 'supr-dashboard'); ?></p>
