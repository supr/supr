<?php

// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

// Get Blog helpPosts from feed
$helpPosts = get_blog_option(1, 'supr_dashboard_help_posts', []);

?>
<div>
    <div class="row">
        <div class="wu-col-sm-9">
            <p><?= __('Need some help setting up your store? You will find the right answer to all your questions in our help section.', 'supr-dashboard'); ?></p>
        </div>
        <div class="wu-col-sm-3">
            <div class="help-icon">
                <i class="streamline streamline-Conversation-Question-Warning"></i>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="wu-col-sm-12">
            <form action="https://supr.help/" target="_blank">
                <p class="search-box">
                    <input id="post-search-input" name="s" value="" type="search" title="<?= __('Search', 'supr-dashboard') ?>">
                    <button type="submit" id="search-submit" class="button">
                        <span class="dashicons dashicons-search"></span>
                    </button>
                </p>
            </form>
            <ul>
                <?php for ($i = 0, $iMax = count($helpPosts['titles'] ?? []); $i < $iMax; $i++) : ?>
                    <li>
                        <a href="<?= $helpPosts['urls'][$i]; ?>" target="_blank">
                            <h4><?= $helpPosts['titles'][$i]; ?></h4>
                        </a>
                    </li>
                <?php endfor; ?>
            </ul>
        </div>
    </div>
</div>
