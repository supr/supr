<?php

// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

?>
<div>
    <div class="row">
        <div class="wu-col-sm-12">
            <p><?= __('Check availability of your store and share it with others', 'supr-dashboard'); ?></p>
            <table class="form-table" style="margin-bottom: 20px">
                <tbody>
                <tr>
                    <?php if (\get_option('elementor_maintenance_mode_mode')) : ?>
                        <th class="text-center" style="width: 20px">
                            <span class="pulse-danger"></span>
                        </th>
                        <td>
                            <?= __('Your store is <strong>not available</strong> for customers', 'supr-dashboard'); ?>
                            <a href="admin.php?page=supr_wordpress_options" class="text-underline"><strong><?= __('Change', 'supr-dashboard'); ?></strong></a>
                        </td>
                    <?php else : ?>
                        <th class="text-center" style="width: 20px">
                            <span class="pulse-success"></span>
                        </th>
                        <td>
                            <?= __('Your store is <strong>available</strong> for customers', 'supr-dashboard'); ?>
                            <a href="admin.php?page=supr_wordpress_options" class="text-underline"><strong><?= __('Change', 'supr-dashboard'); ?></strong></a>
                        </td>
                    <?php endif; ?>
                </tr>
                <tr>
                    <?php if (\get_option('blog_public')) : ?>
                        <th class="text-center" style="width: 20px">
                            <span class="pulse-success"></span>
                        </th>
                        <td>
                            <?= __('Your store is <strong>available</strong> for search engines', 'supr-dashboard'); ?>
                            <a href="admin.php?page=supr_wordpress_options" class="text-underline"><strong><?= __('Change', 'supr-dashboard'); ?></strong></a>
                        </td>
                    <?php else : ?>
                        <th class="text-center" style="width: 20px">
                            <span class="pulse-danger"></span>
                        </th>
                        <td>
                            <?= __('Your store is <strong>not available</strong> for search engines', 'supr-dashboard'); ?>
                            <a href="admin.php?page=supr_wordpress_options" class="text-underline"><strong><?= __('Change', 'supr-dashboard'); ?></strong></a>
                        </td>
                    <?php endif; ?>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="wu-col-sm-12">
            <a class="button button-primary text-center" target="_blank" href="/"><?= __('Visit store', 'supr-dashboard'); ?></a>
            <span class="button button-secondary" id="share-store">
                <div class="dashicons dashicons-share-alt2"></div>
            </span>
            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url(home_url("/")); ?>" target="_blank" class="button button-secondary">
                <div class="dashicons dashicons-facebook-alt"></div>
            </a>
            <a href="https://twitter.com/home?status=Visit%20our%20store%20<?php echo esc_url(home_url("/")); ?>" target="_blank" class="button button-secondary">
                <div class="dashicons dashicons-twitter"></div>
            </a>
        </div>
    </div>
</div>
