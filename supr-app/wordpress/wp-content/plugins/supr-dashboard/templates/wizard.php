<?php

namespace SuprDashboard;

// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

?>
<div class="supr-dashboard-wizard">
    <?php if (WizardStatus::getInstance()->isFinished()) : ?>
        <div id="supr-dashboard-wizard-finished" class="postbox supr-dashboard-postbox">
            <div class="inside">
                <svg class="supr-dashboard-wizard-finished-checkmark" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 52 52">
                    <circle class="supr-dashboard-wizard-finished-checkmark-circle" cx="26" cy="26" r="25" fill="none"/>
                    <path class="supr-dashboard-wizard-finished-checkmark-check" fill="none"
                          d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
                </svg>
                <h2><?= __('Congratulations!', 'supr-dashboard') ?></h2>
                <p>
                    <?= __('The setup of your shop is completed.<br>You can now sell your products through your shop. We wish you good luck!', 'supr-dashboard') ?>
                </p>
            </div>
        </div>
        <div class="postbox postbox-bar supr-dashboard-postbox-bar">
            <div class="inside text-center">
                <button id="dashboard-cta-button-finished"
                        class="dashboard-cta-button"><?= __('Finish assistant', 'supr-dashboard') ?></button>
            </div>
        </div>
    <?php endif; ?>

    <div class="supr-dashboard-wizard-intro">
        <img src="<?= SUPR_DASHBOARD_URL; ?>img/wizard-title-image.jpg">
        <h2><?= __('Hi! This is your new SUPR shop', 'supr-dashboard'); ?></h2>
        <p>
            <?= __('With our setup wizard we help you to set up your shop in no time at all. The checklist guides you step by step through the most important steps - start now! ', 'supr-dashboard') ?>
        </p>
    </div>

    <div class="supr-dashboard-wizard-statusbox">
        <p>
            <strong><?= __('Wizard progress', 'supr-dashboard') ?>:</strong> <span
                    id="supr-dashboard-wizard-current">0</span> <?= __('of', 'supr-dashboard') ?> <span
                    id="supr-dashboard-wizard-total"></span> <?= __('tasks completed', 'supr-dashboard') ?>!
        </p>
        <div id="supr-dashboard-wizard-statusbox-bar">
            <div id="supr-dashboard-wizard-statusbox-bar-inner"></div>
        </div>
    </div>

    <div id="supr-dashboard-wizard-shop-owner"
         class="postbox supr-dashboard-postbox supr-dashboard-wizard-<?= WizardStatus::getInstance()->getOwnerStepStatus() ?>">
        <div class="inside">
            <div class="supr-dashboard-wizard-row">
                <div class="supr-dashboard-wizard-status">
                    <i class="la la-clipboard-list"></i>
                    <i class="la la-clipboard-check"></i>
                </div>
                <div class="supr-dashboard-wizard-content supr-dashboard-wizard-content-contact-data flex-wide">
                    <div class="supr-dashboard-wizard-inner">
                        <h3>1. <?= __('Give details about your shop', 'supr-dashboard') ?></h3>
                        <p><?= __('<strong>Let us start with the setup of your shop.</strong> First of all, enter your owner and contact details so that we can generate your imprint and your customers can see you as the owner. You can change these data at any time.', 'supr-dashboard') ?></p>
                        <div id="supr-dashboard-shop-owner-form-error"
                             class="supr-dashboard-shop-owner-form-error hidden">
                            <p><?= __('<strong>Settings not saved!</strong><br>Please check the marked fields and save the settings again.', 'supr-dashboard') ?></p>
                        </div>
                        <?php if (WizardStatus::getInstance()->getOwnerStepStatus() === 'done') { ?>
                            <a class="dashboard-cta-button"
                               href="admin.php?page=wc-settings" target="_blank"><?= __('Edit shop data', 'supr-dashboard') ?></a>
                        <?php } else { ?>
                            <form id="supr-dashboard-shop-owner-form"
                                  class="supr-dashboard-wizard-row supr-dashboard-wizard-woo-form waiting-form">
                                <div class="supr-dashboard-wizard-woo-form-col">
                                    <h4><?= __('Contact', 'supr-dashboard'); ?></h4>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_owner_salutation"><?= __('Salutation', 'supr-dashboard') ?>
                                            <span class="sdw-req">*</span></label>
                                        <select name="woocommerce_store_owner_salutation"
                                                id="woocommerce_store_owner_salutation" style="" class="">
                                            <option value="m"><?= __('Mr.', 'supr-dashboard') ?></option>
                                            <option value="f"><?= __('Mrs.', 'supr-dashboard') ?></option>
                                        </select>
                                    </div>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_owner_first_name"><?= __('First name', 'supr-dashboard'); ?>
                                            <span class="sdw-req">*</span></label>
                                        <input name="woocommerce_store_owner_first_name"
                                               id="woocommerce_store_owner_first_name" type="text"
                                               value="<?= get_option('woocommerce_store_owner_first_name', '') ?>">
                                    </div>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_owner_last_name"><?= __('Last name', 'supr-dashboard'); ?>
                                            <span class="sdw-req">*</span></label>
                                        <input name="woocommerce_store_owner_last_name"
                                               id="woocommerce_store_owner_last_name" type="text"
                                               value="<?= get_option('woocommerce_store_owner_last_name', '') ?>">
                                    </div>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_owner_phone"><?= __('Phone', 'supr-dashboard') ?>
                                            <span class="sdw-req">*</span></label>
                                        <input name="woocommerce_store_owner_phone" id="woocommerce_store_owner_phone"
                                               type="text"
                                               value="<?= get_option('woocommerce_store_owner_phone', '') ?>">
                                    </div>
                                    <h4><?= __('Address', 'supr-dashboard'); ?></h4>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_address"><?= __('Street, No.', 'supr-dashboard') ?>
                                            <span class="sdw-req">*</span></label>
                                        <input name="woocommerce_store_address" id="woocommerce_store_address"
                                               type="text" value="<?= get_option('woocommerce_store_address', '') ?>">
                                    </div>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_city"><?= __('City', 'supr-dashboard') ?> <span
                                                    class="sdw-req">*</span></label>
                                        <input name="woocommerce_store_city" id="woocommerce_store_city" type="text"
                                               value="<?= get_option('woocommerce_store_city', '') ?>">
                                    </div>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_postcode"><?= __('Postcode', 'supr-dashboard') ?>
                                            <span class="sdw-req">*</span></label>
                                        <input name="woocommerce_store_postcode" id="woocommerce_store_postcode"
                                               type="text" value="<?= get_option('woocommerce_store_postcode', '') ?>">
                                    </div>
                                </div>
                                <div class="supr-dashboard-wizard-woo-form-col">
                                    <h4><?= __('Your business', 'supr-dashboard'); ?></h4>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_owner_company"><?= __('Name of your business', 'supr-dashboard') ?></label>
                                        <input name="woocommerce_store_owner_company"
                                               id="woocommerce_store_owner_company" type="text"
                                               value="<?= get_option('woocommerce_store_owner_company', '') ?>">
                                    </div>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_owner_store_category"><?= __('Shop category', 'supr-dashboard') ?>
                                            <span class="sdw-req">*</span></label>
                                        <?php
                                        $storeCategorie = [
                                            5912 => __('Pharmacies', 'supr-dashboard'),
                                            5511 => __('Automotive Industry', 'supr-dashboard'),
                                            5192 => __('Books & Magazines', 'supr-dashboard'),
                                            5999 => __('Office & catering supplies', 'supr-dashboard'),
                                            5967 => __('Erotic e-commerce', 'supr-dashboard'),
                                            7999 => __('Leisure & Family', 'supr-dashboard'),
                                            5977 => __('Health Care', 'supr-dashboard'),
                                            7995 => __('Gambling', 'supr-dashboard'),
                                            5251 => __('Household & Garden products', 'supr-dashboard'),
                                            7922 => __('Card sales & Couponing', 'supr-dashboard'),
                                            5311 => __('Furniture & household goods', 'supr-dashboard'),
                                            5621 => __('Fashion Women', 'supr-dashboard'),
                                            5641 => __('Fashion children/baby', 'supr-dashboard'),
                                            5611 => __('Fashion Men', 'supr-dashboard'),
                                            5691 => __('Fashion men/women', 'supr-dashboard'),
                                            5735 => __('Music downloads', 'supr-dashboard'),
                                            5499 => __('Food', 'supr-dashboard'),
                                            7221 => __('Photography', 'supr-dashboard'),
                                            5094 => __('Jewellery', 'supr-dashboard'),
                                            7372 => __('Software-Downloads', 'supr-dashboard'),
                                            7299 => __('Other services', 'supr-dashboard'),
                                            5941 => __('Sport & Sportswear', 'supr-dashboard'),
                                            4814 => __('Telecommunications', 'supr-dashboard'),
                                            5732 => __('Consumer electronics', 'supr-dashboard'),
                                            5964 => __('Other goods', 'supr-dashboard')
                                        ];

                                        $currentStoreCategory = (int)get_option('woocommerce_store_owner_store_category', 0);
                                        ?>
                                        <select name="woocommerce_store_owner_store_category"
                                                id="woocommerce_store_owner_store_category">
                                            <?php foreach ($storeCategorie as $categoryNum => $categoryName) : ?>
                                                <option value="<?= $categoryNum ?>"
                                                        <?= $currentStoreCategory === $categoryNum ? ' selected' : '' ?>><?= $categoryName ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_owner_vat"><?= __('Vat.', 'supr-dashboard') ?></label>
                                        <input name="woocommerce_store_owner_vat" id="woocommerce_store_owner_vat"
                                               type="text" value="<?= get_option('woocommerce_store_owner_vat', '') ?>">
                                    </div>
                                    <h4><?= __('Social media', 'supr-dashboard'); ?></h4>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_owner_marketing_facebook"><?= __('Facebook page', 'supr-dashboard'); ?></label>
                                        <input name="woocommerce_store_owner_marketing_facebook"
                                               id="woocommerce_store_owner_marketing_facebook" type="text"
                                               placeholder="<?= __('https://www.facebook.com/meine-seite', 'supr-dashboard') ?>"
                                               value="<?= get_option('woocommerce_store_owner_marketing_facebook', '') ?>">
                                    </div>
                                    <div class="supr-dashboard-wizard-woo-form-row">
                                        <label for="woocommerce_store_owner_marketing_instagram"><?= __('Instagram account', 'supr-dashboard') ?></label>
                                        <input name="woocommerce_store_owner_marketing_instagram"
                                               id="woocommerce_store_owner_marketing_instagram" type="text"
                                               placeholder="@instagram-name"
                                               value="<?= get_option('woocommerce_store_owner_marketing_instagram', '') ?>">
                                    </div>
                                </div>
                            </form>
                            <button id="dashboard-cta-button-owner"
                                    class="dashboard-cta-button"><?= __('Save shop data', 'supr-dashboard') ?></button>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="supr-dashboard-wizard-design"
         class="postbox supr-dashboard-postbox supr-dashboard-wizard-<?= WizardStatus::getInstance()->getDesignStepStatus() ?>">
        <div class="inside">
            <div class="supr-dashboard-wizard-row">
                <div class="supr-dashboard-wizard-status">
                    <i class="la la-clipboard-list"></i>
                    <i class="la la-clipboard-check"></i>
                </div>
                <div class="supr-dashboard-wizard-content">
                    <div class="supr-dashboard-wizard-inner">
                        <h3>2. <?= __('Design your shop', 'supr-dashboard') ?></h3>
                        <p>
                            <?= __('<strong>A good shop design is important for happy customers!</strong> You can choose from a variety of free designs, all optimized for mobile devices like smartphones and tablets.', 'supr-dashboard') ?>
                        </p>
                        <button id="dashboard-cta-button-design"
                                class="dashboard-cta-button"><?= __('Select a design', 'supr-dashboard') ?></button>
                    </div>
                </div>
                <div class="supr-dashboard-wizard-help">
                    <div class="video-preview-image">
                        <a href="https://supr.help/darstellung/template-hinzufuegen-aktivieren/" target="_blank">
                            <img src="<?= SUPR_DASHBOARD_URL; ?>img/video-preview-design.jpg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="supr-dashboard-wizard-product"
         class="postbox supr-dashboard-postbox supr-dashboard-wizard-<?= WizardStatus::getInstance()->getProductsStepStatus() ?>">
        <div class="inside">
            <div class="supr-dashboard-wizard-row">
                <div class="supr-dashboard-wizard-status">
                    <i class="la la-clipboard-list"></i>
                    <i class="la la-clipboard-check"></i>
                </div>
                <div class="supr-dashboard-wizard-content">
                    <div class="supr-dashboard-wizard-inner">
                        <h3>3. <?= __('Create your first product', 'supr-dashboard') ?></h3>
                        <p><?= __('<strong>Create or import products!</strong> Simply add descriptions, prices, pictures or product variations and fill your shop with life. In our video we tell you how to do this the fastest way.', 'supr-dashboard') ?></p>
                        <a class="dashboard-cta-button"
                           href="/wp-admin/post-new.php?post_type=product" target="_blank"><?= __('Create a product', 'supr-dashboard') ?></a>
                        <a class="dashboard-cta-button"
                           href="/wp-admin/edit-tags.php?taxonomy=product_cat&post_type=product" target="_blank"> <?= __('Create a category', 'supr-dashboard') ?></a>
                    </div>
                </div>
                <div class="supr-dashboard-wizard-help">
                    <div class="video-preview-image">
                        <a href="http://supr.help/produkte/produkt-erstellen/" target="_blank">
                            <img src="<?= SUPR_DASHBOARD_URL ?>img/video-preview-product.jpg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="supr-dashboard-wizard-payment"
         class="postbox supr-dashboard-postbox supr-dashboard-wizard-<?= WizardStatus::getInstance()->getPaymentStepStatus() ?>">
        <div class="inside">
            <div class="supr-dashboard-wizard-row">
                <div class="supr-dashboard-wizard-status">
                    <i class="la la-clipboard-list"></i>
                    <i class="la la-clipboard-check"></i>
                </div>
                <div class="supr-dashboard-wizard-content">
                    <div class="supr-dashboard-wizard-inner">
                        <h3>4. <?= __('Set up payment methods', 'supr-dashboard') ?></h3>
                        <p><?= __('<strong>Activate the most popular payment methods now! </strong> To enable your customers to pay for their order in your shop in their preferred way, you can now use the SUPR payment methods.', 'supr-dashboard') ?></p>
                        <a class="dashboard-cta-button"
                           href="/wp-admin/admin.php?page=wc-settings&tab=checkout" target="_blank"><?= __('Activate payment methods', 'supr-dashboard') ?></a>
                    </div>
                </div>
                <div class="supr-dashboard-wizard-help">
                    <div class="video-preview-image">
                        <a href="https://supr.help/konfiguration/bezahlarten-hinzufuegen/" target="_blank">
                            <img src="<?= SUPR_DASHBOARD_URL ?>img/video-preview-payment.jpg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="supr-dashboard-wizard-shipping"
         class="postbox supr-dashboard-postbox supr-dashboard-wizard-<?= WizardStatus::getInstance()->getShippingStepStatus() ?>">
        <div class="inside">
            <div class="supr-dashboard-wizard-row">
                <div class="supr-dashboard-wizard-status">
                    <i class="la la-clipboard-list"></i>
                    <i class="la la-clipboard-check"></i>
                </div>
                <div class="supr-dashboard-wizard-content">
                    <div class="supr-dashboard-wizard-inner">
                        <h3>5. <?= __('Check shipment', 'supr-dashboard') ?></h3>
                        <p><?= __('<strong> At the latest before you set your shop online the shipping options must be correct.</strong> In this section you can set the countries you want to ship to and the costs and methods.', 'supr-dashboard') ?></p>
                        <button id="dashboard-cta-button-shipping"
                                class="dashboard-cta-button"><?= __('Set up shipping methods & costs', 'supr-dashboard') ?></button>
                    </div>
                </div>
                <div class="supr-dashboard-wizard-help">
                    <div class="video-preview-image">
                        <a href="https://supr.help/konfiguration/versandart-hinzufuegen/" target="_blank">
                            <img src="<?= SUPR_DASHBOARD_URL ?>img/video-preview-shipping.jpg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="supr-dashboard-wizard-legal"
         class="postbox supr-dashboard-postbox supr-dashboard-wizard-<?= WizardStatus::getInstance()->getLegalStepStatus() ?>">
        <div class="inside">
            <div class="supr-dashboard-wizard-row">
                <div class="supr-dashboard-wizard-status">
                    <i class="la la-clipboard-list"></i>
                    <i class="la la-clipboard-check"></i>
                </div>
                <div class="supr-dashboard-wizard-content">
                    <div class="supr-dashboard-wizard-inner">
                        <h3> 6. <?= __('Legal pages', 'supr-dashboard') ?></h3>
                        <p><?= __('<strong>Important! Without these contents you should not go online with your shop.</strong> The legal pages are one of the most important contents of your shop. Please add this content and if necessary get professional help to protect yourself from reprimands.', 'supr-dashboard') ?></p>
                        <a class="dashboard-cta-button"
                           href="/wp-admin/post.php?post=<?= get_option('woocommerce_terms_page_id', '') ?>&action=edit" target="_blank"><?= __('Terms and conditions', 'supr-dashboard') ?></a>
                        <a class="dashboard-cta-button"
                           href="/wp-admin/post.php?post=<?= get_option('woocommerce_imprint_page_id', '') ?>&action=edit" target="_blank"><?= __('Imprint', 'supr-dashboard') ?></a>
                        <a class="dashboard-cta-button"
                           href="/wp-admin/post.php?post=<?= get_option('woocommerce_data_security_page_id', '') ?>&action=edit" target="_blank"><?= __('Privacy policy', 'supr-dashboard') ?> </a>
                        <a class="dashboard-cta-button"
                           href="/wp-admin/post.php?post=<?= get_option('woocommerce_revocation_page_id', '') ?>&action=edit" target="_blank"><?= __('Revocation', 'supr-dashboard') ?> </a>
                    </div>
                </div>
                <div class="supr-dashboard-wizard-help">
                    <div class="video-preview-image">
                        <a href="http://supr.help/konfiguration/rechtstexte-einfuegen/" target="_blank">
                            <img src="<?= SUPR_DASHBOARD_URL ?>img/video-preview-legal.jpg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="supr-dashboard-wizard-plan"
         class="postbox supr-dashboard-postbox supr-dashboard-wizard-<?= WizardStatus::getInstance()->getPlanStepStatus() ?>">
        <div class="inside">
            <div class="supr-dashboard-wizard-row">
                <div class="supr-dashboard-wizard-status">
                    <i class="la la-clipboard-list"></i>
                    <i class="la la-clipboard-check"></i>
                </div>
                <div class="supr-dashboard-wizard-content">
                    <div class="supr-dashboard-wizard-inner">
                        <h3>7. <?= __('Book smart plan and save 30%', 'supr-dashboard') ?></h3>
                        <p><?= __('<strong>Book now the smart plan for your shop and save 30%!</strong> You have set up your shop almost completely and now you are ready to become successful with your shop, then book the popular smart plan.', 'supr-dashboard') ?></p>
                        <a href="#show-upsale_coupon-SUPRANGEBOT30" id="dashboard-cta-button-plan"
                           class="dashboard-cta-button"><?= __('Book our smart plan', 'supr-dashboard') ?></a>
                        <button id="dashboard-cta-button-plan-skip"
                            class="dashboard-cta-button dashboard-cta-button-secondary"><?= __('Skip', 'supr-dashboard') ?></button>
                    </div>
                </div>
                <div class="supr-dashboard-wizard-help">
                    <img class="supr-dashboard-wizard-sales-badge" src="<?= SUPR_DASHBOARD_URL ?>img/sales-badge.png">
                </div>
            </div>
        </div>
    </div>

    <div id="supr-dashboard-wizard-public"
         class="postbox supr-dashboard-postbox supr-dashboard-wizard-<?= WizardStatus::getInstance()->getPublicStepStatus() ?>">
        <div class="inside">
            <div class="supr-dashboard-wizard-row">
                <div class="supr-dashboard-wizard-status">
                    <i class="la la-store"></i>
                </div>
                <div class="supr-dashboard-wizard-content">
                    <div class="supr-dashboard-wizard-inner">
                        <h3>8. <?= __('Publish your shop', 'supr-dashboard') ?></h3>
                        <p>

                        </p>
                        <button id="dashboard-cta-button-public"
                                class="dashboard-cta-button"><?= __('Publish the shop now', 'supr-dashboard') ?></button>
                    </div>
                </div>
                <div class="supr-dashboard-wizard-help"></div>
            </div>
        </div>
    </div>

    <div id="supr-dashboard-wizard-skip" class="supr-dashboard-wizard-skip text-center">
        <span id="supr-dashboard-wizard-skip-link"><?= __('I want to skip the setup wizard', 'supr-dashboard') ?></span>
    </div>

</div>