<?php
/**
 * Plugin Name: SUPR - Signup
 * Plugin URI: https://supr.com
 * Description: Customizer of signup process
 * Text Domain: supr-signup
 * Domain Path: /languages
 * Version: 1.0
 * Author: SUPR Development
 * License: GPL
 */

// We use it always in Sunrise.php
if (!defined('SUPR_SIGNUP_PATH')) {
    define('SUPR_SIGNUP_PATH', plugin_dir_path(__FILE__));
}

define('SUPR_SIGNUP_URL', plugins_url('/', __FILE__));
define('SUPR_SIGNUP_FILE', plugin_basename(__FILE__));
define('SUPR_SIGNUP_DIR_NAME', basename(__DIR__));

define('SUPR_SIGNUP_VERSION', '2.0.0');

// Load main Class
require SUPR_SIGNUP_PATH . 'includes/Plugin.php';

/**
 * Callback for WP-Ultimo
 *
 * @param $plan
 * @return string
 */
function supr_signup_pricetable_list($plan)
{
    return \SuprSignup\PriceTable::priceTableList($plan);
}

/**
 * Callback for WP-Ultimo
 */
function supr_signup_footer()
{
    \SuprSignup\SignupFooter::printFooter();
}
