<?php

namespace SuprSignup;

/**
 * Class SignupFooter
 *
 * @package SuprSignup
 */
class SignupFooter
{
    public static function printFooter(): void
    {
        $supr_signup_footer_content = stripslashes(get_blog_option(1, 'supr_signup_footer_content'));

        echo "<div id='supr-signup-footer' class='supr-signup-footer'>$supr_signup_footer_content</div>";
    }
}
