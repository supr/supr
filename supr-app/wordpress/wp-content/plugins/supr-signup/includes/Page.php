<?php

namespace SuprSignup;

/**
 * Class Page
 *
 * @package SuprSignup
 */
class Page
{
    /**
     * Show setting page
     */
    public static function settingsPage(): void
    {
        include SUPR_SIGNUP_PATH . 'templates/network-settings-page.php';
    }
}
