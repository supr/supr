<?php

namespace SuprSignup;

/**
 * Class Plugin
 *
 * @package SuprSignup
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return \SuprSignup\Plugin
     */
    public static function getInstance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Init
     */
    public function adminInit(): void
    {
        // Style for pricind table
        \add_action('admin_enqueue_scripts', [ResourceManager::class, 'enqueuePricingTableCss']);
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // We have only one point registration
        Manager::instance()->redirectToNewRegistration();

        // We can restore password only from main blog
        Manager::instance()->changePasswordForgotUrl();

        // Load translations
        \add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        // Add menu item
        Menu::addMenu();

        // Register registration short code
        Registration::instance()->registerShortCode();

        // Add CSS and JS for login page / registration short code / pass forgot
        ResourceManager::addResources();

        // Allow to sign up with email instead of user name
        Manager::instance()->allowSignUpWithEmail();
        StoreOwner::fullStoreOwnerData();

        // Register end points
        Api::registerRoutes();

        \add_action('admin_init', [$this, 'adminInit'], 0);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_SIGNUP_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::getInstance();
