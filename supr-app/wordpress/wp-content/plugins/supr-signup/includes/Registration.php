<?php

namespace SuprSignup;

/**
 * Class Registration
 *
 * @package SuprSignup
 */
class Registration
{
    /**
     * @var null
     */
    public static $instance;

    /**
     * @var string
     */
    private $defaultPlanName = 'Smart Trial';

    /**
     * @var bool
     */
    private $showDomainField = false;

    /**
     * @var null
     */
    private $error;

    /**
     * @var string
     */
    private $transientKey;

    /**
     * @var array
     */
    private $transientData;

    /**
     * @var string
     */
    private $redirectUrl;

    /**
     * Is it an ajax call?
     *
     * @var bool
     */
    private $ajaxRequest;

    /**
     * @var float
     */
    private $created;

    /**
     * @return \SuprSignup\Registration
     */
    public static function instance(): Registration
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        $this->created = self::microtimeFloat();
    }

    /**
     * Handle post request and create a shop via WP-Ultimo
     */
    public function handlePostRequest(): void
    {
        if (isset($_POST['supr_signup_registration_form_shop_name'])) {
            // If already logged in
            if (\is_user_logged_in()) {
                $blogs = \get_blogs_of_user(get_current_user_id());
                $this->error = sprintf(
                    __('You are already logged in: <a target="_blank" href=\'%s\'>to my shop.</a>', 'supr-signup'),
                    \get_admin_url(\count($blogs) > 0 ? reset($blogs)->userblog_id : 1)
                );

                $this->sendAjaxResponse();

                return;
            }

            // If we have all classes, what we need
            if (!$this->checkRequireClasses()) {
                $this->sendAjaxResponse();

                return;
            }

            $defaultPlan = $this->getDefaultPlan();

            // If we don't have first (default) plan
            if ($defaultPlan === null) {
                \error_log("[SUPR-SIGNUP] Plan '{$this->defaultPlanName}' cannot be found. We cannot register new shops.");
                $this->error = __('Registration is not available. Please try it later.', 'supr-signup');
                $this->sendAjaxResponse();

                return;
            }

            // If we have template id
            $templateId = $_POST['supr_signup_registration_form_template_id'] ?? $_GET['template_id'] ?? null;
            $templateId = $templateId !== null ? (int)$templateId : null;

            // Sanitize
            $this->sanitizeRequestData();

            // Validate
            if (!$this->validateRequestData()) {
                $this->sendAjaxResponse();

                return;
            }

            $user = [
                'data' => [
                    'user_name' => $_POST['supr_signup_registration_form_email'],
                    'user_login' => $_POST['supr_signup_registration_form_email'],
                    'user_email' => $_POST['supr_signup_registration_form_email'],
                    'user_pass' => $_POST['supr_signup_registration_form_password']
                ],
                'plan' => [
                    'plan_id' => $defaultPlan->get_id(),
                    'plan_freq' => 1
                ],
                'meta' => [
                    'plan_id' => $defaultPlan->get_id(),
                    'plan_freq' => 1,
                    'template' => $templateId
                ]
            ];

            // Add the filter to change user meta
            \add_filter('insert_user_meta', [$this, 'changeUserMeta'], 10, 3);

            $shop = [
                'data' => [
                    'blog_title' => $_POST['supr_signup_registration_form_shop_name'],
                    'blogname' => $_POST['supr_signup_registration_form_shop_slug'],
                    'domain_option' => null,
                    'role' => 'administrator'
                ],
                'meta' => [
                    'blog_upload_space' => $defaultPlan->quotas['upload']
                ]
            ];

            // Allow to change user data for other plugins
            $user = \apply_filters('supr_signup_user_data', $user);

            if ($user instanceof \WP_Error) {
                $this->error = $user->get_error_message();
                $this->sendAjaxResponse();

                return;
            }

            // Allow to change blog data for other plugins
            $shop = \apply_filters('supr_signup_blog_data', $shop);

            if ($shop instanceof \WP_Error) {
                $this->error = $shop->get_error_message();
                $this->sendAjaxResponse();

                return;
            }

            $this->transientKey = \WU_Signup::get_transient_key($_GET['cs']);
            $this->transientData = \array_merge($user['data'], $user['plan'], $user['meta'], $shop['data'], $shop['meta']);

            // Save transient for current site
            \set_site_transient($this->transientKey, $this->transientData);

            // Add filter for fast getting the transient data
            \add_filter("pre_site_transient_{$this->transientKey}", [$this, 'getTransientData'], 10, 2);

            // Catch redirect to keep _ga (Google Analytics parameter)
            \add_filter('wp_redirect', [$this, 'keepGAParamsInWpRedirect'], 0);

            // Catch redirect to return url with ajax
            if ($this->isAjax()) {
                \add_filter('wp_redirect', [$this, 'preventWpRedirect']);
                \add_filter('wp_die_handler', [$this, 'getWpDieFunctionName']);
            }

            // Disable copy of all files (a lot of trash)
            if (isset($defaultPlan->site_template) && (int)$defaultPlan->site_template > 0) {
                add_filter('mucd_copy_dirs', [$this, 'customCopyBlogDirs'], 10, 3);
            }

            // Create user and shop
            (new \WU_Signup())->create_account();

            // Try to return ajax response
            $this->sendAjaxResponse();

            return;
        }
    }

    /**
     * We need to copy a lot of files, and PHP-Script does it slowly
     *
     * @param array $dirs
     * @param       $from_site_id
     * @param       $to_site_id
     * @return array
     */
    public function customCopyBlogDirs($dirs, $from_site_id, $to_site_id): array
    {
        // We try to copy it with shel command
        if (\function_exists('shell_exec') && (int)\shell_exec('echo 1') === 1) {
            foreach ($dirs as $num => $dir) {
                // Sync dirs with many sub-processes
                $scriptPath = ABSPATH . '../sync-dirs.sh';
                \shell_exec("{$scriptPath} {$dir['from_dir_path']} {$dir['to_dir_path']}");

                // Solution with rsync (it works slow with EFS AWS Storage)
                //$result = \shell_exec("rsync -r {$dir['from_dir_path']}/ {$dir['to_dir_path']} --exclude cache");

                // Solution with cp (it works slow with EFS AWS Storage)
                //$result = \shell_exec("cp -R {$dir['from_dir_path']}. {$dir['to_dir_path']}");
            }

            // Return empty: don't need something to copy anymore
            return [];
        }

        // SOLUTION WITHOUT SHELL (it works slow with EFS AWS Storage)

        // All folders, that we need to have in a new blog
        // \d{4}: 2018 or 2019 or 2020 - it is default uploads folder for wordpress
        // wp\-less\-cache: styles for admin panel
        $notExcludeRegexp = '/\d{4}|wp\-less\-cache/';

        foreach ($dirs as $num => $dir) {
            $objects = \scandir($dirs[$num]['from_dir_path']);
            foreach ($objects as $object) {
                if ($object !== '.' && $object !== '..' && !\preg_match($notExcludeRegexp, $object) && \filetype($dirs[$num]['from_dir_path'] . '/' . $object) === 'dir') {
                    $dirs[$num]['exclude_dirs'][] = $object;
                }
            }
        }

        return $dirs;
    }

    /**
     * Remove hidden menu-element #S2-1073
     *
     * @param $meta
     * @param $user
     * @param $update
     * @return mixed
     */
    public function changeUserMeta($meta, $user, $update)
    {
        // Create if not exist
        if (!isset($meta['metaboxhidden_nav-menus'])) {
            $meta['metaboxhidden_nav-menus'] = ['add-post-type-product', 'add-post_tag', 'add-product_tag'];
        }

        // Remove hidden element
        if (\is_array($meta['metaboxhidden_nav-menus']) && ($key = array_search('add-product_cat', $meta['metaboxhidden_nav-menus'], true)) !== false) {
            unset($meta['metaboxhidden_nav-menus'][$key]);
        }

        // Return
        return $meta;
    }

    /**
     * Check if classes were loaded
     *
     * @return bool
     */
    private function checkRequireClasses(): bool
    {
        if (!file_exists(SUPR_SIGNUP_PATH . '../wp-ultimo/wp-ultimo.php')) {
            $this->error = __('Class \'\WP_Ultimo\' doesn\'t exist.', 'supr-signup');

            return false;
        }

        return true;
    }

    /**
     * Correct data after user input
     */
    private function sanitizeRequestData(): void
    {
        // BUG FIX: If we don't have slug of the shop, try to generate it on the fly
        // It is a bug of web site (supr.com). There often don't work JS and the slug are not generated
        $_POST['supr_signup_registration_form_shop_slug'] = !empty($_POST['supr_signup_registration_form_shop_slug']) ? \sanitize_title($_POST['supr_signup_registration_form_shop_slug']) : \sanitize_title($_POST['supr_signup_registration_form_shop_name']);

        // BUG FIX: Sometimes we have request for registration not from our domain. But cs is reguired for WP-Ultimo
        // If we don't have data from our register form
        if (!isset($_GET['cs']) || empty($_GET['cs'])) {
            $_GET['cs'] = \uniqid('', true);
        }

        // Sanitise all text fields
        $_POST['supr_signup_registration_form_shop_slug'] = \sanitize_text_field($_POST['supr_signup_registration_form_shop_slug']);
        $_POST['supr_signup_registration_form_shop_name'] = \sanitize_text_field($_POST['supr_signup_registration_form_shop_name']);
        $_POST['supr_signup_registration_form_email'] = \sanitize_text_field($_POST['supr_signup_registration_form_email']);
        $_POST['supr_signup_registration_form_password'] = \sanitize_text_field($_POST['supr_signup_registration_form_password']);
        $_POST['supr_signup_registration_form_age'] = \sanitize_text_field($_POST['supr_signup_registration_form_age']);
        $_POST['supr_signup_registration_form_legal'] = \sanitize_text_field($_POST['supr_signup_registration_form_legal']);

        // Replace unwanted characters in shop name with space.
        $_POST['supr_signup_registration_form_shop_name'] = trim(preg_replace('/[^\p{L}\p{N}&\' ]/u', ' ', $_POST['supr_signup_registration_form_shop_name']));
        // Remove double spaces
        $_POST['supr_signup_registration_form_shop_name'] = preg_replace('/\s+/', ' ', $_POST['supr_signup_registration_form_shop_name']);
        $_POST['supr_signup_registration_form_email'] = strtolower(trim($_POST['supr_signup_registration_form_email']));
        $_POST['supr_signup_registration_form_password'] = trim($_POST['supr_signup_registration_form_password']);
        $_POST['supr_signup_registration_form_age'] = trim($_POST['supr_signup_registration_form_age']);
        $_POST['supr_signup_registration_form_legal'] = filter_var($_POST['supr_signup_registration_form_legal'], FILTER_VALIDATE_BOOLEAN);

        // Each label may contain up to 63 characters. The full domain name may not exceed the length of 253 characters in its textual representation.
        $_POST['supr_signup_registration_form_shop_slug'] = rtrim(substr($_POST['supr_signup_registration_form_shop_slug'], 0, 63), '-');
    }

    /**
     * Simple validation of data
     *
     * @return bool
     */
    private function validateRequestData(): bool
    {
        // At the first step we check the humans check field... it needs to be empty!
        if (!empty($_POST['supr_signup_registration_form_age'])) {
            $this->error = __('Yes, nice try ;)', 'supr-signup');

            return false;
        }

        if (!isset($_GET['cs']) || empty($_GET['cs'])) {
            $this->error = __('GET parameter "cs" doesn\'t exist.', 'supr-signup');

            return false;
        }

        if (empty($_POST['supr_signup_registration_form_shop_name'])) {
            $this->error = __('Shop name cannot be empty.', 'supr-signup');

            return false;
        }

        if (empty($_POST['supr_signup_registration_form_email'])) {
            $this->error = __('Email cannot be empty.', 'supr-signup');

            return false;
        }

        if (empty($_POST['supr_signup_registration_form_password'])) {
            $this->error = __('Password cannot be empty.', 'supr-signup');

            return false;
        }

        if (!is_email($_POST['supr_signup_registration_form_email'])) {
            $this->error = __('Email is not valid.', 'supr-signup');

            return false;
        }

        if (\strlen($_POST['supr_signup_registration_form_password']) < 6) {
            $this->error = __('Password length must be at least 6 characters.', 'supr-signup');

            return false;
        }

        if (empty($_POST['supr_signup_registration_form_shop_slug'])) {
            $this->error = __('Slug cannot be empty.', 'supr-signup');

            return false;
        }

        if ($_POST['supr_signup_registration_form_legal'] !== true) {
            $this->error = __('You must accept terms and conditions.', 'supr-signup');

            return false;
        }

        if (\email_exists($_POST['supr_signup_registration_form_email'])) {
            $this->error = __('This email already exists.', 'supr-signup');

            return false;
        }

        if (\domain_exists($_POST['supr_signup_registration_form_shop_slug'] . '.' . DOMAIN_CURRENT_SITE, '/')) {
            $this->showDomainField = true;
            $this->error = __('This domain is already used.', 'supr-signup');

            return false;
        }

        return true;
    }

    /**
     * Get default plan for user
     *
     * @return null|\WU_Plan
     */
    private function getDefaultPlan(): ?\WU_Plan
    {
        // Get plans
        /** @var \WU_Plan[] $plans */
        $plans = \WU_Plans::get_plans();

        // Loop them
        foreach ($plans as $plan) {
            if (\strtolower($plan->title) === \strtolower($this->defaultPlanName)) {
                return $plan;
            }
        }

        return null;
    }

    /**
     * Register short code and handler
     */
    public function registerShortCode(): void
    {
        \add_action('init', [$this, 'handlePostRequest']);
        \add_shortcode('supr-signup-registration-form', [$this, 'getRegisterForm']);
    }

    /**
     * Return html of register form
     *
     * @param $args
     * @return string
     */
    public function getRegisterForm($args): string
    {
        // Add js and css for reg form
        ResourceManager::enqueueRegistrationFormFiles();

        ob_start();

        $args = shortcode_atts(
            [
                'default' => true
            ],
            $args
        );

        $error = $this->error;
        $showDomainField = $this->showDomainField;

        include SUPR_SIGNUP_PATH . 'templates/registration-form.php';

        return ob_get_clean();
    }

    /**
     * Get current transient data
     *
     * @param        $defaultValue
     * @param string $transientKey
     * @return null|array
     */
    public function getTransientData($defaultValue, $transientKey): ?array
    {
        if ($transientKey === $this->transientKey && !empty($this->transientData)) {
            return $this->transientData;
        }

        return $defaultValue;
    }

    ////////////////////////////////////////////////
    /// SUPPORT FUNCTIONS TO HANDLE AJAX REQUEST ///
    ////////////////////////////////////////////////

    /**
     * Check if ajax request
     *
     * @return bool
     */
    private function isAjax(): bool
    {
        if ($this->ajaxRequest === null) {
            $this->ajaxRequest = isset($_POST['ajax']) ? true : false;
        }

        return $this->ajaxRequest;
    }

    /**
     * Save redirect url and return json response
     *
     * @param $location
     * @param $code
     * @return void
     */
    public function preventWpRedirect($location, $code = 302): void
    {
        $this->redirectUrl = $location;

        $this->sendAjaxResponse(['redirect' => $this->redirectUrl]);

        die;
    }

    /**
     * Keep Google Analytics GET parameter
     *
     * @param string $location
     * @param int $code
     * @return string
     */
    public function keepGAParamsInWpRedirect($location, $code = 302): string
    {
        // If GA param doesn't exist
        if (!isset($_REQUEST['_ga'])) {
            return $location;
        }

        // Parse url
        $url = parse_url($location);
        // Add GA param
        $url['query'] = (empty($url['query']) ? '' : '&') . '_ga=' . $_REQUEST['_ga'];

        // Build back
        $location = ((isset($url['scheme'])) ? $url['scheme'] . '://' : '')
                    . ($url['host'] ?? '')
                    . ($url['path'] ?? '')
                    . ((isset($url['query'])) ? '?' . $url['query'] : '')
                    . ((isset($url['fragment'])) ? '#' . $url['fragment'] : '');

        return $location;
    }

    /**
     * Set error message and return json response
     *
     * @param $message
     */
    public function preventWpDie($message): void
    {
        $this->error = $message;

        $this->sendAjaxResponse();

        die;
    }

    /**
     * Disable wp_die
     *
     * @param $defaultName
     * @return array
     */
    public function getWpDieFunctionName($defaultName): array
    {
        return [$this, 'preventWpDie'];
    }

    /**
     * Send json response (success or error)
     *
     * @param null $data
     */
    private function sendAjaxResponse($data = null): void
    {
        if ($this->isAjax()) {
            if ($data !== null) {
                \wp_send_json_success($data);
            } elseif ($this->error !== null) {
                \wp_send_json_error(['error' => $this->error]);
            } else {
                \wp_send_json_error();
            }
        }
    }

    /**
     * Get microtime in seconds as float
     *
     * @return float
     */
    public static function microtimeFloat(): float
    {
        list($usec, $sec) = explode(' ', microtime());

        return ((float)$usec + (float)$sec);
    }

    /**
     * Get time after creation of this instance
     *
     * @return float
     */
    public function getExecTime(): float
    {
        return self::microtimeFloat() - $this->created;
    }
}
