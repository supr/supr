<?php

namespace SuprSignup;

/**
 * Class StoreOwner
 *
 * @package SuprSignup
 */
class StoreOwner
{
    public static function fullStoreOwnerData(): void
    {
        // It has to been before redirect_to_payment: /plugins/wp-ultimo-billwerk/inc/class-wub-checkout.php:58
        add_action('admin_init', [self::class, 'setStoreOwner'], -11);

        // Update the owner of posts and products
        add_action('admin_init', [self::class, 'updatePostsOwner']);
    }

    /**
     * @return bool
     */
    public static function setStoreOwner(): bool
    {
        $current_blog_id = get_current_blog_id();
        $signup_finished = get_blog_option($current_blog_id, 'supr_signup_finsihed', 0);

        // Check if we have also written it
        // We need it because this option is 1 on main blog. And after sign up we make copy of main blog.
        if ((int)$signup_finished === (int)$current_blog_id) {
            return false;
        }

        // We don't write info about network admin
        if (\is_super_admin()) {
            return false;
        }

        $current_user = wp_get_current_user();

        // We need actual email address
        if (empty($current_user->user_email) || !filter_var($current_user->user_email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        $store_owner_fields = array(
            'email' => array(
                'admin_email',
                'woocommerce_stock_email_recipient',
                'woocommerce_email_from_address',
                'new_admin_email',
                'woocommerce_store_owner_email'
            ),
            'blogname' => 'woocommerce_email_from_name'
        );

        foreach ($store_owner_fields as $store_owner_field => $store_owner_value) {
            switch ($store_owner_field) {
                case 'email':
                    foreach ($store_owner_value as $email_field) {
                        update_blog_option($current_blog_id, $email_field, $current_user->user_email);
                    }

                    break;
                default:
                    $value = get_user_meta($current_user->ID, $store_owner_field, true);
                    update_blog_option($current_blog_id, $store_owner_value, $value);

                    break;
            }
        }

        update_blog_option($current_blog_id, 'supr_signup_finsihed', $current_blog_id);

        return true;
    }

    /**
     * @return bool
     */
    public static function updatePostsOwner(): bool
    {
        global $wpdb;

        $current_blog_id = get_current_blog_id();
        $posts_updated = get_blog_option($current_blog_id, 'supr_signup_post_owner_updated', 0);

        if ((int)$posts_updated === $current_blog_id) {
            return false;
        }

        $current_user = wp_get_current_user();

        // We don't write info about network admin
        if (is_super_admin($current_user->ID)) {
            return false;
        }

        $post_table = $wpdb->prefix . 'posts';
        $result = $wpdb->query($wpdb->prepare("UPDATE $post_table SET post_author = %d;", [$current_user->ID]));

        // We don't do it anymore
        if ($result !== false) {
            update_blog_option($current_blog_id, 'supr_signup_post_owner_updated', $current_blog_id);
        }

        return true;
    }
}
