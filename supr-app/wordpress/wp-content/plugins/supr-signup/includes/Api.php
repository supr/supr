<?php

namespace SuprSignup;

class Api
{
    public static function registerRoutes(): void
    {
        add_action(
            'rest_api_init',
            function () {
                register_rest_route(
                    'supr-signup-rest/v1',
                    '/stat',
                    [
                        'methods' => 'GET',
                        'callback' => [self::class, 'getStat'],
                        'permission_callback' => function () {
                            return true; // For all
                        }
                    ]
                );
            }
        );
    }

    /**
     * Get SignUp stats without test shops
     *
     * @param $data
     */
    public static function getStat($data): void
    {
        global $wpdb;

        $planIndexes = [
            'starter' => 'p_t',
            'smart trial' => 'p_t',
            'basic' => 'p_b',
            'smart' => 'p_s',
            'pro' => 'p_p'
        ];

        $returned = [
            's_a' => 0, // Signups all
            's_t' => 0, // Signups today
            's_y' => 0, // Signups yesterday
            'p_t' => 0, // Plans trial
            'p_b' => 0, // Plans basic
            'p_s' => 0, // Plans smart
            'p_p' => 0  // Pro trial
        ];

        // Get plans stat
        $plans = \get_posts(['post_type' => 'wpultimo_plan', 'numberposts' => 0]);
        foreach ($plans as $plan) {
            $index = $planIndexes[\strtolower($plan->post_title)] ?? null;

            // We count only needed plans
            if (!$index) {
                continue;
            }

            // Get stat
            $returned[$index] += (int)$wpdb->get_var($wpdb->prepare('
                SELECT COUNT(*)
                FROM wu_subscriptions 
                LEFT JOIN wu_site_owner ON wu_site_owner.user_id = wu_subscriptions.user_id
                LEFT JOIN blogs ON blogs.blog_id = wu_site_owner.site_id
                WHERE blogs.domain NOT LIKE \'test%\'
                AND plan_id = %d', $plan->ID));
        }

        // Get reg stat
        $today = (new \DateTime())->modify('today');
        $yesterday = (new \DateTime())->modify('yesterday');

        // Get reg stat
        $returned['s_a'] = (int)$wpdb->get_var('SELECT COUNT(*) FROM blogs WHERE blogs.domain NOT LIKE \'test%\'');
        $returned['s_t'] = (int)$wpdb->get_var($wpdb->prepare('SELECT COUNT(*) FROM blogs WHERE blogs.domain NOT LIKE \'test%\' AND registered > %s', $today->format('Y-m-d H:i:s')));
        $returned['s_y'] = (int)$wpdb->get_var($wpdb->prepare('SELECT COUNT(*) FROM blogs WHERE blogs.domain NOT LIKE \'test%\' AND registered BETWEEN %s AND %s', $yesterday->format('Y-m-d H:i:s'), $today->format('Y-m-d H:i:s')));

        // Send response
        wp_send_json_success($returned);
    }
}
