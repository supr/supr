<?php

namespace SuprSignup;

/**
 * Class Menu
 *
 * @package SuprSignup
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add menu page
        add_action('network_admin_menu', [__CLASS__, 'createMenu']);

        // Add settings link on plugin page
        add_filter('network_admin_plugin_action_links_' . SUPR_SIGNUP_FILE, [__CLASS__, 'addSettingsPage']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createMenu(): void
    {
        add_menu_page(
            __('Registration', 'supr-signup'),
            __('Registration', 'supr-signup'),
            'manage_network',
            'supr_signup_options',
            [Page::class, 'settingsPage'],
            'dashicons-buddicons-buddypress-logo'
        );
    }

    /**
     * @param array $links
     * @return mixed
     */
    public static function addSettingsPage($links): array
    {
        $settings_link = '<a href="admin.php?page=supr_signup_options">' . __('Settings', 'supr-signup') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
