<?php

namespace SuprSignup;

/**
 * Class Manager
 *
 * @package SuprSignup
 */
class Manager
{
    /**
     * @var null|Manager
     */
    private static $instance;

    /**
     * @return \SuprSignup\Manager
     */
    public static function instance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Signup With Email
     * Description: Allow user to signup with email address
     */
    public function allowSignUpWithEmail(): void
    {
        add_filter('wpmu_validate_user_signup', [$this, 'registerWithEmail']);
        add_filter('wpmu_validate_blog_signup', [$this, 'customBlogNameValidation']);
    }

    /**
     * Check URL and redirect to new registration
     */
    public function redirectToNewRegistration(): void
    {
        if (\strpos(\parse_url($_SERVER['REQUEST_URI'])['path'], '/signup') === 0 ||
            \strpos(\parse_url($_SERVER['REQUEST_URI'])['path'], '/wp-signup') === 0) {
            $getParams = '';
            if (isset($_GET['new']) && !empty($_GET['new'])) {
                $getParams = '?new=' . $_GET['new'];
            }
            header('Location: ' . $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/registration/' . $getParams);
            die();
        }
    }

    /**
     * Add filter to pass forgot url
     */
    public function changePasswordForgotUrl(): void
    {
        \add_filter('lostpassword_url', [$this, 'changePasswordForgotUrlFilter'], 10000);
    }

    /**
     * @param null $lostPasswordUrl
     * @param null $redirectTo
     * @return string
     */
    public function changePasswordForgotUrlFilter($lostPasswordUrl = null, $redirectTo = null): string
    {
        $redirectTo = $redirectTo ?? $_GET['redirect_to'] ?? null;

        return \get_site_url(1, '/wp-login.php?action=lostpassword' . ($redirectTo ? '&redirect_to=' . $redirectTo : ''));
    }

    /**
     * @param $result
     * @return mixed
     */
    public function registerWithEmail($result)
    {
        if (!empty($result['user_name']) && is_email($result['user_name'])) {
            unset($result['errors']->errors['user_name']);
        }

        return $result;
    }

    /**
     * Filter to allow hyphens on blognames to bypass wpmu_validate_blog_signup()
     *
     * From: https://wordpress.org/support/topic/how-to-allow-in-site-urls
     *
     * @author Jon Breitenbucher
     * @param array $result
     * @return array $result
     *
     * Original Author: Jon Breitenbucher
     * Adapted by Never Settle (neversettle.it) for use with the NS Cloner
     *
     */
    public function customBlogNameValidation($result): array
    {
        $olderrors = $result['errors'];

        // If Site Name ('blogname') is long enough,
        // and we have no error object, just return.
        if (!\is_object($olderrors)) {
            return $result;
        }

        // Build a new WP_Error Object to hold new errors.
        $errors = new \WP_Error();

        // Look for a 'blogname' $code error in this loop.
        foreach ($olderrors->errors as $code => $error) {
            if ($code === 'blogname') {
                // Sort the 'blogname' error $value with this loop.
                foreach ($error as $key => $value) {
                    // Switch each action based on the $error $value
                    // and our slected options.
                    switch ($value) {
                        case 'Website-Namen dürfen nur aus Kleinbuchstaben (a-z) und Zahlen bestehen.':
                            $ok_chars = '-_';

                            $pattern = '/^[a-z0-9]+([' . $ok_chars . ']?[a-z0-9]+)*$/';
                            preg_match($pattern, $result['blogname'], $match);

                            if ($result['blogname'] !== $match[0]) {
                                // Build a new error to add to the $errors object
                                // Allow Lowercase Letters
                                $ok_chars = __('Only the following Characters are allowed: lowercase letters (a-z), numbers (0-9), hyphen (-) and underscore (_).', 'supr-signup');

                                // Add the new error to the $errors object
                                $errors->add('blogname', $ok_chars);
                            }

                            break;

                        case 'That name is not allowed.':
                            // Do Nothing, just break
                            break;

                        case 'Site name must be at least 4 characters.':
                            // Do Nothing, just break
                            break;

                        case 'Sorry, site names may not contain the character “_”!':
                            // Do Nothing, just break
                            break;

                        case 'Sorry, you may not use that site name.':
                            // Do Nothing, just break
                            break;

                        case 'Sorry, site names must have letters too!':
                            // Do Nothing, just break
                            break;

                        case 'Sorry, that site is reserved!':
                            // Do Nothing, just break
                            break;
                        default:
                            $errors->add('blogname', $value);
                    } // end switch ($value)
                } // end foreach ($error as $key => $value)
            } else {
                // Add other errors to $error object from the nested arrays.
                foreach ($error as $key => $value) {
                    $errors->add($code, $value);
                }
            } // end if ($code == 'blogname')
        } // end foreach ($olderrors->errors as $code => $error)

        // Unset old errors object in $result
        unset($result['errors']);
        // Set new errors object in $result
        $result['errors'] = $errors;

        return $result;
    }
}
