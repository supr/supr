<?php

namespace SuprSignup;

/**
 * Class ResourceManager
 *
 * @package SuprSignup
 */
class ResourceManager
{
    /**
     * In oder to not enqueue two times
     *
     * @var bool
     */
    private static $registrationFormFilesEnqueued = false;

    /**
     * Register files for plugin
     */
    public static function addResources(): void
    {
        // Add css and js into footer
        \add_action('login_enqueue_scripts', [__CLASS__, 'enqueueLoginFiles'], 0);

        // Register css and js for reg form
        \add_action('init', [__CLASS__, 'registerRegistrationFormFiles']);
    }

    /**
     * Style plans list
     */
    public static function enqueuePricingTableCss(): void
    {
        if (isset($_GET['page']) && $_GET['page'] === 'wu-my-account') {
            \wp_enqueue_style('supr-signup-price-table', SUPR_SIGNUP_URL . 'css/price-table.css');
        }
    }

    /**
     * Add files
     */
    public static function enqueueLoginFiles(): void
    {
        // CSS
        \wp_enqueue_style('google-fonts-montserrat-300-400-500-600-700', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700');
        \wp_enqueue_style('supr-signup-style', SUPR_SIGNUP_URL . 'css/login.css');
        \wp_enqueue_style('supr-signup-wub-payment', SUPR_SIGNUP_URL . 'css/wub-checkout.css');
        // JS
        \wp_enqueue_script('supr-signup-wub-payment', SUPR_SIGNUP_URL . 'js/wub-checkout.js', ['jquery'], SUPR_SIGNUP_VERSION, false);
    }

    /**
     * Add new registration files
     */
    public static function registerRegistrationFormFiles(): void
    {
        \wp_register_style('supr-signup-registration-style', SUPR_SIGNUP_URL . 'css/supr-signup-registration.css');
        \wp_register_style('google-fonts-montserrat-300-400-500', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500');
        \wp_register_script('supr-signup-registration-script', SUPR_SIGNUP_URL . 'js/supr-signup-registration.js', ['jquery'], SUPR_SIGNUP_VERSION, false);
        \wp_register_script('supr-signup-registration-slug', SUPR_SIGNUP_URL . 'assets/slug/slug.js', [], SUPR_SIGNUP_VERSION, false);

        // Nice select
        \wp_register_style('supr-signup-registration-nice-select-style', SUPR_SIGNUP_URL . 'assets/nice-select/nice-select.min.css');
        \wp_register_script('supr-signup-registration-nice-select-js', SUPR_SIGNUP_URL . 'assets/nice-select/jquery.nice-select.min.js');

        // Load all files if it is a registration page
        $suprSignupRegPageSlug = \get_option('supr_signup_reg_page_slug');
        if (!empty($suprSignupRegPageSlug) && strpos($_SERVER['REQUEST_URI'], $suprSignupRegPageSlug) !== false) {
            \add_action('wp_enqueue_scripts', [self::class, 'enqueueRegistrationFormFiles']);
        }
    }

    /**
     * Add new registration files
     */
    public static function enqueueRegistrationFormFiles(): void
    {
        if (self::$registrationFormFilesEnqueued) {
            return;
        }

        \wp_enqueue_style('supr-signup-registration-style');
        \wp_enqueue_style('supr-signup-registration-fonts');
        \wp_enqueue_script('supr-signup-registration-script');
        \wp_enqueue_script('supr-signup-registration-slug');

        // We can already have a nice select
        if (!\wp_style_is('supr_admin_nice_select', 'enqueued') && !\wp_style_is('supr_theme_nice_select', 'enqueued')) {
            \wp_enqueue_script('supr-signup-registration-nice-select-js');
            \wp_enqueue_style('supr-signup-registration-nice-select-style');
        }

        self::$registrationFormFilesEnqueued = true;
    }
}
