<?php

namespace SuprSignup;

class TextDomain
{
    public static $domainName = 'supr-signup';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_SIGNUP_DIR_NAME . '/languages/');
    }
}
