<?php

namespace SuprSignup;

/**
 * Class PriceTable
 *
 * @package SuprSignup
 */
class PriceTable
{
    /**
     * Create pricetable items bases on the wpultimo layout
     *
     * @param $plan
     * @return string
     */
    public static function priceTableList($plan): string
    {
        $plan = strtolower($plan);
        $pricetable_list = '';
        $pricetable = [
            "starter" => [
                "store" => [
                    "title" => __('Online store', 'supr-signup'),
                    "notice" => __('including Website & Blog', 'supr-signup'),
                    "description" => '',
                    "class" => "list-item-store"
                ],
                "products" => [
                    "title" => __('Unlimited Products', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-products"
                ],
                "storage" => [
                    "title" => __('Unlimited Storage', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-storage"
                ],
                "media" => [
                    "title" => __('25 Media', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-media"
                ],
                "orders" => [
                    "title" => __('10 Orders/mo', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-orders"
                ],
                "designs" => [
                    "title" => __('1 Template', 'supr-signup'),
                    "notice" => '',
                    "description" => __('Select our smart or pro plan for more professional templates', 'supr-signup'),
                    "class" => "list-item-designs"
                ],
                "payments" => [
                    "title" => __('Only invoice', 'supr-signup'),
                    "notice" => '',
                    "description" => __('Select our smart or pro plan to use payment methods like credit card, paypal or direct debit', 'supr-signup'),
                    "class" => "list-item-payments"
                ],
                "features" => [
                    "title" => __('No Additional Features', 'supr-signup'),
                    "notice" => '',
                    "description" => __('Select our smart or pro plan to get access to additional features like wishlist, tracking and shipping', 'supr-signup'),
                    "class" => "list-item-features"
                ]
            ],
            "basic" => [
                "store" => [
                    "title" => __('Online store', 'supr-signup'),
                    "notice" => __('including Website & Blog', 'supr-signup'),
                    "description" => '',
                    "class" => "list-item-store"
                ],
                "products" => [
                    "title" => __('25 Products', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-products"
                ],
                "storage" => [
                    "title" => __('Unlimited Storage', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-storage"
                ],
                "media" => [
                    "title" => __('250 Media', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-media"
                ],
                "orders" => [
                    "title" => __('50 Orders/mo', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-orders"
                ],
                "designs" => [
                    "title" => __('1 Template', 'supr-signup'),
                    "notice" => '',
                    "description" => __('Select our smart or pro plan for more professional templates', 'supr-signup'),
                    "class" => "list-item-designs"
                ],
                "payments" => [
                    "title" => __('All payments available', 'supr-signup') . '¹²',
                    "notice" => __('Transaction fee 2,5% + 0,25€', 'supr-signup'),
                    "description" => __('Including credit card, paypal, direct debit, Klarna Sofort and cash in advance / Setup fee 29€', 'supr-signup'),
                    "class" => "list-item-payments"
                ],
                "features" => [
                    "title" => __('No Additional Features', 'supr-signup'),
                    "notice" => '',
                    "description" => __('Select our smart or pro plan to get access to additional features like wishlist, tracking and shipping', 'supr-signup'),
                    "class" => "list-item-features"
                ]
            ],
            "smart" => [
                "store" => [
                    "title" => __('Online store', 'supr-signup'),
                    "notice" => __('including Website & Blog', 'supr-signup'),
                    "description" => '',
                    "class" => "list-item-store"
                ],
                "products" => [
                    "title" => __('Unlimited Products', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-products"
                ],
                "storage" => [
                    "title" => __('Unlimited Storage', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-storage"
                ],
                "media" => [
                    "title" => __('Unlimited Media', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-media"
                ],
                "orders" => [
                    "title" => __('Unlimited Orders', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-orders"
                ],
                "designs" => [
                    "title" => __('All templates', 'supr-signup'),
                    "notice" => '',
                    "description" => __('Select our smart or pro plan for more professional templates', 'supr-signup'),
                    "class" => "list-item-designs"
                ],
                "payments" => [
                    "title" => __('All payments available', 'supr-signup') . '²',
                    "notice" => __('Transaction fee 2,0% + 0,25€', 'supr-signup'),
                    "description" => __('Including credit card, paypal, direct debit, Klarna Sofort and cash in advance', 'supr-signup'),
                    "class" => "list-item-payments"
                ],
                "features" => [
                    "title" => __('Additional Features', 'supr-signup'),
                    "notice" => '<a href="https://de.supr.com/apps/" target="_blank"><small>' . __('See all features', 'supr-signup') . '</small></a>',
                    "description" => __('Choose from many useful functions such as wishlist, tracking, shipping, design and marketing tools', 'supr-signup'),
                    "class" => "list-item-features"
                ],
                "support" => [
                    "title" => __('Email Support', 'supr-signup'),
                    "notice" => '',
                    "description" => __('We help you with all problems and questions by email', 'supr-signup'),
                    "class" => "list-item-support"
                ]/*,
            "domain" => [
                "title" => __('Custom Domain', 'supr-signup'),
                "notice" => '',
                "description" => __('Connect your own domain to your store', 'supr-signup'),
                "class" => "list-item-domain"
            ]*/
            ],
            "pro" => [
                "store" => [
                    "title" => __('Online store', 'supr-signup'),
                    "notice" => __('including Website & Blog', 'supr-signup'),
                    "description" => '',
                    "class" => "list-item-store"
                ],
                "products" => [
                    "title" => __('Unlimited Products', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-products"
                ],
                "storage" => [
                    "title" => __('Unlimited Storage', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-storage"
                ],
                "media" => [
                    "title" => __('Unlimited Media', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-media"
                ],
                "orders" => [
                    "title" => __('Unlimited Orders', 'supr-signup'),
                    "notice" => '',
                    "description" => '',
                    "class" => "list-item-orders"
                ],
                "designs" => [
                    "title" => __('All templates', 'supr-signup'),
                    "notice" => '',
                    "description" => __('Select our smart or pro plan for more professional templates', 'supr-signup'),
                    "class" => "list-item-designs"
                ],
                "payments" => [
                    "title" => __('All payments available', 'supr-signup') . '²',
                    "notice" => __('Transaction fee 1,5% + 0,25€', 'supr-signup'),
                    "description" => __('Including credit card, paypal, direct debit, Klarna Sofort and cash in advance', 'supr-signup'),
                    "class" => "list-item-payments"
                ],
                "features" => [
                    "title" => __('Additional Features', 'supr-signup'),
                    "notice" => '<a href="https://de.supr.com/apps/" target="_blank"><small>' . __('See all features', 'supr-signup') . '</small></a>',
                    "description" => __('Choose from many useful functions such as wishlist, tracking, shipping, design and marketing tools', 'supr-signup'),
                    "class" => "list-item-features"
                ],
                "support" => [
                    "title" => __('Email Support', 'supr-signup'),
                    "notice" => '',
                    "description" => __('We help you with all problems and questions by email and chat', 'supr-signup'),
                    "class" => "list-item-support"
                ]/*,
            "domain" => [
                "title" => __('Custom Domain', 'supr-signup'),
                "notice" => '',
                "description" => __('Connect your own domain to your store', 'supr-signup'),
                "class" => "list-item-domain"
            ],
            "custom-design" => [
                "title" => __('Individual Design', 'supr-signup') . '²',
                "notice" => '',
                "description" => __('In the pro plan you have the option to order an individual design from our designers²', 'supr-signup'),
                "class" => "list-item-domain"
            ]*/
            ]
        ];
        if ($plan === 'starter' || $plan === 'basic' || $plan === 'smart' || $plan === 'pro') {
            foreach ($pricetable[$plan] as $key => $value) {
                $pricetable_list .= '<li class="">' . $value['title'];
                $pricetable_list .= ($value['description'] ? '<span class="dashicons dashicons-editor-help" data-tippy-size="small" data-tippy="' . $value['description'] . '"></span>' : '');
                $pricetable_list .= ($value['notice'] ? '<small>' . $value['notice'] . '</small>' : '');
                $pricetable_list .= '</li>';
            }
        }

        return $pricetable_list;
    }
}
