<?php

// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

?>
<div class="wrap supr-signup">
    <h1 class="wp-heading-inline">
        <?= \get_admin_page_title(); ?>
    </h1>

    <?php
    // Save countdown options
    if (isset($_POST['supr_signup_legal_terms'])) {
        $message = ['type' => 'success', 'text' => __('Your settings have been saved', 'supr-signup')];

        \update_option('supr_signup_legal_terms', wp_kses_post($_POST['supr_signup_legal_terms']));
        \update_option('supr_signup_footer_content', wp_kses_post($_POST['supr_signup_footer_content']));
        \update_option('supr_signup_reg_page_slug', trim(sanitize_text_field($_POST['supr_signup_reg_page_slug'])));

        // Validate templates


        $templatesValid = true;
        $templates = [
            'names' => $_POST['supr_signup_templates']['names'],
            'ids' => $_POST['supr_signup_templates']['ids']
        ];

        // Trim all values
        $templates['names'] = array_map('trim', $templates['names']);
        $templates['ids'] = array_map('trim', $templates['ids']);
        // Remove empty values
        $templates['names'] = array_filter($templates['names']);
        $templates['ids'] = array_filter($templates['ids']);
        // Sanitize values with WP special function
        $templates['names'] = array_map('sanitize_text_field', $templates['names']);
        $templates['ids'] = array_map('sanitize_text_field', $templates['ids']);

        foreach ($templates['ids'] as $id) {
            if (\get_site($id) === null) {
                $templatesValid = false;
                $message = ['type' => 'error', 'text' => sprintf(__('Blog #%d doesn\'t exist', 'supr-signup'), $id)];
                break;
            }
        }

        if ($templatesValid) {
            \update_option('supr_signup_templates', $templates);
        }

        echo '<div class="notice notice-' . $message['type'] . ' inline"><p>' . $message['text'] . '</p></div>';
    }

    // Get options
    $suprSignupLegalTerms = stripslashes(\get_option('supr_signup_legal_terms', ''));
    $suprSignupFooterContent = stripslashes(\get_option('supr_signup_footer_content', ''));
    $suprSignupRegPageSlug = \get_option('supr_signup_reg_page_slug', '');
    $suprSignupTemplates = \get_option('supr_signup_templates', ['names' => [], 'ids' => []]);
    // Add one neu
    $suprSignupTemplates['names'][] = '';
    $suprSignupTemplates['ids'][] = '';

    ?>
    <form method="POST">
        <table class="form-table">
            <tbody>
            <tr>
                <th colspan="2"><?= __('Load speed', 'supr-signup'); ?></th>
            </tr>
            <tr>
                <td style="width: 30%;">
                    <?= __('Registration page slug', 'supr-signup') ?>
                </td>
                <td>
                    <input type="text" name="supr_signup_reg_page_slug" value="<?= $suprSignupRegPageSlug ?>" placeholder="registration">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <small>*<?= __('Please set the slug of the registration page. Then we can load styles from the very beginning, and the user will not see the form without styles.', 'supr-signup') ?></small>
                </td>
            </tr>
            <tr>
                <th colspan="2"><?= __('Templates', 'supr-signup'); ?></th>
            </tr>
            <?php foreach ($suprSignupTemplates['names'] as $num => $val) : ?>
                <tr>
                    <td>
                        <?= sprintf(__('Template %1', 'supr-signup'), $num + 1) ?>
                    </td>
                    <td>
                        <input type="text" name="supr_signup_templates[names][]" value="<?= $suprSignupTemplates['names'][$num] ?>" placeholder="Name"> - <input type="text" name="supr_signup_templates[ids][]" value="<?= $suprSignupTemplates['ids'][$num] ?>" placeholder="id">
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="2">
                    <small><?= sprintf(__('* In order to auto apply a template, you should provide a GET parameter <b>template_id</b> to registration: %s. Or a POST parameter <b>supr_signup_registration_form_template_id</b> with AJAX call.', 'supr-signup'), 'https://' . DOMAIN_CURRENT_SITE . '/registration/?template_id=8'); ?></small>
                </td>
            </tr>
            <tr>
                <th colspan="2"><?= __('Texts', 'supr-signup'); ?></th>
            </tr>
            <tr>
                <td>
                    <label for="supr_signup_legal_terms">
                        <?= __('Legal terms text', 'supr-signup'); ?>
                    </label>
                </td>
                <td>
                    <?php
                    $settings = array(
                        'wpautop' => false,
                        'media_buttons' => false,
                        'textarea_rows' => 2
                    );
                    \wp_editor($suprSignupLegalTerms, 'supr_signup_legal_terms', $settings);
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="supr_signup_footer_content">
                        <?= __('Footer (used in WP-Ultimo reg)', 'supr-signup'); ?> <small><?= __('* deprecated', 'supr-signup'); ?></small>
                    </label>
                </td>
                <td>
                    <?php
                    $settings = array(
                        'wpautop' => false,
                        'media_buttons' => false,
                        'textarea_rows' => 4
                    );
                    \wp_editor($suprSignupFooterContent, 'supr_signup_footer_content', $settings);
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php \submit_button(); ?>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
