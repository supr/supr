<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/** @var array $args */
/** @var string|null $error */
/** @var bool $showDomainField */
?>
<div id="supr-signup-registration-form">
    <!-- Signup progress layer -->
    <div id="supr-signup-registration-form-progress" class="supr-signup-registration-form-progress-overlay">
        <div class="supr-signup-registration-form-progress-content">
            <?php
            if (defined('DOMAIN_CURRENT_SITE') && !in_array(DOMAIN_CURRENT_SITE, ['s2devk.construction', 'myvikingshop.de'], true)) {
                echo '<img class="supr-signup-spinner" src="' . SUPR_SIGNUP_URL . 'img/supr-loader.gif"/>';
            } else {
                echo '<img class="supr-signup-spinner" src="' . SUPR_SIGNUP_URL . 'img/default-loader.gif"/>';
            }
            ?>
            <div class="supr-signup-registration-form-progress-content-message">
                <?= __('We are now configuring your ', 'supr-signup') ?>
                <span class="rotate"><?= __('shop, products, pages, design, settings, reports, dashboard, add-ons, e-mails, payment methods, shipping, SSL, subdomain', 'supr-signup') ?></span>
                <small><?= __('This can take up to one minute!', 'supr-signup') ?></small>
            </div>
        </div>
    </div>
    <!-- Signup progress layer END -->
    <div id="supr-signup-registration-form-errors">
        <?= ($error !== null) ? "<p class='error'>{$error}</p>" : '' ?>
    </div>
    <?php
    if (is_user_logged_in()) :
        $blogs = get_blogs_of_user(get_current_user_id());
        echo "<a class='signin' target='_parent' href='" . get_admin_url(count($blogs) > 0 ? reset($blogs)->userblog_id : 1) . "'>" . __('Login', 'supr-signup') . "</a>";
    else :
        $shopName = esc_html($_POST['supr_signup_registration_form_shop_name'] ?? $_GET['new'] ?? '');
        $shopSlug = esc_html($_POST['supr_signup_registration_form_shop_slug'] ?? \sanitize_title($shopName) ?? '');

        // Do we have templates configured?
        $templates = get_option('supr_signup_templates', ['names' => [], 'ids' => []]);
        $hasTemplates = count($templates['names']) > 0 && count($templates['ids']) > 0;
        $templateId = $_POST['supr_signup_registration_form_template_id'] ?? $_GET['template_id'] ?? null;
        $templateId = $templateId !== null ? (int)$templateId : null;

        // We don't show select if we have preselected template id
        $hideTemplateSelect = isset($_GET['template_id']);
        ?>
        <form method="POST" action="<?= get_site_url(null, '/registration') ?>?cs=<?= isset($_REQUEST['cs']) ? htmlspecialchars($_REQUEST['cs']) : uniqid('', true) ?><?= isset($_REQUEST['_ga']) ? '&_ga=' . htmlspecialchars($_REQUEST['_ga']) : '' ?>" target="_parent">
            <label>
                <span><?= __('Shopname', 'supr-signup') ?></span>
                <input type="text" name="supr_signup_registration_form_shop_name" value="<?= $shopName ?>">
            </label>
            <label style="<?= $showDomainField ? '' : 'display: none;' ?>">
                <span><?= __('Shopslug', 'supr-signup') ?></span>
                <input type="text" name="supr_signup_registration_form_shop_slug" value="<?= $shopSlug ?>">
            </label>
            <div class="slug">
                <strong id="supr-signup-shop-slug-container" data-default="<?= __('your site', 'supr-signup') ?>"><?= !empty($shopSlug) ? $shopSlug : __('your site', 'supr-signup') ?></strong>.<?= DOMAIN_CURRENT_SITE ?></div>
            <label>
                <span><?= __('Email', 'supr-signup') ?></span>
                <input type="email" name="supr_signup_registration_form_email" value="<?= esc_html($_POST['supr_signup_registration_form_email'] ?? '') ?>">
            </label>
            <label>
                <span><?= __('Password', 'supr-signup') ?></span>
                <input type="password" name="supr_signup_registration_form_password" value="">
            </label>
            <?php if ($hasTemplates) : ?>
                <?php if ($hideTemplateSelect && $templateId !== null && in_array($templateId, $templates['ids'], true)) : ?>
                    <input type="hidden" name="supr_signup_registration_form_template_id" value="<?= $templateId ?>">
                <?php else : ?>
                    <label for="supr_signup_registration_form_template_id" id="supr_signup_registration_form_template_id_label">
                        <span><?= __('Design template', 'supr-signup') ?></span>
                    </label>
                    <select name="supr_signup_registration_form_template_id" id="supr_signup_registration_form_template_id">
                        <?php foreach ($templates['names'] as $num => $val) : ?>
                            <option value="<?= $templates['ids'][$num] ?>"<?= $templateId !== null && $templates['ids'][$num] === $templateId ? ' selected' : '' ?>><?= $templates['names'][$num] ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="clear"></div>
                <?php endif; ?>
            <?php endif; ?>
            <label class="supr_signup_registration_form_age" style="display: none;">
                <span><?= __('Your age to check', 'supr-signup') ?></span>
                <input type="text" name="supr_signup_registration_form_age" value=""/>
            </label>
            <label class="agree_terms">
                <input name="supr_signup_registration_form_legal" type="checkbox" value="1">
                <?= stripslashes(get_blog_option(1, 'supr_signup_legal_terms', __('I accept terms and conditions', 'supr-signup'))) ?>
            </label>
            <button type="submit"><?= __('Register', 'supr-signup') ?></button>
        </form>
    <?php endif; ?>
</div>
