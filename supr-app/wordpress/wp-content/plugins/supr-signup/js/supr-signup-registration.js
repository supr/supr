jQuery(document).ready(function ($) {
    $('select').niceSelect();

    var textRotatorStarted = false;

    $('#supr-signup-registration-form input[name="supr_signup_registration_form_shop_name"]').keyup(function () {
        generateSlug($(this).val());
    });

    $('#supr-signup-registration-form input[name="supr_signup_registration_form_shop_slug"]').keyup(function () {
        generateSlug($(this).val());
    });

    var generateSlug = function (shopName) {
        "use strict";
        if (shopName == undefined) {
            shopName = $('#supr-signup-registration-form input[name="supr_signup_registration_form_shop_slug"]').val();
        }

        var shopSlug = slug(shopName).toLowerCase();
        $('#supr-signup-registration-form input[name="supr_signup_registration_form_shop_slug"]').val(shopSlug);
        $('#supr-signup-shop-slug-container').html(shopSlug);

        // Set default value
        if (shopSlug == '') {
            $('#supr-signup-shop-slug-container').html($('#supr-signup-shop-slug-container').data('default'));
        }
    };

    // On submit
    $('#supr-signup-registration-form form').on("submit", function (e) {
        e.preventDefault(); // stop the actual submission

        // Start text rotator
        if (textRotatorStarted === false) {
            $(".supr-signup-registration-form-progress-content-message .rotate").textrotator({animation: "flipUp", speed: 2500});
            textRotatorStarted = true;
        }

        // Send data per ajax
        var postData = $(this).serialize();
        // Add flag: it is ajax
        postData = postData + '&ajax=1';

        $.ajax({
            type: 'POST',
            url: this.action,
            data: postData,
            beforeSend: function () {
                // Hide errors
                $('#supr-signup-registration-form-errors').hide();
                // Show load layer
                $('#supr-signup-registration-form-progress').show();
            },
            success: function (response) {
                // Redirect to admin
                if (response.success) {
                    if (response.data.redirect !== undefined) {
                        // Redirect
                        window.top.location.href = response.data.redirect;
                    } else {
                        console.log('[SUPR SIGNUP] Reg is success, but there is not any redirect:');
                        console.log(response);
                    }
                }

                // Show error
                if (!response.success) {
                    // Hide animation
                    $('#supr-signup-registration-form-progress').hide();

                    if (response.data.error !== undefined) {
                        $('#supr-signup-registration-form-errors').html('');
                        $('#supr-signup-registration-form-errors').append($('<p />').addClass('error').html(response.data.error));
                        $('#supr-signup-registration-form-errors').show();
                    } else {
                        console.log('[SUPR SIGNUP] There is an error, but there is not any message:');
                        console.log(response);
                    }
                }
            },
            complete: function () {
                // Hide animation
                $('#supr-signup-registration-form-progress').hide();
            }
        });

        // Don't submit the form
        return false;
    });

});


/* ===========================================================
 * jquery-simple-text-rotator.js v1
 * ===========================================================
 * Copyright 2013 Pete Rojwongsuriya.
 * http://www.thepetedesign.com
 *
 * A very simple and light weight jQuery plugin that
 * allows you to rotate multiple text without changing
 * the layout
 * https://github.com/peachananr/simple-text-rotator
 *
 * ========================================================== */

!function ($) {

    var defaults = {
        animation: "dissolve",
        separator: ",",
        speed: 2000
    };

    $.fx.step.textShadowBlur = function (fx) {
        $(fx.elem).prop('textShadowBlur', fx.now).css({textShadow: '0 0 ' + math.floor(fx.now) + 'px black'});
    };

    $.fn.textrotator = function (options) {
        var settings = $.extend({}, defaults, options);

        return this.each(function () {
            var el = $(this);
            var array = [];
            $.each(el.text().split(settings.separator), function (key, value) {
                array.push(value);
            });
            el.text(array[0]);

            // animation option
            var rotate = function () {
                switch (settings.animation) {
                    case 'dissolve':
                        el.animate({
                            textShadowBlur: 20,
                            opacity: 0
                        }, 500, function () {
                            index = $.inArray(el.text(), array);
                            if ((index + 1) == array.length) {
                                index = -1;
                            }
                            el.text(array[index + 1]).animate({
                                textShadowBlur: 0,
                                opacity: 1
                            }, 500);
                        });
                        break;

                    case 'flip':
                        if (el.find(".back").length > 0) {
                            el.html(el.find(".back").html())
                        }

                        var initial = el.text();
                        var index = $.inArray(initial, array);
                        if ((index + 1) == array.length) {
                            index = -1;
                        }

                        el.html("");
                        $("<span class='front'>" + initial + "</span>").appendTo(el);
                        $("<span class='back'>" + array[index + 1] + "</span>").appendTo(el);
                        el.wrapInner("<span class='rotating' />").find(".rotating").hide().addClass("flip").show().css({
                            "webkitTransform": " rotateY(-180deg)",
                            "mozTransform": " rotateY(-180deg)",
                            "OTransform": " rotateY(-180deg)",
                            "transform": " rotateY(-180deg)"
                        });

                        break;

                    case 'flipUp':
                        if (el.find(".back").length > 0) {
                            el.html(el.find(".back").html())
                        }

                        var initial = el.text();
                        var index = $.inArray(initial, array);
                        if ((index + 1) == array.length) {
                            index = -1;
                        }

                        el.html("");
                        $("<span class='front'>" + initial + "</span>").appendTo(el);
                        $("<span class='back'>" + array[index + 1] + "</span>").appendTo(el);
                        el.wrapInner("<span class='rotating' />").find(".rotating").hide().addClass("flip up").show().css({
                            "webkitTransform": " rotateX(-180deg)",
                            "mozTransform": " rotateX(-180deg)",
                            "OTransform": " rotateX(-180deg)",
                            "transform": " rotateX(-180deg)"
                        });

                        break;

                    case 'flipCube':
                        if (el.find(".back").length > 0) {
                            el.html(el.find(".back").html())
                        }

                        var initial = el.text();
                        var index = $.inArray(initial, array);
                        if ((index + 1) == array.length) {
                            index = -1;
                        }

                        el.html("");
                        $("<span class='front'>" + initial + "</span>").appendTo(el);
                        $("<span class='back'>" + array[index + 1] + "</span>").appendTo(el);
                        el.wrapInner("<span class='rotating' />").find(".rotating").hide().addClass("flip cube").show().css({
                            "webkitTransform": " rotateY(180deg)",
                            "mozTransform": " rotateY(180deg)",
                            "OTransform": " rotateY(180deg)",
                            "transform": " rotateY(180deg)"
                        });

                        break;

                    case 'flipCubeUp':
                        if (el.find(".back").length > 0) {
                            el.html(el.find(".back").html())
                        }

                        var initial = el.text();
                        var index = $.inArray(initial, array);
                        if ((index + 1) == array.length) {
                            index = -1;
                        }

                        el.html("");
                        $("<span class='front'>" + initial + "</span>").appendTo(el);
                        $("<span class='back'>" + array[index + 1] + "</span>").appendTo(el);
                        el.wrapInner("<span class='rotating' />").find(".rotating").hide().addClass("flip cube up").show().css({
                            "webkitTransform": " rotateX(180deg)",
                            "mozTransform": " rotateX(180deg)",
                            "OTransform": " rotateX(180deg)",
                            "transform": " rotateX(180deg)"
                        });

                        break;

                    case 'spin':
                        if (el.find(".rotating").length > 0) {
                            el.html(el.find(".rotating").html())
                        }
                        index = $.inArray(el.text(), array);
                        if ((index + 1) == array.length) {
                            index = -1;
                        }

                        el.wrapInner("<span class='rotating spin' />").find(".rotating").hide().text(array[index + 1]).show().css({
                            "webkitTransform": " rotate(0) scale(1)",
                            "mozTransform": "rotate(0) scale(1)",
                            "OTransform": "rotate(0) scale(1)",
                            "transform": "rotate(0) scale(1)"
                        });
                        break;

                    case 'fade':
                        el.fadeOut(settings.speed, function () {
                            index = $.inArray(el.text(), array);
                            if ((index + 1) == array.length) {
                                index = -1;
                            }
                            el.text(array[index + 1]).fadeIn(settings.speed);
                        });
                        break;
                }
            };
            setInterval(rotate, settings.speed);
        });
    }

}(window.jQuery);


