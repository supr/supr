jQuery(document).ready(function ($) {
    "use strict";
    activate_payment_form_listener();

    /**
     * Validate and hide/show tabs on the payment step
     */
    function activate_payment_form_listener()
    {
        // payment, checkout, billwerk page toggle buttons

        $('.toggle-link').live('click', function () {
            var target_elm = $(this).data('target');
            var text_cache = $(this).data('toggle-text');

            $(target_elm).slideToggle('slow');
            $(this).data('toggle-text', $(this).text());
            $(this).text(text_cache);
        });

        // We check if all fields have any info. If not all, we open tris tab. We need delay because WP-Ultimo-billwerk plugin does it with JS
        setTimeout(function () {
            if ($('#order-summary-address').length) {
                var order_summary_required_address = [
                    '#first_name', '#last_name', '#email', '#street', '#house_number', '#zip', '#city'
                ];
                var order_summary_required_status = true;
                $.each(order_summary_required_address, function (index, value) {
                    if (!$(value).val()) {
                        order_summary_required_status = false;
                    }
                });

                if (!order_summary_required_status) {
                    $('#toggle-link-order-summary-address').click();
                }
            }
        }, 500);
    }
});