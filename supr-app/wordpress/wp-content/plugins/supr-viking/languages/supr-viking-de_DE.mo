��          L      |       �   �   �      [     i     z     �  �  �  �   B          $     5     L                                         <strong>Note:</strong> You cannot log in with your <strong>viking.de</strong> account details but need a separate account. <a href="/login?action=register">Register for free</a> SUPR - Viking SUPR Development Viking Modification https://supr.com Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: SUPR - Viking
PO-Revision-Date: 2020-02-26 13:50+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: supr-viking.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: de_DE
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 <strong>Hinweis:</strong> Sie können sich nicht mit Ihren <strong>viking.de</strong> Zugangdaten einloggen sondern benötigen einen separaten Account. <a href="/login?action=register">Kostenlos registrieren</a> SUPR - Viking SUPR Development Viking Modifizierungen https://supr.com 