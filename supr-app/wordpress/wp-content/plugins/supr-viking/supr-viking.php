<?php
/**
 * Plugin Name: SUPR - Viking
 * Plugin URI: https://supr.com
 * Description: Viking Modification
 * Text Domain: supr-viking
 * Domain Path: /languages
 * Version: 1.0
 * Author: SUPR Development
 * License: GPL
 */

define('SUPR_VIKING_PATH', plugin_dir_path(__FILE__));
define('SUPR_VIKING_URL', plugins_url('/', __FILE__));
define('SUPR_VIKING_FILE', plugin_basename(__FILE__));
define('SUPR_VIKING_DIR_NAME', basename(__DIR__));

define('SUPR_VIKING_VERSION', '1.0.0');

// Load main Class
require SUPR_VIKING_PATH . 'includes/Plugin.php';
