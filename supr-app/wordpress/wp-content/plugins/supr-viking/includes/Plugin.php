<?php

namespace SuprViking;

class Plugin
{
    /**
     * @var Plugin
     */
    private static $instance;

    /**
     * @return Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Init
     */
    public function adminInit(): void
    {
        \add_action('admin_enqueue_scripts', [ResourceManager::class, 'addAdminResources'], 991);
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // Load translations
        \add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        \add_action('admin_init', [$this, 'adminInit'], 0);
        \add_action('login_form', [Login::class, 'footerNotice']);
        \add_action('login_enqueue_scripts', [ResourceManager::class, 'addLoginResource'], 0);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_VIKING_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::instance();
