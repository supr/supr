<?php

namespace SuprViking;

class Login
{
    public static function footerNotice(): void
    {
        echo '<p class="viking-login-notice">' . __(
            '<strong>Note:</strong> You cannot log in with your <strong>viking.de</strong> account details but need a separate account. <a href="/registration/">Register for free</a>',
            'supr-viking'
        ) . '</p>';
    }
}
