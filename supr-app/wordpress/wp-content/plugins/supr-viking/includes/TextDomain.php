<?php

namespace SuprViking;

class TextDomain
{
    public static $domainName = 'supr-viking';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_VIKING_DIR_NAME . '/languages/');
    }
}
