<?php

namespace SuprViking;

use function wp_enqueue_style;

/**
 * Class ResourceManager
 *
 * @package SuprViking
 */
class ResourceManager
{
    /**
     * Add admin resources
     */
    public static function addAdminResources(): void
    {
        // CSS
        \wp_enqueue_style('supr_admin_viking_style', SUPR_VIKING_URL . 'scss/viking-admin.css', [], SUPR_VIKING_VERSION, 'all');
    }

    /**
     * Add login resources
     */
    public static function addLoginResource(): void
    {
        // CSS
        \wp_enqueue_style('supr_login_viking_style', SUPR_VIKING_URL . 'scss/viking-login.css', [], SUPR_VIKING_VERSION, 'all');
    }
}
