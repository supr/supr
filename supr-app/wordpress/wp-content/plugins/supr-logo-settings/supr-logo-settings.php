<?php
/**
 * Plugin Name: SUPR - Logo settings
 * Plugin URI: https://supr.com
 * Description: Ability to set the logo and favicon
 * Text Domain: supr-logo-settings
 * Domain Path: /languages
 * Version: 1.1
 * Author: SUPR Development
 * License: GPL
 */

// Load translations
add_action('plugins_loaded', 'supr_logo_settings_plugin_textdomain');

/**
 * Load text domain
 */
function supr_logo_settings_plugin_textdomain()
{
    load_plugin_textdomain('supr-logo-settings', false, basename(__DIR__) . '/languages/');
}

// Add menu page
add_action('admin_menu', 'add_logo_settings_page');

/**
 * Include in menu a new item
 */
function add_logo_settings_page()
{
    add_submenu_page(
        'themes.php',
        __('Logo', 'supr-logo-settings'),
        __('Logo', 'supr-logo-settings'),
        'manage_options',
        'supr_logo_settings',
        'supr_logo_settings_page_output'
    );
}

// Show default icon from main blog if not exist
add_filter('get_site_icon_url', 'supr_logo_settings_site_icon_url', 10, 3);

/**
 * Show default icon from main blog if not exist
 *
 * @param $url
 * @param $size
 * @param $blog_id
 * @return mixed
 */
function supr_logo_settings_site_icon_url($url, $size, $blog_id)
{
    // If exist, return
    if (!empty($url)) {
        return $url;
    }

    // Try to get property
    $siteFaviconId = \get_option('site_icon', '');
    if (!empty($siteFaviconId)) {
        $siteFaviconImg = \wp_get_attachment_image_src($siteFaviconId, 'thumbnail', true)[0] ?? $url;
    } else {
        $siteFaviconImg = (isset($_SERVER['HTTPS']) ? 'https' : 'http') . "://{$_SERVER['HTTP_HOST']}/favicon.ico";

        // It works too slow :(
        // Get from main blog
        /*switch_to_blog(1);
        $siteFaviconId = get_option('site_icon', '');
        $siteFaviconImg = wp_get_attachment_image_src($siteFaviconId, 'thumbnail', true)[0] ?? $url;
        restore_current_blog();*/
    }

    return $siteFaviconImg;
}

////////////////////////////////////////////////////////////////
/// START SETTINGS PAGE: /wp-admin/themes.php?page=supr_logo_settings
////////////////////////////////////////////////////////////////

// Load additional scripts for settings page
if (isset($_GET['page']) && $_GET['page'] === 'supr_logo_settings') {
    // Register own js and style on initialization
    add_action('admin_init', 'supr_logo_settings_register_script');

    function supr_logo_settings_register_script()
    {
        // We need JS Media API
        wp_enqueue_media();
        wp_register_style('supr_logo_settings_style', plugins_url('css/supr-logo-settings.css', __FILE__), false, '1.0.0', 'all');
        wp_register_script('supr_logo_settings_js', plugins_url('js/supr-logo-settings.js', __FILE__), ['jquery'], '1.0.0', true);
    }

    // Use the registered jquery and style above
    add_action('admin_enqueue_scripts', 'supr_logo_settings_enqueue_style');

    function supr_logo_settings_enqueue_style()
    {
        wp_enqueue_style('supr_logo_settings_style');
        wp_enqueue_script('supr_logo_settings_js');
    }

    function supr_logo_settings_page_output()
    {
        // Validate and save post data
        supr_logo_settings_handle_post_request();
        ?>
        <div class="wrap supr-logo-settings">
            <h1 class="wp-heading-inline"><?= get_admin_page_title(); ?></h1>
            <?php supr_logo_settings_print_form(); ?>
        </div>
        <?php
    }

    // Register own js and style on initialization
    add_action('admin_init', 'supr_logo_settings_add_js_variables');

    /**
     * Add js variables to js script
     */
    function supr_logo_settings_add_js_variables(): void
    {
        $translation_array = [
            'delete_element' => __('Are you sure you want to delete?', 'supr-logo-settings'),
            'delete_element_ok' => __('Yes', 'supr-logo-settings'),
            'delete_element_cancel' => __('No', 'supr-logo-settings')
        ];

        wp_localize_script('supr_logo_settings_js', 'supr_logo_settings_values', $translation_array);
    }

    /**
     * Print form of settings
     */
    function supr_logo_settings_print_form()
    {
        $queryParams = ['page' => 'supr_logo_settings'];

        $siteLogoId = get_theme_mod('custom_logo', '');
        $siteLogoImg = !empty($siteLogoId) ? wp_get_attachment_image_src($siteLogoId, 'full', true)[0] : '';

        $siteFaviconId = get_option('site_icon', '');
        $siteFaviconImg = $siteFaviconId ? wp_get_attachment_image_src($siteFaviconId, 'thumbnail', true)[0] : '';

        ?>
        <form method="POST" id="supr-logo-settings-form" action="?<?= http_build_query(array_merge($queryParams, ['action' => 'update'])); ?>">
            <div class="postbox">
                <div class="inside">
                    <div class="row">
                        <div class="wu-col-sm-12">
                            <h3><?= __('Store logo', 'supr-logo-settings'); ?></h3>
                            <p>
                                <?= __('Give your store an iconic signature. <strong>Your logo!</strong>', 'supr-logo-settings'); ?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="wu-col-md-5">
                            <div class="logo-holder">
                                <img id="supr-logo-settings-select-logo-image" src="<?= $siteLogoImg; ?>" style="display: <?= !empty($siteLogoImg) ? 'block' : 'none'; ?>;">
                            </div>
                        </div>
                        <div class="wu-col-md-7">
                            <input id="supr-logo-settings-select-logo-input" type="hidden" name="img_logo" value="<?= $siteLogoId; ?>">
                            <button id="supr-logo-settings-select-logo-button" class="button button-primary"><?= __('Select shoplogo', 'supr-logo-settings'); ?></button>
                            <br>
                            <a id="supr-logo-settings-delete-logo" href="?<?= http_build_query(array_merge($queryParams, ['action' => 'delete_logo'])) ?>" class="button button-secondary supr-elementor-template-preview"><?= __('Remove', 'supr-logo-settings'); ?></a>
                        </div>
                    </div>
                </div>

                <hr>
                <div class="inside">
                    <div class="row">
                        <div class="wu-col-sm-12">
                            <h3><?= __('Favicon', 'supr-logo-settings'); ?></h3>
                            <p>
                                <?= __('If your store makes it into the bookmarks of your customer\'s browser, then you should reward this with a nice favicon', 'supr-logo-settings'); ?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="wu-col-md-5">
                            <div class="favicon-holder">
                                <img id="supr-logo-settings-select-favicon-image" src="<?= $siteFaviconImg; ?>" style="display: <?= !empty($siteFaviconImg) ? 'block' : 'none'; ?>;">
                            </div>
                        </div>
                        <div class="wu-col-md-7">
                            <input id="supr-logo-settings-select-favicon-input" type="hidden" name="img_favicon" value="<?= $siteFaviconId; ?>">
                            <button id="supr-logo-settings-select-favicon-button" class="button button-primary"><?= __('Select favicon', 'supr-logo-settings'); ?></button>
                            <br>
                            <a id="supr-logo-settings-delete-logo" href="?<?= http_build_query(array_merge($queryParams, ['action' => 'delete_favicon'])) ?>" class="button button-secondary supr-elementor-template-preview"><?= __('Remove', 'supr-logo-settings') ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="postbox postbox-bar">
                <div class="inside">
                    <input type="submit" class="button button-primary supr-elementor-template-activate" value="<?= __('Save changes', 'supr-logo-settings'); ?>">
                </div>
            </div>
        </form>
        <?php
    }

    /**
     * Validate and save POST data in blog options
     */
    function supr_logo_settings_handle_post_request()
    {
        if (!isset($_GET['action'])) {
            return;
        }

        switch ($_GET['action']) {
            case 'delete_favicon':
                update_option('site_icon', '');
                break;
            case 'delete_logo':
                set_theme_mod('custom_logo', '');
                break;
            case 'update':
                if (isset($_POST['img_logo'])) {
                    set_theme_mod('custom_logo', (int)$_POST['img_logo']);
                }
                if (isset($_POST['img_favicon'])) {
                    update_option('site_icon', (int)$_POST['img_favicon']);
                }
                break;
        }
    }
}