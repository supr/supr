��          �       <      <  #   =      a     �  @   �  u   �     A     F     I     P     e     v     �     �  
   �  $   �     �     �  �   �  #   �  *   �     �  L   �  {   4     �     �  	   �     �     �     �               (  +   1     ]     `   Ability to set the logo and favicon Are you sure you want to delete? Favicon Give your store an iconic signature. <strong>Your logo!</strong> If your store makes it into the bookmarks of your customer's browser, then you should reward this with a nice favicon Logo No Remove SUPR - Logo settings SUPR Development Save changes Select favicon Select shoplogo Store logo Unable to find plugin Elementor Pro. Yes https://supr.com MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Project-Id-Version: supr-logo-settings
Language: de
 Ability to set the logo and favicon Bist Du sicher, dass Du fortfahren willst? Favicon Gib Deinem Shop ein unverwechselbares Markenzeichen mit Deinem eigenen Logo. Wenn es Dein Shop in die Favoriten-Liste Deiner Kunden schafft, dann solltest Du das mit einem entsprechenden Icon belohnen Logo Nein Entfernen SUPR - Logo Einstellungen SUPR Development Änderungen speichern Favicon auswählen Logo auswählen Shoplogo Elementor Pro konnte nicht gefunden werden. Ja https://at.supr.com 