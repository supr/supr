(function ($) {
    // Select image button
    $('#supr-logo-settings-select-logo-button, #supr-logo-settings-select-favicon-button').click(function (event) {
        event.preventDefault();
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this);
        var input_image = button.attr('id').replace('-button', '-image');
        var input_input = button.attr('id').replace('-button', '-input');

        wp.media.editor.send.attachment = function (props, attachment) {
            $("#" + input_input).val(attachment.id);
            $("#" + input_image).attr('src', attachment.url).show();
            wp.media.editor.send.attachment = send_attachment_bkp;
        };

        wp.media.editor.open(button);
        return false;
    });

    // Ask if you want really delete
    $('#supr-logo-settings-delete-logo, #supr-logo-settings-delete-logo').click(function () {
        var delete_url = $(this).attr('href');

        Swal.fire({
            title: supr_logo_settings_values.delete_element,
            icon: 'info',
            customClass: 'animated zoomIn faster',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: true,
            confirmButtonText: supr_logo_settings_values.delete_element_ok,
            cancelButtonText: supr_logo_settings_values.delete_element_cancel,
            showClass: {
                popup: '',
                backdrop: '',
                icon: '',
            },
            hideClass: {
                popup: '',
                backdrop: '',
                icon: '',
            }
        }).then(function (object) {
            if (object.value) {
                window.location.href = delete_url;
            }
        });

        return false;
    });
})(jQuery);