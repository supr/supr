<?php
/**
 * Plugin Name: SUPR - Reporting API
 * Plugin URI: https://supr.com
 * Description: API to get data for reporting
 * Text Domain: supr-reporting-api
 * Domain Path: /languages
 * Version: 1.5.1
 * Author: SUPR Development
 * License: GPL
 */

define('SUPR_REPORTING_API_PATH', plugin_dir_path(__FILE__));
define('SUPR_REPORTING_API_URL', plugins_url('/', __FILE__));
define('SUPR_REPORTING_API_FILE', plugin_basename(__FILE__));
define('SUPR_REPORTING_API_DIR_NAME', basename(__DIR__));

// It should be declared in wp-config.php
if (!defined('SUPR_REPORTING_AUTH_SECRET_KEY')) {
    define('SUPR_REPORTING_AUTH_SECRET_KEY', 'KgbB9Vn9MI*sCch?vP],]in%Oj/A%+JO<+}b8%/61bNDGTerFPepna 2)-b`&~9s');
}

define('SUPR_REPORTING_API_VERSION', '1.5.1');

// Load main Class
require SUPR_REPORTING_API_PATH . 'includes/Plugin.php';

// After activation of plugin
register_activation_hook(__FILE__, [\SuprReportingApi\DbManager::instance(), 'createTable']);
