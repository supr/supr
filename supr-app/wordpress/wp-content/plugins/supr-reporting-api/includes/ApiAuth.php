<?php

namespace SuprReportingApi;

class ApiAuth
{
    /**
     * Create auth token
     *
     * @param \WP_REST_Request $request
     * @throws \Exception
     */
    public static function auth($request): void
    {
        $secretKey = SUPR_REPORTING_AUTH_SECRET_KEY;

        $username = $request->get_param('username');
        $password = $request->get_param('password');

        // Try to authenticate the user with the passed credentials
        $user = \wp_authenticate($username, $password);

        // If the authentication fails return a error
        if (!$user instanceof \WP_User) {
            \wp_send_json_error('Password or username is false.', 401);
        }

        // Check capability
        if (!\user_can($user, 'manage_options')) {
            \wp_send_json_error('Access denied.', 403);
        }

        // Try to get token
        $token = AuthTokenRepository::getAuthTokenByUserId($user->ID);

        if ($token === null) {
            // Clear trash
            AuthTokenRepository::removeOldAuthTokens();

            // Create new token
            $token = new AuthToken();
            $token->setBlogId(\get_current_blog_id());
            $token->setUserId($user->ID);
            $token->setToken(\md5($user->ID . $secretKey . \microtime()));
        }

        // Update changed time (in order to not expire)
        AuthTokenRepository::saveAuthToken($token);

        \wp_send_json_success(
            [
                'user' => $token->getUserId(),
                'token' => $token->getToken()
            ]
        );
    }

    /**
     * Check auth token
     *
     * @return bool
     * @throws \Exception
     */
    public static function checkAuth(): bool
    {
        $tokenString = self::getBearerToken();

        $token = AuthTokenRepository::getAuthTokenByToken($tokenString);

        if ($token === null) {
            \wp_send_json_error('You are not authorized.', 401);
        }

        // Update changed time (in order to not expire)
        AuthTokenRepository::saveAuthToken($token);

        return true;
    }

    /**
     * Get header Authorization
     *
     * @return null|string
     */
    private static function getAuthorizationHeader(): ?string
    {
        $headers = null;

        if (isset($_SERVER['Authorization'])) {
            $headers = \trim($_SERVER["Authorization"]);
        } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $headers = \trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (\function_exists('apache_request_headers')) {
            $requestHeaders = \apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = \array_combine(array_map('ucwords', \array_keys($requestHeaders)), \array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = \trim($requestHeaders['Authorization']);
            }
        }

        return $headers;
    }

    /**
     * Get access token from header
     *
     * @return null|string
     */
    private static function getBearerToken(): ?string
    {
        $headers = self::getAuthorizationHeader();

        // HEADER: Get the access token from the header
        if (!empty($headers) && preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
            return $matches[1];
        }

        return null;
    }
}
