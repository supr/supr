<?php

namespace SuprReportingApi;

/**
 * Class Page
 *
 * @package SuprReportingApi
 */
class Page
{
    /**
     * Show setting page
     */
    public static function settingsPage(): void
    {
        include SUPR_REPORTING_API_PATH . 'templates/network-settings-page.php';
    }
}
