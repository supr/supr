<?php

namespace SuprReportingApi;

/**
 * Class ChangedBlog
 *
 * @package SuprReportingApi
 */
class ChangedBlog
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $blogId;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var \DateTime
     */
    private $dateModified;

    /**
     * Order constructor.
     *
     * @param array $data
     */
    public function __construct($data = [])
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['blog_id'])) {
            $this->setBlogId($data['blog_id']);
        }
        if (isset($data['date_created'])) {
            $this->setDateCreated($data['date_created']);
        }
        if (isset($data['date_modified'])) {
            $this->setDateModified($data['date_modified']);
        }
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'id' => $this->getId(),
            'blog_id' => $this->getBlogId(),
            'date_created' => $this->getDateCreated() ? $this->getDateCreated()->format(\DateTime::ATOM) : null,
            'date_modified' => $this->getDateModified() ? $this->getDateModified()->format(\DateTime::ATOM) : null,
        ];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id): void
    {
        $this->id = (int)$id;
    }

    /**
     * @return int
     */
    public function getBlogId(): int
    {
        return $this->blogId;
    }

    /**
     * @param $blogId
     */
    public function setBlogId($blogId): void
    {
        $this->blogId = (int)$blogId;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateCreated(): ?\DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param $dateCreated
     */
    public function setDateCreated($dateCreated): void
    {
        $this->dateCreated = DbManager::convertToDateTime($dateCreated);
    }

    /**
     * @return \DateTime|null
     */
    public function getDateModified(): ?\DateTime
    {
        return $this->dateModified;
    }

    /**
     * @param $dateModified
     */
    public function setDateModified($dateModified): void
    {
        $this->dateModified = DbManager::convertToDateTime($dateModified);
    }
}
