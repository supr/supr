<?php

namespace SuprReportingApi;

/**
 * Class Menu
 *
 * @package SuprReportingApi
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add menu page
        add_action('network_admin_menu', [__CLASS__, 'createMenu']);

        // Add settings link on plugin page
        add_filter('network_admin_plugin_action_links_' . SUPR_REPORTING_API_FILE, [__CLASS__, 'addSettingsPage']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createMenu(): void
    {
        add_menu_page(
            __('Reporting API', 'supr-reporting-api'),
            __('Reporting API', 'supr-reporting-api'),
            'manage_network',
            'supr_reporting_api_options',
            [Page::class, 'settingsPage'],
            'dashicons-rest-api'
        );
    }

    /**
     * @param array $links
     * @return mixed
     */
    public static function addSettingsPage($links): array
    {
        $settings_link = '<a href="admin.php?page=supr_reporting_api_options">' . __('Settings', 'supr-reporting-api') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
