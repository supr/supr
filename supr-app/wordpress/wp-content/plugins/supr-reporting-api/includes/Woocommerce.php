<?php

namespace SuprReportingApi;

/**
 * Class Woocommerce
 *
 * @package SuprReportingApi
 */
class Woocommerce
{
    /**
     * Add checkbox "test order" for admins only
     *
     * @param \WC_Checkout $checkout
     */
    public static function addTestOrderCheckboxToCheckout(\WC_Checkout $checkout): void
    {
        // Add checkbox only for admins
        if (\current_user_can('manage_options')) {
            echo '<div id="supr-repoting-api__is-test-order">';

            \woocommerce_form_field('is_test_order', [
                'type' => 'checkbox',
                'label' => __('This is a test order. This check box is visible only to administrators.', 'supr-reporting-api'),
                'default' => 1
            ], $checkout->get_value('is_test_order'));

            echo '</div>';
        }
    }

    /**
     * Save: test order or real order
     *
     * @param $orderId
     */
    public static function processTestOrderCheckboxField($orderId): void
    {
        // Save true or false
        \update_post_meta($orderId, 'is_test_order', filter_var($_POST['is_test_order'], FILTER_VALIDATE_BOOLEAN));
    }

    /**
     * Display checkbox "test order" on the order edit page
     *
     * @param \WC_Order $order
     */
    public static function displayTestOrderCheckboxInOrderDetails($order): void
    {
        $isTestOrder = \get_post_meta($order->get_id(), 'is_test_order', true);

        echo '<p class="form-field is-test-order-field"><label for="is_test_order" class="' . ($isTestOrder ? 'test-order' : 'real-order') . '"><input type="checkbox" name="is_test_order" id="is_test_order" value="1"' . ($isTestOrder ? ' checked' : '') . '>' . __('This is a test order', 'supr-reporting-api') . '</label></p>';
    }
}
