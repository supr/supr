<?php

namespace SuprReportingApi;

/**
 * Class ApiTranslator
 *
 * This class translate unserialized data to format needed for KPI cron
 *
 * @package SuprReportingApi
 */
class ApiTranslator
{
    /**
     * @param array $orderData
     * @return array
     */
    public static function translateOrderData($orderData): array
    {
        $orderData['items_tax'] = $orderData['cart_tax'];
        $orderData['shop_id'] = $orderData['blog_id'];

        foreach ($orderData['order_items'] as $num => $orderItem) {
            if ((float)$orderItem['total'] > 0 && (float)$orderItem['total_tax'] > 0) {
                $orderData['order_items'][$num]['tax_rate'] = (float)$orderItem['total_tax'] / (float)$orderItem['total'];
            } else {
                $orderData['order_items'][$num]['tax_rate'] = 0;
            }

            $orderData['order_items'][$num]['tax_rate'] = (string)\round($orderData['order_items'][$num]['tax_rate'], 2);
            $orderData['order_items'][$num]['variation_title'] = $orderData['order_items'][$num]['variation_name'];
            unset($orderData['order_items'][$num]['variation_name']);
        }

        unset($orderData['cart_tax'], $orderData['blog_id']);

        return $orderData;
    }
}
