<?php

namespace SuprReportingApi;

class TextDomain
{
    public static $domainName = 'supr-reporting-api';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_REPORTING_API_DIR_NAME . '/languages/');
    }
}
