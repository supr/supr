<?php

namespace SuprReportingApi;

class OrderRepository
{
    /**
     * Save order in DB
     *
     * @param Order $order
     * @throws \Exception
     */
    public static function saveOrder(Order &$order): void
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        // Set order created date
        if ($order->getId() === null && $order->getDateCreated() === null) {
            $order->setDateCreated((new \DateTime()));
        }

        $order->setDateModified((new \DateTime()));

        $data = $order->serialize();

        // It is not in the table of orders
        unset($data['order_items']);

        // Update or insert
        if ($order->getId() > 0) {
            unset($data['id']);
            $wpdb->update(DbManager::instance()->getOrderTableName(), $data, ['id' => $order->getId()]);
        } else {
            // Delete all old orders, because we cannot have the same rows
            $wpdb->delete(DbManager::instance()->getOrderTableName(), ['order_id' => $order->getOrderId(), 'blog_id' => $order->getBlogId()]);
            // Insert new row
            $wpdb->insert(DbManager::instance()->getOrderTableName(), $data);
            // Update id
            $order->setId($wpdb->insert_id);
        }

        // Save order items and write back
        $orderItems = $order->getOrderItems();
        foreach ($orderItems as $num => $orderItem) {
            // Update order id after order saving
            $orderItems[$num]->setOrderId($order->getId());
            OrderItemRepository::saveOrderItem($orderItems[$num]);
        }
        $order->setOrderItems($orderItems);
    }

    /**
     * Get order by id
     *
     * @param int $id
     * @return null|Order
     */
    public static function getOrder($id): ?Order
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getOrderTableName();

        $sql = "SELECT * FROM {$tableName} WHERE `id` = %d";
        $sql = $wpdb->prepare($sql, [$id]);
        $orders = $wpdb->get_results($sql, ARRAY_A);

        $order = isset($orders[0]) ? new Order($orders[0]) : null;

        // Load order items
        if ($order !== null) {
            $order->setOrderItems(OrderItemRepository::getOrderItemsByOrderId($order->getId()));
        }

        return $order;
    }

    /**
     * Get orders
     *
     * @param int $limit
     * @return Order[]
     */
    public static function getOrders($limit = 10): array
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getOrderTableName();

        $sql = "SELECT * FROM {$tableName} ORDER BY `id` ASC LIMIT %d";
        $sql = $wpdb->prepare($sql, [$limit]);
        $orders = $wpdb->get_results($sql, ARRAY_A);

        $returned = [];

        foreach ($orders as $order) {
            $order = new Order($order);
            $order->setOrderItems(OrderItemRepository::getOrderItemsByOrderId($order->getId()));
            $returned[] = $order;
        }

        return $returned;
    }

    /**
     * Delete order by id
     *
     * @param int $id
     */
    public static function deleteOrder($id): void
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $wpdb->delete(DbManager::instance()->getOrderTableName(), ['id' => $id]);
    }

    /**
     * Delete all orders
     *
     * @return bool|int
     */
    public static function deleteAllOrders()
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getOrderTableName();

        return $wpdb->query("DELETE FROM `{$tableName}`");
    }

    /**
     * Get count
     *
     * @return int
     */
    public static function getTotalOrdersCount(): int
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getOrderTableName();

        return $wpdb->get_var("SELECT COUNT(*) FROM {$tableName}");
    }
}
