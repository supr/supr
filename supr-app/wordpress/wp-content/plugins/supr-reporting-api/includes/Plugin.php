<?php

namespace SuprReportingApi;

/**
 * Class Plugin
 *
 * @package SuprReportingApi
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();
        // Create Menu
        Menu::addMenu();
        // Admin init
        \add_action('admin_init', [$this, 'adminInit']);
        // Load translations
        \add_action('init', [TextDomain::class, 'registerTranslations']);

        // Register end points
        \add_action('rest_api_init', [Api::instance(), 'registerRoutes']);

        // Order has been completed
        \add_action('save_post', [Manager::instance(), 'updateOrder'], 10, 1);
        // Order has been changed
        \add_action('edit_post', [Manager::instance(), 'updateOrder'], 10, 1);
        \add_action('woocommerce_update_order', [Manager::instance(), 'updateOrder'], 10, 1);
        // Order has been trashed
        \add_action('wp_trash_post', [Manager::instance(), 'updateOrder'], 10, 1);
        // Order are deleting (but not has been deleted yet)
        \add_action('before_delete_post', [Manager::instance(), 'deleteOrder'], 10, 1);

        // Actualize data if plan of shop was changed
        \add_filter('wu_subscription_before_save', [Manager::instance(), 'updateBlogByPlan'], 10, 2);

        // New blog registered
        \add_action('wp_insert_site', [Manager::instance(), 'updateBlog']);

        // Update blog statuses
        \add_action('make_spam_blog', [Manager::instance(), 'updateBlog']);
        \add_action('make_ham_blog', [Manager::instance(), 'updateBlog']);
        \add_action('mature_blog', [Manager::instance(), 'updateBlog']);
        \add_action('unmature_blog', [Manager::instance(), 'updateBlog']);
        \add_action('archive_blog', [Manager::instance(), 'updateBlog']);
        \add_action('unarchive_blog', [Manager::instance(), 'updateBlog']);
        \add_action('make_delete_blog', [Manager::instance(), 'updateBlog']);
        \add_action('make_undelete_blog', [Manager::instance(), 'updateBlog']);
        \add_action('update_blog_public', [Manager::instance(), 'updateBlog']);
        \add_action('wp_uninitialize_site', [Manager::instance(), 'updateBlog']);
        \add_action('update_option_elementor_maintenance_mode_mode', [Manager::instance(), 'changeBlogStatus'], 10, 3);

        // Update blog options in network admin
        \add_action('wpmu_update_blog_options', [Manager::instance(), 'updateBlog']);

        // Update blog owner data
        \add_filter('update_user_metadata', [Manager::instance(), 'updateUserMetaFilter'], 10, 5);
        \add_filter('email_change_email', [Manager::instance(), 'updateUserEmailFilter'], 10, 3);

        // Update blog owner data in network admin
        \add_action('edit_user_profile_update', [Manager::instance(), 'updateBlogByUserId']);

        // Add checkbox "test order" to checkout
        \add_action('woocommerce_after_order_notes', [Woocommerce::class, 'addTestOrderCheckboxToCheckout']);
        // Handle checkbox "test order" save process
        \add_action('woocommerce_checkout_update_order_meta', [Woocommerce::class, 'processTestOrderCheckboxField']);
        \add_action('woocommerce_process_shop_order_meta', [Woocommerce::class, 'processTestOrderCheckboxField']);
        // Display checkbox "test order" on the order edit page
        \add_action('woocommerce_admin_order_data_after_order_details', [Woocommerce::class, 'displayTestOrderCheckboxInOrderDetails']);
    }

    /**
     * On admin init
     */
    public function adminInit(): void
    {
        \add_action('admin_enqueue_scripts', [ResourceManager::class, 'addAdminResources']);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_REPORTING_API_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::instance();
