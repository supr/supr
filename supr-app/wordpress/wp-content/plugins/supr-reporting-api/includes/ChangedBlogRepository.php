<?php
namespace SuprReportingApi;

class ChangedBlogRepository
{
    /**
     * Save changed shop in DB
     *
     * @param ChangedBlog $changedBlog
     * @throws \Exception
     */
    public static function saveChangedBlog(&$changedBlog): void
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        if ($changedBlog->getId() === null) {
            $changedBlog->setDateCreated((new \DateTime()));
        }

        $changedBlog->setDateModified((new \DateTime()));

        $data = $changedBlog->serialize();

        // Update or insert
        if ($changedBlog->getId() > 0) {
            unset($data['id']);
            $wpdb->update(DbManager::instance()->getChangedBlogTableName(), $data, ['id' => $changedBlog->getId()]);
        } else {
            // Delete all old changed blogs, because we cannot have the same rows
            $wpdb->delete(DbManager::instance()->getChangedBlogTableName(), ['blog_id' => $changedBlog->getBlogId()]);
            // Insert new row
            $wpdb->insert(DbManager::instance()->getChangedBlogTableName(), $data);
            // Update id
            $changedBlog->setId($wpdb->insert_id);
        }
    }

    /**
     * Get changed blog by blog id
     *
     * @param int $blogId
     * @return null|ChangedBlog
     */
    public static function getChangedBlog($blogId): ?ChangedBlog
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getChangedBlogTableName();

        $sql = "SELECT * FROM {$tableName} WHERE `blog_id` = %d";
        $sql = $wpdb->prepare($sql, [$blogId]);
        $orders = $wpdb->get_results($sql, ARRAY_A);

        return (isset($orders[0]) ? new ChangedBlog($orders[0]) : null);
    }

    /**
     * Get orders
     *
     * @param int $limit
     * @return ChangedBlog[]
     */
    public static function getChangedBlogs($limit = 10): array
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getChangedBlogTableName();

        $sql = "SELECT * FROM {$tableName} ORDER BY `id` ASC LIMIT %d";
        $sql = $wpdb->prepare($sql, [$limit]);
        $changedBlogs = $wpdb->get_results($sql, ARRAY_A);

        $returned = [];

        foreach ($changedBlogs as $changedBlog) {
            $changedBlog = new ChangedBlog($changedBlog);
            $returned[] = $changedBlog;
        }

        return $returned;
    }

    /**
     * Delete changed blog by blog id
     *
     * @param int $blogId
     */
    public static function deleteChangedBlog($blogId): void
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $wpdb->delete(DbManager::instance()->getChangedBlogTableName(), ['blog_id' => $blogId]);
    }

    /**
     * Delete all changed blogs
     *
     * @return bool|int
     */
    public static function deleteAllChangedBlogs()
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getChangedBlogTableName();

        return $wpdb->query("DELETE FROM `{$tableName}`");
    }

    /**
     * Get count
     *
     * @return int
     */
    public static function getTotalChangedBlogCount(): int
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getChangedBlogTableName();

        return $wpdb->get_var("SELECT COUNT(*) FROM {$tableName}");
    }
}
