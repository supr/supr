<?php

namespace SuprReportingApi;

/**
 * Class OrderItem
 *
 * @package SuprReportingApi
 */
class OrderItem
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var int
     */
    private $productId;

    /**
     * @var int
     */
    private $variationId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $variationName;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var string
     */
    private $subtotal;

    /**
     * @var string
     */
    private $subtotalTax;

    /**
     * @var string
     */
    private $total;

    /**
     * @var string
     */
    private $totalTax;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var \DateTime
     */
    private $dateModified;

    /**
     * @var string
     */
    private $permalink;

    /**
     * OrderItem constructor.
     *
     * @param array $data
     */
    public function __construct($data = [])
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['order_id'])) {
            $this->setOrderId($data['order_id']);
        }
        if (isset($data['product_id'])) {
            $this->setProductId($data['product_id']);
        }
        if (isset($data['variation_id'])) {
            $this->setVariationId($data['variation_id']);
        }
        if (isset($data['name'])) {
            $this->setName($data['name']);
        }
        if (isset($data['variation_name'])) {
            $this->setVariationName($data['variation_name']);
        }
        if (isset($data['quantity'])) {
            $this->setQuantity($data['quantity']);
        }
        if (isset($data['subtotal'])) {
            $this->setSubtotal($data['subtotal']);
        }
        if (isset($data['subtotal_tax'])) {
            $this->setSubtotalTax($data['subtotal_tax']);
        }
        if (isset($data['total'])) {
            $this->setTotal($data['total']);
        }
        if (isset($data['total_tax'])) {
            $this->setTotalTax($data['total_tax']);
        }
        if (isset($data['date_created'])) {
            $this->setDateCreated($data['date_created']);
        }
        if (isset($data['date_modified'])) {
            $this->setDateModified($data['date_modified']);
        }
        if (isset($data['permalink'])) {
            $this->setPermalink($data['permalink']);
        }
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'id' => $this->getId(),
            'order_id' => $this->getOrderId(),
            'product_id' => $this->getProductId(),
            'variation_id' => $this->getVariationId(),
            'name' => $this->getName(),
            'variation_name' => $this->getVariationName(),
            'quantity' => $this->getQuantity(),
            'subtotal' => $this->getSubtotal(),
            'subtotal_tax' => $this->getSubtotalTax(),
            'total' => $this->getTotal(),
            'total_tax' => $this->getTotalTax(),
            'date_created' => $this->getDateCreated() ? $this->getDateCreated()->format(\DateTime::ATOM) : null,
            'date_modified' => $this->getDateModified() ? $this->getDateModified()->format(\DateTime::ATOM) : null,
            'permalink' => $this->getPermalink()
        ];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id): void
    {
        $this->id = (int)$id;
    }

    /**
     * @return int|null
     */
    public function getOrderId(): ?int
    {
        return $this->orderId;
    }

    /**
     * @param $orderId
     */
    public function setOrderId($orderId): void
    {
        $this->orderId = (int)$orderId;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @param $productId
     */
    public function setProductId($productId): void
    {
        $this->productId = (int)$productId;
    }

    /**
     * @return int|null
     */
    public function getVariationId(): ?int
    {
        return $this->variationId;
    }

    /**
     * @param $variationId
     */
    public function setVariationId($variationId): void
    {
        $this->variationId = $variationId ? (int)$variationId : null;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name): void
    {
        $this->name = $name ? (string)$name : null;
    }

    /**
     * @return string|null
     */
    public function getVariationName(): ?string
    {
        return $this->variationName;
    }

    /**
     * @param $variationName
     */
    public function setVariationName($variationName): void
    {
        $this->variationName = $variationName ? (string)$variationName : null;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity ? (int)$quantity : null;
    }

    /**
     * @return string|null
     */
    public function getTotal(): ?string
    {
        return $this->total;
    }

    /**
     * @param $total
     */
    public function setTotal($total): void
    {
        $this->total = (string)$total;
    }

    /**
     * @return string|null
     */
    public function getTotalTax(): ?string
    {
        return $this->totalTax;
    }

    /**
     * @param $totalTax
     */
    public function setTotalTax($totalTax): void
    {
        $this->totalTax = (string)$totalTax;
    }

    /**
     * @return string|null
     */
    public function getSubtotal(): ?string
    {
        return $this->subtotal;
    }

    /**
     * @param $subtotal
     */
    public function setSubtotal($subtotal): void
    {
        $this->subtotal = (string)$subtotal;
    }

    /**
     * @return string|null
     */
    public function getSubtotalTax(): ?string
    {
        return $this->subtotalTax;
    }

    /**
     * @param $subtotalTax
     */
    public function setSubtotalTax($subtotalTax): void
    {
        $this->subtotalTax = (string)$subtotalTax;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateCreated(): ?\DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param $dateCreated
     */
    public function setDateCreated($dateCreated): void
    {
        $this->dateCreated = DbManager::convertToDateTime($dateCreated);
    }

    /**
     * @return \DateTime|null
     */
    public function getDateModified(): ?\DateTime
    {
        return $this->dateModified;
    }

    /**
     * @param $dateModified
     */
    public function setDateModified($dateModified): void
    {
        $this->dateModified = DbManager::convertToDateTime($dateModified);
    }

    /**
     * @return string|null
     */
    public function getPermalink(): ?string
    {
        return $this->permalink;
    }

    /**
     * @param $permalink
     */
    public function setPermalink($permalink): void
    {
        $this->permalink = (string)$permalink;
    }
}
