<?php

namespace SuprReportingApi;

/**
 * Class Order
 *
 * @package SuprReportingApi
 */
class Order
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $orderId;

    /**
     * @var int
     */
    private $blogId;

    /**
     * @var string
     */
    private $orderKey;

    /**
     * @var string
     */
    private $shippingMethod;

    /**
     * @var string
     */
    private $status;

    /**
     * @var int
     */
    private $parentId;

    /**
     * @var bool
     */
    private $pricesIncludeTax = true;

    /**
     * @var string
     */
    private $total;

    /**
     * @var string
     */
    private $totalTax;

    /**
     * @var string
     */
    private $shippingTotal;

    /**
     * @var string
     */
    private $shippingTax;

    /**
     * @var string
     */
    private $cartTax;

    /**
     * @var string
     */
    private $discountTotal;

    /**
     * @var string
     */
    private $discountTax;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $paymentMethod;

    /**
     * @var \DateTime
     */
    private $dateCompleted;

    /**
     * @var \DateTime
     */
    private $datePaid;

    /**
     * @var bool
     */
    private $testOrder;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var \DateTime
     */
    private $dateModified;

    /**
     * @var OrderItem[]
     */
    private $orderItems = [];

    /**
     * Order constructor.
     *
     * @param array $data
     */
    public function __construct($data = [])
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['order_id'])) {
            $this->setOrderId($data['order_id']);
        }
        if (isset($data['blog_id'])) {
            $this->setBlogId($data['blog_id']);
        }
        if (isset($data['order_key'])) {
            $this->setOrderKey($data['order_key']);
        }
        if (isset($data['shipping_method'])) {
            $this->setShippingMethod($data['shipping_method']);
        }
        if (isset($data['status'])) {
            $this->setStatus($data['status']);
        }
        if (isset($data['parent_id'])) {
            $this->setParentId($data['parent_id']);
        }
        if (isset($data['prices_include_tax'])) {
            $this->setPricesIncludeTax($data['prices_include_tax']);
        }
        if (isset($data['total'])) {
            $this->setTotal($data['total']);
        }
        if (isset($data['total_tax'])) {
            $this->setTotalTax($data['total_tax']);
        }
        if (isset($data['shipping_total'])) {
            $this->setShippingTotal($data['shipping_total']);
        }
        if (isset($data['shipping_tax'])) {
            $this->setShippingTax($data['shipping_tax']);
        }
        if (isset($data['cart_tax'])) {
            $this->setCartTax($data['cart_tax']);
        }
        if (isset($data['discount_total'])) {
            $this->setDiscountTotal($data['discount_total']);
        }
        if (isset($data['discount_tax'])) {
            $this->setDiscountTax($data['discount_tax']);
        }
        if (isset($data['currency'])) {
            $this->setCurrency($data['currency']);
        }
        if (isset($data['payment_method'])) {
            $this->setPaymentMethod($data['payment_method']);
        }
        if (isset($data['date_completed'])) {
            $this->setDateCompleted($data['date_completed']);
        }
        if (isset($data['date_paid'])) {
            $this->setDatePaid($data['date_paid']);
        }
        if (isset($data['test_order'])) {
            $this->setTestOrder($data['test_order']);
        }
        if (isset($data['date_created'])) {
            $this->setDateCreated($data['date_created']);
        }
        if (isset($data['date_modified'])) {
            $this->setDateModified($data['date_modified']);
        }

        if (isset($data['order_items']) && \is_array($data['order_items'])) {
            $orderItems = [];
            foreach ($data['order_items'] as $orderItemData) {
                $orderItems[] = new OrderItem($orderItemData);
            }
            $this->setOrderItems($orderItems);
        }
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        $returned = [
            'id' => $this->getId(),
            'order_id' => $this->getOrderId(),
            'blog_id' => $this->getBlogId(),
            'order_key' => $this->getOrderKey(),
            'shipping_method' => $this->getShippingMethod(),
            'status' => $this->getStatus(),
            'parent_id' => $this->getParentId(),
            'prices_include_tax' => $this->getPricesIncludeTax(),
            'total' => $this->getTotal(),
            'total_tax' => $this->getTotalTax(),
            'shipping_total' => $this->getShippingTotal(),
            'shipping_tax' => $this->getShippingTax(),
            'cart_tax' => $this->getCartTax(),
            'discount_total' => $this->getDiscountTotal(),
            'discount_tax' => $this->getDiscountTax(),
            'currency' => $this->getCurrency(),
            'payment_method' => $this->getPaymentMethod(),
            'date_completed' => $this->getDateCompleted() ? $this->getDateCompleted()->format(\DateTime::ATOM) : null,
            'date_paid' => $this->getDatePaid() ? $this->getDatePaid()->format(\DateTime::ATOM) : null,
            'test_order' => $this->getTestOrder(),
            'date_created' => $this->getDateCreated() ? $this->getDateCreated()->format(\DateTime::ATOM) : null,
            'date_modified' => $this->getDateModified() ? $this->getDateModified()->format(\DateTime::ATOM) : null,
            'order_items' => []
        ];

        foreach ($this->getOrderItems() as $orderItem) {
            $returned['order_items'][] = $orderItem->serialize();
        }

        return $returned;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id): void
    {
        $this->id = (int)$id;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @param $orderId
     */
    public function setOrderId($orderId): void
    {
        $this->orderId = (int)$orderId;
    }

    /**
     * @return int
     */
    public function getBlogId(): int
    {
        return $this->blogId;
    }

    /**
     * @param $blogId
     */
    public function setBlogId($blogId): void
    {
        $this->blogId = (int)$blogId;
    }

    /**
     * @return null|string
     */
    public function getOrderKey(): ?string
    {
        return $this->orderKey;
    }

    /**
     * @param $orderKey
     */
    public function setOrderKey($orderKey): void
    {
        $this->orderKey = $orderKey ? (string)$orderKey : null;
    }

    /**
     * @return string|null
     */
    public function getShippingMethod(): ?string
    {
        return $this->shippingMethod;
    }

    /**
     * @param $shippingMethod
     */
    public function setShippingMethod($shippingMethod): void
    {
        $this->shippingMethod = $shippingMethod ? (string)$shippingMethod : null;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param $status
     */
    public function setStatus($status): void
    {
        $this->status = $status ? (string)$status : null;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    /**
     * @param $parentId
     */
    public function setParentId($parentId): void
    {
        $this->parentId = $parentId ? (int)$parentId : null;
    }

    /**
     * @return bool
     */
    public function getPricesIncludeTax(): bool
    {
        return $this->pricesIncludeTax;
    }

    /**
     * @param $pricesIncludeTax
     */
    public function setPricesIncludeTax($pricesIncludeTax): void
    {
        $this->pricesIncludeTax = (bool)$pricesIncludeTax;
    }

    /**
     * @return string|null
     */
    public function getTotal(): ?string
    {
        return $this->total;
    }

    /**
     * @param $total
     */
    public function setTotal($total): void
    {
        $this->total = (string)$total;
    }

    /**
     * @return string|null
     */
    public function getTotalTax(): ?string
    {
        return $this->totalTax;
    }

    /**
     * @param $totalTax
     */
    public function setTotalTax($totalTax): void
    {
        $this->totalTax = (string)$totalTax;
    }

    /**
     * @return string|null
     */
    public function getShippingTotal(): ?string
    {
        return $this->shippingTotal;
    }

    /**
     * @param $shippingTotal
     */
    public function setShippingTotal($shippingTotal): void
    {
        $this->shippingTotal = (string)$shippingTotal;
    }

    /**
     * @return string|null
     */
    public function getShippingTax(): ?string
    {
        return $this->shippingTax;
    }

    /**
     * @param $shippingTax
     */
    public function setShippingTax($shippingTax): void
    {
        $this->shippingTax = (string)$shippingTax;
    }

    /**
     * @return string|null
     */
    public function getCartTax(): ?string
    {
        return $this->cartTax;
    }

    /**
     * @param $cartTax
     */
    public function setCartTax($cartTax): void
    {
        $this->cartTax = (string)$cartTax;
    }

    /**
     * @return string|null
     */
    public function getDiscountTotal(): ?string
    {
        return $this->discountTotal;
    }

    /**
     * @param $discountTotal
     */
    public function setDiscountTotal($discountTotal): void
    {
        $this->discountTotal = (string)$discountTotal;
    }

    /**
     * @return string|null
     */
    public function getDiscountTax(): ?string
    {
        return $this->discountTax;
    }

    /**
     * @param $discountTax
     */
    public function setDiscountTax($discountTax): void
    {
        $this->discountTax = (string)$discountTax;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = (string)$currency;
    }

    /**
     * @return string|null
     */
    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    /**
     * @param $paymentMethod
     */
    public function setPaymentMethod($paymentMethod): void
    {
        $this->paymentMethod = (string)$paymentMethod;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateCompleted(): ?\DateTime
    {
        return $this->dateCompleted;
    }

    /**
     * @param $dateCompleted
     */
    public function setDateCompleted($dateCompleted): void
    {

        $this->dateCompleted = DbManager::convertToDateTime($dateCompleted);
    }

    /**
     * @return \DateTime|null
     */
    public function getDatePaid(): ?\DateTime
    {
        return $this->datePaid;
    }

    /**
     * @param $datePaid
     */
    public function setDatePaid($datePaid): void
    {
        $this->datePaid = DbManager::convertToDateTime($datePaid);
    }

    /**
     * @return bool
     */
    public function getTestOrder(): bool
    {
        return $this->testOrder;
    }

    /**
     * @param $testOrder
     */
    public function setTestOrder($testOrder): void
    {
        $this->testOrder = (bool)$testOrder;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateCreated(): ?\DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param $dateCreated
     */
    public function setDateCreated($dateCreated): void
    {
        $this->dateCreated = DbManager::convertToDateTime($dateCreated);
    }

    /**
     * @return \DateTime|null
     */
    public function getDateModified(): ?\DateTime
    {
        return $this->dateModified;
    }

    /**
     * @param $dateModified
     */
    public function setDateModified($dateModified): void
    {
        $this->dateModified = DbManager::convertToDateTime($dateModified);
    }

    /**
     * @return OrderItem[]
     */
    public function getOrderItems(): array
    {
        return $this->orderItems;
    }

    /**
     * @param OrderItem[] $orderItems
     */
    public function setOrderItems($orderItems): void
    {
        $this->orderItems = $orderItems;
    }
}
