<?php

namespace SuprReportingApi;

/**
 * Class Manager
 *
 * @package SuprReportingApi
 */
class Manager
{
    /**
     * @var Manager
     */
    public static $instance;

    /**
     * @var array
     */
    private $updatedWcOrders = [];

    /**
     * Prefix for the test shops because don't want to sync test shops!
     *
     * @var string
     */
    public $testPrefix = 'test20';

    public function __construct()
    {
        $this->loadSettings();
    }

    /**
     * Load settings from DB
     */
    public function loadSettings(): void
    {
        $options = \get_blog_option(1, 'supr_reporting_api');
        if (isset($options['test_prefix'])) {
            $this->testPrefix = trim($options['test_prefix']);
        }
    }

    /**
     * @return \SuprReportingApi\Manager
     */
    public static function instance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Check if shop slug is a test shop
     *
     * @param int $blogId
     * @return bool
     */
    public function isTestBlog($blogId = null): bool
    {
        $blogId = $blogId ?? \get_current_blog_id();

        $domain = \get_blog_details($blogId, false)->domain;

        return strpos($domain, $this->testPrefix) === 0;
    }

    /**
     * @param int $postId
     * @throws \Exception
     */
    public function deleteOrder($postId): void
    {
        $this->updateOrder($postId, 'deleted');
    }

    /**
     * Update or create a new order in supr table
     *
     * @param int    $postId
     * @param string $status
     * @throws \Exception
     */
    public function updateOrder($postId, $status = null): void
    {
        // We need only WooCommerce orders
        if (\get_post_type($postId) !== 'shop_order') {
            return;
        }

        // We don't sync test shops
        if ($this->isTestBlog()) {
            return;
        }

        $wcOrder = \wc_get_order($postId);

        // If we cannot get order, we break
        if (!$wcOrder instanceof \WC_Order) {
            error_log("[SUPR REPORTING API]: Cannot get order #{$postId} in blog #" . \get_current_blog_id() . '. $wcOrder has class ' . \get_class($wcOrder) . ' instead of \WC_Order.');

            return;
        }

        // Get shipping method(s)
        $shippingMethods = $wcOrder->get_shipping_methods();
        $shippingMethodTitle = [];
        foreach ($shippingMethods as $shippingMethod) {
            $shippingMethodTitle[] = $shippingMethod->get_method_id();
        }
        $shippingMethodTitle = \implode(',', $shippingMethodTitle);

        // Get payment method
        $paymentMethod = $wcOrder->get_payment_method();

        $orderData = [
            'order_id' => $wcOrder->get_id(),
            'blog_id' => \get_current_blog_id(),
            'order_key' => $wcOrder->get_order_key(),
            'shipping_method' => $shippingMethodTitle,
            'status' => $status ?? $wcOrder->get_status(),
            'parent_id' => $wcOrder->get_parent_id(),
            'prices_include_tax' => $wcOrder->get_prices_include_tax(),
            'total' => $wcOrder->get_total(),
            'total_tax' => $wcOrder->get_total_tax(),
            'shipping_total' => $wcOrder->get_shipping_total(),
            'shipping_tax' => $wcOrder->get_shipping_tax(),
            'cart_tax' => $wcOrder->get_cart_tax(),
            'discount_total' => $wcOrder->get_discount_total(),
            'discount_tax' => $wcOrder->get_discount_tax(),
            'currency' => $wcOrder->get_currency(),
            'payment_method' => !empty($paymentMethod) ? $paymentMethod : null,
            'date_completed' => $wcOrder->get_date_completed() ? $wcOrder->get_date_completed()->format(\DateTime::ATOM) : null,
            'date_paid' => $wcOrder->get_date_paid() ? $wcOrder->get_date_paid()->format(\DateTime::ATOM) : null,
            'test_order' => \get_post_meta($wcOrder->get_id(), 'is_test_order', true) ?? false,
            'date_created' => $wcOrder->get_date_created() ? $wcOrder->get_date_created()->format(\DateTime::ATOM) : null
        ];

        $order = new Order($orderData);

        $wcOrderItems = $wcOrder->get_items();

        $orderItems = [];
        foreach ($wcOrderItems as $wcOrderItem) {
            $wcOrderItemData = $wcOrderItem->get_data();

            $wcProduct = \wc_get_product($wcOrderItemData['product_id']);
            if ($wcProduct) {
                $orderItemName = $wcProduct->get_name() ?? $wcProduct->get_slug();
            }

            // Sometimes we have an empty name. Because the product was already deleted.
            // Then we take a name from order: "Beispiel Produkt 2" or "Beispiel Produkt 5 - L, Weiß"
            if (empty($orderItemName)) {
                $orderItemName = $wcOrderItemData['name'];
            }

            $variationTitle = null;
            // If it is a variant, add variation name: "L,Weiß"
            if ((int)$wcOrderItemData['variation_id'] > 0) {
                $wcVariant = \wc_get_product($wcOrderItemData['variation_id']);
                if ($wcVariant) {
                    $variationTitle = \implode(',', $wcVariant->get_attributes());
                }
            }

            $orderItemData = [
                'product_id' => $wcOrderItemData['product_id'],
                'variation_id' => $wcOrderItemData['variation_id'],
                'variation_name' => $variationTitle,
                'name' => $orderItemName,
                'quantity' => $wcOrderItemData['quantity'],
                'subtotal' => $wcOrderItemData['subtotal'],
                'subtotal_tax' => $wcOrderItemData['subtotal_tax'],
                'total' => $wcOrderItemData['total'],
                'total_tax' => $wcOrderItemData['total_tax'],
                'permalink' => \get_permalink($wcOrderItemData['product_id']) ?? null
            ];

            $orderItems[] = new OrderItem($orderItemData);
        }

        $order->setOrderItems($orderItems);

        OrderRepository::saveOrder($order);

        // Add to updated orders
        $this->updatedWcOrders[] = $order->getOrderId();
    }

    /**
     * Update blog if online status was changed
     *
     * @param $old_value
     * @param $value
     * @param $option
     * @throws \Exception
     */
    public function changeBlogStatus($old_value, $value, $option): void
    {
        $this->updateBlog();
    }

    /**
     * Save row, that blog was changed
     *
     * @param null|\WP_Site|integer $blog
     * @throws \Exception
     */
    public function updateBlog($blog = null): void
    {
        $blogId = null;

        if ($blog instanceof \WP_Site) {
            $blogId = $blog->blog_id;
        } elseif (\is_numeric($blog)) {
            $blogId = (int)$blog;
        } else {
            $blogId = \get_current_blog_id();
        }

        // We don't sync test shops
        if ($this->isTestBlog($blogId)) {
            return;
        }

        $changedBlog = ChangedBlogRepository::getChangedBlog($blogId);

        // If not found
        if ($changedBlog === null) {
            $changedBlog = new ChangedBlog();
            $changedBlog->setBlogId($blogId);
        }

        ChangedBlogRepository::saveChangedBlog($changedBlog);
    }

    /**
     * @param array            $object
     * @param \WU_Subscription $subscription
     * @return array|null
     * @throws \Exception
     */
    public function updateBlogByPlan($object, $subscription): ?array
    {
        $blogId = $subscription->get_user()->get_site_id();

        if ($blogId) {
            $this->updateBlog($blogId);
        }

        return $object;
    }

    /**
     * Mark blog as changed if user meta was changed
     *
     * @param        $check
     * @param int    $objectId
     * @param string $metaKey
     * @param        $metaValue
     * @param        $prevValue
     * @return mixed
     * @throws \Exception
     */
    public function updateUserMetaFilter($check, $objectId, $metaKey, $metaValue, $prevValue)
    {
        // We have in API only first name & last name of blog owners
        // We update blog only if admin data were changed
        if (\in_array($metaKey, ['first_name', 'last_name'], true) && \current_user_can('administrator')) {
            $this->updateBlog();
        }

        return $check;
    }

    /**
     * Mark blog as changed if user email was changed
     *
     * @param $emailChangeEmail
     * @param $user
     * @param $userdata
     * @return mixed
     * @throws \Exception
     */
    public function updateUserEmailFilter($emailChangeEmail, $user, $userdata)
    {
        // We update blog only if admin email was changed
        if (\current_user_can('administrator')) {
            $this->updateBlog();
        }

        return $emailChangeEmail;
    }

    /**
     * Update blog by user id
     *
     * @param $userId
     * @throws \Exception
     */
    public function updateBlogByUserId($userId): void
    {
        // We should get all blogs of the user and then update all of them
        if (function_exists('\wu_get_subscription')) {
            $wuSubscription = \wu_get_subscription($userId);

            if (!$wuSubscription instanceof \WU_Subscription) {
                return;
            }

            // Update all blogs of user
            foreach ($wuSubscription->get_sites_ids() as $blogId) {
                $this->updateBlog($blogId);
            }
        }
    }
}
