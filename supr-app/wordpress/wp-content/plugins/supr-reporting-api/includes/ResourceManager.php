<?php

namespace SuprReportingApi;

/**
 * Class ResourceManager
 *
 * @package SuprReportingApi
 */
class ResourceManager
{
    /**
     * Register files for plugin
     */
    public static function addAdminResources(): void
    {
        // CSS
        if (isset($_GET['post'], $_GET['action']) && $_GET['action'] === 'edit') {
            \wp_enqueue_style('supr_reporting_api_order', SUPR_REPORTING_API_URL . 'css/order.css', [], SUPR_REPORTING_API_VERSION, 'all');
        }
    }
}
