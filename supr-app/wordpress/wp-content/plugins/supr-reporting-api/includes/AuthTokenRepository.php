<?php

namespace SuprReportingApi;

class AuthTokenRepository
{
    /**
     * Get default expired time
     *
     * @return \DateTime
     * @throws \Exception
     */
    private static function getExpiredTime(): \DateTime
    {
        return (new \DateTime())->sub(new \DateInterval('PT2H'));
    }

    /**
     * Save auth token in DB
     *
     * @param AuthToken $authToken
     * @throws \Exception
     */
    public static function saveAuthToken(&$authToken): void
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        if ($authToken->getId() === null) {
            $authToken->setDateCreated((new \DateTime()));
        }

        $authToken->setDateModified((new \DateTime()));

        $data = $authToken->serialize();

        // Update or insert
        if ($authToken->getId() > 0) {
            unset($data['id']);
            $wpdb->update(DbManager::instance()->getAuthTokensTableName(), $data, ['id' => $authToken->getId()]);
        } else {
            $wpdb->insert(DbManager::instance()->getAuthTokensTableName(), $data);
            // Update id
            $authToken->setId($wpdb->insert_id);
        }
    }

    /**
     * Get auth token by user id
     *
     * @param int $userId
     * @return null|AuthToken
     * @throws \Exception
     */
    public static function getAuthTokenByUserId($userId): ?AuthToken
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getAuthTokensTableName();

        $sql = "SELECT * FROM {$tableName} WHERE `user_id` = %d AND `date_modified` > %s";
        $sql = $wpdb->prepare($sql, [$userId, self::getExpiredTime()->format(\DateTime::ATOM)]);
        $authTokens = $wpdb->get_results($sql, ARRAY_A);

        return isset($authTokens[0]) ? new AuthToken($authTokens[0]) : null;
    }

    /**
     * Get auth token by token
     *
     * @param string $token
     * @return null|AuthToken
     * @throws \Exception
     */
    public static function getAuthTokenByToken($token): ?AuthToken
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getAuthTokensTableName();

        $sql = "SELECT * FROM {$tableName} WHERE `token` = %s AND `date_modified` > %s";
        $sql = $wpdb->prepare($sql, [$token, self::getExpiredTime()->format(\DateTime::ATOM)]);
        $authTokens = $wpdb->get_results($sql, ARRAY_A);

        return isset($authTokens[0]) ? new AuthToken($authTokens[0]) : null;
    }

    /**
     * Remove all old tokens
     *
     * @return bool
     * @throws \Exception
     */
    public static function removeOldAuthTokens(): bool
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getAuthTokensTableName();

        $sql = "DELETE FROM {$tableName} WHERE `date_modified` < %s";
        $sql = $wpdb->prepare($sql, [self::getExpiredTime()->format(\DateTime::ATOM)]);

        return $wpdb->query($sql) ? true : false;
    }

    /**
     * Delete order item by id
     *
     * @param int $id
     */
    public static function deleteAuthToken($id): void
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $wpdb->delete(DbManager::instance()->getAuthTokensTableName(), ['id' => $id]);
    }
}
