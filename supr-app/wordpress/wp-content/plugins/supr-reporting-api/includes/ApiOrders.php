<?php

namespace SuprReportingApi;

class ApiOrders
{
    /**
     * Get first orders
     */
    public static function getOrders(): void
    {
        $return = [];

        $orders = OrderRepository::getOrders(100);
        foreach ($orders as $order) {
            $return[] = ApiTranslator::translateOrderData($order->serialize());
        }

        \wp_send_json_success($return);
    }

    /**
     * Get one order
     *
     * @param $data
     */
    public static function getOrder($data): void
    {
        $order = OrderRepository::getOrder((int)$data['id']);

        if (!$order) {
            \wp_send_json_error('Order not found.', 404);
        }

        $return = ApiTranslator::translateOrderData($order->serialize());

        \wp_send_json_success($return);
    }

    /**
     * Delete order
     *
     * @param $data
     */
    public static function deleteOrder($data): void
    {
        // Parse ids
        $ids = \array_filter(\explode('-', $data['ids']));

        // Validate data
        if (!\ctype_digit(\implode('', $ids))) {
             \wp_send_json_error('Id or ids are not numeric.', 400);
        }

        $return = [
            'deleted' => [],
            'not_found' => []
        ];

        foreach ($ids as $id) {
            $order = OrderRepository::getOrder((int)$id);
            if (!$order) {
                $return['not_found'][] = (int)$id;
            } else {
                OrderRepository::deleteOrder($order->getId());
                $return['deleted'][] = (int)$id;
            }
        }

        \wp_send_json_success($return);
    }
}
