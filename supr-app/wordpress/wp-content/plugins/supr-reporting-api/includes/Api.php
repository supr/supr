<?php

namespace SuprReportingApi;

class Api
{
    /**
     * @var null
     */
    public static $instance;

    /**
     * @return \SuprReportingApi\Api
     */
    public static function instance(): Api
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @var string
     */
    public static $namespace = 'supr_reporting_api_rest/v1';

    /**
     * @var array
     */
    private $routes;

    public function __construct()
    {
        $this->routes = [
            [
                'route' => 'auth',
                'options' => [
                    'methods' => 'POST',
                    'callback' => [ApiAuth::class, 'auth']
                ]
            ],
            [
                'route' => 'orders',
                'options' => [
                    'methods' => 'GET',
                    'callback' => [ApiOrders::class, 'getOrders'],
                    'permission_callback' => [ApiAuth::class, 'checkAuth']
                ]
            ],
            [
                'route' => 'orders/(?P<id>\d+)',
                'options' => [
                    'methods' => 'GET',
                    'callback' => [ApiOrders::class, 'getOrder'],
                    'permission_callback' => [ApiAuth::class, 'checkAuth']
                ]
            ],
            [
                'route' => 'orders/(?P<ids>[\d\-]+)',
                'options' => [
                    'methods' => 'DELETE',
                    'callback' => [ApiOrders::class, 'deleteOrder'],
                    'permission_callback' => [ApiAuth::class, 'checkAuth']
                ]
            ],
            [
                'route' => 'shops',
                'options' => [
                    'methods' => 'GET',
                    'callback' => [ApiShops::class, 'getShops'],
                    'permission_callback' => [ApiAuth::class, 'checkAuth']
                ]
            ],
            [
                'route' => 'shops/(?P<id>\d+)',
                'options' => [
                    'methods' => 'GET',
                    'callback' => [ApiShops::class, 'getShop'],
                    'permission_callback' => [ApiAuth::class, 'checkAuth']
                ]
            ],
            [
                'route' => 'shops/(?P<ids>[\d\-]+)',
                'options' => [
                    'methods' => 'DELETE',
                    'callback' => [ApiShops::class, 'deleteShop'],
                    'permission_callback' => [ApiAuth::class, 'checkAuth']
                ]
            ],
        ];
    }

    /**
     * Register all routes
     */
    public function registerRoutes(): void
    {
        // Route are only in main blog
        if (\get_current_blog_id() !== 1) {
            return;
        }

        foreach ($this->routes as $route) {
            \register_rest_route(
                self::$namespace,
                $route['route'],
                $route['options']
            );
        }
    }
}
