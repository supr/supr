<?php

namespace SuprReportingApi;

class ApiShops
{
    /**
     * Get first shops
     */
    public static function getShops(): void
    {
        $return = [];

        $changedShops = ChangedBlogRepository::getChangedBlogs(5);
        foreach ($changedShops as $changedShop) {
            $shopData = self::getShopData($changedShop->getBlogId(), $changedShop);
            // Shop was deleted
            if ($shopData === null) {
                $shopData = [
                    'id' => $changedShop->getBlogId(),
                    'status' => 'deleted',
                    'meta' => [
                        'date_modified' => $changedShop->getDateModified()->format(DATE_ATOM)
                    ]
                ];
            }

            $return[] = $shopData;
        }

        \wp_send_json_success($return);
    }

    /**
     * Get shop data
     *
     * @param $data
     * @throws \Exception
     */
    public static function getShop($data): void
    {
        $return = self::getShopData((int)$data['id']);

        if (!$return) {
            \wp_send_json_error('Shop not found.', 404);
        }

        \wp_send_json_success($return);
    }

    /**
     * Delete changed shop
     *
     * @param $data
     */
    public static function deleteShop($data): void
    {
        // Parse ids
        $ids = \array_filter(\explode('-', $data['ids']));

        // Validate data
        if (!\ctype_digit(\implode('', $ids))) {
            \wp_send_json_error('Id or ids are not numeric.', 400);
        }

        $return = [
            'deleted' => [],
            'not_found' => []
        ];

        foreach ($ids as $id) {
            $changedBlog = ChangedBlogRepository::getChangedBlog((int)$id);
            if (!$changedBlog) {
                $return['not_found'][] = (int)$id;
            } else {
                ChangedBlogRepository::deleteChangedBlog($changedBlog->getBlogId());
                $return['deleted'][] = (int)$id;
            }
        }

        \wp_send_json_success($return);
    }

    /////////////////////
    ////// Private //////
    /////////////////////

    /**
     * @param int|string $shopId
     * @return null|\WP_User
     */
    private static function getUserByShopId($shopId): ?\WP_User
    {
        global $wpdb;

        $userId = null;
        $user = null;

        if (\class_exists('\WU_Site_Owner')) {
            $tableName = \WU_Site_Owner::get_table_name();
            $result = $wpdb->get_row("SELECT user_id FROM {$tableName} WHERE site_id = {$shopId}");
            if ($result) {
                $userId = (int)$result->user_id;
            }
        }

        // Get from DB
        if (!$userId) {
            $tableName = $wpdb->base_prefix . 'usermeta';
            $querystring = "SELECT user_id FROM {$tableName} WHERE (meta_key LIKE 'primary_blog' AND meta_value LIKE '{$shopId}') LIMIT 1";
            $userId = (int)$wpdb->get_var($querystring);
        }

        if ($userId) {
            $user = \get_user_by('id', $userId);

            // Fix the return type, because we can't handle mixed types!
            if (\is_bool($user)) {
                return null;
            }
        }

        return $user;
    }

    /**
     * Get shop data
     *
     * @param int|string  $shopId
     * @param ChangedBlog $changedShop
     * @return array|null
     * @throws \Exception
     */
    private static function getShopData($shopId, $changedShop = null): ?array
    {
        $blogDetails = \get_blog_details(['blog_id' => (int)$shopId], true);

        if (!$blogDetails) {
            return null;
        }

        \switch_to_blog($blogDetails->blog_id);

        // Get user post count in that post type
        $attachmentsCount = \wp_count_posts('attachment');
        $attachmentsCount = $attachmentsCount->inherit ?? 0;

        $diskSpaceUsage = null;

        // It works slow on AWS EBS storage
        /*if (\function_exists('shell_exec') && (int)\shell_exec('echo 1') === 1) {
            $result = \shell_exec('du -s -B1 ' . wp_get_upload_dir()['basedir']);
            $result = preg_replace('/\s/', ' ', $result);
            $result = explode(' ', $result);
            $diskSpaceUsage = (int)$result[0];
        }*/

        $return = [
            'id' => (int)$blogDetails->blog_id,
            'status' => self::getShopStatus($blogDetails),
            'meta' => [
                'name' => $blogDetails->blogname,
                'domain' => $blogDetails->domain,
                'date_registered' => (new \DateTime($blogDetails->registered))->format(DATE_ATOM),
                'date_modified' => $changedShop ? $changedShop->getDateModified()->format(DATE_ATOM) : null,
                'company_name' => \get_option('woocommerce_store_owner_company'),
            ],
            'plan' => null,
            'owner' => null,
            'storage' => [
                'attachments_count' => (int)$attachmentsCount,
                'disk_space_usage' => $diskSpaceUsage,
            ]
        ];

        $owner = self::getUserByShopId($blogDetails->blog_id);

        // Add owner data...
        if ($owner) {
            $return['owner'] = [
                'id' => $owner->ID,
                'email' => $owner->user_email,
                'first_name' => $owner->first_name,
                'last_name' => $owner->last_name
            ];
        }

        // Add plan data
        if ($owner && \function_exists('wu_get_subscription') && $subscription = \wu_get_subscription($owner->ID)) {
            $plan = $subscription->get_plan();

            $return['plan'] = [
                'name' => $plan->title,
                'price' => $subscription->get_price(),
                'status' => $subscription->get_status(),
                'integration_key' => $subscription->integration_key,
                'billwerk_contract_id' => \get_user_meta($owner->ID, 'billwerk_contract_id', true),
                'billwerk_customer_id' => \get_user_meta($owner->ID, 'billwerk_customer_id', true),
                'date_created' => (new \DateTime($subscription->created_at))->format(DATE_ATOM),
                'date_changed' => (new \DateTime($subscription->last_plan_change))->format(DATE_ATOM)
            ];
        }

        \restore_current_blog();

        return $return;
    }

    /**
     * @param \WP_Site $wpSite
     * @return string
     */
    private static function getShopStatus($wpSite): string
    {
        if ($wpSite->archived) {
            return 'archived';
        }

        if ($wpSite->spam) {
            return 'spam';
        }

        if ($wpSite->deleted) {
            return 'deleted';
        }

        if (!$wpSite->public) {
            return 'closed';
        }

        if (\get_blog_option($wpSite->id, 'elementor_maintenance_mode_mode', null)) {
            return 'maintenance_mode';
        }

        return 'public';
    }
}
