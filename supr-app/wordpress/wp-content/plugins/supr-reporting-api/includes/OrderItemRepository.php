<?php
namespace SuprReportingApi;

class OrderItemRepository
{
    /**
     * Save order item in DB
     *
     * @param OrderItem $orderItem
     */
    public static function saveOrderItem(&$orderItem): void
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        if ($orderItem->getId() === null) {
            $orderItem->setDateCreated((new \DateTime()));
        }

        $orderItem->setDateModified((new \DateTime()));

        $data = $orderItem->serialize();

        // Update or insert
        if ($orderItem->getId() > 0) {
            unset($data['id']);
            $wpdb->update(DbManager::instance()->getOrderItemTableName(), $data, ['id' => $orderItem->getId()]);
        } else {
            $wpdb->insert(DbManager::instance()->getOrderItemTableName(), $data);
            // Update id
            $orderItem->setId($wpdb->insert_id);
        }
    }

    /**
     * Get order item by id
     *
     * @param $id
     * @return null|OrderItem
     */
    public static function getOrderItem($id): ?OrderItem
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getOrderItemTableName();

        $sql = "SELECT * FROM {$tableName} WHERE `id` = %d";
        $sql = $wpdb->prepare($sql, [$id]);
        $orderItems = $wpdb->get_results($sql, ARRAY_A);

        return isset($orderItems[0]) ? new OrderItem($orderItems[0]) : null;
    }

    /**
     * get order items by order id
     *
     * @param $orderId
     * @return array
     */
    public static function getOrderItemsByOrderId($orderId): array
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $tableName = DbManager::instance()->getOrderItemTableName();

        $sql = "SELECT * FROM {$tableName} WHERE `order_id` = %d";
        $sql = $wpdb->prepare($sql, [$orderId]);
        $orderItems = $wpdb->get_results($sql, ARRAY_A);

        $returned = [];
        foreach ($orderItems as $orderItem) {
            $returned[] = new OrderItem($orderItem);
        }

        return $returned;
    }

    /**
     * Delete order item by id
     *
     * @param int $id
     */
    public static function deleteOrderItem($id): void
    {
        /** @var \wpdb $wpdb */
        global $wpdb;

        $wpdb->delete(DbManager::instance()->getOrderItemTableName(), ['id' => $id]);
    }
}
