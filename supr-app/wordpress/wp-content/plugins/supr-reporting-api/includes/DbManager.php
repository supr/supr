<?php

namespace SuprReportingApi;

/**
 * Class DbManager
 *
 * @package SuprRepotingApi
 */
class DbManager
{
    /**
     * DB adapter
     *
     * @var \wpdb
     */
    private $db;

    /**
     * Order table name
     *
     * @var string
     */
    private $orderTableName;

    /**
     * Order item table name
     *
     * @var string
     */
    private $orderItemTableName;

    /**
     * Changed blog item table name
     *
     * @var string
     */
    private $changedBlogTableName;

    /**
     * Token table name
     *
     * @var string
     */
    private $authTokensTableName;

    /**
     * @var DbManager
     */
    private static $instance;

    /**
     * DbManager constructor.
     */
    public function __construct()
    {
        global $wpdb;

        $this->db = $wpdb;
        $this->orderTableName = $this->db->base_prefix . 'supr_reporting_api_orders';
        $this->orderItemTableName = $this->db->base_prefix . 'supr_reporting_api_order_items';
        $this->changedBlogTableName = $this->db->base_prefix . 'supr_reporting_api_changed_blogs';
        $this->authTokensTableName = $this->db->base_prefix . 'supr_reporting_api_auth_tokens';

        // Update db structur if needed
        $this->changeTableStructure();
    }

    /**
     * @return DbManager
     */
    public static function instance(): DbManager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return string
     */
    public function getOrderTableName(): string
    {
        return $this->orderTableName;
    }

    /**
     * @return string
     */
    public function getOrderItemTableName(): string
    {
        return $this->orderItemTableName;
    }

    /**
     * @return string
     */
    public function getChangedBlogTableName(): string
    {
        return $this->changedBlogTableName;
    }

    /**
     * @return string
     */
    public function getAuthTokensTableName(): string
    {
        return $this->authTokensTableName;
    }

    /**
     * Create tables if not exist
     */
    public function createTable(): void
    {
        $blogsTableName = $this->db->base_prefix . 'blogs';
        $usersTableName = $this->db->base_prefix . 'users';

        // Order table
        $sqlOrder = "CREATE TABLE IF NOT EXISTS {$this->getOrderTableName()} (
        `id` INT NOT NULL AUTO_INCREMENT,
        `order_id` INT NOT NULL,
        `blog_id` BIGINT(20),
        `order_key` VARCHAR(64),
        `shipping_method` VARCHAR(128),
        `status` VARCHAR(32),
        `parent_id` INT DEFAULT NULL,
        `prices_include_tax` SMALLINT(1) NOT NULL DEFAULT 1,
        `total` DECIMAL(13, 4),
        `total_tax` DECIMAL(13, 4),
        `shipping_total` DECIMAL(13, 4),
        `shipping_tax` DECIMAL(13, 4),
        `cart_tax` DECIMAL(13, 4),
        `discount_total` DECIMAL(13, 4),
        `discount_tax` DECIMAL(13, 4),
        `currency` VARCHAR(3),
        `payment_method` VARCHAR(32),
        `date_completed` TIMESTAMP,
        `date_paid` TIMESTAMP,
        `test_order` SMALLINT(1) NOT NULL DEFAULT 0,
        `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `date_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY(`id`),
        FOREIGN KEY (`blog_id`)
            REFERENCES {$blogsTableName} (`blog_id`)
            ON DELETE CASCADE,
        UNIQUE `order_id_blog_id`(`order_id`, `blog_id`)
        );";

        $sqlOrderItem = "CREATE TABLE IF NOT EXISTS {$this->getOrderItemTableName()} (
        `id` INT NOT NULL AUTO_INCREMENT,
        `order_id` INT NOT NULL,
        `product_id` INT,
        `variation_id` INT,
        `name` TEXT,
        `variation_name` TEXT,
        `permalink` VARCHAR(255) DEFAULT NULL,
        `quantity` SMALLINT,
        `subtotal` DECIMAL(13, 4),
        `subtotal_tax` DECIMAL(13, 4),
        `total` DECIMAL(13, 4),
        `total_tax` DECIMAL(13, 4),
        `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `date_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY(`id`),
        FOREIGN KEY (`order_id`)
            REFERENCES {$this->getOrderTableName()} (`id`)
            ON DELETE CASCADE
        );";

        $sqlChangedBlog = "CREATE TABLE IF NOT EXISTS {$this->getChangedBlogTableName()} (
        `id` INT NOT NULL AUTO_INCREMENT,
        `blog_id`  BIGINT(20) NOT NULL,
        `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `date_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY(`id`),
        UNIQUE(`blog_id`)
        );";

        $sqlAuthTokens = "CREATE TABLE IF NOT EXISTS {$this->getAuthTokensTableName()} (
        `id` INT NOT NULL AUTO_INCREMENT,
        `blog_id`  BIGINT(20) NOT NULL,
        `user_id`  BIGINT(20) UNSIGNED NOT NULL,
        `token` VARCHAR(64),
        `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `date_modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY(`id`),
        FOREIGN KEY (`blog_id`)
            REFERENCES {$blogsTableName} (`blog_id`)
            ON DELETE CASCADE,
        FOREIGN KEY (`user_id`)
            REFERENCES {$usersTableName} (`ID`)
            ON DELETE CASCADE
        );";

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        \dbDelta($sqlOrder);
        \dbDelta($sqlOrderItem);
        \dbDelta($sqlChangedBlog);
        \dbDelta($sqlAuthTokens);

        \add_blog_option(1, "supr_reporting_api_tables_version", SUPR_REPORTING_API_VERSION);
    }

    /**
     * Add columns to table
     */
    public function changeTableStructure(): void
    {
        $currentVersion = \get_blog_option(1, 'supr_reporting_api_tables_version_table_version');

        if ($currentVersion === SUPR_REPORTING_API_VERSION) {
            return;
        }

        // Update DB from old version to the last version
        switch ($currentVersion) {
            case '1.4.0':
                $sql = "ALTER TABLE {$this->getOrderTableName()} ADD `test_order` SMALLINT(1) NOT NULL DEFAULT 0 AFTER `date_paid`;";
                $this->db->query($sql);
                break;
        }

        \update_blog_option(1, 'supr_reporting_api_tables_version_table_version', SUPR_REPORTING_API_VERSION);
    }

    /**
     * Convert variable to Datetime
     *
     * @param $value
     * @return \DateTime|null
     */
    public static function convertToDateTime($value): ?\DateTime
    {
        try {
            if ($value instanceof \DateTime) {
                // Nothing :)
            } elseif (is_numeric($value)) {
                $value = new \DateTime($value);
            } elseif (is_string($value)) {
                $value = new \DateTime($value);
            } else {
                $value = null;
            }
        } catch (\Exception $e) {
            $value = null;
        }

        return $value;
    }
}
