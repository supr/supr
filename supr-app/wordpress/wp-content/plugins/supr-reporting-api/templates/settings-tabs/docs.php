<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

use SuprReportingApi\Api;
?>
<p>You can see all routes <a href="/wp-json/<?= API::$namespace; ?>" target="_blank">here</a>.</p>
<h3>Auth</h3>
<p>In odrer to work with API, you need to create your own token (with POST request). Be sure, that the user has <b>manage_options</b> access. Example:</p>
<pre>
curl -d '{"username":"your_login", "password":"your_password"}' \
-H "Content-Type: application/json" \
-X POST /wp-json/<?= API::$namespace; ?>/auth
        </pre>
<p>In the response you will get this data:</p>
<pre>
{
    "success": true,
    "data": {
        "user": id,
        "token": "0e8bb2d5e70ed3194e1cb192f3f7dba1"
    }
}
        </pre>
<p>Then you can use this token for accessing to other methods. For example:</p>
<pre>
curl -H 'Accept: application/json' \
-H "Authorization: Bearer 0e8bb2d5e70ed3194e1cb192f3f7dba1" \
/wp-json/<?= API::$namespace; ?>/orders
        </pre>
<p>The token will expire after 2 hours inactivity.</p>
<h3>Orders routes</h3>
<p><b>1.</b> GET <b><a href="/wp-json/<?= API::$namespace; ?>/orders" target="_blank">/wp-json/<?= API::$namespace; ?>/orders</a></b> shows the first 100 new or updated orders.</p>
<p>In order to get the next orders, you should delete one or all from first 100 orders.</p>
<p><b>2.</b> GET <b>/wp-json/<?= API::$namespace; ?>/orders/&lt;id&gt;</b> shows one order with global id = &lt;id&gt;.</p>
<p><b>3.</b> DELETE <b>/wp-json/<?= API::$namespace; ?>/orders/&lt;id1&gt;-&lt;id2&gt;-&lt;id3&gt;-&lt;id4&gt;</b> removes one ore more orders with global ids = &lt;id&gt;.</p>
<h3>Shops routes</h3>
<p>We don't save this data in a table. In the table of plugin we save only id of shop if shop has been changed. Other information about shop will be generated from WP-Tables if you call API.</p>
<p><b>1.</b> GET <b><a href="/wp-json/<?= API::$namespace; ?>/shops" target="_blank">/wp-json/<?= API::$namespace; ?>/shops</a></b> shows the first 5 new or updated shops.</p>
<p>In order to get the next shops, you should delete one or all from first 5 shops.</p>
<p><b>2.</b> GET <b>/wp-json/<?= API::$namespace; ?>/shops/&lt;id&gt;</b> shows data for shop with id = &lt;id&gt;.</p>
<p>You can access each shop any time per id. The shop will be showed also if you deleted it from the schedule (because we get shop data from WP tables).</p>
<p><b>3.</b> DELETE <b>/wp-json/<?= API::$namespace; ?>/shops/&lt;id1&gt;-&lt;id2&gt;-&lt;id3&gt;-&lt;id4&gt;</b> removes one ore more shops with global ids = &lt;id&gt;.</p>