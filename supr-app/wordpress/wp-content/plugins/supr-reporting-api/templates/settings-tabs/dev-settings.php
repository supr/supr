<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

global $wpdb;

// Handle request
use SuprReportingApi\Manager;

if (isset($_POST['clearAllTables'])) {
    error_log('[SUPR REPORTING API]: the user #' . \get_current_user_id() . ' deleted all rows from all tables.');

    \SuprReportingApi\ChangedBlogRepository::deleteAllChangedBlogs();
    \SuprReportingApi\OrderRepository::deleteAllOrders();
    ?>
    <div class="notice notice-success is-dismissible">
        <p>Tables were cleared.</p>
    </div>
    <?php
}

if (isset($_POST['supr_reporting_api'])) {
    \update_blog_option(1, 'supr_reporting_api', $_POST['supr_reporting_api']);
    // Reload settings
    Manager::instance()->loadSettings();
    ?>
    <div class="notice notice-success is-dismissible">
        <p>Options were saved.</p>
    </div>
    <?php
}

if (isset($_POST['start_blog_id'], $_POST['end_blog_id']) && is_numeric($_POST['start_blog_id']) && is_numeric($_POST['end_blog_id'])) {
    $tableName = $wpdb->prefix . 'blogs';

    $sql = "SELECT * FROM {$tableName} WHERE `blog_id` >= %d AND `blog_id` <= %d";
    $sql = $wpdb->prepare($sql, [$_POST['start_blog_id'], $_POST['end_blog_id']]);
    $blogs = $wpdb->get_results($sql);

    foreach ($blogs as $key => $row) {
        switch_to_blog($row->blog_id);

        // Update all shops
        Manager::instance()->updateBlog($row->blog_id);
        //print_r("\nShop #{$row->blog_id} was inserted.\n");

        $query = new \WC_Order_Query(
            [
                'limit' => -1,
                'return' => 'ids'
            ]
        );
        $orders = $query->get_orders();

        // Update all orders
        foreach ($orders as $orderId) {
            Manager::instance()->updateOrder($orderId);
            //print_r("Order #{$order->get_id()} of shop #{$row->blog_id} was inserted.\n");
            sleep(0.125);
        }

        restore_current_blog();
    }
    ?>
    <div class="notice notice-success is-dismissible">
        <p>Data was imported.</p>
    </div>
    <?php
}

?>
<h3>Settings</h3>
<form action="" method="POST">
    <table class="form-table">
        <tr>
            <td><label for="supr_reporting_api_test_prefix">Test prefix for blogs domain</label></td>
            <td><input type="text" id="supr_reporting_api_test_prefix" name="supr_reporting_api[test_prefix]" value="<?= Manager::instance()->testPrefix; ?>"/></td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>

<h3>Data state</h3>
<form action="" method="POST" onSubmit="return confirm('Are you sure you wish to delete all rows in all API tables?');">
    <table class="form-table">
        <tr>
            <td>Changed shops:</td>
            <td><?= \SuprReportingApi\ChangedBlogRepository::getTotalChangedBlogCount(); ?></td>
        </tr>
        <tr>
            <td>New orders:</td>
            <td><?= \SuprReportingApi\OrderRepository::getTotalOrdersCount(); ?></td>
        </tr>
        <tr>
            <td>
                <input name="clearAllTables" value="1" type="hidden">
                <input type="submit" class="button button-primary" value="Clear all tables">
            </td>
        </tr>
    </table>
</form>

<h3>New import</h3>
<form action="" method="POST" onSubmit="return confirm('Are you sure you wish to import new data?');">
    <table class="form-table">
        <tr>
            <td><label for="start_blog_id">Start blog id</label></td>
            <td><input type="text" id="start_blog_id" name="start_blog_id" placeholder="1"/></td>
        </tr>
        <tr>
            <td><label for="end_blog_id">End blog id</label></td>
            <td><input type="text" id="end_blog_id" name="end_blog_id" placeholder="100"/></td>
        </tr>
        <tr>
            <td scope="row" colspan="2"><input type="submit" class="button button-primary" value="Import new data"></td>
        </tr>
    </table>
</form>