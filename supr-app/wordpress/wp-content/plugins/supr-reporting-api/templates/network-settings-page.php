<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$tabs = [
    'docs' => 'Documentation',
    'dev-settings' => 'Dev settings'
];

$currentTab = (isset($_GET['tab'], $tabs[$_GET['tab']])) ? $_GET['tab'] : 'docs';
?>

<div class="wrap">
    <h1><?= get_admin_page_title(); ?></h1>
    <h2 class="nav-tab-wrapper">
        <?php foreach ($tabs as $tab => $tabName) : ?>
            <a class="nav-tab<?= $currentTab === $tab ? ' nav-tab-active' : ''; ?>" href="?page=<?= $_GET['page']?>&tab=<?= $tab; ?>"><?= $tabName; ?></a>
        <?php endforeach; ?>
    </h2>

    <?php
    // Include tabs template
    include SUPR_REPORTING_API_PATH . "templates/settings-tabs/{$currentTab}.php";
    ?>
</div>
