<?php
/** @var \SuprAdmanager\Item[] $items */

foreach ($items as $item) :
    $cookieName = 'supr_admanager_sticky_' . $item->getId();
    if (isset($_COOKIE[$cookieName])) {
        continue;
    }
    ?>
    <div class="supr-admanager-sticky animated" id="supr-admanager-sticky">
        <div class="supr-admanager-sticky-close">
            <span class="dashicons dashicons-no-alt"></span>
        </div>
        <a href="<?= $item->getLink(); ?>" target="<?= $item->getTarget(); ?>">
            <img src="<?= $item->getImgSrc(); ?>" alt="<?= $item->getTitle(); ?>">
        </a>
    </div>
    <script type="text/javascript">
        (function ($) {
            // Show
            setTimeout(function () {
                $('#supr-admanager-sticky').addClass('fadeInUp');
            }, 1000);

            // Close for 24 hour
            $('#supr-admanager-sticky').click(function () {
                var supr_admanager_sticky_cookie_name = '<?= $cookieName ?>';

                $('#supr-admanager-sticky').hide();
                var date = new Date();
                var minutes = 60 * 24;
                date.setTime(date.getTime() + (minutes * 60 * 1000));

                document.cookie = supr_admanager_sticky_cookie_name + "=1; expires=" + date.toUTCString() + "; path=/";
            });
        })(jQuery);
    </script>
    <?php break;
endforeach; ?>
