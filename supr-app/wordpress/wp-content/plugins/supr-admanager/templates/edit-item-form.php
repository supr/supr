<?php
/** @var \SuprAdmanager\Item $item */
use SuprAdmanager\ItemManager;

?>
<form method="post" enctype="multipart/form-data">
    <div class="postbox">
        <div class="inside">
            <h2 class="text-uppercase">
                <span>
                    <?= ($item->getId() > 0 ? __('Edit item', 'supr-admanager') . ' <b>' . $item->getTitle() . '</b>' : __('Create new item', 'supr-admanager')); ?>
                </span>
            </h2>
            <hr>
            <div class="row">
                <input type="hidden" name="id" value="<?= $item->getId() ?? ''; ?>">
                <div class="wu-col-md-6">
                    <label><?= __('Name', 'supr-admanager'); ?>
                        <input type="text" name="title" value="<?= $item->getTitle() ?? ''; ?>">
                    </label>

                    <label><?= __('Start date', 'supr-admanager'); ?>
                        <input type="date" name="start_date" value="<?= $item->getStartDate() ? $item->getStartDate()->format('Y-m-d') : ''; ?>"></label>

                    <label><?= __('Link', 'supr-admanager'); ?>
                        <input type="text" name="link" value="<?= $item->getLink() ?? ''; ?>"></label>

                    <label><?= __('Position', 'supr-admanager'); ?>
                        <input type="text" name="position" value="<?= $item->getPosition() ?? '100'; ?>"></label>

                    <label><?= __('Language', 'supr-admanager'); ?>
                        <input type="text" name="lang" value="<?= $item->getLang() ?? 'de_DE'; ?>"></label>

                    <label><?= __('Pages', 'supr-admanager'); ?>
                        <select name="pages[]" size="4" multiple="multiple">
                            <?php foreach (ItemManager::getPages() as $value => $name) : ?>
                                <option value="<?= $value; ?>" <?= \in_array($value, $item->getPages(), true) ? ' selected' : ''; ?>>
                                    <?= $name; ?>
                                </option>
                            <?php endforeach; ?>
                        </select></label>

                    <label><?= __('Packages', 'supr-admanager'); ?>
                        <select name="packages[]" size="4" multiple="multiple">
                            <?php foreach (ItemManager::getSubscriptionPackages() as $name) : ?>
                                <option value="<?= $name; ?>" <?= \in_array($name, $item->getPackages(), true) ? ' selected' : ''; ?>>
                                    <?= __(ucfirst($name), 'supr-admanager'); ?>
                                </option>
                            <?php endforeach; ?>
                        </select></label>
                </div>
                <div class="wu-col-md-6">
                    <label><?= __('Type', 'supr-admanager'); ?>
                        <select name="type">
                            <?php foreach (ItemManager::getItemTypes() as $value => $name) : ?>
                                <option value="<?= $value; ?>" <?= $item->getType() === $value ? ' selected' : ''; ?>>
                                    <?= $name; ?>
                                </option>
                            <?php endforeach; ?>
                        </select></label>

                    <label><?= __('End date', 'supr-admanager'); ?>
                        <input type="date" name="end_date" value="<?= $item->getEndDate() ? $item->getEndDate()->format('Y-m-d') : ''; ?>"></label>

                    <label><?= __('Target', 'supr-admanager'); ?>
                        <select name="target">
                            <option value="_blank"<?= $item->getTarget() === '_blank' ? ' selected' : ''; ?>><?= __('New window', 'supr-admanager'); ?></option>
                            <option value="_self"<?= $item->getTarget() === '_self' ? ' selected' : ''; ?>><?= __('Current window', 'supr-admanager'); ?></option>
                        </select></label>

                    <label><?= __('Active', 'supr-admanager'); ?>
                        <input type="checkbox" name="active" value="1"<?= ($item->isActive() ? ' checked' : ''); ?>></label>

                    <input id="supr-admanager-select-image" type="hidden" name="img_src" value="<?= $item->getImgSrc() ?? ''; ?>">
                    <img src="<?= ($item->getImgSrc() ?? ''); ?>" class="preview" style="display: <?= ($item->getImgSrc() ? 'block' : 'none'); ?>;">
                    <button id="supr-admanager-select-image-button" class="button button-secondary"><?= __('Select image', 'supr-admanager'); ?></button>
                </div>
            </div>
        </div>
    </div>
    <div class="postbox postbox-bar">
        <div class="inside">
            <button class="button button-primary"><?= __('Save changes', 'supr-admanager'); ?></button>
        </div>
    </div>
</form>