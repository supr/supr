<?php
// Settings page
use SuprAdmanager\Item;
use SuprAdmanager\ItemManager;
?>
<div class="wrap supr-admanager settings">
    <h1 class="wp-heading-inline"><?= get_admin_page_title(); ?></h1>

    <?php if (is_multisite() && get_current_blog_id() !== 1) : ?>
        <div class="error notice">
            <p><?= __('In multisite mode, the plugin can only be configured in the main blog.', 'supr-admanager'); ?></p>
        </div>
    <?php else : ?>
        <?php

        $currentTab = $_GET['tab'] ?? key(ItemManager::getItemTypes());

        $itemManager = ItemManager::getInstance();

        // Handle GET data
        if (isset($_GET['action'])) {
            // Handle POST data
            if (isset($_POST['title'])) {
                $itemManager->processPostData();
            } else {
                switch ($_GET['action']) {
                    case 'add':
                    case 'edit':
                        $item = isset($_GET['id']) ? $itemManager->getItemById($_GET['id']) : new Item();
                        include SUPR_ADMANAGER_PATH . 'templates/edit-item-form.php';
                        break;
                    case 'delete':
                        $itemManager->delete($_GET['id']);
                        break;
                }
            }
        }
        ?>
        <!-- TABS -->
        <div id="elementor-template-library-tabs-wrapper" class="nav-tab-wrapper">
            <?php
            $menuTabs = [];

            foreach (ItemManager::getItemTypes() as $index => $name) {
                $menuTabs[$index] = [
                    'name' => $name,
                    'active' => $currentTab === $index
                ];
            }

            $queryParams = ['page' => 'supr_admanager_settings'];

            foreach ($menuTabs as $menuTabSlug => $menuTab) { ?>
                <a class="nav-tab<?= $menuTab['active'] ? ' nav-tab-active' : '' ?>" href="?<?= http_build_query(array_merge($queryParams, ['tab' => $menuTabSlug])); ?>"><?= $menuTab['name']; ?></a>
            <?php } ?>
        </div>
        <?php
        // Show inhalt of tabs
        $items = ItemManager::getInstance()->getItems($currentTab);
        include SUPR_ADMANAGER_PATH . 'templates/edit-items-list.php';
        ?>
    <?php endif; ?>

    <?php if (class_exists('\SuprUpsale\Plugin')) : ?>
        <h2><?= __('You have also SUPR-UPSALE plugin', 'supr-admanager'); ?></h2>
        <p>* <?= __('To make a link to UPSALE, insert #show-upsale instead of a link or at the end of the link. The link can be opened both in the new window and in the current one.', 'supr-admanager'); ?></p>
        <p><?= __('You can also send a coupon to upsale. To do it, use next hash: #show-upsale_coupon-LALA.', 'supr-admanager'); ?></p>
        <p>** <?= __('Please do not forget that only clients without SMART and PRO are having UPSALE.', 'supr-admanager'); ?></p>
    <?php endif; ?>
</div>