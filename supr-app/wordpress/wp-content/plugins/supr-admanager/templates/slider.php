<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/** @var \SuprAdmanager\Item[] $items */

if (count($items) > 0) : ?>
<div class="wrap">
    <div id="supr-dashboard-slider">
        <?php foreach ($items as $item) : ?>
            <div>
                <div class="dashboard-slide-container">
                    <div class="dashboard-slide-inner">
                        <a href="<?= $item->getLink(); ?>" target="<?= $item->getTarget(); ?>">
                            <img src="<?= $item->getImgSrc(); ?>" alt="<?= $item->getTitle(); ?>">
                        </a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>