<?php
/** @var \SuprAdmanager\Item[] $items */

foreach ($items as $item) :
    $cookieName = 'supr_admanager_overlay_' . $item->getId();
    if (isset($_COOKIE[$cookieName])) {
        continue;
    }

    // @todo we don't need it if ssl sertificate is actuel.
    $image = str_replace('https://', 'http://', $item->getImgSrc());

    // Get width of image
    list($width, $height, $type, $attr) = getimagesize($image);
    ?>

    <script type="text/javascript">
        (function ($) {
            setTimeout(function () {
                Swal.fire({
                    width: (<?= $width; ?> + 16) + 'px',
                    padding: '0px 8px 4px 8px',
                    customClass: 'supr-admanager-overlay animated flipInY',
                    showConfirmButton: false,
                    showCancelButton: false,
                    showCloseButton: true,
                    html: '<a id="supr_admanager_overlay_link" href="<?= $item->getLink(); ?>" target="<?= $item->getTarget(); ?>"><img style="width: 100%;" src="<?= $item->getImgSrc(); ?>" alt="<?= $item->getTitle(); ?>"></a>',
                    showClass: {
                        popup: '',
                        backdrop: '',
                        icon: '',
                    },
                    hideClass: {
                        popup: '',
                        backdrop: '',
                        icon: '',
                    }
                }).then(function (object) {
                    supr_admanager_set_cookies();
                });
            }, 5000);

            $('#supr_admanager_overlay_link').live("click", function() {
                supr_admanager_set_cookies();
                swal.close();
            });

            var supr_admanager_set_cookies = function() {
                var supr_admanager_overlay_cookie_name = '<?= $cookieName ?>';

                $('#supr-admanager-sticky').hide();
                var date = new Date();
                var minutes = 60 * 24;
                date.setTime(date.getTime() + (minutes * 60 * 1000));

                document.cookie = supr_admanager_overlay_cookie_name + "=1; expires=" + date.toUTCString() + "; path=/";
            }
        })(jQuery);
    </script>
    <?php break;
endforeach; ?>
