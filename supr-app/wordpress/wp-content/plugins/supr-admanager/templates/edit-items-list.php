<?php
/** @var \SuprAdmanager\Item[] $items */
use SuprDesignStore\DesignElementManager;

/** @var string $currentTab */
?>
<table class="widefat">
    <thead>
    <tr>
        <th class="row-title" style="width: 50px"><?= __('ID', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 120px"><?= __('Preview', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Title', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 200px"><?= __('Period', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 100px"><?= __('Actions', 'supr-design-store'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $queryParams = [
        'delete' => [
            'page' => 'supr_admanager_settings',
            'action' => 'delete',
            'tab' => $currentTab
        ],
        'edit' => [
            'page' => 'supr_admanager_settings',
            'action' => 'edit',
            'tab' => $currentTab
        ],
        'add' => [
            'page' => 'supr_admanager_settings',
            'action' => 'add',
            'tab' => $currentTab
        ]
    ];

    foreach ($items as $item) {
        ?>
        <tr>
            <td class="row-title"><?= $item->getId(); ?></td>
            <td>
                <a href="<?= $item->getImgSrc(); ?>" target="_blank">
                    <img src="<?= $item->getImgSrc(); ?>" style="width: 100px;"/>
                </a>
            </td>
            <td>
                <p>
                    <a href="?<?= http_build_query(array_merge($queryParams['edit'], ['id' => $item->getId()])); ?>">
                        <strong><?= __($item->getTitle(), 'supr-design-store'); ?></strong>
                    </a>
                </p>
                <small><?= $item->isActive() ? __('active', 'supr-design-store') : __('not active', 'supr-design-store'); ?></small>
            </td>
            <td><?= (($item->getStartDate() ? __('from ', 'supr-design-store') . $item->getStartDate()->format('d.m.Y') : '') . ($item->getEndDate() ? __(' till ', 'supr-design-store') . $item->getEndDate()->format('d.m.Y') : '')); ?></td>
            <td>
                <a class="button button-secondary edit" href="?<?= http_build_query(array_merge($queryParams['edit'], ['id' => $item->getId()])); ?>">
                    <span class="dashicons dashicons-edit"></span>
                </a>
                <a class="button button-secondary delete" href="?<?= http_build_query(array_merge($queryParams['delete'], ['id' => $item->getId()])); ?>">
                    <span class="dashicons dashicons-post-trash"></span>
                </a>
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
    <tfoot>
    <tr>
        <th class="row-title"><?= __('ID', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Preview', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Title', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Period', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Actions', 'supr-design-store'); ?></th>
    </tr>
    </tfoot>
</table>
<div class="postbox">
    <div class="inside">
        <a href="?<?= http_build_query($queryParams['add']); ?>" class="button button-primary"><?= __('Create new item', 'supr-design-store'); ?></a>
    </div>
</div>