jQuery(document).ready(function () {
    jQuery('#supr-dashboard-slider').slick({
        adaptiveHeight: true,
        dots: true,
        arrows: true,
        infinite: true,
        speed: 500,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    slidesToShow: 1
                }
        },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    slidesToShow: 1
                }
        }
        ]
    }).slideDown('slow');
});
