(function ($) {
    // Select image button
    $('#supr-admanager-select-image-button').click(function (event) {
        event.preventDefault();
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this);
        var input_preview = button.attr('id').replace('-button', '');

        wp.media.editor.send.attachment = function (props, attachment) {
            $("#" + input_preview).val(attachment.url);
            $(button).parent().find('img.preview').attr('src', attachment.url).show();
            wp.media.editor.send.attachment = send_attachment_bkp;
        };

        wp.media.editor.open(button);
        return false;
    });

    // Ask before delete
    $('.supr-admanager a.delete').click(function () {
        return window.confirm("Are you sure?");
    });

    // Show preview
    $('.supr-admanager-preview').click(function () {
        Swal.fire({
            imageUrl: $(this).data('preview'),
            /*imageHeight: 600,*/
            animation: false
        })
    });
})(jQuery);