<?php

namespace SuprAdmanager;

/**
 * Class AdManager
 *
 * @package SuprAdmanager
 */
class AdManager
{
    /**
     * @var \wpdb
     */
    private $db;

    /**
     * @var string
     */
    private $tableName;

    /**
     * @var array
     */
    public $shopsWithoutPlan = [1, 8];

    /**
     * @var AdManager
     */
    private static $instance;

    /**
     * @return \SuprAdmanager\AdManager
     */
    public static function getInstance(): AdManager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * AdManager constructor.
     */
    private function __construct()
    {
        global $wpdb;

        $this->db = $wpdb;
        $this->tableName = $this->db->base_prefix . 'supr_admanager_item';
    }

    /**
     * Get items
     *
     * @param string $type
     * @return array
     * @throws \Exception
     */
    public function getItems($type = 'dashboard'): array
    {
        $items = [];

        // Query
        $sql = "SELECT * FROM {$this->tableName} WHERE " . $this->getRestrictions($type, $type !== 'dashboard');

        // Debug
        //error_log($sql);

        $result = $this->db->get_results($sql, ARRAY_A);

        if (\is_array($result)) {
            foreach ($result as $row) {
                $items[] = new Item($row);
            }
        }

        return $items;
    }

    /**
     * Get WHERE for query
     *
     * @param      $type
     * @param bool $checkPage
     * @return string
     * @throws \Exception
     */
    private function getRestrictions($type, $checkPage = false): string
    {
        // Status
        $sql = '`active` = 1';

        // Type
        $sql .= ' AND `type` = %s';
        $sql = $this->db->prepare($sql, [$type]);

        $shopId = get_current_blog_id();

        // Package
        $wuPlan = wu_get_account_plan($shopId);
        if (!$wuPlan) {
            // We don't write logs about the system blogs
            if (!\in_array($shopId, $this->shopsWithoutPlan, true)) {
                error_log('[Admanager] Plan for shop #' . $shopId . ' doesn\'t exist.');
            }
            $sql .= ' AND `packages` IS NULL';
        } else {
            $sql .= ' AND (json_contains(`packages`, \'["' . strtoupper($wuPlan) . '"]\') OR `packages` IS NULL)';
        }

        // Time
        $sql .= ' AND (`start_date` <= \'' . (new \DateTime())->format('Y-m-d') . '\' OR `start_date` IS NULL)';
        $sql .= ' AND (`end_date` >= \'' . (new \DateTime())->format('Y-m-d') . '\' OR `end_date` IS NULL)';

        // Page
        if ($checkPage) {
            // @todo
        }

        // Language
        $sql .= ' AND (`lang` like \'' . get_locale() . '\' OR `lang` IS NULL)';

        // Ordering
        $sql .= ' ORDER BY `position`';

        return $sql;
    }
}
