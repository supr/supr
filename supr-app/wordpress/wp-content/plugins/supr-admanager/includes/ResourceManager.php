<?php

namespace SuprAdmanager;

/**
 * Class ResourceManager
 *
 * @package SuprAdmanager
 */
class ResourceManager
{
    /**
     * Register files for plugin
     */
    public static function addAdminResources(): void
    {
        // Slick slider asset
        \wp_enqueue_style('supr_dashboard_asset_slick_style', SUPR_ADMANAGER_URL . 'assets/slick/slick.css', [], SUPR_ADMANAGER_VERSION);
        \wp_enqueue_style('supr_dashboard_asset_slick_theme', SUPR_ADMANAGER_URL . 'assets/slick/slick-theme.css', [], SUPR_ADMANAGER_VERSION);
        \wp_enqueue_script('supr_dashboard_asset_slick_script', SUPR_ADMANAGER_URL . 'assets/slick/slick.min.js', ['jquery'], SUPR_ADMANAGER_VERSION, true);
        \wp_enqueue_style('supr_admanager_css', SUPR_ADMANAGER_URL . 'css/supr-admanager.css', [], '1.0');

        // SUPR slider script
        \wp_enqueue_script('supr_dashboard_slider_script', SUPR_ADMANAGER_URL . 'js/dashboard-slider.js', ['jquery'], SUPR_ADMANAGER_VERSION, true);

        // Settings page
        if (isset($_GET['page']) && $_GET['page'] === 'supr_admanager_settings') {
            // We need JS Media API
            \wp_enqueue_media();
            \wp_enqueue_style('supr_admanager_admin_css', SUPR_ADMANAGER_URL . 'css/supr-admanager-admin.css', [], SUPR_ADMANAGER_VERSION);
            \wp_enqueue_script('supr_admanager_admin_js', SUPR_ADMANAGER_URL . 'js/supr-admanager-admin.js', ['jquery'], SUPR_ADMANAGER_VERSION, true);
        }
    }
}
