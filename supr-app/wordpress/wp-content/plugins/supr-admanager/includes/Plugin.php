<?php

namespace SuprAdmanager;

/**
 * Class Plugin
 *
 * @package SuprAdmanager
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Init
     */
    public function adminInit(): void
    {
        \add_action('admin_enqueue_scripts', [ResourceManager::class, 'addAdminResources']);
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        ItemManager::getInstance()->changeTableStructur();

        Menu::addMenu();

        \add_action('admin_init', [$this, 'adminInit'], 0);

        \add_action('admin_notices', [$this, 'showDashboardSlider']);

        \add_action('admin_notices', [$this, 'showSticky']);

        \add_action('admin_notices', [$this, 'showOverlay']);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_ADMANAGER_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }

    /**
     * Create new tables and write
     */
    public static function activationHook(): void
    {
        ItemManager::getInstance()->createTable();
    }

    /**
     * Render Slider
     */
    public function showDashboardSlider(): void
    {
        if (!\apply_filters('supr-admanager-show-slider', true)) {
            return;
        }

        $screen = get_current_screen();

        if ($screen !== null && $screen->id === 'dashboard') {
            $items = AdManager::getInstance()->getItems();
            require SUPR_ADMANAGER_PATH . 'templates/slider.php';
        }
    }

    /**
     * Render Slider
     */
    public function showSticky(): void
    {
        $items = AdManager::getInstance()->getItems('sticky');
        require SUPR_ADMANAGER_PATH . 'templates/sticky.php';
    }

    /**
     * Render Slider
     */
    public function showOverlay(): void
    {
        $items = AdManager::getInstance()->getItems('overlay');
        require SUPR_ADMANAGER_PATH . 'templates/overlay.php';
    }
}

Plugin::instance();
