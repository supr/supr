<?php

namespace SuprAdmanager;

/**
 * Class ItemManager
 *
 * @package SuprAdmanager
 */
class ItemManager
{
    /**
     * @var \wpdb
     */
    private $db;

    /**
     * @var string
     */
    private $tableName;

    /**
     * @var ItemManager
     */
    private static $instance;

    /**
     * @return \SuprAdmanager\ItemManager
     */
    public static function getInstance(): ItemManager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * ItemManager constructor.
     */
    private function __construct()
    {
        global $wpdb;

        $this->db = $wpdb;
        $this->tableName = $this->db->base_prefix . 'supr_admanager_item';
    }

    /**
     * Get all types of exist items.
     *
     * @return array
     */
    public static function getItemTypes(): array
    {
        return [
            'overlay' => __('Overlay', 'supr-admanager'),
            'sticky' => __('Sticky', 'supr-admanager'),
            'dashboard' => __('Dashboard', 'supr-admanager'),
        ];
    }

    /**
     * Get existing packages
     *
     * @return array
     */
    public static function getSubscriptionPackages(): array
    {
        $subscriptions = get_posts(['post_type' => 'wpultimo_plan', 'numberposts' => 0]);
        $returned = [];
        foreach ($subscriptions as $subscription) {
            $returned[] = strtoupper($subscription->post_title);
        }

        return $returned;
    }

    /**
     * Get all types of exist items.
     *
     * @return array
     */
    public static function getPages(): array
    {
        return [
            /*'index.php' => __('Dashboard', 'supr-admanager'),
            'edit.php?post_type=product' => __('Products', 'supr-admanager'),
            'edit-tags.php?taxonomy=product_cat&post_type=product' => __('Catefories', 'supr-admanager'),*/
        ];
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * Create table for inems if not exist
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS {$this->tableName} (
            `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
            `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `img_src` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `active` tinyint(1) DEFAULT 1,
            `start_date` timestamp NULL DEFAULT NULL,
            `end_date` timestamp NULL DEFAULT NULL,
            `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `target` varchar(16) COLLATE utf8_unicode_ci DEFAULT '_blank',
            `lang` varchar(128) COLLATE utf8_unicode_ci DEFAULT 'en_US',
            `pages` JSON DEFAULT NULL,
            `packages` JSON DEFAULT NULL,
            `type` varchar(32) COLLATE utf8_unicode_ci DEFAULT 'dashboard',
            `options` JSON DEFAULT NULL,
            `position` bigint(20) DEFAULT NULL,
            `time_stamp` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
            PRIMARY KEY(`id`),
            INDEX (`type`)
        );";

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        dbDelta($sql);

        add_blog_option(1, "{$this->tableName}_table_version", SUPR_ADMANAGER_VERSION);
    }

    /**
     * Migrate DB
     */
    public function changeTableStructur(): void
    {
        switch (\get_blog_option(1, "{$this->tableName}_table_version")) {
            case '1.0.0':
                $sql = "ALTER TABLE {$this->tableName} ADD INDEX `item_type`(`type`);";
                $this->db->query($sql);
                break;
        }

        \update_blog_option(1, "{$this->tableName}_table_version", SUPR_ADMANAGER_VERSION);
    }

    /**
     * @param null $type
     * @return Item[]
     */
    public function getItems($type = null): array
    {
        $returned = [];

        $sql = "SELECT * FROM {$this->tableName} WHERE 1";

        if ($type !== null) {
            $sql .= ' AND `type` like %s';
            $sql = $this->db->prepare($sql, [$type]);
        }

        $sql .= ' ORDER BY `position`';

        $elements = $this->db->get_results($sql, ARRAY_A);

        if ($elements) {
            foreach ($elements as $element) {
                $returned[] = new Item($element);
            }
        }

        return $returned;
    }

    /**
     * @param $id
     * @return null|Item
     */
    public function getItemById($id): ?Item
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE `id` = %d";
        $sql = $this->db->prepare($sql, [$id]);
        $items = $this->db->get_results($sql, ARRAY_A);

        return isset($items[0]) ? new Item($items[0]) : null;
    }

    /**
     * Save item to DB
     *
     * @param \SuprAdmanager\Item $item
     * @throws \Exception
     */
    public function save($item): void
    {
        $data = $item->serialize();

        // We need all of empty fields as NULL;
        $data['pages'] = empty($data['pages']) ? null : json_encode($data['pages']);
        $data['packages'] = empty($data['packages']) ? null : json_encode($data['packages']);
        $data['options'] = empty($data['options']) ? null : json_encode($data['options']);
        $data['start_date'] = $data['start_date'] instanceof \DateTime ? $data['start_date']->format('Y-m-d') : null;
        $data['end_date'] = $data['end_date'] instanceof \DateTime ? $data['end_date']->format('Y-m-d') : null;

        if ($item->getId() > 0) {
            unset($data['id']);
            $data['time_stamp'] = $data['time_stamp'] instanceof \DateTime ? $data['time_stamp']->format('Y-m-d') : null;
            $this->db->update($this->tableName, $data, ['id' => $item->getId()]);
        } else {
            $data['time_stamp'] = (new \DateTime())->format('Y-m-d');
            $this->db->insert($this->tableName, $data);
        }
    }

    /**
     * @param $id
     */
    public function delete($id): void
    {
        $this->db->delete($this->tableName, ['id' => $id]);
    }

    /**
     * @return bool
     */
    public function processPostData(): bool
    {
        if (isset($_POST['id']) && (int)$_POST['id'] > 0) {
            $item = $this->getItemById($_POST['id']);
        } else {
            $item = new Item();
        }

        $item->unserialize($_POST);

        $this->save($item);

        return true;
    }
}
