<?php

namespace SuprAdmanager;

/**
 * Class Autoloader
 *
 * @package SuprAdmanager
 */
class Autoloader
{
    /**
     * Run autoloader.
     *
     * Register a function as `__autoload()` implementation.
     *
     * @access public
     * @static
     */
    public static function run()
    {
        spl_autoload_register([__CLASS__, 'autoload']);
    }

    /**
     * Autoload.
     *
     * For a given class, check if it exist and load it.
     *
     * @access private
     * @static
     *
     * @param string $class Class name.
     */
    private static function autoload($class)
    {
        $realClassName = str_replace(__NAMESPACE__ . '\\', '', $class);

        $filePath = SUPR_ADMANAGER_PATH . 'includes/' . $realClassName . '.php';

        if (file_exists($filePath) && is_readable($filePath)) {
            require_once $filePath;
        }
    }
}
