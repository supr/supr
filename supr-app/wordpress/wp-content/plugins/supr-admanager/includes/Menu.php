<?php

namespace SuprAdmanager;

/**
 * Class Menu
 *
 * @package SuprAdmanager
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add menu page
        add_action('network_admin_menu', [__CLASS__, 'createMenu']);

        // Add settings link on plugin page
        add_filter('network_admin_plugin_action_links_' . SUPR_ADMANAGER_FILE, [__CLASS__, 'addSettingsPage']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createMenu(): void
    {
        add_menu_page(
            __('Admanager', 'supr-design-store'),
            __('Admanager', 'supr-design-store'),
            'manage_network',
            'supr_admanager_settings',
            [Page::class, 'settingsPage'],
            'dashicons-megaphone',
            202
        );
    }

    /**
     * @param $links
     * @return mixed
     */
    public static function addSettingsPage($links)
    {
        $settings_link = '<a href="admin.php?page=supr_admanager_settings">' . __('Settings', 'supr-admanager') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
