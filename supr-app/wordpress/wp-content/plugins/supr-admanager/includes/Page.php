<?php

namespace SuprAdmanager;

/**
 * Class Page
 *
 * @package SuprAdmanager
 */
class Page
{
    /**
     * Show setting page
     */
    public static function settingsPage(): void
    {
        include SUPR_ADMANAGER_PATH . 'templates/settings-page.php';
    }
}
