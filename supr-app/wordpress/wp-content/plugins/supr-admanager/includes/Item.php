<?php

namespace SuprAdmanager;

/**
 * Class Item
 *
 * @package SuprAdmanager
 */
class Item
{
    /** @var  integer */
    private $id;

    /** @var  string */
    private $title;

    /** @var  string */
    private $imgSrc;

    /** @var  bool */
    private $active;

    /** @var  \DateTime */
    private $startDate;

    /** @var  \DateTime */
    private $endDate;

    /** @var  string */
    private $link;

    /** @var  string */
    private $target;

    /** @var  string */
    private $lang;

    /** @var  array */
    private $pages;

    /** @var  array */
    private $packages;

    /** @var  string */
    private $type;

    /** @var  array */
    private $options;

    /** @var  integer */
    private $position;

    /** @var  \DateTime */
    private $timeStamp;

    /**
     * @var array
     */
    private $modified = [];

    /**
     * Item constructor.
     *
     * @param null $properties
     */
    public function __construct($properties = null)
    {
        if ($properties !== null) {
            $this->unserialize($properties);
        }
    }

    /**
     * @param $properties
     * @return Item
     */
    public function unserialize($properties): self
    {
        if (isset($properties['id'])) {
            $this->setId((int)$properties['id']);
        }

        $this->setTitle($properties['title'] ?? null);

        $this->setImgSrc($properties['img_src'] ?? null);

        $this->setActive((bool)($properties['active'] ?? 0));

        if (isset($properties['start_date'])) {
            if (empty($properties['start_date'])) {
                $this->setStartDate(null);
            } elseif ($properties['start_date'] instanceof \DateTime) {
                $this->setStartDate($properties['start_date']);
            } else {
                $this->setStartDate(new \DateTime($properties['start_date']));
            }
        }

        if (isset($properties['end_date'])) {
            if (empty($properties['end_date'])) {
                $this->setEndDate(null);
            } elseif ($properties['end_date'] instanceof \DateTime) {
                $this->setEndDate($properties['end_date']);
            } else {
                $this->setEndDate(new \DateTime($properties['end_date']));
            }
        }

        $this->setLink($properties['link'] ?? null);

        $this->setTarget($properties['target'] ?? null);

        $this->setPages($properties['pages'] ?? null);

        $this->setPackages($properties['packages'] ?? null);

        $this->setType($properties['type'] ?? null);

        $this->setOptions($properties['options'] ?? null);

        $this->setPosition((int)$properties['position']);

        if (isset($properties['time_stamp'])) {
            if (empty($properties['time_stamp'])) {
                $this->setTimeStamp(null);
            } elseif ($properties['time_stamp'] instanceof \DateTime) {
                $this->setTimeStamp($properties['time_stamp']);
            } else {
                $this->setTimeStamp(new \DateTime($properties['time_stamp']));
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'img_src' => $this->getImgSrc(),
            'active' => $this->isActive(),
            'start_date' => $this->getStartDate(),
            'end_date' => $this->getEndDate(),
            'link' => $this->getLink(),
            'target' => $this->getTarget(),
            'lang' => $this->getLang(),
            'pages' => $this->getPages(),
            'packages' => $this->getPackages(),
            'type' => $this->getType(),
            'options' => $this->getOptions(),
            'position' => $this->getPosition(),
            'time_stamp' => $this->getTimeStamp(),
        ];
    }

    /**
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Item $this
     */
    public function setId($id): self
    {
        if ($this->id !== $id) {
            $this->id = $id;
            $this->setModified('id', $this->id);
        }

        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Item $this
     */
    public function setTitle($title): self
    {
        if ($this->title !== $title) {
            $this->title = $title;
            $this->setModified('title', $this->title);
        }

        return $this;
    }

    /**
     * @return null|string
     */
    public function getImgSrc(): ?string
    {
        return $this->imgSrc;
    }

    /**
     * @param string $imgSrc
     *
     * @return Item $this
     */
    public function setImgSrc($imgSrc): self
    {
        if ($this->imgSrc !== $imgSrc) {
            $this->imgSrc = $imgSrc;
            $this->setModified('imgSrc', $this->imgSrc);
        }

        return $this;
    }

    /**
     * @return null|bool
     */
    public function isActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return Item $this
     */
    public function setActive($active): self
    {
        if ($this->active !== $active) {
            $this->active = $active;
            $this->setModified('active', $this->active);
        }

        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     *
     * @return Item $this
     */
    public function setStartDate($startDate): self
    {
        if ($this->startDate !== $startDate) {
            $this->startDate = $startDate;
            $this->setModified('startDate', $this->startDate);
        }

        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     *
     * @return Item $this
     */
    public function setEndDate($endDate): self
    {
        if ($this->endDate !== $endDate) {
            $this->endDate = $endDate;
            $this->setModified('endDate', $this->endDate);
        }

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string $link
     *
     * @return Item $this
     */
    public function setLink($link): self
    {
        if ($this->link !== $link) {
            $this->link = $link;
            $this->setModified('link', $this->link);
        }

        return $this;
    }

    /**
     * @return null|string
     */
    public function getTarget(): ?string
    {
        return $this->target;
    }

    /**
     * @param string $target
     *
     * @return Item $this
     */
    public function setTarget($target): self
    {
        if ($this->target !== $target) {
            $this->target = $target;
            $this->setModified('target', $this->target);
        }

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLang(): ?string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     *
     * @return Item $this
     */
    public function setLang($lang): self
    {
        if ($this->lang !== $lang) {
            $this->lang = $lang;
            $this->setModified('lang', $this->lang);
        }

        return $this;
    }

    /**
     * @return null|array
     */
    public function getPages(): ?array
    {
        return json_decode($this->pages ?? '[]');
    }

    /**
     * @param array|string $pages
     *
     * @return Item $this
     */
    public function setPages($pages): self
    {
        $pages = $this->toJson($pages);

        if ($this->pages !== $pages) {
            $this->pages = $pages;
            $this->setModified('pages', $this->pages);
        }

        return $this;
    }

    /**
     * @return null|array
     */
    public function getPackages(): ?array
    {
        return json_decode($this->packages ?? '[]');
    }

    /**
     * @param array|string $packages
     *
     * @return Item $this
     */
    public function setPackages($packages): self
    {
        $packages = $this->toJson($packages);

        if ($this->packages !== $packages) {
            $this->packages = $packages;
            $this->setModified('packages', $this->packages);
        }

        return $this;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Item $this
     */
    public function setType($type): self
    {
        if ($this->type !== $type) {
            $this->type = $type;
            $this->setModified('type', $this->type);
        }

        return $this;
    }

    /**
     * @return null|array
     */
    public function getOptions(): ?array
    {
        return json_decode($this->options ?? '[]');
    }

    /**
     * @param array|string $options
     *
     * @return Item $this
     */
    public function setOptions($options): self
    {
        $options = $this->toJson($options);

        if ($this->options !== $options) {
            $this->options = $options;
            $this->setModified('options', $this->options);
        }

        return $this;
    }

    /**
     * @return null|int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return Item $this
     */
    public function setPosition($position): self
    {
        if ($this->position !== $position) {
            $this->position = $position;
            $this->setModified('position', $this->position);
        }

        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getTimeStamp(): ?\DateTime
    {
        return $this->timeStamp;
    }

    /**
     * @param \DateTime $timeStamp
     *
     * @return Item $this
     */
    public function setTimeStamp($timeStamp): self
    {
        if ($this->timeStamp !== $timeStamp) {
            $this->timeStamp = $timeStamp;
            $this->setModified('timeStamp', $this->timeStamp);
        }

        return $this;
    }

    /**
     * @param $field
     * @param $value
     */
    private function setModified($field, $value): void
    {
        $this->modified[$field] = $value;
    }

    /**
     * @param string|null|array $value
     * @return mixed|string
     */
    private function toJson($value)
    {
        if (empty($value)) {
            return null;
        }

        if (\is_array($value)) {
            $value = json_encode($value);
        }

        // If there is a bag format, we try to correct it
        if (!$this->isJson($value)) {
            $value = stripcslashes($value);

            if (!$this->isJson($value)) {
                $value = preg_replace("/[\t\r\n ]+/", ' ', $value);

                if (!$this->isJson($value)) {
                    error_log('Can not convert to JSON: ' . print_r($value, true));

                    return null;
                }
            }
        }

        return $value;
    }

    /**
     * @param $string
     * @return bool
     */
    private function isJson($string): bool
    {
        json_decode($string);

        return (json_last_error() === JSON_ERROR_NONE);
    }
}
