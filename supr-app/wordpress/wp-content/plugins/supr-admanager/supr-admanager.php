<?php
/**
 * Plugin Name: SUPR - Admanager
 * Plugin URI: https://supr.com
 * Description: Nothing
 * Text Domain: supr-admanager
 * Domain Path: /languages
 * Version: 1.0
 * Author: SUPR Development
 * Author URI: https://supr.com
 * License: GPL12
 */

define('SUPR_ADMANAGER_PATH', plugin_dir_path(__FILE__));
define('SUPR_ADMANAGER_URL', plugins_url('/', __FILE__));
define('SUPR_ADMANAGER_FILE', plugin_basename(__FILE__));
define('SUPR_ADMANAGER_DIR_NAME', basename(__DIR__));

define('SUPR_ADMANAGER_VERSION', '1.1.0');

// Load translations
add_action('plugins_loaded', 'supr_textdomain_admanager');

/**
 * Load textdomain
 */
function supr_textdomain_admanager()
{
    load_plugin_textdomain('supr-dashboard', false, SUPR_ADMANAGER_DIR_NAME . '/languages/');
}

// Load main Class
require(SUPR_ADMANAGER_PATH . 'includes/Plugin.php');

// After activation of plugin
register_activation_hook(__FILE__, [\SuprAdmanager\Plugin::class, 'activationHook']);
