<?php

namespace SuprCronSettings;

/**
 * Class Page
 *
 * @package SuprCronSettings
 */
class Page
{
    /**
     * Show setting page
     */
    public static function networkSettingsPage(): void
    {
        include SUPR_CRON_SETTINGS_PATH . 'templates/network-settings-page.php';
    }
}
