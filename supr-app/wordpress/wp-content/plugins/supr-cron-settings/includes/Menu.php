<?php

namespace SuprCronSettings;

/**
 * Class Menu
 *
 * @package SuprCronSettings
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add menu page
        add_action('network_admin_menu', [__CLASS__, 'createMenu']);

        // Add settings link on plugin page
        add_filter('network_admin_plugin_action_links_' . SUPR_CRON_SETTINGS_FILE, [__CLASS__, 'addSettingsPage']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createMenu(): void
    {
        add_menu_page(
            __('Cron settings', 'supr-cron-settings'),
            __('Cron settings', 'supr-cron-settings'),
            'manage_network',
            'supr_cron_settings_settings',
            [Page::class, 'networkSettingsPage'],
            'dashicons-admin-tools'
        );
    }

    /**
     * @param $links
     * @return mixed
     */
    public static function addSettingsPage($links)
    {
        $settings_link = '<a href="admin.php?page=supr_cron_settings_settings">' . __('Settings', 'supr-cron-settings') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
