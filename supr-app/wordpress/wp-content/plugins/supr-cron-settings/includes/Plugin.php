<?php

namespace SuprCronSettings;

/**
 * Class Plugin
 *
 * @package SuprCronSettings
 */
class Plugin
{
    /**
     * @var null
     */
    private static $instance = null;

    /**
     * @return \SuprCronSettings\Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Init
     */
    public function init(): void
    {
        Menu::addMenu();
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // Load translations
        add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        add_action('init', [$this, 'init'], 0);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_CRON_SETTINGS_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::instance();
