<?php

namespace SuprCronSettings;

class TextDomain
{
    public static $domainName = 'supr-cron-settings';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_CRON_SETTINGS_DIR_NAME . '/languages/');
    }
}
