<?php
/**
 * Plugin Name: SUPR - Cron Settings
 * Plugin URI: https://supr.com
 * Description: It contains the properties, that are used by cron tasks of SUPR.
 * Text Domain: supr-cron-settings
 * Domain Path: /languages
 * Version: 1.0
 * Author: SUPR Development
 * License: GPL
 */

use SuprCronSettings\Manager;

define('SUPR_CRON_SETTINGS_PATH', plugin_dir_path(__FILE__));
define('SUPR_CRON_SETTINGS_URL', plugins_url('/', __FILE__));
define('SUPR_CRON_SETTINGS_FILE', plugin_basename(__FILE__));
define('SUPR_CRON_SETTINGS_DIR_NAME', basename(__DIR__));

define('SUPR_CRON_SETTINGS_VERSION', '1.0.0');

// Load main Class
require SUPR_CRON_SETTINGS_PATH . 'includes/Plugin.php';
