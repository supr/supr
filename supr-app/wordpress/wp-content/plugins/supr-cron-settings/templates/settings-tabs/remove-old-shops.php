<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/*
 * Info: dosc means DeleteOldShopsCron
 */

// Handle request with options
if (isset($_POST['supr_cron_settings_dosc'])) {
    $newOptions = $_POST['supr_cron_settings_dosc'];

    // Trim all values
    $newOptions = array_map('trim', $newOptions);
    // Remove empty values
    $newOptions = array_filter($newOptions);

    // Options for saving in DB
    $options = [];

    // Change system blogs IDs from string to array
    $options['ignore_blogs'] = \sanitize_text_field($newOptions['ignore_blogs']);
    $options['ignore_blogs'] = explode(',', $options['ignore_blogs']);
    $options['ignore_blogs'] = array_map('trim', $options['ignore_blogs']);
    $options['ignore_blogs'] = array_filter($options['ignore_blogs']);
    $options['ignore_blogs'] = array_map('intval', $options['ignore_blogs']);

    // Change customer blogs IDs from string to array
    $options['ignore_blogs_customer'] = \sanitize_text_field($newOptions['ignore_blogs_customer']);
    $options['ignore_blogs_customer'] = explode(',', $options['ignore_blogs_customer']);
    $options['ignore_blogs_customer'] = array_map('trim', $options['ignore_blogs_customer']);
    $options['ignore_blogs_customer'] = array_filter($options['ignore_blogs_customer']);
    $options['ignore_blogs_customer'] = array_map('intval', $options['ignore_blogs_customer']);

    // Get descriptions of ignored blogs
    $options['ignore_blogs_description'] = \sanitize_textarea_field($newOptions['ignore_blogs_description']);
    $options['ignore_blogs_customer_description'] = \sanitize_textarea_field($newOptions['ignore_blogs_customer_description']);

    // Handle checkboxes
    $options['active'] = filter_var($newOptions['active'], FILTER_VALIDATE_BOOLEAN);
    $options['dry_run_mode'] = filter_var($newOptions['dry_run_mode'], FILTER_VALIDATE_BOOLEAN);

    // Save Campaign Monitor settings
    $options['cm_api_key'] = \sanitize_text_field($newOptions['cm_api_key']);
    $options['cm_first_notification'] = \sanitize_text_field($newOptions['cm_first_notification']);
    $options['cm_second_notification'] = \sanitize_text_field($newOptions['cm_second_notification']);
    $options['cm_third_notification'] = \sanitize_text_field($newOptions['cm_third_notification']);

    // Save in DB
    \update_option('supr_cron_settings_dosc', $options);
    ?>
    <div class="notice notice-success is-dismissible">
        <p><?= __('Options were saved.', 'supr-cron-settings') ?></p>
    </div>
    <?php
}

$options = \get_option('supr_cron_settings_dosc', []);

?>
<form action="" method="POST">
    <table class="form-table">
        <tr>
            <td><label for="supr_cron_settings_dosc_ignore_blogs"><?= __('Do not delete this blogs (system blogs)', 'supr-cron-settings') ?></label></td>
            <td><input type="text" id="supr_cron_settings_dosc_ignore_blogs" name="supr_cron_settings_dosc[ignore_blogs]" placeholder="1,2,3" value="<?= $options['ignore_blogs'] ? implode(',', $options['ignore_blogs']) : '' ?>"/></td>
            <td><p><?= __('Enter here IDs of all template blogs / main blog / system blogs like master.mysupr.de.', 'supr-cron-settings') ?></p></td>
        </tr>
        <tr>
            <td colspan="3">
                <p><label for="supr_cron_settings_dosc_ignore_blogs_description"><?= __('Please describe here each ignored blog (one blog per line):', 'supr-cron-settings') ?></label></p>
                <textarea id="supr_cron_settings_dosc_ignore_blogs_description" style="width: 100%; height: 150px;" placeholder="1 // Main shop" name="supr_cron_settings_dosc[ignore_blogs_description]"><?= $options['ignore_blogs_description'] ?? '' ?></textarea>
            </td>
        </tr>
        <tr>
            <td><label for="supr_cron_settings_dosc_ignore_blogs_customer"><?= __('Do not delete this blogs (customer blogs)', 'supr-cron-settings') ?></label></td>
            <td><input type="text" id="supr_cron_settings_dosc_ignore_blogs_customer" name="supr_cron_settings_dosc[ignore_blogs_customer]" placeholder="1,2,3" value="<?= $options['ignore_blogs_customer'] ? implode(',', $options['ignore_blogs_customer']) : '' ?>"/></td>
            <td><p><?= __('Enter here IDs of all terminated customer blogs, that should be not deleted.', 'supr-cron-settings') ?></p></td>
        </tr>
        <tr>
            <td colspan="3">
                <p><label for="supr_cron_settings_dosc_ignore_blogs_customer_description"><?= __('Please describe here each ignored blog (one blog per line):', 'supr-cron-settings') ?></label></p>
                <textarea id="supr_cron_settings_dosc_ignore_blogs_customer_description" style="width: 100%; height: 150px;" placeholder="1245 // He asked to give him time until August 2022" name="supr_cron_settings_dosc[ignore_blogs_customer_description]"><?= $options['ignore_blogs_customer_description'] ?? '' ?></textarea>
            </td>
        </tr>
        <tr>
            <td><label for="supr_cron_settings_dosc_active"><?= __('Activate cron', 'supr-cron-settings') ?></label></td>
            <td><input type="checkbox" id="supr_cron_settings_dosc_active" name="supr_cron_settings_dosc[active]"<?= $options['active'] ? ' checked' : '' ?> value="1"/></td>
            <td><p><?= __('If disabled, the cron will not start.', 'supr-cron-settings') ?></p></td>
        </tr>
        <tr>
            <td><label for="supr_cron_settings_dosc_dry_run_mode"><?= __('Dry run mode', 'supr-cron-settings') ?></label></td>
            <td><input type="checkbox" id="supr_cron_settings_dosc_dry_run_mode" name="supr_cron_settings_dosc[dry_run_mode]"<?= $options['dry_run_mode'] ? ' checked' : '' ?> value="1"/></td>
            <td><p><?= __('If enabled, the cron will not really delete any blogs, it will just write to the logs about deletion.', 'supr-cron-settings') ?></p></td>
        </tr>
        <tr>
            <th colspan="2"><?= __('Campaign Monitor transaction IDS', 'supr-cron-settings') ?></th>
        </tr>
        <tr>
            <td><label for="supr_cron_settings_dosc_cm_api_key"><?= __('API key', 'supr-cron-settings') ?></label></td>
            <td><input type="text" id="supr_cron_settings_dosc_cm_api_key" name="supr_cron_settings_dosc[cm_api_key]" value="<?= $options['cm_api_key'] ?? '' ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_cron_settings_dosc_cm_first_notification"><?= __('First notification id', 'supr-cron-settings') ?></label></td>
            <td><input type="text" id="supr_cron_settings_dosc_cm_first_notification" name="supr_cron_settings_dosc[cm_first_notification]" value="<?= $options['cm_first_notification'] ?? '' ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_cron_settings_dosc_cm_second_notification"><?= __('Second notification id', 'supr-cron-settings') ?></label></td>
            <td><input type="text" id="supr_cron_settings_dosc_cm_second_notification" name="supr_cron_settings_dosc[cm_second_notification]" value="<?= $options['cm_second_notification'] ?? '' ?>"/></td>
        </tr>
        <tr>
            <td><label for="supr_cron_settings_dosc_cm_third_notification"><?= __('Third notification id', 'supr-cron-settings') ?></label></td>
            <td><input type="text" id="supr_cron_settings_dosc_cm_third_notification" name="supr_cron_settings_dosc[cm_third_notification]" value="<?= $options['cm_third_notification'] ?? '' ?>"/></td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>

<p><?= __('Below is a list of stores that will be deleted soon (less than 3 months left till removal). The code for this list must be manually changed if the cron is changed.', 'supr-cron-settings') ?></p>
<p><?= __('Changing the code of the cron please don\'t forget to check the relevance of the queries that form this list.', 'supr-cron-settings') ?></p>

<?php
// Show Table with marked for deletion blogs

global $wpdb;

$blogsToDelete = [
    'test' => [],
    'expired' => [],
    'trial' => []
];

// Default
$ignoreBlogsIds = array_merge([1], $options['ignore_blogs'] ?? [], $options['ignore_blogs_customer'] ?? []);

$lastTrialBookedDate = (new \DateTime())->sub(new \DateInterval('P4M'))->format(DATE_ATOM);
$blogsToDelete['trial'] = $wpdb->get_results("SELECT
    `ws`.`created_at` AS `plan_booked_time`,
    `ws`.`active_until` as `plan_active_until_time`,
    `p`.`post_name` AS `plan`,
    `b`.`blog_id` AS `blog_id`,
    `b`.`domain` AS `domain`,
    `u`.`user_email` AS `user_email`
FROM `{$wpdb->base_prefix}wu_subscriptions` AS `ws`
LEFT JOIN `{$wpdb->base_prefix}posts` AS `p` ON `ws`.`plan_id` = `p`.`ID` AND `p`.`post_type` LIKE 'wpultimo_plan'
LEFT JOIN `{$wpdb->base_prefix}wu_site_owner` AS `wso` ON `ws`.`user_id` = `wso`.`user_id`
LEFT JOIN `{$wpdb->base_prefix}users` AS `u` ON `ws`.`user_id` = `u`.`ID`
LEFT JOIN `{$wpdb->base_prefix}blogs` AS `b` ON `wso`.`site_id` = `b`.`blog_id`
WHERE
    (`p`.`post_name` IS NULL OR `p`.`post_name` NOT IN ('basic', 'smart', 'pro'))
    AND `ws`.`created_at` < '{$lastTrialBookedDate}'
    AND `wso`.`site_id` NOT IN (" . implode(',', $ignoreBlogsIds) . ")", ARRAY_A);

$blogsToDelete['test'] = $wpdb->get_results("SELECT
    `ws`.`created_at` AS `plan_booked_time`,
    `ws`.`active_until` as `plan_active_until_time`,
    `p`.`post_name` AS `plan`,
    `b`.`blog_id` AS `blog_id`,
    `b`.`domain` AS `domain`,
    `u`.`user_email` AS `user_email`
FROM `{$wpdb->base_prefix}wu_subscriptions` AS `ws`
LEFT JOIN `{$wpdb->base_prefix}posts` AS `p` ON `ws`.`plan_id` = `p`.`ID` AND `p`.`post_type` LIKE 'wpultimo_plan'
LEFT JOIN `{$wpdb->base_prefix}wu_site_owner` AS `wso` ON `ws`.`user_id` = `wso`.`user_id`
LEFT JOIN `{$wpdb->base_prefix}users` AS `u` ON `ws`.`user_id` = `u`.`ID`
LEFT JOIN `{$wpdb->base_prefix}blogs` AS `b` ON `wso`.`site_id` = `b`.`blog_id`
WHERE
    `b`.`domain` REGEXP '^test[0-9]{6,}'
    AND `wso`.`site_id` NOT IN (" . implode(',', $ignoreBlogsIds) . ")", ARRAY_A);

$lastTerminatedDate = (new \DateTime())->sub(new \DateInterval('P3M'))->format(DATE_ATOM);
$blogsToDelete['expired'] = $wpdb->get_results("SELECT
    `ws`.`created_at` AS `plan_booked_time`,
    `ws`.`active_until` as `plan_active_until_time`,
    `p`.`post_name` AS `plan`,
    `b`.`blog_id` AS `blog_id`,
    `b`.`domain` AS `domain`,
    `u`.`user_email` AS `user_email`
FROM `{$wpdb->base_prefix}wu_subscriptions` AS `ws`
LEFT JOIN `{$wpdb->base_prefix}posts` AS `p` ON `ws`.`plan_id` = `p`.`ID` AND `p`.`post_type` LIKE 'wpultimo_plan'
LEFT JOIN `{$wpdb->base_prefix}wu_site_owner` AS `wso` ON `ws`.`user_id` = `wso`.`user_id`
LEFT JOIN `{$wpdb->base_prefix}users` AS `u` ON `ws`.`user_id` = `u`.`ID`
LEFT JOIN `{$wpdb->base_prefix}blogs` AS `b` ON `wso`.`site_id` = `b`.`blog_id`
WHERE
    `ws`.`price` > 0
    AND `ws`.`active_until` < '{$lastTerminatedDate}'
    AND `wso`.`site_id` NOT IN (" . implode(',', $ignoreBlogsIds) . ")", ARRAY_A);
?>

<table class="wp-list-table widefat fixed striped">
    <?php foreach ($blogsToDelete as $type => $blogs) : ?>
        <?php if (count($blogs) > 0) : ?>
            <tr>
                <th><?= __(strtoupper($type), 'supr-cron-settings') ?></th>
                <th><?= __('ID', 'supr-cron-settings') ?></th>
                <th><?= __('User', 'supr-cron-settings') ?></th>
                <th><?= __('Plan', 'supr-cron-settings') ?></th>
                <th><?= __('Booked', 'supr-cron-settings') ?></th>
                <th><?= __('Active till', 'supr-cron-settings') ?></th>
            </tr>
        <?php endif; ?>
        <?php foreach ($blogs as $blog) : ?>
            <tr>
                <td><a href="https://<?= $blog['domain'] ?>/wp-admin/" target="_blank"><?= $blog['domain'] ?></a></td>
                <td><?= $blog['blog_id'] ?></td>
                <td><?= $blog['user_email'] ?></td>
                <td><?= $blog['plan'] ?? '<span title="' . __('Please fix it', 'supr-cron-settings') . '" style="padding: 3px; background-color: #ca4a1f; color: white; font-weight: 600;">' . __('without plan', 'supr-cron-settings') . '</span>' ?></td>
                <td><?= date('d.m.Y', strtotime($blog['plan_booked_time'])) ?></td>
                <td><?= date('d.m.Y', strtotime($blog['plan_active_until_time'])) ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endforeach; ?>
</table>
