<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$crons = \get_option('cron', []);
?>
<table class="wp-list-table widefat fixed striped">
    <tr>
        <th><?= __('Next execution time', 'supr-cron-settings') ?></th>
        <th><?= __('Slug', 'supr-cron-settings') ?></th>
        <th><?= __('Schedule', 'supr-cron-settings') ?></th>
        <th><?= __('Interval', 'supr-cron-settings') ?></th>
        <th><?= __('Args', 'supr-cron-settings') ?></th>
    </tr>
    <?php foreach ($crons as $time => $planedCrons) : ?>
        <?php foreach ($planedCrons as $slug => $hashCrons) : ?>
            <?php foreach ($hashCrons as $hash => $hashCron) : ?>
                <tr>
                    <td><?= date('d.m.Y H:i:s', $time) ?></td>
                    <td><?= $slug ?></td>
                    <td><?= $hashCron['schedule'] ?></td>
                    <td><?= $hashCron['interval'] ?></td>
                    <td><?= !empty($hashCron['args']) ? '<code>' . print_r($hashCron['args'], true) . '</code>' : '' ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>
</table>