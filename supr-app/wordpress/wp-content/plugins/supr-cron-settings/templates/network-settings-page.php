<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$tabs = [
    'remove-old-shops' => __('Remove old shops cron', 'supr-cron-settings'),
    'wp-cron-list' => __('WP Crons', 'supr-cron-settings')
];

$currentTab = (isset($_GET['tab'], $tabs[$_GET['tab']])) ? $_GET['tab'] : 'remove-old-shops';
?>
<div class="wrap supr-cron-settings">
    <h2><?= get_admin_page_title(); ?></h2>
    <h2 class="nav-tab-wrapper">
        <?php foreach ($tabs as $tab => $tabName) : ?>
            <a class="nav-tab<?= $currentTab === $tab ? ' nav-tab-active' : ''; ?>" href="?page=<?= $_GET['page']?>&tab=<?= $tab; ?>"><?= $tabName; ?></a>
        <?php endforeach; ?>
    </h2>

    <?php
    // Include tabs template
    include SUPR_CRON_SETTINGS_PATH . "templates/settings-tabs/{$currentTab}.php";
    ?>
</div>