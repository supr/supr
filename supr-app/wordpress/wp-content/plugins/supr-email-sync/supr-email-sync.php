<?php
/**
 * Plugin Name: SUPR - Sync emails
 * Plugin URI: https://supr.com
 * Description: We want to have only 2 emails in the System: Admin E-Mail and Shop E-Mail
 * Text Domain: supr-email-sync
 * Domain Path: /languages
 * Version: 1.0
 * Author: SUPR Development
 * License: GPL
 */

define('SUPR_EMAIL_SYNC_PATH', plugin_dir_path(__FILE__));
define('SUPR_EMAIL_SYNC_URL', plugins_url('/', __FILE__));
define('SUPR_EMAIL_SYNC_FILE', plugin_basename(__FILE__));
define('SUPR_EMAIL_SYNC_DIR_NAME', basename(__DIR__));

define('SUPR_EMAIL_SYNC_VERSION', '1.0.0');

// Load main Class
require SUPR_EMAIL_SYNC_PATH . 'includes/Plugin.php';
