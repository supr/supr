<?php

namespace SuprEmailSync;

class TextDomain
{
    public static $domainName = 'supr-email-sync';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_EMAIL_SYNC_DIR_NAME . '/languages/');
    }
}
