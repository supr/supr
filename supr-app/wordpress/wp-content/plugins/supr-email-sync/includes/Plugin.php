<?php

namespace SuprEmailSync;

/**
 * Class Plugin
 *
 * @package SuprEmailSync
 */
class Plugin
{
    /**
     * @var null
     */
    public static $instance;

    /**
     * @var
     */
    private $db;

    /**
     * @return \SuprEmailSync\Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();

            //Fires when SuprEmailSync was fully loaded and instantiated.
            do_action('supr-email-sync/loaded');
        }

        return self::$instance;
    }

    /**
     * Init
     */
    public function init(): void
    {
        // Action after the plugin was loaded
        do_action('supr-email-sync/init');
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        global $wpdb;

        $this->setDbProvider($wpdb);

        $this->registerAutoloader();

        Manager::instance()->addHooksToSyncEmails();

        // Load translations
        add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        add_action('admin_init', [$this, 'init'], 0);
    }

    /**
     * @param $db
     */
    private function setDbProvider($db): void
    {
        $this->db = $db;
    }

    /**
     * @return \wpdb
     */
    public static function getDbProvider(): \wpdb
    {
        return self::instance()->db;
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_EMAIL_SYNC_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::instance();
