<?php

namespace SuprEmailSync;

/**
 * Class Manager
 *
 * @package SuprEmailSync
 */
class Manager
{
    /**
     * @var null|Manager
     */
    private static $instance;

    /**
     * Manager constructor.
     */
    public function __construct()
    {
    }

    /**
     * @var array[]
     */
    public static $aliases = [
        ['admin_email', 'user_login', 'user_email', 'billing_email'],
        ['woocommerce_store_owner_email']
    ];

    private $syncEmailsInProcess = false;

    /**
     * @param bool $value
     * @return \SuprEmailSync\Manager
     */
    public function setSyncEmailInProcess($value): self
    {
        $this->syncEmailsInProcess = $value;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSyncEmailInProcess(): bool
    {
        return $this->syncEmailsInProcess;
    }

    /**
     * @return \SuprEmailSync\Manager
     */
    public static function instance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Add hook to WP update_option
     */
    public function addHooksToSyncEmails(): void
    {
        add_action('updated_option', [__CLASS__, 'syncEmails'], 10, 3);
        add_filter('send_email_change_email', [__CLASS__, 'changeUserMailFilter'], -1, 3);
    }

    /**
     * We update blog options if email of user was changed
     *
     * @param bool  $bool
     * @param array $oldUserData
     * @param array $newUserData
     * @return mixed
     */
    public static function changeUserMailFilter($bool, $oldUserData, $newUserData): bool
    {
        $shopOwner = self::getUserByShopId(get_current_blog_id());

        if ($shopOwner && (int)$shopOwner->ID === (int)$newUserData['ID']) {
            update_option('admin_email', $newUserData['user_email']);
        }

        return $bool;
    }

    /**
     * @param $optionName
     * @param $oldValue
     * @param $value
     */
    public static function syncEmails($optionName, $oldValue, $value): void
    {
        if (self::instance()->getSyncEmailInProcess()) {
            return;
        }

        // Set sync active to avoid recursion
        self::instance()->setSyncEmailInProcess(true);

        foreach (self::$aliases as $emailsGroup) {
            if (\in_array($optionName, $emailsGroup, true)) {
                // Update all of aliases
                foreach ($emailsGroup as $aliasOptionName) {
                    // We don't need to update himself
                    if ($aliasOptionName === $optionName) {
                        continue;
                    }

                    // Update alias
                    update_option($aliasOptionName, $value);
                }
            }
        }

        // Reset
        self::instance()->setSyncEmailInProcess(false);
    }

    /**
     * @param $shopId
     * @return null|\WP_User
     */
    public static function getUserByShopId($shopId): ?\WP_User
    {
        $userId = null;
        $user = null;

        if (\class_exists('\WU_Site_Owner')) {
            $tableName = \WU_Site_Owner::get_table_name();
            $result = Plugin::getDbProvider()->get_row("SELECT user_id FROM {$tableName} WHERE site_id = {$shopId}");

            if ($result) {
                $userId = (int)$result->user_id;
            }
        }

        if (!$userId) {
            $tableName = Plugin::getDbProvider()->base_prefix . 'usermeta';
            $querystring = "SELECT user_id FROM {$tableName} WHERE (meta_key LIKE 'primary_blog' AND meta_value LIKE '{$shopId}') LIMIT 1";
            $userId = (int)Plugin::getDbProvider()->get_var($querystring);
        }

        if ($userId) {
            $user = \get_user_by('id', $userId);

            // Fix the return type, because we can't handle mixed types!
            if (\is_bool($user)) {
                return null;
            }
        }

        return $user;
    }
}
