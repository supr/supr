<?php

/**
 * Adds pointer messages in admin area.
 */
class WPDesk_Flexible_Shipping_Pointer_Messages implements \WPDesk\PluginBuilder\Plugin\Hookable {

	/**
	 * Variant.
	 *
	 * @var \WPDesk\ABTesting\ABVariant
	 */
	private $variant;

	/**
	 * WPDesk_Flexible_Shipping_Pointer_Messages constructor.
	 *
	 * @param \WPDesk\ABTesting\ABVariant $variant Variant.
	 */
	public function __construct( \WPDesk\ABTesting\ABVariant $variant ) {
		$this->variant = $variant;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'maybe_add_pointer_messages' ) );
	}

	/**
	 * Maybe add pointer messages.
	 */
	public function maybe_add_pointer_messages() {
		if ( $this->variant->is_on( WPDesk_Flexible_Shipping_AB_Pointer_Message_Test::FS_WITH_POINTER_MESSAGE_VIDEO ) ) {
			$this->add_pointer_message_video();
		} elseif ( $this->variant->is_on( WPDesk_Flexible_Shipping_AB_Pointer_Message_Test::FS_WITH_POINTER_MESSAGE_TEXT ) ) {
			$this->add_pointer_message_text();
		}
	}

	/**
	 * Add pointer message with video.
	 */
	private function add_pointer_message_video() {
		ob_start();
		include 'views/html-pointer-message-fs-video.php';
		$content = trim( ob_get_contents() );
		ob_end_clean();

		$this->add_pointer_message(
			'fs_how_to_video',
			__( 'Watch how to use Flexible Shipping', 'flexible-shipping' ),
			$content
		);
	}

	/**
	 * Add pointer message with text.
	 */
	private function add_pointer_message_text() {
		$shipping_zones_url   = admin_url( 'admin.php?page=wc-settings&tab=shipping' );
		$shipping_methods_url = get_locale() === 'pl_PL' ? 'https://www.wpdesk.pl/docs/flexible-shipping-pro-woocommerce-docs/#Metody_wysylki' : 'https://docs.flexibleshipping.com/article/29-shipping-methods';
		ob_start();
		include 'views/html-pointer-message-fs-text.php';
		$content = trim( ob_get_contents() );
		ob_end_clean();

		$this->add_pointer_message(
			'fs_how_to_text',
			__( 'How to start with Flexible Shipping', 'flexible-shipping' ),
			$content
		);
	}

	/**
	 * Add pointer message.
	 *
	 * @param string $id ID.
	 * @param string $title Title.
	 * @param string $content Content.
	 */
	private function add_pointer_message( $id, $title, $content ) {
		$pointer_scripts = new \WPDesk\Pointer\PointersScripts( array( 'plugins' ) );
		$pointer_scripts->hooks();

		$pointer_conditions = new \WPDesk\Pointer\PointerConditions( 'plugins', 'manage_woocommerce' );
		$pointer_message    = new \WPDesk\Pointer\PointerMessage(
			$id,
			'#toplevel_page_woocommerce',
			$title,
			$content,
			new \WPDesk\Pointer\PointerPosition( \WPDesk\Pointer\PointerPosition::LEFT, \WPDesk\Pointer\PointerPosition::LEFT ),
			'wp-pointer',
			480,
			$pointer_conditions
		);
	}

}
