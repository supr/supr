<?php
/**
 * UI Elements
 *
 * Adds UI elements with important triggers for API calls, actions and more.
 *
 * @category   WP Ultimo
 * @package    Billwerk
 * @author     Arindo Duque <arindo@wpultimo.com>
 * @since      0.0.1
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

if (!class_exists('WUB_UI_Elements')) :

class WUB_UI_Elements {

  /**
   * Makes sure we are only using one instance of the plugin
   * 
   * @since 0.0.1
   * @var object WUB_UI_Elements
   */
  public static $instance;

  /**
   * Returns the instance of WUB_UI_Elements
   * 
   * @return object A WUB_UI_Elements instance
   */
  public static function get_instance() {

    if (null === self::$instance) self::$instance = new self();

    return self::$instance;
    
  } // end get_instance;

  /**
   * Adds the hooks and actions
   *
   * @since 0.0.1
   * @return void
   */
  public function __construct() {

    add_action('admin_enqueue_scripts', array($this, 'register_ui_scripts'));

    add_action('admin_bar_menu', array($this, 'add_billwerk_topbar_menu'), 999);

    add_action('current_screen', array($this, 'disable_wpultimo_pages'));

    add_action('init', array($this, 'register_core_scripts'));

  } // end construct;

  public function register_core_scripts() {

    add_action('checkout_enqueue_scripts', array(WU_Scripts(), 'register_scripts'), 1);

  } // end register_core_scripts;

  /**
   * Added function to disable some WP Ultimo pages and delegate them to Billwerk's Admin Panel
   *
   * @since 0.0.1
   * @param WP_Screen $screen
   * @return void
   */
  public function disable_wpultimo_pages($screen) {

    $screens_to_disable = array(
      'toplevel_page_wp-ultimo-coupons-network',
      'coupons_page_wu-edit-coupon-network',
    );

    if (in_array($screen->id, $screens_to_disable)) {

      $message = __('This feature should now be managed directly on the Billwerk admin panel.', 'wu-billwerk');

      $message .= sprintf('<br><br><a class="button button-primary button-streched" style="width: 100%%; text-align: center;" href="%s" target="_blank">%s</a><br>', WP_Ultimo_Billwerk()->api->get_panel_url(), __('Go to Billwerk\'s Admin Panel &rarr;', 'wu-billwerk'));

      WU_Util::wp_die($message, __('Feature Delegated', 'wu-billwerk'));

    } // end if;

  } // end disable_wpultimo_pages;

  /**
   * Register and enqueues UI scripts.
   * We might move this somewhere else in the future
   *
   * @since 0.0.1
   * @return void
   */
  public function register_ui_scripts() {

    $suffix = WP_Ultimo_Billwerk()->get_js_suffix();

    wp_register_script('wub-main', WP_Ultimo_Billwerk()->get_asset("main$suffix.js", 'js'), array('jquery', 'wp-ultimo'), WP_Ultimo_Billwerk()->version, true);

    wp_register_script('wub-ajax-actions', WP_Ultimo_Billwerk()->get_asset("wub-ajax-actions$suffix.js", 'js'), array('jquery', 'wp-ultimo'), WP_Ultimo_Billwerk()->version);

    wp_enqueue_script('wub-main');

    wp_enqueue_script('wub-ajax-actions');

  } // end register_ui_scripts;

  /**
   * Adds the Billwerk actions top-bar item for network admins
   *
   * @since 0.0.1
   * @param WP_Admin_Bar $wp_admin_bar
   * @return void
   */
  public function add_billwerk_topbar_menu($wp_admin_bar) {

    if (!current_user_can('manage_network')) return;

	  $wp_admin_bar->add_node(array(
      'id'    => 'wub_billwerk',
      'title' => __('Billwerk Integration', 'wu-billwerk'),
      'href'  => '#',
    ));

    $wp_admin_bar->add_node(array(
      'id'     => 'wub_billwerk_admin_panel',
      'parent' => 'wub_billwerk',
      'title'  => __('Billwerk Admin Panel &rarr;', 'wu-billwerk'),
      'href'   => WP_Ultimo_Billwerk()->api->get_panel_url(),
      'meta'   => array(
        'target' => '_blank',
      ),
    ));

    $wp_admin_bar->add_node(array(
      'id'     => 'wub_billwerk_gateway_settings',
      'parent' => 'wub_billwerk',
      'title'  => __('Billwerk Settings', 'wu-billwerk'),
      'href'   => network_admin_url('admin.php?page=wp-ultimo&wu-tab=gateways#billwerk'),
    ));

    $wp_admin_bar->add_node(array(
      'id'     => 'wub_billwerk_install',
      'parent' => 'wub_billwerk',
      'title'  => __('Install', 'wu-billwerk'),
      'href'   => '#',
    ));

    $wp_admin_bar->add_node(array(
      'id'     => 'wub_billwerk_actions_install_custom_fields',
      'parent' => 'wub_billwerk_install',
      'title'  => __('Create Custom Fields on Billwerk', 'wu-billwerk'),
      'href'   => wp_nonce_url(admin_url('admin-ajax.php?action=wub_install_custom_fields'), 'install_custom_fields'),
      'meta'   => array(
        'class' => 'wub-ajax',
      ),
    ));

    $wp_admin_bar->add_node(array(
      'id'     => 'wub_billwerk_actions_install_webhook_url',
      'parent' => 'wub_billwerk_install',
      'title'  => __('Add this site as a Webhook Listener on Billwerk', 'wu-billwerk'),
      'href'   => wp_nonce_url(admin_url('admin-ajax.php?action=wub_install_webhook'), 'install_webhook'),
      'meta'   => array(
        'class' => 'wub-ajax',
      ),
    ));

    $wp_admin_bar->add_node(array(
      'id'     => 'wub_billwerk_cache',
      'parent' => 'wub_billwerk',
      'title'  => __('Cache', 'wu-billwerk'),
      'href'   => '#',
    ));

    $wp_admin_bar->add_node(array(
      'id'     => 'wub_billwerk_actions_clear_cache',
      'parent' => 'wub_billwerk_cache',
      'title'  => __('Clear API Cache', 'wu-billwerk'),
      'href'   => wp_nonce_url(admin_url('admin-ajax.php?action=wub_clear_api_cache'), 'clear_api_cache'),
      'meta'   => array(
        'class' => 'wub-ajax',
      ),
    ));

    $wp_admin_bar->add_node(array(
      'id'     => 'wub_billwerk_sync',
      'parent' => 'wub_billwerk',
      'title'  => __('Sync', 'wu-billwerk'),
      'meta'   => array( 'class' => 'my-toolbar-page' ),
      'href'   => '#',
    ));

    $wp_admin_bar->add_node(array(
      'id'     => 'wub_billwerk_actions_sync_plans',
      'parent' => 'wub_billwerk_sync',
      'title'  => __('Sync Plans (from Billwerk)', 'wu-billwerk'),
      'href'   => wp_nonce_url(admin_url('admin-ajax.php?action=wub_sync_plans'), 'sync_plans'),
      'meta'   => array(
        'class' => 'wub-ajax',
      ),
    ));

    $wp_admin_bar->add_node(array(
      'id'     => 'wub_billwerk_actions_sync_contracts',
      'parent' => 'wub_billwerk_sync',
      'title'  => __('Sync Contracts (from Billwerk) - Time Consuming!', 'wu-billwerk'),
      'href'   => wp_nonce_url(admin_url('admin-ajax.php?action=wub_sync_contracts'), 'sync_contracts'),
      'meta'   => array(
        'class' => 'wub-ajax',
      ),
    ));

  } // end add_billwerk_topbar_menu;

} // end WUB_UI_Elements;

// Adds general purpose UI elements
WUB_UI_Elements::get_instance();

endif;