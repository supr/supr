<?php
/**
 * Account Overrides
 *
 * Replaces UI elements with data coming from Billwerk
 * 
 * @category   WP Ultimo
 * @package    Billwerk
 * @author     Arindo Duque <arindo@wpultimo.com>
 * @since      0.0.1
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

if (!class_exists('WUB_Account_Overrides')) :

class WUB_Account_Overrides {

  /**
   * Holds the only activ einstance of this class
   * 
   * @since 0.0.1
   * @var WUB_Account_Overrides
   */
  public static $instance;

  /**
   * Holds the subscription of the user
   *
   * @since 0.0.1
   * @var WU_Subscription|false
   */
  public $subscription;

  /**
   * Holds the customer id on Billwerk
   *
   * @since 0.0.1
   * @var string
   */
  public $customer_id; 

  /**
   * Holds the contract id on Billwerk
   *
   * @since 0.0.1
   * @var string
   */
  public $contract_id;

  /**
   * Keeps track of the total gross so we can display it on the transactions table
   *
   * @since 0.0.1
   * @var float
   */
  public $total_gross;

  /**
   * Returns the instance of WUB_Account_Overrides
   * 
   * @return object A WUB_Account_Overrides instance
   */
  public static function get_instance() {

    if (null === self::$instance) self::$instance = new self();

    return self::$instance;
    
  } // end get_instance;

  /**
   * Adds the hooks and actions
   *
   * @since 0.0.1
   * @return void
   */
  public function __construct() {

    add_action('admin_init', array($this, 'init'));
    
    add_action('admin_init', array($this, 'handle_change_contract'), 1);

    add_action('admin_enqueue_scripts', array($this, 'register_scripts'));
    
  } // end construct;

  /**
   * Initializes the class, loading up the class attributes
   *
   * @since 0.0.1
   * @return void
   */
  public function init() {

    if ($this->setup()) {

      /**
       * Transactions
       */
      
      add_filter('wu_transactions_list_table_get_transactions', array($this, 'get_invoices_from_billwerk'), 10, 4);

      add_filter('wu_transactions_list_table_get_transactions_count', array($this, 'get_invoice_count'));

      add_filter('wu_get_transactions_table_headers', array($this, 'remove_unecessary_headers_from_transactions_table'));

      add_filter('wu_get_transactions_table_sortable_columns', array($this, 'remove_sort_from_transactions_table'));

      add_filter('wu_transactions_get_transactions_total', array($this, 'modify_transaction_totals'));

      add_filter('wu_transactions_get_refunds_total', array($this, 'modify_refunds_totals'));

      /**
       * Subscription management screen
       */

      add_action('wu_edit_subscription_integration_meta_box', array($this, 'add_customer_id_to_integration_metabox'));

      /**
       * Add switch contract metabox
       */

      add_action('current_screen', array($this, 'add_change_contract_metabox'));

      /**
       * Account Page
       */

      add_action('wu_account_integration_meta_box', array($this, 'display_contract_info'));

      add_filter('wu_account_integrated_method_title', array($this, 'display_payment_method'), 10, 3);

      add_action('wu_account_integrated_method_actions_before', array($this, 'display_update_method_link'), 10, 3);

      add_filter('wu_account_display_cancel_integration_link', '__return_false');

    } // end if;

  } // end init;

  /**
   * Checks if we need or not to make the replacements.
   * 
   * Only do it if this customer is a billwerk customer
   *
   * @since 0.0.1
   * @return boolean
   */
  public function setup() {

    $this->subscription = is_network_admin() && isset($_REQUEST['user_id'])
      ? wu_get_subscription( $_REQUEST['user_id'] )
      : wu_get_current_site()->get_subscription();

    if ($this->subscription) {

      $this->billwerk_contract_id = get_user_meta($this->subscription->user_id, 'billwerk_contract_id', true);

      $this->billwerk_customer_id = get_user_meta($this->subscription->user_id, 'billwerk_customer_id', true);

      return true;

      // return $this->billwerk_contract_id && $this->billwerk_customer_id;

    } // end if;

    return false;

  } // end setup;

  /**
   * Register the scripts and styles we are going to use
   *
   * @since 0.0.1
   * @return void
   */
  public function register_scripts() {

    $suffix = WP_Ultimo_Billwerk()->get_js_suffix();

    wp_register_script('wub-subscriptions', WP_Ultimo_Billwerk()->get_asset("wub-subscriptions$suffix.js", 'js'), array('jquery'), WP_Ultimo_Billwerk()->version, true);

  } // end register_scripts;

  /**
   * Replaces the "Billwerk" string with the payment method used by the customer
   *
   * @since 0.0.1
   * @param string $gateway
   * @param WU_Subscription $subscription
   * @return string
   */
  public function display_payment_method($gateway, $subscription) {

    $contract = $this->get_contract_info();

    if (!$contract) return __('Failed to retrieve payment method', 'wu-billwerk');

    if ($contract->PaymentProviderRole == 'OnAccount') {

      return __('On Account', 'wu-billwerk');

    } // end if;
    
    if ($contract->PaymentProviderRole === 'BlackLabel' && isset($contract->PaymentProvider) && $contract->PaymentProvider === 'InvoicePayment') {

      return __('On Account', 'wu-billwerk');
  
    } // end if;
    
    if ($contract->PaymentProviderRole === 'BlackLabel' && isset($contract->PaymentProvider) && $contract->PaymentProvider === 'PayPal') {
      
      $paypal_email = $contract->PaymentBearer->EmailAddress;

      return sprintf(__('PayPal (%s)', 'wu-billwerk'), $paypal_email);
  
    } // end if;

    if (isset($contract->PaymentBearer) && ($contract->PaymentBearer->Type == 'CreditCard' || $contract->PaymentBearer->Type == 'DebitCard')) {

      return sprintf(__('%s (%s) ending in %s', 'wp-ultimo'), implode(' ', preg_split('/(?=[A-Z])/', $contract->PaymentBearer->Type)), $contract->PaymentBearer->CardType, $contract->PaymentBearer->Last4);

    }

    return __('Failed to retrieve payment method', 'wu-billwerk');

  } // end display_payment_method;

  /**
   * Displays a link to update the payment method used on Billwerk
   *
   * @since 0.0.1
   * @param string $gateway
   * @param WU_Subscription $subscription
   * @return void
   */
  public function display_update_method_link($gateway, $subscription) { } // end display_update_method_link;

  /**
   * Handles the change in contract from the subscription management screen
   *
   * @since 0.0.1
   * @return void
   */
  public function handle_change_contract() {

    if (isset($_REQUEST['wub-change-contract']) && isset($_REQUEST['wub-contract']) && !empty($_REQUEST['wub-contract'])) {

     $new_contract_id = $_REQUEST['wub-contract'];

     $contract_info = WP_Ultimo_Billwerk()->api->no_cache()->get("/Contracts/{$new_contract_id}");

     if ($contract_info) {

      $user_id = $_REQUEST['user_id'];

      update_user_meta($user_id, 'billwerk_contract_id', $new_contract_id);
      update_user_meta($user_id, 'billwerk_customer_id', $contract_info->CustomerId);

      $subscription = wu_get_subscription($user_id);

      $subscription->integration_status = 1;
      $subscription->integration_key    = $new_contract_id;

      if ($subscription->save() && WUB_Sync()->sync_contract($new_contract_id)) {

        wp_redirect(add_query_arg('updated', 1));

        exit;

      } // end if;

     } // end if;

    } // end if;

  } // end handle_change_contract;

  /**
   * Add the Billwerk Contract widget
   *
   * @since 0.0.1
   * @param WP_Screen $screen
   * @return void
   */
  public function add_change_contract_metabox($screen) {

    if (isset($_GET['page']) && $_GET['page'] == 'wu-edit-subscription') {

      WP_Ultimo()->enqueue_select2();

      add_meta_box('wub-change-contract', __('Billwerk Contract', 'wp-ultimo'), array($this, 'render_widget_change_contract'), $screen->id, 'side', 'low');

    } // end if;

  } // end add_change_contract_metabox;

  /**
   * Renders the content of the change contract metabox
   *
   * @since 0.0.1
   * @return void
   */
  public function render_widget_change_contract() {

    wp_enqueue_script('wub-subscriptions');

    WP_Ultimo_Billwerk()->render('subscriptions/change-contract', array(
      'contract' => $this->get_contract_info(),
      'user_id'  => $this->subscription->user_id,
    ));

  } // end render_widget_change_contract;

  /**
   * Filters the transactions total and replaces it with the values returned from billwerk
   *
   * @since 0.0.1
   * @param float $value
   * @return float
   */
  public function modify_transaction_totals($value) {

    return $this->total_gross;

  } // end modify_transaction_totals;

    /**
   * Filters the transactions refunds total and replaces it with the values returned from billwerk
   *
   * @since 0.0.1
   * @param float $value
   * @return float
   */
  public function modify_refunds_totals($value) {

    return 0;

  } // end modify_refunds_totals;

  /**
   * Pulls contract info from billwerk using the REST API
   *
   * @since 0.0.1
   * @return object|false
   */
  public function get_contract_info() {

    if ($this->billwerk_contract_id == '') return false;

    $contract_info = WP_Ultimo_Billwerk()->api->get("/Contracts/{$this->billwerk_contract_id}");

    if ($contract_info) {

      return $contract_info;

    } // end if;

    return false;

  } // end get_contract_info;

  /**
   * Displays the contract info on the user Accounts page
   *
   * @since 0.0.1
   * @param WU_Subscription $subscription
   * @return void
   */
  public function display_contract_info($subscription) { 
    
    /**
     * Get contract info
     */
    $contract = $this->get_contract_info(); 
    
    if (!$contract) return;

    ?>

    <li class="account-status">
      <p>
        <strong><?php _e('Customer Name:', 'wp-ultimo'); ?></strong> 
        <?php echo $contract->CustomerName; ?>
      </p>
    </li>
    <?php if ($contract->LifecycleStatus !== 'InTrial') : ?>
      <li class="account-status">
        <p>
          <strong><?php _e('Subscription Status:', 'wp-ultimo'); ?></strong> 
          <?php echo $contract->LifecycleStatus; ?>
        </p>
      </li>
    <?php endif; ?>
    
  <?php } // end display_contract_info;

  /**
   * Displays the contract info on the subscription management screen
   *
   * @since 0.0.1
   * @return void
   */
  public function add_customer_id_to_integration_metabox() { 
    
    /**
     * Check if there is a contract
     */
    if ($this->billwerk_contract_id == '') return;
    
    ?>

    <li>
      <label>
        <?php _e('Contract ID - Billwerk', 'wu-billwerk'); ?>
      </label>
      <span><?php echo $this->billwerk_contract_id; ?></span>
      <br>
      <span>
        <a target="_blank" href="<?php echo WP_Ultimo_Billwerk()->api->environment_url("https://sandbox.billwerk.com/#/contracts/{$this->billwerk_contract_id}"); ?>">
          <?php _e('See on Billwerk', 'wu-billwerk'); ?> &rarr;
        </a>
      </span>
    </li>

    <li>
      <label>
        <?php _e('Customer ID - Billwerk', 'wu-billwerk'); ?>
      </label>
      <span><?php echo $this->billwerk_customer_id; ?></span>
      <br>
      <span>
        <a target="_blank" href="<?php echo WP_Ultimo_Billwerk()->api->environment_url("https://sandbox.billwerk.com/#/customers/{$this->billwerk_customer_id}"); ?>">
          <?php _e('See on Billwerk', 'wu-billwerk'); ?> &rarr;
        </a>
      </span>
    </li>

  <?php } // end add_customer_id_to_integration_metabox;

  /**
   * Remove sorting from the transactions table
   *
   * @since 0.0.1
   * @param array $columsn
   * @return array
   */
  public function remove_sort_from_transactions_table($columsn) {

    return array();

  } // end remove_sort_from_transactions_table;

  /**
   * Filters the transactions table to display only the elements we want there
   *
   * @since 0.0.1
   * @param array $headers
   * @return array
   */
  public function remove_unecessary_headers_from_transactions_table($headers) {

    $unset = array(
      'type', 'id', 'gateway', 'refund',
    );

    foreach($unset as $index) {

      unset( $headers[$index] );

    } // end foreach;

    $headers['vat']   = __('VAT', 'wu-billwerk');
    $headers['gross'] = get_wu_currency_symbol();

    return $headers;

  } // end remove_unecessary_headers_from_transactions_table;

  /**
   * Get the number of invoices of the user on Billwerk
   *
   * @since 0.0.1
   * @param integer $count
   * @return integer
   */
  public function get_invoice_count($count) {

    if ($this->billwerk_customer_id == '') return 0;

    $endpoints = add_query_arg(array(
      'customerId' => $this->billwerk_customer_id,
      'take'       => 9999,
    ), '/Invoices');

    $invoices = WP_Ultimo_Billwerk()->api->get($endpoints);

    if ($invoices && is_array($invoices)) {

      $this->total_gross = array_sum( array_column($invoices, 'TotalGross') );

      return count($invoices);

    } // end if;

    return 0;

  } // end get_invoice_count;

  /**
   * Pull invoices from Billwerk
   *
   * @since 0.0.1
   * @param array $invoices
   * @param integer $user_id
   * @param integer $per_page
   * @param integer $page_number
   * @return array
   */
  public function get_invoices_from_billwerk($invoices, $user_id, $per_page, $page_number) {

    if ($this->billwerk_customer_id == '') return array();

    $endpoints = add_query_arg(array(
      'customerId' => $this->billwerk_customer_id,
      'take'       => $per_page,
      'skip'       => $per_page * ($page_number - 1)
    ), '/Invoices');

    $invoices = WP_Ultimo_Billwerk()->api->get($endpoints);

    if ($invoices && is_array($invoices)) {

      return array_map(array($this, 'convert_to_wpultimo_transaction'), $invoices);

    } // end if;

    return array();

  } // end get_invoices_from_billwerk;

  /**
   * Gets the object return by Billwerk and transforms it into something our list table can understand
   *
   * @since 0.0.1
   * @param object $billwerk_invoice
   * @return object
   */
  public function convert_to_wpultimo_transaction($billwerk_invoice) {

    $desc = sprintf(__('Invoice for %s (%s)', 'wu-billwerk'), $billwerk_invoice->RecipientName, "<small>$billwerk_invoice->InvoiceNumber</small>");

    return (object) array(
      'id'              => $billwerk_invoice->Id,
      'user_id'         => $this->subscription->user_id,
      'reference_id'    => $billwerk_invoice->InvoiceNumber,
      'gateway'         => '',
      'amount'          => wu_format_currency($billwerk_invoice->TotalNet),
      'original_amount' => wu_format_currency($billwerk_invoice->TotalNet),
      'type'            => '',
      'nature'          => '',
      'description'     => $desc . $this->get_download_link_for_invoice($billwerk_invoice->Id),
      'time'            => $billwerk_invoice->Created,
      'vat'             => wu_format_currency($billwerk_invoice->TotalVat),
      'gross'           => wu_format_currency($billwerk_invoice->TotalGross),
    );

  } // end convert_to_wpultimo_transaction;

  /**
   * Returns the download link for a given invoice PDF file
   *
   * @since 0.0.1
   * @param string $invoice_id
   * @return string
   */
  public function get_download_link_for_invoice($invoice_id) {

    $download_url = wp_nonce_url(admin_url("admin-ajax.php?action=wub_serve_invoice_pdf_download&invoice={$invoice_id}"), 'serve_invoice_pdf_download');

    $link = sprintf('<br><small><a href="%s">%s</a></small>', $download_url, __('Download PDF', 'wu-billwerk'));

    return $link;

  } // end get_download_link_for_invoice;

} // end WUB_Account_Overrides;

// Adds general purpose UI elements
WUB_Account_Overrides::get_instance();

endif;