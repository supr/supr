<?php
/**
 * Cron Services
 *
 * Register all the ajax services we'll need on the UI,
 * and there will be a lot of them since we'll be calling Billwerk's API frequently.
 *
 * @category   WP Ultimo
 * @package    Billwerk
 * @author     Arindo Duque <arindo@wpultimo.com>
 * @since      1.0.0
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

if (!class_exists('WUB_Cron_Service')) :

class WUB_Cron_Service {

  /**
   * Makes sure we are only using one instance of the plugin
   *
   * @since 1.0.0
   * @var object WUB_Cron_Service
   */
  public static $instance;

  /**
   * Returns the instance of WUB_Cron_Service
   *
   * @since 0.0.1
   * @return WUB_Cron_Service A WUB_Cron_Service instance
   */
  public static function get_instance(): \WUB_Cron_Service {

    if (null === self::$instance) self::$instance = new self();

    return self::$instance;

  } // end get_instance;

  /**
   * Register the Ajax service providers
   *
   * @since 1.0.0
   * @return void
   */
  public function __construct() {

    add_filter('cron_schedules', [$this, 'add_new_cron_schedules']);

    add_action('init', [$this, 'schedule_crons']);

    add_action('wub_cron_6h_sync_plans', [$this, 'run_plan_resync']);

  } // end construct;

  /**
   * Create and schedules the crons
   *
   * @since 0.0.1
   * @return void
   */
  public function schedule_crons(): void {

    if (wp_get_schedule('wub_cron_6h_sync_plans') === false) {

      add_action('admin_init', [$this, 'create_schedule'], 20);

    } // end if;

  } // end schedule_crons;

  /**
   * Creates two new cron schedules, every 5 minutes and every 20 minutes
   *
   * @since 0.0.1
   * @param array $schedules
   * @return array
   */
  public function add_new_cron_schedules($schedules): array {

    $schedules['6h'] = [
      'interval' => 6 * HOUR_IN_SECONDS,
      'display'  => __('Once every 6 hours'),
    ];

    return $schedules;

  } // end add_new_cron_schedules;

  /**
   * Register the schedules
   *
   * @since 0.0.1
   * @return void
   */
  public function create_schedule(): void {

    // Add sync plans cron only in main blog
    if (get_network()->site_id === get_current_blog_id()) {
        wp_schedule_event(time(), '6h', 'wub_cron_6h_sync_plans');
    }

  } // end create_schedule;

  /**
   * Add the plan resync to run every 20 minutes
   *
   * @since 0.0.1
   * @return void
   */
  public function run_plan_resync(): void {

    WU_Logger::add('wub-cron', __('Started plan syncing...', 'wp-ultimo'));

    $plans = WUB_Sync()->sync_plans();

  } // end run_plan_resync;

} // end WUB_Cron_Service;

// Activate Ajax Service
WUB_Cron_Service::get_instance();

endif;