<?php
/**
 * UI Elements
 *
 * Adds UI elements with important triggers for API calls, actions and more.
 *
 * @category   WP Ultimo
 * @package    Billwerk
 * @author     Arindo Duque <arindo@wpultimo.com>
 * @since      0.0.1
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

if (!class_exists('WUB_Plan_Changes')) :

class WUB_Plan_Changes {

  /**
   * Makes sure we are only using one instance of the plugin
   * 
   * @since 0.0.1
   * @var object WUB_Plan_Changes
   */
  public static $instance;

  /**
   * Returns the instance of WUB_Plan_Changes
   * 
   * @since 0.0.1
   * @return WUB_Plan_Changes A WUB_Plan_Changes instance
   */
  public static function get_instance() {

    if (null === self::$instance) self::$instance = new self();

    return self::$instance;
    
  } // end get_instance;

  /**
   * Adds the hooks and actions
   *
   * @since 0.0.1
   * @return void
   */
  public function __construct() {

    /**
     * Adds the Billwerk Integration options to the plan edit page on WP Ultimo
     */
    // add_filter('wu_plans_advanced_options_tabs', array($this, 'add_billwerk_tab_to_plan_options'));
    // add_action('wu_plans_advanced_options_after_panels', array($this, 'add_billwerk_content_to_plan_options'));

    add_filter('get_space_allowed', array($this, 'change_upload_quota_based_on_plan'));

    /**
     * Prevent disk space chhange event from trigerring,
     * but makes sure we save the new value anyways onto the plan object.
     */
    add_action('wu_plan', function() {

      if (isset($_POST['quotas']['upload'])) {

        $new_quota = $_POST['quotas']['upload'];

        unset($_POST['quotas']['upload']);

        add_action('wu_save_plan', function($plan) use ($new_quota) {

          $plan->quotas['upload'] = $new_quota;

          $plan->save();

        });

      } // end if;

    });

  } // end construct;

  /**
   * Instead of looping through sites and changing the values manually, which does not scale,
   * we'll use a filter to replace the value with the plan's limit.
   * 
   * The downside to this approach is that you are no longer able to customize the option on a 
   * site per site basis.
   * 
   * This fix replicates the fix we added to WP Ultimo core.   
   *
   * @param int $space_allowed Space allowed, in MBs.
   * @return void New space in MBs.
   */
  public function change_upload_quota_based_on_plan($space_allowed) {

    if (current_user_can('manage_network')) {

      return 999999;

    } // end if;

    $subscription = wu_get_current_subscription();

    if ($subscription) {

      $plan = $subscription->get_plan();

      $space_allowed = $plan ? $plan->get_quota('upload') : $space_allowed;

    } // end if;

    return $space_allowed;

  } // end change_upload_quota_based_on_plan;

  /**
   * DEPRECATED
   *
   * @since 0.0.1
   * @param array $tabs
   * @return array
   */
  public function add_billwerk_tab_to_plan_options($tabs) {

    $billwerk = array(
      'billwerk' => __('Billwerk Integration', 'wu-billwerk'),
    );

    return array_merge($billwerk, $tabs);

  } // end add_billwerk_tab_to_plan_options;

  /**
   * DEPRECATED
   *
   * @since 0.0.1
   * @param WU_Plan $plan
   * @return void
   */
  public function add_billwerk_content_to_plan_options($plan) {

    WP_Ultimo_Billwerk()->render('plans/billwerk-tab', compact('plan'));

  } // end add_billwerk_content_to_plan_options;

} // end WUB_Plan_Changes;

// Adds general purpose UI elements
WUB_Plan_Changes::get_instance();

endif;