<?php
/**
 * Ajax Services
 *
 * Register all the ajax services we'll need on the UI,
 * and there will be a lot of them since we'll be calling Billwerk's API frequently.
 *
 * @category   WP Ultimo
 * @package    Billwerk
 * @author     Arindo Duque <arindo@wpultimo.com>
 * @since      0.0.1
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

if (!class_exists('WUB_Checkout')) :

class WUB_Checkout {

  /**
   * Makes sure we are only using one instance of the plugin
   *
   * @since 0.0.1
   * @var object WUB_Checkout
   */
  public static $instance;

  /**
   * Holds the user for later use
   */
  public $user;

  /**
   * Returns the instance of WUB_Checkout
   *
   * @return object A WUB_Checkout instance
   */
  public static function get_instance() {

    if (null === self::$instance) self::$instance = new self();

    return self::$instance;

  } // end get_instance;

  /**
   * Register the Ajax service providers
   *
   * @since 0.0.1
   * @return void
   */
  public function __construct() {

    add_action('parse_request', array($this, 'checkout'));

    add_action('admin_init', array($this, 'redirect_to_payment'), -10);

    add_action('checkout_enqueue_scripts', array($this, 'enqueue_billwerk_scripts'));

    add_action('init', array($this, 'get_user'));

    add_action('wu_after_signup_form', array($this, 'add_coupon_refresher'));

    add_filter('wu_pricing_table_plan', array($this, 'add_plan_attributes_from_billwerk'), 10, 2);

    add_filter('wp_ultimo_registration_step_plans_save_transient', array($this, 'save_coupon_id'));

    add_filter('wu_gateway_get_url', array($this, 'redirect_to_dashboard_after_successful_payment'), 10, 2);

    // add_filter('wu_view_override', array($this, 'use_alternative_plan_template'), 10, 2);

    // add_action('init', array($this, 'add_name_fields'));

  } // end construct;

  /**
   * Redirect the user to the Dashboard instead of the account page
   *
   * @since 0.0.1
   * @param string $url
   * @param string $page_slug
   * @return string the new URL
   */
  function redirect_to_dashboard_after_successful_payment($url, $page_slug) {

    if ($page_slug == 'success') {

      /**
       * Add grace period after the activation is done.
       */
      wu_get_current_subscription()->create_activation_permission();

      /**
       * Returns the dashboard URL
       */
      return admin_url();

    } // end if;

    return $url;

  } // end redirect_to_dashboard_after_successful_payment;

  /**
   * Saves the coupon slug for use later on the signup
   *
   * @since 0.0.1
   * @param array $transient
   * @return array
   */
  public function save_coupon_id($transient) {

    if (isset($_REQUEST['coupon_id'])) {

      $transient['coupon_id'] = $_REQUEST['coupon_id'];

    } // end if;

    return $transient;

  } // end save_coupon_id;

  /**
   * We need to add the variant ids from billwerk on the plan attributes
   *
   * @since 0.0.1
   * @param string $atts
   * @param WU_Plan $plan
   * @return string
   */
  public function add_plan_attributes_from_billwerk($atts, $plan) {

    $billwerk_variants = $plan->billwerk_variant_ids ?: array();

    foreach($billwerk_variants as $freq => $variant_id) {

      $atts .= " data-variant-id-{$freq}='{$variant_id}'";

    } // end foreach;

    return $atts;

  } // end add_plan_attributes_from_billwerk;

  /**
   * Display a different template for the plan
   *
   * @since 0.0.1
   * @param string $template
   * @param string $view
   * @return string
   */
  public function use_alternative_plan_template($template, $view) {

    if ($view == 'signup/pricing-table/plan') {

      return WP_Ultimo_Billwerk()->path('views/signup/plan.php');

    } // end if;

    return $template;

  } // end use_alternative_plan_template;

  /**
   * Add the coupon field
   *
   * @since 0.0.1
   * @return void
   */
  public function add_coupon_refresher() {

    if (isset(WU_Signup()->step) && WU_Signup()->step == 'plan') {

      WP_Ultimo_Billwerk()->render('signup/checkout/coupon-code');

    } // end if;

  } // end add_coupon_refresher;

  /**
   * Sets the current user
   *
   * @since 0.0.1
   * @return void
   */
  public function get_user() {

    /**
     * Set the user
     */
    $this->user = wp_get_current_user();

  } // end get_user;

  /**
   * Adds name fields to the Signup flow
   *
   * @since 0.0.1
   * @return void
   */
  public function add_name_fields() {

    /**
     * First, we add a new step. The ID of this step is customer-info
     * We also pass a order value of 33, that means that our custom step will sit between
     * the third (order 30) and forth (order 40) steps.
     */
    wu_add_signup_step('customer-info', 33, array(
      'name' => __('Your Information', 'wu-billwerk'),
    ));

      /**
       * Now we add the fields
       * The first argument tells the API the step we want to add fields to, in our case, customer-info
       * The type argument tells which type of field we should render, we currently support
       * text, number, password, email, url, html, submit
       */
    wu_add_signup_field('customer-info', 'first_name', 10, array(
      'name'    =>  __('First Name', 'wu-billwerk'),
      'type'    => 'text',
    ));

    wu_add_signup_field('customer-info', 'last_name', 20, array(
      'name'    =>  __('Last Name', 'wu-billwerk'),
      'type'    => 'text',
    ));

    /**
     * Lastly, we add a submit button for that step
     */
    wu_add_signup_field('customer-info', 'submit', 100, array(
      'name'    =>  __('Go to next step', 'wu-billwerk'),
      'type'    => 'submit',
    ));

  } // end add_name_fields;

  /**
   * Holds the billing address fields. Can be filtered.
   *
   * @since 0.0.1
   * @return array
   */
  public function payment_billing_address_fields() {

    return apply_filters('wub_get_payment_billing_address_fields', array(
      'first_name' => array(
        'order'         => 30,
        'type'          => 'text',
        'name'          => __('First Name', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => true,
        'attributes'    => array(
          'v-model'     => 'customer.FirstName',
        ),
        'wrapper_attributes' => array(
          'class'   => 'wu-col-sm-6 wu-col-first'
        ),
      ),
      'last_name' => array(
        'order'         => 30,
        'type'          => 'text',
        'name'          => __('Last Name', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => true,
        'attributes'    => array(
          'v-model'     => 'customer.LastName',
        ),
        'wrapper_attributes' => array(
          'class'   => 'wu-col-sm-6 wu-col-last'
        ),
      ),
      'company' => array(
        'order'         => 30,
        'type'          => 'text',
        'name'          => __('Company Name', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => false,
        'attributes'    => array(
          'v-model'     => 'customer.CompanyName',
        ),
      ),
      'email' => array(
        'order'         => 10,
        'type'          => 'text',
        'name'          => __('Email Address', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => true,
        'attributes'    => array(
          'placeholder'   => __('p.muller@email.com', 'wu-billwerk'),
          'value'         => $this->user->user_email,
          'v-model'       => 'customer.EmailAddress',
        ),
      ),
      'phone' => array(
        'order'         => 20,
        'type'          => 'text',
        'name'          => __('Phone Number', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => false,
        'attributes'    => array(
          'v-model'     => 'customer.PhoneNumber',
        ),
      ),
      'street' => array(
        'order'         => 30,
        'type'          => 'text',
        'name'          => __('Street', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => true,
        'attributes'    => array(
          'v-model'     => 'customer.Address.Street',
        ),
        'wrapper_attributes' => array(
          'class'   => 'wu-col-sm-8 wu-col-first'
        ),
      ),
      'house_number' => array(
        'order'         => 40,
        'type'          => 'text',
        'name'          => __('House No.', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => true,
        'attributes'    => array(
          'v-model'     => 'customer.Address.HouseNumber',
        ),
        'wrapper_attributes' => array(
          'class'   => 'wu-col-sm-4 wu-col-last'
        ),
      ),
      'zip' => array(
        'order'         => 50,
        'type'          => 'text',
        'name'          => __('ZIP Code', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => true,
        'attributes'    => array(
          'v-model'     => 'customer.Address.PostalCode',
        ),
      ),
      'city' => array(
        'order'         => 60,
        'type'          => 'text',
        'name'          => __('City', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => true,
        'attributes'    => array(
          'v-model'     => 'customer.Address.City',
        ),
      ),
      'country' => array(
        'order'         => 70,
        'type'          => 'select',
        'name'          => __('Country', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => true,
        'attributes'    => array(
          'v-model'     => 'customer.Address.Country',
          'v-on:change' => 'refresh_preview',
        ),
        'default'       => 'DE',
        'options'       => WU_Settings::get_countries(),
      ),
      'vat' => array(
        'order'         => 80,
        'type'          => 'text',
        'name'          => __('VAT Number', 'wu-billwerk'),
        'tooltip'       => __('Applicable for countries of the European Union', 'wu-billwerk'),
        'required'      => false,
        'attributes'    => array(
          'v-model'     => 'customer.VatId',
          'v-on:change' => 'refresh_preview',
        ),
      ),
    ), $this->user);

  } // end payment_billing_address_fields;

  /**
   * Returns the available Payment Options
   *
   * @since 0.0.1
   * @return array
   */
  public function get_payment_options() {

    $options = array_keys( WU_Settings::get_setting('billwerk_payment_options', array()) );

    $options = array_combine($options, $options);

    return array_map(function($item) {

      if (strpos($item, 'OnAccount') !== false || $item == 'InvoicePayment') {

        return __('On Account', 'wu-billwerk');

      } else if (strpos($item, 'CreditCard') !== false) {

        return __('Credit Card', 'wu-billwerk');

      } else if (strpos($item, 'Debit') !== false) {

        return __('Direct Debit', 'wu-billwerk');

      } else if ($item == 'PayPal') {

        return __('PayPal', 'wu-billwerk');

      }

      return '--';

    }, $options);

  } // end get_payment_options;

  /**
   * Get the default payment option based on the order set on the settings
   *
   * @since 1.8.3
   * @return void
   */
  public function get_default_payment_option() {

    $options = array_keys( $this->get_payment_options() );

    return current( $options );

  } // end get_default_payment_option;

  /**
   * Checks if this method needs card fields
   *
   * @since 0.0.1
   * @return array
   */
  public function has_card_fields() {

    return array(
      'CreditCard:FakePSP',
      'CreditCard:PayOne'
    );

  } // end has_card_fields;

  /**
   * Checks if this method needs card fields
   *
   * @since 0.0.1
   * @return array
   */
  public function has_direct_debit_fields() {

    return array(
      'Debit:FakePSP',
      'Debit:PayOne'
    );

  } // end has_direct_debit_fields;

  /**
   * Holds the billing option fields (payment methods)
   *
   * @since 0.0.1
   * @return array
   */
  public function payment_billing_option_fields() {

    return apply_filters('wub_get_payment_billing_option_fields', array(

      // 'has_coupon_code' => array(
      //   'order'         => 0,
      //   'type'          => 'checkbox',
      //   'name'          => __('Have a Coupon Code?', 'wu-billwerk'),
      //   'tooltip'       => __('Do you have a coupon code?', 'wu-billwerk'),
      // ),
      // 'coupon_code' => array(
      //   'order'         => 5,
      //   'type'          => 'text',
      //   'name'          => __('Coupon Code', 'wu-billwerk'),
      //   'tooltip'       => '',
      //   'requires'      => array(
      //     'has_coupon_code' => true,
      //   ),
      //   'attributes'    => array(
      //     'v-model'     => 'cart.couponCode',
      //     'v-on:change' => 'refresh_preview'
      //   ),
      // ),
      'payment_method' => array(
        'order'         => 10,
        'type'          => 'html',
        'content'       => $this->get_payment_option_fields(),
        'name'          => __('Payment Method', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => true,
        'options'       => $this->get_payment_options(),
        'attributes'    => array(
          'v-model'       => 'payment_data.bearer',
        ),
      ),
      'cc_holder' => array(
        'order'         => 10,
        'type'          => 'text',
        'name'          => __('Card Holder', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => false,
        'requires'      => array(
          'payment_method' => $this->has_card_fields(),
        ),
        'attributes'    => array(
          'placeholder'   => __('Paul Müller', 'wu-billwerk'),
          'v-model'       => 'payment_data.cardHolder',
        ),
      ),
      'cc_number' => array(
        'order'         => 30,
        'type'          => 'text',
        'name'          => __('Card Number', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => false,
        'requires'      => array(
          'payment_method' => $this->has_card_fields(),
        ),
        'attributes'    => array(
          'placeholder'   => __('················', 'wu-billwerk'),
          'maxlength'     => 19,
          'v-model'       => 'payment_data.cardNumber',
        ),
      ),
      'cc_cvc' => array(
        'order'         => 40,
        'type'          => 'text',
        'name'          => __('CVC', 'wu-billwerk'),
        'tooltip'       => __('This is usually a 3-digit number located on the back of your card', 'wu-billwerk'),
        'required'      => false,
        'requires'      => array(
          'payment_method' => $this->has_card_fields(),
        ),
        'attributes'    => array(
          'placeholder'   => __('···', 'wu-billwerk'),
          'v-model'       => 'payment_data.cvc',
        ),
      ),
      'cc_month' => array(
        'order'         => 50,
        'type'          => 'select',
        'name'          => __('Month', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => false,
        'requires'      => array(
          'payment_method' => $this->has_card_fields(),
        ),
        'options'       => self::get_month_range(),
        'attributes'    => array(
          'v-model'       => 'payment_data.expiryMonth',
        ),
        'wrapper_attributes' => array(
          'class'   => 'wu-col-sm-6 wu-col-first'
        ),
      ),
      'cc_year' => array(
        'order'         => 60,
        'type'          => 'select',
        'name'          => __('Year', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => false,
        'requires'      => array(
          'payment_method' => $this->has_card_fields(),
        ),
        'options'       => self::get_year_range(),
        'attributes'    => array(
          'v-model'       => 'payment_data.expiryYear',
        ),
        'wrapper_attributes' => array(
          'class'   => 'wu-col-sm-6 wu-col-last'
        ),
      ),
      'cc_preview' => array(
        'order'         => 60,
        'type'          => 'html',
        'content'       => '<div class="wu-clearfix"></div><div class="card-preview"></div>',
        'requires'      => array(
          'payment_method' => $this->has_card_fields(),
        ),
      ),
      'debit_holder' => array(
        'order'         => 10,
        'type'          => 'text',
        'name'          => __('Account Holder', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => false,
        'requires'      => array(
          'payment_method' => $this->has_direct_debit_fields(),
        ),
        'attributes'    => array(
          'placeholder'   => __('Paul Müller', 'wu-billwerk'),
          'v-model'       => 'payment_data.accountHolder',
        ),
      ),
      'debit_iban' => array(
        'order'         => 30,
        'type'          => 'text',
        'name'          => __('IBAN', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => false,
        'requires'      => array(
          'payment_method' => $this->has_direct_debit_fields(),
        ),
        'attributes'    => array(
          'placeholder'   => __('················', 'wu-billwerk'),
          'v-model'       => 'payment_data.iban',
        ),
      ),
      'debit_bic' => array(
        'order'         => 30,
        'type'          => 'text',
        'name'          => __('BIC', 'wu-billwerk'),
        'tooltip'       => '',
        'required'      => false,
        'requires'      => array(
          'payment_method' => $this->has_direct_debit_fields(),
        ),
        'attributes'    => array(
          'placeholder'   => __('················', 'wu-billwerk'),
          'v-model'       => 'payment_data.bic',
        ),
      ),
    ), $this->user);

  } // end payment_billing_option_fields;

  /**
   * Get the payment option fields.
   *
   * @return string
   */
  public function get_payment_option_fields() { 
    
    ob_start(); ?>

    <p>
            
      <?php foreach ($this->get_payment_options() as $payment_option_slug => $payment_option_label) : ?>

        <label for="payment_method-<?php echo $payment_option_slug; ?>">
          <input type="radio" name="payment_method" id="payment_method-<?php echo $payment_option_slug; ?>" value="<?php echo $payment_option_slug; ?>" v-model="payment_data.bearer">
          <?php echo $payment_option_label; ?>
        </label>

      <?php endforeach; ?>

      <input type="hidden" id="payment_method" v-model="payment_data.bearer">

    </p>

    <?php 
    
    return ob_get_clean();

  } // get_payment_option_fields;

  /**
   * Returns the submit button for the payment form
   *
   * @since 0.0.1
   * @return array
   */
  public function payment_submit_fields() {

    return array(
      'submit' => array(
        'order'         => 100,
        'type'          => 'submit',
        'name'          => __('Process Payment &rarr;', 'wu-billwerk'),
        'attributes'    => array(
          'v-bind:disabled'  => 'order === false || loading',
          'v-on:click'  => 'pay'
        ),
      ),
    );

  } // end payment_submit_fields;

  /**
   * Displayes the order summary at the top of the page
   *
   * @since 0.0.1
   * @return void
   */
  public function payment_order_summary() {

    ob_start();

      WP_Ultimo_Billwerk()->render('signup/checkout/order-summary');

    $order_summary_template = ob_get_clean();

    return array(
      'order-summary' => array(
        'order'         => 10,
        'type'          => 'html',
        'content'       => $order_summary_template,
      ),
    );

  } // end payment_order_summary;

  /**
   * Returns a month range array, for our month options
   *
   * @since 0.0.1
   * @return array
   */
  public static function get_month_range() {

    $months = range(1, 12);

    $list = array();

    foreach($months as $month) {

      $list[ str_pad($month, 2, '0', STR_PAD_LEFT) ] = date_i18n('F', mktime(0, 0, 0, $month, 1));

    } // end foreach;

    return $list;

  } // end if;

  /**
   * Returns an year range, from this year to the next 7
   *
   * @since 0.0.1
   * @return array
   */
  public static function get_year_range() {

    $current_year = (int) date('Y');

    $range = range($current_year, $current_year + 7);

    return array_combine($range, $range);

  } // end get_year_range;

  /**
   * Tests a site slug to see if it starts of the test prefix.
   *
   * @since 1.0.1
   * @param string $slug Site slug to test.
   * @return boolean
   */
  public function is_test_site($slug) {

    $test_prefix = WU_Settings::get_setting('billwerk_test_prefix', 'test20');

    $is_test_site = strpos($slug, $test_prefix) === 0;

    return apply_filters('wub_is_test_site', $is_test_site, $slug, $test_prefix);

  } // end is_test_site;

  /**
   * Gets the site slug based on the URL
   *
   * @since 1.0.1
   * @param WP_Site $site Site instance.
   * @return string
   */
  public function get_site_slug($site) {

    if (!is_a($site, 'WP_Site')) return '';

    if (is_subdomain_install()) {

      return reset(explode(".", $site->domain));

    } // end if;

    return trim($site->path, '/'); // Case subdirectory;

  } // end get_site_slug;

  /**
   * Gets the full site slug based on the URL
   *
   * @since 1.0.2
   * @param WP_Site $site Site instance.
   * @return string
   */
  public function get_full_site_slug($site) {

    if (!is_a($site, 'WP_Site')) return '';

    return trim($site->domain . $site->path, '/'); // Case subdirectory;

  } // end get_full_site_slug;

  /**
   * Enqueue the billwerk scripts
   *
   * @since 0.0.1
   * @return void
   */
  public function enqueue_billwerk_scripts() {

    /**
     * Get the current plan
     */
    $subscription = wu_get_subscription( get_current_user_id() );
    $plan = $subscription ? $subscription->get_plan() : false;
    $site = get_active_blog_for_user( $this->user->ID );
    $site_id = $site->blog_id;
    $site_full_slug = $this->get_full_site_slug($site);
    $site_slug = $this->get_site_slug($site);

    /**
     * Get the script version
     */
    $scripts_version = WP_Ultimo_Billwerk()->version;

    $suffix = WP_Ultimo_Billwerk()->get_js_suffix();

    /**
     * Register the Script for Billwerk
     */
    wp_register_script('wub-billwerk-js', WUB_API()->environment_url('https://selfservice.sandbox.billwerk.com/subscription.js', ''), array('jquery'), $scripts_version);

    wp_register_script('wub-card-js', WP_Ultimo_Billwerk()->get_asset("plugins/card$suffix.js", 'js'), array('jquery'), $scripts_version);

    wp_register_script('wub-moment-js', WP_Ultimo_Billwerk()->get_asset("plugins/moment$suffix.js", 'js'), array('jquery'), $scripts_version);

    wp_register_script('wub-billwerk-integration', WP_Ultimo_Billwerk()->get_asset("wub-billwerk-integration$suffix.js", 'js'), array('jquery', 'wp-ultimo', 'wub-billwerk-js', 'wub-card-js', 'wub-moment-js'), $scripts_version);

    wp_localize_script('wub-billwerk-integration', 'wub_billwerk_options', array(
      'general_error_msg'   => WU_Settings::get_setting('billwerk_general_error_message'),
      'public_api'          => WU_Settings::get_setting('billwerk_public_api_key'),
      'return_url'          => home_url('/process-payment'),
      'finalize_url'        => home_url('/finalize'),
      'process_payment_url' => home_url('/process-payment'),
      'order_summary_url'   => wp_nonce_url(admin_url('admin-ajax.php?action=wub_get_order_summary'), 'get_order_summary'),
      'log_js_errors_url'   => wp_nonce_url(admin_url('admin-ajax.php?action=wub_log_js_errors'), 'log_js_errors'),
      'error_message'       => __('An error occured while trying to retrieve the order.', 'wu-billwerk'),
      'i10n'                => array(
        'name_placeholder'   => __('Paul Müller', 'wu-billwerk'),
      ),
      'cart'       => array(
        'planVariantId' => $plan ? $plan->billwerk_variant_ids[ $subscription->freq ] : '',
        'couponCode'    => get_user_meta($this->user->ID, 'coupon_id', true),
      ),
      'payment_data' => array(
        'bearer' => $this->get_default_payment_option(),
      ),
      'customer' => array(
        'FirstName'    => $this->user->first_name,
        'LastName'     => $this->user->last_name,
        'EmailAddress' => $this->user->user_email,
        'Tag'          => $this->user->ID,
        'Locale'       => str_replace('_', '-', get_locale()),
        'CompanyName'  => get_blog_option($site_id, 'woocommerce_store_owner_company', ''),
        'Address'      => (object) array(
          'Street'       => get_blog_option($site_id, 'woocommerce_store_address_street', ''),
          'HouseNumber'  => get_blog_option($site_id, 'woocommerce_store_address_house_number', ''),
          'PostalCode'   => get_blog_option($site_id, 'woocommerce_store_postcode', ''),
          'City'         => get_blog_option($site_id, 'woocommerce_store_city', ''),
          'Country'      => get_blog_option($site_id, 'woocommerce_default_country', 'DE'),
        ),
        'CustomFields' => array(
           'is_test'   => $this->is_test_site($site_slug) ? 1 : 0,
           'shop_slug' => $site_full_slug,
        ),
      )
    ));

    wp_enqueue_script('wub-billwerk-integration');

    /**
     * Styles
     */
    wp_enqueue_style('wub-checkout', WP_Ultimo_Billwerk()->get_asset("checkout$suffix.css", 'css'), false, $scripts_version);

  } // end enqueue_billwerk_scripts;

  /**
   * Redirects the user to the payment screen
   *
   * @since 0.0.1
   * @return void
   */
  public function redirect_to_payment() {

    // Don't go to payment if you are super admin or if subscription doesn't exist.
    if (\is_super_admin() || !$subscription = wu_get_current_site()->get_subscription()) return;

    // Check if there is a trial or the trial is over
    if ((float)$subscription->price > 0 && !$subscription->integration_status && !$subscription->is_active()) {

      wp_redirect(get_site_url(1, '/payment')); exit;

    } // end if;

  } // end redirect_to_payment;

  /**
   * Checks if the user already has a subscription active.
   * If so, redirect to the panel instead of redirecting to the payment page
   *
   * Fixes https://github.com/aanduque/wp-ultimo-billwerk/issues/2
   *
   * @since 0.0.1
   * @return void
   */
  public function maybe_redirect_to_admin_panel() {

    $subscription = wu_get_subscription($this->user->ID);

    if (!$subscription || $subscription->is_active() || $subscription->integration_status) {

      switch_to_blog( get_active_blog_for_user( $this->user->ID )->blog_id );

      wp_redirect( admin_url() );

      exit;

    } // end if;

  } // end maybe_redirect_to_admin_panel;

  /**
   * Renders the checkout page
   *
   * @since 0.0.1
   * @param WP_Qeury $query
   * @return void
   */
  public function checkout($query) {

    if ($query->request === WU_Settings::get_setting('billwerk_payment_url', 'payment')) {

      $this->maybe_redirect_to_admin_panel();

      WP_Ultimo_Billwerk()->render('signup/checkout/payment', array(
        'signup' => WU_Signup()
      ));

      exit;

    } // end if;

    if ($query->request === WU_Settings::get_setting('billwerk_finalize_url', 'finalize')) {

      $this->maybe_redirect_to_admin_panel();

      WP_Ultimo_Billwerk()->render('signup/checkout/finalize', array(
        'signup' => WU_Signup()
      ));

      exit;

    } // end if;

    if ($query->request === 'process-payment') {

      update_user_meta($this->user->ID, 'billwerk_contract_id', $_REQUEST['ContractId']);
      update_user_meta($this->user->ID, 'billwerk_customer_id', $_REQUEST['CustomerId']);

      $subscription = wu_get_subscription($this->user->ID);

      $subscription->integration_status = 1;
      $subscription->integration_key    = $_REQUEST['ContractId'];
      $subscription->gateway            = 'billwerk';

      /**
       * Get the contract
       */

      $subscription->save();

      // Sync contract with billwerk
      WUB_Sync()->sync_contract($subscription->integration_key);

      WU_Mail()->send_template('subscription_created', $subscription->get_user_data('user_email'), array(
        'date'               => date(get_option('date_format')),
        'gateway'            => 'Billwerk',
        'billing_start_date' => $subscription->get_billing_start(get_option('date_format'), false),
        'user_name'          => $subscription->get_user_data('display_name')
      ));

      if (WU_Settings::get_setting('billwerk_use_thank_you_page', true)) {

        wp_redirect( home_url('thankyou') );

        exit;

      } // end if;

      switch_to_blog( get_active_blog_for_user( $this->user->ID )->blog_id );

      wp_redirect( wu_get_gateway('billwerk')->get_url('success') );

      exit;

    } // end if;

    if ($query->request === WU_Settings::get_setting('billwerk_thankyou_url', 'thankyou')) {

      echo 'Thank you page';

      die;

    } // end if;

  } // end checkout;

} // end WUB_Checkout;

/**
 * Returns the current instance
 *
 * @since 0.0.1
 * @return WUB_Checkout
 */
function WUB_Checkout() {

  return WUB_Checkout::get_instance();

} // end WUB_Checkout;

WUB_Checkout();

endif;
