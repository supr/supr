<?php
/**
 * API Wrapper for the Billwerk API
 *
 * This class contains is a wrapper for API calls to Billwerk.
 * It caches the results when need, logs errors, manage access tokens and more.
 *
 * @category   WP Ultimo
 * @package    Billwerk/API
 * @author     Arindo Duque <arindo@wpultimo.com>
 * @since      0.0.1
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

if (!class_exists('WUB_API')) :

class WUB_API {

  /**
   * Makes sure we are only using one instance of the plugin
   * 
   * @since 0.0.1
   * @var object WUB_API
   */
  public static $instance;

  /**
   * Decides if we are going to use the sandbox or live version
   *
   * @since 0.0.1
   * @var boolean
   */
  public $live = false;

  /**
   * Decides if we need to log results or errors
   *
   * @since 0.0.1
   * @var boolean
   */
  public $log = true;

  /**
   * Decides if we need to cache the results of API calls or not, useful for debugging
   *
   * @since 0.0.1
   * @var boolean
   */
  private $cache = true;

  /**
   * Used by the no_cache method to allow a single request to ignore the cache mechanism despite
   * of the caching settings being used. This is useful when retrieving data we must get from the server
   * and not from the local copy
   *
   * @var boolean
   * @since
   */
  private $ignore_cache = false;

  /**
   * How long should we keep caches in?
   *
   * @since 0.0.1
   * @var integer
   */
  private $cache_expire = HOUR_IN_SECONDS;

  /**
   * oAuth token endpoint
   *
   * @since 0.0.1
   * @var string
   */
  private $token_endpoint;

  /**
   * Base endpoint for all the API calls
   *
   * @since 0.0.1
   * @var string
   */
  private $base_endpoint;

  /**
   * Holds the client id for the Billwerk API, used when retrieving the Access Token
   *
   * @since 0.0.1
   * @var string
   */
  private $client_id = '';

  /**
   * Holds the client secret for the Billwerk API, used when retrieving the Access Token
   *
   * @since 0.0.1
   * @var string
   */
  private $client_secret = '';

  /**
   * Holds the access token for the Billwerk API, this is the return of the Access Token call
   *
   * @since 0.0.1
   * @var string
   */
  private $token = '';

  /**
   * Returns the instance of WUB_API
   * @return WUB_API A WUB_API instance
   */
  public static function get_instance() {

    if (null === self::$instance) self::$instance = new self();

    return self::$instance;
    
  } // end get_instance;

  public function __construct() {

    /**
     * Setup the environment
     */
    $this->token_endpoint = $this->environment_url("https://sandbox.billwerk.com/oauth/token/");
    $this->base_endpoint  = $this->environment_url("https://sandbox.billwerk.com/api/v1/");

    /**
     * Set the caches
     */
    $this->cache = WU_Settings::get_setting('billwerk_use_cache', true);
    $this->cache_expire = WU_Settings::get_setting('billwerk_cache_expire', HOUR_IN_SECONDS);

  } // end construct;

  /**
   * Checks which the URL we want to use, depending on the environment
   *
   * @since 0.0.1
   * @param string $url
   * @param string $replace_with
   * @return string
   */
  public function environment_url($url, $replace_with = "app.") {

    $is_sandbox = WU_Settings::get_setting('billwerk_sandbox_mode', true);

    if ($is_sandbox == false) {

      $url = str_replace('sandbox.', $replace_with, $url);

    } // end if;

    return $url;

  } // end environment_url;

  /**
   * Set the configuration parameters to the wrapper
   *
   * @since 0.0.1
   * @param array $config
   * @return void
   */
  public function set_config($config) {

    /**
     * Define the array of defaults
     */ 
    $defaults = array(
      'live'          => false, // Set if is SANDBOX or not
      'log'           => $this->log,
      'client_id'     => '',
      'client_secret' => '',
    );

    /**
     * Parse incoming $args into an array and merge it with $defaults
     */ 
    $args = wp_parse_args($config, $defaults);

    foreach($args as $param => $value) {

      $this->{$param} = $value;

    } // end foreach;

    /**
     * Get Token
     */
    $this->token = $this->get_token();

  } // end set_config;

  /**
   * Returns the URL of the admin panel
   *
   * @since 0.0.1
   * @return string
   */
  public function get_panel_url() {

    return $this->environment_url("https://sandbox.billwerk.com");

  } // end get_panel_url;

  /**
   * Logs error messages and requests if the logging is enabled
   *
   * @param string $message
   * @return void
   * @since 0.0.1
   */
  public function log($message) {

    return $this->log && WU_Logger::add('billwerk-api', $message);

  } // end log;

  /**
   * Allows us to blacklist some requests from the caching mechanism
   *
   * @since 0.0.1
   * @return WUB_API
   */
  public function no_cache() {

    $this->ignore_cache = true;

    return $this;

  } // end no_cache;

  /**
   * Sends a Request to the Billwerk API
   *
   * @since 0.0.1
   * @param string $url
   * @param array $args
   * @return mixed
   */
  public function request($url, $args = array()) {

    $cache_key = $this->generate_cache_key($url, $args);

    $_response = $this->get_cache($cache_key);

    if ($_response) return $_response;

    $response = wp_remote_request($url, $args);

    if (is_wp_error($response)) {

      $this->log($response->get_error_message());

    } // end if;

    $body = wp_remote_retrieve_body($response);

    $decoded_response = json_decode($body);

    $this->set_cache($cache_key, $decoded_response);

    $this->ignore_cache = false;

    return $decoded_response;

  } // end request;

  /**
   * Return the access_token for the API
   *
   * @since 0.0.1
   * @return string
   */
  public function get_token() {

    $transient_name = $this->environment_url("billwerk_sandbox_access_token");

    $_token = get_site_transient($transient_name);

    if ($_token) return $_token;

    $token = $this->request($this->token_endpoint, array(
      'method' => 'POST',
      'body'   => array(
				'grant_type' => 'client_credentials'
      ),
      'headers' => array(
        'Authorization' => 'Basic ' . base64_encode("$this->client_id:$this->client_secret")
      )
    ));

    /**
     * Check if the values are present
     * @since 0.0.1
     */
    if (!isset($token->access_token)) {

      /**
       * Add error message, if it is present
       */
      if (isset($token->error)) {

        WP_Ultimo()->add_message(sprintf(__('Billwerk API Error: %s - %s', 'wu-billwerk'), $token->error, $token->error_description), 'error', true);

      } // end if;

      return false;

    } // end if;

    /**
     * Cache Access Token
     */
    set_site_transient($transient_name, $token->access_token, DAY_IN_SECONDS);

    return $token->access_token;

  } // end get_token;

  /**
   * Generates a unique cache key for each request with the same endpoint and same args
   *
   * @since 0.0.1
   * @param string $url
   * @param mixed $data
   * @return string
   */
  public function generate_cache_key($url, $data) {

    return md5($url.json_encode($data));

  } // end generate_cache_key;

  /**
   * Adds a request response to the object cache
   *
   * @since 0.0.1
   * @param string $key
   * @param mixed $data
   * @return bool
   */
  public function set_cache($key, $data) {

    if (!$this->cache || $this->ignore_cache) return false;

    return wp_cache_set($key, $data, 'billwerk_api', $this->cache_expire);

  } // end set_cache;

  /**
   * Returns a cached request response
   *
   * @since 0.0.1
   * @param string $key
   * @return mixed
   */
  public function get_cache($key) {

    if (!$this->cache || $this->ignore_cache) return false;

    return wp_cache_get($key, 'billwerk_api');

  } // end get_cache;

  /**
   * HTTP GET Wrapper for Billwerk
   *
   * @since 0.0.1
   * @param string $endpoint
   * @param array $data
   * @return mixed
   */
  public function get($endpoint, $data = array()) {

    $response = $this->request($this->base_endpoint.$endpoint, array(
      'method'  => 'GET',
      'timeout' => 15,
      'body'    => $data,
      'headers' => array(
        'Authorization' => "Bearer $this->token",
      ),
    ));

    return $response;

  } // end get;

  /**
   * HTTP POST Wrapper for Billwerk
   *
   * @since 0.0.1
   * @param string $endpoint
   * @param array $data
   * @return void
   */
  public function post($endpoint, $data = array()) {

    $response = $this->no_cache()->request($this->base_endpoint.$endpoint, array(
      'method'  => 'POST',
      'body'    => json_encode($data),
      'headers' => array(
        'Authorization' => "Bearer $this->token",
        'Content-Type'  => 'application/json',
      ),
    ));

    return $response;

  } // end post;

  /**
   * HTTP PUT Wrapper for Billwerk
   *
   * @since 0.0.1
   * @param string $endpoint
   * @param array $data
   * @return mixed
   */
  public function put($endpoint, $data = array()) {

    $response = $this->no_cache()->request($this->base_endpoint.$endpoint, array(
      'method'  => 'PUT',
      'body'    => json_encode($data),
      'headers' => array(
        'Authorization' => "Bearer $this->token",
        'Content-Type'  => 'application/json',
      ),
    ));

    return $response;

  } // end put;

  /**
   * HTTP DELETE Wrapper for Billwerk
   *
   * @since 0.0.1
   * @param string $endpoint
   * @param array $data
   * @return mixed
   */
  public function delete($endpoint, $data = array()) {

    $response = $this->no_cache()->request($this->base_endpoint.$endpoint, array(
      'method'  => 'DELETE',
      'body'    => $data,
      'headers' => array(
        'Authorization' => "Bearer $this->token",
      ),
    ));

    return $response;

  } // end delete;

  /**
   * HTTP PATCH Wrapper for Billwerk
   *
   * @since 0.0.1
   * @param string $endpoint
   * @param array $data
   * @return mixed
   */
  public function patch($endpoint, $data = array()) {

    $response = $this->no_cache()->request($this->base_endpoint.$endpoint, array(
      'method'  => 'PATCH',
      'body'    => json_encode($data),
      'headers' => array(
        'Authorization' => "Bearer $this->token",
        'Content-Type'  => 'application/json',
      ),
    ));

    return $response;

  } // end patch;

} // end WUB_API;

/**
 * Returns the active instance of the plugin
 *
 * @since 0.0.1
 * @return WUB_API
 */
function WUB_API($cache = true) {

  return WUB_API::get_instance($cache);

} // end WUB_API;

endif;