<?php

/**
 * Searches for s local subscription based on the contract ID from billwerk
 *
 * @since 0.0.1
 * @param string $contract_id
 * @return WU_Subscription|false
 */
function wub_get_subscription_by_contract_id($contract_id) {

  global $wpdb;

  $results = $wpdb->get_row($wpdb->prepare("SELECT user_id FROM ". WU_Subscription::get_table_name() ." WHERE integration_key = %s", $contract_id));

  if ($results) {

    return wu_get_subscription($results->user_id);

  } // end if;

  return false;

} // end get_subscription_by_contract_id;

/**
 * Searchs locally for a counter-part plan based on the plan id on Billwerk
 *
 * @since 0.0.1
 * @param string $billwerk_plan_id
 * @return WU_Plan|false
 */
function wub_get_local_plan_from_billwerk_plan_id($billwerk_plan_id) {

    $args = array(
    'post_type'      => 'wpultimo_plan',
    'posts_per_page' => 1,
    'meta_query'     => array(
      'plan_id'        => array(
        'key'          => 'wpu_billwerk_plan_id',
        'value'        => $billwerk_plan_id,
      )
    )
  );

  switch_to_blog( get_current_site()->blog_id );

    $results = query_posts($args);

  restore_current_blog();

  if (!empty($results)) {

    return wu_get_plan( $results[0]->ID );

  } // end if;

  return false;

} // end wub_get_local_plan_from_billwerk_plan_id;