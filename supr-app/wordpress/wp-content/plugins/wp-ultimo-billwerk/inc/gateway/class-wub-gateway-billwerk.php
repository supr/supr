<?php
/**
 * Manual Gateway
 *
 * Handles Manual Payments
 *
 * @author      Arindo Duque
 * @category    Admin
 * @package     WP_Ultimo/Gateways/Manual
 * @since       1.2.0
*/

if (!defined('ABSPATH')) {
  exit;
}

/**
 * WU_Gateway_Billwerk
 */
class WU_Gateway_Billwerk extends WU_Gateway {

  /**
   * Initialize the Gateway key elements
   */
  public function init() {

    add_filter('wu_plan_select_button_attributes', array($this, 'prevent_downgrades'), 10, 3);

    add_action('load-toplevel_page_wu-my-account', array($this, 'add_extra_meta_boxes'));

    add_action('init', array($this, 'register_billwerk_webhook_event'));

  } // end init;

  /**
   * Register the Billwerk default webhook event.
   *
   * @since 1.1.0
   * @return void
   */
  public function register_billwerk_webhook_event() {

    /**
     * Add the billwerk main event
     */
    WU_Webhooks()->register_event('billwerk_event', array(
      'name'        => __('Billwerk Event', 'wp-ultimo'),
      'description' => __('This event is fired every time WP Ultimo receives a webhook call from Billwerk. This is basically a pass-through with the Contract data.', 'wp-ultimo'),
      'data'        => array(
        "billwerk_event" => array(),
        "billwerk_contract" => array(),
        "wp_ultimo_subscription" => array(),
        "wp_ultimo_sites" => array(),
      )
    ));

  } // end register_billwerk_webhook_event;

  /**
   * Get Self Service URLs
   *
   * @since 1.0.1
   * @param string $contract_id The contract ID.
   * @return object
   */
  public function get_self_service_url($contract_id) {

    $_self_service = get_site_transient("wub_{$contract_id}_self_service");

    if ($_self_service) {

      return $_self_service;

    } // end if;

    $self_service = WP_Ultimo_Billwerk()->api->get("/Contracts/{$contract_id}/selfServiceToken");

    if ($self_service->Url) {

      set_site_transient("wub_{$contract_id}_self_service", $self_service, 10080 / 2);

      return $self_service;

    } // end if;

    return false;

  } // end get_self_service_url;

  /**
   * Add the change customer info metabox
   *
   * @since 1.0.1
   * @return void
   */
  public function add_extra_meta_boxes() {

    $screen = get_current_screen();

    $contract_id = wu_get_current_subscription()->integration_key;

    if ($contract_id) {

      $self_service = $this->get_self_service_url($contract_id);

      add_meta_box('wub-change-customer-info', __('Payment Data', 'wp-ultimo'), array($this, 'render_widget_change_customer_info'), 'wu-my-account', 'side', 'low', array('self_service' => $self_service));

    } // end if;

  } // end add_extra_meta_boxes;

  /**
   * Renders the change customer info metabox
   *
   * @since 1.0.1
   * @param string $id
   * @param array $self_service
   * @return void
   */
  public function render_widget_change_customer_info($id, $self_service) {

    if (isset($self_service['args']['self_service'])) {

      $ss = $self_service['args']['self_service'];

      printf('<a target="_blank" href="%s">%s</a>', $ss->Url, __('Click here to show or change your billing address or payment method.', 'wu-billwerk'));

    } // end if;

  } // end render_widget_change_customer_info;

  /**
   * Disabled the button that would allow users to initiate downgrades
   *
   * @since 1.0.0
   * @param string $attributes
   * @param WU_Plan $plan
   * @param WU_Plan $current_plan
   * @return string
   */
  public function prevent_downgrades($attributes, $plan, $current_plan) {

    if (!$current_plan || ! ( isset($_GET['action']) && $_GET['action'] == 'change-plan' ) ) return $attributes;

    if ($current_plan->order > $plan->order) {

      $attributes .= " disabled='disabled' ";

      add_filter('wu_plan_select_button_label', function($label, $_plan, $current_plan) use ($plan) {

        return $plan->id == $_plan->id ? __('Contact Support', 'wu-billwerk') : $label;

      }, 10, 3);

    } // end if;

    return $attributes;

  } // end prevent_downgrandes;

  /**
   * First step of the payment flow: proccess_form
   */
  public function process_integration() { } // end process_integration;

  /**
   * Do upgrade or downgrade of plans
   */
  public function change_plan() {

    // Just return in the wrong pages
    if (!isset($_POST['wu_action']) || $_POST['wu_action'] !== 'wu_change_plan') return;

    // Security check
    if (!wp_verify_nonce($_POST['_wpnonce'], 'wu-change-plan')) {
      WP_Ultimo()->add_message(__('You don\'t have permissions to perform this action.', 'wp-ultimo'), 'error');
      return;
    }

    if (!isset($_POST['plan_id'])) {
      WP_Ultimo()->add_message(__('You need to select a valid plan to change to.', 'wp-ultimo'), 'error');
      return;
    }

    // Check frequency
    if (!isset($_POST['plan_freq']) || !$this->check_frequency($_POST['plan_freq'])) {
      WP_Ultimo()->add_message(__('You need to select a valid frequency to change to.', 'wp-ultimo'), 'error');
      return;
    }

    // Get Plans - Current and new one
    $current_plan = $this->plan;
    $new_plan     = new WU_Plan((int) $_POST['plan_id']);

    if (!$new_plan->id) {
      WP_Ultimo()->add_message(__('You need to select a valid plan to change to.', 'wp-ultimo'), 'error');
      return;
    }

    /**
     * Check if this is a downgrade
     */
    if ($current_plan->order > $new_plan->order) {
      WP_Ultimo()->add_message(__('Automatic downgrades are not supported. Please contact support to perform a downgrade.', 'wp-ultimo'), 'error');
      return;
    }

    $variant_ids = $new_plan->billwerk_variant_ids;

    /**
     * Check if a valid variant exists
     */
    if (!isset($variant_ids[ $_POST['plan_freq'] ])) {
      WP_Ultimo()->add_message(__('You need to select a valid plan to change to.', 'wp-ultimo'), 'error');
      return;
    }

    $variant_id = $new_plan->billwerk_variant_ids[ $_POST['plan_freq'] ];

    /**
     * Handle the actual upgrade
     */
    $results = $this->create_billwerk_upgrade_order($new_plan->billwerk_plan_id, $variant_id, $this->subscription->integration_key);

    $new_price = $new_plan->{"price_".$_POST['plan_freq']};
    $new_freq  = (int) $_POST['plan_freq'];

    if ($results) {

      $this->subscription->plan_id            = $new_plan->id;
      $this->subscription->freq               = $new_freq;
      $this->subscription->price              = $new_price;
      $this->subscription->gateway            = 'billwerk';
      $this->subscription->integration_status = true;

      WUB_Sync()->sync_contract($this->subscription->integration_key);

      $this->subscription->save();

      // Redirect to success page
      wp_redirect(WU_Gateway::get_url('plan-changed'));

      exit;

    } // end if;

    return;

  } // end change_plan;

  /**
   * Creates the upgrade order on billwerk and commits it right afterwards
   *
   * @since 1.0.0
   * @param string $plan_id
   * @param string $variant_id
   * @param string $contract_id
   * @return bool
   */
  public function create_billwerk_upgrade_order($plan_id, $variant_id, $contract_id) {

    $order = WP_Ultimo_Billwerk()->api->post("/Orders", array(
      'ContractId' => $contract_id,
      'Cart'       => array (
        'MeteredUsages'          => array(),
        'ComponentSubscriptions' => array(),
        'DiscountSubscriptions'  => array(),
        'RatedItems'             => array(),
        'Quantity'               => 1,
        'PlanId'                 => $plan_id,
        'PlanVariantId'          => $variant_id,
      ),
      'ChangeDate' => null,
    ));

    /**
     * Failed to create the order
     */
    if (!isset($order->Id)) {

      return false;

    } // end if;

    $commit = WP_Ultimo_Billwerk()->api->post("/Orders/{$order->Id}/commit", array(
      'PaymentMethod'  => '',
      'RequirePayment' => true,
    ));

    if (!isset($commit->Id)) {

      return false;

    } // end if;

    return true;

  } // end create_billwerk_upgrade_order;

  /**
   * Remove the Manual integration
   */
  public function remove_integration($redirect = true, $subscription = false) { } // end remove_integration;

  /**
   * Handles the notifications
   */
  public function handle_notifications() {

    $input = @file_get_contents("php://input");

    $event = json_decode($input);

    WU_Logger::add('billwerk-webhooks', json_encode($event));

    /**
     * Handle expiring trials
     */
    if (isset($event->Event) && $event->Event == 'TrialEndApproaching') {

      $this->handle_trial_ending($event->ContractId);

    } // end if;

    /**
     * Handle Successfull Payments
     */
    if (isset($event->Event) && $event->Event == 'PaymentSucceeded') {

      $this->handle_payment_notifications($event->PaymentTransactionId, 'PaymentSucceeded');

    } // end if;

    /**
     * Handle Failed Payments
     */
    if (isset($event->Event) && $event->Event == 'PaymentFailed') {

      $this->handle_payment_notifications($event->PaymentTransactionId, 'PaymentFailed');

    } // end if;

    /**
     * Handle the Contract Changes
     *
     * Moved this to the bottom and remove the event check to make sure we re-sync contracts
     * everytime we get a ContractId.
     */
    if (isset($event->ContractId)) {

      $this->handle_contract_changes($event->ContractId);

    } // end if;

    /**
     * Adds the Billwerk Webhook event
     *
     * @since 1.1.0
     */
    $subscription = isset($event->ContractId) ? wub_get_subscription_by_contract_id($event->ContractId) : null;

    $sites = $subscription ? array_map(function($site_id) {

      return get_site($site_id);

    }, $subscription->get_sites_ids()) : [];

    $data = array(
      'billwerk_event'         => $event,
      'billwerk_contract'      => isset($event->ContractId) ? WP_Ultimo_Billwerk()->api->get("/Contracts/{$event->ContractId}") : null,
      'wp_ultimo_subscription' => $subscription,
      'wp_ultimo_sites'        => $sites,
    );

    WU_Webhooks::get_instance()->send_webhooks('billwerk_event', apply_filters('wu_billwerk_webhook_event_data', $data, $subscription));

    wp_send_json_success('Thanks, Billwerk!');

  } // end handle_notifications;

  /**
   * Handles Payment notifications sent by Billwerk
   *
   * @since 1.0.1
   * @param string $transaction_id
   * @param string $type
   * @return void
   */
  public function handle_payment_notifications($transaction_id, $type) {

    $transaction = WP_Ultimo_Billwerk()->api->no_cache()->get("/PaymentTransactions/{$transaction_id}");

    if (!$transaction) return;

    $contract_id = $transaction->ContractId;

    $subscription = wub_get_subscription_by_contract_id($contract_id);

    $contract_info = WP_Ultimo_Billwerk()->api->no_cache()->get("/Contracts/{$contract_id}");

    if (!$subscription || !$contract_info) return false;

    switch ($type) {

      /**
       * Successful Payment
       */
      case 'PaymentSucceeded':

        WU_Mail()->send_template('payment_receipt', $subscription->get_user_data('user_email'), array(
          'amount'           => wu_format_currency($transaction->Amount),
          'date'             => date(get_option('date_format')),
          'gateway'          => 'Billwerk',
          'new_active_until' => $subscription->get_date('active_until'),
          'user_name'        => $subscription->get_user_data('display_name')
        ));

        break;

      /**
       * Failed Payment
       */
      case 'PaymentFailed':

        WU_Mail()->send_template('payment_failed', $subscription->get_user_data('user_email'), array(
          'amount'           => wu_format_currency($transaction->Amount),
          'date'             => date(get_option('date_format')),
          'gateway'          => 'Billwerk',
          'user_name'        => $subscription->get_user_data('display_name'),
          'account_link'     => $subscription->get_manage_url(),
        ));

        WU_Mail()->send_template('payment_failed_admin', get_network_option(null, 'admin_email'), array(
          'amount'                       => wu_format_currency($transaction->Amount),
          'date'                         => date(get_option('date_format')),
          'gateway'                      => 'Billwerk',
          'user_name'                    => $subscription->get_user_data('display_name'),
          'subscription_management_link' => $subscription->get_manage_url(),
        ));

        break;

    } // end switch;

  } // end handle_payment_notifications;

  /**
   * Notify users when their subscription is expiring
   *
   * @since 1.0.1
   * @param string $contract_id
   * @return void
   */
  public function handle_trial_ending($contract_id) {

    $subscription = wub_get_subscription_by_contract_id($contract_id);

    $contract_info = WP_Ultimo_Billwerk()->api->no_cache()->get("/Contracts/{$contract_id}");

    if (!$subscription || !$contract_info) return false;

    $primary_site = get_active_blog_for_user( $subscription->user_id );

    $email_shortcodes = array(
      'user_name'                    => $subscription->get_user_data('diaplay_name'),
      'user_account_page_link'       => get_admin_url($primary_site->blog_id, 'admin.php?page=wu-my-account'),
      'subscription_management_link' => $subscription->get_manage_url(),
      'days_to_expire'               => WUB_Sync()->get_difference_in_days($contract_info->StartDate, $contract_info->NextBillingDate),
    );

    WU_Mail()->send_template('trial_expiring', $subscription->get_user_data('user_email'), $email_shortcodes);

  } // end handle_trial_ending;

  /**
   * Handles a notification that a change occurred on one contract
   *
   * @since 1.0.0
   * @param string $contract_id
   * @return void
   */
  public function handle_contract_changes($contract_id) {

    WUB_Sync()->sync_contract($contract_id);

  } // end handle_contract_changes,

  /**
   * Gets the available options on Billwerk
   *
   * @since 1.0.0
   * @return array
   */
  public function get_payment_options_from_billwerk() {

    $options = array();

    /**
     * Prevent espensive API call from being made on every page load.
     * 
     * The below check will make sure we only make this API call when necessary,
     * which is when editing the WP Ultimo settings.
     * 
     * This is a better solution than caching the results of that call, which can make
     * gateways unavailable for Admins to change.
     */
    if (!is_admin() || !isset($_GET['page']) || $_GET['page'] !== 'wp-ultimo') {

      return $options;

    } // end if;

    $payment_options = WP_Ultimo_Billwerk()->api->no_cache()->get('paymentProviderSettings?skip=0&take=1000');

    /**
     * Fixes https://github.com/aanduque/wp-ultimo-billwerk/issues/12
     * @since 1.0.0
     */
    if (!is_array($payment_options)) {

      return array();

    } // end if;

    foreach($payment_options as $payment_option) {

      $payment_option->PaymentProvider = $payment_option->PaymentProvider != 'FakeProvider'
        ?  $payment_option->PaymentProvider
        :'FakePSP';

      if (! $payment_option->Blocked && $payment_option->Valid) {

        if ($payment_option->IsCreditCardProvider && $payment_option->Status->CreditCardActive) {

          $options["CreditCard:$payment_option->PaymentProvider"] = sprintf(__('Credit Card - %s'), $payment_option->PaymentProvider);

        } // end if;

        if ($payment_option->IsDebitProvider && $payment_option->Status->DebitActive) {

          $options["Debit:$payment_option->PaymentProvider"] = sprintf(__('Direct Debit - %s'), $payment_option->PaymentProvider);

        } // end if;

        if ($payment_option->IsOnAccountProvider && $payment_option->Status->OnAccountActive) {

          $options["OnAccount:$payment_option->PaymentProvider"] = sprintf(__('On Account - %s'), $payment_option->PaymentProvider);

        } // end if;

        if ($payment_option->IsBlackLabelProvider && $payment_option->Status->BlackLabelActive) {

          $options["$payment_option->PaymentProvider"] = sprintf(__('Black Label - %s'), $payment_option->PaymentProvider);

        } // end if;

      } // end if;

    } // end foreach;

    return $options;

  } // end get_payment_options_from_billwerk;

  /**
   * Creates the custom fields need to our Gateway
   *
   * @return array Setting fields
   */
  public function settings() {

    // Defines this gateway settings field
    return array(

      'billwerk_client_id' => array(
        'title'            => __('Billwerk Client ID', 'wp-ultimo'),
        'desc'             => __('Enter the client ID for the Billwerk account you wish to use.', 'wp-ultimo'),
        'tooltip'          => '',
        'type'             => 'text',
        'placeholder'      => '',
        'default'          => '',
        'require'          => array('active_gateway[billwerk]' => true),
      ),

      'billwerk_client_secret' => array(
        'title'            => __('Billwerk Client Secret', 'wp-ultimo'),
        'desc'             => __('Enter the client Secret for the Billwerk account you wish to use.', 'wp-ultimo'),
        'tooltip'          => '',
        'type'             => 'password',
        'placeholder'      => '',
        'default'          => '',
        'require'          => array('active_gateway[billwerk]' => true),
      ),

      'billwerk_public_api_key' => array(
        'title'            => __('Billwerk Public API Key', 'wp-ultimo'),
        'desc'             => __('Public API key used by the BillwerkJS functions.', 'wp-ultimo'),
        'tooltip'          => '',
        'type'             => 'text',
        'placeholder'      => '',
        'default'          => '',
        'require'          => array('active_gateway[billwerk]' => true),
      ),

      'billwerk_sandbox_mode' => array(
        'title'            => __('Sandbox Mode', 'wp-ultimo'),
        'desc'             => __('Should we use Sandbox mode? Uncheck for live mode.', 'wp-ultimo'),
        'type'             => 'checkbox',
        'tooltip'          => '',
        'default'          => 1,
        'require'          => array('active_gateway[billwerk]' => true),
      ),

      'billwerk_payment_url' => array(
        'title'            => __('Payment URL', 'wp-ultimo'),
        'desc'             => __('Set the payment URL. The user will be redirected here after the signup.', 'wp-ultimo'),
        'tooltip'          => '',
        'type'             => 'text',
        'placeholder'      => '',
        'default'          => 'payment',
        'require'          => array('active_gateway[billwerk]' => true),
      ),

      'billwerk_use_thank_you_page' => array(
        'title'            => __('Should use Thank You Page?', 'wp-ultimo'),
        'desc'             => __('Should we redirect the user to a thank you page after the integration?', 'wp-ultimo'),
        'type'             => 'checkbox',
        'tooltip'          => '',
        'default'          => 1,
        'require'          => array(
          'active_gateway[billwerk]' => true,
        ),
      ),

      'billwerk_thankyou_url' => array(
        'title'            => __('Thank You URL', 'wp-ultimo'),
        'desc'             => __('Set the thank you URL. The user will be redirected here after the payment.', 'wp-ultimo'),
        'tooltip'          => '',
        'type'             => 'text',
        'placeholder'      => '',
        'default'          => 'thankyou',
        'require'          => array(
          'active_gateway[billwerk]'    => true,
          'billwerk_use_thank_you_page' => true,
        ),
      ),

      'billwerk_general_error_message' => array(
        'title'            => __('Billwerk General Error Message', 'wp-ultimo'),
        'desc'             => __('Enter a custom general error message that is displayed to the user when Billwerk returns non-standard errors.', 'wp-ultimo'),
        'tooltip'          => '',
        'type'             => 'textarea',
        'placeholder'      => '',
        'default'          => __('An error occurred while processing your payment. Please contact our support channels.', 'wu-billwerk'),
        'require'          => array('active_gateway[billwerk]' => true),
      ),

      'billwerk_webhook_url' => array(
        'title'            => __('Webhook URL', 'wp-ultimo'),
        'desc'             => sprintf(__('This is the URL you need to add to Billwerk so we can received Webhooks properly.', 'wu-billwerk') . '<br><code>%s</code>', $this->notification_url),
        'type'             => 'note',
        'require'          => array(
          'active_gateway[billwerk]' => true
        ),
      ),

      'billwerk_payment' => array(
        'title'         => __('Billwerk Payment Options', 'wp-ultimo'),
        'desc'          => __('You can limit the payment options using fields below.', 'wp-ultimo'),
        'type'          => 'heading',
        'require'          => array(
          'active_gateway[billwerk]' => true,
        ),
      ),

      'billwerk_payment_options' => array(
        'title'         => __('Active Payment Options', 'wp-ultimo'),
        'desc'          => '',
        'type'          => 'multi_checkbox',
        'options'       => $this->get_payment_options_from_billwerk(),
        'tooltip'       => '',
        'sortable'      => true,
        'require'          => array(
          'active_gateway[billwerk]' => true,
        ),
      ),

      'billwerk_advanced' => array(
        'title'         => __('Billwerk Advanced Options', 'wp-ultimo'),
        'desc'          => __('The options below are advanced, be careful.', 'wp-ultimo'),
        'type'          => 'heading',
        'require'          => array(
          'active_gateway[billwerk]' => true,
        ),
      ),

      'billwerk_use_cache' => array(
        'title'            => __('Enable API Calls Cache', 'wp-ultimo'),
        'desc'             => __('Check this box to cache API calls, use this in production.', 'wp-ultimo'),
        'type'             => 'checkbox',
        'tooltip'          => '',
        'default'          => 1,
        'require'          => array(
          'active_gateway[billwerk]' => true,
        ),
      ),

      'billwerk_cache_expire' => array(
        'title'            => __('Cache Expire Setting (seconds)', 'wp-ultimo'),
        'desc'             => __('How long should we keep the API cache calls, in seconds?', 'wp-ultimo'),
        'tooltip'          => '',
        'type'             => 'number',
        'placeholder'      => '3600',
        'default'          => '3600',
        'require'          => array(
          'active_gateway[billwerk]' => true,
          'billwerk_use_cache'       => true,
        ),
      ),

      'billwerk_other' => array(
        'title'         => __('Billwerk Other Options', 'wp-ultimo'),
        'desc'          => __('Other general options.', 'wp-ultimo'),
        'type'          => 'heading',
        'require'          => array(
          'active_gateway[billwerk]' => true,
        ),
      ),

      'billwerk_test_prefix' => array(
        'title'            => __('Test Site Prefix', 'wp-ultimo'),
        'desc'             => __('Whenever a new site is created starting with this prefix, it will be flag as a test site on Billwerk.', 'wp-ultimo'),
        'tooltip'          => '',
        'type'             => 'text',
        'placeholder'      => 'test20',
        'default'          => 'test20',
        'require'          => array(
          'active_gateway[billwerk]'    => true,
        ),
      ),

    );

  } // end settings;

} // end class WU_Gateway_Billwerk

/**
 * Register the gateway =D
 */
wu_register_gateway('billwerk', __('Billwerk', 'wp-ultimo'), __('This is the Billwerk Integration', 'wp-ultimo'), 'WU_Gateway_Billwerk');
