<?php
/**
 * Ajax Services
 *
 * Register all the ajax services we'll need on the UI,
 * and there will be a lot of them since we'll be calling Billwerk's API frequently.
 *
 * @category   WP Ultimo
 * @package    Billwerk
 * @author     Arindo Duque <arindo@wpultimo.com>
 * @since      0.0.1
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

if (!class_exists('WUB_Ajax_Service')) :

class WUB_Ajax_Service {

  /**
   * Makes sure we are only using one instance of the plugin
   *
   * @since 0.0.1
   * @var WUB_Ajax_Service
   */
  public static $instance;

  /**
   * Returns the instance of WUB_Ajax_Service
   *
   * @since 0.0.1
   * @return WUB_Ajax_Service A WUB_Ajax_Service instance
   */
  public static function get_instance() {

    if (null === self::$instance) self::$instance = new self();

    return self::$instance;

  } // end get_instance;

  /**
   * Register the Ajax service providers
   *
   * @since 0.0.1
   * @return void
   */
  public function __construct() {

    add_action('wp_ajax_wub_clear_api_cache', array($this, 'clear_api_cache'));

    add_action('wp_ajax_wub_sync_plans', array($this, 'sync_plans'));

    add_action('wp_ajax_wub_sync_contracts', array($this, 'sync_contracts'));

    add_action('wp_ajax_wub_get_order_summary', array($this, 'get_order_summary'));

    add_action('wp_ajax_nopriv_wub_check_coupon', array($this, 'check_coupon'));

    add_action('wp_ajax_wub_check_coupon', array($this, 'check_coupon'));

    add_action('wp_ajax_wub_query_contracts', array($this, 'query_contracts'));

    add_action('wp_ajax_wub_serve_invoice_pdf_download', array($this, 'serve_invoice_pdf_download'));

    add_action('wp_ajax_wub_sync_contract', array($this, 'sync_contract'));

    add_action('wp_ajax_wub_install_custom_fields', array($this, 'install_custom_fields'));

    add_action('wp_ajax_wub_install_webhook', array($this, 'install_webhook'));

    add_action('wp_ajax_wub_log_js_errors', array($this, 'log_js_errors'));

  } // end construct;

  /**
   * Log JS errors from the front-end
   *
   * @return void
   */
  public function log_js_errors() {

    $this->verify_nonce('log_js_errors');

    $log_message = sprintf('Error received from Billwerk: %s. User data: %s', json_encode($_POST, JSON_PRETTY_PRINT), json_encode(wp_get_current_user(), JSON_PRETTY_PRINT));

    WU_Logger::add('billwerk-js-errors', $log_message);

    return wp_send_json_success();

  } // end log_js_errors;

  /**
   * Makes sure only authorized scripts can run those actions
   *
   * @since 0.0.1
   * @param string $action
   * @return void
   */
  public function verify_nonce($action) {

    if (!wp_verify_nonce($_REQUEST['_wpnonce'], $action)) {

      return wp_send_json_error(array(
        'message' => __('You are not authorized to perform this action', 'wu-billwerk')
      ));

    } // end if;

  } // end verify_nonce;

  /**
   * Syncs the contract from billwerk with its counterpart from WP Ultimo
   *
   * @since 0.0.1
   */
  public function sync_contract(): void {

    $this->verify_nonce('sync_contract');

    $contract_id = $_REQUEST['contract_id'];

    if ($subscription = WUB_Sync()->sync_contract($contract_id)) {
      wp_send_json_success(array(
        'message' => __('Subscription successfully synced!', 'wu-billwerk'),
        'redirect' => network_admin_url("admin.php?page=wu-edit-subscription&updated=1&user_id=$subscription->user_id"),
      ));
    }

    wp_send_json_error([
      'message' => __('Subscription was not synced. See logs for details.', 'wu-billwerk')
    ]);

  } // end sync_contract;

  /**
   * Sync all the available contracts on the platform. Useful for testing.
   *
   * @since 1.0.0
   * @return string
   */
  public function sync_contracts() {

    global $wpdb;

    $this->verify_nonce('sync_contracts');

    $table_name = WU_Subscription::get_table_name();

    $subscriptions = $wpdb->get_results("SELECT integration_key FROM $table_name WHERE integration_key <> ''");

    // Statistic of sync
    $sync_result = ['success' => 0, 'fail' => 0];

    foreach ($subscriptions as $subscription) {
      // Increase time limit due to the number of sites
      \set_time_limit(5);

      if (WUB_Sync()->sync_contract($subscription->integration_key) !== false) {
          $sync_result['success']++;
      } else {
          $sync_result['fail']++;
      }

    } // end foreach;

    return wp_send_json_success(array(
      'message'  => sprintf(__('%d subscription(s) successfully synced, %d failure(s) occurred.', 'wu-billwerk'), $sync_result['success'], $sync_result['fail']),
      'redirect' => network_admin_url("admin.php?page=wp-ultimo-subscriptions"),
    ));

  } // end sync_contract;

  /**
   * Sends an API request to Billwerk to add Custom Fields to products on the settings there
   *
   * @since 0.0.1
   * @return string
   */
  public function install_custom_fields() {

    $this->verify_nonce('install_custom_fields');

    WP_Ultimo_Billwerk()->api->put("/CustomFieldSettings", json_decode('{"CustomFieldType":"Product","CustomFieldDescriptors":[{"Key":"featured","DisplayName":{"_c":"Featured"},"SelfServiceSignup":"None","SelfServiceUpdate":"None","EditInUI":"ReadWrite","IncludeInReports":false},{"Key":"last_synced","DisplayName":{"_c":"Last Synced"},"SelfServiceSignup":"None","SelfServiceUpdate":"None","EditInUI":"Read","IncludeInReports":false},{"Key":"order","DisplayName":{"_c":"Order (used for Up/Downgrades)"},"SelfServiceSignup":"None","SelfServiceUpdate":"None","EditInUI":"ReadWrite","IncludeInReports":false}]}'));

    WP_Ultimo_Billwerk()->api->put("/CustomFieldSettings", json_decode('{"CustomFieldType":"Customer","CustomFieldDescriptors":[{"Key":"is_test","DisplayName":{"_c":"Testaccount"},"SelfServiceSignup":"Write","SelfServiceUpdate":"None","EditInUI":"ReadWrite","IncludeInReports":true},{"Key":"shop_slug","DisplayName":{"_c":"Shop-Slug"},"SelfServiceSignup":"Write","SelfServiceUpdate":"None","EditInUI":"ReadWrite","IncludeInReports":true}]}'));

    return wp_send_json_success(array(
      'message'  => __('Custom Fields created successfully!', 'wu-billwerk'),
    ));

  } // end install_custom_fields;

  /**
   * Addds the current URL as a Webhook listener on Billwerk
   *
   * @since 0.0.1
   * @return string
   */
  public function install_webhook() {

    $this->verify_nonce('install_webhook');

    $response = WP_Ultimo_Billwerk()->api->post("/webhooks", array(
      'Events' => [
        "ContractCreated","ContractChanged","ContractCancelled","CustomerCreated","CustomerChanged","CustomerDeleted","DunningCreated","InvoiceCreated","InvoiceCorrected","OrderSucceeded","RecurringBillingApproaching","TrialEndApproaching","AccountCreated","CustomerLocked","CustomerUnlocked","DebitAuthCancelled","PaymentBearerExpired","PaymentBearerExpiring","PaymentDataChanged","PaymentEscalated","PaymentEscalationReset","PaymentFailed","PaymentProcessStatusChanged","PaymentRegistered","PaymentSucceeded"
      ],
      'Url' => wu_get_gateway('billwerk')->notification_url,
    ));

    if (isset($response->Id)) {

      return wp_send_json_success(array(
        'message'  => __('Webhook successfully registered!', 'wu-billwerk'),
      ));

    } // end if;

    return wp_send_json_error(array(
      'message'  => __('An enrror occured!', 'wu-billwerk'),
    ));

  } // end install_webhook;

  /**
   * Serve the URL for PDF download
   *
   * @since 0.0.1
   * @return string
   */
  public function serve_invoice_pdf_download() {

    $invoice_id = $_REQUEST['invoice'];

    $invoice_response = WP_Ultimo_Billwerk()->api->post("/Invoices/{$invoice_id}/downloadLink");

    if ($invoice_response->Url) {

      $url = WP_Ultimo_Billwerk()->api->environment_url('https://sandbox.billwerk.com/') . $invoice_response->Url;

      header("Location: $url");

      exit;

    } // end if;

  } // end serve_invoice_pdf_download;

  /**
   * Query contarcts on Billwerk based on a search term
   *
   * @since 0.0.1
   * @return string
   */
  public function query_contracts() {

    $this->verify_nonce('query_contracts');

    $term = $_REQUEST['term'];

    $results = WP_Ultimo_Billwerk()->api->no_cache()->get("/Contracts?search={$term}");

    if ($results === null || !is_array($results)) {

      return wp_send_json_error(array());

    } // end if;

    $formated_results = array_map(function($item) {

      return array(
        'text' => sprintf('%s (%s)', $item->CustomerName, $item->ReferenceCode),
        'slug' => '',
        'id'   => $item->Id,
      );

    }, $results);

    return wp_send_json_success($formated_results);

  } // end query_contracts;

  /**
   * Get an order summary via the API. We are no longer using this, but it can be useful
   *
   * @since 0.0.1
   * @return string
   */
  public function get_order_summary() {

    $this->verify_nonce('get_order_summary');

    $order_id = $_REQUEST['order_id'];

    $order_info = WP_Ultimo_Billwerk()->api->no_cache()->get("/Orders/{$order_id}");

    if (isset($order_info->Message)) {

      return wp_send_json_error(array(
        'message' => __('An error occured while trying to retrieve the order.', 'wu-billwerk')
      ), 500);

    } // end if;

    return wp_send_json_success($order_info);

  } // end get_order_summary;

  /**
   * Checks if a coupon is valid and returns its attributes
   *
   * @since 0.0.1
   * @return string
   */
  public function check_coupon() {

    $coupon = strtoupper($_REQUEST['coupon']);

    /** TODO: Discover why searching does not work */
    $remote_coupons = WUB_API()->get("/Coupons", array(
      // 'CouponCode' => $coupon,
      // 'Active'     => 1,
      // 'Enabled'    => 1,
    ));

    if (!$remote_coupons) {

      return wp_send_json_error([
        'message' => __('Invalid Coupon Code', 'wu-billwerk')
      ]);

    } // end if;

    foreach ($remote_coupons as $coupon_info) {

      if (isset($coupon_info->DiscountId) && $coupon_info->Active && $coupon_info->Enabled) {

        $coupon_info = WUB_API()->get("/Coupons/{$coupon_info->Id}");

        if (in_array($coupon, $coupon_info->Codes)) {

          $discount_info = WP_Ultimo_Billwerk()->api->get("/Discounts/$coupon_info->DiscountId");

          return wp_send_json_success(array(
            'message'   => __('Coupon Applied.', 'wu-billwerk'),
            'coupon'    => $discount_info,
            'coupon_id' => $coupon_info->Id,
          ));

        } // end if;

      } // end if;

    } // end if:

    return wp_send_json_error([
      'message' => __('Invalid Coupon Code', 'wu-billwerk')
    ]);

  } // end check_coupon;

  /**
   * Ajax Service that handles clearing the API object cache, if it is persistent
   *
   * @since 0.0.1
   * @return string
   */
  public function clear_api_cache() {

    $this->verify_nonce('clear_api_cache');

    wp_cache_flush();

    /**
     * Clear access token caches
     */
    delete_site_transient('billwerk_app_access_token');
    delete_site_transient('billwerk_sandbox_access_token');

    return wp_send_json_success(array(
      'message' => __('Caches successfully cleared!', 'wu-billwerk'),
    ));

  } // end clear_api_cache;

  /**
   * Ajax Service that handles Plan Syncing
   *
   * @since 0.0.1
   * @return string
   */
  public function sync_plans() {

    $this->verify_nonce('sync_plans');

    $plans = WUB_Sync()->sync_plans();

    return wp_send_json_success(array(
      'message'  => sprintf(__('%d Plans were successfully synced!', 'wu-billwerk'), count($plans)),
      'redirect' => network_admin_url('admin.php?page=wp-ultimo-plans'),
    ));

  } // end sync_plans;

} // end WUB_Ajax_Service;

// Activate Ajax Service
WUB_Ajax_Service::get_instance();

endif;
