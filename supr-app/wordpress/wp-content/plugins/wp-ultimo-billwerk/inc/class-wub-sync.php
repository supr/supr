<?php
/**
 * Sync Services
 *
 * Register all the ajax services we'll need on the UI,
 * and there will be a lot of them since we'll be calling Billwerk's API frequently.
 *
 * @category   WP Ultimo
 * @package    Billwerk
 * @author     Arindo Duque <arindo@wpultimo.com>
 * @since      1.0.0
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

if (!class_exists('WUB_Sync')) :

class WUB_Sync {

  /**
   * Makes sure we are only using one instance of the plugin
   *
   * @since 0.0.1
   * @var WUB_Sync
   */
  public static $instance;

  /**
   * Returns the instance of WUB_Sync
   *
   * @since 0.0.1
   * @return WUB_Sync A WUB_Sync instance
   */
  public static function get_instance() {

    if (null === self::$instance) self::$instance = new self();

    return self::$instance;

  } // end get_instance;

  /**
   * Return the difference in days, between two dates
   *
   * @since 0.0.1
   * @param string $date
   * @param string $date2
   * @return string
   */
  public function get_difference_in_days($date, $date2) {

    $now = new DateTime($date);
    $later = new DateTime($date2);

    return $later->diff($now)->format("%a");

  } // end get_difference_in_days;

  /**
   * Sync a given Contract by contract_id
   *
   * @since 0.0.1
   * @param string $contract_id Contract ID on Billwerk
   * @return WU_Subscription|false
   */
  public function sync_contract($contract_id) {

    WUB_Logger::add('wub-contract-sync', sprintf(__('Started syncing contract %s...', 'wp-ultimo'), $contract_id));

    $subscription = wub_get_subscription_by_contract_id($contract_id);

    $contract_info = WP_Ultimo_Billwerk()->api->no_cache()->get("/Contracts/{$contract_id}");

    if (!$subscription) {

      WUB_Logger::add('wub-contract-sync', sprintf(__('--- Failed to find local WP Ultimo subscription attached to billwerk contract id %s (subscription might not exist)...', 'wp-ultimo'), $contract_id));

      return false;

    } // end if;

    if (!$contract_info) {

      WUB_Logger::add('wub-contract-sync', sprintf(__('--- Failed to find remote billwerk contract with id %s... This might mean that the WP Ultimo subscription with id %s is without a contract attached to it.', 'wp-ultimo'), $contract_id, $subscription->ID));

      return false;

    } // end if;

    if (defined('WU_BILLWERK_API_LOGS') && WU_BILLWERK_API_LOGS) {

      $_contract_info = json_encode($contract_info, JSON_PRETTY_PRINT);

      WUB_Logger::add('wub-contract-sync', __('--- Contract Info Dump:', 'wp-ultimo'));
      WUB_Logger::add('wub-contract-sync', $_contract_info);

    } // end if;

    switch ($contract_info->LifecycleStatus) {
      case 'Active':
        $end_date = '3000-01-01 00:00:00';

        $subscription->active_until = $end_date;
        $subscription->trial = 0;

        WUB_Logger::add('wub-contract-sync', sprintf(__('--- Billwerk contract with id %s found. The contract status is Active, and for that reason, we will update the active_until value of the subscription id %s to 3000-01-01 00:00:00...', 'wp-ultimo'), $contract_id, $subscription->ID));

        break;
      case 'Ended':
        $end_date = date('Y-m-d H:i:s', strtotime($contract_info->EndDate));

        if ($end_date != $subscription->active_until) {

          $subscription->active_until = $end_date;

          WU_Mail()->send_template('subscription_canceled', $subscription->get_user_data('user_email'), array(
            'date' => date(get_option('date_format')),
            'gateway' => 'Billwerk',
            'new_active_until' => $subscription->get_date('active_until'),
            'user_name' => $subscription->get_user_data('display_name')
          ));

        } // end if;

        WUB_Logger::add('wub-contract-sync', sprintf(__('--- Billwerk contract with id %s found. The contract status is Ended, and for that reason, we will update the active_until value of the subscription id %s to %s, the end date of the contract...', 'wp-ultimo'), $contract_id, $subscription->ID, $end_date));

        $subscription->trial = 0;

        break;
      case 'InTrial':
        $end_date = '3000-01-01 00:00:00'; //date('Y-m-d H:i:s', strtotime( $contract_info->NextBillingDate ));

        $subscription->active_until = $end_date;
        $subscription->trial = $this->get_difference_in_days($contract_info->StartDate, $contract_info->NextBillingDate);

        WUB_Logger::add('wub-contract-sync', sprintf(__('--- Billwerk contract with id %s found. The contract status is InTrial, and for that reason, we will update the active_until value of the subscription id %s to 3000-01-01 00:00:00, and set the number of trial days to %s, in accordance with the contract...', 'wp-ultimo'), $contract_id, $subscription->ID, $subscription->trial));

        break;
    }

    $new_plan = $this->get_plan_id_from_contract_phases($contract_info->Phases);

    if ($new_plan) {

      if ($new_plan->id != $subscription->plan_id) {

        WU_Mail()->send_template('plan_changed', $subscription->get_user_data('user_email'), array(
          'date'               => date(get_option('date_format')),
          'gateway'            => 'Billwerk',
          'billing_start_date' => $subscription->get_billing_start(get_option('date_format'), false),
          'user_name'          => $subscription->get_user_data('display_name'),
          'account_link'       => get_admin_url( get_user_meta($subscription->user_id, 'primary_blog', true) , 'admin.php?page=wu-my-account'),
          'new_plan_name'      => $new_plan->title,
        ));

        $old_plan = $subscription->get_plan();

        $old_plan_name = $old_plan ? $old_plan->title : __('No Plan', 'wp-ultimo');

        WUB_Logger::add('wub-contract-sync', sprintf(__('--- There was a change in plans on the Billwerk Contract. Updating the local subscription to reflect that. Plan changed from %s to %s.', 'wp-ultimo'), $old_plan_name, $new_plan->title));

      } // end if;

      $subscription->plan_id = $new_plan->id;

    } // end if;

    $saved = $subscription->save();

    $last_log_message = $saved
      ? sprintf(__('--- Subscription id %s saving was successful! Syncing contract id %s finished successfully.', 'wp-ultimo'), $subscription->ID, $contract_id)
      : sprintf(__('--- Subscription id %s saving failed! Syncing contract id %s failed.', 'wp-ultimo'), $subscription->ID, $contract_id);

    WUB_Logger::add('wub-contract-sync', $last_log_message);

    return $saved;

  } // end sync_contract;

  /**
   * Get the Plan ID (billwerk ID) from a contract Phase
   *
   * @since 0.0.1
   * @param array $contract_phases
   * @return WU_Plan
   */
  public function get_plan_id_from_contract_phases($contract_phases) {

    $plan = false;

    foreach ($contract_phases as $contract_phase) {

      if ($contract_phase->Type == 'Normal' && isset($contract_phase->PlanId)) {

        $plan = wub_get_local_plan_from_billwerk_plan_id($contract_phase->PlanId);

      } // end if;

    } // end if;

    return $plan;

  } // end get_plan_variant_id_from_contract_phases;

  /**
   * Get the plans from Billwerk and sync the data on this local instance
   *
   * @since 0.0.1
   * @return array
   */
  public function sync_plans() {

    /**
     * Get the plans from the API
     */
    $billwerk_plans = WP_Ultimo_Billwerk()->api->no_cache()->get('/plans', [
      'take' => 100
    ]);

    if ($billwerk_plans && is_array($billwerk_plans)) {

      /**
       * Create or update local plans
       */
      return array_map(array($this, 'create_or_update_billwerk_plan'), $billwerk_plans);

    } // end if;

    return array();

  } // end sync_plans;

  /**
   * Asks API about further information about a plan and creates it on WP Ultimo
   *
   * @since 1.0.0
   * @param object $billwerk_plan
   * @return integer
   */
  public function create_or_update_billwerk_plan($billwerk_plan) {

    $variants = WP_Ultimo_Billwerk()->api->no_cache()->get("/PlanVariants", [
      'planId' => $billwerk_plan->Id,
    ]);

    $variant_ids = array();

    /**
     * Know we define if the sync has happen before by the plans already synced
     * So we no longer depend on keeping this on billwerk
     * Fixes https://github.com/aanduque/wp-ultimo-billwerk/issues/10
     * @since 1.0.0
     */
    $existing_plan = wub_get_local_plan_from_billwerk_plan_id($billwerk_plan->Id);

    $plan = $existing_plan ?: new WU_Plan();

    $plan->title = $billwerk_plan->Name->_c;

    if (property_exists($billwerk_plan->PlanDescription, '_c')) {

      $plan->description = $billwerk_plan->PlanDescription->_c;

    } // end if;

    $plan->hidden      = $billwerk_plan->Hidden;
    $plan->top_deal    = $billwerk_plan->CustomFields->featured == 1;
    $plan->order       = (int) $billwerk_plan->CustomFields->order;

    foreach ($variants as $plan_info) {

      // var_dump($plan_info->RecurringFee);

      /**
       * Handles free plans
       * @since 1.0.0
       */
      if (!isset($plan_info->RecurringFee) || $plan_info->RecurringFee == 0) {

        $variant_ids[1]   = $plan_info->Id;
        $variant_ids[3]   = $plan_info->Id;
        $variant_ids[12]  = $plan_info->Id;

        $plan->price_1  = 0;
        $plan->price_3  = 0;
        $plan->price_12 = 0;
        $plan->free     = true;

        continue;

      }

      if ($plan_info->FeePeriod->Unit == 'Month' && $plan_info->FeePeriod->Quantity == 1) {

        $variant_ids[1]  = $plan_info->Id;
        $plan->price_1   = $plan_info->RecurringFee;
        $plan->setup_fee = $plan_info->SetupFee;
        $plan->free      = false;

      }

      else if ($plan_info->FeePeriod->Unit == 'Month' && $plan_info->FeePeriod->Quantity == 3) {

        $variant_ids[3]  = $plan_info->Id;
        $plan->price_3   = $plan_info->RecurringFee;
        $plan->setup_fee = $plan_info->SetupFee;
        $plan->free      = false;

      }

      else if ($plan_info->FeePeriod->Unit == 'Year') {

        $variant_ids[12] = $plan_info->Id;
        $plan->price_12  = $plan_info->RecurringFee;
        $plan->setup_fee = $plan_info->SetupFee;
        $plan->free      = false;

      } // end if;

    } // end foreach;

    /**
     * Override default prices to avoid saving errors
     */
    if (!$plan->free) {
      $plan->price_1  = $plan->price_1  ? $plan->price_1  : 1;
      $plan->price_3  = $plan->price_3  ? $plan->price_3  : 1;
      $plan->price_12 = $plan->price_12 ? $plan->price_12 : 1;
    } // end if;

    $quotas = $plan->quotas;

    // Don't rewrite if already exists
    if (!isset($quotas['upload'])) {
        $quotas['upload'] = 100;
    }

    $plan->quotas = $quotas;

    $plan->save();

    update_post_meta($plan->id, 'wpu_billwerk_plan_id', $billwerk_plan->Id);
    update_post_meta($plan->id, 'wpu_billwerk_variant_ids', $variant_ids);

    return $plan->id;

  } // end create_or_update_billwerk_plan;

} // end WUB_Sync;

/**
 * Returns the active instance of the plugin
 *
 * @since 0.0.1
 * @return WUB_Sync
 */
function WUB_Sync() {

  return WUB_Sync::get_instance();

} // end WUB_Sync;

endif;