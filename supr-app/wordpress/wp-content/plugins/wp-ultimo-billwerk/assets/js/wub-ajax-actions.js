(function($) {
  $(document).ready(function() {

    $('a.wub-ajax, .wub-ajax a').on('click', function(e) {

      e.preventDefault();

      var url = $(this).attr('href');

      wuswal({
        type: "warning",
        title: "Are you sure?",
        text: "Click OK to continue with this action.",
        showCancelButton: true,
        showConfirmButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      }, function() {
        $.ajax({
          url: url,
        }).done(function (results) {
          wuswal({
            type: results.success ? "success" : "error",
            title: results.success ? "Success" : "Error",
            text: results.data.message,
            showCancelButton: false,
            showConfirmButton: true
          }, function() {
            if (results.data.redirect) {
              window.location.href = results.data.redirect;
            }
          });
        });
      });

    });

  });
})(jQuery);