/**
 * Subscription Changes 
 */
(function ($) {

  $(document).ready(function () {
    $('.wu-subscription-details').off('click');
    $(".wu-subscription-details .wu-tooltip").off();
  });

  // Select: Contract
  $('#wub-contract').select2({
    width: '100%',
    minimumInputLength: 3,
    // tags: [],
    ajax: {
      url: ajaxurl,
      dataType: 'json',
      type: "GET",
      quietMillis: 50,
      data: function (term) {
        return {
          term: term,
          action: 'wub_query_contracts',
          _wpnonce: $('[name=_wpnonce]').val(),
          _wp_http_referer: $('[name=_wp_http_referer]').val(),
        };
      },
      results: function (results) {
        return {
          results: results.data
        };
      }
    }
  });

})(jQuery);