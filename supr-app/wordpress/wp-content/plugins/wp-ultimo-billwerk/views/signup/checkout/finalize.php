<?php
/**
 * The Template for displaying the signup flow for the end user
 *
 * This template can be overridden by copying it to yourtheme/wp-ultimo/signup/signup-header.php.
 *
 * HOWEVER, on occasion WP Ultimo will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @author      NextPress
 * @package     WP_Ultimo/Views
 * @version     1.4.0
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly
}

$finalize_url = home_url('/process-payment');

wp_enqueue_script('jquery-blockui', WP_Ultimo()->url('/inc/setup/js/jquery.blockUI.js'), array('jquery'), $this->version);

wp_enqueue_script('wub-billwerk-js', WUB_API()->environment_url('https://selfservice.sandbox.billwerk.com/subscription.js', ''), array('jquery'), $scripts_version);

/**
 * Enqueue some much needed styles
 */

$suffix = WP_Ultimo()->min;

wp_enqueue_style('login');

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>
      <?php echo apply_filters('wu_signup_page_title', sprintf(__('%s - Signup', 'wp-ultimo'), get_bloginfo('Name'), get_bloginfo('Name'))); ?>
    </title>

    <?php do_action('admin_head'); ?>

    <?php // Signup do action, like the default ?>
    <?php do_action('signup_header'); ?>
    <?php do_action('login_enqueue_scripts'); ?>
    <?php do_action('admin_print_scripts'); ?>

  </head>

  <body class="">

      <?php 
      /**
       * We also need to print the footer admin scripts, to make sure we are enqueing some of the scripts dependencies
       * our scripts need in order to function properly
       */
      do_action('admin_print_footer_scripts'); ?>

    </div>

    <script>

      jQuery("body").block({
        message: null,
        message: '<?php echo esc_js(__('Processing payment...', 'wu-billwerk')); ?>',
        overlayCSS: {
          background: '#F1F1F1',
          opacity: 0.6,
        },
        css: {
          padding: '30px',
          background: 'transparent',
          border: 'none',
          color: '#444',
          top: '50%',
          fontSize: '14px',
          lineHeight: '1.1em',
        },
      });

      SubscriptionJS.finalize(function(result) {
        window.location.href = '<?php echo esc_js($finalize_url); ?>' + "?" + jQuery.param(result);
        // window.href
      },function(val) {
        window.location.href = '<?php echo esc_js($finalize_url); ?>' + "?" + jQuery.param(result);
      });
    </script>

  </body>

</html>