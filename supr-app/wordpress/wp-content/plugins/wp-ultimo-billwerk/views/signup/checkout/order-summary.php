<div id="wub-order-summary">
  
  <div class="loading" v-if="loading">
    <?php _e('Loading Order Summary...', 'wu-billwerk'); ?>
  </div>

  <div class="loading loading-error" v-if="message" v-cloak>
    {{message}}
  </div>
  
  <div v-if="order && !loading" v-cloak>

    <div class="order-description" v-if="order.IsTrial">
      <?php printf(__('This product includes a trial phase of %s %s(s). You can cancel your subscription anytime within the trial. If not cancelled the following fees apply after the trial phase.'), '{{order.TrialPeriod.Quantity}}', '{{order.TrialPeriod.Unit}}'); ?>
    </div>

    <table id="order-summary-table">
      <thead>
        <tr>
          <th class="col-description"><?php _e('Description', 'wu-billwerk'); ?></th>
          <!-- <th class="col-unit-price"><?php _e('Unit Price', 'wu-billwerk'); ?></th> -->
          <!-- <th class="col-quantity"><?php _e('Quantity', 'wu-billwerk'); ?></th> -->
          <th class="col-total-net"><?php _e('Net Total', 'wu-billwerk'); ?></th>
          <th class="col-total-vat-percentage"><?php _e('VAT (%)', 'wu-billwerk'); ?></th>
          <th class="col-total-vat"><?php _e('VAT', 'wu-billwerk'); ?></th>
          <th class="col-total-gross"><?php _e('Gross Total', 'wu-billwerk'); ?></th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="line in order.LineItems">
          <td class="col-description">
            {{line.Description}}<br>
            <!-- <small>{{line.ProductDescription ? line.ProductDescription : '--'}}</small> -->
          </td>
          <!-- <td class="col-unit-price">{{accounting.formatMoney(line.PricePerUnit)}}</td> -->
          <!-- <td class="col-quantity">{{line.Quantity}}</td> -->
          <td class="col-total-net">{{accounting.formatMoney(line.TotalNet)}} {{line.FeePeriod ? `/ ${line.FeePeriod.Unit.toLowerCase()}` : ''}}</td>
          <td class="col-total-vat-percentage">{{line.VatPercentage}}%</td>
          <td class="col-total-vat">{{accounting.formatMoney(line.TotalVat)}}</td>
          <td class="col-total-gross">{{accounting.formatMoney(line.TotalGross)}}</td>
        </tr>
        <tr v-for="line in order.CouponLines">
          <td colspan="4"><?php _e('Coupon Code', 'wu-billwerk'); ?></td>
          <td>{{line.CouponCode}}</td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="4"><?php _e('Grand Total', 'wu-billwerk'); ?> <span v-if="order.IsTrial">(on {{moment( order.NextTotalGrossDate ).format("MMMM Do YYYY") }})</span></td>
          <td>{{accounting.formatMoney(order.TotalGross)}}</td>
        </tr>
      </tfoot>
    </table>

    <div class="order-description">
      <ul>
        <li><?php _e('Recurring charges may apply', 'wu-billwerk'); ?></li>
        <li v-if="!order.IsTrial"><?php printf(__('Next fee of %s will be billed on %s', 'wu-billwerk'), '{{accounting.formatMoney( order.NextTotalGross )}}', '{{ moment( order.NextTotalGrossDate ).format("MMMM Do YYYY, h:mm:ss a") }}'); ?></li>
        <li><?php _e('Fees and periods can vary', 'wu-billwerk'); ?></li>
      </ul>
    </div>
  </div>
</div>