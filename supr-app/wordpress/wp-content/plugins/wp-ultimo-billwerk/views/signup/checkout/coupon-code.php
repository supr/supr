<div id="coupon-code-app">
  
    <div style="text-align: left; width: 275px; margin: 0 auto 10px auto;" v-if="message" v-bind:class="'notice notice-' + (success ? 'success' : 'error')">
      <p>{{message}}</p>
    </div>

  <input name="coupon" v-model="coupon" placeholder="<?php _e('Have a Coupon Code?', 'wu-billwerk'); ?>">
  <button v-on:click="checkCoupon" class="button button-primary"><?php _e('Apply Coupon', 'wu-billwerk'); ?></button>

  <input id="coupon_id" type="hidden" name="coupon_id" v-model="coupon_id">
</div>

<script>
  (function($) {
    
    var coupon_app = new Vue({
      el: "#coupon-code-app",
      data: {
        coupon_id: '',
        coupon: <?php echo json_encode(isset($_GET['coupon']) ? $_GET['coupon'] : ''); ?>,
        message : false,
        success: false,
      },
      mounted: function() {
        if (this.coupon) {
          this.checkCoupon(null);
        }
      },
      methods: {
        checkCoupon: function($event) {
          
          if ($event) {
            $event.preventDefault();
          }

          $(".login").block({
            message: null,
            overlayCSS: {
              background: "#F1F1F1",
              opacity: 0.6
            }
          });
          
          var app = this;

          var ajaxurl = '/wp-admin/admin-ajax.php';

          $.ajax({
            method: 'GET',
            url: `${ajaxurl}?action=wub_check_coupon&coupon=${app.coupon}`,
            success: function(results) {

              coupon_app.message = results.data.message;
              coupon_app.success = results.success;

              $(".login").unblock();

              var current_frequency = $('#wu_plan_freq').val();

              if (results.success) {

                coupon_app.coupon_id = app.coupon;

                $('#signupform').append($('#coupon_id'));

                $('.wu-plan').each(function() {

                  if (!$(this).find('.old-price').get(0)) {

                    $(this).find('h4').after('<div class="old-price">--</div>');

                  } // end if;

                });

                Object.keys(results.data.coupon.Targets).map(function(value) {

                  $('[data-variant-id-'+ current_frequency +'="'+ value +'"]').each(function() {

                    if (!$(this).find('.old-price').get(0)) {

                      $(this).find('h4').after('<div class="old-price">--</div>');

                    } // end if;

                    var old_price = $(this).data('price-' + current_frequency),
                        new_price = old_price * ((100 - results.data.coupon.Effect.ReductionPercent) / 100);

                    $(this).find('.old-price').html(accounting.formatMoney( parseFloat(old_price, 10) ));
                    
                    $(this).find('.plan-price').html( accounting.formatNumber(new_price, 2) );

                  });

                }); // end map

              } else {

                $('.old-price').hide();

                coupon_app.coupon_id = '';

                $('.wu-plan').each(function() {

                  var price = $(this).data('price-' + current_frequency);

                  $(this).find('.plan-price').html( price );

                });

              }
            }
          });

        }
      }
    });

  })(jQuery);
</script>

<?php wp_enqueue_style('wub-checkout', WP_Ultimo_Billwerk()->get_asset('checkout.min.css', 'css'), false); ?>