<style>
li.billwerk_tab a::before {
  content: "\f533" !important;
}
</style>
<?php
// var_dump($plan->billwerk_id);
?>
<div id="wu_billwerk" class="panel wu_options_panel">

  <div class="options_group">
    <p class="form-field">
      <label class="form-field-full">
        <?php _e('Use the select box below to set a default site template for this plan. Be aware that if you allow template selection on the sign-up this settings will be overwritten by the user selection.', 'wp-ultimo'); ?>
      </label>
    </p>
  </div>

  <div class="options_group">

    <p class="form-field">
      <label class="form-field">
        <?php _e('Role', 'wp-ultimo'); ?>
      </label>
      <select class="checkbox" name="role">
        
        <option value=""><?php _e('Use default role selected on WP Ultimo Settings', 'wp-ultimo'); ?></option>

        <?php
        /**
         * Get the roles
         */
        $roles = WU_Settings::get_roles();

        foreach($roles as $value => $label) : 

        ?>

          <option <?php selected($plan->role == $value); ?> value="<?php echo $value; ?>"><?php echo $label ?></option>

        <?php endforeach; ?>

      </select>
    </p>
    <!-- <p class="form-field">
      <label class="form-field-full">
        <?php _e('Note: Changing the role plan will change the roles for every user on this plan on their sites. Users using different roles (users you customized) will not be affected.', 'wp-ultimo'); ?>
      </label>
    </p> -->
  </div>

  <div class="options_group">
    <p class="form-field">
      <label class="form-field">
        <?php _e('Plan Variant', 'wu-billwerk'); ?>
      </label>
      <span class="form-field">
        <strong style="text-transform: uppercase;"><?php echo $plan->billwerk_variant_title; ?></strong> (<?php echo $plan->billwerk_variant_id; ?>)
      </span>
    </p>
  </div>

  <!-- <div class="options_group">

    <p class="form-field">

      <label class="form-field">
        <?php _e('Plan Variant Information', 'wu-billwerk'); ?>
      </label>

      <span class="">
        
        <a href="<?php echo wp_nonce_url(admin_url('admin-ajax.php?action=wub_clear_api_cache'), 'clear_api_cache'); ?>" class="button wub-ajax"><?php _e('Load Plan Variant Information', 'wu-ultimo'); ?></a>

      </span>
    </p>

  </div> -->

</div>