<!-- <?php //var_dump($contract); ?> -->
<p>
  <?php _e('This subscription is automaticalled synced everytime Billwerk sends us a webhook call informing a contract change. If for some reason that does not work, use the button below to re-sync it.', 'wu-billwerk'); ?>
</p>

<label style="margin-bottom: 5px; text-transform: uppercase; display: block; font-size: 11px;" for="wub-contract">
  <strong><?php _e('Current Contract', 'wp-ultimo'); ?></strong>
</label>

<ul>
  <li><strong><?php _e('Customer Name', 'wu-billwerk'); ?></strong>: <?php echo $contract ? $contract->CustomerName : __('No customer', 'wu-billwerk'); ?></li>
  <li><strong><?php _e('Reference Code', 'wu-billwerk'); ?></strong>: <?php echo $contract ? $contract->ReferenceCode : __('No reference code', 'wu-billwerk');; ?></li>
</ul>

<?php if ($contract) : ?>
  <a href="<?php echo wp_nonce_url(admin_url("admin-ajax.php?action=wub_sync_contract&contract_id=$contract->Id"), 'sync_contract'); ?>" class="button button-primary button-streched wub-ajax">
    <?php _e('Re-sync Subscription', 'wu-billwerk'); ?>
  </a>
<?php else: ?>
  <a href="#" class="button button-streched" disabled="disabled">
    <?php _e('Re-sync Subscription', 'wu-billwerk'); ?>
  </a>
<?php endif; ?>

<hr style="margin-top: 20px;">

<p>
  <?php _e('Use this selector to change the contract on billwerk associated with this subscription.', 'wu-billwerk'); ?>
</p>

<div class="input-text-wrap">

  <label style="margin-bottom: 5px; text-transform: uppercase; display: block; font-size: 11px;" for="wub-contract">
    <strong><?php _e('Change Contract', 'wp-ultimo'); ?></strong>
  </label>

  <input type="text" name="wub-contract" id="wub-contract" class="regular-text" placeholder="<?php _e('Select a contract', 'wu-billwerk'); ?>">

  <?php wp_nonce_field('query_contracts'); ?>

  <br><br>

  <button type="submit" class="button button-streched" name="wub-change-contract">
    <?php _e('Change Contract', 'wu-billwerk'); ?>
  </button>

</div>