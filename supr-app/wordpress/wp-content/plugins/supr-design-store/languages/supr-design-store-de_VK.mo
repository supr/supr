��    D      <      \      \     ]     a     i  .   r  0   �     �  
   �     �  
   �     �     	            &   %     L     d     v     �     �     �     �     �     �     �     �                    '     4     :     A     H  W   K  R   �  F   �     =     X     i     n     {     �     �     �     �     �     �     �     �     �               $     7     F  
   O  7   Z  9   �     �     �     �     �     	     	     %	     1	     B	  �   K	     �	     �	  
   
  ;   
  E   N
     �
     �
  	   �
  
   �
     �
     �
     �
     �
  +   �
          2     I     a     x     �     �     �     �     �     �     �     �            	   &  	   0  	   :     D  b   G  b   �  H        V     v     �     �     �     �     �     �     �     �     �     �          )     ?     R     e     x     �  
   �  6   �  7   �               1     D     a     m     u     �     �    -  Actions Activate An error occurred while activating the design. An error occurred while activating the template. Blog archive Blog post  Cancel Categories Color scheme Color schemes Colors Config Create gallery of designs und elements Create new color scheme Create new design Create new element Create new font scheme Create new template Description Design store Designs Edit color scheme Edit design Edit font scheme Edit template Elements Font scheme Font schemes Fonts Footer Header ID If you activate the selected [object_name], your previous settings will be overwritten. If you activate the selected template, your previous settings will be overwritten. In multisite mode, the plugin can only be configured in the main blog. Load settings from blog id Maintenance mode Name New category Pages Position Preview Processing... Product catalog Product page SUPR - Design store SUPR Development Save changes Select categories Select color scheme Select element Select font scheme Select preview Settings Start page The design has been successfully applied to your store. The template has been successfully applied to your store. Type Use [object_name]? Use template? We are applying the element... color scheme design font scheme https://supr.com template MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Project-Id-Version: Viking Shop-design-store
Language: de
  -  Aktionen Aktivieren Bei der Aktivierung des Designs ist ein Fehler aufgetreten. Bei dem Versuch die Vorlage zu aktivieren ist ein Fehler aufgetreten. Blog-Archiv Blogbeitrag Abbrechen Kategorien Farbkonzept Farbkonzept Farben Konfigurieren Galerie von Designs und Elementen erstellen Neues Element erstellen Neues Design erstellen Neues Element erstellen Neue Vorlage erstellen Neue Vorlage erstellen Beschreibung Designladen Designs Farbkonzept bearbeiten Design bearbeiten Vorlage bearbeiten Vorlage bearbeiten Elemente Schriftkonzept Schriftkonzept Schriften Fusszeile Kopfzeile ID Wenn Du die ausgewählte Vorlage aktivierst, werden deine bisherigen Einstellungen überschrieben. Wenn Du die ausgewählte Vorlage aktivierst, werden Deine bisherigen Einstellungen überschrieben. Im Multisite-Modus kann das Plugin nur im Hauptblog konfiguriert werden. Einstellungen von Blog-ID laden Wartungsmodus Name Neue Kategorie Seiten Position Vorschau Wird verarbeitet... Produktkatalog Produktseite Viking Shop - Designladen Viking Shop Entwicklung Änderungen speichern Kategorien auswählen Element auswählen Element auswählen Element auswählen Vorschau auswählen Einstellungen Startseite Das Design wurde erfolgreich in deinem Shop verwendet. Die Vorlage wurde erfolgreich in Deinem Shop verwendet. Typ [object_name] verwenden? Vorlage verwenden? Wir wenden das Element an... Farbkonzept Designs Schriftkonzept https://Viking Shop.com Vorlage verwenden? 