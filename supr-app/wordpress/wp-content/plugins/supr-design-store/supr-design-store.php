<?php
/**
 * Plugin Name: SUPR - Design store
 * Plugin URI: https://supr.com
 * Description: Create gallery of designs und elements
 * Text Domain: supr-design-store
 * Domain Path: /languages
 * Version: 1.0.0
 * Author: SUPR Development
 * License: GPL
 */

define('SUPR_DESIGN_STORE_PATH', plugin_dir_path(__FILE__));
define('SUPR_DESIGN_STORE_URL', plugins_url('/', __FILE__));
define('SUPR_DESIGN_STORE_FILE', plugin_basename(__FILE__));
define('SUPR_DESIGN_DIR_NAME', basename(__DIR__));

define('SUPR_DESIGN_STORE_VERSION', '1.2.0');

// Load main Class
require(SUPR_DESIGN_STORE_PATH . 'includes/Plugin.php');

// After activation of plugin
register_activation_hook(__FILE__, [\SuprDesignStore\Plugin::class, 'activationHook']);
