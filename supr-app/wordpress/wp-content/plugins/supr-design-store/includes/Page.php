<?php

namespace SuprDesignStore;

/**
 * Class Page
 *
 * @package SuprDesignStore
 */
class Page
{
    /**
     * Show elements page
     */
    public static function elementsPage(): void
    {
        include SUPR_DESIGN_STORE_PATH . 'templates/elements-page.php';
    }

    /**
     * Show designs page
     */
    public static function designsPage(): void
    {
        include SUPR_DESIGN_STORE_PATH . 'templates/designs-page.php';
    }

    /**
     * Show colors page
     */
    public static function colorsPage(): void
    {
        include SUPR_DESIGN_STORE_PATH . 'templates/colors-page.php';
    }

    /**
     * Show fonts page
     */
    public static function fontsPage(): void
    {
        include SUPR_DESIGN_STORE_PATH . 'templates/fonts-page.php';
    }

    /**
     * Show setting page
     */
    public static function settingsPage(): void
    {
        include SUPR_DESIGN_STORE_PATH . 'templates/settings-page.php';
    }
}
