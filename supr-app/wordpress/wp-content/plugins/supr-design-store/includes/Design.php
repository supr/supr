<?php

namespace SuprDesignStore;

/**
 * Class Design
 *
 * @package SuprDesignStore
 */
class Design
{
    private $id;

    private $name;

    private $description;

    private $categories;

    private $preview;

    private $previewId;

    private $colorSchemeId;

    private $fontSchemeId;

    private $position;

    private $publicStatus = true;

    private $timestamp;

    /** @var DesignElement[] */
    private $elements = [];

    public function __construct($properties = null)
    {
        if ($properties !== null) {
            $this->unserialize($properties);
        }
    }

    /**
     * @param array $properties
     */
    public function unserialize($properties): void
    {
        if (isset($properties['id'])) {
            $this->setId($properties['id']);
        }
        if (isset($properties['name'])) {
            $this->setName($properties['name']);
        }
        if (isset($properties['description'])) {
            $this->setDescription($properties['description']);
        }
        if (isset($properties['categories'])) {
            $this->setCategories($properties['categories']);
        }
        if (isset($properties['preview'])) {
            $this->setPreview($properties['preview']);
        }
        if (isset($properties['preview_id'])) {
            $this->setPreviewId($properties['preview_id']);
        }
        if (isset($properties['color_scheme_id'])) {
            $this->setColorSchemeId($properties['color_scheme_id']);
        }
        if (isset($properties['font_scheme_id'])) {
            $this->setFontSchemeId($properties['font_scheme_id']);
        }
        if (isset($properties['position'])) {
            $this->setPosition($properties['position']);
        }
        if (isset($properties['timestamp'])) {
            $this->setTimestamp($properties['timestamp']);
        }

        $this->setPublicStatus($properties['public_status'] ?? true);

        if (isset($properties['elements']) && \is_array($properties['elements']) && \count($properties['elements']) > 0) {
            $this->clearElements();
            foreach ((array) $properties['elements'] as $element) {
                $this->addElement($element);
            }
        }
    }

    /**
     * Remove all of elements from design
     */
    public function clearElements(): void
    {
        $this->elements = [];
    }

    /**
     * @param $element
     */
    public function addElement($element): void
    {
        if ($element instanceof DesignElement) {
            $this->elements[] = $element;
        } elseif (\is_array($element) && isset($element['id'])) {
            $designElement = new DesignElement();
            $designElement->unserialize($element);
            $this->elements[] = $designElement;
        } elseif ((int)$element > 0) {
            $designManager = new DesignElementManager();
            $this->elements[] = $designManager->getElementById($element);
        }
    }

    /**
     * @return \SuprDesignStore\DesignElement[]
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        // Get all elements
        $elements = [];
        foreach ($this->getElements() as $element) {
            $elements[] = $element->serialize();
        }

        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'categories' => $this->getCategories(),
            'preview' => $this->getPreview(),
            'preview_id' => $this->getPreviewId(),
            'color_scheme_id' => $this->getColorSchemeId(),
            'font_scheme_id' => $this->getFontSchemeId(),
            'position' => $this->getPosition(),
            'timestamp' => $this->getTimestamp(),
            'public_status' => $this->getPublicStatus(),
            'elements' => $elements
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return \is_array($this->categories) ? $this->categories : explode(';', $this->categories);
    }

    /**
     * @param array|string $categories
     */
    public function setCategories($categories): void
    {
        if (!\is_array($categories)) {
            $categories = explode(';', $categories);
        }

        $categories = array_filter($categories);
        asort($categories);

        $this->categories = implode(';', $categories);
    }

    /**
     * @return mixed
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @param $preview
     */
    public function setPreview($preview): void
    {
        $this->preview = $preview;
    }

    /**
     * @return mixed
     */
    public function getPreviewId()
    {
        return $this->previewId;
    }

    /**
     * @param $previewId
     */
    public function setPreviewId($previewId): void
    {
        $this->previewId = $previewId;
    }

    /**
     * @return mixed
     */
    public function getColorSchemeId()
    {
        return $this->colorSchemeId;
    }

    /**
     * @param $colorSchemeId
     */
    public function setColorSchemeId($colorSchemeId): void
    {
        $this->colorSchemeId = $colorSchemeId;
    }

    /**
     * @return mixed
     */
    public function getFontSchemeId()
    {
        return $this->fontSchemeId;
    }

    /**
     * @param $fontSchemeId
     */
    public function setFontSchemeId($fontSchemeId): void
    {
        $this->fontSchemeId = $fontSchemeId;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param $position
     */
    public function setPosition($position): void
    {
        $this->position = $position;
    }

    /**
     * @return bool
     */
    public function getPublicStatus(): bool
    {
        return $this->publicStatus;
    }

    /**
     * @param $publicStatus
     */
    public function setPublicStatus($publicStatus): void
    {
        $this->publicStatus = (bool)$publicStatus;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param $timestamp
     */
    public function setTimestamp($timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @param $elementId
     * @return bool
     */
    public function hasElement($elementId): bool
    {
        foreach ($this->elements as $element) {
            if ($element->getId() === $elementId) {
                return true;
            }
        }

        return false;
    }
}
