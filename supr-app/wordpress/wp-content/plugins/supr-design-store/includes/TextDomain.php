<?php

namespace SuprDesignStore;

class TextDomain
{
    public static $domainName = 'supr-design-store';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_DESIGN_DIR_NAME . '/languages/');
    }
}
