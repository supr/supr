<?php

namespace SuprDesignStore;

/**
 * Class DesignElement
 *
 * @package SuprDesignStore
 */
class DesignElement
{
    private $id;

    private $name;

    private $description;

    private $type;

    private $preview;

    private $previewId;

    private $config;

    private $position;

    private $publicStatus = true;

    private $timestamp;

    /**
     * DesignElement constructor.
     *
     * @param null $properties
     */
    public function __construct($properties = null)
    {
        if ($properties !== null) {
            $this->unserialize($properties);
        }
    }

    /**
     * @param $properties
     */
    public function unserialize($properties): void
    {
        if (isset($properties['id'])) {
            $this->setId($properties['id']);
        }
        if (isset($properties['name'])) {
            $this->setName($properties['name']);
        }
        if (isset($properties['description'])) {
            $this->setDescription($properties['description']);
        }
        if (isset($properties['type'])) {
            $this->setType($properties['type']);
        }
        if (isset($properties['preview'])) {
            $this->setPreview($properties['preview']);
        }
        if (isset($properties['preview_id'])) {
            $this->setPreviewId($properties['preview_id']);
        }
        if (isset($properties['config'])) {
            $this->setConfig($properties['config']);
        }
        if (isset($properties['position'])) {
            $this->setPosition($properties['position']);
        }
        if (isset($properties['timestamp'])) {
            $this->setTimestamp($properties['timestamp']);
        }

        $this->setPublicStatus($properties['public_status'] ?? true);
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'type' => $this->getType(),
            'preview' => $this->getPreview(),
            'preview_id' => $this->getPreviewId(),
            'config' => $this->getConfig(),
            'position' => $this->getPosition(),
            'public_status' => $this->getPublicStatus(),
            'timestamp' => $this->getTimestamp()
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @param $preview
     */
    public function setPreview($preview): void
    {
        $this->preview = $preview;
    }

    /**
     * @return mixed
     */
    public function getPreviewId()
    {
        return $this->previewId;
    }

    /**
     * @param $previewId
     */
    public function setPreviewId($previewId): void
    {
        $this->previewId = $previewId;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param $config
     * @return bool
     */
    public function setConfig($config): bool
    {
        if (empty($config)) {
            $config = '{}';
        }

        // If there is a bag format, we try to correct it
        if (!$this->isJson($config)) {
            $config = stripcslashes($config);

            if (!$this->isJson($config)) {
                $config = preg_replace("/[\t\r\n ]+/", ' ', $config);

                if (!$this->isJson($config)) {
                    error_log('Can not convert to JSON: ' . print_r($config, true));

                    return false;
                }
            }
        }

        $this->config = $config;

        return true;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param $position
     */
    public function setPosition($position): void
    {
        $this->position = $position;
    }

    /**
     * @return bool
     */
    public function getPublicStatus(): bool
    {
        return $this->publicStatus;
    }

    /**
     * @param $publicStatus
     */
    public function setPublicStatus($publicStatus): void
    {
        $this->publicStatus = (bool)$publicStatus;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param $timestamp
     */
    public function setTimestamp($timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @param $string
     * @return bool
     */
    private function isJson($string): bool
    {
        json_decode($string);

        return (json_last_error() === JSON_ERROR_NONE);
    }
}
