<?php

namespace SuprDesignStore;

/**
 * Class DesignColorSchemeManager
 *
 * @package SuprDesignStore
 */
class DesignColorSchemeManager
{
    private $tableName;

    /**
     * DesignColorSchemeManager constructor.
     */
    public function __construct()
    {
        global $wpdb;

        $this->db = $wpdb;

        $this->tableName = $this->db->base_prefix . 'supr_design_store_color_scheme';
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * Create table for color schemes if not exist
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS {$this->tableName} (
        `id` INT NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(64),
        `preview` TEXT,
        `preview_id` INT,
        `config` JSON,
        `position` SMALLINT NOT NULL DEFAULT 100,
        `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY(`id`)
        );";

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        dbDelta($sql);

        add_blog_option(1, "{$this->tableName}_table_version", SUPR_DESIGN_STORE_VERSION);
    }

    /**
     * @return \SuprDesignStore\DesignColorScheme[]
     */
    public function getDesignColorSchemes(): array
    {
        $returned = [];

        $sql = "SELECT * FROM {$this->tableName}";

        $sql .= ' ORDER BY `position`';

        $designColorSchemes = $this->db->get_results($sql, ARRAY_A);

        if ($designColorSchemes) {
            foreach ($designColorSchemes as $designColorScheme) {
                $returned[] = new DesignColorScheme($designColorScheme);
            }
        }

        return $returned;
    }

    /**
     * @param $id
     * @return null|\SuprDesignStore\DesignColorScheme
     */
    public function getDesignColorSchemeById($id): ?DesignColorScheme
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE `id` = %d";
        $sql = $this->db->prepare($sql, [$id]);
        $designColorSchemes = $this->db->get_results($sql, ARRAY_A);

        return isset($designColorSchemes[0]) ? new DesignColorScheme($designColorSchemes[0]) : null;
    }

    /**
     * @return array|string
     */
    public function getCurrentConfig()
    {
        if (!class_exists('\Elementor\plugin')) {
            return '{}';
        }

        $scheme = \Elementor\plugin::instance()->schemes_manager->get_scheme('color');

        return \json_encode($scheme->get_scheme_value());
    }

    /**
     * @param \SuprDesignStore\DesignColorScheme $designColorScheme
     */
    public function save($designColorScheme): void
    {
        $data = $designColorScheme->serialize();
        if ($designColorScheme->getId() > 0) {
            unset($data['id']);
            $this->db->update($this->tableName, $data, ['id' => $designColorScheme->getId()]);
        } else {
            unset($data['timestamp']);
            $this->db->insert($this->tableName, $data);
        }
    }

    /**
     * @param $id
     */
    public function delete($id): void
    {
        $this->db->delete($this->tableName, ['id' => $id]);
    }

    /**
     * @return bool
     */
    public function processPostData(): bool
    {
        if (isset($_POST['id']) && (int)$_POST['id'] > 0) {
            $designColorScheme = $this->getDesignColorSchemeById($_POST['id']);
        } else {
            $designColorScheme = new DesignColorScheme();
        }

        $designColorScheme->unserialize($_POST);

        $this->save($designColorScheme);

        return true;
    }

    /**
     * @param $designColorSchemeId
     * @return bool
     */
    public function applyDesignColorScheme($designColorSchemeId): bool
    {
        // Check Plan
        if (!Manager::instance()->canUseDesignStore()) {
            return false;
        }

        $designColorScheme = $this->getDesignColorSchemeById($designColorSchemeId);

        // If color scheme doesn't exist
        if ($designColorScheme === null) {
            return false;
        }

        $config = json_decode($designColorScheme->getConfig(), true);

        // If config doesn't exist
        if (!\is_array($config) || \count($config) !== 4) {
            return false;
        }

        if (!class_exists('\Elementor\plugin')) {
            return false;
        }

        // Update local temlate

        $scheme = \Elementor\plugin::instance()->schemes_manager->get_scheme('color');

        $scheme->save_scheme($config);

        return true;
    }
}
