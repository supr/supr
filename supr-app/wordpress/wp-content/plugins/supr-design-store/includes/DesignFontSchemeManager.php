<?php

namespace SuprDesignStore;

/**
 * Class DesignFontSchemeManager
 *
 * @package SuprDesignStore
 */
class DesignFontSchemeManager
{
    private $db;

    private $tableName;

    /**
     * DesignFontSchemeManager constructor.
     */
    public function __construct()
    {
        global $wpdb;

        $this->db = $wpdb;
        $this->tableName = $this->db->base_prefix . 'supr_design_store_font_scheme';
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * Create table for font schemes if not exist
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS {$this->tableName} (
        `id` INT NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(64),
        `preview` TEXT,
        `preview_id` INT,
        `config` JSON,
        `position` SMALLINT NOT NULL DEFAULT 100,
        `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY(`id`)
        );";

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        dbDelta($sql);

        add_blog_option(1, "{$this->tableName}_table_version", SUPR_DESIGN_STORE_VERSION);
    }

    /**
     * @return DesignFontScheme[]
     */
    public function getDesignFontSchemes(): array
    {
        $returned = [];

        $sql = "SELECT * FROM {$this->tableName}";

        $sql .= ' ORDER BY `position`';

        $designFontSchemes = $this->db->get_results($sql, ARRAY_A);

        if ($designFontSchemes) {
            foreach ($designFontSchemes as $designFontScheme) {
                $returned[] = new DesignFontScheme($designFontScheme);
            }
        }

        return $returned;
    }

    /**
     * @param $id
     * @return null|DesignFontScheme
     */
    public function getDesignFontSchemeById($id): ?DesignFontScheme
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE `id` = %d";
        $sql = $this->db->prepare($sql, [$id]);
        $designFontSchemes = $this->db->get_results($sql, ARRAY_A);

        return isset($designFontSchemes[0]) ? new DesignFontScheme($designFontSchemes[0]) : null;
    }

    /**
     * @return array|string
     */
    public function getCurrentConfig()
    {
        if (!class_exists('\Elementor\plugin')) {
            return '{}';
        }

        $scheme = \Elementor\plugin::instance()->schemes_manager->get_scheme('typography');

        return \json_encode($scheme->get_scheme_value());
    }

    /**
     * @param \SuprDesignStore\DesignFontScheme $designFontScheme
     */
    public function save($designFontScheme): void
    {
        $data = $designFontScheme->serialize();
        if ($designFontScheme->getId() > 0) {
            unset($data['id']);
            $this->db->update($this->tableName, $data, ['id' => $designFontScheme->getId()]);
        } else {
            unset($data['timestamp']);
            $this->db->insert($this->tableName, $data);
        }
    }

    /**
     * @param $id
     */
    public function delete($id): void
    {
        $this->db->delete($this->tableName, ['id' => $id]);
    }

    /**
     * @return bool
     */
    public function processPostData(): bool
    {
        if (isset($_POST['id']) && (int)$_POST['id'] > 0) {
            $designFontScheme = $this->getDesignFontSchemeById($_POST['id']);
        } else {
            $designFontScheme = new DesignFontScheme();
        }

        $designFontScheme->unserialize($_POST);

        $this->save($designFontScheme);

        return true;
    }

    /**
     * @param $designFontSchemeId
     * @return bool
     */
    public function applyDesignFontScheme($designFontSchemeId): bool
    {
        // Check Plan
        if (!Manager::instance()->canUseDesignStore()) {
            return false;
        }

        $designFontScheme = $this->getDesignFontSchemeById($designFontSchemeId);

        // If font scheme doesn't exist
        if ($designFontScheme === null) {
            return false;
        }

        $config = json_decode($designFontScheme->getConfig(), true);

        // If config doesn't exist
        if (!\is_array($config) || \count($config) !== 4) {
            return false;
        }

        if (!class_exists('\Elementor\plugin')) {
            return false;
        }

        // Update local temlate

        $scheme = \Elementor\plugin::instance()->schemes_manager->get_scheme('typography');

        $scheme->save_scheme($config);

        return true;
    }
}
