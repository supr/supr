<?php

namespace SuprDesignStore;

class AjaxRestEndpoint
{
    /**
     * Add endpoints to get colors and fonts settings
     */
    public static function addEndPoints()
    {
        //add_action('wp_ajax_nopriv_supr-design-store-get-color-scheme', array(self::class, 'getColorScheme'));
        add_action('wp_ajax_supr-design-store-get-color-scheme', array(self::class, 'getColorScheme'));
        //add_action('wp_ajax_nopriv_supr-design-store-get-font-scheme', array(self::class, 'getFontScheme'));
        add_action('wp_ajax_supr-design-store-get-font-scheme', array(self::class, 'getFontScheme'));
    }

    /**
     * Get color scheme from blog option
     *
     * @return void
     */
    public static function getColorScheme(): void
    {
        $blog_id = $_REQUEST['blog_id'] ?? 10;
        echo json_encode(get_blog_option($blog_id, 'elementor_scheme_color', []));
        wp_die();
    }

    /**
     * Get fonts from blog option
     *
     * @return void
     */
    public static function getFontScheme(): void
    {
        $blog_id = $_REQUEST['blog_id'] ?? 10;
        echo json_encode(get_blog_option($blog_id, 'elementor_scheme_typography', []));
        wp_die();
    }
}
