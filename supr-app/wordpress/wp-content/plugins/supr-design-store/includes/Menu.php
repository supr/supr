<?php

namespace SuprDesignStore;

/**
 * Class Menu
 *
 * @package SuprDesignStore
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add menu pages
        add_action('admin_menu', [__CLASS__, 'createMenu']);

        // Add network admin menu page
        add_action('network_admin_menu', [__CLASS__, 'createNetworkMenu']);

        // Add settings link on plugin page
        add_filter('network_admin_plugin_action_links_' . SUPR_DESIGN_STORE_FILE, [__CLASS__, 'addSettingsPage']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createMenu(): void
    {
        add_menu_page(
            __('Design store', 'supr-design-store'),
            __('Design store', 'supr-design-store'),
            'manage_options',
            'supr_design_store',
            [Page::class, 'designsPage']
        );

        add_submenu_page(
            'supr_design_store',
            __('Designs', 'supr-design-store'),
            __('Designs', 'supr-design-store'),
            'manage_options',
            'supr_design_store_designs',
            [Page::class, 'designsPage']
        );

        add_submenu_page(
            'supr_design_store',
            __('Elements', 'supr-design-store'),
            __('Elements', 'supr-design-store'),
            'manage_options',
            'supr_design_store_elements',
            [Page::class, 'elementsPage']
        );

        add_submenu_page(
            'supr_design_store',
            __('Colors', 'supr-design-store'),
            __('Colors', 'supr-design-store'),
            'manage_network',
            'supr_design_store_colors',
            [Page::class, 'colorsPage']
        );

        add_submenu_page(
            'supr_design_store',
            __('Fonts', 'supr-design-store'),
            __('Fonts', 'supr-design-store'),
            'manage_network',
            'supr_design_store_fonts',
            [Page::class, 'fontsPage']
        );

        //remove_submenu_page('supr_design_store', 'supr_design_store');
    }

    /**
     * Add menu to admin panel
     */
    public static function createNetworkMenu(): void
    {
        add_menu_page(
            __('Design store', 'supr-design-store'),
            __('Design store', 'supr-design-store'),
            'manage_network',
            'supr_design_store_settings',
            [Page::class, 'settingsPage'],
            'dashicons-admin-customizer',
            201
        );
    }

    /**
     * @param $links
     * @return mixed
     */
    public static function addSettingsPage($links)
    {
        $settings_link = '<a href="admin.php?page=supr_design_store_settings">' . __('Settings', 'supr-design-store') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
