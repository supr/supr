<?php

namespace SuprDesignStore;

/**
 * Class ResourceManager
 *
 * @package SuprDesignStore
 */
class ResourceManager
{
    /**
     * Register files for plugin
     */
    public static function addResources(): void
    {
        // We need it for upload of preview on settings page
        wp_enqueue_media();
        // JS and CSS
        wp_register_style('supr_design_store_style', SUPR_DESIGN_STORE_URL . 'css/supr-design-store.css', false, '1.0.0', 'all');
        wp_register_script('supr_design_store_js', SUPR_DESIGN_STORE_URL . 'js/supr-design-store.js', ['jquery'], '1.0.0', true);
        // Use the registered jquery and style above
        add_action('admin_enqueue_scripts', [__CLASS__, 'enqueueFiles']);
    }

    /**
     * Add files
     */
    public static function enqueueFiles(): void
    {
        wp_enqueue_style('supr_design_store_style');
        wp_enqueue_script('supr_design_store_js');
    }

    /**
     * Add js variables to js script
     */
    public static function addJsVariables(): void
    {
        $translation_array = [
            'activate_dialog_title' => __('Use [object_name]?', 'supr-design-store'),
            'activate_dialog_text' => __('If you activate the selected [object_name], your previous settings will be overwritten.', 'supr-design-store'),
            'activate_dialog_ok' => __('Activate', 'supr-design-store'),
            'activate_dialog_cancel' => __('Cancel', 'supr-design-store'),
            'loading_dialog_title' => __('Processing...', 'supr-design-store'),
            'loading_dialog_text' => __('We are applying the element...', 'supr-design-store')
        ];

        wp_localize_script('supr_design_store_js', 'supr_design_store_values', $translation_array);
    }
}
