<?php

namespace SuprDesignStore;

/**
 * Class DesignManager
 *
 * @package SuprDesignStore
 */
class DesignManager
{
    private $db;

    private $tableName;

    /**
     * The name of the relationship table between the design and the elements.
     *
     * @var string
     */
    private $tableRelationName;

    /**
     * DesignManager constructor.
     */
    public function __construct()
    {
        global $wpdb;

        $this->db = $wpdb;
        $this->tableName = $this->db->base_prefix . 'supr_design_store_design';
        $this->tableRelationName = $this->db->base_prefix . 'supr_design_store_design2element';
    }

    /**
     * Create table for elements if not exist
     */
    public function createTable(): void
    {
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        $designColorSchemeManager = new DesignColorSchemeManager();
        $designFontSchemeManager = new DesignFontSchemeManager();

        // Create designs table
        $sql = "CREATE TABLE IF NOT EXISTS {$this->tableName} (
        `id` INT NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(64),
        `description` TEXT,
        `categories` VARCHAR(256),
        `preview` TEXT,
        `preview_id` INT,
        `color_scheme_id` INT,
        `font_scheme_id` INT,
        `position` SMALLINT NOT NULL DEFAULT 100,
        `public_status` TINYINT(1) DEFAULT 1,
        `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY(`id`),
        FOREIGN KEY (color_scheme_id) REFERENCES {$designColorSchemeManager->getTableName()}(id) ON DELETE SET NULL ON UPDATE CASCADE,
        FOREIGN KEY (font_scheme_id) REFERENCES {$designFontSchemeManager->getTableName()}(id) ON DELETE SET NULL ON UPDATE CASCADE
        );";

        dbDelta($sql);

        add_blog_option(1, "{$this->tableName}_table_version", SUPR_DESIGN_STORE_VERSION);

        // Create relations table
        $designElementManager = new DesignElementManager();

        $sql = "CREATE TABLE IF NOT EXISTS {$this->tableRelationName} (
        `id` INT NOT NULL AUTO_INCREMENT,
        `design_id` INT NOT NULL,
        `element_id` INT NOT NULL,
        `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY(`id`),
        FOREIGN KEY (design_id) REFERENCES {$this->tableName}(id) ON DELETE CASCADE,
        FOREIGN KEY (element_id) REFERENCES {$designElementManager->getTableName()}(id) ON DELETE CASCADE
        );";

        dbDelta($sql);

        add_option("{$this->tableRelationName}_table_version", SUPR_DESIGN_STORE_VERSION);
    }

    /**
     * @param null $category
     * @return \SuprDesignStore\Design[]
     */
    public function getDesigns($category = null): array
    {
        $returned = [];

        $sql = "SELECT * FROM {$this->tableName} WHERE 1";

        if ($category !== null) {
            $sql .= ' AND `categories` like %s';
            $sql = $this->db->prepare($sql, ['%' . $category . '%']);
        }

        if (!is_super_admin()) {
            $sql .= ' AND `public_status` = 1';
        }

        $sql .= ' ORDER BY `position`';

        $designs = $this->db->get_results($sql, ARRAY_A);

        if ($designs) {
            foreach ($designs as $design) {
                $design['elements'] = $this->getElementsIds($design['id']);
                $returned[] = new Design($design);
            }
        }

        return $returned;
    }

    /**
     * @param $id
     * @return null|Design
     */
    public function getDesignById($id): ?Design
    {
        // Find design
        $sql = "SELECT * FROM {$this->tableName} WHERE `id` = %d";
        $sql = $this->db->prepare($sql, [$id]);
        $designs = $this->db->get_results($sql, ARRAY_A);

        if (isset($designs[0])) {
            $designs[0]['elements'] = $this->getElementsIds($id);
            return new Design($designs[0]);
        }

        return null;
    }

    /**
     * Get all categories of themes as an array
     *
     * @return array
     */
    public function getCategories(): array
    {
        $sql = "SELECT DISTINCT `categories` FROM {$this->tableName}";
        $result = $this->db->get_results($sql, ARRAY_A);

        $categories = [];

        foreach ($result as $row) {
            $array = explode(';', $row['categories']);
            // Delete empty values
            $array = array_filter($array);
            // Save in returned array
            foreach ($array as $item) {
                $categories[$item] = $item;
            }
        }

        return array_values($categories);
    }

    /**
     * @param $designId
     * @return array
     */
    public function getElementsIds($designId): array
    {
        $elements = [];
        // Get elements
        $sql = "SELECT * FROM {$this->tableRelationName} WHERE `design_id` = %d";
        $sql = $this->db->prepare($sql, [$designId]);
        $relations = $this->db->get_results($sql, ARRAY_A);
        foreach ($relations as $relation) {
            $elements[] = $relation['element_id'];
        }

        return $elements;
    }

    /**
     * @param \SuprDesignStore\Design $design
     */
    public function save($design): void
    {
        $data = $design->serialize();
        // It is a string
        $data['categories'] = implode(';', $data['categories']);
        unset($data['elements']);
        if ($design->getId() > 0) {
            unset($data['id']);
            $this->db->update($this->tableName, $data, ['id' => $design->getId()]);
        } else {
            unset($data['timestamp']);
            $this->db->insert($this->tableName, $data);
            $design->setId($this->db->insert_id);
        }

        // Delete all relations
        $this->db->delete($this->tableRelationName, ['design_id' => $design->getId()]);

        // Save new relations
        foreach ($design->getElements() as $element) {
            $data = ['design_id' => $design->getId(), 'element_id' => $element->getId()];
            $this->db->insert($this->tableRelationName, $data);
        }
    }

    /**
     * @param $id
     */
    public function delete($id): void
    {
        $this->db->delete($this->tableName, ['id' => $id]);
    }

    /**
     * @return bool
     */
    public function processPostData(): bool
    {
        // We can change Category for design
        if (isset($_POST['new_category']) && !empty($_POST['new_category'])) {
            $_POST['categories'][] = $_POST['new_category'];
        }

        if (isset($_POST['id']) && (int)$_POST['id'] > 0) {
            $design = $this->getDesignById($_POST['id']);
        } else {
            $design = new Design();
        }

        $design->unserialize($_POST);

        $this->save($design);

        return true;
    }

    /**
     * @param $designId
     * @return bool
     */
    public function applyDesign($designId): bool
    {
        // Check Plan
        if (!Manager::instance()->canUseDesignStore()) {
            return false;
        }

        // Disable W3TC caching
        define('DONOTCACHEDB', true);

        // Get all elements of design
        $design = $this->getDesignById($designId);
        $elementIds = $this->getElementsIds($designId);

        // If design doesn't exist
        if ($design === null || empty($elementIds)) {
            return false;
        }

        $designElementManager = new DesignElementManager();
        $designColorSchemeManager = new DesignColorSchemeManager();
        $designFontSchemeManager = new DesignFontSchemeManager();

        // Apply all elements
        $success = true;
        foreach ($elementIds as $elementId) {
            $success = $success && $designElementManager->applyElement($elementId);
        }

        // Apply colors
        if ($design->getColorSchemeId() !== null) {
            $success = $success && $designColorSchemeManager->applyDesignColorScheme($design->getColorSchemeId());
        }

        // Allpy fonts
        if ($design->getFontSchemeId() !== null) {
            $success = $success && $designFontSchemeManager->applyDesignFontScheme($design->getFontSchemeId());
        }

        return $success;
    }

    //////////////////
    //    UPDATES   //
    //////////////////

    /**
     * Update tables from v 1.0.0 to 1.1.0
     */
    public function updateTableAddColorAndFont(): void
    {
        global $wpdb;

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        $designColorSchemeManager = new DesignColorSchemeManager();
        $designFontSchemeManager = new DesignFontSchemeManager();

        $sql = "ALTER TABLE {$this->tableName} ADD `color_scheme_id` INT";
        $wpdb->query($sql);

        $sql = "ALTER TABLE {$this->tableName} ADD `font_scheme_id` INT";
        $wpdb->query($sql);

        $sql = "
ALTER TABLE {$this->tableName} ADD FOREIGN KEY (color_scheme_id) REFERENCES {$designColorSchemeManager->getTableName()}(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE {$this->tableName} ADD FOREIGN KEY (font_scheme_id) REFERENCES {$designFontSchemeManager->getTableName()}(id) ON DELETE SET NULL ON UPDATE CASCADE;
";
        dbDelta($sql);
    }

    /**
     * Update tables from v 1.1.0 to 1.2.0
     */
    public function updateTableAddPublicStatus(): void
    {
        global $wpdb;

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        $sql = "ALTER TABLE {$this->tableName} ADD `public_status` TINYINT(1) DEFAULT 1";
        $wpdb->query($sql);

        dbDelta($sql);
    }
}
