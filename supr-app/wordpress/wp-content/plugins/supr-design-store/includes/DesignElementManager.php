<?php

namespace SuprDesignStore;

/**
 * Class DesignElementManager
 *
 * @package SuprDesignStore
 */
class DesignElementManager
{
    private $db;

    private $tableName;

    /**
     * DesignElementManager constructor.
     */
    public function __construct()
    {
        global $wpdb;

        $this->db = $wpdb;
        $this->tableName = $this->db->base_prefix . 'supr_design_store_element';
    }

    /**
     * Get all types of exist elements.
     *
     * @return array
     */
    public static function getElementTypes(): array
    {
        return [
            'kopfzeile' => __('Header', 'supr-design-store'),
            'fusszeile' => __('Footer', 'supr-design-store'),
            'blog-beitrag' => __('Blog post ', 'supr-design-store'),
            'blog-archiv' => __('Blog archive', 'supr-design-store'),
            'produktkatalog' => __('Product catalog', 'supr-design-store'),
            'produktseite' => __('Product page', 'supr-design-store'),
            'wartungsmodus' => __('Maintenance mode', 'supr-design-store'),
            'seiten' => __('Pages', 'supr-design-store'),
            'startseite' => __('Start page', 'supr-design-store')
        ];
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * Create table for elements if not exist
     */
    public function createTable(): void
    {
        $sql = "CREATE TABLE IF NOT EXISTS {$this->tableName} (
        `id` INT NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(64),
        `description` TEXT,
        `type` VARCHAR(32) NOT NULL,
        `preview` TEXT,
        `preview_id` INT,
        `config` JSON,
        `position` SMALLINT NOT NULL DEFAULT 100,
        `public_status` TINYINT(1) DEFAULT 1,
        `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY(`id`)
        );";

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        dbDelta($sql);

        add_blog_option(1, "{$this->tableName}_table_version", SUPR_DESIGN_STORE_VERSION);
    }

    /**
     * @param null $type
     * @return \SuprDesignStore\DesignElement[]
     */
    public function getElements($type = null): array
    {
        $returned = [];

        $sql = "SELECT * FROM {$this->tableName} WHERE 1";

        if ($type !== null) {
            $sql .= ' AND `type` like %s';
            $sql = $this->db->prepare($sql, [$type]);
        }

        if (!is_super_admin()) {
            $sql .= ' AND `public_status` = 1';
        }

        $sql .= ' ORDER BY `position`';

        $elements = $this->db->get_results($sql, ARRAY_A);

        if ($elements) {
            foreach ($elements as $element) {
                $returned[] = new DesignElement($element);
            }
        }

        return $returned;
    }

    /**
     * @param $id
     * @return null|\SuprDesignStore\DesignElement
     */
    public function getElementById($id): ?DesignElement
    {
        $sql = "SELECT * FROM {$this->tableName} WHERE `id` = %d";
        $sql = $this->db->prepare($sql, [$id]);
        $elements = $this->db->get_results($sql, ARRAY_A);

        return isset($elements[0]) ? new DesignElement($elements[0]) : null;
    }

    /**
     * @param \SuprDesignStore\DesignElement $designElement
     */
    public function save($designElement): void
    {
        $data = $designElement->serialize();
        if ($designElement->getId() > 0) {
            unset($data['id']);
            $this->db->update($this->tableName, $data, ['id' => $designElement->getId()]);
        } else {
            unset($data['timestamp']);
            $this->db->insert($this->tableName, $data);
        }
    }

    /**
     * @param $id
     */
    public function delete($id): void
    {
        $this->db->delete($this->tableName, ['id' => $id]);
    }

    /**
     * @return bool
     */
    public function processPostData(): bool
    {
        if (isset($_POST['id']) && (int)$_POST['id'] > 0) {
            $designElement = $this->getElementById($_POST['id']);
        } else {
            $designElement = new DesignElement();
        }

        $designElement->unserialize($_POST);

        $this->save($designElement);

        return true;
    }

    /**
     * @param $elementId
     * @return bool
     */
    public function applyElement($elementId): bool
    {
        // Check Plan
        if (!Manager::instance()->canUseDesignStore()) {
            return false;
        }

        $designElement = $this->getElementById($elementId);

        // If element doesn't exist
        if ($designElement === null) {
            return false;
        }

        $config = json_decode($designElement->getConfig(), true);

        // If config doesn't exist
        if (!\is_array($config) || !isset($config['content'])) {
            return false;
        }

        // Find post for changes
        $wpPosts = get_posts(
            [
                'post_type' => 'elementor_library',
                'name' => $designElement->getType(),
                'post_status' => 'publish'
            ]
        );

        // If post doesn't exist
        if (!isset($wpPosts[0])) {
            return false;
        }

        /** @var \WP_Post $wpPost */
        $wpPost = $wpPosts[0];

        // It was: Content should be string with slashes (see: plugins/elementor/includes/template-library/manager.php (278))
        // $config['content'] = addslashes(json_encode($config['content']));

        // Replace Images
        $config['content'] = $this->replaceImages($config['content']);
        $config['content'] = json_encode($config['content']);

        if (!class_exists('\Elementor\plugin')) {
            return false;
        }

        // Update local temlate
        $result = \Elementor\plugin::instance()->templates_manager->update_template(array_merge($config, ['source' => 'local', 'id' => $wpPost->ID]));

        do_action('supr-design-store-element-applied');

        return !($result instanceof \WP_Error);

        // It works, but not good. It is better to make it with templates manager
        //return update_post_meta($wpPost->ID, '_elementor_data', addslashes(json_encode($config['content'])));
    }

    /**
     * Replace images with your own
     *
     * @param array $array
     * @return mixed
     */
    private function replaceImages($array)
    {
        foreach ($array as $num => $val) {
            if (\is_array($val)) {
                if (isset($val['url'], $val['id']) && strpos($num, 'image') !== false) {
                    // It happens sometimes, that url is empty #***REMOVED***-339 :(
                    if (!empty($array[$num]['url']) && preg_match('/\.(jpe?g|gif|png|webp|svg)$/i', $array[$num]['url'])) {
                        $array[$num] = $this->saveImageFromUrl($array[$num]['url']);
                    }
                } else {
                    $array[$num] = $this->replaceImages($val);
                }
            }
        }

        return $array;
    }

    /**
     * Save image in the blog from foreign url
     *
     * @param string $imageUrl
     * @return array
     */
    private function saveImageFromUrl($imageUrl): array
    {
        // Get upload dir
        $uploadDir = wp_upload_dir();

        // Options for get file
        $arrContextOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false
            ]
        ];

        $imageData = file_get_contents($imageUrl, false, stream_context_create($arrContextOptions));
        $filename = basename($imageUrl);
        $newFilePath = $uploadDir['path'] . '/' . $filename;

        // Check if also exist and try to return it
        if (file_exists($newFilePath)) {
            // Try to find it
            $attachment = $this->db->get_col($this->db->prepare("SELECT `ID` FROM `{$this->db->posts}` WHERE `guid`='%s';", $newFilePath));

            if (isset($attachment[0])) {
                return [
                    'id' => $attachment[0],
                    'url' => $uploadDir['url'] . '/' . basename($newFilePath)
                ];
            }
        }

        // Check if also exist and change the name
        $i = 0;
        while (file_exists($newFilePath)) {
            $i++;
            $newFilePath = $uploadDir['path'] . '/' . $i . '_' . $filename;
        }

        // Put the file
        file_put_contents($newFilePath, $imageData);

        // Prepare attachment's options
        $wpFiletype = wp_check_filetype($filename, null);

        $attachment = [
            'guid' => $newFilePath,
            'post_mime_type' => $wpFiletype['type'],
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit'
        ];

        // Insert attachment
        $attachId = wp_insert_attachment($attachment, $newFilePath);
        require_once ABSPATH . 'wp-admin/includes/image.php';
        $attachData = wp_generate_attachment_metadata($attachId, $newFilePath);
        wp_update_attachment_metadata($attachId, $attachData);

        return [
            'id' => $attachId,
            'url' => $uploadDir['url'] . '/' . basename($newFilePath)
        ];
    }

    /**
     * Update tables from v 1.1.0 to 1.2.0
     */
    public function updateTableAddPublicStatus(): void
    {
        global $wpdb;

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        $sql = "ALTER TABLE {$this->tableName} ADD `public_status` TINYINT(1) DEFAULT 1";
        $wpdb->query($sql);

        dbDelta($sql);
    }
}
