<?php

namespace SuprDesignStore;

/**
 * Class Plugin
 *
 * @package SuprDesignStore
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Init
     */
    public function init(): void
    {
        self::doUpgrade();

        // Load JS and CSS for all own pages
        if (isset($_GET['page']) && strpos($_GET['page'], 'supr_design_store') === 0) {
            ResourceManager::addResources();
            ResourceManager::addJsVariables();
        }
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // Load translations
        \add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        Menu::addMenu();
        AjaxRestEndpoint::addEndPoints();

        \add_action('admin_init', [$this, 'init'], 0);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_DESIGN_STORE_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }

    /**
     * Create new tables and write
     */
    public static function activationHook(): void
    {
        (new DesignElementManager())->createTable();
        (new DesignManager())->createTable();
    }

    /**
     * @return bool
     */
    public static function doUpgrade(): bool
    {
        //(new DesignManager())->updateTableAddColorAndFont();
        // Check version
        if (\get_blog_option(1, 'supr_design_store_version', '') === SUPR_DESIGN_STORE_VERSION) {
            return true;
        }

        // Do updates
        switch (\get_blog_option(1, 'supr_design_store_version')) {
            case null:
            case '1.0.0':
                (new DesignColorSchemeManager())->createTable();
                (new DesignFontSchemeManager())->createTable();
                (new DesignManager())->updateTableAddColorAndFont();
                break;
            case '1.1.0':
                (new DesignManager())->updateTableAddPublicStatus();
                (new DesignElementManager())->updateTableAddPublicStatus();
                break;
        }

        // Set actual version
        \update_blog_option(1, 'supr_design_store_version', SUPR_DESIGN_STORE_VERSION);

        // Do next upgrade
        return self::doUpgrade();
    }
}

Plugin::instance();
