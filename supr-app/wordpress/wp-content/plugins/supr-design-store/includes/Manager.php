<?php

namespace SuprDesignStore;

/**
 * Class Manager
 *
 * @package SuprDesignStore
 */
class Manager
{
    /**
     * @var null
     */
    public static $instance;

    /**
     * @var boolean
     */
    private $canUseDesignStore;

    /**
     * @return Manager
     */
    public static function instance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return bool
     */
    public function canUseDesignStore(): bool
    {
        // Check
        if ($this->canUseDesignStore === null) {
            // Default is true
            $this->canUseDesignStore = true;

            // Check Plan
            if (class_exists('\SuprUpsale\Manager')) {
                $wpPlanName = \SuprUpsale\Manager::instance()->getPlanName();
                if (!\in_array($wpPlanName, ['smart', 'smart trial', 'pro']) && !is_super_admin()) {
                    error_log('[Design Store] Shop #' . get_current_blog_id() . ' with plan ' . $wpPlanName . ' tried to use DesignStore without frontend restrictions.');
                    $this->canUseDesignStore = false;
                }
            }
        }

        return $this->canUseDesignStore;
    }
}
