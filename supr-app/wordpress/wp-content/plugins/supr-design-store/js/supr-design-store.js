(function ($) {
    // Select image button
    $('#supr-design-store-select-image-button').click(function (event) {
        event.preventDefault();
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this);
        var input_preview = button.attr('id').replace('-button', '');
        var input_preview_id = input_preview + '-id';

        wp.media.editor.send.attachment = function (props, attachment) {
            $("#" + input_preview).val(attachment.url);
            $("#" + input_preview_id).val(attachment.id);
            $(button).parent().find('img.preview').attr('src', attachment.url).show();
            wp.media.editor.send.attachment = send_attachment_bkp;
        };

        wp.media.editor.open(button);
        return false;
    });

    // Ask before delete
    $('.supr-design-store a.delete').click(function () {
        return window.confirm("Are you sure?");
    });

    // Show preview
    $('.supr-design-store-template-preview').click(function () {
        Swal.fire({
            imageUrl: $(this).data('preview'),
            width: 980,
            animation: false
        })
    });

    // Ask if you want really activate
    $('.supr-design-store-object-activate').click(function () {
        var activate_url = $(this).data('url');
        var object_name = $(this).data('object-name');
        Swal.fire({
            title: supr_design_store_values.activate_dialog_title.replace('[object_name]', object_name),
            icon: 'info',
            text: supr_design_store_values.activate_dialog_text.replace('[object_name]', object_name),
            customClass: 'animated zoomIn faster',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: true,
            confirmButtonText: supr_design_store_values.activate_dialog_ok,
            cancelButtonText: supr_design_store_values.activate_dialog_cancel,
            showClass: {
                popup: '',
                backdrop: '',
                icon: '',
            },
            hideClass: {
                popup: '',
                backdrop: '',
                icon: '',
            }
        }).then(function (object) {
            if (object.value) {
                // Show loading
                Swal.fire({
                    title: supr_design_store_values.loading_dialog_title,
                    html: supr_design_store_values.loading_dialog_text,
                    onOpen: function () {
                        Swal.showLoading();
                    }
                });
                window.location.href = activate_url;
            }
        })
    });

    // Styling for config
    // Format json data
    if (document.getElementById('supr-design-store-config')) {
        supr_design_format_json('#supr-design-store-config');
    }

    // Load elementor settings from blog option
    $('#supr-design-store-load-colors').on('click', function () {
        var blog_id = $('#supr-design-store-load-colors-store').val();
        $('#supr-design-store-config').prop('disabled', true);
        $('#supr-design-store-config').val('I think a part of me will always be waiting for you');
        jQuery.ajax({
            data: {
                action: 'supr-design-store-get-color-scheme',
                blog_id: blog_id
            },
            url: "/wp-admin/admin-ajax.php",
            success: function (data, textStatus, XMLHttpRequest) {
                Swal.fire(
                    'Success!',
                    'Settings imported from store id: ' + blog_id,
                    'success'
                );
                $('#supr-design-store-config').val(data);
                $('#supr-design-store-config').data('json', JSON.parse(data));
                supr_design_format_json('#supr-design-store-config');
                $('#supr-design-store-config').prop('disabled', false);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                Swal.fire(
                    'Error!',
                    'an error occurred while loading ',
                    'error'
                );
                $('#supr-design-store-config').val(errorThrown);
                $('#supr-design-store-config').prop('disabled', false);
            }
        });
    });

    // Load elementor settings from blog option
    $('#supr-design-store-load-fonts').on('click', function () {
        var blog_id = $('#supr-design-store-load-fonts-store').val();
        $('#supr-design-store-config').prop('disabled', true);
        $('#supr-design-store-config').val('I think a part of me will always be waiting for you');
        jQuery.ajax({
            data: {
                action: 'supr-design-store-get-font-scheme',
                blog_id: blog_id
            },
            url: "/wp-admin/admin-ajax.php",
            success: function (data, textStatus, XMLHttpRequest) {
                Swal.fire(
                    'Success!',
                    'Settings imported from store id: ' + blog_id,
                    'success'
                );
                $('#supr-design-store-config').val(data);
                $('#supr-design-store-config').data('json', JSON.parse(data));
                supr_design_format_json('#supr-design-store-config');
                $('#supr-design-store-config').prop('disabled', false);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                Swal.fire(
                    'Error!',
                    'an error occurred while loading ',
                    'error'
                );
                $('#supr-design-store-config').val(errorThrown);
                $('#supr-design-store-config').prop('disabled', false);
            }
        });
    });

})(jQuery);


/**
 * Fomat json fior config textarea
 *
 * @param selector
 */
function supr_design_format_json(selector)
{
    var supr_design_store_config = jQuery(selector).data("json");
    jQuery(selector).val(JSON.stringify(supr_design_store_config, undefined, 2));
}