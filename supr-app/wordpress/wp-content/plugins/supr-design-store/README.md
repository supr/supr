# DESIGN-STORE

The plugin is an extension for the plugin [Elementor](https://elementor.com).

## How does plugin work?

With the help of the plug-in you can create your own sets of elements, colors, fonts and designs, which consist of elements. And also you can very quickly activate and switch between designs and elements.

