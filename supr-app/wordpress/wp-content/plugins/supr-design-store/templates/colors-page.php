<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$designColorSchemeManager = new \SuprDesignStore\DesignColorSchemeManager();

// Handle request
if (isset($_GET['action'], $_GET['id']) && $_GET['action'] === 'apply') {
    if (!$designColorSchemeManager->applyDesignColorScheme($_GET['id'])) : ?>
        <div class="notice notice-error">
            <p><?= __('An error occurred while activating the template.', 'supr-design-store'); ?></p>
        </div>
    <?php else : ?>
        <div class="notice notice-success">
            <p><?= __('The template has been successfully applied to your store.', 'supr-design-store'); ?></p>
        </div>
    <?php endif;
}

$queryParams = ['page' => 'supr_design_store_colors'];
$designColorSchemes = $designColorSchemeManager->getDesignColorSchemes();
?>
<div class="wrap supr-design-store-template-gallery">
    <h1 class="wp-heading-inline"><?= get_admin_page_title(); ?></h1>
    <?php if (count($designColorSchemes) === 0) : ?>
        <p><?= __('Unfortunately, no color schemes have been added yet.', 'supr-design-store') ?></p>
    <?php else : ?>
        <div class="row supr-design-store-template-gallery-list display-flex">
            <?php foreach ($designColorSchemes as $designColorScheme) : ?>
                <div class="supr-design-store-template-gallery-item wu-col-sm-6 wu-col-md-4">
                    <div class="postbox">
                        <div class="inside">
                            <?php if (!empty($designColorScheme->getPreview())) : ?>
                                <img alt='Color scheme preview <?= $designColorScheme->getName() ?>' src="<?= $designColorScheme->getPreview() ?>">
                            <?php endif; ?>
                            <h4 class="text-uppercase"><?= __($designColorScheme->getName(), 'supr-design-store'); ?></h4>
                            <?php
                            $config = json_decode($designColorScheme->getConfig(), true);
                            if (is_array($config)) {
                                foreach ($config as $num => $color) {
                                    echo "<div style='background-color: {$color};' class='supr-design-store__color-box'></div>";
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="postbox postbox-bar">
                        <div class="inside">
                            <button data-preview="<?= $designColorScheme->getPreview(); ?>" class="button button-secondary supr-design-store-template-preview"><?= __('Preview', 'supr-design-store') ?></button>
                            <button data-url="?<?= http_build_query(array_merge($queryParams, ['action' => 'apply', 'id' => $designColorScheme->getId()])) ?>" data-object-name="<?= __('color scheme', 'supr-design-store') ?>" class="button button-primary supr-design-store-object-activate"><?= __('Activate', 'supr-design-store') ?></button>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>