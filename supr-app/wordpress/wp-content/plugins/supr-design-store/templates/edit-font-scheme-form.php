<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/** @var \SuprDesignStore\DesignFontScheme $designFontScheme */
?>
<form method="post" enctype="multipart/form-data">
    <div class="postbox">
        <div class="inside">
            <h2 class="text-uppercase">
                <span>
                    <?= ($designFontScheme->getId() > 0 ? __('Edit font scheme', 'supr-design-store') . ' <b>' . $designFontScheme->getName() . '</b>' : __('Create new font scheme', 'supr-design-store')); ?>
                </span>
            </h2>
            <hr>
            <div class="row">
                <input type="hidden" name="id" value="<?= $designFontScheme->getId() ?? ''; ?>">
                <div class="wu-col-md-4">
                    <label><?= __('Name', 'supr-design-store'); ?>
                        <input type="text" name="name" value="<?= $designFontScheme->getName() ?? ''; ?>">
                    </label>
                    <label><?= __('Position', 'supr-design-store'); ?>
                        <input type="text" name="position" value="<?= $designFontScheme->getPosition() ?? '100'; ?>"></label>
                    <input id="supr-design-store-select-image" type="hidden" name="preview" value="<?= $designFontScheme->getPreview() ?? ''; ?>">
                    <input id="supr-design-store-select-image-id" type="hidden" name="preview_id" value="<?= $designFontScheme->getPreviewId() ?? ''; ?>">
                    <img src="<?= ($designFontScheme->getPreview() ?? ''); ?>" class="preview" style="display: <?= ($designFontScheme->getPreview() ? 'block' : 'none'); ?>;">
                    <button id="supr-design-store-select-image-button" class="button button-secondary"><?= __('Select preview'); ?></button>
                </div>
                <div class="wu-col-md-8">
                    <label><?= __('Config', 'supr-design-store'); ?>
                        <textarea name="config" id="supr-design-store-config" data-json='<?= $designFontScheme->getConfig() ?? '{}'; ?>'><?= $designFontScheme->getConfig() ?? ''; ?></textarea>
                    </label>
                    <div>
                        <input id="supr-design-store-load-fonts-store" type="text" name="supr-design-store-load-fonts-store" value="10" style="margin: 0 5px 0 0; width: 50px; text-align: centerw">
                        <span id="supr-design-store-load-fonts" class="button button-secondary"><?= __('Load settings from blog id', 'supr-design-store'); ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="postbox postbox-bar">
        <div class="inside">
            <button class="button button-primary"><?= __('Save changes', 'supr-design-store'); ?></button>
        </div>
    </div>
</form>