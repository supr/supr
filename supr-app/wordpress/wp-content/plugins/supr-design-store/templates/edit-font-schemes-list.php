<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/** @var \SuprDesignStore\DesignFontScheme[] $designFontSchemes */
?>
<table class="widefat">
    <thead>
    <tr>
        <th class="row-title" style="width: 50px"><?= __('ID', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 120px"><?= __('Preview', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Name', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 150px"><?= __('Actions', 'supr-design-store'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $queryParams = [
        'delete' => [
            'page' => 'supr_design_store_settings',
            'action' => 'delete',
            'type' => 'font-scheme',
            'tab' => 'font-schemes'
        ],
        'edit' => [
            'page' => 'supr_design_store_settings',
            'action' => 'edit',
            'type' => 'font-scheme',
            'tab' => 'font-schemes'
        ],
        'add' => [
            'page' => 'supr_design_store_settings',
            'action' => 'add',
            'type' => 'font-scheme',
            'tab' => 'font-schemes'
        ]
    ];

    foreach ($designFontSchemes as $designFontScheme) {
        ?>
        <tr>
            <td class="row-title"><?= $designFontScheme->getId(); ?></td>
            <td>
                <a href="<?= $designFontScheme->getPreview(); ?>" target="_blank">
                    <img src="<?= $designFontScheme->getPreview(); ?>" style="width: 100px;"/>
                </a>
            </td>
            <td>
                <p>
                    <a href="?<?= http_build_query(array_merge($queryParams['edit'], ['id' => $designFontScheme->getId()])); ?>">
                        <strong><?= __($designFontScheme->getName(), 'supr-design-store'); ?></strong>
                    </a>
                </p>
                <?php
                $config = json_decode($designFontScheme->getConfig(), true);
                if (is_array($config)) {
                    foreach ($config as $num => $font) {
                        echo "<div style='font-family: {$font['font_family']}; font-weight: {$font['font_weight']};' class='supr-design-store__admin__font-box'>" . __($font['font_family'] . ' - ' . $font['font_weight'], 'supr-design-store') .'</div>';
                    }
                } else {
                    echo "<small>{$designFontScheme->getConfig()}</small>";
                }
                ?>
            </td>
            <td>
                <a class="button button-secondary edit" href="?<?= http_build_query(array_merge($queryParams['edit'], ['id' => $designFontScheme->getId()])); ?>">
                    <span class="dashicons dashicons-edit"></span>
                </a>
                <a class="button button-secondary delete" href="?<?= http_build_query(array_merge($queryParams['delete'], ['id' => $designFontScheme->getId()])); ?>">
                    <span class="dashicons dashicons-post-trash"></span>
                </a>
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
    <tfoot>
    <tr>
        <th class="row-title"><?= __('ID', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Preview', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Name', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Actions', 'supr-design-store'); ?></th>
    </tr>
    </tfoot>
</table>
<div class="postbox">
    <div class="inside">
        <a href="?<?= http_build_query($queryParams['add']); ?>" class="button button-primary"><?= __('Create new element', 'supr-design-store'); ?></a>
    </div>
</div>