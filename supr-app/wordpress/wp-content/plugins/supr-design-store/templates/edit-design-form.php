<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/** @var \SuprDesignStore\Design $design */
use SuprDesignStore\DesignColorSchemeManager;
use SuprDesignStore\DesignElementManager;
use SuprDesignStore\DesignFontSchemeManager;
use SuprDesignStore\DesignManager;
?>
<form method="post" enctype="multipart/form-data">
    <div class="postbox">
        <div class="inside">
            <h2 class="text-uppercase">
                <span>
                    <?= ($design->getId() > 0 ? __('Edit design', 'supr-design-store') . ' <b>' . $design->getName() . '</b>' : __('Create new design', 'supr-design-store')); ?>
                </span>
            </h2>
            <hr>
            <div class="row">
                <input type="hidden" name="id" value="<?= $design->getId() ?? ''; ?>">
                <div class="wu-col-md-4">
                    <label>
                        <?= __('Name', 'supr-design-store'); ?>
                        <input type="text" name="name" value="<?= $design->getName() ?? ''; ?>">
                    </label>
                    <label><?= __('Only for super admins', 'supr-design-store'); ?>
                        <input type="checkbox" name="public_status" value="0" <?= $design->getPublicStatus() === false ? ' checked' : ''; ?>>
                    </label>
                    <label>
                        <?= __('Color scheme', 'supr-design-store'); ?>
                        <select name="color_scheme_id">
                            <option value="">
                                <?= __('Select color scheme', 'supr-design-store'); ?>
                            </option>
                            <?php foreach ((new DesignColorSchemeManager())->getDesignColorSchemes() as $designColorScheme) { ?>
                                <option value="<?= $designColorScheme->getId(); ?>" <?= $design->getColorSchemeId() === $designColorScheme->getId() ? ' selected' : ''; ?>>
                                    <?= __($designColorScheme->getName(), 'supr-design-store'); ?>
                                </option>
                            <?php } ?>
                        </select>
                    </label>
                    <label>
                        <?= __('Font scheme', 'supr-design-store'); ?>
                        <select name="font_scheme_id">
                            <option value="">
                                <?= __('Select font scheme', 'supr-design-store'); ?>
                            </option>
                            <?php foreach ((new DesignFontSchemeManager())->getDesignFontSchemes() as $designFontScheme) { ?>
                                <option value="<?= $designFontScheme->getId(); ?>" <?= $design->getFontSchemeId() === $designFontScheme->getId() ? ' selected' : ''; ?>>
                                    <?= __($designFontScheme->getName(), 'supr-design-store'); ?>
                                </option>
                            <?php } ?>
                        </select>
                    </label>
                    <label>
                        <?= __('Position', 'supr-design-store'); ?>
                        <input type="text" name="position" value="<?= $design->getPosition() ?? '100'; ?>">
                    </label>
                    <label>
                        <?= __('Description', 'supr-design-store'); ?>
                        <input type="text" name="description" value="<?= $design->getDescription() ?? ''; ?>">
                    </label>
                    <label>
                        <?= __('Categories', 'supr-design-store'); ?>
                        <select name="categories[]" size="4" multiple="multiple">
                            <option value="">
                                <?= __('Select categories', 'supr-design-store'); ?>
                            </option>
                            <?php foreach ((new DesignManager())->getCategories() as $category) { ?>
                                <option value="<?= $category; ?>" <?= in_array($category, $design->getCategories(), false) ? ' selected' : ''; ?>>
                                    <?= __($category, 'supr-design-store'); ?>
                                </option>
                            <?php } ?>
                        </select>
                    </label>
                    <label>
                        <?= __('New category', 'supr-design-store'); ?>
                        <input type="text" name="new_category" value="">
                    </label>
                    <input id="supr-design-store-select-image" type="hidden" name="preview" value="<?= $design->getPreview() ?? ''; ?>">
                    <input id="supr-design-store-select-image-id" type="hidden" name="preview_id" value="<?= $design->getPreviewId() ?? ''; ?>">
                    <img src="<?= ($design->getPreview() ?? ''); ?>" class="preview" style="display: <?= ($design->getPreview() ? 'block' : 'none'); ?>;">
                    <button id="supr-design-store-select-image-button" class="button button-secondary"><?= __('Select preview'); ?></button>
                </div>
                <div class="wu-col-md-8">
                    <?php foreach (DesignElementManager::getElementTypes() as $type => $name) : ?>
                        <label>
                            <?= $name; ?>
                            <select name="elements[]">
                                <option value="">
                                    <?= __('Select element', 'supr-design-store'); ?>
                                </option>
                                <?php foreach ((new DesignElementManager())->getElements($type) as $designElement) { ?>
                                    <option value="<?= $designElement->getId(); ?>" <?= $design->hasElement($designElement->getId()) ? ' selected' : ''; ?>>
                                        <?= __($designElement->getName(), 'supr-design-store'); ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </label>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="postbox postbox-bar">
        <div class="inside">
            <button class="button button-primary"><?= __('Save changes', 'supr-design-store'); ?></button>
        </div>
    </div>
</form>