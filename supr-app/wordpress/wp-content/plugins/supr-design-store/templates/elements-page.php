<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

use SuprDesignStore\DesignElementManager;

$designElementManager = new DesignElementManager();

// Handle request
if (isset($_GET['action'], $_GET['id']) && $_GET['action'] === 'apply') {
    if (!$designElementManager->applyElement($_GET['id'])) : ?>
        <div class="notice notice-error">
            <p><?= __('An error occurred while activating the template.', 'supr-design-store') ?></p>
        </div>
    <?php else : ?>
        <div class="notice notice-success">
            <p><?= __('The template has been successfully applied to your store.', 'supr-design-store') ?></p>
        </div>
    <?php endif;
}

$queryParams = ['page' => 'supr_design_store_elements'];
$types = DesignElementManager::getElementTypes();
$currentTab = isset($_GET['tab']) ? \sanitize_text_field($_GET['tab']) : key($types);
$elements = $designElementManager->getElements($currentTab);
?>
<div class="wrap supr-design-store-template-gallery">
    <h1 class="wp-heading-inline"><?= get_admin_page_title() ?></h1>
    <!-- SUPR - Tabs -->
    <div id="elementor-template-library-tabs-wrapper" class="nav-tab-wrapper">
        <?php foreach ($types as $type => $name) : ?>
            <a class="nav-tab<?= ($currentTab === $type) ? ' nav-tab-active' : '' ?>" href="?<?= http_build_query(array_merge($queryParams, ['tab' => $type])) ?>"><?= $name ?></a>
        <?php endforeach; ?>
    </div>
    <br>
    <!-- SUPR - Tabs -->
    <?php if (count($elements) === 0) : ?>
        <p><?= __('Unfortunately, no elements have been added in this category yet.', 'supr-design-store') ?></p>
    <?php else : ?>
        <div class="row supr-design-store-template-gallery-list display-flex">
            <?php foreach ($elements as $element) : ?>
                <div class="supr-design-store-template-gallery-item wu-col-sm-6 wu-col-md-4">
                    <div class="postbox">
                        <div class="inside">
                            <img src="<?= $element->getPreview() ?>">
                            <h4 class="text-uppercase"><?= __($element->getName(), 'supr-design-store') ?></h4>
                            <p>
                                <?= __($element->getDescription(), 'supr-design-store') ?>
                            </p>
                            <p>
                                <small>
                                    <strong><?= __('Type', 'supr-design-store') ?>:</strong> <?= $types[$element->getType()] ?>
                                </small>
                            </p>
                        </div>
                    </div>
                    <div class="postbox postbox-bar">
                        <div class="inside">
                            <button data-preview="<?= $element->getPreview() ?>" class="button button-secondary supr-design-store-template-preview"><?= __('Preview', 'supr-design-store') ?></button>
                            <button data-url="?<?= http_build_query(array_merge($queryParams, ['tab' => $currentTab, 'action' => 'apply', 'id' => $element->getId()])) ?>" data-object-name="<?= __('template', 'supr-design-store') ?>" class="button button-primary supr-design-store-object-activate"><?= __('Activate', 'supr-design-store') ?></button>
                            <?php if ($element->getPublicStatus() === false) : ?>
                                <div class="super-admin-only"><?= __('Super admin only', 'supr-design-store') ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>