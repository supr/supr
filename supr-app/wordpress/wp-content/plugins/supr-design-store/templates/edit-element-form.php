<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/** @var \SuprDesignStore\DesignElement $designElement */
use SuprDesignStore\DesignElementManager;
?>
<form method="post" enctype="multipart/form-data">
    <div class="postbox">
        <div class="inside">
            <h2 class="text-uppercase">
                <span>
                    <?= ($designElement->getId() > 0 ? __('Edit template', 'supr-design-store') . ' <b>' . $designElement->getName() . '</b>' : __('Create new template', 'supr-design-store')); ?>
                </span>
            </h2>
            <hr>
            <div class="row">
                <input type="hidden" name="id" value="<?= $designElement->getId() ?? ''; ?>">
                <div class="wu-col-md-4">
                    <label><?= __('Name', 'supr-design-store'); ?>
                        <input type="text" name="name" value="<?= $designElement->getName() ?? ''; ?>">
                    </label>
                    <label><?= __('Only for super admins', 'supr-design-store'); ?>
                        <input type="checkbox" name="public_status" value="0" <?= $designElement->getPublicStatus() === false ? ' checked' : ''; ?>>
                    </label>
                    <div class="clear"></div>
                    <label><?= __('Type', 'supr-design-store'); ?>
                        <select name="type">
                            <?php foreach (DesignElementManager::getElementTypes() as $value => $name) : ?>
                                <option value="<?= $value; ?>" <?= $designElement->getType() === $value ? ' selected' : ''; ?>>
                                    <?= $name; ?>
                                </option>
                            <?php endforeach; ?>
                        </select></label>
                    <label><?= __('Position', 'supr-design-store'); ?>
                        <input type="text" name="position" value="<?= $designElement->getPosition() ?? '100'; ?>"></label>
                    <label><?= __('Description', 'supr-design-store'); ?>
                        <input type="text" name="description" value="<?= $designElement->getDescription() ?? ''; ?>"></label>
                    <input id="supr-design-store-select-image" type="hidden" name="preview" value="<?= $designElement->getPreview() ?? ''; ?>">
                    <input id="supr-design-store-select-image-id" type="hidden" name="preview_id" value="<?= $designElement->getPreviewId() ?? ''; ?>">
                    <img src="<?= ($designElement->getPreview() ?? ''); ?>" class="preview" style="display: <?= ($designElement->getPreview() ? 'block' : 'none'); ?>;">
                    <button id="supr-design-store-select-image-button" class="button button-secondary"><?= __('Select preview'); ?></button>
                </div>
                <div class="wu-col-md-8">
                    <label><?= __('Config', 'supr-design-store'); ?>
                        <textarea name="config" id="supr-design-store-config" data-json='<?= str_replace("'", "&#39;", $designElement->getConfig()) ?? '{}'; ?>'><?= $designElement->getConfig() ?? ''; ?></textarea>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="postbox postbox-bar">
        <div class="inside">
            <button class="button button-primary"><?= __('Save changes', 'supr-design-store'); ?></button>
        </div>
    </div>
</form>