<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

use SuprDesignStore\Design;
use SuprDesignStore\DesignColorScheme;
use SuprDesignStore\DesignColorSchemeManager;
use SuprDesignStore\DesignElement;
use SuprDesignStore\DesignElementManager;
use SuprDesignStore\DesignFontScheme;
use SuprDesignStore\DesignFontSchemeManager;
use SuprDesignStore\DesignManager;
?>
<div class="wrap supr-design-store settings">
    <h1 class="wp-heading-inline"><?= get_admin_page_title(); ?></h1>
    <!-- TABS -->
    <div id="elementor-template-library-tabs-wrapper" class="nav-tab-wrapper">
        <?php
        $menuTabs = [
            'elements' => [
                'name' => __('Elements', 'supr-design-store'),
                'active' => !isset($_GET['tab']) || $_GET['tab'] === 'elements'
            ],
            'designs' => [
                'name' => __('Designs', 'supr-design-store'),
                'active' => isset($_GET['tab']) && $_GET['tab'] === 'designs'
            ],
            'color-schemes' => [
                'name' => __('Color schemes', 'supr-design-store'),
                'active' => isset($_GET['tab']) && $_GET['tab'] === 'color-schemes'
            ],
            'font-schemes' => [
                'name' => __('Font schemes', 'supr-design-store'),
                'active' => isset($_GET['tab']) && $_GET['tab'] === 'font-schemes'
            ]
        ];

        $queryParams = ['page' => 'supr_design_store_settings'];

        foreach ($menuTabs as $menuTabSlug => $menuTab) { ?>
            <a class="nav-tab<?= $menuTab['active'] ? ' nav-tab-active' : '' ?>" href="?<?= http_build_query(array_merge($queryParams, ['tab' => $menuTabSlug])); ?>"><?= $menuTab['name']; ?></a>
        <?php } ?>
    </div>
    <?php if (is_multisite() && get_current_blog_id() !== 1) : ?>
        <div class="error notice">
            <p><?= __('In multisite mode, the plugin can only be configured in the main blog.', 'supr-design-store'); ?></p>
        </div>
    <?php else : ?>
        <?php
        // [ELEMENTS] Handle POST or GET data
        if (isset($_GET['type']) && $_GET['type'] === 'element') {
            $designElementManager = new DesignElementManager();
            if (isset($_GET['action']) && $_GET['action'] === 'delete') {
                $designElementManager->delete($_GET['id']);
            } else {
                // Handle data
                if (isset($_POST['name'])) {
                    $designElementManager->processPostData();
                } else {
                    $designElement = isset($_GET['id']) ? $designElementManager->getElementById($_GET['id']) : (new DesignElement());
                    include SUPR_DESIGN_STORE_PATH . 'templates/edit-element-form.php';
                }
            }
        }

        // [DESIGNS] Handle POST or GET data
        if (isset($_GET['type']) && $_GET['type'] === 'design') {
            $designManager = new DesignManager();
            if (isset($_GET['action']) && $_GET['action'] === 'delete') {
                $designManager->delete($_GET['id']);
            } else {
                // Handle data
                if (isset($_POST['name'])) {
                    $designManager->processPostData();
                } else {
                    $design = isset($_GET['id']) ? $designManager->getDesignById($_GET['id']) : (new Design());
                    include SUPR_DESIGN_STORE_PATH . 'templates/edit-design-form.php';
                }
            }
        }

        // [COLORS] Handle POST or GET data
        if (isset($_GET['type']) && $_GET['type'] === 'color-scheme') {
            $designColorSchemeManager = new DesignColorSchemeManager();
            if (isset($_GET['action']) && $_GET['action'] === 'delete') {
                $designColorSchemeManager->delete($_GET['id']);
            } else {
                // Handle data
                if (isset($_POST['name'])) {
                    $designColorSchemeManager->processPostData();
                } else {
                    $designColorScheme = isset($_GET['id']) ? $designColorSchemeManager->getDesignColorSchemeById($_GET['id']) : new DesignColorScheme();

                    if (isset($_GET['id'])) {
                        $designColorScheme=$designColorSchemeManager->getDesignColorSchemeById($_GET['id']);
                    } else {
                        $designColorScheme = new DesignColorScheme();
                        $designColorScheme->setConfig($designColorSchemeManager->getCurrentConfig());
                    }

                    include SUPR_DESIGN_STORE_PATH . 'templates/edit-color-scheme-form.php';
                }
            }
        }

        // [FONTS] Handle POST or GET data
        if (isset($_GET['type']) && $_GET['type'] === 'font-scheme') {
            $designFontSchemeManager = new DesignFontSchemeManager();
            if (isset($_GET['action']) && $_GET['action'] === 'delete') {
                $designFontSchemeManager->delete($_GET['id']);
            } else {
                // Handle data
                if (isset($_POST['name'])) {
                    $designFontSchemeManager->processPostData();
                } else {
                    $designFontScheme = isset($_GET['id']) ? $designFontSchemeManager->getDesignFontSchemeById($_GET['id']) : new DesignFontScheme();

                    if (isset($_GET['id'])) {
                        $designFontScheme=$designFontSchemeManager->getDesignFontSchemeById($_GET['id']);
                    } else {
                        $designFontScheme = new DesignFontScheme();
                        $designFontScheme->setConfig($designFontSchemeManager->getCurrentConfig());
                    }

                    include SUPR_DESIGN_STORE_PATH . 'templates/edit-font-scheme-form.php';
                }
            }
        }

        // Show inhalt of tabs
        $currentTab = $_GET['tab'] ?? 'elements';
        switch ($currentTab) {
            case 'elements':
                $designElementManager = new DesignElementManager();
                $designElements = $designElementManager->getElements();
                include SUPR_DESIGN_STORE_PATH . 'templates/edit-elements-list.php';
                break;
            case 'designs':
                $designManager = new DesignManager();
                $designs = $designManager->getDesigns();
                include SUPR_DESIGN_STORE_PATH . 'templates/edit-designs-list.php';
                break;
            case 'color-schemes':
                $designColorSchemeManager = new DesignColorSchemeManager();
                $designColorSchemes = $designColorSchemeManager->getDesignColorSchemes();
                include SUPR_DESIGN_STORE_PATH . 'templates/edit-color-schemes-list.php';
                break;
            case 'font-schemes':
                $designFontSchemeManager = new DesignFontSchemeManager();
                $designFontSchemes = $designFontSchemeManager->getDesignFontSchemes();
                include SUPR_DESIGN_STORE_PATH . 'templates/edit-font-schemes-list.php';
                break;
        }
        ?>
    <?php endif; ?>
</div>