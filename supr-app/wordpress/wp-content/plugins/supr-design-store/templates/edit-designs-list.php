<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/** @var \SuprDesignStore\Design[] $designs */
?>
<table class="widefat supr-design-store-table">
    <thead>
    <tr>
        <th class="row-title" style="width: 50px"><?= __('ID', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 120px"><?= __('Preview', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Name', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Elements', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 150px"><?= __('Categories', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 150px"><?= __('Actions', 'supr-design-store'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $queryParams = [
        'delete' => [
            'page' => 'supr_design_store_settings',
            'action' => 'delete',
            'type' => 'design',
            'tab' => 'designs'
        ],
        'edit' => [
            'page' => 'supr_design_store_settings',
            'action' => 'edit',
            'type' => 'design',
            'tab' => 'designs'
        ],
        'add' => [
            'page' => 'supr_design_store_settings',
            'action' => 'add',
            'type' => 'design',
            'tab' => 'designs'
        ]
    ];

    foreach ($designs as $design) {
        ?>
        <tr class="<?= $design->getPublicStatus() === false ? 'super-admin-only' : ''; ?>">
            <td class="row-title"><?= $design->getId(); ?></td>
            <td>
                <a href="<?= $design->getPreview(); ?>" target="_blank">
                    <img src="<?= $design->getPreview(); ?>" style="width: 100px;"/>
                </a>
            </td>
            <td>
                <p>
                    <a href="?<?= http_build_query(array_merge($queryParams['edit'], ['id' => $design->getId()])); ?>">
                        <strong><?= __($design->getName(), 'supr-design-store'); ?></strong>
                    </a>
                </p>
                <small><?= $design->getDescription(); ?></small>
            </td>
            <td><?php foreach ($design->getElements() as $num => $element) {
                    echo ($num !== 0 ? ' - ' : '') . __($element->getName(), 'supr-design-store');
                } ?></td>
            <td><?= implode(', ', $design->getCategories()); ?></td>
            <td>
                <a class="button button-secondary edit" href="?<?= http_build_query(array_merge($queryParams['edit'], ['id' => $design->getId()])); ?>">
                    <span class="dashicons dashicons-edit"></span>
                </a>
                <a class="button button-secondary delete" href="?<?= http_build_query(array_merge($queryParams['delete'], ['id' => $design->getId()])); ?>">
                    <span class="dashicons dashicons-post-trash"></span>
                </a>
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
    <tfoot>
    <tr>
        <th class="row-title"><?= __('ID', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Preview', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Name', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Elements', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Categories', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Actions', 'supr-design-store'); ?></th>
    </tr>
    </tfoot>
</table>
<div class="postbox">
    <div class="inside">
        <a href="?<?= http_build_query($queryParams['add']); ?>" class="button button-primary"><?= __('Create new design', 'supr-design-store'); ?></a>
    </div>
</div>