<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/** @var \SuprDesignStore\DesignColorScheme[] $designColorSchemes */
?>
<table class="widefat">
    <thead>
    <tr>
        <th class="row-title" style="width: 50px"><?= __('ID', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 120px"><?= __('Preview', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Name', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 150px"><?= __('Actions', 'supr-design-store'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $queryParams = [
        'delete' => [
            'page' => 'supr_design_store_settings',
            'action' => 'delete',
            'type' => 'color-scheme',
            'tab' => 'color-schemes'
        ],
        'edit' => [
            'page' => 'supr_design_store_settings',
            'action' => 'edit',
            'type' => 'color-scheme',
            'tab' => 'color-schemes'
        ],
        'add' => [
            'page' => 'supr_design_store_settings',
            'action' => 'add',
            'type' => 'color-scheme',
            'tab' => 'color-schemes'
        ]
    ];

    foreach ($designColorSchemes as $designColorScheme) {
        ?>
        <tr>
            <td class="row-title"><?= $designColorScheme->getId(); ?></td>
            <td>
                <a href="<?= $designColorScheme->getPreview(); ?>" target="_blank">
                    <img src="<?= $designColorScheme->getPreview(); ?>" style="width: 100px;"/>
                </a>
            </td>
            <td>
                <p>
                    <a href="?<?= http_build_query(array_merge($queryParams['edit'], ['id' => $designColorScheme->getId()])); ?>">
                        <strong><?= __($designColorScheme->getName(), 'supr-design-store'); ?></strong>
                    </a>
                </p>
                <?php
                $config = json_decode($designColorScheme->getConfig(), true);
                if (is_array($config)) {
                    foreach ($config as $num => $color) {
                        echo "<div style='background-color: {$color};' class='supr-design-store__admin__color-box'></div>";
                    }
                } else {
                    echo "<small>{$designColorScheme->getConfig()}</small>";
                }
                ?>
            </td>
            <td>
                <a class="button button-secondary edit" href="?<?= http_build_query(array_merge($queryParams['edit'], ['id' => $designColorScheme->getId()])); ?>">
                    <span class="dashicons dashicons-edit"></span>
                </a>
                <a class="button button-secondary delete" href="?<?= http_build_query(array_merge($queryParams['delete'], ['id' => $designColorScheme->getId()])); ?>">
                    <span class="dashicons dashicons-post-trash"></span>
                </a>
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
    <tfoot>
    <tr>
        <th class="row-title"><?= __('ID', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Preview', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Name', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Actions', 'supr-design-store'); ?></th>
    </tr>
    </tfoot>
</table>
<div class="postbox">
    <div class="inside">
        <a href="?<?= http_build_query($queryParams['add']); ?>" class="button button-primary"><?= __('Create new element', 'supr-design-store'); ?></a>
    </div>
</div>