<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

/** @var \SuprDesignStore\DesignElement[] $designElements */
use SuprDesignStore\DesignElementManager;
?>
<table class="widefat supr-design-store-table">
    <thead>
    <tr>
        <th class="row-title" style="width: 50px"><?= __('ID', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 120px"><?= __('Preview', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Name', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 150px"><?= __('Type', 'supr-design-store'); ?></th>
        <th class="row-title" style="width: 150px"><?= __('Actions', 'supr-design-store'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $queryParams = [
        'delete' => [
            'page' => 'supr_design_store_settings',
            'action' => 'delete',
            'type' => 'element',
            'tab' => 'elements'
        ],
        'edit' => [
            'page' => 'supr_design_store_settings',
            'action' => 'edit',
            'type' => 'element',
            'tab' => 'elements'
        ],
        'add' => [
            'page' => 'supr_design_store_settings',
            'action' => 'add',
            'type' => 'element',
            'tab' => 'elements'
        ]
    ];

    foreach ($designElements as $designElement) {
        ?>
        <tr class="<?= $designElement->getPublicStatus() === false ? 'super-admin-only' : ''; ?>">
            <td class="row-title"><?= $designElement->getId(); ?></td>
            <td>
                <a href="<?= $designElement->getPreview(); ?>" target="_blank">
                    <img src="<?= $designElement->getPreview(); ?>" style="width: 100px;"/>
                </a>
            </td>
            <td>
                <p>
                    <a href="?<?= http_build_query(array_merge($queryParams['edit'], ['id' => $designElement->getId()])); ?>">
                        <strong><?= __($designElement->getName(), 'supr-design-store'); ?></strong>
                    </a>
                </p>
                <small><?= $designElement->getDescription(); ?></small>
            </td>
            <td><?= DesignElementManager::getElementTypes()[$designElement->getType()]; ?></td>
            <td>
                <a class="button button-secondary edit" href="?<?= http_build_query(array_merge($queryParams['edit'], ['id' => $designElement->getId()])); ?>">
                    <span class="dashicons dashicons-edit"></span>
                </a>
                <a class="button button-secondary delete" href="?<?= http_build_query(array_merge($queryParams['delete'], ['id' => $designElement->getId()])); ?>">
                    <span class="dashicons dashicons-post-trash"></span>
                </a>
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
    <tfoot>
    <tr>
        <th class="row-title"><?= __('ID', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Preview', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Name', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Type', 'supr-design-store'); ?></th>
        <th class="row-title"><?= __('Actions', 'supr-design-store'); ?></th>
    </tr>
    </tfoot>
</table>
<div class="postbox">
    <div class="inside">
        <a href="?<?= http_build_query($queryParams['add']); ?>" class="button button-primary"><?= __('Create new element', 'supr-design-store'); ?></a>
    </div>
</div>