<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$designManager = new \SuprDesignStore\DesignManager();

// Handle request
if (isset($_GET['action'], $_GET['id']) && $_GET['action'] === 'apply') {
    if (!$designManager->applyDesign($_GET['id'])) : ?>
        <div class="notice notice-error">
            <p><?= __('An error occurred while activating the design.', 'supr-design-store') ?></p>
        </div>
    <?php else : ?>
        <div class="notice notice-success">
            <p><?= __('The design has been successfully applied to your store.', 'supr-design-store') ?></p>
        </div>
    <?php endif;
}

$queryParams = ['page' => 'supr_design_store_designs'];

$categories = $designManager->getCategories();

$currentTab = isset($_GET['tab']) ? \sanitize_text_field($_GET['tab']) : strtolower($categories[0] ?? '');

$designs = $designManager->getDesigns($currentTab);

?>
<div class="wrap supr-design-store-template-gallery">
    <h1 class="wp-heading-inline"><?= get_admin_page_title() ?></h1>
    <?php if (count($categories) > 0) : ?>
        <!-- SUPR - Tabs -->
        <div id="elementor-template-library-tabs-wrapper" class="nav-tab-wrapper">
            <?php foreach ($categories as $category) : ?>
                <a class="nav-tab<?= ($currentTab === strtolower($category)) ? ' nav-tab-active' : '' ?>" href="?<?= http_build_query(array_merge($queryParams, ['tab' => strtolower($category)])) ?>"><?= __($category, 'supr-design-store') ?></a>
            <?php endforeach; ?>
        </div>
        <br>
        <!-- SUPR - Tabs -->
    <?php endif; ?>
    <?php if (count($designs) === 0) : ?>
        <p><?= __('Unfortunately, no designs have been added yet.', 'supr-design-store') ?></p>
    <?php else : ?>
        <div class="row supr-design-store-template-gallery-list display-flex">
            <?php foreach ($designs as $design) : ?>
                <div class="supr-design-store-template-gallery-item wu-col-sm-6 wu-col-md-4">
                    <div class="postbox">
                        <div class="inside">
                            <img src="<?= $design->getPreview() ?>">
                            <h4 class="text-uppercase"><?= __($design->getName(), 'supr-design-store') ?></h4>
                            <p>
                                <?= __($design->getDescription(), 'supr-design-store') ?>
                            </p>
                        </div>
                    </div>
                    <div class="postbox postbox-bar">
                        <div class="inside">
                            <button data-preview="<?= $design->getPreview() ?>" class="button button-secondary supr-design-store-template-preview"><?= __('Preview', 'supr-design-store') ?></button>
                            <button data-url="?<?= http_build_query(array_merge($queryParams, ['tab' => $currentTab, 'action' => 'apply', 'id' => $design->getId()])) ?>" data-object-name="<?= __('design', 'supr-design-store') ?>" class="button button-primary supr-design-store-object-activate"><?= __('Activate', 'supr-design-store') ?></button>
                            <?php if ($design->getPublicStatus() === false) : ?>
                                <div class="super-admin-only"><?= __('Super admin only', 'supr-design-store') ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
