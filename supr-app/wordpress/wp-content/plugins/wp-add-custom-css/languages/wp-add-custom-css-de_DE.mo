��          �      L      �  "   �      �               ,  	   A     K  
   S     ^     c  E   j  2   �     �  
   �     �     �       @     �   _  &   	     0     E     X     t  
   �     �     �     �  
   �  J   �  B     	   R     \  	   j     t  
   �  N   �                                        
                          	                                  "WP Add Custom CSS" is a plugin by Add custom CSS rules for this %s Advanced editor Advanced editor layout Available post types CSS rules Credits Custom CSS Dark Enable Enable advanced css editor, including line numbers and code coloring. Enable page specific CSS for the post types below. Main CSS Post types Save Settings Wordpress Add Custom CSS Write here the CSS rules you want to apply to the whole website. MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Project-Id-Version: Shop-add-custom-css
Language: de
 "WP Add Custom CSS" ist ein Plugin von Passe %s-Aussehen an Erweiterter Editor Erweitertes Editoren Layout Verfügbare Beitragsarten CSS Regeln Authoren Angepasstes CSS Dunkel Aktivieren Aktiviere den erweiterten CSS Editor, mit Zeilennummern und farbigem Code. Aktiviere seitenspezifisches CSS für die folgenden Beitragsarten. Haupt CSS Beitragsarten Speichern Einstellungen CSS Editor Definiere hier die CSS-Regeln, welche für die gesamte Webseite gelten sollen. 