<?php

namespace SuprTracking;

/**
 * Class ShortCode
 *
 * @package SuprTracking
 */
class ShortCode
{
    /**
     * Register short codes
     */
    public static function registerShortCode(): void
    {
        \add_shortcode('supr-tracking-disable-link', [self::class, 'getDisableLink']);
    }

    /**
     * Return link to disable tracking with JS
     *
     * @param $params
     * @return string
     */
    public static function getDisableLink($params): string
    {
        $params['text'] = $params['text'] ?? __('Disable tracking', 'supr-tracking');
        $params['success'] = $params['success'] ?? __('Tracking disabled!', 'supr-tracking');

        $returned = '<a href="javascript:document.cookie=\'supr_tracking_tracking_enabled=0;domain=.' . self::getCurrentMainDomain() . ';path=/\'; alert(\'' . \htmlspecialchars($params['success']) . '\');">' . \htmlspecialchars($params['text']) . '</a>';

        return $returned;
    }

    /**
     * Get main domain of current site
     *
     * @return string
     */
    public static function getCurrentMainDomain(): string
    {
        $urlParts = \parse_url(\site_url());
        $domain = $urlParts['host'];
        $domainParts = \explode('.', $domain);

        return $domainParts[\count($domainParts) - 2] . '.' . $domainParts[\count($domainParts) - 1];
    }
}
