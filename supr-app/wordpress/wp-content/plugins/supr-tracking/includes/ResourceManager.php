<?php

namespace SuprTracking;

/**
 * Class ResourceManager
 *
 * @package SuprTracking
 */
class ResourceManager
{
    /**
     * Add files for plugin
     */
    public static function addAdminSettingsResources(): void
    {
        // CSS
        \wp_enqueue_style('supr_tracking_style', SUPR_TRACKING_URL . 'css/supr-tracking.css', false, SUPR_TRACKING_VERSION, 'all');
    }

    /**
     * Add tracking codes from options
     */
    public static function addTrackingCodes(): void
    {
        if (Manager::getInstance()->isTrackingEnabled() === false) {
            return;
        }

        // Because of cookies we cannot include it directly
        \add_filter('supr_cookies_async_scripts_head', [self::class, 'addScriptToHead'], 10, 2);
        \add_filter('supr_cookies_async_scripts_body', [self::class, 'addScriptToBody'], 10, 2);

        // In admin we can include it directly because the user accepted our AGB
        if (\is_admin()) {
            // Tracking for admin panel
            \add_action('admin_head', [__CLASS__, 'printAdminGoogleAnalyticsCodeHead']);
            \add_action('admin_print_footer_scripts', [__CLASS__, 'printAdminGoogleAnalyticsCode']);
        }
    }

    /**
     * Add tracking code to header string
     *
     * @param string $script
     * @param $requestUri
     * @return string
     */
    public static function addScriptToHead($script, $requestUri): string
    {
        if (Manager::getInstance()->isSignUpPage($requestUri)) {
            //Tracking for registration page
            $script .= self::getRegistrationGoogleAnalyticsCodeHead();
        } elseif (Manager::getInstance()->isWpUltimoBillwerkPaymentPage($requestUri)) {
            //Tracking for Wp Ultimo Billwerk payment page
            $script .= self::getWpUltimoBillwerkPaymentGoogleAnalyticsCodeHead();
        } else {
            // Frontend tracking
            $script .= self::getMainGoogleAnalyticsCodeHead();
        }

        return $script;
    }

    /**
     * Add tracking code to body string
     *
     * @param string $script
     * @param $requestUri
     * @return string
     */
    public static function addScriptToBody($script, $requestUri): string
    {
        if (Manager::getInstance()->isSignUpPage($requestUri)) {
            //Tracking for registration page
            $script .= self::getRegistrationGoogleAnalyticsCode();
            $script .= self::getRegistrationFacebookAnalyticsCode();
        } elseif (Manager::getInstance()->isWpUltimoBillwerkPaymentPage($requestUri)) {
            //Tracking for Wp Ultimo Billwerk payment page
            $script .= self::getWpUltimoBillwerkPaymentGoogleAnalyticsCode();
        } else {
            // Frontend tracking
            $script .= self::getMainGoogleAnalyticsCode();
        }

        return $script;
    }

    /**
     * Get frontend GA code (body)
     *
     * @return null|string
     */
    public static function getMainGoogleAnalyticsCode(): ?string
    {
        $options = Options::getInstance();
        return $options->getOption('ga_main_tracking_code');
    }

    /**
     * Get frontend GA code (head)
     *
     * @return null|string
     */
    public static function getMainGoogleAnalyticsCodeHead(): ?string
    {
        $options = Options::getInstance();
        return $options->getOption('ga_main_tracking_code_head');
    }

    /**
     * Print admin GA code (body)
     */
    public static function printAdminGoogleAnalyticsCode(): void
    {
        $options = Options::getInstance();
        echo $options->getOption('ga_admin_tracking_code');
    }

    /**
     * Print frontend GA code (head)
     */
    public static function printAdminGoogleAnalyticsCodeHead(): void
    {
        $options = Options::getInstance();
        echo $options->getOption('ga_admin_tracking_code_head');
    }

    /**
     * Get registration GA code (body)
     *
     * @return null|string
     */
    public static function getRegistrationGoogleAnalyticsCode(): ?string
    {
        $options = Options::getInstance();
        return $options->getOption('ga_registration_tracking_code');
    }

    /**
     * Get registration GA code (head)
     *
     * @return null|string
     */
    public static function getRegistrationGoogleAnalyticsCodeHead(): ?string
    {
        $options = Options::getInstance();

        return $options->getOption('ga_registration_tracking_code_head');
    }

    /**
     * Get WpUltimoBillwerk payment GA code (head)
     *
     * @return null|string
     */
    public static function getWpUltimoBillwerkPaymentGoogleAnalyticsCodeHead(): ?string
    {
        $options = Options::getInstance();

        return $options->getOption('ga_wp_ultimo_billwerk_payment_tracking_code_head');
    }

    /**
     * Get WpUltimoBillwerk payment GA code (body)
     *
     * @return null|string
     */
    public static function getWpUltimoBillwerkPaymentGoogleAnalyticsCode(): ?string
    {
        $options = Options::getInstance();
        return $options->getOption('ga_wp_ultimo_billwerk_payment_tracking_code');
    }

    /**
     * Get registration Facebook code (body)
     *
     * @return null|string
     */
    public static function getRegistrationFacebookAnalyticsCode(): ?string
    {
        $options = Options::getInstance();
        return $options->getOption('facebook_registration_tracking_code');
    }
}
