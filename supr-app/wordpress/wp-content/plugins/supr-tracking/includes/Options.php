<?php

namespace SuprTracking;

/**
 * Class Options
 *
 * @package SuprTracking
 */
class Options
{
    /**
     * @var Options
     */
    public static $instance;

    /**
     * @var string
     */
    private $optionsPrefix = 'supr_tracking_';

    /**
     * @var array
     */
    private $optionsNames = [
        'tracking_enabled' => 1,
        'ga_main_tracking_code' => '',
        'ga_main_tracking_code_head' => '',
        'ga_admin_tracking_code' => '',
        'ga_admin_tracking_code_head' => '',
        'ga_registration_tracking_code' => '',
        'ga_registration_tracking_code_head' => '',
        'ga_wp_ultimo_billwerk_payment_tracking_code' => '',
        'ga_wp_ultimo_billwerk_payment_tracking_code_head' => '',
        'facebook_registration_tracking_code' => ''
    ];

    /**
     * @var array
     */
    private $options;

    /**
     * @return Options
     */
    public static function getInstance(): Options
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->loadOptions();
    }

    /**
     * @param string $optionsName
     * @param        $optionsValue
     * @return mixed
     */
    private function cleanOption($optionsName, $optionsValue)
    {
        $optionsValue = \stripslashes($optionsValue);

        return $optionsValue;
    }

    /**
     * Load all of options in this class
     */
    private function loadOptions(): void
    {
        foreach ($this->optionsNames as $optionsName => $optionsDefaultValue) {
            $wpOptionName = $this->optionsPrefix . $optionsName;
            $this->options[$optionsName] = $this->cleanOption($optionsName, \get_blog_option(1, $wpOptionName, $optionsDefaultValue));
        }
    }

    /**
     * @param string $optionsName
     * @return mixed
     * @throws \Exception
     */
    public function getOption($optionsName)
    {
        if (!isset($this->optionsNames[$optionsName])) {
            throw new \Exception("The option '{$optionsName}' is not registered.");
        }

        return $this->options[$optionsName];
    }

    /**
     * @param string $optionsName
     * @param        $optionsValue
     * @return bool
     * @throws \Exception
     */
    public function setOption(string $optionsName, $optionsValue): bool
    {
        if (!isset($this->optionsNames[$optionsName])) {
            throw new \Exception("The option '{$optionsName}' is not registered.");
        }

        $wpOptionName = $this->optionsPrefix . $optionsName;

        $optionsValue = $this->cleanOption($optionsName, $optionsValue);

        $this->options[$optionsName] = $optionsValue;
        return \update_option($wpOptionName, $optionsValue);
    }
}
