<?php

namespace SuprTracking;

/**
 * Class Menu
 *
 * @package SuprTracking
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        //Add network admin menu page
        \add_action('network_admin_menu', [__CLASS__, 'createNetworkAdminMenu']);

        //Add settings link on network admin plugin page
        \add_filter('network_admin_plugin_action_links_' . SUPR_TRACKING_FILE, [__CLASS__, 'addSettingsPage']);
    }

    /**
     * Add menu to network admin panel
     */
    public static function createNetworkAdminMenu(): void
    {
        \add_menu_page(
            __('Tracking', 'supr-tracking'),
            __('Tracking', 'supr-tracking'),
            'manage_network',
            'supr_tracking_settings',
            [Page::class, 'settingsPage'],
            'dashicons-external'
        );
    }

    /**
     * @param $links
     * @return mixed
     */
    public static function addSettingsPage($links)
    {
        $settings_link = '<a href="admin.php?page=supr_tracking_settings">' . __('Settings', 'supr-tracking') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
