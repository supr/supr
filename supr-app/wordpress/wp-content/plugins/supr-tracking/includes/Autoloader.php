<?php

namespace SuprTracking;

/**
 * Class Autoloader
 *
 * @package SuprTracking
 */
class Autoloader
{
    /**
     * Run autoloader.
     *
     * Register a function as `__autoload()` implementation.
     */
    public static function run(): void
    {
        spl_autoload_register([__CLASS__, 'autoload']);
    }

    /**
     * Autoload.
     *
     * For a given class, check if it exist and load it.
     *
     * @param string $class Class name.
     */
    private static function autoload($class): void
    {
        $realClassName = str_replace(__NAMESPACE__ . '\\', '', $class);

        $filePath = SUPR_TRACKING_PATH . 'includes/' . $realClassName . '.php';

        if (file_exists($filePath) && is_readable($filePath)) {
            require_once $filePath;
        }
    }
}
