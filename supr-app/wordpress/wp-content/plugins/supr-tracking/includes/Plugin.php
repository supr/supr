<?php

namespace SuprTracking;

/**
 * Class Plugin
 *
 * @package SuprTracking
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Admin init
     */
    public function adminInit(): void
    {
        if (isset($_GET['page']) && $_GET['page'] === 'supr_tracking_settings') {
            // Add css and js into footer
            \add_action('admin_enqueue_scripts', [ResourceManager::class, 'addAdminSettingsResources']);
        }
    }

    /**
     * Init
     */
    public function init(): void
    {
        ResourceManager::addTrackingCodes();
        ShortCode::registerShortCode();
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // Add menu
        Menu::addMenu();

        // Load translations
        TextDomain::registerTranslations();

        \add_action('admin_init', [$this, 'adminInit'], 0);
        \add_action('init', [$this, 'init'], 0);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_TRACKING_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::instance();
