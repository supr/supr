<?php

namespace SuprTracking;

/**
 * Class Page
 *
 * @package SuprTracking
 */
class Page
{
    /**
     * Show setting page
     */
    public static function settingsPage(): void
    {
        include SUPR_TRACKING_PATH . 'templates/settings-page.php';
    }
}
