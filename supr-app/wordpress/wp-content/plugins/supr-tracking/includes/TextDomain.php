<?php

namespace SuprTracking;

class TextDomain
{
    public static $domainName = 'supr-tracking';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_TRACKING_DIR_NAME . '/languages/');
    }
}
