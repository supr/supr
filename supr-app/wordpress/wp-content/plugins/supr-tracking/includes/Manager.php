<?php

namespace SuprTracking;

/**
 * Class Manager
 *
 * @package SuprTracking
 */
class Manager
{
    /**
     * @var Manager
     */
    private static $instance;

    /**
     * Manager constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return Manager
     */
    public static function getInstance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Return true if tracking enabled for current user
     *
     * @return bool
     * @throws \Exception
     */
    public function isTrackingEnabled(): bool
    {
        // If the user disabled the tracking
        if (isset($_COOKIE['supr_tracking_tracking_enabled']) && (int)$_COOKIE['supr_tracking_tracking_enabled'] === 0) {
            return false;
        }

        return (bool)((int)Options::getInstance()->getOption('tracking_enabled'));
    }

    /**
     * @param null $requestUri
     * @return bool
     */
    public function isSignUpPage($requestUri = null): bool
    {
        $requestUri = $requestUri ?? $_SERVER['REQUEST_URI'];

        if (\preg_match("#^\/(registration|signup)\/?$#", \parse_url($requestUri)['path'])) {
            return true;
        }

        return false;
    }

    /**
     * @param null $requestUri
     * @return bool
     */
    public function isWpUltimoBillwerkPaymentPage($requestUri = null): bool
    {
        $requestUri = $requestUri ?? $_SERVER['REQUEST_URI'];
        $paymentPageSlug = 'payment';

        // Payment slug can be changed in wp-ultimo
        if (class_exists('\WU_Settings')) {
            $paymentPageSlug = \WU_Settings::get_setting('billwerk_payment_url', 'payment');
        }

        if (\preg_match("#^\/{$paymentPageSlug}\/?$#", \parse_url($requestUri)['path'])) {
            return true;
        }

        return false;
    }
}
