<?php
/**
 * Plugin Name: SUPR - Tracking
 * Plugin URI: https://supr.com
 * Description: Google tracking for WP Multisites
 * Text Domain: supr-tracking
 * Domain Path: /languages
 * Version: 1.1.0
 * Author: SUPR Development
 * License: GPL
 */

define('SUPR_TRACKING_PATH', plugin_dir_path(__FILE__));
define('SUPR_TRACKING_URL', plugins_url('/', __FILE__));
define('SUPR_TRACKING_FILE', plugin_basename(__FILE__));
define('SUPR_TRACKING_DIR_NAME', basename(__DIR__));

define('SUPR_TRACKING_VERSION', '1.1.0');

// Load main Class
require SUPR_TRACKING_PATH . 'includes/Plugin.php';
