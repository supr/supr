<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$tabs = [
    'general' => __('General', 'supr-tracking'),
    'google-analytics' => __('Google Analytics', 'supr-tracking'),
    'facebook-analytics' => __('Facebook Analytics', 'supr-tracking')
];

$currentTab = (isset($_GET['tab']) && isset($tabs[$_GET['tab']])) ? $_GET['tab'] : 'general';
?>

<div class="wrap">
    <h2><?= get_admin_page_title(); ?></h2>
    <h2 class="nav-tab-wrapper">
        <?php foreach ($tabs as $tab => $tabName) : ?>
            <a class="nav-tab<?= $currentTab === $tab ? ' nav-tab-active' : ''; ?>" href="?page=<?= $_GET['page']?>&tab=<?= $tab; ?>"><?= $tabName; ?></a>
        <?php endforeach; ?>
    </h2>

    <?php

    // Include tabs template
    include SUPR_TRACKING_PATH . "templates/settings-tabs/{$currentTab}.php";
    ?>
</div>