<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$options = \SuprTracking\Options::getInstance();

// Handle request
if (isset($_POST['supr_tracking_options'])) {
    foreach ($_POST['supr_tracking_options'] as $optionsName => $optionsValue) {
        $options->setOption($optionsName, $optionsValue);
    }
}
?>
<form action="" method="POST">
    <table class="form-table">
        <tr>
            <th colspan="2"><?= __('Main tracking code', 'supr-tracking') ?></th>
        </tr>
        <tr>
            <td>
                <label for="supr_tracking_option_ga_main_tracking_code_head"><?= __('HEAD', 'supr-tracking'); ?></label>
                <br>
                <small class="gray"><?= __('It will be placed in &lt;head&gt; on frontend', 'supr-tracking'); ?></small>
            </td>
            <td>
                <textarea id="supr_tracking_option_ga_main_tracking_code_head" name="supr_tracking_options[ga_main_tracking_code_head]"><?= $options->getOption('ga_main_tracking_code_head') ?? ''; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <label for="supr_tracking_option_ga_main_tracking_code"><?= __('BODY', 'supr-tracking'); ?></label>
                <br>
                <small class="gray"><?= __('It will be placed in &lt;footer&gt; on frontend', 'supr-tracking'); ?></small>
            </td>
            <td>
                <textarea id="supr_tracking_option_ga_main_tracking_code" name="supr_tracking_options[ga_main_tracking_code]"><?= $options->getOption('ga_main_tracking_code') ?? ''; ?></textarea>
            </td>
        </tr>
        <tr>
            <th colspan="2"><?= __('Tracking code for admin panel', 'supr-tracking') ?></th>
        </tr>
        <tr>
            <td>
                <label for="supr_tracking_option_ga_admin_tracking_code_head"><?= __('HEAD', 'supr-tracking'); ?></label>
                <br>
                <small class="gray"><?= __('It will be placed in &lt;head&gt; on admin pages', 'supr-tracking'); ?></small>
            </td>
            <td>
                <textarea id="supr_tracking_option_ga_admin_tracking_code_head" name="supr_tracking_options[ga_admin_tracking_code_head]"><?= $options->getOption('ga_admin_tracking_code_head') ?? ''; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <label for="supr_tracking_option_ga_admin_tracking_code"><?= __('BODY', 'supr-tracking'); ?></label>
                <br>
                <small class="gray"><?= __('It will be placed in &lt;footer&gt; on admin pages', 'supr-tracking'); ?></small>
            </td>
            <td>
                <textarea id="supr_tracking_option_ga_admin_tracking_code" name="supr_tracking_options[ga_admin_tracking_code]"><?= $options->getOption('ga_admin_tracking_code') ?? ''; ?></textarea>
            </td>
        </tr>
        <tr>
            <th colspan="2"><?= __('Tracking code for registration', 'supr-tracking') ?></th>
        </tr>
        <tr>
            <td>
                <label for="supr_tracking_option_ga_registration_tracking_code_head"><?= __('HEAD', 'supr-tracking'); ?></label>
                <br>
                <small class="gray"><?= __('It will be placed in &lt;head&gt; on registration page', 'supr-tracking'); ?></small>
            </td>
            <td>
                <textarea id="supr_tracking_option_ga_registration_tracking_code_head" name="supr_tracking_options[ga_registration_tracking_code_head]"><?= $options->getOption('ga_registration_tracking_code_head') ?? ''; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <label for="supr_tracking_option_ga_registration_tracking_code"><?= __('BODY', 'supr-tracking'); ?></label>
                <br>
                <small class="gray"><?= __('It will be placed in &lt;footer&gt; on registration page', 'supr-tracking'); ?></small>
            </td>
            <td>
                <textarea id="supr_tracking_option_ga_registration_tracking_code" name="supr_tracking_options[ga_registration_tracking_code]"><?= $options->getOption('ga_registration_tracking_code') ?? ''; ?></textarea>
            </td>
        </tr>
        <tr>
            <th colspan="2"><?= __('Tracking code for WP-Ultimo-Billwerk payment page', 'supr-tracking') ?></th>
        </tr>
        <tr>
            <td>
                <label for="supr_tracking_option_ga_wp_ultimo_billwerk_payment_tracking_code_head"><?= __('HEAD', 'supr-tracking'); ?></label>
                <br>
                <small class="gray"><?= __('It will be placed in &lt;head&gt; on WP-Ultimo-Billwerk payment page', 'supr-tracking'); ?></small>
            </td>
            <td>
                <textarea id="supr_tracking_option_ga_wp_ultimo_billwerk_payment_tracking_code_head" name="supr_tracking_options[ga_wp_ultimo_billwerk_payment_tracking_code_head]"><?= $options->getOption('ga_wp_ultimo_billwerk_payment_tracking_code_head') ?? ''; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>
                <label for="supr_tracking_option_ga_wp_ultimo_billwerk_payment_tracking_code"><?= __('BODY', 'supr-tracking'); ?></label>
                <br>
                <small class="gray"><?= __('It will be placed in &lt;footer&gt; on WP-Ultimo-Billwerk payment page', 'supr-tracking'); ?></small>
            </td>
            <td>
                <textarea id="supr_tracking_option_ga_wp_ultimo_billwerk_payment_tracking_code" name="supr_tracking_options[ga_wp_ultimo_billwerk_payment_tracking_code]"><?= $options->getOption('ga_wp_ultimo_billwerk_payment_tracking_code') ?? ''; ?></textarea>
            </td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>
