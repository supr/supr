<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$options = \SuprTracking\Options::getInstance();

// Handle request (we check tmp_post_value because other one is not sent, if it is not checked)
if (isset($_POST['supr_tracking_options'])) {
    // Unset tmp value
    unset($_POST['supr_tracking_options']['tmp_post_value']);

    // Sanitize
    $newOptions = array_map('\sanitize_text_field', $_POST['supr_tracking_options']);

    // Parse checkbox
    $newOptions['tracking_enabled'] = $newOptions['tracking_enabled'] ? (int)$newOptions['tracking_enabled'] : 0;

    // Save options
    foreach ($newOptions as $optionsName => $optionsValue) {
        $options->setOption($optionsName, $optionsValue);
    }
}
?>
<form action="" method="POST">
    <input type="hidden" name="supr_tracking_options[tmp_post_value]" value="1"/>
    <table class="form-table">
        <tr>
            <td><label for="supr_tracking_option_tracking_enabled"><?= __('Enable tracking', 'supr-tracking'); ?></label></td>
            <td><input type="checkbox" id="supr_tracking_option_tracking_enabled" name="supr_tracking_options[tracking_enabled]" value="1" <?= (int)$options->getOption('tracking_enabled') === 1 ? ' checked' : ''; ?>/></td>
        </tr>
        <tr>
            <td><label for="supr_tracking_diasable_short_code">To allow the user to disable tracking, you can use this short code: </label></td>
            <td><input type="text" id="supr_tracking_diasable_short_code" readonly value='[supr-tracking-disable-link text="Disable tracking" success="Tracking disabled!"]'></td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>
