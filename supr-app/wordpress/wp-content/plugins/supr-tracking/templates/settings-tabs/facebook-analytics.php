<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();

$options = \SuprTracking\Options::getInstance();

// Handle request
if (isset($_POST['supr_tracking_options'])) {
    foreach ($_POST['supr_tracking_options'] as $optionsName => $optionsValue) {
        $options->setOption($optionsName, $optionsValue);
    }
}
?>
<form action="" method="POST">
    <table class="form-table">
        <tr>
            <td>
                <label for="supr_tracking_option_facebook_registration_tracking_code"><?= __('Tracking code for registration', 'supr-tracking'); ?></label>
                <br>
                <small class="gray"><?= __('It will be placed in &lt;footer&gt; on registration pages', 'supr-tracking'); ?></small>
            </td>
            <td>
                <textarea id="supr_tracking_option_facebook_registration_tracking_code" name="supr_tracking_options[facebook_registration_tracking_code]"><?= $options->getOption('facebook_registration_tracking_code') ?? ''; ?></textarea>
            </td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>