# Description

This plugin parses css links and replaces it with own storaged css.

# How it works

1. We found google font: https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,300,400,600&subset=latin,latin-ext

2.  We parse it and load all needet font-files from this API: https://google-webfonts-helper.herokuapp.com/api/fonts/open-sans

    We load all fonts all needed types: 

    1. Fonts formats: **eot, woff, woff2, svg, ttf**

    2. Types to load (from the example above): **300italic, 400italic, 600italic, 300, 400, 600**

    And in this case we will have 30 font files.

3. We generate own css and save it to: https://mysupr.watch/wp-content/uploads/supr-google-fonts/css/open-sans-300italic-400italic-600italic-300-400-600-subset-latin-latin-ext.css

    ```bash
    /* 300 */
    @font-face {
      font-family: 'Open Sans';
      font-style: normal;
      font-weight: 300;
      src: url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-normal.eot'); /* IE9 Compat Modes */
      src: local('Open Sans Light'), local('OpenSans-Light'),
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-normal.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-normal.woff2') format('woff2'), /* Super Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-normal.woff') format('woff'), /* Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-normal.ttf') format('truetype'), /* Safari, Android, iOS */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-normal.svg') format('svg'); /* Legacy iOS */
    }
    /* 300italic */
    @font-face {
      font-family: 'Open Sans';
      font-style: italic;
      font-weight: 300;
      src: url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-italic.eot'); /* IE9 Compat Modes */
      src: local('Open Sans Light Italic'), local('OpenSans-LightItalic'),
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-italic.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-italic.woff2') format('woff2'), /* Super Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-italic.woff') format('woff'), /* Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-italic.ttf') format('truetype'), /* Safari, Android, iOS */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-300-italic.svg') format('svg'); /* Legacy iOS */
    }
    /* regular */
    @font-face {
      font-family: 'Open Sans';
      font-style: normal;
      font-weight: 400;
      src: url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-normal.eot'); /* IE9 Compat Modes */
      src: local('Open Sans Regular'), local('OpenSans-Regular'),
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-normal.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-normal.woff2') format('woff2'), /* Super Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-normal.woff') format('woff'), /* Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-normal.ttf') format('truetype'), /* Safari, Android, iOS */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-normal.svg') format('svg'); /* Legacy iOS */
    }
    /* italic */
    @font-face {
      font-family: 'Open Sans';
      font-style: italic;
      font-weight: 400;
      src: url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-italic.eot'); /* IE9 Compat Modes */
      src: local('Open Sans Italic'), local('OpenSans-Italic'),
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-italic.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-italic.woff2') format('woff2'), /* Super Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-italic.woff') format('woff'), /* Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-italic.ttf') format('truetype'), /* Safari, Android, iOS */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-400-italic.svg') format('svg'); /* Legacy iOS */
    }
    /* 600 */
    @font-face {
      font-family: 'Open Sans';
      font-style: normal;
      font-weight: 600;
      src: url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-normal.eot'); /* IE9 Compat Modes */
      src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'),
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-normal.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-normal.woff2') format('woff2'), /* Super Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-normal.woff') format('woff'), /* Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-normal.ttf') format('truetype'), /* Safari, Android, iOS */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-normal.svg') format('svg'); /* Legacy iOS */
    }
    /* 600italic */
    @font-face {
      font-family: 'Open Sans';
      font-style: italic;
      font-weight: 600;
      src: url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-italic.eot'); /* IE9 Compat Modes */
      src: local('Open Sans SemiBold Italic'), local('OpenSans-SemiBoldItalic'),
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-italic.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-italic.woff2') format('woff2'), /* Super Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-italic.woff') format('woff'), /* Modern Browsers */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-italic.ttf') format('truetype'), /* Safari, Android, iOS */
           url('https://mysupr.watch/wp-content/uploads/supr-google-fonts/fonts/open-sans-600-italic.svg') format('svg'); /* Legacy iOS */
    }
    ```

4. This new css url will be used instead of google css.

# Usecases for undestanding and testing

## Usecase 1

**Google css:** https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,300,400,600&subset=latin,latin-ext

**API:** https://google-webfonts-helper.herokuapp.com/api/fonts/open-sans

**Our css:** https://mysupr.watch/wp-content/uploads/supr-google-fonts/css/open-sans-300italic-400italic-600italic-300-400-600-subset-latin-latin-ext.css

## Usecase 2

**Google css:** https://fonts.googleapis.com/css?family=Noto+Serif%3A400%2C400i%2C700%2C700i

**API:** https://google-webfonts-helper.herokuapp.com/api/fonts/noto-serif

**Our css:** https://mysupr.watch/wp-content/uploads/supr-google-fonts/css/noto-serif-400-400i-700-700i.css
