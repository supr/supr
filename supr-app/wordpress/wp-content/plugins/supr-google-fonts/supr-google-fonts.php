<?php
/**
 * Plugin Name: SUPR - Google Fonts
 * Plugin URI: https://supr.com
 * Description: Replace google fonts with own hosted fonts
 * Text Domain: supr-google-fonts
 * Domain Path: /languages
 * Version: 1.0
 * Author: SUPR Development
 * License: GPL
 */

define('SUPR_GOOGLE_FONTS_PATH', plugin_dir_path(__FILE__));
define('SUPR_GOOGLE_FONTS_URL', plugins_url('/', __FILE__));
define('SUPR_GOOGLE_FONTS_FILE', plugin_basename(__FILE__));
define('SUPR_GOOGLE_FONTS_DIR_NAME', basename(__DIR__));


define('SUPR_GOOGLE_FONTS_VERSION', '1.1.1');

// Load main Class
require SUPR_GOOGLE_FONTS_PATH . 'includes/Plugin.php';
