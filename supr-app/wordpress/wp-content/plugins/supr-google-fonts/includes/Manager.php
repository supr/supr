<?php

namespace SuprGoogleFonts;

/**
 * Class Manager
 *
 * @package SuprGoogleFonts
 */
class Manager
{
    /**
     * @var null
     */
    public static $instance;

    private $googleFontRegexp = '/[\'"](https?:)?\/\/fonts\.googleapis\.com\/css\?family=(\S*)[\'"]/i';

    /**
     * @return Manager
     */
    public static function instance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Check version of Plugin and do some actions
     */
    public function checkVersion(): void
    {
        $currentVersion = \get_blog_option(1, 'supr-google-fonts-plugin-version');

        $actualVersion = SUPR_GOOGLE_FONTS_VERSION;

        if ($currentVersion !== $actualVersion) {
            // Clear old css files
            $cssUploadsFolder = $this->getUploadsFolder('css');
            $files = glob($cssUploadsFolder . '*'); // get all file names

            foreach ($files as $file) { // iterate files
                if (is_file($file)) {
                    unlink($file); // delete file
                }
            }

            \update_blog_option(1, 'supr-google-fonts-plugin-version', $actualVersion);
        }
    }

    public function replaceGoogleFonts(): void
    {
        \add_filter('style_loader_tag', [$this, 'replaceGoogleFontsInHtml'], 10, 4);
    }

    /**
     * @param $html
     * @param $handle
     * @param $href
     * @param $media
     * @return mixed
     */
    public function replaceGoogleFontsInHtml($html, $handle, $href, $media)
    {
        return preg_replace_callback(
            $this->googleFontRegexp,
            function ($matches) {
                // Get font settings from url: Open+Sans:300italic,400italic,600italic,300,400,600&subset=latin,latin-ext
                $familyGetParam = \urldecode($matches[2]);

                $selfHostedFontCss = Manager::instance()->storeGoogleFontCss($familyGetParam);

                // Return if successfully stored
                return $selfHostedFontCss ? "'{$selfHostedFontCss}'" : $matches[0];
            },
            $html
        );
    }

    /**
     * Download and storage google font
     *
     * @param $familyGetParam
     * @return string|null
     */
    private function storeGoogleFontCss($familyGetParam): ?string
    {
        $fileSlug = $this->slugify($familyGetParam);

        // Fix: File name too long #S2-567
        if (\strlen($fileSlug) > 250) {
            $fileSlug = \sha1($fileSlug);
        }

        $cssFileName = $fileSlug . '.css';

        $targetPath = $this->getUploadsFolder('css') . $cssFileName;

        // Load fonts and create css
        if (!\file_exists($targetPath)) {
            $fonts = $this->parseUrlToFonts($familyGetParam);
            $css = '';

            foreach ($fonts as $font) {
                $googleFont = ApiManager::getFont($font['name']);

                if ($googleFont === null) {
                    return null;
                }

                foreach ($googleFont['variants'] as $variant) {
                    if (\in_array($variant['id'], $font['variants'], true)) {
                        // Try to get all possible formats
                        $fontFiles = [
                            'eot' => [
                                'file' => $this->storeGoogleFont($variant['eot'], 'eot', $variant['fontFamily'], $variant['fontStyle'], $variant['fontWeight']),
                                'filePostfix' => '?#iefix',
                                'format' => 'embedded-opentype',
                                'comment' => '/* IE6-IE8 */',
                            ],
                            'woff' => [
                                'file' => $this->storeGoogleFont($variant['woff'], 'woff', $variant['fontFamily'], $variant['fontStyle'], $variant['fontWeight']),
                                'filePostfix' => '',
                                'format' => 'woff',
                                'comment' => '/* Modern Browsers */'
                            ],
                            'woff2' => [
                                'file' => $this->storeGoogleFont($variant['woff2'], 'woff2', $variant['fontFamily'], $variant['fontStyle'], $variant['fontWeight']),
                                'filePostfix' => '',
                                'format' => 'woff2',
                                'comment' => '/* Super Modern Browsers */'
                            ],
                            'svg' => [
                                'file' => $this->storeGoogleFont($variant['svg'], 'svg', $variant['fontFamily'], $variant['fontStyle'], $variant['fontWeight']),
                                'filePostfix' => '',
                                'format' => 'svg',
                                'comment' => '/* Legacy iOS */'
                            ],
                            'ttf' => [
                                'file' => $this->storeGoogleFont($variant['ttf'], 'ttf', $variant['fontFamily'], $variant['fontStyle'], $variant['fontWeight']),
                                'filePostfix' => '',
                                'format' => 'truetype',
                                'comment' => '/* Safari, Android, iOS */'
                            ],
                        ];

                        // For support most popular browsers we need woff and woff2
                        if ($fontFiles['woff'] === null || $fontFiles['woff2'] === null) {
                            return null;
                        }

                        // Create locals in css string format
                        $locals = [];
                        if (\is_array($variant['local'])) {
                            foreach ($variant['local'] as $local) {
                                $locals[] = "local('{$local}')";
                            }
                        }

                        // Default value if nothing exists
                        if (\count($locals) === 0) {
                            $locals[] = "local('')";
                        }

                        // Create src lines
                        $src = 'src: ' . implode(', ', $locals);
                        foreach ($fontFiles as $fontType => $fontOptions) {
                            if ($fontOptions['file'] === null) {
                                continue;
                            }
                            $src .= ',' . PHP_EOL . "                           url('{$fontOptions['file']}{$fontOptions['filePostfix']}') format('{$fontOptions['format']}')";
                        }
                        $src .= ';';

                        // Create eot src line
                        $eotSrc = '';
                        if ($fontFiles['eot']['file'] !== null) {
                            $eotSrc = "src: url('{$fontFiles['eot']['file']}'); /* IE9 Compat Modes */";
                        }

                        // Add to css
                        $css .= "
                    /* {$variant['id']} */
                    @font-face {
                      font-family: {$variant['fontFamily']};
                      font-style: {$variant['fontStyle']};
                      font-weight: {$variant['fontWeight']};
                      {$eotSrc}
                      {$src}
                    }";
                    }
                }
            }

            $css .= "\n/* Generated with [SUPR - Google Fonts] at [" . (new \DateTime())->format('d.m.Y H:i:s') . "] from [{$familyGetParam}] */";

            file_put_contents($targetPath, $css);

            \do_action('supr-clear-cache');
        }

        return $this->getSrc($cssFileName, 'css');
    }

    /**
     * Save font on own storage and return a link to it
     *
     * @param $src
     * @param $ext
     * @param $fontFamily
     * @param $fontStyle
     * @param $fontWeight
     * @return string|null
     */
    private function storeGoogleFont($src, $ext, $fontFamily, $fontStyle, $fontWeight): ?string
    {
        // Simple validation
        if (empty($src)) {
            return null;
        }

        $fileName = $this->slugify("{$fontFamily} {$fontWeight} {$fontStyle}") . '.' . $ext;

        $targetPath = $this->getUploadsFolder() . $fileName;

        if (!\file_exists($targetPath)) {
            $data = file_get_contents($src, false);

            if (!$data) {
                error_log('[SUPR GOOGLE FONTS] Cannot load font from google. Source: ' . $src);

                return null;
            }

            file_put_contents($targetPath, $data);
        }

        return $this->getSrc($fileName);
    }

    /**
     * Parse url to font: from
     * "Open+Sans:300italic,400italic,600italic,300,400,600|Aclonica:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic&subset=latin,latin-ext"
     * to array
     *
     * @param $urlPath
     * @return array
     */
    private function parseUrlToFonts($urlPath): array
    {
        $fonts = [];

        $fontExample = [
            'name' => '',
            'variants' => []
        ];

        $parts = \explode('&', $urlPath);

        $fontStrings = \explode('|', $parts[0]);

        foreach ($fontStrings as $fontString) {
            $font = $fontExample;

            // Get name
            $tmp = \explode(':', $fontString);
            $font['name'] = $this->slugify($tmp[0]);

            // Get types
            if (isset($tmp[1])) {
                $font['variants'] = preg_split('/[,;]/', $tmp[1]);
            } else {
                // Add variant normal = 400
                $font['variants'] = ['400'];
            }

            // Type from API and type from google are different. Add it too!
            // 400 = regular, 400i = italic, 700i = 700italic, ...
            $additionalVariants = [];
            foreach ($font['variants'] as $variant) {
                switch ($variant) {
                    case '400':
                        $additionalVariants[] = 'regular';
                        break;
                    case '400i':
                    case '400italic':
                        $additionalVariants[] = 'italic';
                        break;
                    case '100i':
                    case '200i':
                    case '300i':
                    case '500i':
                    case '600i':
                    case '700i':
                    case '800i':
                    case '900i':
                        $additionalVariants[] = \str_replace('i', 'italic', $variant);
                        break;
                }
            }

            $font['variants'] = \array_merge($font['variants'], $additionalVariants);

            $fonts[] = $font;
        }

        return $fonts;
    }

    /**
     * Returns the uploads directory
     *
     * @param string $type
     * @return string
     */
    private function getUploadsFolder($type = 'fonts'): string
    {
        $path = \defined('ABSPATH') ? ABSPATH : \get_home_path();
        $path .= 'wp-content/uploads/supr-google-fonts';

        if (!\is_dir($path)) {
            \mkdir($path);
            \mkdir($path . '/css');
            \mkdir($path . '/fonts');
        }

        return $path . '/' . $type . '/';
    }

    /**
     * Return link to font
     *
     * @param        $fontName
     * @param string $type
     * @return string
     */
    private function getSrc($fontName, $type = 'fonts'): string
    {
        return get_home_url(1) . '/wp-content/uploads/supr-google-fonts/' . $type . '/' . $fontName;
    }

    /**
     * Simple slugify function
     *
     * @param $text
     * @return mixed|string
     */
    private function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return null;
        }

        return $text;
    }
}
