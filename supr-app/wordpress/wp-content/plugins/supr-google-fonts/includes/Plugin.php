<?php

namespace SuprGoogleFonts;

/**
 * Class Plugin
 *
 * @package SuprGoogleFonts
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        $manager = Manager::instance();
        \add_action('admin_init', [$manager, 'checkVersion']);
        $manager->replaceGoogleFonts();
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_GOOGLE_FONTS_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::instance();
