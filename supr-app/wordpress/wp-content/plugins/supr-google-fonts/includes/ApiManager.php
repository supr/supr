<?php

namespace SuprGoogleFonts;

/**
 * Class ApiManager
 *
 * @package SuprGoogleFonts
 */
class ApiManager
{
    private static $url = 'https://google-webfonts-helper.herokuapp.com/api/fonts/';

    /**
     * Get Font
     *
     * @param $font
     * @return array|mixed|object
     */
    public static function getFont($font)
    {
        $route = self::$url . $font;

        $ch = \curl_init($route);

        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 3,
            CURLOPT_AUTOREFERER => true,
            CURLOPT_CONNECTTIMEOUT => 3,
            CURLOPT_TIMEOUT => 3
        );

        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);
        curl_close($ch);

        $returned = \json_decode($content, true);

        if (!\is_array($returned)) {
            $actualLink = "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
            error_log('[SUPR GOOGLE FONTS] Cannot get font "' . $font . '" from ' . $route . "\nShop-URL: {$actualLink}" . "\nResponse: " . print_r($content, true));
            return null;
        }

        return $returned;
    }
}

Plugin::instance();
