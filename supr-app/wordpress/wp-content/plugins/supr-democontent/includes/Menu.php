<?php

namespace SuprDemocontent;

/**
 * Class Menu
 *
 * @package SuprCopOnboarding
 */
class Menu
{
    /**
     * Add menu elements for plugin
     */
    public static function addMenu(): void
    {
        // Add menu page
        add_action('network_admin_menu', [__CLASS__, 'createMenu']);

        // Add settings link on plugin page
        add_filter('network_admin_plugin_action_links_' . SUPR_DEMOCONTENT_FILE, [__CLASS__, 'addSettingsPage']);
    }

    /**
     * Add menu to admin panel
     */
    public static function createMenu(): void
    {
        add_menu_page(
            __('Democontent', 'supr-cop-onboarding'),
            __('Democontent', 'supr-cop-onboarding'),
            'manage_network',
            'supr_democontent_options',
            [Page::class, 'settingsPage'],
            'dashicons-media-text'
        );
    }

    /**
     * @param array $links
     * @return mixed
     */
    public static function addSettingsPage($links): array
    {
        $settings_link = '<a href="admin.php?page=supr_democontent_options">' . __('Settings', 'supr-democontent') . '</a>';
        array_unshift($links, $settings_link);

        return $links;
    }
}
