<?php

namespace SuprDemocontent;

/**
 * Class ResourceManager
 *
 * @package SuprDemocontent
 */
class ResourceManager
{
    /**
     * Register files for plugin
     */
    public static function addResources(): void
    {
        \wp_register_style('supr_democontent_style', SUPR_DEMOCONTENT_URL . 'css/supr-democontent.css', [], SUPR_DEMOCONTENT_VERSION, 'all');
        \wp_register_script('supr_democontent_js', SUPR_DEMOCONTENT_URL . 'js/supr-democontent.js', ['jquery'], SUPR_DEMOCONTENT_VERSION, true);
        // Use the registered jquery and style above
        \add_action('admin_enqueue_scripts', [__CLASS__, 'enqueueFiles']);
    }

    /**
     * Add files
     */
    public static function enqueueFiles(): void
    {
        \wp_enqueue_style('supr_democontent_style');
        \wp_enqueue_script('supr_democontent_js');
    }

    /**
     * Add js variables to js script
     */
    public static function addJsVariables(): void
    {
        $values = [
            'remove_button_text' => __('Remove Demo Content', 'supr-democontent'),
            'remove_dialog_title' => __('Remove Demo Content', 'supr-democontent'),
            'remove_dialog_html' => self::createDialogHtml(),
            'remove_dialog_apply_button_text' => __('Delete selected content', 'supr-democontent'),
            'remove_dialog_cancel_button_text' => __('Cancel', 'supr-democontent'),
            'success_dialog_title' => __('Done!', 'supr-democontent'),
            'success_dialog_text' => __('We have deleted all you checked :)', 'supr-democontent'),
            'fail_dialog_title' => __('Fail!', 'supr-democontent'),
            'fail_dialog_text' => __('Something went wrong :(', 'supr-democontent'),
            'has_demo_products' => Manager::instance()->hasDemoProducts(),
            'has_demo_images' => Manager::instance()->hasDemoImages(),
            'has_demo_posts' => Manager::instance()->hasDemoBlogPages(),
            'has_demo_categories' => Manager::instance()->hasDemoCategories(),
            'loading_dialog_title' => __('Processing...', 'supr-democontent'),
            'loading_dialog_text' => __('We are removing the marked items...', 'supr-democontent')
        ];

        \wp_localize_script('supr_democontent_js', 'supr_democontent_values', $values);
    }

    /**
     * @return string
     */
    public static function createDialogHtml(): string
    {
        $demoProducts = Manager::instance()->getDemoProducts();

        $demoImages = Manager::instance()->getDemoImages();

        $demoBlogPages = Manager::instance()->getDemoBlogPages();

        $demoCategories = Manager::instance()->getDemoCategories();

        //var_dump($demoImages); exit;

        $html = '<p>' . __(
            'Here you can delete all the examples with which we filled your shop preliminarily. Watch out: the deletion can not be undone. If you modified example content, make sure not to delete it here.',
            'supr-democontent'
        ) . '</p>';

        $html .= '<button class="supr-democontent-check-all button" data-checked="0">' . __('Un-/Check all', 'supr-democontent') . '</button>';

        // Make list of products
        if (\count($demoProducts) > 0) {
            $html .= '<h4 class="supr-democontent-list-title">' . __('Products', 'supr-democontent') . '</h4>';
            foreach ($demoProducts as $demoProduct) {
                $html .= "<div class='supr-democontent-product-list-item'><label><input type='checkbox' class='supr-democontent-product' data-id='{$demoProduct->get_id()}'> {$demoProduct->get_title()}</label> <a target='_blank' href='" . get_post_permalink(
                    $demoProduct->get_id()
                ) . "'>" . __('View', 'supr-democontent') . "</a></div>";
            }
        }

        // Make list of images
        if (\count($demoImages) > 0) {
            $html .= '<h4 class="supr-democontent-list-title">' . __('Images', 'supr-democontent') . '</h4>';
            foreach ($demoImages as $productId => $images) {
                /** @var \WP_Post $image */
                foreach ($images as $image) {
                    $imageUrl = wp_get_attachment_image_src($image->ID, [150, 150]);
                    // Feature for Piwi (it doesn't work by Piwi)
                    //$imageUrl = [0 => 'https://plan.' . DOMAIN_CURRENT_SITE . '/wp-content/uploads/sites/9/2018/09/beispielprodukt_1-2-150x150.jpg'];
                    $imageUrl = \is_array($imageUrl) ? $imageUrl[0] : '';
                    $html .= "<label class='supr-democontent-image-list-item'><input type='checkbox' class='supr-democontent-image' data-parent='{$productId}' data-id='{$image->ID}'> <img src='{$imageUrl}' alt='{$image->post_name}'></label>";
                }
            }
        }

        if (\count($demoCategories) > 0) {
            $html .= '<h4 class="supr-democontent-list-title">' . __('Categories', 'supr-democontent') . '</h4>';
            foreach ($demoCategories as $demoCategory) {
                $html .= "<div class='supr-democontent-category-list-item'><label><input type='checkbox' class='supr-democontent-category' data-id='{$demoCategory->term_id}'> {$demoCategory->name}</label> <a target='_blank' href='" . get_category_link(
                    $demoCategory->term_id
                ) . "'>" . __('View', 'supr-democontent') . "</a></div>";
            }
        }

        if (\count($demoBlogPages) > 0) {
            $html .= '<h4 class="supr-democontent-list-title">' . __('Blog Entries', 'supr-democontent') . '</h4>';
            foreach ($demoBlogPages as $demoBlogPage) {
                $html .= "<div class='supr-democontent-post-list-item'><label><input type='checkbox' class='supr-democontent-post' data-id='{$demoBlogPage->ID}'> {$demoBlogPage->post_title}</label> <a target='_blank' href='" . get_post_permalink(
                    $demoBlogPage->ID
                ) . "'>" . __('View', 'supr-democontent') . "</a></div>";
            }
        }

        return $html;
    }
}
