<?php

namespace SuprDemocontent;

class AjaxRestEndpoint
{
    /**
     * Add endpoints to get colors and fonts settings
     */
    public static function addEndPoints()
    {
        add_action('wp_ajax_supr-democontent-remove-posts', array(self::class, 'removePosts'));
    }

    /**
     * Get color scheme from blog option
     *
     * @return void
     */
    public static function removePosts(): void
    {
        $postIds = $_POST['postIds'] ?? null;

        $categoryIds = $_POST['categoryIds'] ?? null;

        $returned = ['success' => true, 'errors' => []];

        if (\is_array($postIds)) {
            // We can have one picture in different products
            $postIds = array_unique($postIds);

            $deleted = 0;
            foreach ($postIds as $postId) {
                $checkDeleted = wp_delete_post($postId);
                $deleted += $checkDeleted ? 1 : 0;
                if (!$checkDeleted) {
                    $returned['errors'][] = sprintf(__('The element #%d are not deleted.', 'supr-democontent'), $postId);
                }
            }
            $returned['success'] = $returned['success'] && ($deleted === \count($postIds));
        }

        if (\is_array($categoryIds)) {
            // Just in case
            $categoryIds = array_unique($categoryIds);
            $deleted = 0;
            foreach ($categoryIds as $categoryId) {
                $checkDeleted = wp_delete_term($categoryId, 'product_cat');
                $deleted += $checkDeleted ? 1 : 0;
                if (!$checkDeleted) {
                    $returned['errors'][] = sprintf(__('The category #%d are not deleted.', 'supr-democontent'), $categoryId);
                }
            }
            $returned['success'] = $returned['success'] && ($deleted === \count($categoryIds));
        }

        echo json_encode($returned);
        wp_die();
    }
}
