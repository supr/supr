<?php

namespace SuprDemocontent;

/**
 * Class Page
 *
 * @package SuprDemocontent
 */
class Page
{
    /**
     * Show setting page
     */
    public static function settingsPage(): void
    {
        include SUPR_DEMOCONTENT_PATH . 'templates/settings-page.php';
    }
}
