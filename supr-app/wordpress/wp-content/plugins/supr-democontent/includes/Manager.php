<?php

namespace SuprDemocontent;

/**
 * Class Manager
 *
 * @package SuprDemocontent
 */
class Manager
{
    /**
     * @var Manager
     */
    public static $instance;

    /**
     * @var string Option name in DB
     */
    private static $blog_option_has_demo_content = 'supr_democontent_has_democontent';

    /**
     * @var \WC_Product[]
     */
    private $demoProducts;

    /**
     * @var \WP_Post[]
     */
    private $demoImages;

    /**
     * @var \WP_Post[]
     */
    private $demoBlogPages;

    /**
     * @var \WP_Term[]
     */
    private $demoCategories;

    /**
     * @var array
     */
    private $demoProductIds = [];

    /**
     * @var array
     */
    private $demoBlogPageIds = [];

    /**
     * @var array
     */
    private $demoCategoryIds = [];

    /**
     * Pictures ids in main gallery
     *
     * @var array
     */
    private $demoImageIds = [];

    /**
     * Manager constructor.
     */
    private function __construct()
    {
        $options = get_blog_option(1, 'supr_democontent_options', []);

        if (isset($options['demo_products_ids'])) {
            $this->demoProductIds = array_map('trim', explode(',', $options['demo_products_ids']));
        }

        if (isset($options['demo_pages_ids'])) {
            $this->demoBlogPageIds = array_map('trim', explode(',', $options['demo_pages_ids']));
        }

        if (isset($options['demo_categories_ids'])) {
            $this->demoCategoryIds = array_map('trim', explode(',', $options['demo_categories_ids']));
        }

        if (isset($options['demo_images_ids'])) {
            $this->demoImageIds = array_map('trim', explode(',', $options['demo_images_ids']));
        }
    }

    /**
     * @return array
     */
    public function getDemoProductIds(): array
    {
        return $this->demoProductIds;
    }

    /**
     * @return Manager
     */
    public static function instance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return \WP_Term[]
     */
    public function getDemoCategories(): array
    {
        if ($this->demoCategories !== null) {
            return $this->demoCategories;
        }

        $this->demoCategories = [];

        foreach ($this->demoCategoryIds as $categoryId) {
            $term = get_term($categoryId);

            if ($term instanceof \WP_Term) {
                $this->demoCategories[] = $term;
            }
        }

        return $this->demoCategories;
    }

    /**
     * @return bool
     */
    public function hasDemoCategories(): bool
    {
        return \count($this->getDemoCategories()) > 0;
    }

    /**
     * @return \WC_Product[]
     */
    public function getDemoProducts(): array
    {
        if ($this->demoProducts !== null) {
            return $this->demoProducts;
        }

        $this->demoProducts = [];

        foreach ($this->demoProductIds as $productId) {
            $wcPF = new \WC_Product_Factory();
            $wcProduct = $wcPF->get_product($productId);
            if ($wcProduct instanceof \WC_Product) {
                $this->demoProducts[] = $wcProduct;
            }
        }

        return $this->demoProducts;
    }

    /**
     * @return bool
     */
    public function hasDemoProducts(): bool
    {
        return \count($this->getDemoProducts()) > 0;
    }

    /**
     * @return array
     */
    public function getDemoImages(): ?array
    {
        if ($this->demoImages !== null) {
            return $this->demoImages;
        }

        $this->demoImages = [];

        // Get images of products
        foreach ($this->getDemoProducts() as $product) {
            //Gallery
            $imageIds = $product->get_gallery_image_ids();

            $imageIndex = 'product-' . $product->get_id();

            // Thumbnail
            $thumbnailId = get_post_thumbnail_id($product->get_id());
            if ($thumbnailId) {
                $imageIds[] = $thumbnailId;
            }

            foreach ($imageIds as $imageId) {
                if ($image = get_post($imageId)) {
                    if (!isset($this->demoImages[$imageIndex])) {
                        $this->demoImages[$imageIndex] = [];
                    }
                    $this->demoImages[$imageIndex][] = $image;
                }
            }
        }

        // Get images of posts
        foreach ($this->getDemoBlogPages() as $page) {
            $imageIndex = 'post-' . $page->ID;
            $attachedImages = get_attached_media('image', $page->ID);

            if (\count($attachedImages) > 0) {
                if (!isset($this->demoImages[0])) {
                    $this->demoImages[$imageIndex] = [];
                }

                foreach ($attachedImages as $image) {
                    $this->demoImages[$imageIndex][] = $image;
                }
            }
        }

        // Get images of categories
        foreach ($this->getDemoCategories() as $category) {
            $imageIndex = 'category-' . $category->term_id;
            $thumbnailId = get_term_meta($category->term_id, 'thumbnail_id', true);

            if ($thumbnailId && $thumbnail = get_post($thumbnailId)) {
                if (!isset($this->demoImages[0])) {
                    $this->demoImages[$imageIndex] = [];
                }
                $this->demoImages[$imageIndex][] = $thumbnail;
            }
        }

        // Add image of main category
        foreach ($this->demoImageIds as $imageId) {
            if ($image = get_post($imageId)) {
                if (!isset($this->demoImages['without-parent'])) {
                    $this->demoImages['without-parent'] = [];
                }
                $this->demoImages['without-parent'][] = $image;
            }
        }

        return $this->demoImages;
    }

    /**
     * @return bool
     */
    public function hasDemoImages(): bool
    {
        return \count($this->getDemoImages()) > 0;
    }

    /**
     * @return array
     */
    public function getDemoBlogPages(): array
    {
        if ($this->demoBlogPages !== null) {
            return $this->demoBlogPages;
        }

        $this->demoBlogPages = get_posts(['post__in' => $this->demoBlogPageIds, 'posts_per_page' => 100]);

        return $this->demoBlogPages;
    }

    /**
     * @return bool
     */
    public function hasDemoBlogPages(): bool
    {
        return \count($this->getDemoBlogPages()) > 0;
    }

    /**
     * @return bool
     */
    public function hasDemoContent(): bool
    {
        // Get from blog options
        $hasDemoContent = (bool)get_option($this::$blog_option_has_demo_content, true);

        if ($hasDemoContent === false) {
            return false;
        }

        $hasDemoContent = $this->hasDemoProducts() || $this->hasDemoImages() || $this->hasDemoBlogPages() || $this->hasDemoImages();

        // If this was also deleted, we save it in blog options
        update_option($this::$blog_option_has_demo_content, $hasDemoContent);

        return $hasDemoContent;
    }
}
