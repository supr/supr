<?php

namespace SuprDemocontent;

/**
 * Class Plugin
 *
 * @package SuprDemocontent
 */
class Plugin
{
    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Admin Init
     */
    public function adminInit(): void
    {
        // We need this plugin only with demo content
        if (Manager::instance()->hasDemoContent() === false) {
            return;
        }
        ResourceManager::addResources();
        ResourceManager::addJsVariables();
        AjaxRestEndpoint::addEndPoints();
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        // Create Menu
        Menu::addMenu();

        // Load translations
        \add_action('plugins_loaded', [TextDomain::class, 'registerTranslations']);

        \add_action('admin_init', [$this, 'adminInit'], 0);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_DEMOCONTENT_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::instance();
