<?php

namespace SuprDemocontent;

/**
 * Class TextDomain
 *
 * @package SuprDemocontent
 */
class TextDomain
{
    public static $domainName = 'supr-democontent';

    public static function registerTranslations(): void
    {
        \load_plugin_textdomain(self::$domainName, false, SUPR_DEMOCONTENT_DIR_NAME . '/languages/');
    }
}
