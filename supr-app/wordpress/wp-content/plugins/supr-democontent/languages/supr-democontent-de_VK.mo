��          �       L      L     M     Z  
   a     l     �     �  �   �     P     W     e     n     �  !   �      �     �     �  #   �  "     �   7     �  	   �  
   �  "   	     ,     4  �   <     �     �     �     �       .   4  '   c     �     �  &   �  *   �   Blog Entries Cancel Categories Delete selected content Done! Fail! Here you can delete all the examples with which we filled your shop preliminarily. Watch out: the deletion can not be undone. If you modified example content, make sure not to delete it here. Images Processing... Products Remove Demo Content Something went wrong :( The category #%d are not deleted. The element #%d are not deleted. Un-/Check all View We are removing the marked items... We have deleted all you checked :) MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Project-Id-Version: Viking Shop-democontent
Language: de
 Blog-Einträge Abbrechen Kategorien Ausgewählten Demo-Inhalt löschen Fertig! Fehler! Hier kannst du alle Beispiel-Inhalte löschen. ACHTUNG: Änderungen an Beispiel-Inhalten werden mit gelöscht, wenn du diese zum löschen ausgewählt hast. Bilder Wird verarbeitet... Produkte Beispiel-Inhalt löschen Irgendwas ist schief gelaufen! Die Kategorie(n) #%d wurde(n) nicht gelöscht. Element(e) #%d wurde(n) nicht gelöscht Alles Markieren Ansehen Wir löschen die markierten Inhalte... Wir haben die markierten Inhalte gelöscht 