<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();
?>
<div class="wrap">
    <h2><?= get_admin_page_title(); ?></h2>
    <?php
    // Handle request
    if (isset($_POST['supr_democontent_options'])) {
        $newOptions = $_POST['supr_democontent_options'];

        // Trim all values
        $newOptions = array_map('trim', $newOptions);
        // Remove empty values
        $newOptions = array_filter($newOptions);
        // Sanitize values with WP special function
        $newOptions = array_map('sanitize_text_field', $newOptions);

        update_option('supr_democontent_options', $newOptions);
    }

    $options = get_option('supr_democontent_options', []);
    ?>

    <form action="" method="POST">
        <table class="form-table">
            <tr>
                <th colspan="2"><?= __('Main options', 'supr-democontent'); ?></th>
            </tr>
            <tr>
                <td><label for="supr_democontent_options_demo_products_ids"><?= __('IDs of demo products', 'supr-democontent'); ?></label></td>
                <td><input type="text" id="supr_democontent_options_demo_products_ids" placeholder="1,2,3" name="supr_democontent_options[demo_products_ids]" value="<?= $options['demo_products_ids'] ?? ''; ?>"/></td>
            </tr>
            <tr>
                <td><label for="supr_democontent_options_demo_pages_ids"><?= __('IDs of demo pages', 'supr-democontent'); ?></label></td>
                <td><input type="text" id="supr_democontent_options_demo_pages_ids" placeholder="1,2,3" name="supr_democontent_options[demo_pages_ids]" value="<?= $options['demo_pages_ids'] ?? ''; ?>"/></td>
            </tr>
            <tr>
                <td><label for="supr_democontent_options_demo_categories_ids"><?= __('IDs of demo categories', 'supr-democontent'); ?></label></td>
                <td><input type="text" id="supr_democontent_options_demo_categories_ids" placeholder="1,2,3" name="supr_democontent_options[demo_categories_ids]" value="<?= $options['demo_categories_ids'] ?? ''; ?>"/></td>
            </tr>
            <tr>
                <td><label for="supr_democontent_options_demo_images_ids"><?= __('IDs of demo images (without images of products)', 'supr-democontent'); ?></label></td>
                <td><input type="text" id="supr_democontent_options_demo_images_ids" placeholder="1,2,3" name="supr_democontent_options[demo_images_ids]" value="<?= $options['demo_images_ids'] ?? ''; ?>"/></td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
    <h3><?= __('Maintenance mode', 'supr-democontent'); ?></h3>
    <p><?= __('To show a normal shop in maintenance mode, you need a GET parameter', 'supr-democontent'); ?>:
        <input type="text" style="width: 450px" value="https://[shop-url]/?supr_democontent_cop_check=1" readonly></p>
</div>