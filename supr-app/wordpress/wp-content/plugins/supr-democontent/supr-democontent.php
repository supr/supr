<?php
/**
 * Plugin Name: SUPR - Demo content
 * Plugin URI: https://supr.com
 * Description: Delete demo content from shop
 * Text Domain: supr-democontent
 * Domain Path: /languages
 * Version: 1.0.0
 * Author: SUPR Development
 * License: GPL
 */

define('SUPR_DEMOCONTENT_PATH', plugin_dir_path(__FILE__));
define('SUPR_DEMOCONTENT_URL', plugins_url('/', __FILE__));
define('SUPR_DEMOCONTENT_FILE', plugin_basename(__FILE__));
define('SUPR_DEMOCONTENT_DIR_NAME', basename(__DIR__));

define('SUPR_DEMOCONTENT_VERSION', '1.1.0');

// Load main Class
require(SUPR_DEMOCONTENT_PATH . 'includes/Plugin.php');
