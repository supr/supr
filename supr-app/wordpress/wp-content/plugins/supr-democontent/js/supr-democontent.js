(function ($) {

    // We show it only on product, upload or blog posts seite
    var supr_democontent_screen_selectors = [];

    if (supr_democontent_values.has_demo_products) {
        supr_democontent_screen_selectors.push('body.edit-php.post-type-product');
    }

    if (supr_democontent_values.has_demo_images) {
        supr_democontent_screen_selectors.push('body.upload-php.post-type-attachment');
    }

    if (supr_democontent_values.has_demo_posts) {
        supr_democontent_screen_selectors.push('body.edit-php.post-type-post');
    }

    if (supr_democontent_values.has_demo_categories) {
        supr_democontent_screen_selectors.push('body.edit-tags-php.taxonomy-product_cat');
    }

    if (supr_democontent_screen_selectors.length === 0) {
        return;
    }

    var supr_democontent_add_remove_button = false;
    var supr_democontent_header_end_element;

    for (var i = 0; i < supr_democontent_screen_selectors.length; i++) {
        var element = $(supr_democontent_screen_selectors[i]);
        if (element.length > 0) {
            supr_democontent_header_end_element = element.find('hr.wp-header-end');
            supr_democontent_add_remove_button = true;
        }
    }

    // If not found, we don't do anything
    if (!supr_democontent_add_remove_button) {
        return;
    }

    // Create button
    var supr_democontent_remove_button = $('<a />', {
        'class': 'page-title-action',
        'id': 'supr-demo-content-remove-button',
        'href': '#'
    }).html(supr_democontent_values.remove_button_text).on('click', function () {
        supr_democontent_modal_open();
    });

    // Add button in DOM
    supr_democontent_remove_button.insertBefore(supr_democontent_header_end_element);
})(jQuery);

function supr_democontent_modal_open()
{
    var post_ids_to_remove = [];
    var category_ids_to_remove = [];

    Swal.fire({
        title: supr_democontent_values.remove_dialog_title,
        html: supr_democontent_values.remove_dialog_html,
        customClass: 'animated zoomIn faster',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: true,
        confirmButtonText: supr_democontent_values.remove_dialog_apply_button_text,
        cancelButtonText: supr_democontent_values.remove_dialog_cancel_button_text,
        showClass: {
            popup: '',
            backdrop: '',
            icon: '',
        },
        hideClass: {
            popup: '',
            backdrop: '',
            icon: '',
        },
        onOpen: function () {
            var swal_content_box = jQuery(Swal.getContent());

            // Check/uncheck all
            swal_content_box.find('button.supr-democontent-check-all').on('click', function () {
                var checked = jQuery(this).data("checked");
                checked = Math.abs(checked - 1);
                swal_content_box.find('input[type=checkbox]').attr('checked', checked === 1);
                jQuery(this).data("checked", checked);
            });

            // Check images if product was checked
            swal_content_box.find('input.supr-democontent-product').change(function () {
                var checked = jQuery(this).prop("checked");
                var product_id = jQuery(this).data('id');
                swal_content_box.find('input.supr-democontent-image[data-parent=product-' + product_id + ']').attr('checked', checked);
            });

            // Check images if post was checked
            swal_content_box.find('input.supr-democontent-post').change(function () {
                var checked = jQuery(this).prop("checked");
                var post_id = jQuery(this).data('id');
                swal_content_box.find('input.supr-democontent-image[data-parent=post-' + post_id + ']').attr('checked', checked);
            });

            // Check images if post was checked
            swal_content_box.find('input.supr-democontent-category').change(function () {
                var checked = jQuery(this).prop("checked");
                var category_id = jQuery(this).data('id');
                swal_content_box.find('input.supr-democontent-image[data-parent=category-' + category_id + ']').attr('checked', checked);
            });
        },
        onClose: function () {
            var swal_content_box = jQuery(Swal.getContent());
            post_ids_to_remove = [];
            category_ids_to_remove = [];
            swal_content_box.find('input[type=checkbox]:checked:not(.supr-democontent-category)').each(function (index) {
                post_ids_to_remove.push(jQuery(this).data('id'));
            });
            swal_content_box.find('input.supr-democontent-category[type=checkbox]:checked').each(function (index) {
                category_ids_to_remove.push(jQuery(this).data('id'));
            });
        }
    }).then(function (object) {
        if (object.value) {
            if (post_ids_to_remove.length === 0 && category_ids_to_remove.length === 0) {
                console.log('You have not selected anything!');
                return;
            }

            // Show loading
            Swal.fire({
                title: supr_democontent_values.loading_dialog_title,
                html: supr_democontent_values.loading_dialog_text,
                onOpen: function () {
                    Swal.showLoading();
                }
            });

            jQuery.post('/wp-admin/admin-ajax.php', {action: "supr-democontent-remove-posts", postIds: post_ids_to_remove, categoryIds: category_ids_to_remove})
                .done(function (data) {
                    data = JSON.parse(data);

                    // We don't show errors (SEE: #S2-808)
                    supr_democontent_modal_ok();
                })
                .fail(function () {
                    supr_democontent_modal_fail(data);
                });
        }
    })
}

function supr_democontent_modal_ok()
{
    Swal.fire({
        title: supr_democontent_values.success_dialog_title,
        text: supr_democontent_values.success_dialog_text,
        icon: 'success',
        onClose: function () {
            location.reload();
        }
        });
}

function supr_democontent_modal_fail(data)
{
    var error_text = supr_democontent_values.fail_dialog_text;

    // Show errors if exist
    if (data.errors) {
        error_text += '<br>';
        for (var i = 0; i < data.errors.length; i++) {
            error_text += '<br>' + data.errors[i];
        }
    }

    Swal.fire(
        supr_democontent_values.fail_dialog_title,
        error_text,
        'error'
    );
}
