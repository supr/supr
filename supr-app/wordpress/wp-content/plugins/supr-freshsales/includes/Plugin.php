<?php

namespace SuprFreshSales;

/**
 * Class Plugin
 *
 * @package SuprFreshSales
 */
class Plugin
{
    public static $cookieName = 'supr_freshsales_updated';

    public static $clientIdOptionName = 'supr_freshsales_client_id';

    /**
     * @var Plugin
     */
    public static $instance;

    /**
     * @return Plugin
     */
    public static function instance(): Plugin
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * On admin init
     */
    public function adminInit(): void
    {
        // Actualize data in CM if it is not a super admin => it is admin in admin panel
        if (!isset($_COOKIE[self::$cookieName]) && !\is_super_admin()) {
            Manager::updateBlog();
        }
    }

    /**
     * Plugin constructor
     */
    private function __construct()
    {
        $this->registerAutoloader();

        Menu::addMenu();

        // Sync works only for Multisite version
        if (!\is_multisite()) {
            return;
        }

        \add_action('admin_init', [$this, 'adminInit'], 100);

        // Actualize data in FD if plan was changed
        \add_filter('wu_subscription_before_save', [Manager::class, 'updateBlogByPlan'], 10, 2);

        // Update blog statuses
        \add_action('make_spam_blog', [Manager::class, 'updateBlog']);
        \add_action('make_ham_blog', [Manager::class, 'updateBlog']);
        \add_action('mature_blog', [Manager::class, 'updateBlog']);
        \add_action('unmature_blog', [Manager::class, 'updateBlog']);
        \add_action('archive_blog', [Manager::class, 'updateBlog']);
        \add_action('unarchive_blog', [Manager::class, 'updateBlog']);
        \add_action('make_delete_blog', [Manager::class, 'updateBlog']);
        \add_action('make_undelete_blog', [Manager::class, 'updateBlog']);
        \add_action('update_blog_public', [Manager::class, 'updateBlog']);
        \add_action('wp_uninitialize_site', [Manager::class, 'onDeleteBlog']);
    }

    /**
     * Register autoloader
     */
    private function registerAutoloader(): void
    {
        require_once SUPR_FRESHSALES_PATH . 'includes/Autoloader.php';

        Autoloader::run();
    }
}

Plugin::instance();
