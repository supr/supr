<?php

namespace SuprFreshSales;

/**
 * Class Page
 *
 * @package SuprFreshSales
 */
class Page
{
    /**
     * Show setting page
     */
    public static function settingsPage(): void
    {
        include SUPR_FRESHSALES_PATH . 'templates/settings-page.php';
    }
}
