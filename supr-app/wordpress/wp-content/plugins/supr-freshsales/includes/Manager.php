<?php

namespace SuprFreshSales;

/**
 * Class Manager
 *
 * @package SuprFreshSales
 */
class Manager
{
    /**
     * @var Manager
     */
    public static $instance;

    /**
     * @var string|null
     */
    private $apiKey;

    /**
     * @var string|null
     */
    private $domain;

    /**
     * Manager constructor.
     */
    public function __construct()
    {
        $options = \is_multisite() ? \get_blog_option(1, 'supr_freshsales_options') : \get_option('supr_freshsales_options');

        $this->apiKey = $options['api_key'] ? \trim($options['api_key']) : null;
        $this->domain = $options['domain'] ? \trim($options['domain']) : null;
    }

    /**
     * @return string
     */
    private function getMainUrl(): string
    {
        return "https://{$this->domain}.freshsales.io/api/";
    }

    /**
     * @return bool
     */
    public function hasApiKey(): bool
    {
        return $this->apiKey !== null;
    }

    /**
     * @return Manager
     */
    public static function instance(): Manager
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $shopId
     * @return null|\WP_User
     */
    public static function getUserByShopId($shopId): ?\WP_User
    {
        global $wpdb;

        $userId = null;
        $user = null;

        if (\class_exists('\WU_Site_Owner')) {
            $tableName = \WU_Site_Owner::get_table_name();
            $result = $wpdb->get_row("SELECT user_id FROM {$tableName} WHERE site_id = {$shopId}");

            if ($result) {
                $userId = (int)$result->user_id;
            }
        }

        if (!$userId) {
            $tableName = $wpdb->base_prefix . 'usermeta';
            $querystring = "SELECT user_id FROM {$tableName} WHERE (meta_key LIKE 'primary_blog' AND meta_value LIKE '{$shopId}') LIMIT 1";
            $userId = (int)$wpdb->get_var($querystring);
        }

        if ($userId) {
            $user = \get_user_by('id', $userId);

            // Fix the return type, because we can't handle mixed types!
            if (\is_bool($user)) {
                return null;
            }
        }

        return $user;
    }

    /**
     * @param $userId
     * @return false|\WP_Site
     */
    public static function getShopByUserId($userId)
    {
        global $wpdb;

        $shopId = null;

        if (\class_exists('\WU_Site_Owner')) {
            $tableName = \WU_Site_Owner::get_table_name();
            $result = $wpdb->get_row("SELECT site_id FROM {$tableName} WHERE user_id = {$userId}");
            $shopId = $result ? $result->site_id : null;
        } else {
            $shops = \get_blogs_of_user($userId);
            if (\is_array($shops) && !empty($shops)) {
                $shopId = $shops[0]->site_id;
            }
        }

        if ($shopId === null) {
            return false;
        }

        return \get_blog_details(['blog_id' => $shopId], true);
    }

    /**
     * @param null|false|int|\WP_Site $shop
     * @param null|string $wuPlan
     * @param null|string $status
     * @return array|null
     * @throws \Exception
     */
    public static function getShopData($shop = null, $wuPlan = null, $status = null): ?array
    {
        if ($shop instanceof \WP_Site) {
            // All is ok
        } else {
            $shopId = \is_int($shop) ? $shop : \get_current_blog_id();
            $shop = \get_blog_details(['blog_id' => $shopId], true);
        }

        // Get user of shop
        $user = self::getUserByShopId($shop->id);

        // There is not any user
        if ($user === null || (int)$user->ID === 0) {
            return null;
        }

        $params = [
            'email' => \get_blog_option($shop->id, 'woocommerce_store_owner_email', $user->user_email),
            'first_name' => \get_blog_option($shop->id, 'woocommerce_store_owner_first_name', $user->first_name),
            'last_name' => \get_blog_option($shop->id, 'woocommerce_store_owner_last_name', $user->last_name),
            'display_name' => \trim(
                \get_blog_option($shop->id, 'woocommerce_store_owner_first_name', $user->first_name) . ' ' . \get_blog_option($shop->id, 'woocommerce_store_owner_last_name', $user->first_name)
            ),
            'zipcode' => \get_blog_option($shop->id, 'woocommerce_store_postcode', '-'),
            'city' => \get_blog_option($shop->id, 'woocommerce_store_city', '-'),
            'country' => \get_blog_option($shop->id, 'woocommerce_default_country', '-'),
            'address' => \get_blog_option($shop->id, 'woocommerce_store_address', '-'),
            'work_number' => \get_blog_option($shop->id, 'woocommerce_store_owner_phone', '-'),
            'updated_at' => (new \DateTime())->format(DATE_ATOM),
            'custom_field' => []
        ];

        // It is required
        if (empty($params['last_name'])) {
            $params['last_name'] = $shop->blogname;
        }

        if ($wuPlan === null) {
            if (\function_exists('wu_get_site')) {
                $wuSite = \wu_get_site($shop->id);

                if ($wuSubscription = $wuSite->get_subscription()) {
                    $wuPlan = $wuSubscription->get_plan() ? $wuSubscription->get_plan()->title : false;
                    $wuPlanCreatedAt = ($wuSubscription->last_plan_change !== null && \strtotime($wuSubscription->last_plan_change) > 0) ? $wuSubscription->last_plan_change : $wuSubscription->created_at;
                }
            }

            if (!$wuPlan) {
                // Write log
                error_log('[Freshsales] Plan for shop #' . \get_current_blog_id() . ' doesn\'t exist.');

                $wuPlan = 'none';
            }
        }

        // Get custom fields
        $params['custom_field']['cf_shop'] = $shop->blogname;
        $params['custom_field']['cf_package'] = $wuPlan;
        $params['custom_field']['cf_booked'] = isset($wuPlanCreatedAt) ? date('Y-m-d', strtotime($wuPlanCreatedAt)) : null;
        $params['custom_field']['cf_signup'] = $user->user_registered ? date('Y-m-d', strtotime($user->user_registered)) : date('Y-m-d');
        $params['custom_field']['cf_company_name'] = \get_blog_option($shop->id, 'woocommerce_store_owner_company', '');
        $params['custom_field']['cf_main_blog'] = \get_network()->domain;

        switch_to_blog($shop->id);
        $params['custom_field']['cf_orders'] = (int)\wc_orders_count('completed');
        $params['custom_field']['cf_products'] = (int)\wp_count_posts('product')->publish;
        restore_current_blog();

        // If more than 500 orders, it is priority customer
        if ($params['custom_field']['orders'] > 500) {
            $params['has_authority'] = true;
        }

        $params['custom_field']['cf_shop_id'] = $shop->id;
        $params['custom_field']['cf_shop_url'] = $shop->siteurl;

        $shopStatus = [];

        // If status was already detected
        if ($status !== null) {
            $shopStatus[] = $status;
        } else {
            // Get statuses of shop
            if (\get_blog_option($shop->id, 'elementor_maintenance_mode_mode', null)) {
                $shopStatus[] = 'Maintenance mode';
            }

            if ((int)$shop->public === 0) {
                $shopStatus[] = 'Closed for search engine';
            }

            if ($shop->archived) {
                $shopStatus[] = 'Archived';
            }

            if ($shop->mature) {
                $shopStatus[] = 'Mature';
            }

            if ($shop->spam) {
                $shopStatus[] = 'Spam';
            }

            if ($shop->deleted) {
                $shopStatus[] = 'Deleted';
            }

            if (\count($shopStatus) === 0) {
                $shopStatus[] = 'Public';
            }
        }

        $params['custom_field']['cf_shop_status'] = implode(', ', $shopStatus);

        return $params;
    }

    public static function onDeleteBlog($shop): bool
    {
        return self::updateBlog($shop, null, 'Deleted');
    }

    /**
     * Send data to FS about current shop
     *
     * @param null|\WP_Site|int $shop
     * @param null|string       $wuPlan
     * @param null|string       $status
     * @return bool
     */
    public static function updateBlog($shop = null, $wuPlan = null, $status = null): bool
    {
        // Set cookie for 1 hour, after that we will try again
        self::setCookie('PT1H');

        if (!self::instance()->hasApiKey()) {
            error_log('[Freshsales] Api key doesn\'t exist.');

            return false;
        }

        if ($shop instanceof \WP_Site) {
            // All is ok
        } elseif (\is_int($shop)) {
            $shop = \get_blog_details(['blog_id' => $shop], true);
        } else {
            $shop = \get_blog_details(['blog_id' => \get_current_blog_id()], true);
        }

        // We have all test-shops in black list
        if (self::inBlackList($shop->domain, $shop->id)) {
            return false;
        }

        // For main blog we don't need Campaign Monitor
        if ($shop->id === 1 && \is_multisite()) {
            return true;
        }

        $params = self::getShopData($shop, $wuPlan, $status);

        // If we don't have a data jet
        if ($params === null || empty($params['email']) || empty($params['last_name'])) {
            return false;
        }

        // Check email
        if (!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
            error_log('[Freshsales] Email ' . $params['email'] . ' for the shop ' . \get_bloginfo('url') . ' is not valid.');

            return false;
        }

        // Try to get a contact id from blog-options
        $freshsalesUserId = \get_blog_option($shop->id, Plugin::$clientIdOptionName);

        // Try immediately to update
        if ($freshsalesUserId) {
            $result = self::instance()->sendRequest('PUT', ['contact' => $params], 'contacts/' . $freshsalesUserId);

            if ($result) {
                self::setCookie();

                return true;
            }
        }

        // Try to get a contact by email if not in blog options or if update was wrong
        $existedContactsResult = self::instance()->sendRequest(
            'POST',
            ['filter_rule' => [['attribute' => 'contact_email.email', 'operator' => 'is_in', 'value' => $params['email']]]],
            'filtered_search/contact'
        );

        if (isset($existedContactsResult['contacts'][0])) {
            $freshsalesContactId = $existedContactsResult['contacts'][0]['id'];
            // Update option for next time
            \update_option(Plugin::$clientIdOptionName, $freshsalesContactId);
            self::setCookie();

            return true;
        }

        // Create a new contact
        $newContact = self::instance()->sendRequest('POST', ['contact' => $params], 'contacts');

        // Set id of user from freshsales
        if (isset($newContact['contact']['id'])) {
            \update_option(Plugin::$clientIdOptionName, $newContact['contact']['id']);
            self::setCookie();

            return true;
        }

        error_log('[Freshsales] Can\'t update info of the shop ' . \get_bloginfo('url') . ' (' . $params['email'] . '):' . print_r($newContact, true));

        return false;
    }

    /**
     * Set cookie (shop was updated)
     *
     * @param string $dateInterval
     */
    public static function setCookie($dateInterval = 'P1D'): void
    {
        setcookie(Plugin::$cookieName, '1', (new \DateTime())->add(new \DateInterval($dateInterval))->getTimestamp());  /* expired after 1 day */
    }

    /**
     * @param array            $object
     * @param \WU_Subscription $subscription
     *
     * @return array
     */
    public static function updateBlogByPlan($object, $subscription): ?array
    {
        $shop = self::getShopByUserId($subscription->user_id);

        // If we cannot get a shop
        if ($shop === false) {
            return $object;
        }

        $wuPlan = $subscription->get_plan()->title;

        self::updateBlog($shop, $wuPlan);

        return $object;
    }

    /**
     * @param string $type
     * @param array  $data
     * @param        $url
     * @return array|bool|mixed|object
     */
    private function sendRequest($type, $data, $url)
    {
        $url = $this->getMainUrl() . $url;

        // Add GET params
        if ($type === 'GET' && \count($data) > 0) {
            $url .= '?' . http_build_query($data);
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $header = [];
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Token token=' . $this->apiKey;

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);

        switch ($type) {
            case 'GET':
                break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                break;
        }

        try {
            $serverOutput = curl_exec($ch);
        } catch (\Exception $e) {
            error_log("[Freshsales] Cannot connect to API: {$e->getMessage()}");

            return false;
        }

        $info = curl_getinfo($ch);
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers = '';
        curl_close($ch);

        $response = json_decode($serverOutput, true);

        // Hack: we get from server always different responses :( One time with headers, another time without headers.
        if (json_last_error() !== JSON_ERROR_NONE) {
            $headers = substr($serverOutput, 0, $headerSize);
            $response = json_decode(substr($serverOutput, $headerSize), true);
        }

        if ($info['http_code'] >= 200 && $info['http_code'] < 300) {
            return $response;
        }

        if ($info['http_code'] === 404) {
            error_log("[Freshsales] Url is false\nHTTP Status Code: {$info['http_code']}\nUrl: {$url}");

            return false;
        }

        error_log(
            '[Freshsales] Error in the shop ' . \get_bloginfo('url')
            . "\nMethod: {$type}"
            . "\nUrl: {$url}"
            . "\nData: " . print_r($data, true)
            . "\nHTTP Status Code: " . $info['http_code']
            . "\nHeaders: " . print_r($headers, true)
            . "\nResponse: " . print_r($response, true)
        );

        //var_dump('[Freshsales] Error, HTTP Status Code: ' . $info['http_code'] . "\nHeaders:" . print_r($headers, true) . "\nResponse" . print_r($response, true));

        return false;
    }

    /**
     * @param      $shopDomain
     * @param null $shopId
     * @return bool
     */
    public static function inBlackList($shopDomain, $shopId = null): bool
    {
        $options = \is_multisite() ? \get_blog_option(1, 'supr_freshsales_options') : \get_option('supr_freshsales_options');

        $inBlackList = false;

        if (isset($options['not_sync_domain']) && !empty($options['not_sync_domain'])) {
            // If regexp
            if (@preg_match($options['not_sync_domain'], null) !== false) {
                \preg_match($options['not_sync_domain'], $shopDomain, $matches);

                if (\count($matches) > 0) {
                    $inBlackList = true;
                }
            } elseif (\strpos($shopDomain, $options['not_sync_domain']) !== false) {
                $inBlackList = true;
            }
        }

        if (isset($options['not_sync_blog_ids']) && !empty($options['not_sync_blog_ids'])) {
            $blogIds = \array_map('\intval', \explode(',', $options['not_sync_blog_ids']));
            if (\in_array($shopId, $blogIds, true)) {
                $inBlackList = true;
            }
        }

        return $inBlackList;
    }
}
