<?php
// Protection from direct access to file
defined('ABSPATH') || http_response_code(404) . die();
?>
<div class="wrap">
    <h2><?= get_admin_page_title(); ?></h2>
    <?php
    // Handle request
    if (isset($_POST['supr_freshsales_options'])) {
        $options = $_POST['supr_freshsales_options'];

        // Trim all values
        $options = array_map('trim', $options);
        // Remove empty values
        $options = array_filter($options);
        // Sanitize values with WP special function
        $options = array_map('sanitize_text_field', $options);

        update_option('supr_freshsales_options', $options);
    }

    $options = get_option('supr_freshsales_options', []);
    ?>

    <form action="" method="POST">
        <table class="form-table">
            <tr>
                <th colspan="2"><?= __('Options', 'supr-freshsales'); ?></th>
            </tr>
            <tr>
                <td><label for="supr_freshsales_option_api_key"><?= __('API Key', 'supr-freshsales'); ?></label></td>
                <td><input type="text" id="supr_freshsales_option_api_key" name="supr_freshsales_options[api_key]" value="<?= $options['api_key'] ?? ''; ?>"/></td>
            </tr>
            <tr>
                <td><label for="supr_freshsales_option_password"><?= __('Domain name', 'supr-freshsales'); ?></label></td>
                <td><input type="text" id="supr_freshsales_option_password" name="supr_freshsales_options[domain]" value="<?= $options['domain'] ?? ''; ?>"/></td>
            </tr>
            <tr>
                <td><label for="supr_freshsales_option_not_sync_domain"><?= __('Black list (shop domain)', 'supr-campaign-monitor'); ?></label></td>
                <td>
                    <input type="text" id="supr_freshsales_option_not_sync_domain" placeholder="/^test[0-9a-z]{1,}/" name="supr_freshsales_options[not_sync_domain]" value="<?= $options['not_sync_domain'] ?? ''; ?>"/>
                </td>
            </tr>
            <tr>
                <td><label for="supr_freshsales_option_not_sync_blog_ids"><?= __('Black list (shop ids) ', 'supr-campaign-monitor'); ?></label></td>
                <td>
                    <input type="text" id="supr_freshsales_option_not_sync_blog_ids" placeholder="1,9,23,598" name="supr_freshsales_options[not_sync_blog_ids]" value="<?= $options['not_sync_blog_ids'] ?? ''; ?>"/>
                </td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
</div>
