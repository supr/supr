<?php
/**
 * Plugin Name: SUPR - Freshsales
 * Plugin URI: https://supr.com
 * Description: Integration of Freshsales in WP Ultimo
 * Text Domain: supr-freshsales
 * Domain Path: /languages
 * Version: 1.1.0
 * Author: SUPR Development
 * License: GPL
 */

// API Documentation https://www.freshsales.io/api/#create_contact

define('SUPR_FRESHSALES_PATH', plugin_dir_path(__FILE__));
define('SUPR_FRESHSALES_URL', plugins_url('/', __FILE__));
define('SUPR_FRESHSALES_FILE', plugin_basename(__FILE__));
define('SUPR_FRESHSALES_DIR_NAME', basename(__DIR__));


define('SUPR_FRESHSALES_VERSION', '1.1.0');

// Load main Class
require SUPR_FRESHSALES_PATH . 'includes/Plugin.php';
