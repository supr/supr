<?php

/**
 * Wordpress child theme
 *
 * Description: Action loads stylesheet from parent theme (storefront)
 *
 */
global $storefront_version;

$storefront = (object)array(
    'version' => $storefront_version,

    /**
     * Initialize all the things.
     */
    'main' => require 'inc/class-storefront.php',
);

add_filter('storefront_woocommerce_args', 'storefront_supr_woocommerce_args');

/**
 * Change default woocommerce thumbnails size
 *
 * @param $args
 * @return mixed
 */
function storefront_supr_woocommerce_args($args)
{
    $args['single_image_width'] = 900;
    $args['thumbnail_image_width'] = 500;

    return $args;
}
