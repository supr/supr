<?php
/**
 * Storefront Class
 *
 * @author   WooThemes
 * @since    2.0.0
 * @package  storefront
 */

if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('Storefront')) :

    /**
     * The main Storefront class
     */
    class Storefront
    {
        /**
         * Setup class.
         *
         * @since 1.0
         */
        public function __construct()
        {
            add_action('after_setup_theme', array($this, 'setup'));
            add_action('widgets_init', array($this, 'widgets_init'));
            add_action('wp_enqueue_scripts', array($this, 'scripts'), 10);
            add_action('wp_enqueue_scripts', array($this, 'child_scripts'), 30); // After WooCommerce.
            add_filter('body_class', array($this, 'body_classes'));
            add_filter('wp_page_menu_args', array($this, 'page_menu_args'));
            add_filter('navigation_markup_template', array($this, 'navigation_markup_template'));
            add_action('wp_enqueue_scripts', array($this, 'supr_elementor_inline_style'), 30);
        }

        /**
         * Sets up theme defaults and registers support for various WordPress features.
         *
         * Note that this function is hooked into the after_setup_theme hook, which
         * runs before the init hook. The init hook is too late for some features, such
         * as indicating support for post thumbnails.
         */
        public function setup()
        {
            /*
             * Load Localisation files.
             *
             * Note: the first-loaded translation file overrides any following ones if the same translation is present.
             */

            // Loads wp-content/languages/themes/storefront-it_IT.mo.
            load_theme_textdomain('supr-storefront', trailingslashit(WP_LANG_DIR) . 'themes/');

            // Loads wp-content/themes/child-theme-name/languages/it_IT.mo.
            load_theme_textdomain('supr-storefront', get_stylesheet_directory() . '/languages');

            // Loads wp-content/themes/storefront/languages/it_IT.mo.
            load_theme_textdomain('supr-storefront', get_template_directory() . '/languages');

            /**
             * Add default posts and comments RSS feed links to head.
             */
            add_theme_support('automatic-feed-links');

            /*
             * Enable support for Post Thumbnails on posts and pages.
             *
             * @link https://developer.wordpress.org/reference/functions/add_theme_support/#Post_Thumbnails
             */
            add_theme_support('post-thumbnails');

            /**
             * Enable support for site logo
             */
            add_theme_support(
                'custom-logo',
                apply_filters(
                    'storefront_custom_logo_args',
                    array(
                        'height' => 110,
                        'width' => 470,
                        'flex-width' => true,
                        'flex-height' => true,
                    )
                )
            );

            /*
             * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
             * to output valid HTML5.
             */
            add_theme_support(
                'html5',
                apply_filters(
                    'storefront_html5_args',
                    array(
                        'search-form',
                        'comment-form',
                        'comment-list',
                        'gallery',
                        'caption',
                        'widgets',
                    )
                )
            );

            /**
             *  Add support for the Site Logo plugin and the site logo functionality in JetPack
             *  https://github.com/automattic/site-logo
             *  http://jetpack.me/
             */
            add_theme_support(
                'site-logo',
                apply_filters(
                    'storefront_site_logo_args',
                    array(
                        'size' => 'full'
                    )
                )
            );

            add_theme_support('wc-product-gallery-zoom');
            add_theme_support('wc-product-gallery-lightbox');
            add_theme_support('wc-product-gallery-slider');

            // Declare support for title theme feature.
            add_theme_support('title-tag');

            // Declare support for selective refreshing of widgets.
            add_theme_support('customize-selective-refresh-widgets');
        }

        /**
         * Register widget area.
         *
         * @link https://codex.wordpress.org/Function_Reference/register_sidebar
         */
        public function widgets_init()
        {

            $sidebar_args['sidebar_pages'] = array(
                'name' => __('Pages', 'supr-storefront'),
                'id' => 'sidebar-pages',
                'description' => '',
            );

            $sidebar_args['sidebar_blog'] = array(
                'name' => __('Blog', 'supr-storefront'),
                'id' => 'sidebar-blog',
                'description' => ''
            );

            $sidebar_args['sidebar_product_catalogue'] = array(
                'name' => __('Product catalogue', 'supr-storefront'),
                'id' => 'sidebar-product-catalogue',
                'description' => '',
            );

            $sidebar_args['sidebar_product_page'] = array(
                'name' => __('Product page', 'supr-storefront'),
                'id' => 'sidebar-product-page',
                'description' => '',
            );

            $sidebar_args = apply_filters('storefront_sidebar_args', $sidebar_args);

            foreach ($sidebar_args as $sidebar => $args) {
                $widget_tags = array(
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget' => '</div>',
                    'before_title' => '<span class="gamma widget-title">',
                    'after_title' => '</span>',
                );

                /**
                 * Dynamically generated filter hooks. Allow changing widget wrapper and title tags. See the list below.
                 *
                 * 'storefront_header_widget_tags'
                 * 'storefront_sidebar_widget_tags'
                 *
                 * 'storefront_footer_1_widget_tags'
                 * 'storefront_footer_2_widget_tags'
                 * 'storefront_footer_3_widget_tags'
                 * 'storefront_footer_4_widget_tags'
                 */
                $filter_hook = sprintf('storefront_%s_widget_tags', $sidebar);
                $widget_tags = apply_filters($filter_hook, $widget_tags);

                if (is_array($widget_tags)) {
                    register_sidebar($args + $widget_tags);
                }
            }
        }

        /**
         * Enqueue scripts and styles.
         *
         * @since  1.0.0
         */
        public function scripts()
        {
            global $storefront_version;

            /**
             * Styles
             */
            wp_enqueue_style('storefront-style', get_template_directory_uri() . '/style.css', '', $storefront_version);
            wp_style_add_data('storefront-style', 'rtl', 'replace');

            wp_enqueue_style('storefront-icons', get_template_directory_uri() . '/assets/css/base/icons.css', '', $storefront_version);
            wp_style_add_data('storefront-icons', 'rtl', 'replace');

            /**
             * Scripts
             */
            $suffix = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';

            wp_enqueue_script('storefront-navigation', get_template_directory_uri() . '/assets/js/navigation' . $suffix . '.js', array(), $storefront_version, true);
            wp_enqueue_script('storefront-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix' . $suffix . '.js', array(), '20130115', true);

            if (has_nav_menu('handheld')) {
                $storefront_l10n = array(
                    'expand' => __('Expand child menu', 'supr-storefront'),
                    'collapse' => __('Collapse child menu', 'supr-storefront'),
                );

                wp_localize_script('storefront-navigation', 'storefrontScreenReaderText', $storefront_l10n);
            }

            if (is_page_template('template-homepage.php') && has_post_thumbnail()) {
                wp_enqueue_script('storefront-homepage', get_template_directory_uri() . '/assets/js/homepage' . $suffix . '.js', array(), $storefront_version, true);
            }

            if (is_singular() && comments_open() && get_option('thread_comments')) {
                wp_enqueue_script('comment-reply');
            }
        }

        /**
         * Enqueue child theme stylesheet.
         * A separate function is required as the child theme css needs to be enqueued _after_ the parent theme
         * primary css and the separate WooCommerce css.
         *
         * @since  1.5.3
         */
        public function child_scripts()
        {
            if (is_child_theme()) {
                $child_theme = wp_get_theme(get_stylesheet());
                wp_enqueue_style('storefront-child-style', get_stylesheet_uri(), array(), $child_theme->get('Version'));
            }
        }

        /**
         * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
         *
         * @param array $args Configuration arguments.
         * @return array
         */
        public function page_menu_args($args)
        {
            $args['show_home'] = true;

            return $args;
        }

        /**
         * Adds custom classes to the array of body classes.
         *
         * @param array $classes Classes for the body element.
         * @return array
         */
        public function body_classes($classes)
        {
            // Adds a class of group-blog to blogs with more than 1 published author.
            if (is_multi_author()) {
                $classes[] = 'group-blog';
            }

            if (!function_exists('woocommerce_breadcrumb')) {
                $classes[] = 'no-wc-breadcrumb';
            }

            /**
             * What is this?!
             * Take the blue pill, close this file and forget you saw the following code.
             * Or take the red pill, filter storefront_make_me_cute and see how deep the rabbit hole goes...
             */
            $cute = apply_filters('storefront_make_me_cute', false);

            if (true === $cute) {
                $classes[] = 'storefront-cute';
            }

            // If our main sidebar doesn't contain widgets, adjust the layout to be full-width.
            if (!is_active_sidebar('sidebar-1')) {
                $classes[] = 'storefront-full-width-content';
            }

            // Add class when using homepage template + featured image
            if (is_page_template('template-homepage.php') && has_post_thumbnail()) {
                $classes[] = 'has-post-thumbnail';
            }

            return $classes;
        }

        /**
         * Custom navigation markup template hooked into `navigation_markup_template` filter hook.
         */
        public function navigation_markup_template()
        {
            $template = '<nav id="post-navigation" class="navigation %1$s" role="navigation" aria-label="' . esc_html__('Post Navigation', 'supr-storefront') . '">';
            $template .= '<h2 class="screen-reader-text">%2$s</h2>';
            $template .= '<div class="nav-links">%3$s</div>';
            $template .= '</nav>';

            return apply_filters('storefront_navigation_markup_template', $template);
        }

        /**
         * Add styles
         */
        public function supr_elementor_inline_style(): void
        {
            $elementor_scheme_color = get_option('elementor_scheme_color', []); // Default empty array
            $elementor_scheme_color_accent = $elementor_scheme_color[4] ?? '#1a1a1e'; // If index 4 not exist, than our color
            $custom_css = "
                .widget_price_filter .ui-slider .ui-slider-range,
                .widget_price_filter .ui-slider .ui-slider-handle {
                    background-color: {$elementor_scheme_color_accent};
                }
                a {
                    color: {$elementor_scheme_color_accent};
                }
                a:hover,
                a:active {
                    color: {$this->darken_color($elementor_scheme_color_accent, 1.25)};
                }
            ";

            wp_register_style('supr_elementor_inline_style', false);
            wp_enqueue_style('supr_elementor_inline_style');
            wp_add_inline_style('supr_elementor_inline_style', $custom_css);
        }

        public function darken_color($rgb, $darker = 2): string
        {

            $hash = (strpos($rgb, '#') !== false) ? '#' : '';
            $rgb = (strlen($rgb) == 7) ? str_replace('#', '', $rgb) : ((strlen($rgb) == 6) ? $rgb : false);
            if (strlen($rgb) != 6) {
                return $hash . '000000';
            }
            $darker = ($darker > 1) ? $darker : 1;

            list($R16, $G16, $B16) = str_split($rgb, 2);

            $R = sprintf("%02X", floor(hexdec($R16) / $darker));
            $G = sprintf("%02X", floor(hexdec($G16) / $darker));
            $B = sprintf("%02X", floor(hexdec($B16) / $darker));

            return $hash . $R . $G . $B;
        }
    }
endif;

return new Storefront();
