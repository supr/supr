<?php

get_header();

?>

    <div class="page-not-found">
        <h1>Uups!</h1>
        <span>
        Die gewünschte Seite konnte nicht gefunden werden…
        <br><br>
        <a href="<?= get_home_url(); ?>">Zurück zur Startseite</a>
    </span>

    </div>


<?php

get_footer();
