<?php

// Load translations
// wp-content/languages/themes/hello-elementor-supr-de_DE.mo.
load_theme_textdomain('hello-elementor-supr', trailingslashit(WP_LANG_DIR) . 'themes/');

// Add theme features
require_once __DIR__ . '/includes/ThemeFeatures.php';

new \HelloElementorSupr\ThemeFeatures();
