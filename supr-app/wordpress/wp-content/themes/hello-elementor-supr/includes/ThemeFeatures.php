<?php

namespace HelloElementorSupr;

/**
 * Class ThemeFeatures
 */
class ThemeFeatures
{
    public function __construct()
    {
        \add_action('widgets_init', [$this, 'widgetsInit']);

        \add_action('after_setup_theme', [$this, 'addWooCommerceSupport'], 11);

        \add_filter('woo_variation_gallery_default_width', [$this, 'changeWooVariationGalleryDefaultWidth'], 9);

        \add_action('wp_enqueue_scripts', [$this, 'enqueueCss'], 30);
    }

    /**
     * Add css to theme
     */
    public function enqueueCss(): void
    {
        // Theme styles
        \wp_enqueue_style('hello-elementor-supr', get_stylesheet_directory_uri() . '/style.css');
        if (\is_404()) {
            \wp_enqueue_style('hello-elementor-supr-404', get_stylesheet_directory_uri() . '/style-404.css');
        }
        // Inline WooCommerce CSS
        $this->elementorInlineStyle();
    }

    /**
     * Set settings for WooCommerce
     */
    public static function addWooCommerceSupport(): void
    {
        $wooCommerceThemeSettings = [
            'single_image_width' => 900,
            'thumbnail_image_width' => 500,
            'product_grid' => [
                'default_columns' => 3,
                'default_rows' => 4,
                'min_columns' => 1,
                'max_columns' => 6,
                'min_rows' => 1,
            ]
        ];

        \add_theme_support('woocommerce', $wooCommerceThemeSettings);
    }

    /**
     * Add inline styles to rewrite WooCommerce styles
     */
    public function elementorInlineStyle(): void
    {
        $elementorSchemeColor = \get_option('elementor_scheme_color', []); // Default empty array
        $elementorSchemeColorAccent = $elementorSchemeColor[4] ?? '#1a1a1e'; // If index 4 not exist, than our color
        $elementorSchemeColorAccentDark = $this->darkenColor($elementorSchemeColorAccent, 1.2);

        $helloElementorSuprCustom = "
        a,
        .elementor a {
            color: {$elementorSchemeColorAccent};
        }
        a:hover,
        a:active,
        .elementor a:hover,
        .elementor a:active {
            color: {$elementorSchemeColorAccentDark};
        }
        .widget_price_filter .ui-slider .ui-slider-range,
        .widget_price_filter .ui-slider .ui-slider-handle {
            background-color: {$elementorSchemeColorAccent} !important;
        }
        body.woocommerce-cart a.button.alt,
        form.woocommerce-checkout button#place_order {
            border-color: {$elementorSchemeColorAccentDark};
            background-color: {$elementorSchemeColorAccent};
        }
        body.woocommerce-cart a.button.alt:hover,
        form.woocommerce-checkout button#place_order:hover {
            background-color: {$elementorSchemeColorAccentDark};
        }
        ";

        \wp_add_inline_style('hello-elementor-supr', $helloElementorSuprCustom);
    }

    /**
     * Do more dark color as $rgb
     *
     * @param     $rgb
     * @param int $darker
     * @return string
     */
    private function darkenColor($rgb, $darker = 2): string
    {
        $hash = (strpos($rgb, '#') !== false) ? '#' : '';
        $rgb = (strlen($rgb) == 7) ? str_replace('#', '', $rgb) : ((strlen($rgb) == 6) ? $rgb : false);
        if (strlen($rgb) != 6) {
            return $hash . '000000';
        }
        $darker = ($darker > 1) ? $darker : 1;

        list($R16, $G16, $B16) = str_split($rgb, 2);

        $R = sprintf("%02X", floor(hexdec($R16) / $darker));
        $G = sprintf("%02X", floor(hexdec($G16) / $darker));
        $B = sprintf("%02X", floor(hexdec($B16) / $darker));

        return $hash . $R . $G . $B;
    }

    /**
     * Register widget area.
     *
     * @link https://codex.wordpress.org/Function_Reference/register_sidebar
     */
    public function widgetsInit(): void
    {
        $sidebar_args = [];

        $sidebar_args['sidebar_pages'] = [
            'name' => __('Pages', 'hello-elementor-supr'),
            'id' => 'sidebar-pages',
            'description' => '',
        ];

        $sidebar_args['sidebar_blog'] = [
            'name' => __('Blog', 'hello-elementor-supr'),
            'id' => 'sidebar-blog',
            'description' => ''
        ];

        $sidebar_args['sidebar_product_catalogue'] = [
            'name' => __('Product catalogue', 'hello-elementor-supr'),
            'id' => 'sidebar-product-catalogue',
            'description' => '',
        ];

        $sidebar_args['sidebar_product_page'] = [
            'name' => __('Product page', 'hello-elementor-supr'),
            'id' => 'sidebar-product-page',
            'description' => '',
        ];

        $shared_widget_args = [
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<span class="gamma widget-title">',
            'after_title' => '</span>',
        ];

        foreach ($sidebar_args as $sidebar => $args) {
            \register_sidebar(array_merge($shared_widget_args, $args));
        }
    }

    /**
     * Set width of gallery in product view to 100%
     *
     * @param $width
     * @return int
     */
    public function changeWooVariationGalleryDefaultWidth($width): int
    {
        return 100;
    }
}
