<?php
/*
 * Template Name: SUPR checkout
 */

\define('WOOCOMMERCE_CHECKOUT', true);
\get_header();

// If the SUPR Commerce Suite plugin is active the classes to output the SUPR checkout can be used...
if (\class_exists('\SuprCommerceSuite\Page')) {
    \SuprCommerceSuite\Page::checkoutForm();
} else {
    echo '<p style="text-align: center; padding: 15px;">Please activate <strong>SUPR Commerce Suite</strong> to use this template!</p>';
}

\get_footer();
