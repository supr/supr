<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package HelloElementor
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
?>
<main class="site-main" role="main">
    <div class="page-content error404">

        <img class="error404-image"
             src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/404.png'); ?>"
             alt="404 Page not found"/>

        <h1><?php _e('Product or page not found', 'hello-elementor-supr'); ?></h1>
        <p>
            <?php _e('The product or page you\'re looking for could not be found. Go back to the homepage or use our search.', 'hello-elementor-supr'); ?>
        </p>
        <p>
            <a class="button button-primary"
               href="<?= get_home_url(); ?>"><?php _e('Back to homepage', 'hello-elementor-supr'); ?></a>
        </p>
    </div>
</main>
