<?php

/**
 * Class health
 */
class health
{
    /**
     * @var string
     */
    private $dbHost;

    /**
     * @var string
     */
    private $dbUser;

    /**
     * @var string
     */
    private $dbPass;

    /**
     * @var string
     */
    private $dbName;

    /**
     * @var \mysqli
     */
    private $dbConnection;

    /**
     * @var string
     */
    private $wpConfigFile = __DIR__ . '/wp-config.php';

    /**
     * HealthChecker constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->parseWpConfig();
        $this->dbConnection = \mysqli_connect($this->dbHost, $this->dbUser, $this->dbPass, $this->dbName);

        if (!$this->dbConnection) {
            throw new \Exception('Can\'t establish connection to main database!');
        }
    }

    /**
     * By default we will disconnect from database.
     */
    public function __destruct()
    {
        \mysqli_close($this->dbConnection);
    }

    /**
     * Get database credentials from wp-config.php
     *
     * @throws \Exception
     */
    private function parseWpConfig(): void
    {
        if (!\file_exists($this->wpConfigFile)) {
            throw new \Exception('The main WordPress configuration not found!');
        }

        $values = [];
        $wpConfig = \file_get_contents($this->wpConfigFile);
        \preg_match_all("/define\('([^']+)', '([^']+)'\);/", $wpConfig, $matches);
        $count = \count($matches[1]);

        for ($i = 0; $i < $count; $i++) {
            $values[$matches[1][$i]] = $matches[2][$i];
        }

        $this->dbHost = $values['DB_HOST'];
        $this->dbUser = $values['DB_USER'];
        $this->dbPass = $values['DB_PASSWORD'];
        $this->dbName = $values['DB_NAME'];
    }

    /**
     * @return string
     */
    public function check(): string
    {
        return "OK\n";
    }
}

// The health check direct thru the database...
//$health = new health();
//echo $health->check();

// This isn't working because of HTTP to HTTPS redirects!
//include 'wp-config.php';

$loads = \sys_getloadavg();
\usleep($loads[0] * 10000);

echo "Load:<br>\n";
echo "<ul>\n";
foreach ($loads as $load) {
    echo "\t<li>{$load}</li>\n";
}
echo "</ul>\n";
