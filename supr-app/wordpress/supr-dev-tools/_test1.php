<?php

session_start();
if (!isset($_SESSION['count'])) {
    $_SESSION['count'] = 1;
} else {
    $_SESSION['count']++;
}
echo "Count: {$_SESSION['count']}";
session_write_close();

?>
<p>Source:</p>
<pre>session_start();
if (!isset($_SESSION['count'])) {
    $_SESSION['count'] = 1;
} else {
    $_SESSION['count']++;
}
echo "Count: {$_SESSION['count']}";
session_write_close();</pre>
