<?php

// Load the page header...
require __DIR__ . '/__head.php';

echo '<h2>PHP Global Variables Checker</h2><hr />';

echo '<p>The PHP $_ENV variable:</p>';
echo '<pre>';
\print_r($_ENV);
echo '</pre>';

echo '<p>The PHP $_SERVER variable:</p>';
echo '<pre>';
\print_r($_SERVER);
echo '</pre>';

echo '<p>The PHP $_REQUEST variable:</p>';
echo '<pre>';
\print_r($_REQUEST);
echo '</pre>';

echo '<p>The PHP INI variable:</p>';
echo '<pre>';
\print_r(ini_get_all());
echo '</pre>';

// Load the page header...
require __DIR__ . '/__footer.php';
