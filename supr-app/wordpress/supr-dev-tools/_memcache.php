<?php

// Load the page header...
require __DIR__ . '/__head.php';

echo '<h2>PHP Memcache Support Checker</h2><hr />';

/**
 * Function to test the memcache server connection.
 */
function TestMemcache()
{
    echo "<pre>\n";

    $sessionSavePath = ini_get('session.save_path');
    $sessionSaveHost = parse_url($sessionSavePath, PHP_URL_HOST);
    $sessionSavePort = parse_url($sessionSavePath, PHP_URL_PORT);

    echo "session.save_path: {$sessionSavePath}\n";
    echo "hostname: {$sessionSaveHost}\n";
    echo "port: {$sessionSavePort}\n";

    /*
    echo "\n > Memcache:\n";

    // Memcache
    $mTimeA = microtime(true);
    $memcache = new Memcache();
    $memcache->connect($sessionSaveHost, $sessionSavePort) or die('Could not connect Memcache');
    $memcache->set('test', 9223372036854775807);
    $memcache->get('test');
    echo "Version: {$memcache->getVersion()}\n";
    printf("Time: %.5f\n", microtime(true) - $mTimeA);
    */

    echo "\n > Memcached:\n";

    // Memcached
    $mTimeB = microtime(true);
    $memcached = new Memcached();
    $memcached->addServer($sessionSaveHost, $sessionSavePort) or die('Could not connect Memcached');
    $memcached->set('test', 9223372036854775807);
    $memcached->get('test');
    echo "Version: {$memcached->getVersion()[$sessionSavePath]}\n";
    printf("Time: %.5f\n", microtime(true) - $mTimeB);

    echo "</pre>\n";

    echo "<h2>Object handling test</h2>\n";

    $tmpObject = new stdClass();
    $tmpObject->str_attr = 'string';
    $tmpObject->int_attr = 9223372036854775807;
    $tmpObject->obj_attr = new stdClass();
    //$tmpObject->obj_attr = $tmpObject;

    /*
    echo "<p>Storing data in the Memcache (data will expire in 10 seconds)</p>\n";
    $memcache->set('tmpObject', $tmpObject, false, 10) or die('Failed to save data at the Memcache server');
    echo "<p>Data from the cache:</p>\n";
    echo '<pre>';
    var_dump($memcache->get('tmpObject'));
    echo '</pre>';
    */

    echo "<p>Storing data in the Memcached (data will expire in 10 seconds)</p>\n";
    $memcached->set('tmpObject', $tmpObject, 10) or die('Failed to save data at the Memcached server');
    echo "<p>Data from the cache:</p>\n";
    echo '<pre>';
    var_dump($memcached->get('tmpObject'));
    echo '</pre>';
}

TestMemcache();

// Load the page header...
require __DIR__ . '/__footer.php';
