<?php

/**
 * PHP Script to benchmark PHP and MySQL-Server
 * http://odan.github.io/benchmark-php/
 *
 * inspired by / thanks to:
 * - www.php-benchmark-script.com  (Alessandro Torrisi)
 * - www.webdesign-informatik.de
 *
 * @author  odan
 * @license MIT
 */

// -----------------------------------------------------------------------------
// Setup
// -----------------------------------------------------------------------------
\set_time_limit(120);

$options = [];

// Show or hide the server name and IP address
$showServerName = false;

// Optional: mysql performance test
$values = [];
$wpConfig = \file_get_contents(\dirname(__DIR__) . '/wp-config.php');
\preg_match_all("/define\('([^']+)', '([^']+)'\);/", $wpConfig, $matches);
$count = \count($matches[1]);

for ($i = 0; $i < $count; $i++) {
    $values[$matches[1][$i]] = $matches[2][$i];
}

$options['db.host'] = $values['DB_HOST'];
$options['db.user'] = $values['DB_USER'];
$options['db.pw'] = $values['DB_PASSWORD'];
$options['db.name'] = $values['DB_NAME'];

// Load the page header...
require __DIR__ . '/__head.php';

// -----------------------------------------------------------------------------
// Main
// -----------------------------------------------------------------------------
// check performance
$benchmarkResult = \test_benchmark($options);

// Render the HTML output
echo \print_benchmark_result($benchmarkResult, $showServerName);

// Load the page footer...
require __DIR__ . '/__footer.php';
exit;

// -----------------------------------------------------------------------------
// Benchmark functions
// -----------------------------------------------------------------------------

function test_benchmark($settings)
{
    $result = [];
    $result['version'] = '1.2';
    $result['sysinfo']['time'] = date('Y-m-d H:i:s');
    $result['sysinfo']['php_version'] = PHP_VERSION;
    $result['sysinfo']['platform'] = PHP_OS;
    $result['sysinfo']['server_name'] = $_SERVER['SERVER_NAME'];
    $result['sysinfo']['server_addr'] = $_SERVER['SERVER_ADDR'];
    $result['sysinfo']['xdebug'] = \in_array('xdebug', \get_loaded_extensions());

    $timeStart = \microtime(true);

    \test_math($result);
    \test_string($result);
    \test_loops($result);
    \test_ifelse($result);

    $result['benchmark']['calculation_total'] = \timer_diff($timeStart) . ' sec.';

    if (isset($settings['db.host'])) {
        \test_mysql($result, $settings);
    }

    $result['benchmark']['total'] = \timer_diff($timeStart) . ' sec.';

    return $result;
}

function test_math(&$result, $count = 999999)
{
    $timeStart = \microtime(true);
    $mathFunctions = ['abs', 'acos', 'asin', 'atan', 'decbin', 'floor', 'exp', 'sin', 'tan', 'pi', 'is_finite', 'is_nan', 'sqrt'];

    for ($i = 0; $i < $count; $i++) {
        foreach ($mathFunctions as $function) {
            if (\in_array($function, ['floor', 'sqrt'], true)) {
                \call_user_func_array($function, [(float)$i]);
            } else {
                \call_user_func_array($function, [$i]);
            }
        }
    }

    $result['benchmark']['math'] = \timer_diff($timeStart) . ' sec.';
}


function test_string(&$result, $count = 99999)
{
    $timeStart = \microtime(true);
    $stringFunctions = ['addslashes', 'chunk_split', 'metaphone', 'strip_tags', 'md5', 'sha1', 'strtoupper', 'strtolower', 'strrev', 'strlen', 'soundex', 'ord'];
    $string = 'the quick brown fox jumps over the lazy dog';

    for ($i = 0; $i < $count; $i++) {
        foreach ($stringFunctions as $function) {
            \call_user_func_array($function, [$string]);
        }
    }

    $result['benchmark']['string'] = \timer_diff($timeStart) . ' sec.';
}

function test_loops(&$result, $count = 999999)
{
    $timeStart = \microtime(true);

    for ($i = 0; $i < $count; ++$i) {
        // nothing to do!
    }

    $i = 0;

    while ($i < $count) {
        ++$i;
    }

    $result['benchmark']['loops'] = \timer_diff($timeStart) . ' sec.';
}

function test_ifelse(&$result, $count = 999999)
{
    $timeStart = \microtime(true);

    for ($i = 0; $i < $count; $i ++) {
        if (-1 === $i) {
            // nothing to do!
        } elseif (-2 === $i) {
            // nothing to do!
        } else {
            if (- 3 === $i) {
                // nothing to do!
            }
        }
    }

    $result['benchmark']['ifelse'] = \timer_diff($timeStart) . ' sec.';
}

function test_mysql(&$result, $settings)
{
    $timeStart = \microtime(true);

    $link = \mysqli_connect($settings['db.host'], $settings['db.user'], $settings['db.pw']);
    $result['benchmark']['mysql_connect'] = \timer_diff($timeStart) . ' sec.';

    \mysqli_select_db($link, $settings['db.name']);
    $result['benchmark']['mysql_select_db'] = \timer_diff($timeStart) . ' sec.';

    $dbResult = \mysqli_query($link, 'SELECT VERSION() as version;');
    $arr_row = \mysqli_fetch_array($dbResult);
    $result['sysinfo']['mysql_version'] = $arr_row['version'];
    $result['benchmark']['mysql_query_version'] = \timer_diff($timeStart) . ' sec.';

    $query = "SELECT BENCHMARK(1000000,ENCODE('hello',RAND()));";
    \mysqli_query($link, $query);
    $result['benchmark']['mysql_query_benchmark'] = \timer_diff($timeStart) . ' sec.';

    \mysqli_close($link);

    $result['benchmark']['mysql_total'] = \timer_diff($timeStart) . ' sec.';

    return $result;
}

function timer_diff($timeStart)
{
    return \number_format(\microtime(true) - $timeStart, 3);
}

function print_benchmark_result($data, $showServerName = true)
{
    $result = '<table cellspacing="0">';
    $result .= '<thead><tr><th>System Info</th><th></th></tr></thead>';
    $result .= '<tbody>';
    $result .= '<tr class="even"><td>Version</td><td>' . \h($data['version']) . '</td></tr>';
    $result .= '<tr class="even"><td>Time</td><td>' . \h($data['sysinfo']['time']) . '</td></tr>';

    if (!empty($data['sysinfo']['xdebug'])) {
        // You are running the benchmark with xdebug enabled. This has a major impact on runtime performance.
        $result .= '<tr class="even"><td>Xdebug</td><td><span style="color: darkred">' . \h('Warning: Xdebug is enabled!') . '</span></td></tr>';
    }

    $result .= '<tr class="even"><td>PHP Version</td><td>' . \h($data['sysinfo']['php_version']) . '</td></tr>';
    $result .= '<tr class="even"><td>Platform</td><td>' . \h($data['sysinfo']['platform']) . '</td></tr>';

    if ($showServerName == true) {
        $result .= '<tr class="even"><td>Server name</td><td>' . \h($data['sysinfo']['server_name']) . '</td></tr>';
        $result .= '<tr class="even"><td>Server address</td><td>' . \h($data['sysinfo']['server_addr']) . '</td></tr>';
    }

    $result .= '</tbody>';

    $result .= '<thead><tr><th>Benchmark</th><th></th></tr></thead>';
    $result .= '<tbody>';
    $result .= '<tr><td>Math</td><td>' . \h($data['benchmark']['math']) . '</td></tr>';
    $result .= '<tr><td>String</td><td>' . \h($data['benchmark']['string']) . '</td></tr>';
    $result .= '<tr><td>Loops</td><td>' . \h($data['benchmark']['loops']) . '</td></tr>';
    $result .= '<tr><td>If Else</td><td>' . \h($data['benchmark']['ifelse']) . '</td></tr>';
    $result .= '<tr class="even"><td>Calculation total</td><td>' . \h($data['benchmark']['calculation_total']) . '</td></tr>';
    $result .= '</tbody>';

    if (isset($data['sysinfo']['mysql_version'])) {
        $result .= '<thead><tr><th>MySQL</th><th></th></tr></thead>';
        $result .= '<tbody>';
        $result .= '<tr><td>MySQL Version</td><td>' . \h($data['sysinfo']['mysql_version']) . '</td></tr>';
        $result .= '<tr><td>MySQL Connect</td><td>' . \h($data['benchmark']['mysql_connect']) . '</td></tr>';
        $result .= '<tr><td>MySQL Select DB</td><td>' . \h($data['benchmark']['mysql_select_db']) . '</td></tr>';
        $result .= '<tr><td>MySQL Query Version</td><td>' . \h($data['benchmark']['mysql_query_version']) . '</td></tr>';
        $result .= '<tr><td>MySQL Benchmark</td><td>' . \h($data['benchmark']['mysql_query_benchmark']) . '</td></tr>';
        $result .= '<tr class="even"><td>MySQL Total</td><td>' . \h($data['benchmark']['mysql_total']) . '</td></tr>';
        $result .= '</tbody>';
    }

    $result .= '<thead><tr><th>Total</th><th>' . \h($data['benchmark']['total']) . '</th></tr></thead>';
    $result .= '</table>';

    return $result;
}

function h($v)
{
    return \htmlentities($v);
}
