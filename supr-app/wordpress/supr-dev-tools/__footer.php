</main>
<footer class="footer">
    <div class="container">
        <span class="text-muted">WordPress helper tools made with <span class="heart fa-pulse">❤</span> by <strong>SUPR</strong></span>
    </div>
</footer>
<style>
    .heart {
        color: red;
        font-size: 1.27rem;
    }
    /* Icon pulse */
    .fa-pulse {
        display: inline-block;
        -moz-animation: pulse 2s infinite linear;
        -o-animation: pulse 2s infinite linear;
        -webkit-animation: pulse 2s infinite linear;
        animation: pulse 2s infinite linear;
    }

    @-webkit-keyframes pulse {
        0% { opacity: 1; }
        50% { opacity: 0; }
        100% { opacity: 1; }
    }
    @-moz-keyframes pulse {
        0% { opacity: 1; }
        50% { opacity: 0; }
        100% { opacity: 1; }
    }
    @-o-keyframes pulse {
        0% { opacity: 1; }
        50% { opacity: 0; }
        100% { opacity: 1; }
    }
    @-ms-keyframes pulse {
        0% { opacity: 1; }
        50% { opacity: 0; }
        100% { opacity: 1; }
    }
    @keyframes pulse {
        0% { opacity: 1; }
        50% { opacity: 0; }
        100% { opacity: 1; }
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>
