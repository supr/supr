<?php

$sessionSelfStarted = false;

$mTimeA = \microtime(true);

if (\session_status() === PHP_SESSION_NONE) {
    \session_start();
    $sessionSelfStarted = true;
}

// Load the page header...
require __DIR__ . '/__head.php';

echo '<h2>PHP Session Support Checker</h2>' . "\n";
echo '<hr />' . "\n";
echo '<p>The php session status:</p>' . "\n";
echo '<ul>' . "\n";

if ($sessionSelfStarted) {
    echo "<li>Session was not auto started!</li>\n";
}

\printf("<li>Session start time: %.5f</li>\n", microtime(true) - $mTimeA);
echo '<li>Session ID: ' . \session_id() . "</li>\n";
echo '<li>Session status: ' . \session_status() . "</li>\n";
echo "<li>FYI:<br>\n";
echo '<ul>' . "\n";
echo '    <li>' . PHP_SESSION_DISABLED . ' - sessions are disabled (PHP_SESSION_DISABLED).</li>' . "\n";
echo '    <li>' . PHP_SESSION_NONE . ' - sessions are enabled, but none exists (PHP_SESSION_NONE).</li>' . "\n";
echo '    <li>' . PHP_SESSION_ACTIVE . ' - sessions are enabled, and one exists (PHP_SESSION_ACTIVE).</li>' . "\n";
echo '</ul>' . "\n";
echo '</li>' . "\n";
echo '</ul>' . "\n";

echo '<h2>Test sessions</h2>' . "\n";
echo '<p>';

// Check if the page has been reloaded
if (!isset($_GET['reload']) || $_GET['reload'] !== 'true') {
    // Set the message
    $_SESSION['MESSAGE'] = 'Session support is enabled!';

    // Give user link to check
    echo '<a href="?reload=true">Click HERE</a> to check for PHP Session Support.<br />';
} else {
    // Check if the message has been carried on in the reload
    if (isset($_SESSION['MESSAGE'])) {
        echo $_SESSION['MESSAGE'];
    } else {
        echo 'Sorry, the test message is not located at the session store. It appears session support is not enabled.';
    }

    echo ' <a href="?reload=false">Click HERE</a> to go back.';
}

echo '</p>' . "\n";

$mTimeB = \microtime(true);
\session_write_close();
\printf("<p>Session write close time: %.5f</p>\n", \microtime(true) - $mTimeB);

// Load the page header...
require __DIR__ . '/__footer.php';
