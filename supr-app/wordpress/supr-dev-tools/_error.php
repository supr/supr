<?php

// Load the page header...
require __DIR__ . '/__head.php';

echo '<h2>PHP Error Handling Checker</h2><hr />';

echo '<p>Triggering a "E_USER_NOTICE"!</p>';
\trigger_error('This is a "E_USER_NOTICE" error message!', E_USER_NOTICE);
echo '<p>Triggering a "E_USER_DEPRECATED"!</p>';
\trigger_error('This is a "E_USER_DEPRECATED" error message!', E_USER_DEPRECATED);
echo '<p>Triggering a "E_USER_WARNING"!</p>';
\trigger_error('This is a "E_USER_WARNING" error message!', E_USER_WARNING);
echo '<p>Triggering a "E_USER_ERROR"!</p>';
\trigger_error('This is a "E_USER_ERROR" error message!', E_USER_ERROR);

// Load the page header...
require __DIR__ . '/__footer.php';
