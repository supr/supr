#!/bin/bash

# include bash base
. _base.sh

# get action
action="$1"

# File for check if fixtures was already loaded
testFile="sunrise.php"

_dirContentFixtures="${_dirFixtures}/wp-content"
_dirContent="${_dirRoot}/wordpress/wp-content"

# For automated actions we need this script to shut up
if [ "$2" == "y" ] || [ "$2" == "silent" ]
then
    silent="$2"
else
    silent=""
fi

# Check if we have already fixtures on the storage
if ([ "$silent" == "y" ] || [ "$silent" == "silent" ]) && [ -f "$_dirContent/$testFile" ]
then
    echo "${underline}${green}The fixtures already exist!${reset}"
    exit 0
fi

if [ ! -d "${_dirContentFixtures}" ]
then
    mkdir -p ${_dirContentFixtures}
    chmod 777 ${_dirContentFixtures}
    echo "${underline}${green}The fixtures dump target directory at \"${_dirContentFixtures}\" created!${reset}"
fi

if [ "$action" == "dump" ]
then
    if [ "$silent" == "" ]
    then
        read -p "${orange}Dump the content from \"${_dirContent}\" to fixtures \"${_dirContentFixtures}\" directory (y/n)? ${reset}"
    fi

    if [ "$REPLY" == "y" ] || [ "$silent" == "y" ] || [ "$silent" == "silent" ]
    then
        rsync -auv --exclude=".*" --exclude-from 'exclude-list.txt' ${_dirContent}/ ${_dirContentFixtures}
        find ${_dirContentFixtures} -name '*.log' -type f -delete
        echo "${underline}${green}The content dump from \"${_dirContent}\" to fixtures \"${_dirContentFixtures}\" directory finished${reset}"
    fi
elif [ "${action}" == "load" ]
then
    if [ "$silent" == "" ]
    then
        read -p "${orange}Restore the fixtures from \"${_dirContentFixtures}\" to \"${_dirContent}\" directory (y/n)? ${reset}"
    fi

    if [ "$REPLY" == "y" ] || [ "$silent" == "y" ] || [ "$silent" == "silent" ]
    then
        # Before syncing the fixtures to the storage we change the dir mode to write able (omitting sub directories!)...
        find ${_dirContentFixtures}/ -type d -links 2 -exec chmod 0777 {} \;
        # We do the same for the included files...
        find ${_dirContentFixtures}/ -type f -exec chmod 0666 {} \;
        rsync -aruv --chmod=+rw --exclude='.*' --exclude-from 'exclude-list.txt' ${_dirContentFixtures}/ ${_dirContent}
        echo "${underline}${green}The fixtures from \"${_dirContentFixtures}\" successful loaded into the content at \"${_dirContent}\" directory!${reset}"
    fi
else
    echo -e "${bold}Options: dump | load${reset}"
fi
