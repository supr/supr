#!/bin/bash

# discover the directories...
_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
_dirRoot="$(dirname "${_dir}")"
_dirBackup="${_dirRoot}/backup"
_dirFixtures="${_dirRoot}/fixtures"

# include bash base...
. ${_dir}/_base.sh

echo "bin: ${_dir}"
echo "root: ${_dirRoot}"
echo "backup: ${_dirBackup}"
echo "fixtures: ${_dirFixtures}"
