#!/bin/bash

# include bash base
. _base.sh

# get action
action="$1"

_dirUploadsBackup="${_dirBackup}/uploads/sites"
_dirUploads="wordpress/wp-content/uploads/sites"

if [ ! -d ${_dirUploadsBackup} ]
then
    mkdir -p ${_dirUploadsBackup}
    chmod 777 ${_dirUploadsBackup}
    echo "${underline}${green}The backup directory created at \"${_dirUploadsBackup}\"${reset}"
fi

if [ "${action}" == "backup" ]
then
    read -p "${orange}Backup \"wp-content/uploads/sites\" (y/n)? ${reset}"
    if [ "$REPLY" == "y" ]
    then
        if [ -d "${_dirUploadsBackup}" ]
        then
            _now=$(date +"%Y%m%d_%H%M%S")
            _dirBlogsBackupHistory="${_dirUploadsBackup}~${_now}"

            read -p "${orange}Old backup directory of the \"${_dirUploads}\" backup found! Make a history of it to \"${_dirBlogsBackupHistory}\" (y) or it will be deleted (n)? ${reset}"
            if [ "$REPLY" == "y" ]
            then
                mv ${_dirUploadsBackup} ${_dirBlogsBackupHistory}
                echo "${underline}${green}The history of the \"${_dirUploads}\" directory created at \"${_dirBlogsBackupHistory}\"${reset}"
            else
                rm -rf ${_dirUploadsBackup}
                echo "${underline}${green}The backup directory deleted to recreate it at \"${_dirUploadsBackup}\"${reset}"
            fi
        fi

        if [ ! -d "${_dirUploadsBackup}" ]
        then
            mkdir -p ${_dirUploadsBackup}
            chmod 777 ${_dirUploadsBackup}
            echo "${underline}${green}The backup directory created at \"${_dirUploadsBackup}\"${reset}"
        fi

        read -p "${orange}Create a new backup of your \"${_dirUploads}\" directory to \"${_dirUploadsBackup}\" directory (y/n)? ${reset}"
        if [ "$REPLY" == "y" ]
        then
            rsync -rtv ${_dirUploads}/ ${_dirUploadsBackup}
            find ${_dirUploadsBackup} -name '*.log' -type f -delete
            echo "${underline}${green}The backup was created from \"${_dirUploads}\" to \"${_dirUploadsBackup}\" directory${reset}"
        fi
    fi
elif [ "${action}" == "restore" ]
then
    read -p "${orange}Restore the \"${_dirUploads}\" directory (y/n)? ${reset}"
    if [ "$REPLY" == "y" ]
    then
        rsync -rtv ${_dirUploadsBackup}/ ${_dirUploads}
        echo "${underline}${green}The \"${_dirUploads}\" directory was restored from \"${_dirUploadsBackup}\" to \"${_dirUploads}\" directory${reset}"
    fi
else
    echo -e "${bold}Options: backup | restore${reset}"
fi
