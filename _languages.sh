#!/bin/bash

PWD_NOW="$(pwd)"

PLUGIN_DIRECTORY=$1

# Check the plugin directory existance...
[ "$PLUGIN_DIRECTORY" == "" ] && { echo "${underline}${red}The required plugin directory is not given! Generating the po files is not possible!${reset}"; exit 1; }
[ ! -d "$PLUGIN_DIRECTORY" ] && { echo "${underline}${red}The selected plugin directory \"$PLUGIN_DIRECTORY\" doesn't exist!${reset}"; exit 1; }

# Change to the plugin directory for better find operations...
cd "$PLUGIN_DIRECTORY" || exit

PLUGIN_DIRECTORY=$PWD
PLUGIN_DOMAIN=$(basename "$PLUGIN_DIRECTORY")
PLUGIN_LANGUAGES_DIRECTORY="$(readlink -f "$PLUGIN_DIRECTORY"/languages)"

# @todo If no languages directory exists, we can create a new one?
[ ! -d "$PLUGIN_DIRECTORY" ] && { echo "${underline}${red}The languages directory inside the \"$PLUGIN_DOMAIN\" plugin not found!${reset}"; exit 1; }

LANGUAGES='en_US en_GB de_DE de_AT'

echo "WordPress translation files will be generated now:"
echo ""
echo "    * Plugin: $PLUGIN_DOMAIN"
echo "    * Directory: $PLUGIN_DIRECTORY"
echo "    * Translations: $PLUGIN_LANGUAGES_DIRECTORY"
echo ""

read -p "${orange}Do you want to continue (y/n)? ${reset}"
if [[ "$REPLY" != "y" ]]
then
    echo "${underline}${red}The generation of language files aborded!${reset}"
    exit 1
fi
