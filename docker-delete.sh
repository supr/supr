#!/bin/bash

# include bash base
. _base.sh

read -p "${orange}Stop and delete all Docker containers, images, volumes and networks (y/n)? ${reset}"
if [[ "$REPLY" != "y" ]]
then
    exit 1;
fi

./docker-stop.sh

read -p "${orange}Delete all containers (y/n)? ${reset}"
if [[ "$REPLY" == "y" ]]
then
    docker rm -v $(docker ps -a -q)
fi

read -p "${orange}Delete all images (y/n)? ${reset}"
if [[ "$REPLY" == "y" ]]
then
    docker rmi -f $(docker images -q)
fi

read -p "${orange}Delete all volumes and networks (y/n)? ${reset}"
if [[ "$REPLY" == "y" ]]
then
    docker system prune --all --force --volumes
fi
