#!/bin/bash

# include bash base
. _base.sh

# include mysql configuration
. _mariadb.sh

# get action
action="$1"

_dirMariaDB="${_dirRoot}/${MyDockerHostName}"
_dirMariaDBFixtures="${_dirRoot}/${MyDockerHostName}/fixtures"

_file="$2"

if [ "$_file" == "" ]
then
    _file="${MyDATABASE}.sql"
fi

if [ ! -d ${_dirMariaDBFixtures} ]
then
    mkdir -p ${_dirMariaDBFixtures}
    chmod 777 ${_dirMariaDBFixtures}
    echo "${orange}The dump directory created at \"${_dirMariaDBFixtures}\"${reset}"
fi

if [ "$action" == "dump" ]
then
    read -p "${orange}Dump the MariaDB data into \"${_dirMariaDBFixtures}/${_file}\" file (y/n)? ${reset}"
    if [ "$REPLY" == "y" ]
    then
        if [ "$(docker ps -a | grep ${MyDockerHostName})" ]
        then
            echo "$ docker exec ${MyDockerHostName} /usr/bin/mysqldump --extended-insert=FALSE -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} > ${_dirMariaDBFixtures}/${_file}"
            docker exec ${MyDockerHostName} /usr/bin/mysqldump --extended-insert=FALSE -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} > ${_dirMariaDBFixtures}/${_file}
        else
            echo "$ mysqldump --extended-insert=FALSE -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} > ${_dirMariaDBFixtures}/${_file}"
            mysqldump --extended-insert=FALSE -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} > ${_dirMariaDBFixtures}/${_file}
        fi
        echo "${underline}${green}The MariaDB dump into \"${_dirMariaDBFixtures}/${_file}\" finished${reset}"
    fi
elif [ "${action}" == "load" ]
then
    read -p "${orange}Load the MariaDB data from \"${_dirMariaDBFixtures}/${_file}\" file (y/n)? ${reset}"
    if [ "$REPLY" == "y" ]
    then
        if [ -f "${_dirMariaDBFixtures}/${_file}" ]
        then
            if [ "$(docker ps -a | grep ${MyDockerHostName})" ]
            then
                echo "$ cat ${_dirMariaDB}/drop-tables.sql | docker exec -i ${MyDockerHostName} /usr/bin/mysql -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE}"
                cat ${_dirMariaDB}/drop-tables.sql | docker exec -i ${MyDockerHostName} /usr/bin/mysql -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE}
                echo "$ cat ${_dirMariaDBFixtures}/${_file} | docker exec -i ${MyDockerHostName} /usr/bin/mysql -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE}"
                cat ${_dirMariaDBFixtures}/${_file} | docker exec -i ${MyDockerHostName} /usr/bin/mysql -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE}
            else
                echo "$ mysql -A -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} < ${_dirMariaDB}/drop-tables.sql"
                mysql -A -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} < ${_dirMariaDB}/drop-tables.sql
                echo "$ mysql -A -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} < ${_dirMariaDBFixtures}/${_file}"
                mysql -A -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} < ${_dirMariaDBFixtures}/${_file}
            fi
            echo "${underline}${green}The MariaDB was dumped into \"${_dirMariaDBFixtures}/${_file}\"${reset}"
        else
            echo -e "${underline}${red}The MariaDB dump file not fount at \"${_dirMariaDBFixtures}/${_file}\"!${reset}"
        fi
    fi
elif [ "${action}" == "drop" ]
then
    read -p "${orange}Delete all existing tables inside the MariaDB (y/n)? ${reset}"
    if [ "$REPLY" == "y" ]
    then
        if [ "$(docker ps -a | grep ${MyDockerHostName})" ]
        then
            echo "$ cat ${_dirMariaDB}/drop-tables.sql | docker exec -i ${MyDockerHostName} /usr/bin/mysql -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE}"
            cat ${_dirMariaDB}/drop-tables.sql | docker exec -i ${MyDockerHostName} /usr/bin/mysql -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE}
        else
            echo "$ mysql -A -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} < ${_dirMariaDB}/drop-tables.sql"
            mysql -A -h ${MyHOST} -u ${MyUSER} -p${MyPASSWORD} ${MyDATABASE} < ${_dirMariaDB}/drop-tables.sql
        fi
        echo "${underline}${green}All MariaDB tables was doped!${reset}"
    fi
else
    echo -e "${bold}Options: dump | load | drop ${reset}"
fi
