# SUPR Network

The current repository holds the sources for the [SUPR Network](https://supr.network/), a multi-tenant e-commerce shop platform application basing on WordPress and WooCommerce and it's provided on [Amazon AWS](https://aws.amazon.com/) the [Elastic Beanstalk](https://eu-central-1.console.aws.amazon.com/elasticbeanstalk/).

## Dependencies

1. Linux based operating system like [Ubuntu](https://ubuntu.com/download/desktop), [Xubuntu](https://xubuntu.org/download/)
1. Shell extension like [zsh](https://www.zsh.org/) with [oh-my-zsh](https://ohmyz.sh/): [docs/oh-my-zsh.md](docs/oh-my-zsh.md)
1. [PHP](https://www.php.net/) [CLI](https://www.php.net/manual/en/features.commandline.php) for local development: [docs/php.md](docs/php.md)
1. Environment runtime [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/): [docs/docker.md](docs/docker.md) & [docs/docker-compose.md](docs/docker-compose.md)

_**Note:** All in the following documentations given information are basing on **Ubuntu** and the project structure located at `/var/www` of your local installation!_

1. To get involved into the development of this project you need to get a local copy of this repository:

    ```shell
    git clone git@gitlab.com:supr/supr.git
    cd supr
    ```

1. Create a local environment configuration:

    ```shell
    ./env.sh
    ```

   Answer the questions:

   * Enter environment slug: `supr`
   * Enter your domain: `supr.network`

    **Note:** the `.env` file holds the following main environment variables:

    * The `HOST_UID` and `HOST_GID` of your local user and this ID's are used inside the container to get access to the volumes with right permissions.
    * The `ENV_SLUG` a slug name for the environment to separate his from other environments.
    * The `MYSQL_HOST`, `MYSQL_DATABASE`, `MYSQL_USER` and `MYSQL_PASSWORD` to get access to the database.
    * The `OPCACHE_VALIDATE_TIMESTAMPS` to activate `1` or deactivate `0` the opcache rebuild by each request. Set this to `0` in development mode.
    * The `DOMAIN_CURRENT_SITE` the main domain for this environment and application.

1. Generate SSL Certificates:

    ```shell
    ./ssl.sh
    ```

1. After the `.env` file and SSL certificates were generated, the Docker Compose environment can be started:

    ```shell
    docker-compose -p supr up --build
    ```

    _Alternative_, start the Docker Compose environment with the helper script: 

    ```shell
    ./docker-compose.sh
    ```

    Answer the questions:

    ```
    Start the Docker Compose environment (y/n)? y
    Run the Docker Compose environment with -d option in detached mode (y/n)? n
    Run the Docker Compose environment with --build option (y/n)? y
    Run the Docker Compose environment with --force-recreate option (y/n)? n
    ```

1. To get access to the local environment, first add the list of existing shops into local hosts configuration inside `/etc/hosts` file:

    ```shell
    sudo nano /etc/hosts
    ```

    Add following list of domains:

    ```
    127.0.0.1       supr.network
    ```

    _Alternatively_, use the helper script to add the entry to your `/etc/hosts` file:

    ```shell
    ./hosts.sh
    ```

1. Now open one of the shop by accessing following domains:

    * [https://supr.network/](https://supr.network/network)
    
    _**NOTE:** we use a self signed certificate for the local environment, so the SSL certificate warning can be ignored / accepted!_

For more details and troubleshooting consult the [docs/installation.md](docs/installation.md) for installation instructions.

## Branching Model

In this project we decided to use Gitflow Workflow is a Git workflow design that was first published and made popular [by Vincent Driessen at nvie](http://nvie.com/posts/a-successful-git-branching-model/).

To start working with Gitflow make sure it is installed:

```shell
apt-get install git-flow
```

After a first checkout is required to initialize git flow before starting working with it.

```shell
git checkout main
git flow init -fd
```

_**Note:** Because the `develop` branch is currently configured as the default branch it is required to switch into `main` branch before Gitflow initialization!_

Main rules we've defined so far:

1. No commits into the `main` branch allowed!
2. All small work, one commit or small features, can be committed into `develop` branch but functionality should not be broken!
3. All greater feature should be placed into feature branches. All feature branches should reference to Jira Epic or Issue references by the Jira id.

    ```shell
    git flow feature start {JIRA-ID}-ticket-title
    ```

For more information following sources will be help full:

* [Gitflow Workflow description by Atlassian](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
* [Gitflow cheatsheet](https://danielkummer.github.io/git-flow-cheatsheet/)
* [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/) ([by Vincent Driessen from nvie.com](https://nvie.com/))
* [Gitflow Command Line Arguments](https://github.com/nvie/gitflow/wiki/Command-Line-Arguments)

Initialize or recreate the GIT-Flow prefixes:

```shell
# Init all default branches:
git flow init -fd

# Init all default branches by naming them:
git flow init -d --feature feature/ --bugfix bugfix/ --release release/ --hotfix hotfix/ --support support/ -t ''

# Restore all default branches by naming them:
git config gitflow.prefix.hotfix "hotfix/"
git config gitflow.prefix.feature "feature/"
git config gitflow.prefix.bugfix "bugfix/"
git config gitflow.prefix.release "release/"
git config gitflow.prefix.support "support/"
```

## Versions

[To get more information's about the versions and releases read the VERSION.md](VERSION.md)

The current version number schema is following the [Semantic Versioning](https://semver.org/)

The format: X.Y.Z-a.n

Example for a full formatted version: 3.10.0-rc.3

* **X** = major = breaking changes, several depended services touched, full-test needed, new feature
* **Y** = minor = all other things (improvements, bug fixes, etc.)
* **Z** = hotfix = it can hold only hotfixes with very small changes
* **a** = release state = `alpha` / `beta` / `rc` (release candidate)
* **n** = release version = iteration of `alpha` / `beta` / `rc` (release candidate)

Note: If we have to go back from a `rc` to a `beta`, it will be `beta2`.
