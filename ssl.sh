#!/bin/bash

# include bash base
. _base.sh

# Generate a SSL certificate
# Special feature: we sign the certificate ourselves and it is generated without a passphrase!
# Attention! DO NOT use this script on a production environment! This is only a helper for local development environment!
# @link http://blog.justin.kelly.org.au/how-to-create-a-self-sign-ssl-cert-with-no-pa/
if sudo test -f "./config/certs/supr.network.key"
then
    echo "${underline}${green}The SSL Cert for the Web-Server exists!${reset}"
else
    sudo openssl genrsa -out ./config/certs/supr.network.key 1024
    #sudo openssl req -new -key ./config/certs/supr.network.key -out ./config/certs/supr.network.csr -config ./server/openssl.cnf
    #sudo openssl req -new -x509 -extensions v3_ca -keyout ./config/certs/supr.network.key -out /etc/ssl/certs/supr.network.pem -days 365 -config ./server/openssl.cnf
    sudo openssl req -nodes -newkey rsa:2048 -key ./config/certs/supr.network.key -out ./config/certs/supr.network.csr -days 365 -config ./config/openssl/openssl.cnf -subj "/C=DE/ST=NRW/L=Cologne/O=SUPR/OU=SUPR-Network/CN=*.supr.network"
    sudo openssl x509 -req -days 365 -in ./config/certs/supr.network.csr -signkey ./config/certs/supr.network.key -out ./config/certs/supr.network.crt

    #openssl req -x509 -out localhost.crt -keyout localhost.key \
    #  -newkey rsa:2048 -nodes -sha256 \
    #  -subj '/CN=localhost' -extensions EXT -config <( \
    #   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")

    sudo chmod 660 ./config/certs/supr.network.crt
    sudo chmod 660 ./config/certs/supr.network.csr
    sudo chmod 660 ./config/certs/supr.network.key

    echo "${underline}${green}The SSL Cert for the Web-Server created!${reset}"
fi
