#!/bin/bash

# include bash base
. _base.sh

read -p "${orange}Stop all running containers (y/n)? ${reset}"
if [[ "$REPLY" == "y" ]]; then
    docker stop $(docker ps -a -q)
fi
