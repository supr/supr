#!/bin/bash

# include bash base
. _base.sh

# The environment config file is required for current Docker Compose configuration
if [ ! -f .env ]
then
    ./env.sh
fi

# Load the environment config file
set -a
[ -f .env ] && . .env
set +a

read -p "${orange}Start the Docker Compose environment (y/n)? ${reset}"
if [[ "$REPLY" != "y" ]]
then
    echo "${underline}${magenta}Aborting the building and starting of the Docker Compose environment!${reset}"
    exit 1
fi

# Get a prefix for the whole environment
DockerComposePrefix="$1"

if [[ "$DockerComposePrefix" == "" ]]
then
    if [ -z "${ENV_SLUG}" ]
    then
        echo "${underline}${magenta}Please set a prefix for the Docker Compose environment!${reset}"
        exit 1
    else
        DockerComposePrefix="${ENV_SLUG}"
        echo "${green}The Docker Compose prefix isn't given and the slug \"${ENV_SLUG}\" from .env file will be used!${reset}"
    fi
fi

# Get the Docker Compose action
DockerComposeOptions="$2"

if [[ "${DockerComposeOptions}" == "" ]]
then
    echo "Usage:"
    echo "    docker-compose.sh [options]"
    echo ""
    echo "Options:"
    echo "   -d, --detach     Detached mode: Run container in the background, print new container name."
    echo "   --build          Start the Docker Compose environment with the build option"
    echo "   --force-recreate Force recreate of all Docker containers during the start of the Docker Compose environment"

    read -p "${orange}Run the Docker Compose environment with ${underline}-d${nounderline} option in detached mode (y/n)? ${reset}"
    if [[ "$REPLY" == "y" ]]
    then
        DockerComposeOptions="${DockerComposeOptions}-d "
        echo "${underline}${blue}Docker Compose with${nounderline} -d ${underline}option to run detached mode added!${reset}"
    fi

    read -p "${orange}Start the Docker Compose environment with ${underline}--build${nounderline} option (y/n)? ${reset}"
    if [[ "$REPLY" == "y" ]]
    then
        DockerComposeOptions="${DockerComposeOptions}--build "
        echo "${underline}${blue}Docker Compose${nounderline} --build ${underline}option added!${reset}"
    fi

    read -p "${orange}Start the Docker Compose environment with ${underline}--force-recreate${nounderline} option (y/n)? ${reset}"
    if [[ "$REPLY" == "y" ]]
    then
        DockerComposeOptions="${DockerComposeOptions}--force-recreate "
        echo "${underline}${blue}Docker Compose${nounderline} --force-recreate ${underline}option added!${reset}"
    fi
fi

echo "${green}The Docker Compose environment is starting now!${reset}"
echo "$ docker-compose -p ${DockerComposePrefix} up ${DockerComposeOptions}${reset}"
docker-compose -p ${DockerComposePrefix} up ${DockerComposeOptions}
echo "${underline}${green}Building and starting the Docker Compose environment finished!${reset}"
