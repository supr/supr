# Docker Compose

Install the Docker-Compose daemon local:

```shell
sudo apt-get -y install docker-compose
```

After making changes specially inside the Docker configuration it is necessary to rebuild the environment:

```shell
sudo docker-compose up --force-recreate
```

Stop all running Docker Compose services:

```shell
sudo docker-compose stop
```

Delete all running Docker Compose services:

```shell
sudo docker-compose rm
```

## Cheat sheet

Access bash inside the php images:

```shell
docker-compose exec php bash
```

Run a command exc. `composer update`:

```shell
docker-compose exec php composer update
```

Run a Symfony command:

```shell
docker-compose exec php php /var/www/symfony/app/console cache:clear
```

Run Symfony commands with an alias = plus simple!:

```shell
docker-compose exec php bash
sf cache:clear
```

Execute a MySQL command:

```shell
docker-compose exec db mysql -uroot -p"password"
```

Execute a Redis command:

```shell
docker-compose exec redis redis-cli
```
