# PHP

**How to install PHP 7.4 on Ubuntu 20.04**

Add `ondrej/php` which has PHP 7.4 package and other required PHP extensions.

```shell
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
```

This PPA can be added to your system manually by copying the lines below and adding them to your system’s software sources.

Once the PPA repository has been added, install php 7.4 on your server.

```shell
sudo apt-get install php7.4
```

Install PHP 7.4 with extensions:

```shell
sudo apt install apache2 libapache2-mod-php7.4 php7.4 php7.4-cli php7.4-fpm php7.4-pdo php7.4-mysql php7.4-pgsql php7.4-sqlite3 php7.4-intl php7.4-bc php7.4-gd php7.4-mb php7.4-dom php7.4-json php7.4-xml php7.4-zip php7.4-bz2 php7.4-bcmath php7.4-curl php7.4-soap
```

Install PHP extensions:

```shell
sudo apt-get install php-common php-mysql php-curl php-gd php-pear php-imagick php-imap php-memcache php-memcached php-mongodb php-pspell php-tidy php-xmlrpc php-json php-pgsql
```

## PHP 7.3

Install PHP 7.3 with extensions:

```shell
sudo apt install apache2 libapache2-mod-php7.3 php7.3 php7.3-cli php7.3-fpm php7.3-pdo php7.3-mysql php7.3-pgsql php7.3-sqlite3 php7.3-intl php7.3-bc php7.3-gd php7.3-mb php7.3-dom php7.3-json php7.3-xml php7.3-zip php7.3-bz2 php7.3-bcmath php7.3-curl php7.3-soap
```

## PHP 7.2

Install PHP 7.2 with extensions:

```shell
sudo apt install apache2 libapache2-mod-php7.2 php7.2 php7.2-cli php7.2-fpm php7.2-pdo php7.2-mysql php7.2-pgsql php7.2-sqlite3 php7.2-intl php7.2-bc php7.2-gd php7.2-mb php7.2-dom php7.2-json php7.2-xml php7.2-zip php7.2-bz2 php7.2-bcmath php7.2-curl php7.2-soap
```

## PHP 7.1

Install PHP 7.1 with extensions:

```shell
sudo apt-get install php7.1 php7.1-cli php7.1-fpm php7.1-json php7.1-pdo php7.1-mysql php7.1-zip php7.1-gd php7.1-mbstring php7.1-curl php7.1-xml php7.1-bcmath php7.1-json php7.1-intl
```

## PHP 7.0

Install PHP 7.0 with extensions:

```shell
sudo apt-get install apache2 libapache2-mod-php7.0 php7.0-pdo php7.0-mysql php7.0-pgsql php7.0-sqlite3 php-memcache php-memcached php-mongodb php-imagick php7.0-intl php7.0-bc php7.0-gd php7.0-mb php7.0-mc php7.0-dom php7.0-json php7.0-xml php7.0-zip php7.0-bz2 php7.0-bcmath php7.0-curl php7.0-soap
```

## PHP 5.6

From php5.6 to php7.0 :

```shell
sudo a2dismod php5.6 ; sudo a2enmod php7.0 ; sudo service apache2 restart
sudo update-alternatives --set php /usr/bin/php7.0
```

From php7.0 to php5.6 :

```shell
sudo a2dismod php7.0 ; sudo a2enmod php5.6 ; sudo service apache2 restart
sudo update-alternatives --set php /usr/bin/php5.6
```

## PHP modules installation

```shell
sudo apt-get install apache2 php-common php-mysql php-curl php-gd php-pear php-imagick php-imap php-mcrypt php-memcache php-pspell php-recode php-tidy php-xmlrpc php-xsl php-json php-pgsql
```
