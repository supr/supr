# DOCKER

**We assume that the development environment will run on Ubuntu.**

- [https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)

To run docker with your local user you need to add your user to the docker group.

Add the docker group if it doesn't already exist:

```shell
sudo groupadd docker
```

Add the connected your local user "$USER" to the docker group. Change the user name to match your preferred user if you do not want to use your current user:

```shell
sudo usermod -aG docker $(whoami)
sudo gpasswd -a $USER docker
```

Either do a ```newgrp docker``` or log out/in to activate the changes to groups.

## Build Docker Image

```shell
docker build -t supr .
```

If you want to rebuild a container defined in the Docker Compose configuration:

```shell
docker-compose up -d --no-deps --build supr-nginx-proxy
```

Execute the container image: 

```shell
docker run -d -v /wordpress:/var/www/html -p 8080:80 -p 8443:443 --name supr supr
```

More variable image with links to local storage:

```shell
docker run -d -v /wordpress:/var/www/html -v /ssl/certs:/etc/nginx/certs -v /etc/sites/etc:/etc/nginx/sites-enabled -p 8080:80 -p 8443:443 --name mysupr mysupr_wordpress
```

## Test installation

Test docker by running a simple Ubuntu server:

```shell
docker pull ubuntu
```

Run a local instance:

```shell
docker run -i -t ubuntu:20.04 /bin/bash
# Old and not working 🤔:
#docker run -i -t ubuntu:20.04 /bin/bash ping 8.8.8.8
#docker run -i -t ubuntu:20.04 ping 8.8.8.8
```

Recommendation is to take Ubuntu as base image for you own image, update it first and then install your packages like `ping`:

```shell
apt update && apt upgrade && apt dist-upgrade
apt install -y iputils-ping
apt auto-clean && apt auto-remove
ping 8.8.8.8
```

## Cheat sheet

List all local images:

```shell
docker images
```

Get a list of all running images:

```shell
docker ps -a       
```

Alternative command:

```shell
docker container ls
```

Check the CPU power consumption of all running docker images:

```shell
docker stats $(docker inspect -f "" $(docker ps -q))
```

Stop **all** containers:

```shell
docker stop $(docker ps -a -q)
```

Delete **all** containers:

```shell
docker rm $(docker ps -a -q)
```

Delete **all** images:

```shell
docker rmi $(docker images -q)
```
