# SURP Handbook

In this docs you will find a description how to setup and administrate the environment and application:

## Infrastructure

1. [Installation](installation.md)
    1. [Docker](docker.md) and [Docker Compose](docker-compose.md)

## Repository

* Working with [git](git.md) and [git flow](git-flow.md)
