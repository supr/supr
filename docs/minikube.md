# Minikube

Minikube is a tool that makes it easy to run Kubernetes locally. Minikube runs a single-node Kubernetes cluster inside a VM on your laptop for users looking to try out Kubernetes or develop with it day-to-day.

As the [README](https://github.com/kubernetes/minikube/blob/0c616a6b42b28a1aab8397f5a9061f8ebbd9f3d9/README.md#reusing-the-docker-daemon) describes, you can reuse the Docker daemon from Minikube with `eval $(minikube docker-env)`.

Further information:

* [Development on Minikube and DevOps](https://medium.com/devopslinks/using-kubernetes-minikube-for-local-development-c37c6e56e3db)
* [Intro to Minikube and Kubernetes Entities](https://codefresh.io/kubernetes-tutorial/intro-minikube-kubernetes-entities/)
    * [Demo Chat Application](https://github.com/containers101/demochat)

## Installation

Installing [kubectl](https://kubernetes.io/docs/getting-started-guides/kubectl/):

```bash
curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && chmod +x kubectl && sudo mv kubectl /usr/local/bin/
```

Installing [minikube](https://github.com/kubernetes/minikube/releases):

```bash
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/
```

## Starting

Start **minikube**:

```bash
minikube start
```

Start **minikube** with defined resources:

```bash
minikube start --cpus=2 --memory=4096
```

## Run Test Service

Run the "hello-minikube" service inside **minikube**:

```bash
kubectl run hello-minikube --image=k8s.gcr.io/echoserver:1.10 --port=8080
```

Make the "hello-minikube" service exposing it's service:

```bash
kubectl expose deployment hello-minikube --type=NodePort
```

We have now launched an echoserver pod but we have to wait until the pod is up before curling/accessing it via the exposed service.

## Service View

Check the status of the cluster:

```bash
minikube status
```

View the a detailed configuration of the current cluster:

```bash
kubectl config view
```

To check whether the pod is up and running we can use the following:

```bash
kubectl get pod
```

To check all running services (including the owen service by **minikube**):

```bash
kubectl get pods --all-namespaces
```

Get some status infos:

```bash
kubectl get componentstatus
kubectl cluster-info
```

## Dashboard

```bash
minikube dashboard
```

If the dashboard is not available, then we need to build a new one:

```bash
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```

Alternative method is to use the proxy:

```bash
kubectl proxy
```

Open the localhost proxy on port 8001 in your browser: [http://localhost:8001/ui/](http://localhost:8001/ui/)

Open the dashboard: [http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/)

## Scale Up

However, we can use “kubectl scale deployment” to increase the number of WordPress containers because they are using a Portworx “shared” volume.

Here is the command to increase WordPress to 10 containers:

```bash
kubectl scale deployment wordpress --replicas 10
```

## Manage Minikube Add Ons

Minikube provides a set of built-in add-ons and some of them such as dashboard is enabled by default. These add-ons that can be enabled, disabled, or opened inside of the local Kubernetes environment.

```bash
minikube addons list
```

### Minikube Registry

* [Sharing a local registry with minikube](https://blog.hasura.io/sharing-a-local-registry-for-minikube-37c7240d0615)

```bash
minikube addons enable registry
```

If you want to use a external registry (but not needed by now!):

```bash
minikube addons configure registry-creds
minikube addons enable registry-creds
```

## Minikube Cluster Access

Configure kubectl inside development machine to access minikube cluster run the following, on the host, to determine the user access token to minikube cluster.

```
kubectl describe secrets
Name:           default-token-xj982
...
token:          eyJhb...<redacted>...
```

Copy the token's value as $minikube-token and then proceed to configure kubectl, in the development machine, to use the minikube cluster:

```bash
kubectl config set-cluster minikube -server=https://$minikube-ip:8443 --insecure-skip-tls-verify=true
kubectl config set-credentials minikube --token=$minikube-token
kubectl config set-context minikube --cluster=minikube --user=minikube
kubectl config use-context minikube
```

Run kubectl get nodes inside the development virtual machine to confirm the configuration has been set correctly.
