# Environment Installation and Setup 

There multiple ways to get the application running:

1. (**DONE**) _Alternative_, install a local web-, database and caching server
2. (**DONE**) Use Docker and Docker Compose to serve the application
3. (**TODO**) we serve the application with Kubernetes and local with Minikube
4. (**TODO**) we serve a test environment on AWS

## Initial setup

**This manual describes how to install a fully functional environment - beginning on a blank system.**

First update, upgrade and install the host system:

```shell
apt update && apt upgrade && apt dist-upgrade
apt install apt-transport-https ca-certificates software-properties-common curl git
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install docker-ce
curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
apt autoremove
```

_**Note:** to be save, check the Docker Compose version: ```docker-compose --version```_

The easiest way to get the project running is to build a **Docker** image and run the **Docker Compose** environment.

_**Note:** Make sure you have the latest versions of **Docker** and **Docker Compose** installed on your machine._

_**Note:** Because of permission problems inside the containers we need to run the environment with **sudo**._

Before we start with install, we need to free ports. for that you need to stop running processes:

```shell
sudo service apache2 stop & sudo service mysqld stop & sudo service memcached stop & sudo service nginx stop
```

## Deployment of the application

1. Generate SSL Certificates:

    ```shell
    ./ssl.sh
    ```

1. Generate the Docker Compose environment file:

    ```shell
    ./docker-compose.sh
    ```

1. Build the Docker containers and bring them up:

    ```shell
    docker-compose up -d --build
    ```

    Get a process list of all Docker services:

    ```shell
    docker ps -a
    ```

    The output should look like this:

    ```
    CONTAINER ID        IMAGE                             COMMAND                  CREATED             STATUS              PORTS                                      NAMES
    85bde35dc182        supr2_supr-app                      "./docker-entrypoint…"   9 minutes ago       Up 9 minutes        80/tcp                                     supr-app
    fe9f42f1d9c0        supr2_supr-cert-manager             "python3 api.py"         9 minutes ago       Up 9 minutes        5000/tcp                                   supr-cert-manager
    a4bd85fb966e        supr2_supr-nginx                    "/files/init.sh"         10 minutes ago      Up 9 minutes        0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   supr-nginx
    cd98a79da3fc        mariadb                           "docker-entrypoint.s…"   10 minutes ago      Up 9 minutes        3306/tcp                                   supr-mariadb
    d91b7e36de06        memcached                         "docker-entrypoint.s…"   10 minutes ago      Up 9 minutes        11211/tcp                                  supr-memcached
    ```

    **Note:** for more information consider **[Docker.md](docker.md)** and **[Docker-Compose.md](docker-compose.md)** readme files. 

1. Deploy the application:

    _**Note:** the Docker Compose configuration is mounting the local ```./wordpress``` directory into the container. In that case the deployment should be executed on the local host to!_

    ```shell
    composer install
    ```

1. Create the WordPress configuration:

    _**Note:** the Docker Compose configuration is mounting the local ```./wordpress``` directory into the container. In that case the WordPress configuration should be created again!_

    ```shell
    cp supr-app/wordpress/wp-config-sample.php supr-app/wordpress/wp-config.php
    ```

    Than this file need to be edit (example: ```nano supr-app/wordpress/wp-config.php```) and following content need to be added:

    ```php
    // ** MySQL settings - You can get this info from your web host ** //
    /** The name of the database for WordPress */
    define('DB_NAME', 'supr2_de');
    
    /** MySQL database username */
    define('DB_USER', 'supr2_de');
    
    /** MySQL database password */
    define('DB_PASSWORD', 'qwer1234');
    
    /** MySQL hostname */
    define('DB_HOST', 'supr-mariadb');
    ```

1. **WordPress upload file fixtures**

    Load the file fixtures:

    ```shell
    ./fixtures-uploads.sh load
    ```

    _**Note:** In some cases (at this moment we don't know why) the upload directory owner isn't the same like the user on the host. In this case the file upload isn't working!_

    1. Fix file and directory mod

        ```shell
        find supr-app/wordpress/wp-content/uploads -type d -exec chmod 0777 {} \;
        find supr-app/wordpress/wp-content/uploads -type f -exec chmod 0666 {} \;
        ```

    1. Fix the directory owner

        ```shell
        chown www-data:www-data supr-app/wordpress/wp-content/uploads -R
        ```

1. **WordPress database fixtures**

    _**Note:** The database fixtures are loaded during the first build. But it is possible and needed to load them sometimes again!_

    1. Load database fixtures with a helper script:

        ```shell
        ./fixtures-mariadb.sh load
        ```

    1. Delete MariaDB data and rebuild the container:

        ```shell
        docker stop supr-mariadb
        docker rm supr-mariadb
        rm data/mariadb/* -rf
        sudo docker-compose up -d --build --force-recreate supr-mariadb
        ```

1. Domain Setup

    Connect the Domain:

    ```shell
    nano /etc/hosts
    ```

    Add following rule:

    ```
    127.0.0.1       mysupr.network www.mysupr.network
    127.0.0.1       master.mysupr.network template-premium.mysupr.network plan.mysupr.network
    ```

    After this the setup is finished and we can try to access the application.

    * Master Website - [https://mysupr.network/](https://mysupr.network/)
    * Website with a payed package (for free) - [https://plan.mysupr.network/](https://plan.mysupr.network/)
    * Website with basic template (default) - [https://template-premium.mysupr.network/](https://template-premium.mysupr.network/)
    * Website to build templates - [https://builder.mysupr.network/](https://builder.mysupr.network/)

### Rebuild the environment

To delete and to recreate the complete environment following steps are required:

```shell
./docker-delete.sh
sudo rm supr-app/wordpress/wp-content/uploads/sites/* -rf
sudo docker-compose up -d --build
./fixtures-uploads.sh load
```

## Working with Docker

***Additional commands***

To stop all docker containers:

```shell
sudo ./docker-stop.sh
```

To enter in the docker container:

```shell
sudo ./docker-enter.sh [container-name]
```

_**Attention!!!** By executing the Docker delete script **all** Docker images will be deleted. So please be careful!_

## Local Runtime

Before starting the local deployment there are some components required.

Install the Apache web server with all needed PHP Modules:

```shell
sudo apt-get install apache2 libapache2-mod-php7.2 php7.2-pdo php7.2-mysql php7.2-pgsql php7.2-sqlite3 php-memcache php-memcached php-mongodb php-imagick php7.2-intl php7.2-bc php7.2-gd php7.2-mb php7.2-dom php7.2-json php7.2-xml php7.2-zip php7.2-bz2 php7.2-bcmath php7.2-curl php7.2-soap
```

Install the Memcached:

```shell
sudo apt-get install memcached php7.2-memcached
```

Install the MariaDB:

```shell
sudo apt-get install mariadb-server mariadb-client
```

Connect with the database:

```shell
mysql -u root -p
```

Create a user with his database:

```shell
CREATE DATABASE supr2_de;
CREATE USER 'supr2_de'@'%' IDENTIFIED BY 'qwer1234';
GRANT ALL ON supr2_de.* TO 'supr2_de'@'%';
FLUSH PRIVILEGES;
```

Next step is to  create the ```supr-app/wordpress/wp-config.php``` from ```supr-app/wordpress/wp-config-sample.php```:

```shell
cp supr-app/wordpress/wp-config-sample.php supr-app/wordpress/wp-config.php
```

And edit the config file:

```shell
nano supr-app/wordpress/wp-config.php
```

This parts are required:

```php
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'supr2_de');

/** MySQL database username */
define('DB_USER', 'supr2_de');

/** MySQL database password */
define('DB_PASSWORD', 'qwer1234');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');
```

**Attention!!!** The table prefix need to be set to empty string: ```$table_prefix  = '';```

```php
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = '';

define('DISALLOW_FILE_EDIT', true);

define('WP_POST_REVISIONS',false);

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Multisite */
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'mysupr.network');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

define('WP_CACHE', true);

// WP Ultimo: You need to copy the sunrise.php from the wp-ultimo directory to your wp-content directory.
define('SUNRISE', true);

define('ADMIN_COOKIE_PATH', '/');
define('COOKIE_DOMAIN', '');
define('COOKIEPATH', '');
define('SITECOOKIEPATH', '');

/* That's all, stop editing! Happy blogging. */
```

Load the initial WordPress database setup:

```shell
./fixtures-mariadb.sh load
```

Load the Apache configuration:

```shell
./apache.sh
```

## Related documents

Related documents within this repository:

* [Docker](docker.md) - read more about Docker, the installation and the usage.
* [Docker Compose](docker-compose.md) - in addition to docker we use Docker Compose to 
* [Kubernetes](Kubernetes.md)
* [Minikube](Minikube.md)

All the setup and documentation is based on following resources:

* [https://dev.to/foresthoffman/hosting-wordpress-over-https-with-docker-5gc](https://dev.to/foresthoffman/hosting-wordpress-over-https-with-docker-5gc)
* [https://www.devoply.com/](https://www.devoply.com/)
* [http://www.wordpressdocker.com/](http://www.wordpressdocker.com/)

The WordPress installation requires a https connection. We need to provide this via a reverse proxy:

* [https://github.com/SteveLTN/https-portal](https://github.com/SteveLTN/https-portal)
* [https://cloud.google.com/community/tutorials/nginx-reverse-proxy-docker](https://cloud.google.com/community/tutorials/nginx-reverse-proxy-docker)
* [https://github.com/jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy)
