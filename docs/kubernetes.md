# WordPress on Kubernetes

This documentation is based on the official docs:

- [Deploying WordPress and MySQL with Persistent Volumes](https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/)
- [WordPress in Kubernetes: The Perfect Setup](https://sysdig.com/blog/wordpress-kubernetes-perfect-setup/)
- [Kubernetes, Portworx, and WordPress: Scalability and HA](https://dzone.com/articles/kubernetes-and-wordpress-scalability-and-ha)

## Pre Configuration

Create the Secret object from the following command. You will need to replace YOUR_PASSWORD with the password you want to use:

```bash
kubectl create secret generic mysql-pass --from-literal=password=qwer1234
kubectl create secret generic mysql-root-pass --from-literal=password=qwer1234
```

Verify that the Secret exists by running the following command:

```bash
kubectl get secrets
```

> **Note:** To protect the Secret from exposure, neither get nor describe show its contents.

## MySQL Deployment

Deploy MySQL from the mysql-deployment.yaml file:

```bash
kubectl create -f mysql-deployment.yaml
```

Verify that a PersistentVolume got dynamically provisioned. Note that it can It can take up to a few minutes for the PVs to be provisioned and bound.

```bash
kubectl get pvc
```

Verify that the Pod is running by running the following command:

```bash
kubectl get pods
```

> **Note:** It can take up to a few minutes for the Pod’s Status to be RUNNING.

## WordPress Deployment

Create a WordPress Service and Deployment from the ```wordpress-deployment.yaml``` file:

```bash
kubectl create -f wordpress-deployment.yaml
```
Verify that a PersistentVolume got dynamically provisioned:

```bash
kubectl get pvc
```

> **Note:** It can take up to a few minutes for the PVs to be provisioned and bound.

Verify that the Service is running by running the following command:

```bash
kubectl get services wordpress
```

> **Note:** Minikube can only expose Services through ```NodePort```. The ```EXTERNAL-IP``` is always pending.

Run the following command to get the IP Address for the WordPress Service:

```bash
minikube service wordpress --url
```

## WordPress Docker Resources

https://hub.docker.com/r/_/wordpress/

https://github.com/docker-library/wordpress/blob/c919246e5ce9a94cb0ad275072d34563ef2ecc46/php7.2/fpm/Dockerfile
