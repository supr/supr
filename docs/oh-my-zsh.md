# Oh my zsh.

The Z-Shell (zsh) is a Unix shell that can be used both as an interactive login shell and as a powerful command line interpreter for shell scripts. The zsh is often seen as an extended Bourne shell which combines many improvements and features of bash, ksh and tcsh.

## Installation

Install pre requirements if not installed:

```shell
sudo apt install curl git zsh
```

Install "Oh My ZSH!" (oh-my-zsh) shell:

```shell
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh
```

If during the installation errors occur mainly there are related to the authorization. To fix this run change shell command:

```shell
chsh -s `which zsh` && /usr/bin/env zsh && . ~/.zshrc
```

## Extend with Plugins

Install Oh My ZSH Completion plugin for `symfony/console` based tools. Clone the repository into `~/.oh-my-zsh/custom/plugins/symfony-console` directory.

```shell
git clone https://github.com/jerguslejko/zsh-symfony-completion.git $ZSH_CUSTOM/plugins/symfony-console
```

Activate the plugin by inserting `symfony-console` into the plugins list in your `~/.zshrc`:

```shell
plugins=(symfony-console)
```

Optionally, specify which `symfony/console` tools you want to activate auto-completion for. If you do not define this variable, values below will be used as defaults. Please note, this line MUST appear before source `$ZSH/oh-my-zsh.sh`.

```shell
export SYMFONY_CONSOLE_TOOLS="composer artisan valet envoy bin/console"
```

Enabling Plugins (`zsh-autosuggestions` & `zsh-syntax-highlighting`):

* Download [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions):

    ```shell
    git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
    ```

* Download [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting)

    ```shell
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
    ```

* Activate both plugins by appending `nano ~/.zshrc` find `plugins=(git)` to `plugins=(git zsh-autosuggestions zsh-syntax-highlighting)`
* Reopen terminal

### Activate useful Plugins

Full list of current useful plugins:

```shell
# Symfony
export SYMFONY_CONSOLE_TOOLS="composer artisan valet envoy bin/console"

plugins=(
    ant
    autojump
    battery
    bower
    brew
    colorize
    composer
    command-not-found
    docker
    docker-compose
    extract
    git
    git-flow
    github
    history
    kubectl
    node
    npm
    phing
    pip
    python
    screen
    symfony-console
    symfony2
    vagrant
    yarn
    zsh-autosuggestions
    zsh-syntax-highlighting
)

# Terminal navigation
alias lll="ls -als"
alias ..="cd .."
alias ...="cd ../../"

# navigation aliases
alias dev="cd ~/lekkerland_dunit"

# kubectl aliases
alias k="kubectl"
alias kgx="kubectl config get-contexts"

# docker aliases
alias d="docker"
alias dps="docker ps"
```

## Themes

Install and activate the Agnoster Theme by installing the Powerline fonts:

```shell
sudo apt-get install software-properties-common
sudo apt-add-repository universe
sudo apt-get update
sudo apt-get install python3-pip
sudo pip3 install git+git://github.com/Lokaltog/powerline
wget https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf
sudo mv PowerlineSymbols.otf /usr/share/fonts/
sudo fc-cache -vf
sudo mv 10-powerline-symbols.conf /etc/fonts/conf.d/
```
