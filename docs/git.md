# GIT


## GIT Log

Get out of the GIT repository who is working on?

```bash
git log --pretty="%ae" | sort | uniq -c
git log -n 10 -p  
git log -pn 10
git log --pretty="%ae" | nl
git log --pretty="%h %ad %ae %s" | nl
git log --pretty="%h %ad %ae"
git log --decorate=full --all --pretty=format:'%h %d %s %cr %ae' --abbrev-commit
git log --decorate=full --all --pretty=format:'%h %d %s %cr %ae' --abbrev-commit | grep 'refs/tags'
git log --date-order --graph --tags --simplify-by-decoration --pretty=format:'%ai %h %d'
git log --date-order --graph --tags --simplify-by-decoration --pretty=format:'%ai %h %d'
```

## Removing sensitive data from a repository

Add your secrets to `private.txt` file.

Make the **bfg** tool available:

```shell
wget -O bfg.jar https://repo1.maven.org/maven2/com/madgag/bfg/1.13.2/bfg-1.13.2.jar
```

Now remove all private things from the repository and push back all the changes to the server:

```shell
java -jar bfg.jar --replace-text private.txt .
git reflog expire --expire=now --all && git gc --prune=now --aggressive
git push --force
```

_**Note:** before you can push all this changes you need to unprotect all branches._

Sources:

* https://docs.github.com/en/github/authenticating-to-github/removing-sensitive-data-from-a-repository
* https://rtyley.github.io/bfg-repo-cleaner/
