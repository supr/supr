#!/bin/bash

# include bash base
. _base.sh

read -p "${orange}Do you want to append hosts config to /etc/hosts file (y/n)? ${reset}"
if [[ "$REPLY" == "y" ]]
then
    sudo bash -c "echo '' >> /etc/hosts"
    sudo bash -c "cat hosts >> /etc/hosts"
fi
